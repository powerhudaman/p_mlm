CREATE TABLE `log_aktivitas_2` (
`id`  int(12) NOT NULL AUTO_INCREMENT ,
`id_admin`  varchar(50) NULL ,
`id_member`  varchar(50) NULL ,
`halaman`  text NULL ,
`aksi`  int(3) NULL ,
`created_date`  datetime NULL ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`id`)
)
;


# catatan kode aksi log aktivitas 2
# 1 = insert
# 2 = update
# 3 = delete
# 4 = approve
