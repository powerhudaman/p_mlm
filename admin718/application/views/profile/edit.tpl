{extends file='index_admin_.tpl'}
{block name=content}
  	
  	<div class="row">
  		<!--<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">
  					
  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>-->
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                   {if $this->session->flashdata('pesan') !=""}
                      {$this->session->flashdata('pesan')}
                    {/if}
                </div><!-- /.box-header -->
                <!-- form start -->
                
                {form_open_multipart(site_url($url))}
                  
                  <div class="box-body">
                      
                      <div class="form-group">
                        {form_error('username')}
                        <label for=""><span class="text-red">*</span> Username</label>
                        <input type="text" name="username" id="" class="form-control" value="{$edit['username']}" readonly="readonly">
                        <input type="hidden" name="user_id" value="{$edit['user_id']}" readonly="readonly">
                      </div>

                      <div class="form-group">
                        {form_error('password')}
                        <label for=""><span class="text-red">*</span> Password</label>
                        <input type="password" name="password" id="" class="form-control">
                      </div>

                      <div class="form-group">
                        {form_error('password-konfirm')}
                        <label for=""><span class="text-red">*</span> Konfirmasi Password</label>
                        <input type="password" name="password-konfirm" id="" class="form-control">
                      </div>

                      <div class="form-group">
                        {form_error('nama_depan')}
                        <label for=""><span class="text-red">*</span> Nama Depan</label>
                        <input type="text" name="nama_depan" id="nama_depan" class="form-control" value="{$edit['nama_depan']}">
                      </div>
                      
                      <div class="form-group">
                        {form_error('nama_belakang')}
                        <label for=""><span class="text-red">*</span> Nama Belakang</label>
                        <input type="text" name="nama_belakang" id="" class="form-control" value="{$edit['nama_belakang']}">
                      </div>

                      <div class="form-group">
                        {form_error('foto')}
                        <label for="">Foto Profil</label>
                        <input type="file" name="foto" id="">
                      </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}