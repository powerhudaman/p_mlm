{extends file='index_admin_.tpl'}
{block name=content}
  	
  	<div class="row">
  		<!--<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">
  					
  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>-->
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {assign var=foo value=['action'=>site_url($url),'b'=>'blue']}
                
                {assign var=akses value=json_decode($edit['hak_akses'],true)}
                {form_open(site_url($url))}
                  
                  <div class="box-body">
                      
                      <div class="form-group">
                        {form_error('nama')}
                        <label for=""><span class="text-red">*</span> Nama Group</label>
                        <input type="text" name="nama" id="nama" class="form-control" value="{$edit['nama']}">
                        <input type="hidden" name="id" value="{$edit['id']}">
                      </div>
                      <div class="form-group">
                        {form_error('keterangan')}
                        <label for="">Keterangan</label>
                        <textarea name="keterangan" id="keterangan" cols="30" rows="10" class="form-control">{$edit['keterangan']}</textarea>
                      </div>

                      <div class="form-group">
                        <label for="">Hak Akses</label>
                        <table class="table table-striped">
                          <tr>
                              <th>Nama Modul</th>
                              <th>Tanpa Akses</th>
                              <th>Baca</th>
                              <th>Tulis</th>
                          </tr>
                          {foreach from=$hak_akses key=k item=v}
                            
                            <tr>
                              <th colspan="4">{$k}</th>
                            </tr>

                            {foreach from=$v key=b item=hak}
                              {assign var=ceked_0 value=''}
                              {assign var=ceked_1 value=''}
                              {assign var=ceked_2 value=''}

                              {if $akses[$k][$hak] == 0}
                                  {$ceked_0='checked'}
                              {/if}

                              {if $akses[$k][$hak] == 1}
                                  {$ceked_1='checked'}
                              {/if}

                              {if $akses[$k][$hak] == 2}
                                  {$ceked_2='checked'}
                              {/if}
                              
                              <tr>
                                <td>{$hak}</td>
                                <td><input type="radio" name="hak_akses[{$k}][{$hak}]" value="0" {$ceked_0}></td>
                                <td><input type="radio" name="hak_akses[{$k}][{$hak}]" value="1" {$ceked_1}></td>
                                <td><input type="radio" name="hak_akses[{$k}][{$hak}]" value="2" {$ceked_2}></td>
                              </tr>
                            {/foreach}
                          {/foreach}
                        </table>
                      </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>

                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}