{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<!--<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>-->
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                {assign var=foo value=['action'=>site_url('admingroup/save'),'b'=>'blue']}

                {form_open(site_url($url))}

                  <div class="box-body">

                      <div class="form-group">
                        {form_error('nama')}
                        <label for=""><span class="text-red">*</span> Nama Group</label>
                        <input type="text" name="nama" id="nama" class="form-control" value="{set_value('nama')}">
                      </div>
                      <div class="form-group">
                        {form_error('keterangan')}
                        <label for="">Keterangan</label>
                        <textarea name="keterangan" id="keterangan" cols="30" rows="10" class="form-control">{set_value('keterangan')}</textarea>
                      </div>
                      <div class="form-group">
                        <label for="">Hak Akses</label>
                        <table class="table table-striped">
                          <tr>
                              <th>Nama Modul</th>
                              <th>Tanpa Akses</th>
                              <th>Baca</th>
                              <th>Tulis</th>
                          </tr>

                          {foreach from=$hak_akses key=k item=v}

                            <tr>
                              <th colspan="4">{$k}</th>
                            </tr>

                            {foreach from=$v key=b item=hak}

                              <tr>
                                <td>{$hak}</td>
                                <td><input type="radio" name="hak_akses[{$k}][{$hak}]" value="0" id="" checked="checked"></td>
                                <td><input type="radio" name="hak_akses[{$k}][{$hak}]" value="1" id=""></td>
                                <td><input type="radio" name="hak_akses[{$k}][{$hak}]" value="2" id=""></td>
                              </tr>
                            {/foreach}
                          {/foreach}
                        </table>
                      </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}
