{extends file='index_admin_.tpl'}
{block name=content}
  	{form_open_multipart(site_url($url))}
  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">
            {$page_header}
  				</div>
  				<div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="">Modul Type</label>
                  <select name="modul_type" id="modul_type" class="form-control">
                    {foreach from=$modul_types key=k item=modul_type}
                      <option value="{$k}">{$modul_type}</option>
                    {/foreach}
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="">Metode Install</label>
                  <select name="modul_metode" id="modul_metode" class="form-control">
                    {foreach from=$modul_metodes key=k item=modul_metode}
                      <option value="{$k}">{$modul_metode}</option>
                    {/foreach}
                  </select>
                </div>
              </div>
  				</div>
  			</div>
  		</div> <!-- end type modul -->
      
      <!-- data modul -->
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h4>Data Modul</h4>
          </div>
          <div class="box-body">
            
            <div class="form-group">
              <label for="">Nama Modul</label>
              <input type="text" name="nama_modul" id="nama_modul" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Tangal Dibuat</label>
              <input type="text" name="create_date" id="create_date" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Pembuat</label>
              <input type="text" name="author" id="author" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Email Pembuat</label>
              <input type="text" name="author_email" id="author_email" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Url Website Pembuat</label>
              <input type="text" name="author_url" id="author_url" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Hak Cipta</label>
              <input type="text" name="copyright" id="copyright" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Lisensi</label>
              <input type="text" name="lisensi" id="lisensi" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Versi</label>
              <input type="text" name="version" id="version" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Deskripsi</label>
              <textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
            </div>
            <div class="form-group">
              
              <a href="#" class="btn btn-primary" onclick="buat_folder()"><i class="fa fa-folder"></i> Buat Folder Packaging</a>
            </div>
          </div>
        </div>
      </div>
      <!-- end data modul -->
      <!-- data require -->
      <div class="col-md-12" id="form_require">
        <div class="box">
          <div class="box-header">
            <h3>Require</h3>
          </div>
          <div class="box-body">
            <div class="col-md-6">
              <label for="">Type</label>
              <select name="type_require" id="type_require" class="form-control">
                <option value="component">Component</option>
                <option value="module">Module</option>
                <option value="plugin">Plugin</option>
                <option value="template">Template</option>
              </select>
            </div>
            <div class="col-md-5">
              <label for="">Nama </label>
              <input type="text" name="nama_require" id="nama_require" class="form-control">
              
            </div>
            <div class="col-md-1">
              <a href="#form_require" class="btn btn-sm btn-primary" data-no="0" onclick="tambah_require()" id="b_require"><i class="fa fa-plus"></i></a>
            </div>

            <!-- tabel begin -->
              <div class="col-md-12">
                <table class="table table-bordered table-striped" id="tabel_require">
                  <thead>
                    <th>Type</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                  </thead>
                  <!--<tr id="">
                    <td><input type="text" name="require[][type]" id="" class="form-control" readonly="readonly"></td>
                    <td><input type="text" name="require[][nama]" id="" class="form-control" readonly="readonly"></td>
                    <td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_require()"><i class="fa fa-trash"></i></a></td>
                  </tr>-->
                </table>
              </div>
            <!-- tabel end -->
          </div>
        </div>
      </div>
      <!-- end data require -->
      <!-- data filebackend begin -->
      <div class="col-md-12" id="form_filebackend">
        <div class="box">
          <div class="box-header">
            <h3>File Backend</h3>
          </div>
          <div class="box-body">
            <div class="row"> <!-- row 1 -->
                <div class="col-md-6">
                  <label for="">Origin <span class="text-red">*</span></label>
                  <input type="text" name="origin_backend" id="origin_backend" class="form-control">
                </div>
                <div class="col-md-6">
                  <label for="">Destination <span class="text-red">*</span></label>
                  <input type="text" name="destination_backend" id="destination_backend" class="form-control">
                </div>
            </div><!-- end row 1 -->
            <div class="row"><!-- row 2 -->
                <div class="col-md-12">
                  <table class="table">
                    <tr>
                      <td>File Item</td>
                      <td>
                        <select name="backend_file_type" id="backend_file_type" class="form-control">
                          <option value=""></option>
                          <option value="filename">filename</option>
                          <option value="folder">folder</option>
                        </select>
                      </td>
                      <td>
                        <input type="file" name="backend_file_browser" id="backend_file_browser">
                        <input type="text" name="backend_file_name" id="backend_file_name" class="form-control" placeholder="Nama File">
                        <input type="text" name="backend_folder_browser" id="backend_folder_browser" class="form-control" placeholder="Alamat Folder Yang Akan Dicopy">
                      </td>
                      <td>Destination</td>
                      <td><input type="text" name="backend_file_destination" id="backend_file_destination" class="form-control"></td>
                      <td><a href="#form_filebackend" class="btn btn-primary" data-no="0" id="b_filebackend" onclick="tambah_backend()"><i class="fa fa-plus"></i></a></td>
                    </tr>
                  </table>
                </div>
            </div><!-- end row 2 -->
            <div class="row"><!-- row 3 -->
                <table class="table table-bordered table-striped" id="tabel_file_backend">
                  <thead>
                    <th>Type</th>
                    <th>File / Folder</th>
                    <th>Destination</th>
                    <th>Aksi</th>
                  </thead>
                  <!--<tr id="">
                    <td><input type="text" name="file_backend[][filetype]" id="" class="form-control" value="" readonly="readonly"></td>
                    <td><input type="text" name="file_backend[][fileitem]" id="" class="form-control" value="" readonly="readonly"></td>
                    <td><input type="text" name="file_backend[][destination]" id="" class="form-control" value="" readonly="readonly"></td>
                    <td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_backend()"><i class="fa fa-trash"></i></a></td>
                  </tr>-->
                </table>
            </div><!-- end row 3 -->
          </div>
        </div>
      </div>
      <!-- data filebackend end -->

      <!-- data filebackend uninstall begin -->
      <div class="col-md-12" id="form_backend_uninstall">
        <div class="box">
          <div class="box-header">
            <h3>File Backend uninstall</h3>
          </div>
          <div class="box-body">
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Nama Folder</label>
                <input type="text" name="nama_folder_backend" id="nama_folder_backend" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Nama Direktori</label>
                <input type="text" name="nama_direktori_backend" id="nama_direktori_backend" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <a href="#form_backend_uninstall" class="btn btn-primary" data-no="0" id="b_backend_uninstall" onclick="tambah_backend_uninstall()"><i class="fa fa-plus"></i></a>
            </div>
            <!-- tabel begin -->
            <div class="col-md-12">
              <table class="table table-bordered table-striped" id="tabel_backend_uninstall">
                <thead>
                  <th>Folder</th>
                  <th>Direktori</th>
                  <th>Aksi</th>
                </thead>
                <!--<tr id="">
                  <td><input type="text" name="files_uninstall_backend[][folder]" id="" value="" class="form-control" readonly="readonly"></td>
                  <td><input type="text" name="files_uninstall_backend[][direktori]" id="" value="" class="form-control" readonly="readonly"></td>
                  <td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_backend_uninstall()"><i class="fa fa-trash"></i></a></td>
                </tr>-->
              </table>
            </div>
            <!-- tabel end -->
          </div>
        </div>
      </div>
      <!-- data filebackend uninstall end -->

      <!-- data filefrontend begin -->
      <div class="col-md-12" id="form_filefrontend">
        <div class="box">
          <div class="box-header">
            <h3>File Frontend</h3>
          </div>
          <div class="box-body">
            <div class="row"> <!-- row 1 -->
                <div class="col-md-6">
                  <label for="">Origin <span class="text-red">*</span></label>
                  <input type="text" name="origin_frontend" id="origin_frontend" class="form-control">
                </div>
                <div class="col-md-6">
                  <label for="">Destination <span class="text-red">*</span></label>
                  <input type="text" name="destination_frontend" id="destination_frontend" class="form-control">
                </div>
            </div><!-- end row 1 -->
            <div class="row"><!-- row 2 -->
                <div class="col-md-12">
                  <table class="table">
                    <tr>
                      <td>File Item</td>
                      <td>
                        <select name="frontend_file_type" id="frontend_file_type" class="form-control">
                          <option value=""></option>
                          <option value="filename">filename</option>
                          <option value="folder">folder</option>
                        </select>
                      </td>
                      <td>
                        <input type="file" name="frontend_file_browser" id="frontend_file_browser">
                        <input type="text" name="frontend_file_name" id="frontend_file_name" class="form-control" placeholder="Nama File">
                        <input type="text" name="frontend_folder_browser" id="frontend_folder_browser" class="form-control" placeholder="Alamat Folder Yang Akan Dicopy">
                      </td>
                      <td>Destination</td>
                      <td><input type="text" name="frontend_file_destination" id="frontend_file_destination" class="form-control"></td>
                      <td><a href="#form_filefrontend" class="btn btn-primary" data-no="0" id="b_filebackend" onclick="tambah_frontend()"><i class="fa fa-plus"></i></a></td>
                    </tr>
                  </table>
                </div>
            </div><!-- end row 2 -->
            <div class="row"><!-- row 3 -->
                <table class="table table-bordered table-striped" id="tabel_file_frontend">
                  <thead>
                    <th>Type</th>
                    <th>File / Folder</th>
                    <th>Destination</th>
                    <th>Aksi</th>
                  </thead>
                  <!--<tr id="">
                    <td><input type="text" name="file_frontend[][filetype]" id="" class="form-control" value="" readonly="readonly"></td>
                    <td><input type="text" name="file_frontend[][fileitem]" id="" class="form-control" value="" readonly="readonly"></td>
                    <td><input type="text" name="file_frontend[][destination]" id="" class="form-control" value="" readonly="readonly"></td>
                    <td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_frontend()"><i class="fa fa-trash"></i></a></td>
                  </tr>-->
                </table>
            </div><!-- end row 3 -->
          </div>
        </div>
      </div>
      <!-- data filefrontend end -->

      <!-- data filefrontend uninstall begin -->
      <div class="col-md-12" id="form_frontend_uninstall">
        <div class="box">
          <div class="box-header">
            <h3>File Frontend uninstall</h3>
          </div>
          <div class="box-body">
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Nama Folder</label>
                <input type="text" name="nama_folder_frontend" id="nama_folder_frontend" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Nama Direktori</label>
                <input type="text" name="nama_direktori_frontend" id="nama_direktori_frontend" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <a href="#form_frontend_uninstall" class="btn btn-primary" data-no="0" id="b_frontend_uninstall" onclick="tambah_frontend_uninstall()"><i class="fa fa-plus"></i></a>
            </div>
            <!-- tabel begin -->
            <div class="col-md-12">
              <table class="table table-bordered table-striped" id="tabel_frontend_uninstall">
                <thead>
                  <th>Folder</th>
                  <th>Direktori</th>
                  <th>Aksi</th>
                </thead>
                <!--<tr id="">
                  <td><input type="text" name="files_uninstall_frontend[][folder]" id="" value="" class="form-control" readonly="readonly"></td>
                  <td><input type="text" name="files_uninstall_frontend[][direktori]" id="" value="" class="form-control" readonly="readonly"></td>
                  <td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_frontend_uninstall()"><i class="fa fa-trash"></i></a></td>
                </tr>-->
              </table>
            </div>
            <!-- tabel end -->
          </div>
        </div>
      </div>
      <!-- data filefrontend uninstall end -->

      <!-- data query begin -->
      <div class="col-md-12" id="form_query_install">
        <div class="box">
          <div class="box-header">
            <h3>File Query Install</h3>
          </div>
          <div class="box-body">
            <div class="col-md-8">
              <div class="form-group">
                <!--<label for="">File SQL</label>
                <input type="file" name="query_install" id="query_install">-->
                <label for="">Nama Query</label>
                <input type="text" name="nama_query" id="nama_query" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <a href="#form_query_install" class="btn btn-primary" data-no="0" id="b_query_install" onclick="tambah_query_install()"><i class="fa fa-plus"></i></a>
            </div>
            <!-- tabel begin -->
            <div class="col-md-12">
              <table class="table table-bordered table-striped" id="tabel_query_install">
                <thead>
                  <th>File Query</th>
                  <th>Aksi</th>
                </thead>
                <!--<tr id="">
                  <td><input type="text" name="require[][nama]" id="" class="form-control" readonly="readonly"></td>
                  <td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_require()"><i class="fa fa-trash"></i></a></td>
                </tr>-->
              </table>
            </div>
            <!-- tabel end -->
          </div>
        </div>
      </div>
      <!-- data query end -->

      <!-- data query uninstall begin -->
      <div class="col-md-12" id="form_query_uninstall">
        <div class="box">
          <div class="box-header">
            <h3>File Query Uninstall</h3>
          </div>
          <div class="box-body">
            <div class="col-md-8">
              <div class="form-group">
                <label for="">Nama Query</label>
                <input type="text" name="nama_query_uninstall" id="nama_query_uninstall" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <a href="#form_query_uninstall" class="btn btn-primary" data-no="0" id="b_query_uninstall" onclick="tambah_query_uninstall()"><i class="fa fa-plus"></i></a>
            </div>
            <!-- tabel begin -->
            <div class="col-md-12">
              <table class="table table-bordered table-striped" id="tabel_query_uninstall">
                <thead>
                  <th>File Query</th>
                  <th>Aksi</th>
                </thead>
                <!--<tr id="">
                  <td><input type="text" name="require[][nama]" id="" class="form-control" readonly="readonly"></td>
                  <td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_require()"><i class="fa fa-trash"></i></a></td>
                </tr>-->
              </table>
            </div>
            <!-- tabel end -->
          </div>
        </div>
      </div>
      <!-- data query uninstall end -->
      
      <div class="col-md-12">
        <div class="box">
          <div class="box-header"></div>
          <div class="box-body"></div>
        </div>
      </div>
  		<div class="col-md-12 col-sm-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">

          </div><!-- /.box-header -->
          <!-- form start -->
          
            <div class="box-body">
            
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          
        </div><!-- /.box -->
      </div>
  	</div> <!-- end row -->
      {form_close()}

{/block}

{block name=sidebar-menu}
   <ul class="sidebar-menu">
      <!-- Optionally, you can add icons to the links -->
      <li><a href="{site_url('extension/fetch_component')}"><i class="fa fa-th"></i> <span>Component</span></a></li>
      <li><a href="{site_url('extension/fetch_modul')}"><i class="fa  fa-indent"></i> <span>Module</span></a></li>
      <li><a href="{site_url('extension/fetch_plugin')}"><i class="fa fa-plug"></i> <span>Plugin</span></a></li>
      <li><a href="{site_url('extension/fetch_template')}"><i class="fa fa-plug"></i> <span>Template</span></a></li>
    </ul><!-- /.sidebar-menu -->
{/block}

{block name=script_js}
    
    <script>
        jQuery(document).ready(function($) {
          $('#create_date').datepicker({ format:'yyyy-mm-dd' });
          $('#backend_file_browser').hide();
          $('#backend_file_name').hide();
          $('#backend_folder_browser').hide();

          $('#backend_file_type').change(function(e) {
             $('#backend_file_browser').val('');
             $('#backend_file_name').val('');
             $('#backend_folder_browser').val('');
             var status = $('#backend_file_type').val();
              if(status == "filename"){
                 $('#backend_file_browser').show();
                 $('#backend_file_name').show();
                 $('#backend_folder_browser').hide();

              }
              if(status == "folder"){
                 $('#backend_file_browser').hide();
                 $('#backend_file_name').hide();
                 $('#backend_folder_browser').show();
              }if(status == ""){
                 $('#backend_file_browser').hide();
                 $('#backend_file_name').hide();
                 $('#backend_folder_browser').hide();
              }

          });

           /// front end

          $('#frontend_file_browser').hide();
          $('#frontend_file_name').hide();
          $('#frontend_folder_browser').hide();

          $('#frontend_file_type').change(function(e) {
             $('#frontend_file_browser').val('');
             $('#frontend_file_name').val('');
             $('#frontend_folder_browser').val('');
             var status = $('#frontend_file_type').val();
              if(status == "filename"){
                 $('#frontend_file_browser').show();
                 $('#frontend_file_name').show();
                 $('#frontend_folder_browser').hide();

              }
              if(status == "folder"){
                 $('#frontend_file_browser').hide();
                 $('#frontend_file_name').hide();
                 $('#frontend_folder_browser').show();
              }if(status == ""){
                 $('#frontend_file_browser').hide();
                 $('#frontend_file_name').hide();
                 $('#frontend_folder_browser').hide();
              }

          });


        });
        // fungsi ini digunakan untuk membuat folder packaging
        var buat_folder = function(){
          var nama_modul = $('#nama_modul').val();
          $.ajax({
              url: "{site_url('packager/buat_folder_packaging')}",
              type: 'post',
              dataType:'json',
              data: { 'nama_modul':nama_modul },
              success: function (data) {
                alert(data.pesan);
              }
            });
        }// end buat_folder()

        var tambah_require = function(){
          var no = $('#b_require').attr('data-no');
          var type = $('#type_require').val();
          var nama = $('#nama_require').val();
          no = parseInt(no) + 1;

          var html ='';

              html +='<tr id="td_require'+no+'">';
                    html +='<td><input type="text" name="require['+no+'][type]" id="" class="form-control" value="'+type+'" readonly="readonly"></td>';
                    html +='<td><input type="text" name="require['+no+'][nama]" id="" class="form-control" value="'+nama+'" readonly="readonly"></td>';
                    html +='<td><a href="#form_require" class="btn btn-sm btn-danger" onclick="hapus_require('+no+')"><i class="fa fa-trash"></i></a></td>';
                  html +='</tr>';
          $('#tabel_require').append(html);
          $('#b_require').attr('data-no',no);
          $('#nama_require').val('');

        }

        var hapus_require = function(no){
            $('#td_require'+no).remove();
        }

        var tambah_query_install = function(){
            var no = $('#b_query_install').attr('data-no');
            var nama_modul = $('#nama_modul').val();
            var nama_query = $('#nama_query').val();
            
                no = parseInt(no) + 1;
            var html ='';
            
            html +='<tr id="td_query_install'+no+'">';
              html +='<td><input type="text" name="query_files['+no+'][sql]" id="query_files'+no+'" class="form-control" value="'+nama_query+'" readonly="readonly"></td>';
              html +='<td><a href="#form_query_install" class="btn btn-sm btn-danger" onclick="hapus_query_install('+no+')"><i class="fa fa-trash"></i></a></td>';
            html +='</tr>';
            $('#tabel_query_install').append(html);
            $('#b_query_install').attr('data-no',no);
            $('#nama_query').val('');
        }

        var hapus_query_install = function(no){
          $('#td_query_install'+no).remove();
        }

        var tambah_query_uninstall = function(){
            var no = $('#b_query_uninstall').attr('data-no');
            var nama_modul = $('#nama_modul').val();
            var nama_query = $('#nama_query_uninstall').val();
            
                no = parseInt(no) + 1;
            var html ='';
            
            html +='<tr id="td_query_uninstall'+no+'">';
              html +='<td><input type="text" name="query_files_uninstall['+no+'][sql]" id="query_files_uninstall'+no+'" class="form-control" value="'+nama_query+'" readonly="readonly"></td>';
              html +='<td><a href="#form_query_uninstall" class="btn btn-sm btn-danger" onclick="hapus_query_uninstall('+no+')"><i class="fa fa-trash"></i></a></td>';
            html +='</tr>';
            $('#tabel_query_uninstall').append(html);
            $('#b_query_uninstall').attr('data-no',no);
            $('#nama_query').val('');
        }

        var hapus_query_uninstall = function(no){
          $('#td_query_uninstall'+no).remove();
        }

        var tambah_backend = function(){

          var no = $('#b_filebackend').attr('data-no');
          no = parseInt(no) + 1;
          var filetype = $('#backend_file_type').val();
          var nama_modul = $('#nama_modul').val();
          var html ='';

          if(filetype == "filename"){
            var formdata = new FormData();
            var fileitem = document.getElementById('backend_file_browser');
            var filename = $('#backend_file_name').val();
            var backend_destination = $('#backend_file_destination').val();



            formdata.append('nama_modul',nama_modul);
            formdata.append('fileitem',fileitem.files[0]);
            formdata.append('filename',filename);
            formdata.append('filetype',filetype);
            $.ajax({
              url: "{site_url('packager/upload_backend')}",
              type: 'POST',
              dataType: 'json',
              contentType:false,
              processData:false,
              data: formdata,
              success:function(data){
                  
                  html +='<tr id="td_file_backend'+no+'">';
                    html +='<td><input type="text" name="file_backend['+no+'][filetype]" id="filetype'+no+'" class="form-control" value="'+filetype+'" readonly="readonly"></td>';
                    html +='<td><input type="text" name="file_backend['+no+'][fileitem]" id="filename'+no+'" class="form-control" value="'+data.filename+'" readonly="readonly"></td>';
                    html +='<td><input type="text" name="file_backend['+no+'][destination]" id="" class="form-control" value="'+backend_destination+'" readonly="readonly"></td>';
                    html +='<td><a href="#form_filebackend" class="btn btn-sm btn-danger" onclick="hapus_backend('+no+')"><i class="fa fa-trash"></i></a></td>'
                  html +='</tr>';

                  $('#backend_file_type').val('');
                  $('#backend_file_name').val('');
                  $('#backend_file_destination').val('');
                  $('#backend_file_browser').val('');

                  $('#b_filebackend').attr('data-no',no);
                  $('#tabel_file_backend').append(html);
              }
            })
            

          }
          if(filetype == "folder"){
              var formdata = new FormData();
              var folderdir = $('#backend_folder_browser').val();
              var backend_destination = $('#backend_file_destination').val();

              formdata.append('nama_modul',nama_modul);
              formdata.append('folderdir',folderdir);
              formdata.append('filetype',filetype);
              $.ajax({
              url: "{site_url('packager/upload_backend')}",
              type: 'POST',
              dataType: 'json',
              contentType:false,
              processData:false,
              data: formdata,
              success:function(data){
                  html +='<tr id="td_file_backend'+no+'">';
                    html +='<td><input type="text" name="file_backend['+no+'][filetype]" id="filetype'+no+'" class="form-control" value="'+filetype+'" readonly="readonly"></td>';
                    html +='<td><input type="text" name="file_backend['+no+'][fileitem]" id="filename'+no+'" class="form-control" value="'+data.foldername+'" readonly="readonly"></td>';
                    html +='<td><input type="text" name="file_backend['+no+'][destination]" id="" class="form-control" value="'+backend_destination+'" readonly="readonly"></td>';
                    html +='<td><a href="#tabel_file_backend" class="btn btn-sm btn-danger" onclick="hapus_backend('+no+')"><i class="fa fa-trash"></i></a></td>'
                  html +='</tr>';

                  $('#backend_file_type').val('');
                  $('#backend_file_destination').val('');
                  $('#backend_file_destination').val('');

                  $('#b_filebackend').attr('data-no',no);
                  $('#tabel_file_backend').append(html);
                  
              }
            })
              
            
          }

        }

        var hapus_backend = function(no){
            var filetype = $('#filetype'+no).val();
            var fileitem = $('#filename'+no).val();
            var nama_modul = $('#nama_modul').val();

            $.ajax({
              url: "{site_url('packager/deload_backend')}",
              type: 'post',
              dataType: 'json',
              data: { nama_modul:nama_modul, filetype:filetype, fileitem:fileitem },
              success:function(){
                $('#td_file_backend'+no).remove();
              }
            })
            
        }

        var tambah_backend_uninstall = function(){
            var no = $('#b_backend_uninstall').attr('data-no');
            var nama_folder = $('#nama_folder_backend').val();
            var nama_direktori = $('#nama_direktori_backend').val();
            
                no = parseInt(no) + 1;
            var html ='';
            
            html +='<tr id="td_backend_uninstall'+no+'">';
              html +='<td><input type="text" name="files_uninstall_backend['+no+'][folder]" id="" value="'+nama_folder+'" class="form-control" readonly="readonly"></td>';
              html +='<td><input type="text" name="files_uninstall_backend['+no+'][direktori]" id="" value="'+nama_direktori+'" class="form-control" readonly="readonly"></td>';
              html +='<td><a href="#form_backend_uninstall" class="btn btn-sm btn-danger" onclick="hapus_backend_uninstall('+no+')"><i class="fa fa-trash"></i></a></td>';
            html +='</tr>';

            $('#tabel_backend_uninstall').append(html);
            $('#b_backend_uninstall').attr('data-no',no);
            $('#nama_folder_backend').val('');
            $('#nama_direktori_backend').val('');

        }

        var hapus_backend_uninstall = function(no){
          $('#td_backend_uninstall'+no).remove();
        }

        var tambah_frontend = function(){

            var no = $('#b_filebackend').attr('data-no');
            no = parseInt(no) + 1;
            var filetype = $('#frontend_file_type').val();
            var nama_modul = $('#nama_modul').val();
            var html ='';

            if(filetype == "filename"){
              var formdata = new FormData();
              var fileitem = document.getElementById('frontend_file_browser');
              var filename = $('#frontend_file_name').val();
              var frontend_destination = $('#frontend_file_destination').val();



              formdata.append('nama_modul',nama_modul);
              formdata.append('fileitem',fileitem.files[0]);
              formdata.append('filename',filename);
              formdata.append('filetype',filetype);
              $.ajax({
                url: "{site_url('packager/upload_frontend')}",
                type: 'POST',
                dataType: 'json',
                contentType:false,
                processData:false,
                data: formdata,
                success:function(data){
                    
                    html +='<tr id="td_file_frontend'+no+'">';
                      html +='<td><input type="text" name="file_frontend['+no+'][filetype]" id="filetype'+no+'" class="form-control" value="'+filetype+'" readonly="readonly"></td>';
                      html +='<td><input type="text" name="file_frontend['+no+'][fileitem]" id="filename'+no+'" class="form-control" value="'+data.filename+'" readonly="readonly"></td>';
                      html +='<td><input type="text" name="file_frontend['+no+'][destination]" id="" class="form-control" value="'+frontend_destination+'" readonly="readonly"></td>';
                      html +='<td><a href="#form_filefrontend" class="btn btn-sm btn-danger" onclick="hapus_frontend('+no+')"><i class="fa fa-trash"></i></a></td>'
                    html +='</tr>';

                    $('#frontend_file_type').val('');
                    $('#frontend_file_name').val('');
                    $('#frontend_file_destination').val('');
                    $('#frontend_file_browser').val('');

                    $('#b_filebackend').attr('data-no',no);
                    $('#tabel_file_frontend').append(html);
                }
              })
              

            }
            if(filetype == "folder"){
                var formdata = new FormData();
                var folderdir = $('#frontend_folder_browser').val();
                var frontend_destination = $('#frontend_file_destination').val();

                formdata.append('nama_modul',nama_modul);
                formdata.append('folderdir',folderdir);
                formdata.append('filetype',filetype);
                $.ajax({
                url: "{site_url('packager/upload_frontend')}",
                type: 'POST',
                dataType: 'json',
                contentType:false,
                processData:false,
                data: formdata,
                success:function(data){
                    html +='<tr id="td_file_frontend'+no+'">';
                      html +='<td><input type="text" name="file_frontend['+no+'][filetype]" id="filetype'+no+'" class="form-control" value="'+filetype+'" readonly="readonly"></td>';
                      html +='<td><input type="text" name="file_frontend['+no+'][fileitem]" id="filename'+no+'" class="form-control" value="'+data.foldername+'" readonly="readonly"></td>';
                      html +='<td><input type="text" name="file_frontend['+no+'][destination]" id="" class="form-control" value="'+frontend_destination+'" readonly="readonly"></td>';
                      html +='<td><a href="#tabel_file_frontend" class="btn btn-sm btn-danger" onclick="hapus_frontend('+no+')"><i class="fa fa-trash"></i></a></td>'
                    html +='</tr>';

                    $('#frontend_file_type').val('');
                    $('#frontend_file_destination').val('');
                    $('#frontend_file_destination').val('');

                    $('#b_filebackend').attr('data-no',no);
                    $('#tabel_file_frontend').append(html);
                    
                }
              })
                
              
            }

        }

        var hapus_frontend = function(no){
            var filetype = $('#filetype'+no).val();
            var fileitem = $('#filename'+no).val();
            var nama_modul = $('#nama_modul').val();

            $.ajax({
              url: "{site_url('packager/deload_frontend')}",
              type: 'post',
              dataType: 'json',
              data: { nama_modul:nama_modul, filetype:filetype, fileitem:fileitem },
              success:function(){
                $('#td_file_frontend'+no).remove();
              }
            })
            
        }

        var tambah_frontend_uninstall = function(){
            var no = $('#b_frontend_uninstall').attr('data-no');
            var nama_folder = $('#nama_folder_frontend').val();
            var nama_direktori = $('#nama_direktori_frontend').val();
            
                no = parseInt(no) + 1;
            var html ='';
            
            html +='<tr id="td_frontend_uninstall'+no+'">';
              html +='<td><input type="text" name="files_uninstall_frontend['+no+'][folder]" id="" value="'+nama_folder+'" class="form-control" readonly="readonly"></td>';
              html +='<td><input type="text" name="files_uninstall_frontend['+no+'][direktori]" id="" value="'+nama_direktori+'" class="form-control" readonly="readonly"></td>';
              html +='<td><a href="#form_frontend_uninstall" class="btn btn-sm btn-danger" onclick="hapus_frontend_uninstall('+no+')"><i class="fa fa-trash"></i></a></td>';
            html +='</tr>';

            $('#tabel_frontend_uninstall').append(html);
            $('#b_frontend_uninstall').attr('data-no',no);
            $('#nama_folder_frontend').val('');
            $('#nama_direktori_frontend').val('');

        }

        var hapus_frontend_uninstall = function(no){
          $('#td_frontend_uninstall'+no).remove();
        }



    </script>

{/block}