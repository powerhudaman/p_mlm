{extends file='index_admin_.tpl'}
{block name=content}
  	
  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">
  					
  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Quick Example</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {form_open('')}
                  <div class="box-body">
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}