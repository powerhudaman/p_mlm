<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{$this->data['site_name']}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{base_url('assets')}/bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{base_url('assets')}/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="{base_url('assets')}/plugins/pnotify/pnotify.custom.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="{base_url('assets')}/dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="{base_url('assets')}/bootstrap/css/dropdown-hover.css">



    {if isset($js_head)}

          {foreach from=$js_head item=js }
              <script src="{base_url('assets')}/{$js}"></script>
          {/foreach}

    {/if}

    {if isset($css_head)}

          {foreach from=$css_head item=css }
              <link rel="stylesheet" href="{base_url('assets')}/{$css}">
          {/foreach}

    {/if}

    {block name=script_css}


    {/block}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>M</b>Z</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">{$this->data['site_name']}</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- menu by priveleges begin -->
          <div class="">

             <!--<ul class="nav navbar-nav ">
                {assign var=base value=$smarty.const.ADMINS}
                {assign var=url_dashboard value="dashboard/index"}
                <li class="active"><a href="{site_url($url_dashboard)}"><span>Dashboard</span></a></li>

                {assign var=menus value=$this->data['menus']}
                {assign var=privileges value=$this->session->userdata('hak_akses')}


                {foreach from=$privileges key=k item=item}
                  {assign var=hitung_akses value=0}
                  {foreach from=$item key=item_key item=nilai_akses}
                      {$hitung_akses = $hitung_akses + $nilai_akses}
                  {/foreach}

                  {if $hitung_akses > 0}
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$menus[$k]['title']} <span class="caret"></span></a>
                      {if array_key_exists('subs', $menus[$k])}
                        <ul class="dropdown-menu" role="menu">
                          {foreach from=$menus[$k]['subs'] key=menu_key item=menu_item}
                              {if !array_key_exists('subs',$menu_item)}
                                {if $privileges[$k][$menu_key] > 0}
                                  <li><a href="{$menu_item['link']}">{$menu_item['title']}</a></li>
                                {/if}
                              {/if}
                              {if array_key_exists('subs',$menu_item)}
                                  {if count($menu_item['subs']) > 0}
                                    {if $privileges[$k][$menu_key] > 0}
                                      <li class="dropdown-submenu">
                                       <a href="{$menu_item['link']}">{$menu_item['title']} <span class="caret"></span></a>

                                        <ul class="dropdown-menu" role="menu">
                                              {foreach from=$menu_item['subs'] key=b item=sub}
                                                {if count($sub['subs']) == '0'}
                                                  {if $privileges[$k][$b] > 0}
                                                    <li><a href="{$sub['link']}">{$sub['title']}</a></li>
                                                  {/if}
                                                {/if}
                                                {if count($sub['subs']) > '0'}
                                                    {if $privileges[$k][$b] > 0}
                                                      <li class="dropdown-submenu">
                                                        <a href="{$sub['link']}">{$sub['title']} <span class="caret"></span></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            {foreach from=$sub['subs'] key=sub_2 item=sub_2_item}
                                                              {if $privileges[$k][$sub_2] > 0}
                                                                <li><a href="{$sub_2_item['link']}">{$sub_2_item['title']}</a></li>
                                                              {/if}
                                                            {/foreach}
                                                        </ul>
                                                      </li>
                                                    {/if}
                                                {/if}
                                              {/foreach}
                                        </ul>
                                      </li>
                                    {/if}
                                  {/if}
                                  {if count($menu_item['subs']) =='0'}
                                      {if $privileges[$k][$menu_key] > 0}
                                        <li><a href="{$menu_item['link']}">{$menu_item['title']}</a></li>
                                      {/if}
                                  {/if}
                              {/if}

                          {/foreach}
                        </ul>
                      {/if}

                    </li>
                  {/if}

                {/foreach}
                <li class="dropdown">
                  <a href="#" class="dropbtn" data-toggle="dropdown">Quickmenu <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    {assign var=data_shortcut value=$this->session->userdata('shortcut')}
                    {foreach from=$data_shortcut key=ka item=sa}
                      <li><a href="{$sa['link']}">{$sa['nama']}</a></li>
                    {/foreach}
                  </ul>
                </li>
              </ul>-->
          </div>
          <!-- menu by priveleges end -->

          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->

              <!-- Notifications Menu -->
              <li class="dropdown notifications-menu">
                <!-- Menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning" id="label-total-notifikasi">{$this->t_new_notif}</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header" id="total-notifikasi">Anda Memiliki {$this->t_new_notif} notifikasi</li>
                  <li>
                    <!-- Inner Menu: contains the notifications -->
                    <ul class="menu" id="list-notifikasi">
                      <li><!-- start notification -->
                        {if $this->t_new_notif > 0 }
                          {foreach from=$this->notif_admin key=k item=i}
                            {if $i['status_notif'] == 0}
                              <a href="{site_url('notifikasi/link')}/{$i['id']}">
                                {$i['title_notif']} {$i['pesan_notif']}
                              </a>
                            {/if}
                          {/foreach}
                        {/if}
                      </li><!-- end notification -->
                    </ul>
                  </li>
                  <!-- <li class="footer"><a href="{site_url('cart_order/cart_order/notifikasi')}">View all</a></li> -->
                </ul>
              </li>
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  {if $this->session->userdata('image') !=""}
                    <img src="{base_url('assets')}/img/username/{$this->session->userdata('image')}" width="160px" height="160px" class="user-image" alt="User Image">
                  {/if}
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">{$this->session->userdata('nama')|capitalize}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    {if $this->session->userdata('image') !=""}
                      <img src="{base_url('assets')}/img/username/{$this->session->userdata('image')}" class="img-circle" alt="User Image" width="160px" height="160px">
                    {/if}
                    <p>
                      {$this->session->userdata('nama')|capitalize} - {$this->session->userdata('level')|capitalize}

                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{site_url('profile/edit')}" class="btn btn-warning btn-flat">Profile</a>
                      <a href="{site_url('shortcut/fetch')}" class="btn btn-default btn-flat">Shortcut</a>
                    </div>
                    <div class="pull-right">
                      <a href="{site_url('cms/logout')}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              {if $this->session->userdata('image') !=""}
                <img src="{base_url('assets')}/img/username/{$this->session->userdata('image')}" width="160px" height="160px" class="img-circle" alt="User Image">
              {/if}
            </div>
            <div class="pull-left info">
              <p>{$this->session->userdata('nama')|capitalize}</p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- menu kiri  -->
          <ul class="sidebar-menu tree" data-widget="tree">
            <!-- Optionally, you can add icons to the links -->
            <!--<li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
            <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu" style="display: none;">
                <li><a href="#">Link in level 2</a></li>
                <li><a href="#">Link in level 2</a></li>
              </ul>
            </li>-->
            {assign var=base value=$smarty.const.ADMINS}
            {assign var=url_dashboard value="dashboard/index"}
            <li><a href="{site_url($url_dashboard)}"><span>Dashboard</span></a></li>

            {assign var=menus value=$this->data['menus']}
            {assign var=privileges value=$this->session->userdata('hak_akses')}


            {foreach from=$privileges key=k item=item}
              {assign var=hitung_akses value=0}
              {foreach from=$item key=item_key item=nilai_akses}
                  {$hitung_akses = $hitung_akses + $nilai_akses}
              {/foreach}
              {if $k!='component'}
                {if $hitung_akses > 0}
                  <li class="treeview">
                    <a href="#">{$menus[$k]['title']} <span class="caret"></span></a>
                    {if array_key_exists('subs', $menus[$k])}
                      <ul class="treeview-menu">
                        {foreach from=$menus[$k]['subs'] key=menu_key item=menu_item}
                            {if !array_key_exists('subs',$menu_item)}
                              {if $privileges[$k][$menu_key] > 0}
                                <li><a href="{$menu_item['link']}">{$menu_item['title']}</a></li>
                              {/if}
                            {/if}
                            {if array_key_exists('subs',$menu_item)}
                                {if count($menu_item['subs']) > 0}
                                  {if $privileges[$k][$menu_key] > 0}
                                    <li class="treeview">
                                     <a href="{$menu_item['link']}">{$menu_item['title']} <span class="caret"></span></a>

                                      <ul class="treeview-menu">
                                            {foreach from=$menu_item['subs'] key=b item=sub}
                                              {if count($sub['subs']) == '0'}
                                                {if $privileges[$k][$b] > 0}
                                                  <li><a href="{$sub['link']}">{$sub['title']}</a></li>
                                                {/if}
                                              {/if}
                                              {if count($sub['subs']) > '0'}
                                                  {if $privileges[$k][$b] > 0}
                                                    <li class="treeview">
                                                      <a href="{$sub['link']}">{$sub['title']} <span class="caret"></span></a>
                                                      <ul class="treeview-menu" role="menu">
                                                          {foreach from=$sub['subs'] key=sub_2 item=sub_2_item}
                                                            {if $privileges[$k][$sub_2] > 0}
                                                              <li><a href="{$sub_2_item['link']}">{$sub_2_item['title']}</a></li>
                                                            {/if}
                                                          {/foreach}
                                                      </ul>
                                                    </li>
                                                  {/if}
                                              {/if}
                                            {/foreach}
                                      </ul>
                                    </li>
                                  {/if}
                                {/if}
                                {if count($menu_item['subs']) =='0'}
                                    {if $privileges[$k][$menu_key] > 0}
                                      <li><a href="{$menu_item['link']}">{$menu_item['title']}</a></li>
                                    {/if}
                                {/if}
                            {/if}

                        {/foreach}
                      </ul>
                    {/if}

                  </li>
                {/if}
              {/if}
              {if $k == 'component'}
                {foreach from=$menus['component']['subs'] key=mk item=mi}
                  {if $privileges['component'][$mk] > 0}
                    <li class="treeview">
                      <a href="{$mi['link']}">{$mi['title']}<span class="caret"></span>
                      </a>
                      {if array_key_exists('subs',$mi)}
                        <ul class="treeview-menu" style="display: none;">
                          {foreach from=$mi['subs'] key=mk2 item=mi2}
                            {if $privileges['component'][$mk2] > 0}
                              <!-- bila menu sudah tidak mepunyai subs  -->
                              {if array_key_exists('subs',$mi2) && count($mi2['subs']) == 0}
                                <li><a href="{$mi2['link']}">{$mi2['title']}</a></li>
                              {/if}
                              <!-- bila menu sudah tidak mepunyai subs  -->
                              <!-- bila menu masih mepunyai subs  -->
                              {if array_key_exists('subs',$mi2) && count($mi2['subs']) > 0}
                                  <li class="treeview">
                                    <a href="{$mi2['link']}">{$mi2['title']}<span class="caret"></span>
                                    </a>
                                    <ul class="treeview-menu" style="display: none;">
                                      {foreach from=$mi2['subs'] key=mk3 item=mi3}
                                        {if $privileges['component'][$mk3] > 0}
                                          <li><a href="{$mi3['link']}">{$mi3['title']}</a></li>
                                        {/if}
                                      {/foreach}
                                    </ul>
                                  </li>
                              {/if}
                              <!-- bila menu masih mepunyai subs  -->
                            {/if}
                          {/foreach}
                        </ul>
                      {/if}
                    </li>
                  {/if}
                {/foreach}
              {/if}
            {/foreach}



          </ul>


          <!-- menu kiri -->
          <!-- Sidebar Menu -->
          {block name=sidebar-menu}{/block}
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            {$page_header}
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Your Page Content Here -->
          {block name=content}{/block}

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; {date('Y')} <a href="#">{$this->data['site_name']}</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-link"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">List Shortcut</h3>
            <ul class="control-sidebar-menu">
              {assign var=data_shortcut value=$this->session->userdata('shortcut')}
              {foreach from=$data_shortcut key=k item=s}
              <li>
                <a href="{$s['link']}">
                    <h4 class="control-sidebar-subheading">{$s['nama']}</h4>
                </a>
              </li>
              {/foreach}
            </ul><!-- /.control-sidebar-menu -->
          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>
              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Some information about this general settings option
                </p>
              </div><!-- /.form-group -->
            </form>
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="{base_url('assets')}/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{base_url('assets')}/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{base_url('assets')}/dist/js/app.min.js"></script>

    <script src="{base_url('assets')}/plugins/pnotify/pnotify.custom.min.js"></script>



    {if isset($js_footer)}

          {foreach from=$js_footer item=v }
              <script src="{base_url('assets')}/{$v}"></script>
          {/foreach}

    {/if}

    {block name=script_js}{/block}

    {if !empty($this->url_socket)}
      <script type='text/javascript' src='{$this->url_socket}/socket.io/socket.io.js'></script>
      <script>
        /*
        id="total-notifikasi"
        id="list-notifikasi"
        <li><!-- start notification -->
              <a href="#">
                <i class="fa fa-users text-aqua"></i> 5 new members joined today
              </a>
            </li>
         */
          // socket
          var list_notifikasi = [];
          var socket = io.connect('{$this->url_socket}');
          socket.on('initial notifikasi', function(data){
           var html = '';
           var no = 0;
           for (var i = 0; i < data.length; i++){
             no++;
             list_notifikasi.push(data[i].invoice_no+' ::  ' + data[i].pesan + ' updated by : '+data[i].user);
             $('#total-notifikasi').html('Anda Memiliki '+list_notifikasi.length+' Notifikasi');
             $('#label-total-notifikasi').html(list_notifikasi.length);
                if(no < 6){
                  html +='<li>';
                    html +=data[i].invoice_no+' ::  ' + data[i].pesan;
                  html +='</li>';
                }

           }
           $('#list-notifikasi').html(html);
         });

         socket.on('respon_invoce',function(data){
           var html = '';
          //       html+='<div class="comment-text">';
          //         html+='<span class="username">';
          //           html+='<strong>'+data.invoice_no+'</strong>';
          //           html+='<span class="text-muted pull-right"></span>';
          //         html+='</span><!-- /.username -->';
          //         html+=' <br>'+data.pesan + '<br><i>updated by : '+data.user+'</i>';
          //       html+='</div>';
          //  $('#notifikasi').append(html);
           list_notifikasi.push(data.invoice_no+' ::  ' + data.pesan + ' updated by : '+data.user);

          //  list_notifikasi.splice(0,4);

           new PNotify({
            title: 'Pemberitahuan',
            text: data.invoice_no+' ::  ' + data.pesan + ' updated by : '+data.user,
            type: 'success'
           });

           $('#total-notifikasi').html('Anda Memiliki '+list_notifikasi.length+' Notifikasi');
           $('#label-total-notifikasi').html(list_notifikasi.length);
           $('#list-notifikasi').html('');
             html +='<li>';
               html +=data[i].invoice_no+' ::  ' + data[i].pesan;
             html +='</li>';
             $('#list-notifikasi').html(html);
          //  alert(data.invoice_no+' ::  ' + data.pesan + ' updated by : '+data.user);
         });
      </script>
    {/if}

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
  </body>
</html>
