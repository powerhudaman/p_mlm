{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">
            <!-- <a href="#" class="btn btn-primary" title="" ><i class="fa fa-refresh"></i></a> -->
  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3><br>
                  {if $this->session->flashdata('pesan') !=""}
                    {$this->session->flashdata('pesan')}
                  {/if}
                </div><!-- /.box-header -->
                <!-- form start -->

                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12" id="filter">
                        {assign var=aturan_form value=['method'=>'get']}
                        {form_open(site_url('log_aktivitas/fetch'),$aturan_form)}
                        <!--
                        $halaman = $this->input->get('halaman',true);
                        $jenis_operasi = $this->input->get('jenis_operasi',true);
                        $keyword = $this->input->get('keyword',true);
                        $ip_user = $this->input->get('ip_user',true);
                        $user_id = $this->input->get('user_id',true);
                        $tgl_1 = $this->input->get('tgl_1',true);
                        $tgl_2 = $this->input->get('tgl_2',true);
                       -->
                       <div class="form-group">
                           <label for="">Halaman</label>
                           <select name="halaman" id="halaman" class="form-control">
                               <option value=""></option>
                             {foreach from=$list_halaman key=k item=halaman}
                               <option value="{$halaman}">{$halaman}</option>
                             {/foreach}
                           </select>
                       </div>
                       <div class="form-group">
                           <label for="">Jenis Operasi</label>
                           <select name="jenis_operasi" id="jenis_operasi" class="form-control">
                               <option value=""></option>
                             {foreach from=$list_operasi key=k item=op}
                               <option value="{$op}">{$op}</option>
                             {/foreach}
                           </select>
                       </div>
                       <div class="form-group">
                            <label for="">Keyword</label>
                            <input type="text" name="keyword" id="keyword" class="form-control">
                        </div>
                        <div class="form-group">
                              <label for="">IP User</label>
                              <input type="text" name="ip_user" id="ip_user" class="form-control">
                        </div>
                        <div class="form-group">
                              <label for="">Operator</label>
                              <input type="text" name="user_id" id="user_id" class="form-control">
                        </div>
                        <div class="form-group">
                              <label for="">Tanggal Mulai</label>
                              <input type="text" name="tgl_1" id="tgl_1" class="form-control">
                        </div>

                        <div class="form-group">
                              <label for="">Tanggal Selesai</label>
                              <input type="text" name="tgl_2" id="tgl_2" class="form-control">
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Cari" class="btn btn-success">
                        </div>
                        {form_close()}
                      </div>
                      <!--
                      <div class="col-md-12">
                        <a href="#" class="btn btn-primary">Tampilkan Filter</a>
                      </div>
                      -->
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="table-responsive">
                          <table id="" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>Kode Log</th>
                                <th>Halaman</th>
                                <th>JOperasi</th>
                                <th>Data</th>
                                <th>IP User</th>
                                <th>Username</th>
                                <th>Tanggal</th>
                              </tr>
                            </thead>
                            <tbody>
                              {if array_key_exists('pesan',$list_logs)}
                                  <tr>
                                    <td colspan="8">Data Log Aktivitas Tidak Ditemukan</td>
                                  </tr>
                              {/if}
                              {if !array_key_exists('pesan',$list_logs)}
                              {foreach from=$list_logs key=k item=v}
                              <tr>
                                <td>{$v['kode_log']}</td>
                                <td>{$v['halaman']}</td>
                                <td>{$v['jenis_operasi']}</td>
                                <td>{$v['data']}</td>
                                <td>{$v['ip_user']}</td>
                                <td>{$v['username']}</td>
                                <td>{$v['tgl']}</td>
                              </tr>
                              {/foreach}
                              {/if}
                            </tbody>
                          </table>
                        </div>

                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        {$pagination}
                      </div>
                    </div>
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div>

  	</div>


{/block}

{block name=script_js}
  <script>

    $('#bara_produk').autocomplete({
      source:"{site_url('cart_catalog/produk_edit/autocomplete_bara_produk')}",
      delay: 1000
    });
    $('#produk_id').autocomplete({
      source:"{site_url('cart_catalog/produk_edit/autocomplete_produk_id')}",
      delay: 1000
    });
    $('#judul_produk').autocomplete({
      source:"{site_url('cart_catalog/produk_edit/autocomplete_judul')}",
      delay: 1000
    });
    $("#tgl_1").datepicker({ format:'yyyy-mm-dd' });
    $("#tgl_2").datepicker({ format:'yyyy-mm-dd' });

  </script>

{/block}
{block name=sidebar-menu}
   {$side_menu}
{/block}
