{extends file='index_admin_.tpl'}
{block name=content}
    <style media="screen">
      .box-menu{
        display: block;
        width:60px;
        height: 60px;
        border:1px solid black;
        margin: 5px;
      }

      .back-content {
        margin: 10px 0 0;
        background: #a9a7a7;
        border: 1px dotted red;
        display: block;
      }
    </style>

  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                  <div class="box-body">
                    <!-- Grap Registrasi -->
                    <!-- Main row -->
                    <div class="row">
                      <!-- Left col -->
                      <section class="col-lg-7 connectedSortable">
                        <!-- Custom tabs (Charts with tabs)-->
                        <div class="nav-tabs-custom">
                          <!-- Tabs within a box -->
                          <ul class="nav nav-tabs pull-right">
                            <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
                            <!-- <li><a href="#sales-chart" data-toggle="tab">Donut</a></li> -->
                            <li class="pull-left header"><i class="fa fa-inbox"></i> Registrasi</li>
                          </ul>
                          <div class="tab-content no-padding">
                            <!-- Morris chart - Sales -->
                            <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                            <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
                          </div>
                        </div>
                        <!-- /.nav-tabs-custom -->
                        <!-- div.box.box-success>div.box-header+div.box-body.border-radius-none -->
                        <!-- Bonus Sponsor  -->
                        <div class="box box-success">
                          <div class="box-header">
                            <i class="fa fa-comments-o"></i>
                            <h3 class="box-title">Bonus Sponsor</h3>
                          </div>
                          <div class="box-body border-radius-none">
                            <table class="table table-bordered table-striped">
                              <tr>
                                <td>ID Member</td>
                                <td>Nama Penerima</td>
                                <td>ID Downline</td>
                                <td>Nama Downline</td>
                                <td>Approve</td>
                              </tr>
                              {if $d_bonus_sponsor ==0}
                                <tr>
                                  <td colspan="5" class="text-center">Belum ada bonus sponsor</td>
                                </tr>
                              {/if}
                               {if $d_bonus_sponsor !=0}
                                {foreach from=$d_bonus_sponsor item=i key=k}
                                  <tr>
                                    <td>{$i['id_penerima']}</td>
                                    <td>{$i['nama_penerima']}</td>
                                    <td>{$i['kode_downline']}</td>
                                    <td>{$i['nama_downline']}</td>
                                    <td><a href="{site_url('komisi/bonus_sponsor/approve_komisi')}/{$i['id']}" class="btn btn-primary">Approve</a></td>
                                  </tr>
                                {/foreach}
                              {/if}
                            </table>
                          </div>
                        </div>
                        <!-- Bonus Sponsor  -->

                        <!-- Bonus Pasangan  -->
                        <div class="box box-success">
                          <div class="box-header">
                            <i class="fa fa-comments-o"></i>
                            <h3 class="box-title">Bonus Pasangan</h3>
                          </div>
                          <div class="box-body border-radius-none">
                            <div class="table-responsive">
                              <table class="table table-bordered table-striped">
                                
                                <tr>
                                  <td>ID Member</td>
                                  <td>Nama Penerima</td>
                                  <td>Jumlah Pasangan</td>
                                  <td>Bonus Pasangan</td>
                                  <td>Tgl Pasangan</td>
                                  <td>Approve</td>
                                </tr>
                                {if $d_bonus_pasangan ==0}
                                  <tr>
                                    <td colspan="6" class="text-center">Belum ada bonus pasangan</td>
                                  </tr>
                                {/if}
                                {if $d_bonus_pasangan !=0}
                                  
                                  {foreach from=$d_bonus_pasangan item=i key=k}
                                     <tr>
                                        <td>{$i['id_penerima']}</td>
                                        <td>{$i['penerima']}</td>
                                        <td>{$i['jumlah_pasangan']}</td>
                                        <td>{number_format($i['total_komisi'],0,",",".")}</td>
                                        <td>{$i['tgl']}</td>
                                        <td>
                                          <a href="{site_url('komisi/bonus_pasangan/approve_komisi')}/{$i['id']}" class="btn btn-primary">Approve</a>
                                        </td>
                                      </tr>
                                  {/foreach}
                                  
                                {/if}
                              </table>
                            </div>

                          </div>
                        </div>
                        <!-- Bonus Pasangan  -->

                        <!-- Bonus Reward -->
                        <div class="box box-success">
                          <div class="box-header">
                            <h3 class="box-header">Bonus Reward</h3>
                          </div>
                          <div class="box-body border-radius-none">
                            <table class="table table-bordered table-striped">
                              <tr>
                                <td>ID Member</td>
                                <td>Nama Penerima</td>
                                <td>Total Downline</td>
                                <td>Reward Level</td>
                                <td>Approve</td>
                              </tr>
                              {if $d_bonus_reward == 0}
                                <tr>
                                   <td colspan="5" class="text-center">Belum ada bonus reward</td>
                                </tr>
                              {/if}
                              {if $d_bonus_reward !=0}
                              
                              {foreach from=$d_bonus_reward item=i key=k}
                                <tr>
                                  <td>{$i['id_member']}</td>
                                  <td>{$i['nama_penerima']}</td>
                                  <td>{$i['t_pasangan']}</td>
                                  <td>{$i['level']}</td>
                                  <td><a href="{site_url('komisi/bonus_reward/approve_komisi')}/{$i['id']}" class="btn btn-primary">Approve</a></td>
                                </tr>
                              {/foreach}
                              
                              
                              {/if}
                            </table>
                          </div>
                        </div>
                        <!-- Bonus Reward -->

                      </section>
                      <!-- /.Left col -->
                      <!-- right col (We are only adding the ID to make the widgets sortable)-->
                      <section class="col-lg-5 connectedSortable">

                        <!-- solid sales graph -->
                        <div class="box box-solid bg-teal-gradient">
                          <div class="box-header">
                            <i class="fa fa-th"></i>

                            <h3 class="box-title">Komisi Graph</h3>

                            <!-- <div class="box-tools pull-right">
                              <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                              <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                              </button>
                            </div> -->
                          </div>
                          <div class="box-body border-radius-none">
                            <div class="chart" id="line-chart" style="height: 250px;"></div>
                          </div>
                          <!-- /.box-body -->
                          <div class="box-footer no-border">
                            <div class="row">
                              <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                <input type="text" class="knob" data-readonly="true" value="{$p_sponsor}" data-width="60" data-height="60"
                                       data-fgColor="#39CCCC">

                                <div class="knob-label">Bonus Sponsor</div>
                              </div>
                              <!-- ./col -->
                              <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                <input type="text" class="knob" data-readonly="true" value="{$p_pasangan}" data-width="60" data-height="60"
                                       data-fgColor="#39CCCC">

                                <div class="knob-label">Bonus Pasangan</div>
                              </div>
                              <!-- ./col -->
                              <div class="col-xs-4 text-center">
                                <input type="text" class="knob" data-readonly="true" value="{$p_reward}" data-width="60" data-height="60"
                                       data-fgColor="#39CCCC">

                                <div class="knob-label">Bonus Reward</div>
                              </div>
                              <!-- ./col -->
                            </div>
                            <!-- /.row -->
                          </div>
                          <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->

                      </section>
                      <!-- right col -->
                    </div>
                    <!-- /.row (main row) -->
                    <!-- Grap Registrasi -->
                  </div><!-- /.box-body -->

                  <div class="box-footer">

                  </div>

              </div><!-- /.box -->
            </div>

  	</div>

    <div class="modal fade" id="lokasi_modul" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id=""></h4>
          </div>
          <div class="modal-body">
            <!-- map layout -->
            <div class="col-md-12" style="margin-top:20px;">

                                  <!-- contoh map layout -->


                                  <div class="row" data-pg-collapsed="">
                                        <div class="col-md-12 back-content" data-posisi="pos-1" id="l-pos-1">
                                            <h3>Top Nama (pos 1)</h3>
                                        </div>
                                    </div>
                                    <div class="row" data-pg-collapsed="">
                                        <div class="col-md-9 back-content" data-posisi="pos-2" id="l-pos-2">
                                            <h3>Top Nav (pos 2)</h3>

                                        </div>
                                        <div class="col-md-3 back-content" data-posisi="pos-3" id="l-pos-3">
                                            <h3>Search (pos 3)</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 back-content" data-pg-collapsed="" data-posisi="pos-4" id="l-pos-4">
                                            <h3>Banner Slider (pos 4)</h3>

                                        </div>
                                    </div>
                                    <div class="row" data-pg-collapsed="">
                                        <div class="col-md-3 back-content" data-pg-collapsed="" data-posisi="pos-5" id="l-pos-5">
                                            <h3>Left Nav (pos 5)</h3>

                                        </div>
                                        <div class="col-md-9 back-content" data-pg-collapsed="" data-posisi="pos-6" id="l-pos-6">
                                            <h3>Main Content (pos 6)</h3>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 back-content" data-pg-collapsed="" data-posisi="pos-8" id="l-pos-8">
                                            <h3>Bottom Left Nav (pos 8)</h3>

                                        </div>
                                        <div class="col-md-9 back-content" data-pg-collapsed="" data-posisi="pos-9" id="l-pos-9">
                                            <h3>Bottom Content (pos 9)</h3>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 back-content" data-pg-collapsed="" data-posisi="pos-11" id="l-pos-11">
                                            <h3>Foot Content (pos 11)</h3>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 back-content" data-pg-collapsed="" data-posisi="pos-12" id="l-pos-12">
                                            <h3>Bottom Foot Content (pos 12)</h3>

                                        </div>
                                    </div>


                                  <!-- contoh map layout end -->
                            </div>
            <!-- map layout -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary"></button> -->
          </div>
        </div>
      </div>
    </div>

{/block}

{block name=sidebar-menu}
   <ul class="sidebar-menu">
      <!-- Optionally, you can add icons to the links -->
      <li><a href="#"><i class="fa fa-gear"></i> <span>Setting</span></a></li>
      <li><a href="{site_url('global_configuration/edit')}"><i class="fa  fa-indent"></i> <span>Global Configuration</span></a></li>
    </ul><!-- /.sidebar-menu -->
{/block}
{block name=script_js}
  <script>
     /* Morris.js Charts */
    // Sales chart
    var area = new Morris.Area({
      element: 'revenue-chart',
      resize: true,
      data:{$stat_register},
      xkey: 'y',
      ykeys: ['item1'],
      labels: ['Total'],
      lineColors: ['#a0d0e0'],
      hideHover: 'auto'
    });

    // komisi
        var line = new Morris.Line({
        element: 'line-chart',
        resize: true,
        data: {$stat_komisi_dashboard},
        xkey: 'y',
        ykeys: ['item'],
        labels: ['Total Komisi'],
        lineColors: ['#efefef'],
        lineWidth: 2,
        hideHover: 'auto',
        gridTextColor: "#fff",
        gridStrokeWidth: 0.4,
        pointSize: 4,
        pointStrokeColors: ["#efefef"],
        gridLineColor: "#efefef",
        gridTextFamily: "Open Sans",
        gridTextSize: 10
    });
  </script>
{/block}
