{extends file='index_admin_.tpl'}
{block name=content}
  	
  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">
  					{$path}
            {if is_dir($path)}
                Direktori
            {/if}
  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {form_open_multipart(site_url($url))}
                  <div class="box-body">
                    {if $this->session->flashdata('pesan') !=""}
                      {$this->session->flashdata('pesan')}
                    {/if}
                    <div class="form-group">
                      <label for="">Upload File Zip</label>
                      <input name="extensi" type="file" name="" id="">
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}
{block name=sidebar-menu}
   <ul class="sidebar-menu">
      <!-- Optionally, you can add icons to the links -->
      <li><a href="{site_url('extension/fetch_component')}"><i class="fa fa-th"></i> <span>Component</span></a></li>
      <li><a href="{site_url('extension/fetch_modul')}"><i class="fa  fa-indent"></i> <span>Module</span></a></li>
      <li><a href="{site_url('extension/fetch_plugin')}"><i class="fa fa-plug"></i> <span>Plugin</span></a></li>
      <li><a href="{site_url('extension/fetch_template')}"><i class="fa fa-plug"></i> <span>Template</span></a></li>
    </ul><!-- /.sidebar-menu -->
{/block}