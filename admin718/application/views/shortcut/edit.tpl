{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<!--<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>-->
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                {form_open_multipart(site_url($url))}

                  <div class="box-body">

                    <div class="form-group">
                        <label for="">Nama Shortcut</label>
                        <input type="text" name="nama" id="nama" class="form-control" value="{$detail['nama']}">
                        <input type="hidden" name="id" value="{$detail['id']}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="">Pilih Menu</label>
                      <select name="pilih_menu" id="pilih-menu" class="form-control">
                        <option value=""></option>
                        {foreach from=$menus_data key=k item=i}
                          {if !array_key_exists('subs',$i)}
                            <option value="{$i['value']}">{$i['nama']}</option>
                          {/if}
                          {if array_key_exists('subs',$i)}
                            <option value="{$i['value']}">{$i['nama']}</option>
                            {foreach from=$i['subs'] key=kk item=ii}
                              {if !array_key_exists('subs',$ii)}
                                <option value="{$ii['value']}">{$ii['nama']}</option>
                              {/if}
                              {if array_key_exists('subs',$ii)}
                                <option value="{$ii['value']}">{$ii['nama']}</option>
                                {foreach from=$ii['subs'] key=kkk item=iii}
                                  {if !array_key_exists('subs',$iii)}
                                    <option value="{$iii['value']}">{$iii['nama']}</option>
                                  {/if}
                                  {if array_key_exists('subs',$iii)}
                                    <option value="{$iii['value']}">{$iii['nama']}</option>
                                    {foreach from=$iii['subs'] key=kkkk item=iiii}
                                      {if !array_key_exists('subs',$iiii)}
                                        <option value="{$iiii['value']}">{$iiii['nama']}</option>
                                      {/if}

                                    {/foreach}
                                  {/if}
                                {/foreach}
                              {/if}
                            {/foreach}
                          {/if}
                        {/foreach}
                      </select>
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}
