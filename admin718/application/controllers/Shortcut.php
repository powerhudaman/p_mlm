<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shortcut extends MY_Controller {
  private $hak_akses = array(
					'users_management'=>array(
						'group_users',
						'users'
						),
					'customer_management'=>array(
						'customers',
						'group_customers'
						),
					'menu_management'=>array(
							'menu',
							'pagemanager'
						),
					'extensions'=>array(
							'installs',
							'packager',
							'comps',
							'mods',
							'plugs'
						),
					'module_manager'=>array(
							'install',
							'list_modul'
						),
					'component'=>array()
		);

  public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->load->helper('security');
		// $this->cek_hak_akses($this->privileges['users_management']['users'],'0|1|2');
		$this->load->library('datatables');
	}

  public function list_components(){
		$this->hak_akses['component'] = array();
		$q = "select id,nama,alias,link from menu where menu_type='main' and level='1' and publish ='0' ";
		$components_1 = $this->crut->list_datas($q);
		$t_1 = $this->crut->list_row($q);
		if($t_1 > 0){
			foreach($components_1 as $k => $v){
				/// cek apakah komponen memiliki sub menu
				$q_cek = "select id,nama,alias,link from menu where menu_type='main' and `level` > '1' and publish ='0' and parent='".$v['id']."'";
				$hasil_cek = $this->crut->list_datas($q_cek);
				if($hasil_cek > 0){
					$this->hak_akses['component'][] = $v['nama'];
					foreach($hasil_cek as $b => $sub){
						$this->hak_akses['component'][] = $sub['nama'];
						$q_cek = "select id,nama,alias,link from menu where menu_type='main' and `level` > '2' and publish ='0' and parent='".$sub['id']."'";
						$hasil_cek_3 = $this->crut->list_datas($q_cek);
						if($hasil_cek_3 > 0){
							foreach($hasil_cek_3 as $c => $sub_2){
							$this->hak_akses['component'][] = $sub_2['nama'];
							}
						}
					}
				}
				if($hasil_cek == 0){
					$this->hak_akses['component'][] = $v['nama'];
				}
			}
		}
	}

  public function add(){
    // $this->cek_hak_akses($this->privileges['users_management']['users'],'0|1|2');
    $this->list_components();

    $group_id = $this->session->userdata('level_id');
    $q="select * from admin_groups where delete_status ='0' and id ='".$group_id."'";

    /// hak akses menus
    $menu = $this->data['menus'];
    $privileges = $this->session->userdata('hak_akses');

    // menus yang sudah di list hak akses
    $menus = array();
    $no_menu = 0;
    foreach ($privileges as $k => $item) {
      $no_menu++;
      $hitung_akses = 0;
      foreach ($item as $ik => $nilai_akses) {
        $hitung_akses = $hitung_akses + $nilai_akses;
      }
      if($hitung_akses > 0 ){ // hitung akses begin
        // menu level 1 begin
        $menus[$no_menu] = array(
          'nama'=> $menu[$k]['title'],
          'value'=> '',
          'subs'=>array()
        );
        // menu level 1 end
        if(array_key_exists('subs',$menu[$k])){ // cek level 2 begin
            $no_menu_2 = 0;
            foreach ($menu[$k]['subs'] as $menu_key => $menu_item) { // foreach level 2 begin
              $no_menu_2++;
              if(!array_key_exists('subs',$menu_item)){ // menu level 2 begin
                if($privileges[$k][$menu_key] > 0){ // cek hak akses menu level 2 begin
                  $menus[$no_menu]['subs'][$no_menu_2] = array(
                    'nama'=> '--'.$menu_item['title'],
                    'value'=> $menu_item['link']
                  );
                } // cek hak akses menu level 2 end
              } // menu level 2 end
              if(array_key_exists('subs',$menu_item)){ // cek level 3 begin
                if(count($menu_item['subs']) > 0){ // cek data menu masih ada begin
                  if($privileges[$k][$menu_key] > 0){ // hitung akses level 3 begin
                    $menus[$no_menu]['subs'][$no_menu_2] = array(
                      'nama'=> '--'.$menu_item['title'],
                      'value'=> $menu_item['link'],
                      'subs'=>array()
                    );
                    $no_menu_3 = 0;
                    foreach ($menu_item['subs'] as $b => $item_sub) { // foreach level 3 begin
                      $no_menu_3++;
                      if(count($item_sub['subs']) == '0'){ // cek bila sudah tidak ada sub lagi begin
                        if($privileges[$k][$b] > 0){
                          $menus[$no_menu]['subs'][$no_menu_2]['subs'][$no_menu_3] = array(
                            'nama'=> '---'.$item_sub['title'],
                            'value'=> $item_sub['link']
                          );
                        }
                      }// cek bila sudah tidak ada sub lagi end
                      if(count($item_sub['subs']) > '0'){ // cek bila masih ada sub lagi begin
                        $menus[$no_menu]['subs'][$no_menu_2]['subs'][$no_menu_3] = array(
                          'nama'=> '---'.$item_sub['title'],
                          'value'=> $item_sub['link'],
                          'subs'=>array()
                        );
                        $no_menu_4 = 0;
                        foreach($item_sub['subs'] as $c => $item_sub_2){
                          $no_menu_4++;
                          if($privileges[$k][$c] > 0){
                            $menus[$no_menu]['subs'][$no_menu_2]['subs'][$no_menu_3]['subs'][$no_menu_4] = array(
                              'nama'=> '----'.$item_sub_2['title'],
                              'value'=> $item_sub_2['link']
                            );
                          }
                        }
                      }// cek bila masih ada sub lagi end
                    }// foreach level 3 end

                  }// hitung akses level 3 end
                }// cek data menu masih ada end
                if(count($menu_item['subs']) == '0'){
                  if($privileges[$k][$menu_key] > 0){
                    $menus[$no_menu]['subs'][$no_menu_2] = array(
                      'nama'=> '--'.$menu_item['title'],
                      'value'=> $menu_item['link']
                    );
                  }
                }
              } // cek level 3 end
            } // foreach level 2 end
        } // cek level 2 end
      } // hitung akses end
    }
    /*echo '<pre>';
    print_r($menus);
    echo '</pre>';
    die();*/


    $data['page_header'] ='Tambah Shortcut Menu';
    $data['url'] = 'Shortcut/save';
    $data['menus_data'] = $menus;
    $this->parser->parse("shortcut/add.tpl",$data);
  }

  public function save(){
    $input['id_admin'] = $this->session->userdata('user_id');
    $input['nama'] = $this->input->post('nama',true);
    $input['link'] = $this->input->post('pilih_menu',true);
    $input['status_delete'] = 0;
    $sukses = "Proses Tambah  Shortcut Berhasil";
    $gagal = "Proses Tambah  Shortcut Gagal";
    $response = $this->crut->insert('shortcut',$input,'Tambah Shortcut Menu',$sukses,$gagal);
    $q_link="select nama,link from shortcut where id_admin ='".$this->session->userdata('user_id')."' and status_delete ='0'";
    $data_link = $this->crut->list_datas($q_link);
    $this->session->set_userdata('shortcut',$data_link);

    $this->session->set_flashdata('pesan',pesan($response));
    redirect(site_url('shortcut/add'));
  }

  public function fetch(){
		  // $this->cek_hak_akses($this->privileges['users_management']['users'],'0|1|2');

			$data['page_header'] ='Data Shortcut';
			$data['url_add'] = 'shortcut/add';
			// $data['contents'] = 'admin_group/add.tpl';
			$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
			$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
										'plugins/datatables/dataTables.bootstrap.min.js');
			$this->parser->parse("shortcut/fetch.tpl",$data);
		}

		public function fetch_data(){
			// $this->cek_hak_akses($this->privileges['users_management']['users'],'0|1|2');
      $this->datatables->where(array('id_admin'=>$this->session->userdata('user_id'),'status_delete'=>'0'));
			$this->datatables->select('id,nama,link');
			$this->datatables->from('shortcut')
			->unset_column('id')
			->add_column('aksi',link_generator('shortcut','$1'),'id')
			;
			echo $this->datatables->generate('json');
		}

    public function edit($id){
      // $this->cek_hak_akses($this->privileges['users_management']['users'],'0|1|2');
      $this->list_components();

      $group_id = $this->session->userdata('level_id');
      $q="select * from shortcut where id_admin ='".$this->session->userdata('user_id')."' and status_delete ='0' and id='".$id."'";

      /// hak akses menus
      $menu = $this->data['menus'];
      $privileges = $this->session->userdata('hak_akses');

      // menus yang sudah di list hak akses
      $menus = array();
      $no_menu = 0;
      foreach ($privileges as $k => $item) {
        $no_menu++;
        $hitung_akses = 0;
        foreach ($item as $ik => $nilai_akses) {
          $hitung_akses = $hitung_akses + $nilai_akses;
        }
        if($hitung_akses > 0 ){ // hitung akses begin
          // menu level 1 begin
          $menus[$no_menu] = array(
            'nama'=> $menu[$k]['title'],
            'value'=> '',
            'subs'=>array()
          );
          // menu level 1 end
          if(array_key_exists('subs',$menu[$k])){ // cek level 2 begin
              $no_menu_2 = 0;
              foreach ($menu[$k]['subs'] as $menu_key => $menu_item) { // foreach level 2 begin
                $no_menu_2++;
                if(!array_key_exists('subs',$menu_item)){ // menu level 2 begin
                  if($privileges[$k][$menu_key] > 0){ // cek hak akses menu level 2 begin
                    $menus[$no_menu]['subs'][$no_menu_2] = array(
                      'nama'=> '--'.$menu_item['title'],
                      'value'=> $menu_item['link']
                    );
                  } // cek hak akses menu level 2 end
                } // menu level 2 end
                if(array_key_exists('subs',$menu_item)){ // cek level 3 begin
                  if(count($menu_item['subs']) > 0){ // cek data menu masih ada begin
                    if($privileges[$k][$menu_key] > 0){ // hitung akses level 3 begin
                      $menus[$no_menu]['subs'][$no_menu_2] = array(
                        'nama'=> '--'.$menu_item['title'],
                        'value'=> $menu_item['link'],
                        'subs'=>array()
                      );
                      $no_menu_3 = 0;
                      foreach ($menu_item['subs'] as $b => $item_sub) { // foreach level 3 begin
                        $no_menu_3++;
                        if(count($item_sub['subs']) == '0'){ // cek bila sudah tidak ada sub lagi begin
                          if($privileges[$k][$b] > 0){
                            $menus[$no_menu]['subs'][$no_menu_2]['subs'][$no_menu_3] = array(
                              'nama'=> '---'.$item_sub['title'],
                              'value'=> $item_sub['link']
                            );
                          }
                        }// cek bila sudah tidak ada sub lagi end
                        if(count($item_sub['subs']) > '0'){ // cek bila masih ada sub lagi begin
                          $menus[$no_menu]['subs'][$no_menu_2]['subs'][$no_menu_3] = array(
                            'nama'=> '---'.$item_sub['title'],
                            'value'=> $item_sub['link'],
                            'subs'=>array()
                          );
                          $no_menu_4 = 0;
                          foreach($item_sub['subs'] as $c => $item_sub_2){
                            $no_menu_4++;
                            if($privileges[$k][$c] > 0){
                              $menus[$no_menu]['subs'][$no_menu_2]['subs'][$no_menu_3]['subs'][$no_menu_4] = array(
                                'nama'=> '----'.$item_sub_2['title'],
                                'value'=> $item_sub_2['link']
                              );
                            }
                          }
                        }// cek bila masih ada sub lagi end
                      }// foreach level 3 end

                    }// hitung akses level 3 end
                  }// cek data menu masih ada end
                  if(count($menu_item['subs']) == '0'){
                    if($privileges[$k][$menu_key] > 0){
                      $menus[$no_menu]['subs'][$no_menu_2] = array(
                        'nama'=> '--'.$menu_item['title'],
                        'value'=> $menu_item['link']
                      );
                    }
                  }
                } // cek level 3 end
              } // foreach level 2 end
          } // cek level 2 end
        } // hitung akses end
      }
      /*echo '<pre>';
      print_r($menus);
      echo '</pre>';
      die();*/

      $data['detail'] = $this->crut->list_row($q);
      $data['page_header'] ='Tambah Shortcut Menu';
      $data['url'] = 'Shortcut/update';
      $data['menus_data'] = $menus;
      $this->parser->parse("shortcut/edit.tpl",$data);
    }

    public function update(){
      // $input['id_admin'] = $this->session->userdata('user_id');
      $input['nama'] = $this->input->post('nama',true);
      $input['link'] = $this->input->post('pilih_menu',true);
      // $input['status_delete'] = 0;
      $filter = array('id_admin'=>$this->session->userdata('user_id'),'id'=>$this->input->post('id'));
      $sukses = "Proses Update  Shortcut Berhasil";
      $gagal = "Proses Update  Shortcut Gagal";
      $response = $this->crut->update($filter,'shortcut',$input,'Update Shortcut Menu',$sukses,$gagal);
      $q_link="select nama,link from shortcut where id_admin ='".$this->session->userdata('user_id')."' and status_delete ='0'";
      $data_link = $this->crut->list_datas($q_link);
      $this->session->set_userdata('shortcut',$data_link);
      $this->session->set_flashdata('pesan',pesan($response));
      redirect(site_url('shortcut/fetch'));
    }

    public function delete($id){
      $input['status_delete'] = 1;
      $filter = array('id_admin'=>$this->session->userdata('user_id'),'id'=>$id);
      $sukses = "Proses Hapus  Shortcut Berhasil";
      $gagal = "Proses Hapus  Shortcut Gagal";
      $response = $this->crut->update($filter,'shortcut',$input,'Update Shortcut Menu',$sukses,$gagal);
      $q_link="select nama,link from shortcut where id_admin ='".$this->session->userdata('user_id')."' and status_delete ='0'";
      $data_link = $this->crut->list_datas($q_link);
      $this->session->set_userdata('shortcut',$data_link);
      $this->session->set_flashdata('pesan',pesan($response));
      redirect(site_url('shortcut/fetch'));
    }


}
