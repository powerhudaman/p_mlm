<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');

		$this->load->helper('security');
		// $this->cek_hak_akses($this->privileges['users_management']['users'],'1|2');
		$this->load->library('datatables');

		if(!$this->session->has_userdata('user_id')){
			redirect(site_url('cms/logout'));
		}


	}

	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');

    public function edit(){

		// $this->cek_hak_akses($this->privileges['users_management']['users'],'2');


		// $q="select * from admin_groups where delete_status ='0'";
        $id = $this->session->userdata('user_id');
		$q_username = "select * from admins where user_id='$id' and status = '2' ";

		$data['page_header'] ='Edit Profile';
		$data['url'] = 'Profile/update';
		// $data['status'] = $this->status;
		// $data['groups'] = $this->crut->list_datas($q);
		$data['edit'] = $this->crut->list_row($q_username);
		$this->parser->parse("profile/edit.tpl",$data);
	}

    public function update(){
		// $this->cek_hak_akses($this->privileges['users_management']['users'],'2');

		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash',
			array('required'=>'%s Tidak Boleh Kosong'
				)
			);

		$this->form_validation->set_rules('password', 'Password', 'trim|alpha_dash');

		$this->form_validation->set_rules('password-konfirm', 'Password Konfirm', 'trim|matches[password]',
			array(
					'matches'=>'{field} Harus Sama Dengan Input Password'
				));

		$this->form_validation->set_rules('nama_depan', 'Nama Depan', 'trim|required|alpha_numeric_spaces',
			array(
					'required'=>'%s Tidak Boleh Kosong'
			));

		$this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'trim|alpha_numeric_spaces');


		if ($this->form_validation->run() == FALSE) {
			$this->edit();
		} else {
			if(isset($_POST['password']) && !empty($_POST['password']) && !empty($_POST['password-konfirm'])){
				$input['password'] = do_hash($_POST['username'].'-'.$_POST['password'],'md5');
			}
			$input['nama_depan'] = $this->input->post('nama_depan',true);
			$input['nama_belakang'] = $this->input->post('nama_belakang',true);
			// $input['email'] = $this->input->post('email',true);

			$input['date_added'] = date('Y-m-d H:i:s');
			$sukses = "Proses Update  Profile Berhasil";
			$gagal = "Proses Update  Profile Gagal";
			// $response = $this->crut->insert('admins',$input,'Update Group User',$sukses,$gagal);
            $id = $this->session->userdata('user_id');

			$filter = array('user_id'=>$id);
			$response = $this->crut->update($filter,'admins',$input,'Proses Update User',$sukses,$gagal);

			//insert log
			$dt = array('filter'=>$filter,'input'=>$input);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'ProfileAdmin',
				'jenis_operasi'=>'Update',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log


			if(isset($_FILES['foto']) && !empty($_FILES['foto']['name'])){
				$config['upload_path'] = FCPATH.'/assets/img/username/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '1000000000000000';
				$config['file_name'] = $input['username'];
				$config['overwrite'] = true;

				// $this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ( $this->upload->do_upload('foto')){

					$data = $this->upload->data();

					$update['image'] = $data['file_name'];

					$filter = array('username'=>$input['username']);
					$this->crut->update($filter,'admins',$update,'','','');
				}

			}
			// redirect(site_url('Admingroup/fetch'));
            $this->session->set_flashdata('pesan', pesan($response));
			redirect(site_url('profile/edit'));
		}
	}

}
