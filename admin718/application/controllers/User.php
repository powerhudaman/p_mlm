<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');

		$this->load->helper('security');
		$this->cek_hak_akses($this->privileges['users_management']['users'],'1|2');
		$this->load->library('datatables');


	}

	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');

	public function add(){
		$this->cek_hak_akses($this->privileges['users_management']['users'],'2');

		$q="select * from admin_groups where delete_status ='0'";

		$data['page_header'] ='User Manajemen';
		$data['url'] = 'User/save';
		$data['status'] = $this->status;
		$data['groups'] = $this->crut->list_datas($q);
		$this->parser->parse("user/add.tpl",$data);
	}

	public function save(){
		$this->cek_hak_akses($this->privileges['users_management']['users'],'2');

		$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[admins.username]|alpha_dash',
			array('required'=>'%s Tidak Boleh Kosong',
				   'is_unique' =>'Username Sudah Digunakan Silahkan Input Username Lain'
				)
			);

		$this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_dash',
			array(
				'required'=>'%s Tidak Boleh Kosong'
				));

		$this->form_validation->set_rules('password-konfirm', 'Password Konfirm', 'trim|required|matches[password]',
			array(
					'required'=>'%s Tidak Boleh Kosong',
					'matches'=>'{field} Harus Sama Dengan Input Password'
				));

		$this->form_validation->set_rules('nama_depan', 'Nama Depan', 'trim|required|alpha_numeric_spaces',
			array(
					'required'=>'%s Tidak Boleh Kosong'
			));

		$this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'trim|alpha_numeric_spaces');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[admins.email]',
			array(
					'required'=>'%s Tidak Boleh Kosong',
					'valid_email' =>'{field} Harus di Input Dengan Email',
					'is_unique' =>'{field} Yang Anda Input Sudah Di Gunakan Silahkan Menggunakan Email Lain'
				));

		$this->form_validation->set_rules('status', 'Status User', 'trim|required|numeric',
			array(
					'required'=>'%s Harus Dipilih',
					'numeric'=>'{field} Hanya Bisa Di Input Oleh Angka'
				));
		$this->form_validation->set_rules('group', 'Group User', 'trim|required|numeric',
			array(
					'required'=>'%s Harus Dipilih',
					'numeric'=>'{field} Hanya Bisa Di Input Oleh Angka'
				));

		if ($this->form_validation->run() == FALSE) {
			$this->add();
		} else {
			$input['username'] = $this->input->post('username',true);
			$input['password'] = do_hash($_POST['username'].'-'.$_POST['password'],'md5');
			$input['nama_depan'] = $this->input->post('nama_depan',true);
			$input['nama_belakang'] = $this->input->post('nama_belakang',true);
			$input['email'] = $this->input->post('email',true);
			$input['status'] = $this->input->post('status',true);
			$input['user_group_id'] = $this->input->post('group',true);
			$input['date_added'] = date('Y-m-d H:i:s');
			$sukses = "Proses Tambah  Users Berhasil";
			$gagal = "Proses Tambah  Users Gagal";
			$response = $this->crut->insert('admins',$input,'Tambah Group User',$sukses,$gagal);

			//insert log
			$dt = array('input'=>$input);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'User',
				'jenis_operasi'=>'Insert',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log


			if(isset($_FILES['foto']) && !empty($_FILES['foto']['name'])){
				$config['upload_path'] = FCPATH.'/assets/img/username/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '100000';
				$config['file_name'] = $input['username'];
				$config['overwrite'] = true;

				// $this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ( $this->upload->do_upload('foto')){

					$data = $this->upload->data();

					$update['image'] = $data['file_name'];

					$filter = array('username'=>$input['username']);
					$this->crut->update($filter,'admins',$update,'','','');
				}

			}
			// redirect(site_url('Admingroup/fetch'));
			$this->session->set_flashdata('pesan', pesan($response));
			redirect(site_url('User/add'));
		}
	}

	public function fetch(){
		$this->cek_hak_akses($this->privileges['users_management']['users'],'1|2');

		$data['page_header'] ='Data User ';
		$data['url_admin'] = ADMINS;
		$data['url_add'] = 'User/add';

		// $data['contents'] = 'admin_group/add.tpl';

		$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
		$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
									'plugins/datatables/dataTables.bootstrap.min.js');

		$this->parser->parse("user/fetch.tpl",$data);
	}

/*
	public function fetch_data(){

			 $column_order = array('a.username','a.nama_depan','a.status','b.nama',null); //set column field database for datatable orderable keterangan
 			 $column_search = array('a.nama_depan'); //set column field database for datatable searchable just nama
 			 $order = array('a.user_id' => 'desc'); // default order

				$q = "select a.user_id, b.nama, a.username, a.nama_depan, a.status from admins as a, admin_groups as b where delete_status ='0' and a.user_group_id = b.id ";
				$i = 0;
				foreach($column_search as $item){

					if($_POST['search']['value']) // if datatable send POST for search
		            {
		                if($i == 0){
		                	$q.= " and $item like '%".$_POST['search']['value']."%' ";
		                }
		                if($i > 0 ){
		                	$q.= " or $item like '%".$_POST['search']['value']."%' ";

		                }
		            }

		            $i++;
				}

				// echo $q;
				// die();

				if(isset($_POST['order'])) // here order processing
		        {
		            $q.= " order by ".$column_order[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir']." ";
		        }
		        else if(isset($order))
		        {
		            $q.= "order by a.user_id DESC ";
		        }

				$hasil = $this->crut->list_datas($q);
				$total_data = $this->crut->list_count($q);

				if($total_data > 0){

					$json = array(
						'draw'=>"",
						'recordsTotal' => $total_data,
						'recordsFiltered' => $total_data
					);
					foreach($hasil as $k => $v){
						$json['data'][] = array(
								'username' => $v['username'],
								'nama' =>$v['nama_depan'],
								'status'=>$this->status[$v['status']],
								'hak_akses' => $v['nama'],
								'aksi' =>'<a href="'.site_url("User/edit/".$v['user_id']).'" class="btn btn-warning btn-sm" title="Edit "><i class="fa fa-pencil"></i></a>
	                          <a href="'.site_url("User/delete/".$v['user_id']).'" class="btn btn-danger btn-sm" title="Delete "><i class="fa fa-trash"></i></a>'
							);
					}
				}
				if($total_data == 0){
					$json = array(
						'draw'=>"",
						'recordsTotal' => $total_data,
						'recordsFiltered' => $total_data
					);

						$json['data'] = array(
								'username' => '',
								'nama' =>'',
								'status'=>'',
								'hak_akses' => '',
								'aksi' =>''
							);

				}
				$json = json_encode($json);
				echo $json;
	}
*/
public function fetch_data(){
	/*select a.id as id, concat(a.nama_depan,' ',a.nama_belakang) as nama, a.email, a.telephone, b.nama as nama_group, sum(c.total) as total_belanja, a.`status`
	from customer as a
	INNER JOIN customer_group as b on a.customer_group_id = b.customer_group_id
	LEFT JOIN orders as c on a.id = c.customer_id
	GROUP BY a.id*/
	$this->datatables->select("a.user_id as user_id, b.nama as hak_akses, a.username, a.nama_depan as nama, a.status");
	$this->datatables->from('admins a');
	$this->datatables->join('admin_groups b', 'a.user_group_id = b.id')->where('b.delete_status',0)
	->unset_column('user_id')
	// ->unset_column('a.status')
	->add_column('aksi',link_generator('user','$1'),'user_id')
	// ->add_column('status',ganti_array($this->status,'$2'),'a.status')
	;
	echo $this->datatables->generate('json');
}

	public function edit($id){

		$this->cek_hak_akses($this->privileges['users_management']['users'],'2');


		$q="select * from admin_groups where delete_status ='0'";
		$q_username = "select * from admins where user_id='$id' ";

		$data['page_header'] ='User Manajemen';
		$data['url'] = 'User/update';
		$data['status'] = $this->status;
		$data['groups'] = $this->crut->list_datas($q);
		$data['edit'] = $this->crut->list_row($q_username);
		$this->parser->parse("user/edit.tpl",$data);
	}


	public function update(){
		$this->cek_hak_akses($this->privileges['users_management']['users'],'2');

		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash',
			array('required'=>'%s Tidak Boleh Kosong'
				)
			);

		$this->form_validation->set_rules('password', 'Password', 'trim|alpha_dash');

		$this->form_validation->set_rules('password-konfirm', 'Password Konfirm', 'trim|matches[password]',
			array(
					'matches'=>'{field} Harus Sama Dengan Input Password'
				));

		$this->form_validation->set_rules('nama_depan', 'Nama Depan', 'trim|required|alpha_numeric_spaces',
			array(
					'required'=>'%s Tidak Boleh Kosong'
			));

		$this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'trim|alpha_numeric_spaces');

		/*$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[admins.email]',
			array(
					'required'=>'%s Tidak Boleh Kosong',
					'valid_email' =>'{field} Harus di Input Dengan Email',
					'is_unique' =>'{field} Yang Anda Input Sudah Di Gunakan Silahkan Menggunakan Email Lain'
				));*/

		$this->form_validation->set_rules('status', 'Status User', 'trim|required|numeric',
			array(
					'required'=>'%s Harus Dipilih',
					'numeric'=>'{field} Hanya Bisa Di Input Oleh Angka'
				));
		$this->form_validation->set_rules('group', 'Group User', 'trim|required|numeric',
			array(
					'required'=>'%s Harus Dipilih',
					'numeric'=>'{field} Hanya Bisa Di Input Oleh Angka'
				));

		if ($this->form_validation->run() == FALSE) {
			$this->edit($_POST['user_id']);
		} else {
			$input['username'] = $this->input->post('username',true);
			if(isset($_POST['password']) && !empty($_POST['password']) && !empty($_POST['password-konfirm'])){
				$input['password'] = do_hash($_POST['username'].'-'.$_POST['password'],'md5');
			}
			$input['nama_depan'] = $this->input->post('nama_depan',true);
			$input['nama_belakang'] = $this->input->post('nama_belakang',true);
			// $input['email'] = $this->input->post('email',true);
			$input['status'] = $this->input->post('status',true);
			$input['user_group_id'] = $this->input->post('group',true);
			$input['date_added'] = date('Y-m-d H:i:s');
			$sukses = "Proses Update  Users Berhasil";
			$gagal = "Proses Update  Users Gagal";
			// $response = $this->crut->insert('admins',$input,'Update Group User',$sukses,$gagal);
			$filter = array('username'=>$input['username']);
			$response = $this->crut->update($filter,'admins',$input,'Proses Update User',$sukses,$gagal);

			//insert log
			$dt = array('filter'=>$filter,'input'=>$input);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'User',
				'jenis_operasi'=>'Update',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log


			if(isset($_FILES['foto']) && !empty($_FILES['foto']['name'])){
				$config['upload_path'] = FCPATH.'/assets/img/username/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '100000';
				$config['file_name'] = $input['username'];
				$config['overwrite'] = true;

				// $this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ( $this->upload->do_upload('foto')){

					$data = $this->upload->data();

					$update['image'] = $data['file_name'];

					$filter = array('username'=>$input['username']);
					$this->crut->update($filter,'admins',$update,'','','');
				}

			}
			// redirect(site_url('Admingroup/fetch'));
			$this->session->set_flashdata('pesan', pesan($response));
			redirect(site_url('User/edit/'.$_POST['user_id']));
		}
	}

	public function delete($id){
		$this->cek_hak_akses($this->privileges['users_management']['users'],'2');


			$sukses = "Proses Delete Users Berhasil";
			$gagal = "Proses Delete Users Gagal";
			$filter = array('user_id'=>$id);
			$response = $this->crut->delete($filter,'admins','Update Group User',$sukses,$gagal);
			//insert log
			$dt = array('filter'=>$filter);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'User',
				'jenis_operasi'=>'Delete',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log
			$this->session->set_flashdata('pesan', pesan($response));
			redirect(site_url('user/fetch'));
	}

}

/* End of file User.php */
/* Location: ./application/controllers/admin718/User.php */
