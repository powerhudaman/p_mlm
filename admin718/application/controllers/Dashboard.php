<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	private $bonus_sponsor = 0;
	private $bonus_pasangan = 0;
	private $bonus_cabang= 0;
	
	public function __construct()
	{
		parent::__construct();
		$this->bonus_sponsor = $this->crut->setting('komisi_configuration','bonus_sponsor','setting')['value'];
		$this->bonus_pasangan = $this->crut->setting('komisi_configuration','bonus_pasangan','setting')['value'];
		$this->bonus_cabang = $this->crut->setting('komisi_configuration','bonus_cabang','setting')['value'];
	}

	public function index()
	{
		$data['page_header'] ='Dashboard';

		$q_register = "select DATE_FORMAT(a.created_date,'%Y-%m-%d') as tgl, count(*) as t from member_master as a where DATE_FORMAT(a.created_date,'%Y-%m') ='".date('Y-m')."' GROUP BY DATE_FORMAT(a.created_date,'%Y-%m-%d');";
		// echo $q_register;
		// { y: '2011 Q1', item1: 2666, item2: 2666 }
		$d_register = $this->crut->list_datas($q_register);
		$stat_register = array();
		if($d_register !=0){
			foreach ($d_register as $k => $v) {
				$stat_register[] = array('y'=>$v['tgl'],'item1'=>$v['t']);
			}
		}

		// komisi


		// komisi cabang
		$q_k_cabang = "select DATE_FORMAT(a.created_date,'%Y-%m-%d') as tgl, sum(a.komisi) as total_komisi from bonus_cabang as a where a.approve ='1' and DATE_FORMAT(a.created_date,'%Y-%m') ='".date('Y-m')."' GROUP BY DATE_FORMAT(a.created_date,'%Y-%m-%d');";
		$d_k_cabang = $this->crut->list_datas($q_k_cabang);
		// komisi cabang
		
		$q_k_pasangan = "select DATE_FORMAT(a.created_date,'%Y-%m-%d') as tgl, sum(a.komisi) as total_komisi from bonus_pasangan_2 as a where a.status_approve ='1' and DATE_FORMAT(a.created_date,'%Y-%m') ='".date('Y-m')."' GROUP BY DATE_FORMAT(a.created_date,'%Y-%m-%d');";
		$d_k_pasangan = $this->crut->list_datas($q_k_pasangan);
		
		
		$q_k_sponsor = "select DATE_FORMAT(a.created_date,'%Y-%m-%d') as tgl, sum(a.komisi) as total_komisi from bonus_sponsor as a where a.status_approve ='1' and DATE_FORMAT(a.created_date,'%Y-%m') ='".date('Y-m')."' GROUP BY DATE_FORMAT(a.created_date,'%Y-%m-%d');";
		$d_k_sponsor = $this->crut->list_datas($q_k_sponsor);
		
		
		$q_k_reward = "select DATE_FORMAT(a.created_date,'%Y-%m-%d') as tgl, sum(a.komisi) as total_komisi from bonus_reward as a where a.status_approve ='1' and DATE_FORMAT(a.created_date,'%Y-%m') ='".date('Y-m')."' GROUP BY DATE_FORMAT(a.created_date,'%Y-%m-%d');";
		$d_k_reward = $this->crut->list_datas($q_k_reward);

		// range komisi
		$b_awal = date('Y-m').'-1';
		$b_akhir = date('Y-m').'-31';
		
		$begin = new DateTime( $b_awal );
		$end = new DateTime( $b_akhir );
		
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		$stat_komisi = array();

		foreach ( $period as $dt => $vd ){
			$tgl = $vd->format( "Y-m-d" );
			$stat_komisi[$tgl] = 0;

			// if($d_k_cabang !=0){
			// 	foreach ($d_k_cabang as $k => $v) {
			// 		if($tgl == $v['tgl']){
			// 			$stat_komisi[$tgl] = $stat_komisi[$tgl] + $v['total_komisi'];
			// 		}
			// 	}
			// }

			if($d_k_pasangan !=0){
				foreach ($d_k_pasangan as $k => $v) {
					if($tgl == $v['tgl']){
						$stat_komisi[$tgl] = $stat_komisi[$tgl] + $v['total_komisi'];
					}
				}
			}

			if($d_k_sponsor !=0){
				foreach ($d_k_sponsor as $k => $v) {
					if($tgl == $v['tgl']){
						$stat_komisi[$tgl] = $stat_komisi[$tgl] + $v['total_komisi'];
					}
				}
			}

			if($d_k_reward !=0){
				foreach ($d_k_reward as $k => $v) {
					if($tgl == $v['tgl']){
						$stat_komisi[$tgl] = $stat_komisi[$tgl] + $v['total_komisi'];
					}
				}
			}


		}
		// range komisi
		$stat_komisi_dashboard = array();
		foreach ($stat_komisi as $key => $value) {
			$stat_komisi_dashboard[] = array('y'=>$key,'item'=>$value);
		}
		// echo '<pre>';
		// print_r($stat_komisi_dashboard);
		// echo json_encode($stat_komisi_dashboard);
		// die();
		
		// presentase komisi
		$cek_bonus_sponsor = "select count(*) total_sponsor from bonus_sponsor where  status_approve ='1' and DATE_FORMAT(created_date,'%Y-%m') ='".date('Y-m')."'  ";
		$d_bonus = $this->crut->list_row($cek_bonus_sponsor);

		$cek_bonus_pasangan = "select sum(komisi) as total_pasangan from bonus_pasangan_2 where  status_approve ='1' and DATE_FORMAT(created_date,'%Y-%m') ='".date('Y-m')."'  ";
		$d_bonus_2 = $this->crut->list_row($cek_bonus_pasangan);
		// echo $cek_bonus_pasangan;

		$cek_bonus_cabang= "select count(*) total_cabang from bonus_cabang where  approve ='1' and DATE_FORMAT(created_date,'%Y-%m') ='".date('Y-m')."'  ";
		$d_bonus_3= $this->crut->list_row($cek_bonus_cabang);

		$cek_bonus_reward = "select  sum(a.komisi) as total_komisi from bonus_reward as a where a.status_approve ='1' and DATE_FORMAT(a.created_date,'%Y-%m') ='".date('Y-m')."'";
		$d_bonus_4 = $this->crut->list_row($cek_bonus_reward);


		$bonus_pasangan = $d_bonus_2['total_pasangan'];
		$bonus_cabang= $this->bonus_cabang * $d_bonus_3['total_cabang'];
		$bonus_sponsor= $this->bonus_sponsor * $d_bonus['total_sponsor'];
		$bonus_reward = $d_bonus_4['total_komisi'];
		$total_bonus = $bonus_pasangan  + $bonus_sponsor + $bonus_reward;
		$p_pasangan = 0;
		$p_cabang = 0;
		$p_sponsor = 0;
		$p_reward = 0;
		if($total_bonus > 0){
			$p_pasangan = round((($bonus_pasangan / $total_bonus) * 100));
			$p_cabang = round((($bonus_cabang / $total_bonus) * 100));
			$p_sponsor = round((($bonus_sponsor / $total_bonus) * 100));
			$p_reward = round((($bonus_reward / $total_bonus) * 100));
		}
		
		// presentase komisi
		// komisi

		// quick view
		$q_bonus_sponsor = "SELECT    a.id AS id,
								a.`kode` AS kode,
								b.`nama_lengkap` AS nama_penerima,
								b.id_member AS id_penerima,
								b.no_rek AS no_rek,
								b.nama_bank AS nama_bank,
								b.atas_nama AS an_bank,
								a.komisi AS komisi,
								c.`id_member` AS kode_downline,
								c.`nama_lengkap` AS nama_downline,
								DATE_FORMAT(a.created_date, '%Y-%M-%d')AS tgl,
								a.`status_approve` AS status_approve,
								CASE
									WHEN a.status_approve = '1' THEN
										'Sudah Di Approve'
									WHEN a.status_approve = '0' THEN
										'Belum Di Approve'
									END AS status_terima,
								a.status_auto_save 
						FROM bonus_sponsor AS a
						INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
						INNER JOIN member_master AS c ON a.`id_downline` = c.`id_member` where a.status_approve ='0' order by a.status_approve ASC limit 10 " ;
		$d_bonus_sponsor = $this->crut->list_datas($q_bonus_sponsor);

		$q_bonus_pasangan = "SELECT	a.id as id,
							b.nama_lengkap as penerima,
							a.id_member as id_penerima,
							b.no_rek as no_rek,
							b.nama_bank as nama_bank,
							b.atas_nama as an_bank,
							DATE_FORMAT(a.tgl, '%Y-%m-%d')AS tgl,
							a.t_pasangan AS jumlah_pasangan,
							a.komisi AS total_komisi,
							CASE
								WHEN a.status_approve = '1' THEN
									'Sudah di Approve'
								WHEN a.status_approve = '0' THEN
									'Belum di Approve'
								END AS status_terima,
								a.tgl_approve,
								a.status_approve AS kode_approve
					FROM	 bonus_pasangan_2 a
					INNER JOIN  member_master AS b ON a.`id_member` = b.`id_member`
					WHERE	!isnull(a.id) and a.t_pasangan > 0 and a.status_approve ='0' order by a.status_approve ASC limit 10";
		$d_bonus_pasangan = $this->crut->list_datas($q_bonus_pasangan);


		$q_bonus_reward = "SELECT a.id AS id, a.`id_member` AS id_member, b.`nama_lengkap` AS nama_penerima, a.`t_level` AS `level`, c.`t_kiri` AS t_kiri, a.t_pasangan,
		c.`t_kanan` AS t_kanan, a.`t_reward` AS reward, DATE_FORMAT(a.`created_date`,'%Y-%m-%d') AS tgl,
		CASE
			WHEN a.status_approve = '1' THEN
				'Sudah di Approve'
			WHEN a.status_approve = '0' THEN
				'Belum di Approve'
			END AS status_terima,
			a.status_approve as status_approve
		 FROM bonus_reward AS a
		INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
		INNER JOIN bonus_pasangan_2 AS c ON a.`id_member` = c.`id_member` AND a.`t_pasangan` = c.`t_pasangan` where a.status_approve ='0' limit 10";
		$d_bonus_reward = $this->crut->list_datas($q_bonus_reward);

		// quick view
		$data['d_register'] = $d_register;
		$data['stat_register'] = json_encode($stat_register);
		$data['stat_komisi_dashboard'] = json_encode($stat_komisi_dashboard);
		$data['p_pasangan'] = $p_pasangan;
		$data['p_cabang'] = $p_cabang;
		$data['p_sponsor'] = $p_sponsor;
		$data['p_reward'] = $p_reward;
		$data['d_bonus_sponsor'] = $d_bonus_sponsor;
		$data['d_bonus_pasangan'] = $d_bonus_pasangan;
		$data['d_bonus_reward'] = $d_bonus_reward;


		$data['css_head'] = array('plugins/morris/morris.css');
		$data['js_footer'] = array('plugins/morris/morris.js',
									'plugins/raphael/raphael.min.js',
									'plugins/knob/jquery.knob.js',
									'plugins/sparkline/jquery.sparkline.js',
									'plugins/datepicker/bootstrap-datepicker.js',
									'plugins/slimScroll/jquery.slimscroll.min.js',
									'dist/js/pages/dashboard.js',
									'dist/js/demo.js'
								);


		$this->parser->parse("dashboard/dashboard.tpl",$data);

	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
