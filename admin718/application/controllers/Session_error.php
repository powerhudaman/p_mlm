<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Session_error extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->helper('security');
		$filter = array('code'=>'global_configuration','key'=>'site_name');
		$site_name = $this->crut->detail($filter,'setting');
		$this->data['site_name'] = $site_name['value'];


	}

	public function index()
	{
		$this->session->set_userdata('session_alive', 'Hi world!');
		$this->redirect();
		$data['title'] = "Mizanstore";

		$this->parser->parse("session_error.tpl",$data);


	}

	public function test(){
		 if ($this->session->userdata('session_alive')) {
			 		echo 'Session is alive.';
				} else {
					 echo 'Session is dead.';
				}
	}
	public function reset(){
		$this->session->sess_destroy(); echo 'Session reset.';
	}

	/*
session->set_userdata('session_alive', 'Hi world!'); echo 'Session initialized.'; } ``` function test() { if ($this->session->userdata('session_alive')) { echo 'Session is alive.'; } else { echo 'Session is dead.'; } } function reset() { $this->session->sess_destroy(); echo 'Session reset.'; } ``` }
	 */


	public function proses_login(){
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash',
			array('required'=>'%s Tidak Boleh Kosong',
				   'alpha_dash' =>'{field} Hanya Bisa Menggunakan {param}'
				)
			);

		$this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_dash',
			array(
				'required'=>'%s Tidak Boleh Kosong'
				));

		if ($this->form_validation->run() == FALSE) {
				$this->index();
		} else {

				$filter = array('username'=>$this->input->post('username',true),
								'password'=>do_hash($_POST['username'].'-'.$_POST['password'],'md5'),
								'status'=>'2');


				$userdata = $this->crut->detail($filter,'admins'); /// detail userdata

				if(array_key_exists('user_id', $userdata)){ /// cek jika userdata bukan 0 maka data ditemukan
					$filter_group = array('id'=>$userdata['user_group_id'],'delete_status'=>'0');

					$group_data = $this->crut->detail($filter_group,'admin_groups'); // cek group user untuk menentukan hak akses
					if(array_key_exists('hak_akses', $group_data)){
						/// set session
						$session = array('user_id'=>$userdata['user_id'],
										 'username'=>$userdata['username'],
										 'nama'=> $userdata['nama_depan'].' '.$userdata['nama_belakang'],
										 'image'=>$userdata['image'],
										 'level'=>$group_data['nama'],
										 'level_id'=>$userdata['user_group_id'],
										 'hak_akses'=> json_decode($group_data['hak_akses'],true),
										 'login'=>true
							);

						$this->session->set_userdata($session);
						// echo'<pre>';
						// print_r($_SESSION);
						redirect(site_url('dashboard/index'));
					}
					else{
						$pesan = array('pesan'=>'Maaf Username atau Password Anda Salah','status'=>false,'kode'=>2);
						$this->session->set_flashdata('pesan', pesan($pesan));
						redirect(site_url('cms/index'));
					}
				}
				else{
					$pesan = array('pesan'=>'Maaf Username atau Password Anda Salah','status'=>false,'kode'=>2);
					$this->session->set_flashdata('pesan', pesan($pesan));
					redirect(site_url('cms/index'));

				}
		}
	}

	public function logout(){
			$this->session->sess_destroy();
			redirect(site_url('cms/index'));
	}

	public function redirect(){
		if($this->session->has_userdata('username')){
			redirect(site_url('dashboard/index'));
		}
	}

}

/* End of file Cms.php */
/* Location: ./application/controllers/Cms.php */
