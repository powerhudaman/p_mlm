<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_aktivitas extends MY_Controller {

	protected $list_halaman = array(1=>'Login',2=>'Admingroup',3=>'User',4=>'Membergroup',5=>'BonusSponsor',6=>'BonusPasangan',7=>'Kupon',8=>'SettingKomisi',9=>'ProfileAdmin');
	protected $list_operasi = array(1=>'Insert',2=>'Update',3=>'Delete',4=>'Login',5=>'Logout');


	public function fetch($offset = 0){
		$this->cek_hak_akses($this->privileges['log_aktivitas']['log_aktivitas'],'1|2');
		// $offset = $this->uri->segment(4);
		$filter = "";
		$halaman = $this->input->get('halaman',true);
		$jenis_operasi = $this->input->get('jenis_operasi',true);
		$keyword = $this->input->get('keyword',true);
		$ip_user = $this->input->get('ip_user',true);
		$user_id = $this->input->get('user_id',true);
		$tgl_1 = $this->input->get('tgl_1',true);
		$tgl_2 = $this->input->get('tgl_2',true);

		if(!empty($halaman)){
			if(empty($filter)){
				$filter =" where a.halaman ='".$halaman."'";
			}else{
				$filter .=" and a.halaman ='".$halaman."'";
			}
		}

		if(!empty($jenis_operasi)){
			if(empty($filter)){
				$filter =" where a.jenis_operasi ='".$jenis_operasi."'";
			}else{
				$filter .=" and a.jenis_operasi ='".$jenis_operasi."'";
			}
		}

		if(!empty($keyword)){
			if(empty($filter)){
				$filter =" where a.data like '%".$keyword."%'";
			}else{
				$filter .=" and a.data like '%".$keyword."%'";
			}
		}

		if(!empty($ip_user)){
			if(empty($filter)){
				$filter =" where a.ip_user ='".$ip_user."'";
			}else{
				$filter .=" and a.ip_user ='".$ip_user."'";
			}
		}

		if(!empty($user_id)){
			if(empty($filter)){
				$filter =" where CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
			}else{
				$filter .=" and CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
			}
		}

		if(!empty($tgl_1) && !empty($tgl_2)){
			if(empty($filter)){
				$filter =" where date_format(a.date_created,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
			}else{
				$filter .=" and date_format(a.date_created,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
			}
		}



		 $q_log = "SELECT a.`kode_log` AS kode_log, a.`halaman` AS halaman, a.`jenis_operasi` AS jenis_operasi, a.`data` AS `data`, a.`ip_user` AS ip_user,
 DATE_FORMAT(a.`date_created`,'%d-%M-%Y') AS tgl, CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') AS username FROM log_aktivitas AS a
 INNER JOIN admins AS b ON a.`user_id` = b.`user_id` ".$filter;

 $q_log_count = "SELECT count(a.kode_log) as total FROM log_aktivitas AS a
INNER JOIN admins AS b ON a.`user_id` = b.`user_id` ".$filter;
 	// 	$d_log = $this->crut->list_data($q_log);


		// echo $q_log;
		// die();

		$this->load->library('pagination');

		$config['base_url'] = site_url('log_aktivitas/fetch');

		$config['per_page'] = 50;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$list_logs = $this->crut->list_data($q_log,$config['per_page'],$offset);

		$config['total_rows'] = $this->crut->list_row($q_log_count)['total'];

		$this->pagination->initialize($config);

		$data['page_header'] ='Data Log';
		$data['url_admin'] = ADMINS;
		$data['url_add'] = '';
		$data['list_halaman'] = $this->list_halaman;
		$data['list_operasi'] = $this->list_operasi;
		// $data['side_menu'] = $this->side_menu;
		$data['list_logs'] = $list_logs;
		$data['pagination'] = $this->pagination->create_links();



			// $data['contents'] = 'admin_group/add.tpl';


		$data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
															'plugins/select2/select2.min.css',
															'plugins/datepicker/datepicker3.css'
												);
		$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
																'plugins/select2/select2.min.js',
																'plugins/datepicker/bootstrap-datepicker.js'
												);

		$this->parser->parse("logs/fetch.tpl",$data);
		}


}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
