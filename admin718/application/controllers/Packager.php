<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packager extends MY_Controller {

	private $modul_type = array('component'=>'Component','module'=>'Module','plugin'=>'Plugin');
	private $modul_metode = array('install'=>'Install','update'=>'Update','patch'=>'Patch');

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('file_manipulation');
		$this->load->library('zip');
	}

	public function index()
	{
		$this->cek_hak_akses($this->privileges['extensions']['installs'],'2');
		$data['path'] = $parent_path = realpath(__DIR__ . '/../../../assets/temp_mod');
		// $data['path'] = $parent_path = FCPATH.'\..\..\..\assets\temp_mod';
		$data['page_header'] ='Pakager Untuk Modul';

		//inisialiasi
		$data['modul_types'] = $this->modul_type;
		$data['modul_metodes'] = $this->modul_metode;

		$data['css_head'] = array('plugins/datepicker/datepicker3.css');
		$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
									'plugins/datepicker/bootstrap-datepicker.js',
									'plugins/ckeditor/ckeditor.js'
			);

		$data['url'] = 'Packager/proses';
		$this->parser->parse("packager/packager.tpl",$data);
	}

	public function buat_folder_packaging(){
		$parent_path = realpath(__DIR__ . '/../../../assets/temp_mod');
		$json = array();

		if(is_dir($parent_path.'/'.$_POST['nama_modul'])){
    		$json = array('pesan'=>'Folder '.$_POST['nama_modul'].' sudah ada');
    	}else{
    		mkdir($parent_path.'/'.$_POST['nama_modul'], 0777);
    		mkdir($parent_path.'/'.$_POST['nama_modul'].'/file_backend', 0777);
    		mkdir($parent_path.'/'.$_POST['nama_modul'].'/file_frontend', 0777);
    		$json = array('pesan'=>'Folder '.$_POST['nama_modul'].' Berhasil Dibuat');
    	}

    	$json = json_encode($json);

    	echo $json;
	}

	public function upload_backend(){
		
		$parent_path = realpath(__DIR__ . '/../../../assets/temp_mod');

		if($_POST['filetype'] == "filename"){

			$config['upload_path'] = $parent_path.'/'.$_POST['nama_modul'].'/file_backend/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '100000';
			$config['file_name'] = url_title($_POST['filename'],'_',true);
			$config['overwrite'] = true;

			
			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload('fileitem')){
				$error = $this->upload->display_errors();
				var_dump($error);
			}
			else{
				$data = $this->upload->data();
				$json = array('pesan'=>$data['file_name'].' Sukses Di Upload','filename'=>$data['file_name']);
				$json = json_encode($json);
				echo $json;
			}

		}
		if($_POST['filetype'] == "folder"){
			$pecah = explode('/', $_POST['folderdir']);
			$sumber = FCPATH.'/'.$_POST['folderdir'];
			$tujuan = $parent_path.'/'.$_POST['nama_modul'].'/file_backend/'.end($pecah);
			copy_folder($sumber,$tujuan);
			$json = array('pesan'=>'Folder Berhasil Di Copy '.end($pecah),'foldername'=>end($pecah));
				$json = json_encode($json);
				echo $json;
		}
		
	}
	public function deload_backend(){

		$file_name = realpath(__DIR__ . '/../../../assets/temp_mod').'/'.$_POST['nama_modul'].'/file_backend/'.$_POST['fileitem'];


		if($_POST['filetype'] == "filename"){

			$json = array();
			if(unlink($file_name)){
				$json = array('pesan'=>$_POST['fileitem'].' Berhasil Di Hapus');
			}else{
				$json = array('pesan'=>$_POST['fileitem'].' Berhasil Di Hapus');
			}
			$json = json_encode($json);
			echo $json;
		}
		if($_POST['filetype'] == "folder"){
				delete_folder($file_name);
				$json = array('pesan'=>$_POST['fileitem'].' Berhasil Di Hapus');
				$json = json_encode($json);
				echo $json;
		}
	}

	public function upload_frontend(){
		
		$parent_path = realpath(__DIR__ . '/../../../assets/temp_mod');
		$root_path = realpath(__DIR__ . '/../../../');

		// var_dump($_POST);

		// echo json_encode(array('pesan'=>$root_path,'data'=>$_POST));

		if($_POST['filetype'] == "filename"){

			$config['upload_path'] = $parent_path.'/'.$_POST['nama_modul'].'/file_frontend/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '100000';
			$config['file_name'] = url_title($_POST['filename'],'_',true);
			$config['overwrite'] = true;

			
			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload('fileitem')){
				$error = $this->upload->display_errors();
				var_dump($error);
			}
			else{
				$data = $this->upload->data();
				$json = array('pesan'=>$data['file_name'].' Sukses Di Upload','filename'=>$data['file_name']);
				$json = json_encode($json);
				echo $json;
			}

		}
		if($_POST['filetype'] == "folder"){
			$pecah = explode('/', $_POST['folderdir']);
			$sumber = $root_path.'/'.$_POST['folderdir'];
			$tujuan = $parent_path.'/'.$_POST['nama_modul'].'/file_frontend/'.end($pecah);
			copy_folder($sumber,$tujuan);
			$json = array('pesan'=>'Folder Berhasil Di Copy '.end($pecah),'foldername'=>end($pecah));
				$json = json_encode($json);
				echo $json;
		}
		
	}
	public function deload_frontend(){

		$file_name = realpath(__DIR__ . '/../../../assets/temp_mod').'/'.$_POST['nama_modul'].'/file_frontend/'.$_POST['fileitem'];


		if($_POST['filetype'] == "filename"){

			$json = array();
			if(unlink($file_name)){
				$json = array('pesan'=>$_POST['fileitem'].' Berhasil Di Hapus');
			}else{
				$json = array('pesan'=>$_POST['fileitem'].' Berhasil Di Hapus');
			}
			$json = json_encode($json);
			echo $json;
		}
		if($_POST['filetype'] == "folder"){
				delete_folder($file_name);
				$json = array('pesan'=>$_POST['fileitem'].' Berhasil Di Hapus');
				$json = json_encode($json);
				echo $json;
		}
	}



	public function proses(){
		// echo '<pre>';
		// print_r($_POST);
		// echo '</pre>';

		$xml_raw ="<?xml version='1.0' encoding='utf-8'?>\n";
		$xml_raw .="<extension method='".$_POST['modul_metode']."' type='".$_POST['modul_type']."' version='".$_POST['version']."'>\n";

		// inisialiasi
		$xml_raw.="<nama>".$this->input->post('nama_modul',true)."</nama>\n";  
	    $xml_raw.="<creationDate>".$this->input->post('create_date',true)."</creationDate>\n";
	    $xml_raw.="<author>".$this->input->post('author',true)."</author>\n"; 
	    $xml_raw.="<authorEmail>".$this->input->post('author_email',true)."</authorEmail>\n";
	    $xml_raw.="<authorUrl>".$this->input->post('author_url',true)."</authorUrl>\n";
	    $xml_raw.="<copyright>".$this->input->post('copyright',true)."</copyright>\n";
	    $xml_raw.="<license>".$this->input->post('lisensi',true)."</license>\n";
	    $xml_raw.="<version>".$this->input->post('version',true)."</version>\n";
	    $xml_raw.="<description>".$this->input->post('description',true)."</description>\n";

	    // require
	     if(isset($_POST['require'])){
	    	$xml_raw.="<require>\n";
	    	  foreach($_POST['require'] as $k => $v){
    			$xml_raw.="<req type='".$v['type']."'>\n";
		            $xml_raw.="<name>".$v['nama']."</name>\n";
		        $xml_raw.="</req>\n";
	    	  }
	    	$xml_raw.="</require>\n";
	    }

	    // query files
	    if(isset($_POST['query_files'])){
	    	$xml_raw.="<query_files>\n";
	    	  foreach($_POST['query_files'] as $k => $v){
	    	  	$xml_raw.="<sql>".$v['sql']."</sql>\n";
	    	  }
	    	$xml_raw.="</query_files>\n";
	    }
	    // files backend
	    if(isset($_POST['file_backend'])){

		    $xml_raw.="<files_backend dir_origin='".$this->input->post('origin_backend',true)."' dir_destination='".$this->input->post('destination_backend',true)."'>\n";
		    	foreach($_POST['file_backend'] as $k => $v ){
		    		if($v['filetype'] == "filename"){
		    			$xml_raw.="<filename dir='".$v['destination']."'>\n";
				            $xml_raw.="<name>".$v['fileitem']."</name>\n";
				        $xml_raw.="</filename>\n";
		    		}
		    		if($v['filetype'] == "folder"){
		    			$xml_raw.="<folder dir='".$v['destination']."'>\n";
				            $xml_raw.="<name>".$v['fileitem']."</name>\n";
				        $xml_raw.="</folder>\n";
		    		}
		    	}  
		    $xml_raw.="</files_backend>\n";
	    }
	    // files frontend
	    if(isset($_POST['file_frontend'])){

		    $xml_raw.="<files_frontend dir_origin='".$this->input->post('origin_frontend',true)."' dir_destination='".$this->input->post('destination_frontend',true)."'>\n";
		    	foreach($_POST['file_frontend'] as $k => $v ){
		    		if($v['filetype'] == "filename"){
		    			$xml_raw.="<filename dir='".$v['destination']."'>\n";
				            $xml_raw.="<name>".$v['fileitem']."</name>\n";
				        $xml_raw.="</filename>\n";
		    		}
		    		if($v['filetype'] == "folder"){
		    			$xml_raw.="<folder dir='".$v['destination']."'>\n";
				            $xml_raw.="<name>".$v['fileitem']."</name>\n";
				        $xml_raw.="</folder>\n";
		    		}
		    	}  
		    $xml_raw.="</files_frontend>\n";
	    }
	    // query uninstall
	    if(isset($_POST['query_files_uninstall'])){
	    	$xml_raw.="<query_uninstall>\n";
	    	  foreach($_POST['query_files_uninstall'] as $k => $v){
	    	  	$xml_raw.="<sql>".$v['sql']."</sql>\n";
	    	  }
	    	$xml_raw.="</query_uninstall>\n";
	    }
	    // files backend uninstall
	    if(isset($_POST['files_uninstall_backend'])){

		    $xml_raw.="<files_uninstall_backend>\n";
		    	foreach($_POST['files_uninstall_backend'] as $k => $v ){
		    	
		    			$xml_raw.="<folder dir='".$v['direktori']."'>\n";
				            $xml_raw.="<name>".$v['folder']."</name>\n";
				        $xml_raw.="</folder>\n";
		    		
		    	}  
		    $xml_raw.="</files_uninstall_backend>\n";
	    }
	    // files frontend uninstall
	    if(isset($_POST['files_uninstall_frontend'])){

		    $xml_raw.="<files_uninstall_frontend>\n";
		    	foreach($_POST['files_uninstall_frontend'] as $k => $v ){
		    	
		    			$xml_raw.="<folder dir='".$v['direktori']."'>\n";
				            $xml_raw.="<name>".$v['folder']."</name>\n";
				        $xml_raw.="</folder>\n";
		    	
		    	}  
		    $xml_raw.="</files_uninstall_frontend>\n";
	    }

	    


		$xml_raw .="</extension>";
		var_dump($xml_raw);

		$file_name = realpath(__DIR__ . '/../../../assets/temp_mod').'/'.$_POST['nama_modul'].'/';
		$myfile = fopen($file_name.'/install.xml', "w") or die("Unable to open file!");
		fwrite($myfile, $xml_raw);

		$this->zip->read_dir($file_name,false); // Creates a folder called "myfolder"
		$this->zip->download($_POST['nama_modul'].'.zip');
	}

	public function coba_root(){
		$root_path = realpath(__DIR__ . '/../../../');
		echo $root_path;
	}

}

/* End of file Packager.php */
/* Location: ./application/controllers/Packager.php */