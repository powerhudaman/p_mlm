<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Extension extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('file_manipulation');
	}

	public function index()
	{
		$this->install();
	}

	public function install(){
		$this->cek_hak_akses($this->privileges['extensions']['installs'],'2');
		$data['path'] = $parent_path = realpath(__DIR__ . '/../../../assets/temp_mod');
		// $data['path'] = $parent_path = FCPATH.'\..\..\..\assets\temp_mod';
		$data['page_header'] ='Install Module';
		$data['url'] = 'Extension/do_install_2';
		$this->parser->parse("extension/install.tpl",$data);

	}

	public function do_install(){
		$this->cek_hak_akses($this->privileges['extensions']['installs'],'2');

		$config['upload_path'] = FCPATH.'/assets/temp_mod/';
		$config['allowed_types'] = 'zip';
		$config['max_size']  = '1000000';
		$confir['overwrite'] = true;
	
		
		$this->upload->initialize($config);
		
		if ($this->upload->do_upload('extensi')){
			$data =$this->upload->data();

			$zip = new ZipArchive;
            $file = $data['full_path'];
            chmod($file,0777);
            if ($zip->open($file) === TRUE) {
            		// $parent_path = realpath(__DIR__ . '/..');
                    $zip->extractTo(FCPATH.'/assets/temp_mod/'.$data['raw_name']);
                    $zip->close();

                    /// baca file xml
                    $folder_path = FCPATH.'assets/temp_mod/'.$data['raw_name'].'/';
                    $xml_installer = FCPATH.'/assets/temp_mod/'.$data['raw_name'].'/install.xml';
                    $xml_raw= simplexml_load_file($xml_installer) or die("Error: Cannot create object");
                    /*echo'<pre>';
                    print_r($xml_raw);
                    echo'</pre>';
                    echo 'ok <br>';*/

                    /// proses install modul
                    /// cek syarat kondisi untuk install module
                    if(isset($xml_raw->require)){
                    	$require = $xml_raw->require;
                    }

                    // proses query untuk kebutuhan install
                    if(isset($xml_raw->query_files)){
                    	$querys = $xml_raw->query_files;
                    	foreach($querys as $k){
                    		if(!empty($k->sql)){
                    			$file = $folder_path.$k->sql;
	                    		$myfile = fopen($file, "r") or die("Unable to open file!");
	                    					$q_s = fread($myfile,filesize($file));
	                    					$q_p = explode(";", $q_s.';');
	                    					foreach($q_p as $q_k){
												echo $q_k."<br><br>";
												if(!empty($q_k)){
	                    							$this->db->query($q_k.';');
												}
	                    					}
										fclose($myfile);
                    		}
                    	}
                    }

                    if(isset($xml_raw->files)){
                    	$files = $xml_raw->files;
                    	$path_modules = APPPATH;
                    	if(is_dir($path_modules.$xml_raw->files[0]['dir'])){
                    		echo 'is_folder';
                    	}else{
                    		mkdir($path_modules.$xml_raw->files[0]['dir'], 0777);
                    		echo $path_modules.$xml_raw->files[0]['dir'].'<br>';
                    		echo 'belum ada folder<br>';
                    	}
                    	foreach($files as $k){
                    		/// cek apakah ada file yang akan di proses
                    		if(isset($k->filename)){
                    			$filenames = $k->filename;
                    			foreach($filenames as $b){
                    				echo 'File yang akan di proses '.$b.';<br>';
                    				copy($folder_path.'/'.$b,$path_modules.$xml_raw->files[0]['dir'].'/'.$b);
                    			}
                    		}

                    		/// cek apakah ada folder yang akan di proses
                    		if(isset($k->folder)){
                    			$folders = $k->folder;
                    			foreach($folders as $c){
                    				echo 'Folder yang akan di proses '.$c.';<br>';
                    				$sumber = $folder_path.'/'.$c;
                    				$tujuan = $path_modules.$xml_raw->files[0]['dir'].'/'.$c;
                    				copy_folder($sumber,$tujuan);
                    			}
                    		}
                    	}
                    }


                    delete_folder(FCPATH.'assets/temp_mod/'.$data['raw_name']);
                    unlink(FCPATH.'assets/temp_mod/'.$data['file_name']);
                    $pesan = array('status'=>true,'pesan'=>'Proses Install '.$xml_raw[1].' '.$xml_raw->nama,'kode'=>1);
                    $this->session->set_flashdata('pesan', pesan($pesan));
                    redirect(site_url('extension/index'));

            } else {
                    echo 'failed';
                    $pesan = array('status'=>false,'pesan'=>'Terjadi Kesalahan Saat Instalasi','kode'=>2);
                    $this->session->set_flashdata('pesan', pesan($pesan));
                    redirect(site_url('extension/index'));
            }

		}
		else{
			$error = array('error' => $this->upload->display_errors());
			$pesan = array('status'=>false,'pesan'=>$error['error'],'kode'=>2);
            $this->session->set_flashdata('pesan', pesan($pesan));
            redirect(site_url('extension/index'));
		}
	}

	public function do_install_2(){

		$this->cek_hak_akses($this->privileges['extensions']['installs'],'2');

		$config['upload_path'] = FCPATH.'/assets/temp_mod/';
		$config['allowed_types'] = 'zip';
		$config['max_size']  = '1000000';
		$confir['overwrite'] = true;
	
		
		$this->upload->initialize($config);
		
		if ($this->upload->do_upload('extensi')){
			$data =$this->upload->data();

			$zip = new ZipArchive;
            $file = $data['full_path'];
            chmod($file,0777);
            if ($zip->open($file) === TRUE) {
            		// $parent_path = realpath(__DIR__ . '/..');
                    $zip->extractTo(FCPATH.'/assets/temp_mod/'.$data['raw_name']);
                    $zip->close();

                    /// baca file xml
                    $folder_path = FCPATH.'assets/temp_mod/'.$data['raw_name'].'/';
                    $xml_installer = FCPATH.'/assets/temp_mod/'.$data['raw_name'].'/install.xml';
                    $xml_raw= simplexml_load_file($xml_installer) or die("Error: Cannot create object");
                    echo'<pre>';
                    print_r($xml_raw);
                    echo'</pre>';
                    echo 'ok <br>';

                    /// proses install modul
                    /// cek syarat kondisi untuk install module
                    if(isset($xml_raw->require)){
                    	$require = $xml_raw->require;
                    	$filter ='';
                    	foreach($require as $k => $v){
                    		$filter['type'] = $v->req['type'];
                    		$filter['nama'] = $v->req->name;
                    		$cek_modul = $this->crut->list_count_where($filter,'extensions');
                    		if($cek_modul == 0){
                    			$pesan = array('status'=>false,'pesan'=>$filter['type'].' '.$filter['nama'].' Tidak Ditemukan Sehingga Proses Instalasi Dibatalkan','kode'=>2);
                    			$this->session->set_flashdata('pesan', pesan($pesan));
                    			redirect(site_url('extension/index'));
                    		}
                    	}
                    }

                    // proses query untuk kebutuhan install
                   /*if(isset($xml_raw->query_files)){
                    	$querys = $xml_raw->query_files;
                    	foreach($querys as $k){
                    		if(!empty($k->sql)){
                    			$file = $folder_path.$k->sql;
	                    		$myfile = fopen($file, "r") or die("Unable to open file!");
	                    					$q_s = fread($myfile,filesize($file));
	                    					$q_p = explode(";", $q_s.';');
	                    					foreach($q_p as $q_k){
												echo $q_k."<br><br>";
												if(!empty($q_k)){
	                    							$this->db->query($q_k.';');
												}
	                    					}
										fclose($myfile);
                    		}
                    	}
                    }*/

                     if(isset($xml_raw->files_backend)){
                    	$files = $xml_raw->files_backend;
                    	$path_modules = FCPATH;
                    	if(is_dir($path_modules.$xml_raw->files_backend[0]['dir_destination'])){
                    		echo 'is_folder<br>';
                    	}else{
                    		mkdir($path_modules.$xml_raw->files_backend[0]['dir_destination'], 0777);
                    		echo $path_modules.$xml_raw->files_backend[0]['dir_destination'].'<br>';
                    		echo 'belum ada folder<br>';
                    	}
                    	foreach($files as $k){
                    		/// cek apakah ada file yang akan di proses
                    		if(isset($k->filename)){
                    			$filenames = $k->filename;
                    			foreach($filenames as $b){
                    				// echo 'File yang akan di proses '.$b['dir'].';<br>';
                    				// var_dump($b);
                    				if($b['dir'] ==""){
                    					$sumber = $folder_path.$xml_raw->files_backend[0]['dir_origin'].'/'.$b->name;
                    					$tujuan = $path_modules.$xml_raw->files_backend[0]['dir_destination'].'/'.$b->name;
                    					copy($sumber,$tujuan);
                    					// echo $path_modules.$xml_raw->files_backend[0]['dir_destination'].'/'.$b->name.'<br>';
                    					// echo 'Dir = '.$b['dir'].'<br>';
                    				}
                    				if($b['dir'] !=""){
                    					$sumber = $folder_path.$xml_raw->files_backend[0]['dir_origin'].'/'.$b->name;
                    					$tujuan = FCPATH.'/'.$b['dir'].'/'.$b->name;
                    					copy($sumber,$tujuan);
                    				}
                    			}
                    		}

                    		/// cek apakah ada folder yang akan di proses
                    		if(isset($k->folder)){
                    			$folders = $k->folder;
                    			foreach($folders as $c){
                    				if($c['dir'] == ""){
                    					echo 'Folder yang akan di proses '.$c.';<br>';
	                    				$sumber = $folder_path.$xml_raw->files_backend[0]['dir_origin'].'/'.$c->name;
	                    				$tujuan = $path_modules.$xml_raw->files_backend[0]['dir_destination'].'/'.$c->name;
	                    				copy_folder($sumber,$tujuan);
                    				}
                    				if($c['dir'] !=""){
                    					$sumber = $folder_path.$xml_raw->files_backend[0]['dir_origin'].'/'.$c->name;
	                    				$tujuan = FCPATH.$c['dir'].'/'.$c->name;
	                    				copy_folder($sumber,$tujuan);
                    				}
                    				
                    			}
                    		}
                    	}
                    }

                    if(isset($xml_raw->files_frontend)){
                    	$files = $xml_raw->files_frontend;
                    	$path_modules = realpath(__DIR__ . '/../../../');
                    	if(is_dir($path_modules.$xml_raw->files_frontend[0]['dir_destination'])){
                    		echo 'is_folder<br>';
                    	}else{
                    		mkdir($path_modules.$xml_raw->files_frontend[0]['dir_destination'], 0777);
                    		echo $path_modules.$xml_raw->files_frontend[0]['dir_destination'].'<br>';
                    		echo 'belum ada folder<br>';
                    	}
                    	foreach($files as $k){
                    		/// cek apakah ada file yang akan di proses
                    		if(isset($k->filename)){
                    			$filenames = $k->filename;
                    			foreach($filenames as $b){
                    				// echo 'File yang akan di proses '.$b['dir'].';<br>';
                    				// var_dump($b);
                    				if($b['dir'] ==""){
                    					$sumber = $folder_path.$xml_raw->files_frontend[0]['dir_origin'].'/'.$b->name;
                    					$tujuan = $path_modules.$xml_raw->files_frontend[0]['dir_destination'].'/'.$b->name;
                    					copy($sumber,$tujuan);
                    					// echo $path_modules.$xml_raw->files_frontend[0]['dir_destination'].'/'.$b->name.'<br>';
                    					// echo 'Dir = '.$b['dir'].'<br>';
                    				}
                    				if($b['dir'] !=""){
                    					$sumber = $folder_path.$xml_raw->files_frontend[0]['dir_origin'].'/'.$b->name;
                    					$tujuan = realpath(__DIR__ . '/../../../').'/'.$b['dir'].'/'.$b->name;
                    					copy($sumber,$tujuan);
                    				}
                    			}
                    		}

                    		/// cek apakah ada folder yang akan di proses
                    		if(isset($k->folder)){
                    			$folders = $k->folder;
                    			foreach($folders as $c){
                    				if($c['dir'] == ""){
                    					echo 'Folder yang akan di proses '.$c.';<br>';
	                    				$sumber = $folder_path.$xml_raw->files_frontend[0]['dir_origin'].'/'.$c->name;
	                    				$tujuan = $path_modules.$xml_raw->files_frontend[0]['dir_destination'].'/'.$c->name;
	                    				copy_folder($sumber,$tujuan);
                    				}
                    				if($c['dir'] !=""){
                    					$sumber = $folder_path.$xml_raw->files_frontend[0]['dir_origin'].'/'.$c->name;
	                    				$tujuan = realpath(__DIR__ . '/../../../').$c['dir'].'/'.$c->name;
	                    				copy_folder($sumber,$tujuan);
                    				}
                    				
                    			}
                    		}
                    	}
                    }

                  
                    delete_folder(FCPATH.'assets/temp_mod/'.$data['raw_name']);
                    unlink(FCPATH.'assets/temp_mod/'.$data['file_name']);
                    $pesan = array('status'=>true,'pesan'=>'Proses Install '.$xml_raw[1].' '.$xml_raw->nama,'kode'=>1);
                    $this->session->set_flashdata('pesan', pesan($pesan));
                    redirect(site_url('extension/index'));

            } else {
                    echo 'failed';
                    $pesan = array('status'=>false,'pesan'=>'Terjadi Kesalahan Saat Instalasi','kode'=>2);
                    $this->session->set_flashdata('pesan', pesan($pesan));
                    redirect(site_url('extension/index'));
            }

		}
		else{
			$error = array('error' => $this->upload->display_errors());
			$pesan = array('status'=>false,'pesan'=>$error['error'],'kode'=>2);
            $this->session->set_flashdata('pesan', pesan($pesan));
            redirect(site_url('extension/index'));
		}

		

	}


	/*public function uninstall($components =""){
		$this->cek_hak_akses($this->privileges['extensions']['installs'],'2');

		$ext = $this->crut->ext($components);
		if($ext !='0'){
			$file_uninstall = APPPATH.'/modules/'.$ext['nama'].'/install.xml';
			$folder_path = APPPATH.'/modules/'.$ext['nama'].'/';
			$xml_raw = simplexml_load_file($file_uninstall);

			// proses query untuk kebutuhan uninstall
            if(isset($xml_raw->query_uninstall)){
            	$querys = $xml_raw->query_uninstall;
            	foreach($querys as $k){
            		if(!empty($k->sql)){
            			$file = $folder_path.$k->sql;
                		$myfile = fopen($file, "r") or die("Unable to open file!");
                					$q_s = fread($myfile,filesize($file));
                					$q_p = explode(";", $q_s.';');
                					foreach($q_p as $q_k){
										echo $q_k."<br><br>";
										if(!empty($q_k)){
                							$this->db->query($q_k.';');
										}
                					}
								fclose($myfile);
            		}
            	}
            } // end of query uninstall

            // folder remove
            if(isset($xml_raw->files_uninstall)){

            	$files = $xml_raw->files_uninstall;
				foreach($files as $k){
            		/// cek apakah ada file yang akan di proses
            		if(isset($k->filename)){
            			$filenames = $k->filename;
            			foreach($filenames as $b){
            				if(unlink($folder_path.$b)){
            					echo 'Proses Hapus '.$folder_path.$b.' Berhasil';
            				}else{
            					echo 'Proses Hapus '.$folder_path.$b.' Gagal';

            				}
            			}
            		}

            		/// cek apakah ada folder yang akan di proses
            		if(isset($k->folder)){
            			$folders = $k->folder;
            			$folder_path_delete = APPPATH.'/modules/';
            			foreach($folders as $c){
            				if(is_dir($folder_path_delete.$c)){

            						if(delete_folder($folder_path_delete.$c)){
		            					// echo 'Proses Hapus folder '.$folder_path_delete.$c.' Berhasil';
		            				}else{
		            					// echo 'Proses Hapus folder '.$folder_path_delete.$c.' Gagal';

		            				}
            				}else{
            					// echo $folder_path_delete.$c.' Tidak Ada';
            				}
            				
            			}
            		}
            	}

			} // end of query uninstall
			$pesan = array('status'=>true,'pesan'=>'Proses Un Install '.$ext['nama'],'kode'=>1);
            $this->session->set_flashdata('pesan', pesan($pesan));
            redirect(site_url('extension/index'));
		}
		if($ext == 0){
			$pesan = array('status'=>false,'pesan'=>'Proses uninstall '.$components.' Gagal','kode'=>2);
            $this->session->set_flashdata('pesan', pesan($pesan));
            redirect(site_url('extension/index'));
		}

	}*/

	public function uninstall($components =""){
		$this->cek_hak_akses($this->privileges['extensions']['installs'],'2');

		$ext = $this->crut->ext($components);
		if($ext !='0'){
			$file_uninstall = APPPATH.'/modules/'.$ext['nama'].'/install.xml';
			$folder_path = APPPATH.'/modules/'.$ext['nama'].'/';
			$xml_raw = simplexml_load_file($file_uninstall);

			// proses query untuk kebutuhan uninstall
            if(isset($xml_raw->query_uninstall)){
            	$querys = $xml_raw->query_uninstall;
            	foreach($querys as $k){
            		if(!empty($k->sql)){
            			$file = $folder_path.$k->sql;
                		$myfile = fopen($file, "r") or die("Unable to open file!");
                					$q_s = fread($myfile,filesize($file));
                					$q_p = explode(";", $q_s.';');
                					foreach($q_p as $q_k){
										echo $q_k."<br><br>";
										if(!empty($q_k)){
                							$this->db->query($q_k.';');
										}
                					}
								fclose($myfile);
            		}
            	}
            } // end of query uninstall

            // folder remove backend
            if(isset($xml_raw->files_uninstall_backend)){

            	$files = $xml_raw->files_uninstall_backend;
				foreach($files as $k){
            		/// cek apakah ada file yang akan di proses
            		/*if(isset($k->filename)){
            			$filenames = $k->filename;
            			foreach($filenames as $b){
            				if(unlink($folder_path.$b)){
            					echo 'Proses Hapus '.$folder_path.$b.' Berhasil';
            				}else{
            					echo 'Proses Hapus '.$folder_path.$b.' Gagal';

            				}
            			}
            		}*/

            		/// cek apakah ada folder yang akan di proses
            		if(isset($k->folder)){
            			$folders = $k->folder;
            			$folder_path_delete = FCPATH.'/';
            			foreach($folders as $c){
            				if(is_dir($folder_path_delete.$c['dir'])){

            						if(delete_folder($folder_path_delete.$c['dir'].'/'.$c->name)){
		            					// echo 'Proses Hapus folder '.$folder_path_delete.$c.' Berhasil';
		            				}else{
		            					// echo 'Proses Hapus folder '.$folder_path_delete.$c.' Gagal';

		            				}
            				}else{
            					// echo $folder_path_delete.$c.' Tidak Ada';
            				}
            				
            			}
            		}
            	}

			} // end of file backend
			// folder remove frontend
            if(isset($xml_raw->files_uninstall_frontend)){

            	$files = $xml_raw->files_uninstall_frontend;
				foreach($files as $k){
            		/// cek apakah ada file yang akan di proses
            		/*if(isset($k->filename)){
            			$filenames = $k->filename;
            			foreach($filenames as $b){
            				if(unlink($folder_path.$b)){
            					echo 'Proses Hapus '.$folder_path.$b.' Berhasil';
            				}else{
            					echo 'Proses Hapus '.$folder_path.$b.' Gagal';

            				}
            			}
            		}*/

            		/// cek apakah ada folder yang akan di proses
            		if(isset($k->folder)){
            			$folders = $k->folder;
            			$folder_path_delete = realpath(__DIR__ . '/../../../').'/';
            			foreach($folders as $c){
            				if(is_dir($folder_path_delete.$c['dir'])){

            						if(delete_folder($folder_path_delete.$c['dir'].'/'.$c->name)){
		            					// echo 'Proses Hapus folder '.$folder_path_delete.$c.' Berhasil';
		            				}else{
		            					// echo 'Proses Hapus folder '.$folder_path_delete.$c.' Gagal';

		            				}
            				}else{
            					// echo $folder_path_delete.$c.' Tidak Ada';
            				}
            				
            			}
            		}
            	}

			} // end of file backend
			$pesan = array('status'=>true,'pesan'=>'Proses Un Install '.$ext['nama'],'kode'=>1);
            $this->session->set_flashdata('pesan', pesan($pesan));
            redirect(site_url('extension/index'));
		}
		if($ext == 0){
			$pesan = array('status'=>false,'pesan'=>'Proses uninstall '.$components.' Gagal','kode'=>2);
            $this->session->set_flashdata('pesan', pesan($pesan));
            redirect(site_url('extension/index'));
		}

	}


	public function fetch_component(){
		$this->cek_hak_akses($this->privileges['extensions']['comps'],'1|2');

		$data['page_header'] ='Data Component Yang Terinstall';
		$data['url_admin'] = ADMINS;
		$data['url_add'] = 'Extension/install';

		// $data['contents'] = 'admin_group/add.tpl';
		
		$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
		$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
									'plugins/datatables/dataTables.bootstrap.min.js');

		$this->parser->parse("extension/fetch_component.tpl",$data);

	}

	public function fetch_data_component(){

			 $column_order = array('nama',null); //set column field database for datatable orderable keterangan
 			 $column_search = array('nama'); //set column field database for datatable searchable just nama
 			 $order = array('id' => 'desc'); // default order

				$q = "select * from extensions where type ='component' ";
				$i = 0;
				foreach($column_search as $item){

					if($_POST['search']['value']) // if datatable send POST for search
		            {
		                 
		                $q.= " and $item like '%".$_POST['search']['value']."%' ";
		            }

		            $i++;
				}

				if(isset($_POST['order'])) // here order processing
		        {
		            $q.= " order by ".$column_order[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir']." ";
		        } 
		        else if(isset($order))
		        {
		            $q.= "order by `id` DESC ";
		        }

				$hasil = $this->crut->list_datas($q);
				$total_data = $this->crut->list_count($q);
				if($total_data > 0){

						$json = array(
							'draw'=>"",
							'recordsTotal' => $total_data,
							'recordsFiltered' => $total_data
						);
						foreach($hasil as $k => $v){
							$json['data'][] = array(
									'nama' =>$v['nama'],
									'aksi' =>'<a href="'.site_url("Extension/uninstall/".$v['nama']).'" class="btn btn-danger btn-sm" title="Delete "><i class="fa fa-trash"></i></a>'
								); 
						}

				}
					

				if($total_data == 0){
					$json = array(
						'draw'=>"",
						'recordsTotal' => $total_data,
						'recordsFiltered' => $total_data
					);
					
						$json['data'] = array(
								'username' => '',
								'nama' =>'',
								'status'=>'',
								'hak_akses' => '',
								'aksi' =>''
							); 
					
				}

				$json = json_encode($json);
				echo $json;

		
	}


	public function xml_reader(){
		$this->cek_hak_akses($this->privileges['extensions']['installs'],'2');
		$data['path'] = $parent_path = realpath(__DIR__ . '/../../../assets/temp_mod');
		// $data['path'] = $parent_path = FCPATH.'\..\..\..\assets\temp_mod';
		$data['page_header'] ='XML Reader For Testing';
		$data['url'] = 'Extension/xml_view';
		$this->parser->parse("extension/xmlreader.tpl",$data);
	}


	public function xml_view(){

		$this->cek_hak_akses($this->privileges['extensions']['installs'],'2');

		$config['upload_path'] = FCPATH.'/assets/temp_mod/';
		$config['allowed_types'] = 'xml';
		$config['max_size']  = '1000000';
		$confir['overwrite'] = true;
	
		
		$this->upload->initialize($config);
		
		if ($this->upload->do_upload('extensi')){
			$data =$this->upload->data();

			$xml_installer = FCPATH.'/assets/temp_mod/'.$data['file_name'];
                    $xml_raw= simplexml_load_file($xml_installer) or die("Error: Cannot create object");
                    echo'<pre>';
                    print_r($xml_raw);
                    echo'</pre>';
                    echo 'ok <br>';

                    if(isset($xml_raw->files_backend)){
                    	$files = $xml_raw->files_backend;
                    	$path_modules = APPPATH;
                    	if(is_dir($path_modules.$xml_raw->files_backend[0]['dir'])){
                    		echo 'is_folder';
                    	}else{
                    		mkdir($path_modules.$xml_raw->files_backend[0]['dir'], 0777);
                    		echo $path_modules.$xml_raw->files_backend[0]['dir'].'<br>';
                    		echo 'belum ada folder<br>';
                    	}
                    	foreach($files as $k){
                    		/// cek apakah ada file yang akan di proses
                    		if(isset($k->filename)){
                    			$filenames = $k->filename;
                    			foreach($filenames as $b){
                    				echo 'File yang akan di proses '.$b.';<br>';
                    				// copy($folder_path.'/'.$b,$path_modules.$xml_raw->files[0]['dir'].'/'.$b);
                    			}
                    		}

                    		/// cek apakah ada folder yang akan di proses
                    		if(isset($k->folder)){
                    			$folders = $k->folder;
                    			foreach($folders as $c){
                    				echo 'Folder yang akan di proses '.$c.';<br>';
                    				// $sumber = $folder_path.'/'.$c;
                    				// $tujuan = $path_modules.$xml_raw->files[0]['dir'].'/'.$c;
                    				// copy_folder($sumber,$tujuan);
                    			}
                    		}
                    	}
                    }

                    if(isset($xml_raw->bookstore)){
                    	$books = $xml_raw->bookstore;
                    	foreach($books->book as $k => $v){
                    		echo $v['category'].' - '.$v->title['lang'].'<br>';
                    	}
                    }
		}

	}

}

/* End of file Extension.php */
/* Location: ./application/controllers/admin718/Extension.php */