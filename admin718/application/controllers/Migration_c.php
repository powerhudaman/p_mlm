<?php

class Migration extends CI_Controller
{

        public function index()
        {
                $this->load->library('migration');

                if ($this->migration->current() === FALSE)
                {
                        show_error($this->migration->error_string());
                }
        }

        public function coba(){
          $this->load->library('migration');
          $list = $this->migration->find_migrations();

          var_dump($list);
        }

        public function version($v){
          $this->load->library('migration');
          // $list = $this->migration->version($v);
          if ($this->migration->version($v) === FALSE)
          {
                  show_error($this->migration->error_string());
          }

          // var_dump($list);
        }

        public function migrasi_number($v){
          $this->load->library('migration');
          $no = $this->migration->_get_version();
          echo $no;
        }


}
