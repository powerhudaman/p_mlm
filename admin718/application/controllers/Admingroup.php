<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admingroup extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->cek_hak_akses($this->privileges['users_management']['group_users'],'1|2');
	}

	private $hak_akses = array(
					'users_management'=>array(
						'group_users',
						'users'
						),
					'customer_management'=>array(
						'customers',
						'group_customers'
						),
					'menu_management'=>array(
							'menu',
							'pagemanager'
						),
						'log_aktivitas'=>array(
								'log_aktivitas'
							),
					'extensions'=>array(
							'installs',
							'packager',
							'comps',
							'mods',
							'plugs'
						),
					'module_manager'=>array(
							'install',
							'list_modul'
						),
					'component'=>array()
		);

	public function list_components(){
		$this->hak_akses['component'] = array();
		$q = "select id,nama,alias,link from menu where menu_type='main' and level='1' and publish ='0' ";
		$components_1 = $this->crut->list_datas($q);
		$t_1 = $this->crut->list_row($q);
		if($t_1 > 0){
			foreach($components_1 as $k => $v){
				/// cek apakah komponen memiliki sub menu
				$q_cek = "select id,nama,alias,link from menu where menu_type='main' and `level` > '1' and publish ='0' and parent='".$v['id']."'";
				$hasil_cek = $this->crut->list_datas($q_cek);
				if($hasil_cek > 0){
					$this->hak_akses['component'][] = $v['nama'];
					foreach($hasil_cek as $b => $sub){
						$this->hak_akses['component'][] = $sub['nama'];
						$q_cek = "select id,nama,alias,link from menu where menu_type='main' and `level` > '2' and publish ='0' and parent='".$sub['id']."'";
						$hasil_cek_3 = $this->crut->list_datas($q_cek);
						if($hasil_cek_3 > 0){
							foreach($hasil_cek_3 as $c => $sub_2){
							$this->hak_akses['component'][] = $sub_2['nama'];
							}
						}
					}
				}
				if($hasil_cek == 0){
					$this->hak_akses['component'][] = $v['nama'];
				}
			}
		}


	}

	public function add(){

		$this->cek_hak_akses($this->privileges['users_management']['group_users'],'2');
		$this->list_components();
		$data['page_header'] ='User Group';
		// $data['contents'] = 'admin_group/add.tpl';
		$data['hak_akses'] = $this->hak_akses;
		$data['url'] = 'Admingroup/save';
		$this->parser->parse("admin_group/add.tpl",$data);
	}

	public function save(){
		$this->cek_hak_akses($this->privileges['users_management']['group_users'],'2');


		$this->form_validation->set_rules('nama', 'Nama Group', 'required|alpha_numeric_spaces',
			array('required'=>'Anda harus mengisi Field %s',
				  'alpha_numeric_spaces'=>'nama Anda Hanya Boleh Melakukan Input dengan ALphabet dan Angka'
				));
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|alpha_numeric_spaces',
			array('alpha_numeric_spaces'=>'Keterangan Hanya boleh di isi dengan ALphabet dan  Angka'));
		if ($this->form_validation->run() == FALSE) {
			$this->add();
		} else {
			$input['nama'] = $this->input->post('nama',true);
			$input['keterangan'] = $this->input->post('keterangan',true);
			$input['hak_akses'] = json_encode($this->input->post('hak_akses'));
			$input['delete_status'] = '0';
			$input['create'] = date('Y-m-d H:i:s');
			$sukses = "Proses Tambah Group Users Berhasil";
			$gagal = "Proses Tambah Group Users Gagal";
			$response = $this->crut->insert('admin_groups',$input,'Tambah Group User',$sukses,$gagal);

			//insert log
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'Admingroup',
				'jenis_operasi'=>'Insert',
				'data'=>json_encode($input),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log
			$this->session->set_flashdata('pesan',pesan($response));
			redirect(site_url('Admingroup/fetch'));

		}

	}

	public function fetch(){

		$this->cek_hak_akses($this->privileges['users_management']['group_users'],'1|2');

		$data['page_header'] ='Data User Group';
		$data['url_admin'] = ADMINS;
		$data['url_add'] = 'Admingroup/add';

		// $data['contents'] = 'admin_group/add.tpl';

		$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
		$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
									'plugins/datatables/dataTables.bootstrap.min.js');

		$this->parser->parse("admin_group/fetch.tpl",$data);
	}

	public function fetch_data(){

			 $column_order = array('nama','keterangan',null); //set column field database for datatable orderable keterangan
 			 $column_search = array('nama'); //set column field database for datatable searchable just nama
 			 $order = array('id' => 'desc'); // default order

				$q = "select * from admin_groups where delete_status ='0' ";
				$i = 0;
				foreach($column_search as $item){

					if($_POST['search']['value']) // if datatable send POST for search
		            {

		                $q.= " and $item like '%".$_POST['search']['value']."%' ";
		            }

		            $i++;
				}

				if(isset($_POST['order'])) // here order processing
		        {
		            $q.= " order by ".$column_order[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir']." ";
		        }
		        else if(isset($order))
		        {
		            $q.= "order by `id` DESC ";
		        }

				$hasil = $this->crut->list_datas($q);
				$total_data = $this->crut->list_count($q);

				$json = array(
						'draw'=>"",
						'recordsTotal' => $total_data,
						'recordsFiltered' => $total_data
					);
				foreach($hasil as $k => $v){
					$json['data'][] = array(
							'nama' =>$v['nama'],
							'keterangan' => $v['keterangan'],
							'aksi' =>'<a href="'.site_url("Admingroup/edit/".$v['id']).'" class="btn btn-warning btn-sm" title="Edit "><i class="fa fa-pencil"></i></a>
                          <a href="'.site_url("Admingroup/delete/".$v['id']).'" class="btn btn-danger btn-sm" title="Delete "><i class="fa fa-trash"></i></a>'
						);
				}

				$json = json_encode($json);
				echo $json;


	}

	public function edit($id){
		$this->cek_hak_akses($this->privileges['users_management']['group_users'],'2');

		$this->list_components();
		$q = "select * from admin_groups where delete_status ='0' and id ='$id' ";
		$data['page_header'] ='User Group Edit';
		$data['url'] = 'Admingroup/update';
		$data['edit'] = $this->crut->list_row($q);
		$data['hak_akses'] = $this->hak_akses;
		// echo'<pre>';
		// print_r($data['hak_akses']);
		// echo'</pre>';
		// die();
		$this->parser->parse("admin_group/edit.tpl",$data);
	}

	public function update(){
		$this->cek_hak_akses($this->privileges['users_management']['group_users'],'2');

		$this->form_validation->set_rules('id', 'ID Group', 'required|numeric',
			array('required'=>'Anda harus mengisi Field %s',
				  'numeric'=>'ID Anda Hanya Boleh Melakukan Input dengan Angka'
				));
		$this->form_validation->set_rules('nama', 'Nama Group', 'required|alpha_numeric_spaces',
			array('required'=>'Anda harus mengisi Field %s',
				  'alpha_numeric_spaces'=>'nama Anda Hanya Boleh Melakukan Input dengan ALphabet dan Angka'
				));
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|alpha_numeric_spaces',
			array('alpha_numeric_spaces'=>'Keterangan Hanya boleh di isi dengan ALphabet dan  Angka'));
		if ($this->form_validation->run() == FALSE) {
			$this->edit($_POST['id']);
		} else {
			$input['nama'] = $this->input->post('nama',true);
			$input['keterangan'] = $this->input->post('keterangan',true);
			$input['hak_akses'] = json_encode($this->input->post('hak_akses'));
			$input['delete_status'] = '0';
			$input['update'] = date('Y-m-d H:i:s');
			$sukses = "Proses Update Group Users Berhasil";
			$gagal = "Proses Update Group Users Gagal";
			$filter = array('id'=>$_POST['id']);
			$response = $this->crut->update($filter,'admin_groups',$input,'Update Group User',$sukses,$gagal);
			//insert log
			$dt = array('filter'=>$filter,'input'=>$input);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'Admingroup',
				'jenis_operasi'=>'Update',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log

			redirect(site_url('Admingroup/fetch'));
		}

	}

	public function delete($id){
		$this->cek_hak_akses($this->privileges['users_management']['group_users'],'2');

			$input['delete_status'] = '1';
			$input['delete'] = date('Y-m-d H:i:s');
			$sukses = "Proses Delete Group Users Berhasil";
			$gagal = "Proses Delete Group Users Gagal";
			$filter = array('id'=>$id);
			$response = $this->crut->update($filter,'admin_groups',$input,'Update Group User',$sukses,$gagal);

			//insert log
			$dt = array('filter'=>$filter,'input'=>$input);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'Admingroup',
				'jenis_operasi'=>'Delete',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log
			redirect(site_url('Admingroup/fetch'));
	}

}

/* End of file Admingroup.php */
/* Location: ./application/controllers/Admingroup.php */
