<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lib_rajaongkir {

	protected $key = '6d92efcffa971adb9b93cb7d7a69b16d'; /// key rajaongkir

	/// provinsi
	public function provinsi(){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://pro.rajaongkir.com/api/province",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10000,
		  CURLOPT_TIMEOUT => 3000,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key:".$this->key
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		$data ='';
		if ($err) {
		  $data =  "cURL Error #:" . $err;
		} else {
		  $data = $response;
		}

		return $data;
	}


	public function kota($provinsi,$kota = ''){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://pro.rajaongkir.com/api/city?province=".$provinsi."&id=".$kota,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key:".$this->key
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		$data ='';

		if ($err) {
		  $data = "cURL Error #:" . $err;
		} else {
		  $data = $response;
		}

		return $data;
	}


	public function kecamatan($kota,$kecamatan = ''){
		$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://pro.rajaongkir.com/api/subdistrict?city=".$kota."&id=".$kecamatan,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "key:".$this->key
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);
			$data ='';
			if ($err) {
			  $data = "cURL Error #:" . $err;
			} else {
			  $data = $response;
			}

			return $data;
	}


	public function international_origin($id ='',$provinsi = ''){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://pro.rajaongkir.com/api/internationalOrigin?id=$id&province=$provinsi",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key:".$this->key
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		$data = '';
		if ($err) {
		  $data = "cURL Error #:" . $err;
		} else {
		  $data = $response;
		}
		$data = json_decode($data,true);
		return $data;

	}


	public function international_destination(){
		$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://pro.rajaongkir.com/api/internationalDestination?",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "key:".$this->key
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);
			$data = '';
			if ($err) {
			  $data = "cURL Error #:" . $err;
			} else {
			  $data = $response;
			}
			$data = json_decode($data,true);
		    return $data;
	}


	public function biaya_ongkir($asal ='',$type_asal='' ,$tujuan ='',$type_tujuan='',$berat ='',$kurir=''){
			//type kota = city
			//type kecamatan = subdistrict
		$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "origin=$asal&originType=$type_asal&destination=$tujuan&destinationType=$type_tujuan&weight=$berat&courier=$kurir",
			  CURLOPT_HTTPHEADER => array(
			    "content-type: application/x-www-form-urlencoded",
			    "key:".$this->key
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);
			$data = '';

			if ($err) {
			  $data =  "cURL Error #:" . $err;
			} else {
			  $data =  $response;
			  $data = json_decode($data,true);
			  $data = json_encode($data['rajaongkir']['results']);
			}

		    return $data;

	}

	public function biaya_ongkir_detail($asal ='',$type_asal='' ,$tujuan ='',$type_tujuan='',$berat ='',$kurir=''){
			//type kota = city
			//type kecamatan = subdistrict
		$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "origin=$asal&originType=$type_asal&destination=$tujuan&destinationType=$type_tujuan&weight=$berat&courier=$kurir",
				CURLOPT_HTTPHEADER => array(
					"content-type: application/x-www-form-urlencoded",
					"key:".$this->key
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);
			$data = '';

			if ($err) {
				$data =  "cURL Error #:" . $err;
			} else {
				$data =  $response;
				$data = json_decode($data,true);
				$data = json_encode($data['rajaongkir']);
			}

				return $data;

	}

	public function kurirs(){
		$data = array();

		$data['kurirs'][] = "JNE";
		$data['kurirs'][] = "POS";
		$data['kurirs'][] = "TIKI";
		// $data['kurirs'][] = "PCP";
		$data['kurirs'][] = "ESL";
		$data['kurirs'][] = "RPX";
		// $data['kurirs'][] = "PANDU";
		$data['kurirs'][] = "WAHANA";


		return $data['kurirs'];
	}

	public function services(){
		$data = array();


		/// service kurir JNE
		$data['service_kurir']['JNE'][] = array(
			'text'  => 'JNE YES',
			'value' => 'JNE_YES'
		);
		$data['service_kurir']['JNE'][] = array(
			'text'  => 'JNE Reguler',
			'value' => 'JNE_REG'
		);
		$data['service_kurir']['JNE'][] = array(
			'text'  => 'JNE OKE',
			'value' => 'JNE_OKE'
		);
		$data['service_kurir']['JNE'][] = array(
			'text'  => 'CTC Reguler',
			'value' => 'JNE_CTC'
		);
		$data['service_kurir']['JNE'][] = array(
			'text'  => 'CTC OKE',
			'value' => 'JNE_CTCOK'
		);
		$data['service_kurir']['JNE'][] = array(
			'text'  => 'CTC YES',
			'value' => 'JNE_CTCYES'
		);
		$data['service_kurir']['JNE'][] = array(
			'text'  => 'JNE JTR',
			'value' => 'JNE_JTR'
		);
		/*$data['service_kurir']['JNE'][] = array(
			'text'  => $this->language->get('jne_jtr250'),
			'value' => 'JNE_JTR250'
		);
		$data['service_kurir']['JNE'][] = array(
			'text'  => $this->language->get('jne_jtr<150'),
			'value' => 'JNE_JTR<150'
		);
		$data['service_kurir']['JNE'][] = array(
			'text'  => $this->language->get('jne_jtr>250'),
			'value' => 'JNE_JTR>250'
		);*/

		/// service kurir TIKI
		$data['service_kurir']['TIKI'][] = array(
			'text'  => 'TIKI Economi Service',
			'value' => 'TIKI_ECO'
		);
		$data['service_kurir']['TIKI'][] = array(
			'text'  => 'TIKI Regular Service',
			'value' => 'TIKI_REG'
		);
		$data['service_kurir']['TIKI'][] = array(
			'text'  => 'TIKI One Night Service',
			'value' => 'TIKI_ONS'
		);
		$data['service_kurir']['TIKI'][] = array(
			'text'  => 'TIKI Holiday Delivery Service',
			'value' => 'TIKI_HDS'
		);
		$data['service_kurir']['TIKI'][] = array(
			'text'  => 'TIKI Same Day Service',
			'value' => 'TIKI_SDS'
		);

		// service pos
		$data['service_kurir']['POS'][] = array(
			'text'  => 'POS Biasa',
			'value' => 'POS_Paketpos Biasa'
		);
		$data['service_kurir']['POS'][] = array(
			'text'  => 'POS Paket Kilat',
			'value' => 'POS_Paket Kilat'
		);
		$data['service_kurir']['POS'][] = array(
			'text'  => 'POS Surat Kilat Khusus',
			'value' => 'POS_Surat Kilat Khusus'
		);
		$data['service_kurir']['POS'][] = array(
			'text'  => 'POS Express Next Day',
			'value' => 'POS_Express Next Day'
		);

		// service esl
		$data['service_kurir']['ESL'][] = array(
			'text'  => "ESL RPX/RDX",
			'value' => 'ESL_RPX/RDX'
		);

		// service rpx
		$data['service_kurir']['RPX'][] = array(
			'text'  => 'RPX SameDay Package',
			'value' => 'RPX_SDP'
		);
		$data['service_kurir']['RPX'][] = array(
			'text'  => 'RPX MidDay Package',
			'value' => 'RPX_MDP'
		);
		$data['service_kurir']['RPX'][] = array(
			'text'  => 'RPX Next Day Package',
			'value' => 'RPX_NDP'
		);
		$data['service_kurir']['RPX'][] = array(
			'text'  => 'RPX Regular Package',
			'value' => 'RPX_RGP'
		);
		$data['service_kurir']['RPX'][] = array(
			'text'  => 'RPX Retail Package',
			'value' => 'RPX_REP'
		);
		$data['service_kurir']['RPX'][] = array(
			'text'  => 'RPX Ecommerce Package',
			'value' => 'RPX_ERP'
		);

		// wahana
		$data['service_kurir']['WAHANA'][] = array(
			'text'  => 'WAHANA Domestic Express Service',
			'value' => 'WAHANA_DES'
		);

		return $data['service_kurir'];
	}

}
