<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	public $privileges;
	public $url_socket;

	public $notif_admin = array();
	public $t_new_notif = 0;
	public $kode_log_2 = array('1'=>'Insert','2'=>'Update','3'=>'Delete','4'=>'Approve','5'=>'Login','6'=>'Logout','7'=>'Approve Member');
	public $kode_log_type_sms = array(1=>'Registrasi Member',2=>'Bonus Sponsor',3=>'Approval Bonus Sponsor',4=>'Approval Bonus Pasangan',5=>'Approval Bonus Reward');
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('login')){
			$pesan = array('pesan'=>'Anda Belum Login Silahkan Login Dahulu','status'=>false,'kode'=>2);
			$this->session->set_flashdata('pesan', pesan($pesan));
			redirect(site_url('cms/index'));
		}

		$this->privileges = $this->session->userdata('hak_akses');


		$menus = array(
                                'users_management'=>array('title'=>'Manajemen Admin','simbol'=>'fa fa-users'),
                                'customer_management'=>array('title'=>'Customers','simbol'=>'fa fa-user'),
                                'menu_management'=>array('title'=>'Design','simbol'=>'fa fa-tasks'),
                                'extensions'=>array('title'=>'Extension','simbol'=>'fa fa-tasks'),
                                'module_manager'=>array('title'=>'Module','simbol'=>'fa fa-plus-square-o'),
                                'component'=>array('title'=>'Component','simbol'=>'fa fa-plus-square-o'),
																'log_aktivitas'=>array('title'=>'Log Aktivitas','simbol'=>'fa fa-plus-square-o')

                            );

		$menus['users_management']['subs'] = array(
                                                    'group_users'=>array('title'=>'Group Users','link'=>site_url('admingroup/fetch')),
                                                    'users'=>array('title'=>'Users','link'=>site_url('user/fetch'))
                                                );
		$menus['log_aktivitas']['subs'] = array(
                                                    'log_aktivitas'=>array('title'=>'Log Aktivitas','link'=>site_url('log_aktivitas/fetch'))
                                                );
		$menus['customer_management']['subs'] = array(
                                                    'customers'=>array('title'=>'Customers','link'=>site_url('customer/fetch')),
                                                    'group_customers'=>array('title'=>'Customer Groups','link'=>site_url('customergroup/fetch'))
                                                );
		$menus['menu_management']['subs'] = array(
                                                    'menu'=>array('title'=>'Menu Editor','link'=>site_url('menu/groupmenu/fetch')),
                                                    'pagemanager'=>array('title'=>'Page Manager','link'=>site_url('pagemanager/pagemanager/fetch'))
                                                );
		$menus['extensions']['subs'] = array(
                                                    'installs'=>array('title'=>'Install Extensi','link'=>site_url('extension/install')),
                                                    'packager'=>array('title'=>'Packager Module','link'=>site_url('packager/index')),
                                                    'comps'=>array('title'=>'Component Management','link'=>site_url('extension/fetch_component')),
                                                    'mods'=>array('title'=>'Module Management','link'=>site_url('extension/fetch_module')),
                                                    'plugs'=>array('title'=>'Plugin Management','link'=>site_url('extension/fetch_plugin')),
                                                );
		$menus['module_manager']['subs'] = array(
                                                    'install'=>array('title'=>'Install Module','link'=>site_url('module/install')),
                                                    'list_modul'=>array('title'=>'List Module','link'=>site_url('module/fetch'))
                                                );
		$menu_component = $this->Components();
		$menus['component']['subs'] = $menu_component;

		$this->data['menus'] = $menus;
		$filter = array('code'=>'global_configuration','key'=>'site_name');
		$site_name = $this->crut->detail($filter,'setting');
		$this->data['site_name'] = $site_name['value'];

		$this->url_socket = $this->crut->setting('global_configuration','url_socket','setting')['value'];
		// $this->expired_order();
		$this->notifikasi_admin();
		if(count($this->notif_admin) > 0){
						$no = 0;
						foreach ($this->notif_admin as $k => $v) {
							if($v['status_notif'] == 0){
								$no++;
							}
						}
						$this->t_new_notif = $no;
					}


	}

	public function components(){
		// $menu['subs'] = array();
		$q = "select id,nama,alias,link from menu where menu_type='main' and level='1' and publish ='0' ";
		$components_1 = $this->crut->list_datas($q);
		$t_1 = $this->crut->list_row($q);
		if($t_1 > 0){

			foreach($components_1 as $k => $v){
				$menu[$v['nama']] = array('title'=>$v['alias'],'link'=>site_url($v['link']),'subs'=>array(),'id_menu'=>$v['id']);
				/// cek apakah komponen memiliki sub menu
				$q_cek = "select id,nama,alias,link from menu where menu_type='main' and `level` > '1' and publish ='0' and parent='".$v['id']."'";
				$hasil_cek = $this->crut->list_datas($q_cek);
				if($hasil_cek > 0){
					foreach($hasil_cek as $b => $sub){
						$menu[$v['nama']]['subs'][$sub['nama']] = array('title'=>$sub['alias'],'link'=>site_url($sub['link']),'subs'=>array(),'id_menu'=>$sub['id']);
						$q_cek_3 = "select id,nama,alias,link from menu where menu_type='main' and `level` > '2' and publish ='0' and parent='".$sub['id']."'";
						$hasil_cek_3 = $this->crut->list_datas($q_cek_3);
						if($hasil_cek_3 > 0){
							foreach($hasil_cek_3 as $c => $sub_3){
								$menu[$v['nama']]['subs'][$sub['nama']]['subs'][$sub_3['nama']] = array('title'=>$sub_3['alias'],'link'=>site_url($sub_3['link']),'id_menu'=>$sub_3['id']);
							}
						}

					}

				}
			}

		}
		return $menu;

	}

	public function menu_type($menu = array()){
		$q = "select id,menu_type,nama from menu_type";
		$components_1 = $this->crut->list_datas($q);
		$t_1 = $this->crut->list_row($q);
		if($t_1 > 0){

			foreach($components_1 as $k => $v){
				/// cek apakah komponen memiliki sub menu
				$q_cek = "select id,nama,alias,link from menu where menu_type='main' and level > 1 and publish ='0' and parent='".$v['id']."'";
				$hasil_cek = $this->crut->list_row($q_cek);
				if($hasil_cek > 0){

				}
				if($hasil_cek == 0){
					$menu['subs'][$v['menu_type']] = array('title'=>$v['nama'],'link'=>site_url($v['menu_type']));
				}
			}

		}
		return $menu['subs'];
	}

	/*
	public function cek_hak_akses_global($param = array()){
		$hitung = 0;
		foreach($param as $k => $v){
			$hitung = $hitung + $v;
		}

		if($hitung == 0){
			$pesan = array('status'=>false,'pesan'=>'Anda Tidak Memilik Hak akses ',kode=>2);
			$this->session->set_flashdata('pesan', 'value');
			redirect(site_url('dashboard/index'));
		}
	}*/

	/**
	 * [cek_hak_akses fungsi ini digunakan untuk malakukan pengecekan terhadap hak akses apabila rolenya tidak sesuai maka akan di redirect ke dashboard]
	 * @param  [type] $param [kunci akses]
	 * @param  string $role  [aturan akses]
	 * @return [type]        [description]
	 */
	public function cek_hak_akses($param,$role = ''){
		$hitung = $param;
		if($hitung == 0){
			$pesan = array('status'=>false,'pesan'=>'Anda Tidak Memilik Hak akses ','kode'=>2);
			$this->session->set_flashdata('pesan', 'value');
			redirect(site_url('dashboard/index'));
		}
		if(!empty($role)){
			$pecah_roler = explode('|', $role.'|');
			foreach($pecah_roler as $k => $v){
				if(!empty($v)){
					if($hitung >= $v){
						break;
					}else{
						$pesan = array('status'=>false,'pesan'=>'Anda Tidak Memilik Hak akses ',kode=>2);
						$this->session->set_flashdata('pesan', 'value');
						redirect(site_url('dashboard/index'));
					}
				}
			}
		}
	}

	/**
	 * [side_menu_3 digunakan untuk menampilkan side menu]
	 * @param  array  $menu_list [description]
	 * @param  array  $hak_akses [description]
	 * @param  string $page      [description]
	 * @return [type]            [description]
	 */
	public function side_menu_3($menu_list =array(), $hak_akses = array(),$page =''){

		// var_dump($menu_list);
		// die();
		$html ='
		  <!-- Sidebar Menu -->
          <ul class="sidebar-menu">';
      		foreach($menu_list as $k => $menu_item){
      			if(!array_key_exists('subs', $menu_item) && array_key_exists($k, $hak_akses)){
      				if($hak_akses[$k] > 0){
      					$html .='<li><a href="'.$menu_item['link'].'"><span>'.$menu_item['title'].'</span></a></li>';
      				}
      			}
      			if(array_key_exists('subs', $menu_item) && array_key_exists($k, $hak_akses)){
      				if($hak_akses[$k] > 0 && array_key_exists($k, $hak_akses)){
	      				$html.='<li class="treeview">
			              <a href="'.$menu_item['link'].'"><span>'.$menu_item['title'].'</span> <i class="fa fa-angle-left pull-right"></i></a>
			              <ul class="treeview-menu">';
			              	foreach($menu_item['subs'] as $b => $sub){
			              		if($hak_akses[$b] > 0 && array_key_exists($b, $hak_akses)){
			              			$html.='<li><a href="'.$sub['link'].'">'.$sub['title'].'</a></li>';
			              		}
			              	}

			              $html.='</ul>
			            </li>';
		        	}
      			}
      		}


        $html.='
          </ul><!-- /.sidebar-menu -->
		';

		return $html;
	}

	private function expired_order(){
		$auto_expired = $this->crut->setting('order_configuration','auto_expired','setting')['value'];;
		if($auto_expired == 1){
			// order_status_expired
			$status_invoice_baru = $this->crut->setting('order_configuration','order_status','setting')['value'];
			$status_invoice_expired = $this->crut->setting('order_configuration','order_status_expired','setting')['value'];
			$q_order = "select a.order_id as order_id, a.invoice_no as invoice_no, concat(a.payment_firstname,' ',a.payment_lastname) as nama_pembeli,  concat(a.shipping_firstname,' ',a.shipping_lastname) as nama_penerima, a.email as email, a.date_added as tgl, a.total as tagihan, b.nama as `status`, b.warna as warna, a.date_expired as tgl_expired from orders as a, order_status as b where a.order_status_id = b.id and a.order_status_id ='".$status_invoice_baru."'  and DATEDIFF(a.date_expired,now()) < 1";


			$d_order = $this->crut->list_datas($q_order);
			if($d_order !=0){
				foreach ($d_order as $k => $v) {
					$update['order_status_id'] = $status_invoice_expired;

					$filter = array('order_id'=>$v['order_id'],'invoice_no'=>$v['invoice_no']);
					$response = $this->crut->update($filter,'orders',$update,'','','');
					if($response['status'] && $response['kode'] == '1'){
						// input ke order history
						$history['order_id'] = $v['order_id'];
						$history['user_id'] = 'auto expired';
						$history['order_status_id'] = $status_invoice_expired;
						$history['notify'] = 0;
						$history['comment'] = 'proses auto expired';
						$history['date_added'] = date('Y-m-d H:i:s');
						$this->crut->insert('order_history',$history,'Input Data History Invoice','','');
					}
				}
			}
		}

	}

	protected function notifikasi_admin(){
			$type_notif = array('0'=>'Bonus Sponsor','1'=>'Bonus Pasangan','2'=>'Bonus Cabang','3'=>'Bonus Reward');
			$gambar_notif = array('0'=>base_url().'assets/img/icon_image/ic-bonus-sponsor.png',
							'1'=>base_url().'assets/img/icon_image/ic-bonus-pasangan.png',
							'2'=>base_url().'assets/img/icon_image/ic-bonus-cabang.png',
							'3'=>base_url().'assets/img/icon_image/ic-bonus-reward.png');
			$id_member = $this->session->userdata('login_member')['id_member'];
			$q_notif = "SELECT * FROM notifikasi_admin  ORDER BY created_date DESC ";
			// echo $q_notif;
			$d_notif = $this->crut->list_datas($q_notif);
			if($d_notif !=0){
				foreach ($d_notif as $k => $v) {
					$this->notif_admin[$k] = $v;
					$this->notif_admin[$k]['title_notif'] = $type_notif[$v['type_notif']];
					$this->notif_admin[$k]['gambar_notif'] = $gambar_notif[$v['type_notif']];
				}
				// $this->notif_member = $d_notif;
			}
		}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
