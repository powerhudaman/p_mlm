<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagemanager extends MY_Controller {

	protected $posisi = array(
			'pos-1'=>'top-name',
			'pos-2'=>'top-nav',
			'pos-3'=>'search',
			'pos-4'=>'Banner',
			'pos-5'=>'left-nav',
			'pos-6'=>'main-content',
			'pos-7'=>'right-nav',
			'pos-8'=>'bot-left-nav',
			'pos-9'=>'bot-content',
			'pos-10'=>'bot-right-nav',
			'pos-11'=>'foot-content',
			'pos-12'=>'bot-foot-content',
		);

		protected $type_grid = array('col-md-1'=>'col-1','col-md-2'=>'col-2','col-md-3'=>'col-3','col-md-4'=>'col-4','col-md-5'=>'col-5','col-md-6'=>'col-6','col-md-7'=>'col-7','col-md-8'=>'col-8','col-md-9'=>'col-9','col-md-10'=>'col-10','col-md-11'=>'col-11','col-md-12'=>'col-12');

	public function index()
	{

	}

	public function add(){
		$this->cek_hak_akses($this->privileges['menu_management']['pagemanager'],'2');
		$q_komponen = "select * from extensions where type='component'";
		$q_modul = "select * from extensions where type='module'";
		$data['page_header'] ='Tambah Halaman Baru';
		$data['list_component'] = $this->crut->list_datas($q_komponen);
		$data['list_module'] = $this->crut->list_datas($q_modul);
		$data['posisi_page'] = $this->posisi;
		$data['type_grids'] = $this->type_grid;
		$data['css_head'] = array(
												'plugins/dragula/custom_layout.css',
												'plugins/dragula/dragula.min.css',
		);
		$data['js_footer'] = array(
									'plugins/dragula/dragula.min.js'
			);
		$this->parser->parse("pageadd.tpl",$data);
	}

	public function save(){
		$this->cek_hak_akses($this->privileges['menu_management']['pagemanager'],'2');

		///// simpan ke master layout
		$input_layout['nama'] = $this->input->post('nama',true);
		$input_layout['component'] = $this->input->post('component',true);
		$sukses = "Proses Tambah Halaman Dengan Nama ".$input_layout['nama']." Berhasil";
		$gagal = "Proses Tambah Halaman Dengan Nama ".$input_layout['nama']." Gagal";
		$pesan = $this->crut->insert('layout',$input_layout,'Tambah Halaman',$sukses,$gagal);
		$routes = $this->input->post('route');
		foreach($routes as $k => $v){
			$input_route['layout_id'] = $pesan['ids'];
			$input_route['route'] = $v['route'];
			$input_route['link'] = $v['link'];
			$this->crut->insert('layout_route',$input_route,'','','');
		}
		$moduls = $this->input->post('module');
		foreach($moduls as $k => $v){
			$param_pecah = explode(".", $v['parameter'].".");
			$input_module['layout_id'] = $pesan['ids'];
			$input_module['code'] = $v['module'];
			$input_module['parameter'] = $param_pecah[0];
			$input_module['parameter_name'] = $param_pecah[1];
			$input_module['position'] = $v['posisi'];
			$input_module['size_block'] = $v['size_block'];
			$input_module['sort_order'] = $v['order'];
			$this->crut->insert('layout_module',$input_module,'','','');
		}

		$this->session->set_flashdata('pesan', pesan($pesan));
		redirect(site_url('pagemanager/pagemanager/fetch'));

	}

	public function fetch(){
		$this->cek_hak_akses($this->privileges['menu_management']['pagemanager'],'1|2');

		$data['page_header'] ='Data Halaman';
		$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
		$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
									'plugins/datatables/dataTables.bootstrap.min.js');

		$this->parser->parse("pagefetch.tpl",$data);
	}

	public function fetch_data(){
			 $column_order = array('nama',null); //set column field database for datatable orderable keterangan
 			 $column_search = array('nama'); //set column field database for datatable searchable just nama
 			 $order = array('layout_id' => 'desc'); // default order

				$q = "select * from layout ";
				$i = 0;
				foreach($column_search as $item){

					if($_POST['search']['value']) // if datatable send POST for search
		            {

		                $q.= " where $item like '%".$_POST['search']['value']."%' ";
		            }

		            $i++;
				}

				if(isset($_POST['order'])) // here order processing
		        {
		            $q.= " order by ".$column_order[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir']." ";
		        }
		        else if(isset($order))
		        {
		            $q.= "order by `layout_id` DESC ";
		        }

				$hasil = $this->crut->list_datas($q);
				$total_data = $this->crut->list_count($q);

				$json = array(
						'draw'=>"",
						'recordsTotal' => $total_data,
						'recordsFiltered' => $total_data
					);
				foreach($hasil as $k => $v){
					$json['data'][] = array(
							'nama' =>$v['nama'],
							'aksi' =>'<a href="'.site_url("pagemanager/pagemanager/edit/".$v['layout_id']).'" class="btn btn-warning btn-sm" title="Edit "><i class="fa fa-pencil"></i></a>
                          <a href="'.site_url("pagemanager/pagemanager/delete/".$v['layout_id']).'" class="btn btn-danger btn-sm" title="Delete "><i class="fa fa-trash"></i></a>'
						);
				}

				$json = json_encode($json);
				echo $json;
	}

	public function edit($id =''){
		$this->cek_hak_akses($this->privileges['menu_management']['pagemanager'],'2');

		$q_komponen = "select * from extensions where type='component'";
		$q_modul = "select * from extensions where type='module'";

		$q_edit = "select * from layout where layout_id ='".$id."'";
		$q_routes = "select * from layout_route where layout_id='".$id."'";
		$q_modules = "select * from layout_module where layout_id='".$id."' order by position,sort_order ASC";

		$data['page_header'] ='Edit Data Halaman';
		$data['list_component'] = $this->crut->list_datas($q_komponen);
		$data['list_module'] = $this->crut->list_datas($q_modul);
		$data['posisi_page'] = $this->posisi;
		$data['type_grids'] = $this->type_grid;

		$data['detail'] = $this->crut->list_row($q_edit);
		$data['routes'] = $this->crut->list_datas($q_routes);
		$data['count_routes'] = $this->crut->list_count($q_routes);

		$data['modules'] = $this->crut->list_datas($q_modules);
		$data['count_modules'] = $this->crut->list_count($q_modules);

		$data['css_head'] = array(
												'plugins/dragula/custom_layout.css',
												'plugins/dragula/dragula.min.css',
		);
		$data['js_footer'] = array(
									'plugins/dragula/dragula.min.js'
			);

		$this->parser->parse("pageedit.tpl",$data);
	}

	public function update(){
		$this->cek_hak_akses($this->privileges['menu_management']['pagemanager'],'2');

		///// simpan ke master layout
		$filter = array('layout_id'=>$this->input->post('id'));
		$input_layout['nama'] = $this->input->post('nama',true);
		$input_layout['component'] = $this->input->post('component',true);
		$sukses = "Proses Update Halaman Dengan Nama ".$input_layout['nama']." Berhasil";
		$gagal = "Proses Update Halaman Dengan Nama ".$input_layout['nama']." Gagal";
		$pesan = $this->crut->update($filter,'layout',$input_layout,'Update Halaman',$sukses,$gagal);
		$routes = $this->input->post('route');
		$this->crut->delete($filter,'layout_route','','','');
		foreach($routes as $k => $v){
			$input_route['layout_id'] = $filter['layout_id'];
			$input_route['route'] = $v['route'];
			$input_route['menu_id'] = $v['menu_id'];
			$input_route['link'] = $v['link'];
			$input_route['controller'] = $v['controller'];
			$input_route['parameter'] = $v['parameter'];

			$this->crut->insert('layout_route',$input_route,'','','');
		}
		$moduls = $this->input->post('module');
		$this->crut->delete($filter,'layout_module','','','');
		foreach($moduls as $k => $v){
			$param_pecah = explode(".", $v['parameter'].".");
			$input_module['layout_id'] = $filter['layout_id'];
			$input_module['code'] = $v['module'];
			$input_module['parameter'] = $param_pecah[0];
			$input_module['parameter_name'] = $param_pecah[1];
			$input_module['position'] = $v['posisi'];
			$input_module['size_block'] = $v['size_block'];
			$input_module['sort_order'] = $v['order'];
			$this->crut->insert('layout_module',$input_module,'','','');
		}

		$this->session->set_flashdata('pesan', pesan($pesan));
		redirect(site_url('pagemanager/pagemanager/edit/'.$filter['layout_id']));

	}

	public function delete($id){
		$this->cek_hak_akses($this->privileges['menu_management']['pagemanager'],'2');

		$filter = array('layout_id'=>$id);
		$sukses = "Proses Hapus Halaman Dengan ID ".$id." Berhasil";
		$gagal = "Proses Hapus Halaman Dengan ID ".$id." Gagal";
		$this->crut->delete($filter,'layout_route','','','');
		$this->crut->delete($filter,'layout_module','','','');
		$pesan = $this->crut->delete($filter,'layout','Hapus Halaman',$sukses,$gagal);
		$this->session->set_flashdata('pesan', pesan($pesan));
		redirect(site_url('pagemanager/pagemanager/fetch'));

	}


}

/* End of file Pagemanager.php */
/* Location: ./application/modules/pagemanager/controllers/Pagemanager.php */
