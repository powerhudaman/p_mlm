{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3><br>
                  {if $this->session->flashdata('pesan') !=""}
                    {$this->session->flashdata('pesan')}
                  {/if}
                </div><!-- /.box-header -->
                <!-- form start -->
                {form_open(site_url('pagemanager/pagemanager/update'))}
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="box">
                          <div class="box-body">
                            <div class="form-group">
                              <label for="">Page Name</label>
                              <input type="text" name="nama" id="" value="{$detail['nama']}" class="form-control">
                              <input type="hidden" name="id" value="{$detail['layout_id']}">
                            </div>
                            <div class="form-group">
                              <label for="">Component</label>
                              <select name="component" id="" class="form-control">
                                <option value=""></option>
                                {foreach from=$list_component key=k item=v}
                                    {assign var=pilih value=''}
                                    {if $v['nama'] == $detail['component']}
                                        {$pilih='selected'}
                                    {/if}
                                  <option value="{$v['nama']}" {$pilih}>{$v['nama']}</option>
                                {/foreach}
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="box box-header">
                          <h5>Route</h5>
                        </div>
                        <div class="box">
                          <div class="box-body">
                            <table class="table table-bordered table-striped" id="routes">
                              <tr>
                                <th>Nama Route</th>
                                <th>Link Url</th>
                                <th></th>
                              </tr>
                              <tr id="">
                                <td><input type="text" name="" id="routeinput-1" class="form-control"></td>
                                <td><input type="text" name="" id="linkinput-1" class="form-control"></td>
                                <td><a href="#" class="btn btn-sm btn-primary" onclick="tambah_route()" data-no="{$count_routes}" id="b_route">+</a></td>
                              </tr>
                              {assign var=no_routes value='0'}
                              {foreach from=$routes key=k item=v}
                                {$no_routes = $no_routes + 1}
                                <tr id="route{$no_routes}">
                                  <td><input type="text" name="route[{$no_routes}][route]" id="routeinput-{$no_routes}" class="form-control" value="{$v['route']}">
                                    <input type="hidden" name="route[{$no_routes}][menu_id]" value="{$v['menu_id']}">

                                    <input type="hidden" name="route[{$no_routes}][controller]" value="{$v['controller']}">
                                    <input type="hidden" name="route[{$no_routes}][parameter]" value="{$v['parameter']}">
                                  </td>
                                  <td>
                                    <input type="text" name="route[{$no_routes}][link]" id="linkinput-$no_routes}" class="form-control" value="{$v['link']}">
                                  </td>
                                  <td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_route({$no_routes})">X</a></td>
                                </tr>
                              {/foreach}
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="box box-primary">
                          <div class="box box-header">
                            <h5>Module Show</h5>
                          </div>
                          <div class="box-body">
                            <div class="row">
                              <div class="col-md-3">
                                <div class="panel-group ">
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse1">List Modul</a>
                                      </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse">
                                      <ul class="list-group" id="left-content">
                                        {assign var=id_module value='0'}
                                        {foreach from=$list_module key=k item=v}
                                          {$id_module = $id_module + 1}
                                          <li class="list-group-item" id="modul_drag{$id_module}" data-module="{$v['nama']}" data-no="">{$v['nama']}</li>
                                        {/foreach}
                                      </ul>
                                    </div>
                                  </div>
                                </div>

                              </div>
                              <div class="col-md-9">

                                    <!-- contoh map layout -->


                                      <div class="row" data-pg-collapsed>
                                          <div class="col-md-12 back-content" data-posisi ="pos-1" id="l-pos-1">
                                              <h3>Top Nama (pos 1)</h3>
                                          </div>
                                      </div>
                                      <div class="row" data-pg-collapsed>
                                          <div class="col-md-9 back-content" data-posisi ="pos-2" id="l-pos-2">
                                              <h3>Top Nav (pos 2)</h3>

                                          </div>
                                          <div class="col-md-3 back-content" data-posisi ="pos-3" id="l-pos-3">
                                              <h3>Search (pos 3)</h3>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-12 back-content" data-pg-collapsed data-posisi ="pos-4" id="l-pos-4" >
                                              <h3>Banner Slider (pos 4)</h3>

                                          </div>
                                      </div>
                                      <div class="row" data-pg-collapsed>
                                          <div class="col-md-3 back-content" data-pg-collapsed data-posisi ="pos-5" id="l-pos-5">
                                              <h3>Left Nav (pos 5)</h3>

                                          </div>
                                          <div class="col-md-9 back-content" data-pg-collapsed data-posisi ="pos-6" id="l-pos-6">
                                              <h3>Main Content (pos 6)</h3>

                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-3 back-content" data-pg-collapsed data-posisi ="pos-8" id="l-pos-8">
                                              <h3>Bottom Left Nav (pos 8)</h3>

                                          </div>
                                          <div class="col-md-9 back-content" data-pg-collapsed data-posisi ="pos-9" id="l-pos-9">
                                              <h3>Bottom Content (pos 9)</h3>

                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-12 back-content" data-pg-collapsed data-posisi ="pos-11" id="l-pos-11">
                                              <h3>Foot Content (pos 11)</h3>

                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-12 back-content" data-pg-collapsed data-posisi ="pos-12" id="l-pos-12">
                                              <h3>Bottom Foot Content (pos 12)</h3>

                                          </div>
                                      </div>


                                    <!-- contoh map layout end -->
                              </div>
                            </div>

                             <table class="table table-bordered table-striped" id="moduls" style="display:none;">
                              <tr>
                                <th>Module</th>
                                <th>Paramater</th>
                                <th>Posisi</th>
                                <th>Ukurang</th>
                                <th>Ordering</th>
                              </tr>
                              {assign var=no_modules value='0'}
                              {if $modules !='0'}
                                {foreach from=$modules key=k item=v}
                                    {$no_modules = $no_modules + 1}
                                    <tr id="modul{$no_modules}">
                                      <td><input type="text" name="module[{$no_modules}][module]" id="" value="{$v['code']}" class="form-control" readonly="readonly"></td>
                                      <td><input type="text" name="module[{$no_modules}][parameter]" id="" value="{$v['parameter']}.{$v['parameter_name']}" class="form-control" readonly="readonly"></td>
                                      <td><input type="text" name="module[{$no_modules}][posisi]" id="module_posisi_{$no_modules}" value="{$v['position']}" class="form-control" readonly="readonly"></td>
                                      <td><input type="text" name="module[{$no_modules}][size_block]" id="" value="{$v['size_block']}" class="form-control" readonly="readonly"></td>
                                      <td><input type="text" name="module[{$no_modules}][order]" id="" value="{$v['sort_order']}" class="form-control" readonly="readonly"></td>
                                      <td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_module({$no_modules})">X</a></td>
                                    </tr>
                                {/foreach}
                              {/if}
                            </table>

                          </div>
                        </div>
                      </div>
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

    <!-- modal dialog -->

    <div class="modal fade" tabindex="-1" role="dialog" id="modal_parameter">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Parameter Module <span id="modal_title"></span></h4>
          </div>
          <div class="modal-body">
            <h4>Data Parameter Module</h4>
            <div class="form-group">
              <label for="">Parameter</label>
              <input type="hidden" name="module" id="module" value="">
              <input type="hidden" name="posisi" id="posisi" value="">
              <input type="hidden" name="id_mod" id="id_mod" value="">;
              <div id="list-parameter"></div>

            </div>
            <div class="form-group">
                <label for="">Ukuran</label>
                <select name="parameter_ukuran" id="parameter_ukuran" class="form-control">
                  <option value=""></option>
                  {foreach from=$type_grids key=k item=v}
                  <option value="{$k}">{$v}</option>
                  {/foreach}
                </select>
            </div>
            <div class="form-group">
              <label for="">Urutan</label>
              <input type="number" name="order" id="order" class="form-control" value="">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="#" class="btn btn-primary" onclick="tambah_module()" data-no="{count($modules)}" id="b_module">Simpan</a>
          </div>
        </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

    <!-- modal dialog end -->

{/block}

{block name=script_js}

    <script>
      var tambah_route = function(){
        var html='';
        var route_addr = $("#routeinput-1").val();
        var link_addr = $("#linkinput-1").val();
        var b_route = $("#b_route").attr("data-no");
        var no = parseInt(b_route) + 1;
            html +='<tr id="route'+no+'">';
              html +='<td><input type="text" name="route['+no+'][route]" id="routeinput-'+no+'" class="form-control" value="'+route_addr+'">';
                      html +='<input type="hidden" name="route['+no+'][menu_id]" value="">';
                      // html +='<input type="hidden" name="route['+no+'][link]" value="">';
                      html +='<input type="hidden" name="route['+no+'][controller]" value="">';
                      html +='<input type="hidden" name="route['+no+'][parameter]" value="">';
              html +='</td>';
              html +='<td>';
                html +='<input type="text" name="route['+no+'][link]" id="linkinput-'+no+'" class="form-control" value="'+link_addr+'">';
              html +='</td>';
              html +='<td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_route('+no+')">X</a></td>';
            html +='</tr>';
        $("#routes").append(html);
        $("#routeinput-1").val('');
        $("#linkinput-1").val('');
        $("#b_route").attr("data-no",no);
      }

      var hapus_route = function(id){
          $('#route'+id).remove();
      }

      //// get module data begin
      /*
      $("#module").change(function(){
          $("#r-parameter").html('Loading');
          var param_data = $("#module").val();
          $.getJSON('{site_url("'+param_data+'/'+param_data+'/parameter")}','', function(data) {
              var html ='<select name="" id="parameter" class="form-control"><option value=""></option>';
              $.each(data, function(i,val) {
                   html +='<option value="'+val.code+'.'+val.nama+'">'+val.nama+'</option>';
                   // console.log(val)
                });
              html +='</select>';
            $("#r-parameter").html(html);

          });

      })
      */
      /// get module data end

      var tambah_module = function(){
        var html ='';
        var mod = $("#module").val();
        var param = $("#parameter").val();
        var pos = $("#posisi").val();
        var size_block = $('#parameter_ukuran').val();
        var order = $("#order").val();
        var b_module = $("#b_module").attr("data-no");
        var no = parseInt(b_module) + 1;
        var target_id_mod = $('#id_mod').val();

        html +='<tr id="modul'+no+'">';
            html +='<td><input type="text" name="module['+no+'][module]" id="" value="'+mod+'" class="form-control" readonly="readonly"></td>';
            html +='<td><input type="text" name="module['+no+'][parameter]" id="" value="'+param+'" class="form-control" readonly="readonly"></td>';
            html +='<td><input type="text" name="module['+no+'][posisi]" id="module_posisi_'+no+'" value="'+pos+'" class="form-control" readonly="readonly"></td>';
              html +='<td><input type="text" name="module['+no+'][size_block]" id="" value="'+size_block+'" class="form-control" readonly="readonly"></td>';
            html +='<td><input type="text" name="module['+no+'][order]" id="" value="'+order+'" class="form-control" readonly="readonly"></td>';
            html +='<td><a href="#" class="btn btn-sm btn-danger" onclick="hapus_module('+no+')">X</a></td>';
          html +='</tr>';
          $("#moduls").append(html);
          $('#posisi').val('');
          $("#b_module").attr("data-no",no);
          $("#"+target_id_mod).attr("data-no",no);
          $("#"+target_id_mod).append(" ("+param+" -- "+order+")");

          $("#module").val();
          $("#parameter").val();
          $("#order").val();

          $('#modal_parameter').modal('hide');
      }

      var hapus_module = function(no){
        $("#modul"+no).remove();
      }

      var getDataModule = function(module,element_target){
          $.getJSON('{site_url("'+module+'/'+module+'/parameter")}','', function(data) {
                var html ='<select name="" id="parameter" class="form-control"><option value=""></option>';
                /*optional stuff to do after success */
                $.each(data, function(i,val) {
                     html +='<option value="'+val.code+'.'+val.nama+'">'+val.nama+'</option>';
                     // console.log(val)
                  });
                html +='</select>';
              $("#"+element_target).html(html);
          });
      }
      var close_button = function(){
        var id = $('#id_mod').val();
        $('#'+id).remove();
        $('#id_mod').val('');
      }

      var containers = [document.querySelector('#left-content'),
                        document.querySelector('#l-pos-1'),
                        document.querySelector('#l-pos-2'),
                        document.querySelector('#l-pos-3'),
                        document.querySelector('#l-pos-4'),
                        document.querySelector('#l-pos-5'),
                        document.querySelector('#l-pos-6'),
                        document.querySelector('#l-pos-8'),
                        document.querySelector('#l-pos-9'),
                        document.querySelector('#l-pos-11'),
                        document.querySelector('#l-pos-12')
                      ];
      dragula(containers, {
        isContainer: function (el) {
          return false; // only elements in drake.containers will be taken into account
        },
        moves: function (el, source, handle, sibling) {
          return true; // elements are always draggable by default
        },
        accepts: function (el, target, source, sibling) {
          return target !== document.getElementById("left-content")
        },
        invalid: function (el, handle) {
          return false; // don't prevent any drags from initiating by default
        },
        direction: 'vertical',             // Y axis is considered when determining where an element would be dropped
        // copy: false,                       // elements are moved by default, not copied
        copy: function (el, source) {
          return source === document.getElementById("left-content")
        },
        copySortSource: false,             // elements in copy-source containers can be reordered
        revertOnSpill: true,              // spilling will put the element back where it was dragged from, if this is true
        removeOnSpill: true,              // spilling will `.remove` the element, if this is true
        mirrorContainer: document.body,    // set the element that gets mirror elements appended
        ignoreInputTextSelection: true     // allows users to select input text, see details below
      })
      .on('drop',function(el, target, source, sibling){
        var module = $('#'+el.id).attr('data-module');
        var posisi = $('#'+target.id).attr('data-posisi');

        $('#module').val(module);
        $('#posisi').val(posisi);

        var b_module = $("#b_module").attr("data-no");
            b_module = parseInt(b_module) + 1;

        var el_no = $('#'+el.id).attr('data-no')
        var cek_module_input = $('#module_posisi_'+el_no);

        // cek apakah mod dibuat baru atau pindah posisi, bila hanya pindah posisi maka fungsi ini akan dilakukan
        if( el_no > 0 ){
          $('#module_posisi_'+el_no).val(posisi);
        }
        if(el_no == 0 ){
          $('#modal_parameter').modal('show');
          $('#modal_title').html(module);
          getDataModule(module,"list-parameter");

          $('#'+target.id).children('li#'+el.id).attr('id','mod_'+b_module);
          $('#id_mod').val('mod_'+b_module);
        }

      })
      .on('remove',function(el, target, source, sibling){
        var module = $('#'+el.id).attr('data-module');
        var no = $('#'+el.id).attr('data-no');
        hapus_module(no);
      });

      /*
        fungsi ini digunakan untuk memasukan list module kedalam posisi yang telah disediankan
       */
      var list_pos_module = function(id,nama,param,no,order=''){
        html = '';
        html += '<li class="list-group-item" id="mod_'+no+'" data-module="'+nama+'" data-no="'+no+'">'+nama+' ('+param+' -- '+order+')</li>';
        $('#l-'+id).append(html);
        // alert(id);
      }
      {assign var=no_modules_pos value='0'}
      {if $modules !='0'}
        {foreach from=$modules key=k item=v}
            {$no_modules_pos = $no_modules_pos + 1}
            list_pos_module('{$v['position']}','{$v['code']}','{$v['parameter_name']}','{$no_modules_pos}','{$v['sort_order']}');
        {/foreach}
      {/if}

    </script>

{/block}
