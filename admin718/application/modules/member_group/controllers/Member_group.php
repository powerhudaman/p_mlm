<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_group extends MY_Controller {
	protected $approval = array('1'=>'Tidak Approval','2'=>'Approval');
	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');
  protected $component_akses = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->cek_hak_akses($this->privileges['component']['member_group'],'1|2');
    $this->list_akses();
	}

  private function list_akses(){
    $q_1 = "select * from component_akses where level_component = '1' and status_delete = '0'";
    $d_1 = $this->crut->list_datas($q_1);
    foreach ($d_1 as $k1 => $v1) {
      $this->component_akses['component_akses'][] = array('code'=>$v1['code_component'],'nama'=>$v1['nama_component']);
      // level 2
      $q_2 = "select * from component_akses where level_component = '2' and parent_id = '".$v1['id']."' and status_delete = '0'";
      $d_2 = $this->crut->list_datas($q_2);
      if($d_2 !=0){
        foreach ($d_2 as $k2 => $v2) {
          $this->component_akses['component_akses'][] = array('code'=>$v2['code_component'],'nama'=>$v2['nama_component']);

          // level 3
          $q_3 = "select * from component_akses where level_component = '3' and parent_id = '".$v2['id']."' and status_delete = '0'";
          $d_3 = $this->crut->list_datas($q_3);
          if($d_3 !=0){
            foreach ($d_3 as $k3 => $v3) {
              $this->component_akses['component_akses'][] = array('code'=>$v3['code_component'],'nama'=>$v3['nama_component']);

            }
          }
          // level 3
        }
      }
      // level 2
    }
  }

	public function add(){
		$this->cek_hak_akses($this->privileges['component']['member_group'],'2');

		// $q="select * from admin_groups where delete_status ='0'";

		$data['page_header'] ='Data Member Group';
		$data['url'] = 'User/save';
    $data['component_akses'] = $this->component_akses['component_akses'];
		// $data['approval'] = $this->approval;
		// $data['status'] = $this->status;
		// $data['groups'] = $this->crut->list_datas($q);
		$this->parser->parse("add.tpl",$data);
	}
	public function save(){
		$this->cek_hak_akses($this->privileges['component']['member_group'],'2');
		// $this->cek_hak_akses($this->privileges['customer_management']['group_customers'],'2');
		/*
		ALTER TABLE `customer_group`
		ADD COLUMN `addon`  longtext NULL AFTER `sort_order`;
		 */

		$this->form_validation->set_rules('nama', 'Nama Group', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			$this->add();
		} else {
      $input['kode'] = genRndString(5,'1234567890BCDFGHJKLMNPQRSTVWXYZ');
      $input['nama'] = $this->input->post('nama',true);
			$input['hak_akses'] = json_encode($_POST['component_akses']);
      $input['status_delete'] = 0;

			$sukses = "Proses Tambah Group Member dengan Nama = ".$input['nama']." Berhasil";
			$gagal = "Proses Tambah Group Member dengan Nama = ".$input['nama']." Gagal";
			$pesan = $this->crut->insert('member_group',$input,'Tambah Member Group',$sukses,$gagal);
      //insert log
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'Membergroup',
				'jenis_operasi'=>'Insert',
				'data'=>json_encode($input),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log
			$this->session->set_flashdata('pesan', pesan($pesan));
      // var_dump($pesan);
			redirect(site_url('member_group/fetch'));
		}

	}
	public function fetch(){
		$this->cek_hak_akses($this->privileges['component']['member_group'],'1|2');

			$data['page_header'] ='Data Member Group';
			$data['url_admin'] = ADMINS;
			$data['url_add'] = 'member_group/add';

			// $data['contents'] = 'admin_group/add.tpl';

			$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
			$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
										'plugins/datatables/dataTables.bootstrap.min.js');

			$this->parser->parse("fetch.tpl",$data);
		}


    public function fetch_data(){
			// $this->cek_hak_akses($this->privileges['component']['produk_slider'],'1|2');
      $this->load->library('datatables');

			$this->datatables->select('id,kode, nama');
			$this->datatables->from('member_group')->where('status_delete',0)
			->unset_column('id')
			->add_column('aksi',link_generator('member_group/member_group','$1'),'id')
			;
			echo $this->datatables->generate('json');
		}


		public function edit($id){
			// $this->cek_hak_akses($this->privileges['customer_management']['group_customers'],'2');
			$this->cek_hak_akses($this->privileges['component']['member_group'],'2');


			$q="select * from member_group where id ='".$id."'";

			$data['page_header'] ='Edit Data Kustomer Group';
			$data['url'] = 'User/update';
      $data['component_akses'] = $this->component_akses['component_akses'];

			$data['approval'] = $this->approval;
			$data['status'] = $this->status;

			$data['detail'] = $this->crut->list_row($q);
			$data['detail_addon'] = '';
			if($data['detail']['addon'] !=""){
				$data['detail_addon'] = json_decode($data['detail']['addon'],true);
				foreach ($data['detail_addon'] as $k => $v) {
					$data[$k] = $v;
				}
			}
			$this->parser->parse("edit.tpl",$data);
		}

		public function update(){
			$this->cek_hak_akses($this->privileges['component']['member_group'],'2');


			$this->form_validation->set_rules('nama', 'Nama Group', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$this->edit($this->input->post('id',true));
			} else {
				$filter = array('id'=>$this->input->post('id',true));
        $input['nama'] = $this->input->post('nama',true);
        $input['hak_akses'] = json_encode($_POST['component_akses']);

        $sukses = "Proses Update Group Member dengan Nama = ".$input['nama']." Berhasil";
        $gagal = "Proses Update Group Member dengan Nama = ".$input['nama']." Gagal";
        $pesan = $this->crut->update($filter,'member_group',$input,'Update Member Group',$sukses,$gagal);
        //insert log
        $dt = array('filter'=>$filter,'input'=>$input);
        $insert_log = array(
          'kode_log'=>microtime(),
          'halaman'=>'Membergroup',
          'jenis_operasi'=>'Update',
          'data'=>json_encode($dt),
          'ip_user'=>$_SERVER['REMOTE_ADDR'],
          'user_id'=>$this->session->userdata('user_id'),
          'date_created'=>date('Y-m-d H:i:s')
        );
        $this->crut->insert('log_aktivitas',$insert_log,'','','');
        //insert log
        $this->session->set_flashdata('pesan', pesan($pesan));
				redirect(site_url('member_group/fetch'));
			}

		}

		public function delete($id){
				// $this->cek_hak_akses($this->privileges['customer_management']['group_customers'],'2');
				$this->cek_hak_akses($this->privileges['component']['member_group'],'2');

				$filter = array('id'=>$id);
				$sukses = "Proses Hapus Member dengan ID = ".$id." Berhasil";
				$gagal = "Proses Hapus Member dengan ID = ".$id." Gagal";
				$input['status_delete'] = '1';
				$pesan = $this->crut->update($filter,'member_group',$input,'Hapus Customer Group',$sukses,$gagal);
        //insert log
        $dt = array('filter'=>$filter,'input'=>$input);
        $insert_log = array(
          'kode_log'=>microtime(),
          'halaman'=>'Membergroup',
          'jenis_operasi'=>'Delete',
          'data'=>json_encode($dt),
          'ip_user'=>$_SERVER['REMOTE_ADDR'],
          'user_id'=>$this->session->userdata('user_id'),
          'date_created'=>date('Y-m-d H:i:s')
        );
        $this->crut->insert('log_aktivitas',$insert_log,'','','');
        //insert log

				$this->session->set_flashdata('pesan', pesan($pesan));
				redirect(site_url('member_group/fetch'));
		}

}

/* End of file Customergroup.php */
/* Location: ./application/modules/customergroup/controllers/Customergroup.php */
