{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
            {form_open(site_url('member_group/update'))}
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                  <div class="box-body">
                    <div class="form-group">
                      {form_error('nama')}
                      <label for="">Nama</label>
                      <input type="text" name="nama" id="nama" class="form-control" value="{$detail['nama']}" required>
                      <input type="hidden" name="id" value="{$detail['id']}">
                    </div>
                  </div><!-- /.box-body -->


              </div><!-- /.box -->

              <!-- accordion  -->
              <!--
                <div class="box box-primary">
                  <div class="box-header">
                    <h4>Pengaturan Tambahan</h4>
                  </div>
                  <div class="box-body">

                    <div class="panel-group" id="accordion">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                            Pengaturan Fitur Reseller</a>
                          </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                          <div class="panel-body">
                            <div class="form-group">
                              <label for="">Format No Invoice</label>
                              <input type="text" name="addon[reseller][no_invoice]" id="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="">Minimal Belanja</label>
                              <input type="text" name="addon[reseller][minimal_belanja]" id="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="">Status Reseller</label>
                              <select name="addon[reseller][status_reseller]" id="" class="form-control">
                                <option value=""></option>
                                {foreach from=$status key=k item=v}
                                  <option value="{$k}">{$v}</option>
                                {/foreach}
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>-->
                  <!-- hak_akses  -->
                    <div class="box box-primary">
                      <div class="box-header">
                        <h4>Hak Akses Member Group</h4>
                      </div>
                      <div class="box-body">
                          <div class="form-group">
                            <table class="table table-striped">
                              <tr>
                                  <th>Nama Modul</th>
                                  <th>Tanpa Akses</th>
                                  <th>Baca</th>
                                  <th>Tulis</th>
                              </tr>
                              {assign var=cek_0 value=''}
                              {assign var=cek_1 value=''}
                              {assign var=cek_2 value=''}
                              {foreach from=$component_akses key=k item=v}
                                {if $list_akses[$k][$v['code']] == 0}
                                  {$cek_0 = 'checked'}
                                {/if}
                                {if $list_akses[$k][$v['code']] == 1}
                                  {$cek_1 = 'checked'}
                                {/if}
                                {if $list_akses[$k][$v['code']] == 2}
                                  {$cek_2 = 'checked'}
                                {/if}
                                <tr>
                                  <td>{$v['nama']}</td>
                                  <td><input type="radio" name="component_akses[{$k}][{$v['code']}]" value="0" id="" checked="{$cek_0}"></td>
                                  <td><input type="radio" name="component_akses[{$k}][{$v['code']}]" value="1" id="" checked="{$cek_1}"></td>
                                  <td><input type="radio" name="component_akses[{$k}][{$v['code']}]" value="2" id="" checked="{$cek_2}"></td>
                                </tr>

                              {/foreach}

                            </table>
                          </div>
                      </div>
                    </div>
                  <!-- hak_akses -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              <!-- accordion -->
              {form_close()}

            </div>

  	</div>

{/block}
