{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">
            <!-- <a href="#" class="btn btn-primary" title="" ><i class="fa fa-refresh"></i></a> -->
  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3><br>
                  {if $this->session->flashdata('pesan') !=""}
                    {$this->session->flashdata('pesan')}
                  {/if}
                </div><!-- /.box-header -->
                <!-- form start -->

                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12" id="filter">
                        {assign var=aturan_form value=['method'=>'get']}
                        {form_open(site_url('sms_log/fetch'),$aturan_form)}
                        <!--
                        $halaman = $this->input->get('halaman',true);
                        $jenis_operasi = $this->input->get('jenis_operasi',true);
                        $keyword = $this->input->get('keyword',true);
                        $ip_user = $this->input->get('ip_user',true);
                        $user_id = $this->input->get('user_id',true);
                        $tgl_1 = $this->input->get('tgl_1',true);
                        $tgl_2 = $this->input->get('tgl_2',true);
                       -->

                       <div class="form-group">
                           <label for="">Jenis Operasi</label>
                           <select name="jenis_operasi" id="jenis_operasi" class="form-control">
                               <option value=""></option>
                             {foreach from=$list_operasi key=k item=op}
                               <option value="{$k}">{$op}</option>
                             {/foreach}
                           </select>
                       </div>
                       <div class="form-group">
                              <label for="">ID Member</label>
                              <input type="text" name="id_member" id="id_member" class="form-control">
                        </div>

                        <div class="form-group">
                              <label for="">Tanggal Mulai</label>
                              <input type="text" name="tgl_1" id="tgl_1" class="form-control">
                        </div>

                        <div class="form-group">
                              <label for="">Tanggal Selesai</label>
                              <input type="text" name="tgl_2" id="tgl_2" class="form-control">
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Cari" class="btn btn-success">
                        </div>
                        {form_close()}
                      </div>
                      <!--
                      <div class="col-md-12">
                        <a href="#" class="btn btn-primary">Tampilkan Filter</a>
                      </div>
                      -->
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        {if $link_get !='?'}
                          <a href="{site_url('sms_log/kirim_sms_massal')}{$link_get}" class="btn btn-success">Kirim SMs Massal</a> <br>
                        {/if}
                        <div class="table-responsive">
                          <table id="" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>Tanggal</th>
                                <th>ID Member</th>
                                <th>Judul Pesan</th>
                                <th>Isi Pesan</th>
                                <th>No Tujuan</th>
                                <th>Status</th>
                                <th>Aksi</th>
                              </tr>
                            </thead>
                            <tbody>
                              {if array_key_exists('pesan',$list_logs)}
                                  <tr>
                                    <td colspan="7">Data Log Aktivitas Tidak Ditemukan</td>
                                  </tr>
                              {/if}
                              {if !array_key_exists('pesan',$list_logs)}
                              {foreach from=$list_logs key=k item=v}
                              <tr>
                                <td>{$v['created_date']}</td>
                                <td>{$v['id_member']}</td>
                                <td>{$v['judul_pesan']}</td>
                                <td>{$v['isi_pesan']}</td>
                                <td>{$v['no_tujuan']}</td>
                                <td>{$v['status_terkirim']}</td>
                                <td>
                                  <a href="{site_url('sms_log/kirim_sms_status')}/{$v['id']}" class="btn btn-warning">Kirim Ulang</a>
                                </td>
                              </tr>
                              {/foreach}
                              {/if}
                            </tbody>
                          </table>
                        </div>

                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        {$pagination}
                      </div>
                    </div>
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div>

  	</div>


{/block}

{block name=script_js}
  <script>

     $('#id_member').autocomplete({
      source: "{site_url('/member_master/autocomplete_hirarki')}",
      select: function(event, data) {
        $("#id_member").focus();
      }
    });

    $("#tgl_1").datepicker({ format:'yyyy-mm-dd' });
    $("#tgl_2").datepicker({ format:'yyyy-mm-dd' });

  </script>

{/block}
{block name=sidebar-menu}
   {$side_menu}
{/block}
