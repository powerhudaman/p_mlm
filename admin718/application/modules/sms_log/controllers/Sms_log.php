<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms_log extends MY_Controller {

	protected $list_operasi = array();


	public function fetch($offset = 0){
    $this->list_operasi = $this->kode_log_type_sms;
		// $this->cek_hak_akses($this->privileges['log_aktivitas']['log_aktivitas'],'1|2');
		// $offset = $this->uri->segment(4);
		$filter = "";
		$jenis_operasi = $this->input->get('jenis_operasi',true);
		$id_member = $this->input->get('id_member',true);
		$pecah = explode(' | ',$id_member.' | ');
		$id_member = $pecah[0];

		$tgl_1 = $this->input->get('tgl_1',true);
        $tgl_2 = $this->input->get('tgl_2',true);


		if(!empty($jenis_operasi)){
			if(empty($filter)){
				$filter =" where a.type_sms ='".$jenis_operasi."'";
			}else{
				$filter .=" and a.type_sms ='".$jenis_operasi."'";
			}
        }

        if(!empty($id_member)){
			if(empty($filter)){
				$filter =" where a.id_member ='".$id_member."'";
			}else{
				$filter .=" and a.id_member ='".$id_member."'";
			}
		}




		if(!empty($tgl_1) && !empty($tgl_2)){
			if(empty($filter)){
				$filter =" where date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
			}else{
				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
			}
		}

    $link_get = "?";
    foreach ($_GET as $k => $v) {
      $link_get  .=$k.'='.$v.'&';
    }
    // echo $link_get;
    // echo '<pre>';
    // print_r($_GET);
    // die();



		 $q_log = " SELECT * FROM sms_log as a ".$filter;

                $q_log_count = "SELECT count(*) as total FROM sms_log as a ".$filter;
 	        // 	$d_log = $this->crut->list_data($q_log);


		// echo $q_log;
		// die();

		$this->load->library('pagination');

		$config['base_url'] = site_url('log_aktivitas_2/fetch');

		$config['per_page'] = 50;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$list_logs = $this->crut->list_data($q_log,$config['per_page'],$offset);

		$config['total_rows'] = $this->crut->list_row($q_log_count)['total'];

		$this->pagination->initialize($config);

		$data['page_header'] ='Data Log';
		$data['url_admin'] = '';
		$data['url_add'] = '';

		$data['list_operasi'] = $this->list_operasi;
		// $data['side_menu'] = $this->side_menu;
		$data['list_logs'] = $list_logs;
    $data['link_get'] = $link_get;

		$data['pagination'] = $this->pagination->create_links();



			// $data['contents'] = 'admin_group/add.tpl';


			$data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
			'plugins/select2/select2.min.css',
			'plugins/datepicker/datepicker3.css'
				);
				$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
								'plugins/select2/select2.min.js',
								'plugins/datepicker/bootstrap-datepicker.js'
				);

		$this->parser->parse("sms_log/fetch.tpl",$data);

		}

    public function kirim_sms_status($id){
      $q = "select * from sms_log where id = '".$id."' ";
      $d_q = $this->crut->list_row($q);
      $response = array('status'=>false,'kode'=>2,'pesan'=>'Sms log tidak dikenali silahkan coba lagi');
      if($d_q !=0){
        $respon_sms = sms_zensiva($d_q['isi_pesan'],$d_q['no_tujuan']);
        if($respon_sms == "Success"){
          $response = array('status'=>true,'kode'=>1,'pesan'=>'Sms Berhasil dikirim ulang ');
        }else{
          $response = array('status'=>false,'kode'=>2,'pesan'=>'Sms gagal dikirim dengan keterangan '.$respon_sms);
        }
      }else{
        $response = array('status'=>false,'kode'=>2,'pesan'=>'Sms log tidak dikenali silahkan coba lagi');
      }
      $this->session->set_flashdata('pesan',pesan($response));
      redirect(site_url('sms_log/fetch'));
    }

    public function kirim_sms_massal(){
      $filter = "";
  		$jenis_operasi = $this->input->get('jenis_operasi',true);
  		$id_member = $this->input->get('id_member',true);
  		$pecah = explode(' | ',$id_member.' | ');
  		$id_member = $pecah[0];

  		$tgl_1 = $this->input->get('tgl_1',true);
          $tgl_2 = $this->input->get('tgl_2',true);


  		if(!empty($jenis_operasi)){
  			if(empty($filter)){
  				$filter =" where a.type_sms ='".$jenis_operasi."'";
  			}else{
  				$filter .=" and a.type_sms ='".$jenis_operasi."'";
  			}
          }

          if(!empty($id_member)){
  			if(empty($filter)){
  				$filter =" where a.id_member ='".$id_member."'";
  			}else{
  				$filter .=" and a.id_member ='".$id_member."'";
  			}
  		}

  		if(!empty($tgl_1) && !empty($tgl_2)){
  			if(empty($filter)){
  				$filter =" where date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
  			}else{
  				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
  			}
  		}

  		 $q_log = " SELECT * FROM sms_log as a ".$filter;
      $d_log = $this->crut->list_datas($q_log);
      $response = array('status'=>false,'kode'=>2,'pesan'=>'Sms log tidak dikenali silahkan coba lagi');

      if($d_log !=0){
        $no = 0;
        foreach ($d_log as $k => $v) {
          $respon_sms = sms_zensiva($v['isi_pesan'],$v['no_tujuan']);
          if($respon_sms == "Success"){
            $no++;
          }
        }
        if($no > 0 ){
          $response = array('status'=>true,'kode'=>1,'pesan'=>'Sms Berhasil dikirim ulang dengan jumlah  '.$no.' SMS ');

        }else{
          $response = array('status'=>false,'kode'=>2,'pesan'=>'Sms log tidak dikenali silahkan coba lagi');

        }
      }else{
        $response = array('status'=>false,'kode'=>2,'pesan'=>'Sms log tidak dikenali silahkan coba lagi');

      }

      $this->session->set_flashdata('pesan',pesan($response));
      redirect(site_url('sms_log/fetch'));

    }

}
