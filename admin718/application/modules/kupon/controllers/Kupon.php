<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kupon extends MY_Controller {
	protected $approval = array('1'=>'Tidak Approval','2'=>'Approval');
	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');
  protected $component_akses = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		// $this->cek_hak_akses($this->privileges['component']['member_group'],'1|2');

	}

  public function tambah_kupon(){
    $data['page_header'] ='Tambah Kupon';
    $data['url_admin'] = ADMINS;
    $data['url_add'] = '';

    $data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
                              'plugins/select2/select2.min.css',
                              'plugins/datepicker/datepicker3.css'
                        );
    $data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
                                'plugins/select2/select2.min.js',
                                'plugins/datepicker/bootstrap-datepicker.js'
                        );

    $this->parser->parse("kupon/add.tpl",$data);
  }

  public function save_kupon(){
    $q_cek = "select id,id_member,nama_lengkap,kupon from member_master where id = '".$this->input->post('nama',true)."';";
    $d_cek = $this->crut->list_row($q_cek);
    if($d_cek !=0){
      $filter = array('id'=>$this->input->post('nama',true));
      $update['kupon'] = $this->input->post('nilai_kupon')+$d_cek['kupon'];
      $update['update_date'] = date('Y-m-d H:i:s');
      $update['update_by'] = $this->session->userdata('user_id');
      $sukses = "Proses Tambah Kupon Untuk ID Member = ".$d_cek['id_member']." Berhasil";
      $gagal = "Proses Tambah Kupon  Untuk ID Member = ".$d_cek['id_member']." Gagal";
      $response = $this->crut->update($filter,'member_master',$update,'',$sukses,$gagal);

      //insert log
      $dt = array('filter'=>$filter,'input'=>$update);
      $insert_log = array(
        'kode_log'=>microtime(),
        'halaman'=>'Kupon',
        'jenis_operasi'=>'Insert',
        'data'=>json_encode($dt),
        'ip_user'=>$_SERVER['REMOTE_ADDR'],
        'user_id'=>$this->session->userdata('user_id'),
        'date_created'=>date('Y-m-d H:i:s')
      );
      $this->crut->insert('log_aktivitas',$insert_log,'','','');
      //insert log
      $this->session->set_flashdata('pesan', pesan($response));
      redirect(site_url('kupon/fetch'));
    }

  }

  public function fetch(){
		// $this->cek_hak_akses($this->privileges['component']['member_group'],'1|2');

			$data['page_header'] ='Data Kupon Member';
			$data['url_admin'] = ADMINS;
			$data['url_add'] = 'kupon/tambah_kupon';

			// $data['contents'] = 'admin_group/add.tpl';

			$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
			$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
										'plugins/datatables/dataTables.bootstrap.min.js');

			$this->parser->parse("kupon/fetch.tpl",$data);
		}


    public function fetch_data(){
			// $this->cek_hak_akses($this->privileges['component']['produk_slider'],'1|2');
      $this->load->library('datatables');

			$this->datatables->select('id,id_member,nama_lengkap, kupon');
			$this->datatables->from('member_master')->where('del',0)
			->unset_column('id')
			->add_column('aksi',link_generator('member_master/member_master','$1'),'id')
			;
			echo $this->datatables->generate('json');
		}




  public function autocomplete_member(){
    $q = $this->input->get('term');
    $q_auto = "SELECT a.`id` AS id, a.`id_member` as id_member, a.`nama_lengkap` AS nama_lengkap, b.`nama` AS nama_group FROM member_master AS a
INNER JOIN member_group AS b ON a.`id_group` = b.`id` where CONCAT(a.`id_member`,' ',a.`nama_lengkap`,' ',b.`nama`) like'%".$q."%'";
    $data_option = $this->crut->list_datas($q_auto);
    $json = array();
    foreach($data_option as $k => $v){
      $json[] = array('label'=>$v['id_member'].'-'.$v['nama_lengkap'].'-'.$v['nama_group'],'value'=>$v['id']);
    }
    // echo '<pre>';
    // print_r($json);
    $json = json_encode($json);
    echo $json;
  }



}

/* End of file Customergroup.php */
/* Location: ./application/modules/customergroup/controllers/Customergroup.php */
