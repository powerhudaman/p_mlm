{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">
              <div class="btn-group pull-right">
                <a href="{site_url($url_add)}" class="btn btn-primary" title="Tambah Kupon" ><i class="fa fa-plus"></i></a>

              </div>
  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3><br>
                  {if $this->session->flashdata('pesan') !=""}
                    {$this->session->flashdata('pesan')}
                  {/if}
                </div><!-- /.box-header -->
                <!-- form start -->

                  <div class="box-body">

                    <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>

                        <th>ID Member</th>
                        <th>Nama Lengkap</th>
                        <th>Kupon</th>
                        <th>Aksi</th>

                      </tr>
                    </thead>
                    <tbody>
                      <tr>

                        <td colspan="4">Data Tidak Ditemukan</td>

                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>

                        <th>ID Member</th>
                        <th>Nama Lengkap</th>
                        <th>Kupon</th>
                        <th>Aksi</th>

                      </tr>
                    </tfoot>
                  </table>

                  </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div>

  	</div>


{/block}

{block name=script_js}
  {assign var='url' value="kupon/kupon/fetch_data"}
    <script>
      $(function () {
        $("#example1").DataTable({
              "processing": true,
              "serverSide": true,
              "ajax": {
                  "url": "{site_url($url)}",
                  "type": "POST"
              },
              columns: [
                        { data:'id_member' },
                        { data:'nama_lengkap' },
                        { data:'kupon' },
                        { data:'aksi' }
                ],
              "columnDefs":[
                              {
                                 "sTitle":"Nama Group",
                                 "aTargets": [ "nama" ]
                              },
                              {
                                  "aTargets": [ 3 ],
                                  "bSortable": false
                              }
              ]
        });
      });
    </script>

{/block}
