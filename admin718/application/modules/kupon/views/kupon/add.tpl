{extends file='index_admin_.tpl'} {block name=content}

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header">

      </div>
      <div class="box-body">

      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12">
    {form_open(site_url('kupon/save_kupon'))}
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">{$page_header}</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->

      <div class="box-body">
        <div class="form-group">
          {form_error('nama')}
          <label for="">Nama Member</label>
          <input type="text" name="nama" id="nama" class="form-control" value="{set_value('nama')}" required>
        </div>

        <div class="form-group">
          {form_error('nilai_kupon')}
          <label for="">Nilai Kupon Yang Ditambahkan</label>
          <input type="text" name="nilai_kupon" id="nilai_kupon" class="form-control" value="{set_value('nilai_kupon')}" required>
        </div>
      </div>
      <!-- /.box-body -->


    </div>
    <!-- /.box -->


    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
  <!-- accordion -->
  {form_close()}

</div>

</div>

{/block}
{block name=script_js}
  <script>
  $('#nama').autocomplete({
    source:"{site_url('kupon/kupon/autocomplete_member')}"
  });
  </script>
{/block}
