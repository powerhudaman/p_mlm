<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Global_configuration extends MY_Controller {
	protected $code = 'global_configuration';
	protected $seo_word = array('0'=>'Tidak','1'=>'Aktif');
	public function __construct()
	{
		parent::__construct();
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
										$this->load->helper('file_manipulation');
	}

	public function index()
	{
		$this->edit();
	}

	public function edit(){
		$q_setting = "select * from setting where code='".$this->code."'";
		$q_themes = "select * from extensions where type ='template' ";
		$q_customer_group = "select * from member_group where status_delete ='0'";

		$data['page_header'] ='Global Configuration';
		$data['themes'] = $this->crut->list_datas($q_themes);
		$data['customer_group'] = $this->crut->list_datas($q_customer_group);
		$data['seo_word'] = $this->seo_word;
		$data['css_head'] = array('plugins/datepicker/datepicker3.css');
		$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
									'plugins/datepicker/bootstrap-datepicker.js',
									'plugins/ckeditor/ckeditor.js'
			);
		$data['detail'] = array();
		if($this->crut->list_count($q_setting) > 0){
			$configs = $this->crut->list_datas($q_setting);
			$detail  = array();
			foreach ($configs as $key => $value) {
				$detail[$value['key']] = $value['value'];
			}
			$data['detail'] = $detail;
		}
		$this->parser->parse("global_configuration.tpl",$data);
	}

	public function save(){
		$this->form_validation->set_rules('site_name', 'Nama Situs', 'alpha_numeric_spaces',
			array('alpha_numeric_spaces'=>'{field} Hanya Bisa Di Isi Dengan Huruf angka dan spasi')
		);
		$this->form_validation->set_rules('site_status', 'Status Situs', 'numeric',
			array('numeric'=>'{field} Hanya Bisa Di Isi Dengan Angka')
		);
		if ($this->form_validation->run() == FALSE) {
			$this->edit();
		} else {
			// $url_frontend = $this->crut->setting('global_configuration','url_frontend','setting')['value'];

			$q_cek = "select * from setting where code='".$this->code."'";
			if($this->crut->list_count($q_cek) > 0){
				$this->crut->delete(array('code'=>$this->code),'setting');
			}
			$input = array();
			$posts = $_POST;
			foreach($posts as $k => $v){
				$input[] = array(
						'code'=>$this->code,
						'key'=>$k,
						'value'=> $this->input->post($k)
					);
			}
				if(isset($_FILES['logo']) && !empty($_FILES['logo']['name'])){
					$config['upload_path'] = FCPATH.'/assets/img/logo';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']  = '10000';
					$config['max_width']  = '1024';
					$config['max_height']  = '768';
					$config['overwrite'] = true;
					$config['file_name'] ='logo';

					$this->upload->initialize($config);

					if ( ! $this->upload->do_upload('logo')){
						$error = array('error' => $this->upload->display_errors());
					}
					else{
						$data = array('upload_data' => $this->upload->data());
						echo "success";
						$input[] = array(
							'code'=>$this->code,
							'key'=>'logo',
							'value'=> $data['upload_data']['file_name']
						);
					}
				}

				$input[] = array(
						'code'=>$this->code,
						'key'=>'login_attempt',
						'value'=> $this->input->post('login_attempt')
					);

				$pesan = $this->crut->insert_batch('setting',$input,'Simpan Setting Global','Setting Global Berhasil Di Simpan','Setting Global Gagal Di Simpan');
				$this->session->set_flashdata('pesan', pesan($pesan));
				redirect(site_url('global_configuration/edit'));

		}
	}

}

/* End of file Global_configuration.php */
/* Location: ./application/modules/global_configuration/controllers/Global_configuration.php */
