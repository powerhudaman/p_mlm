{extends file='index_admin_.tpl'}
{block name=content}
  	<div class="row">
  		    <div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {form_open_multipart(site_url('global_configuration/save'))}
                  <div class="box-body">
                    {if $this->session->flashdata('pesan') !=""}
                      {$this->session->flashdata('pesan')}
                    {/if}
                    <!-- begin tabs -->

                        <div role="tabpanel">
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                              <a href="#general" aria-controls="home" role="tab" data-toggle="tab">General</a>
                            </li>
                            <li role="presentation">
                              <a href="#email" aria-controls="tab" role="tab" data-toggle="tab">Email</a>
                            </li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="general">

                                <div class="form-group">
                                  {form_error('site_name')}
                                  <label for="">Nama Situs</label>
                                  <input type="text" name="site_name" id="" class="form-control" value="{$detail['site_name']}">
                                </div>
                                <div class="form-group">
                                  <label for="">Logo Situs</label>
                                  <input type="file" name="logo" id="">
                                </div>
                                <div class="form-group">
                                  <label for="">Telepon</label>
                                  <input type="text" name="site_phone" id="" class="form-control" value="{$detail['site_phone']}">
                                </div>
                                <div class="form-group">
                                  {form_error('site_status')}

                                  <label for="">Status Situs</label>
                                  <select name="site_status" id="" class="form-control">
                                    <option value="1" {if $detail['site_status'] == '1'}selected{/if} >Aktif</option>
                                    <option value="2" {if $detail['site_status'] == '2'}selected{/if} >Tidak Aktif</option>
                                  </select>
                                </div>

                                <div class="form-group">
                                  <label for="">Pesan Maintenance</label>
                                  <textarea name="pesan_maintenance" id="pesan_maintenance" cols="30" rows="10" class="form-control">{$detail['pesan_maintenance']}</textarea>
                                </div>

                                <div class="form-group">
                                  <label for="">Tanggal Aktif</label>
                                  <input type="text" name="site_date" id="site_date" class="form-control" value="{$detail['site_date']}">
                                </div>

                                <div class="form-group">
                                  <label for="">Site Meta_Desc</label>
                                  <textarea name="site_meta_desc" id="" cols="30" rows="10" class="form-control">{$detail['site_meta_desc']}</textarea>

                                </div>
                                <div class="form-group">
                                  <label for="">Site Meta Keyword</label>
                                  <textarea name="site_meta_keyword" id="" cols="30" rows="10" class="form-control">{$detail['site_meta_keyword']}</textarea>

                                </div>
                                <div class="form-group">
                                  <label for="">Login Attempt</label>
                                  <input type="text" name="login_attempt" id="" class="form-control" value="{$detail['login_attempt']}">
                                </div>

                                <div class="form-group">
                                  <label for="">Member Group Default</label>
                                  <select name="member_group" id="" class="form-control">
                                    <option value=""></option>
                                    {foreach from=$customer_group key=k item=group}
                                      {assign var=pilih value=''}
                                      {if array_key_exists('member_group',$detail) }
                                        {if $group['id'] == $detail['member_group']}
                                          {$pilih ='selected'}
                                        {/if}
                                      {/if}
                                      <option value="{$group['id']}" {$pilih}>{$group['nama']}</option>
                                    {/foreach}
                                  </select>
                                </div>

                                <div class="form-group">
                                  <label for="">Tema Front End</label>
                                  <select name="themes" id="" class="form-control">
                                    <option value=""></option>
                                    {foreach from=$themes key=k item=theme}
                                      {assign var=pilih value=''}
                                      {if array_key_exists('themes',$detail) }
                                        {if $theme['nama'] == $detail['themes']}
                                          {$pilih ='selected'}
                                        {/if}
                                      {/if}
                                    <option value="{$theme['nama']}" {$pilih}>{$theme['nama']}</option>
                                    {/foreach}
                                  </select>
                                </div>

                                <div class="form-group">
                                  <label for="">Enkripsi Kode</label>
                                  <input type="text" name="enkripsi_kode" id="" value="{$detail['enkripsi_kode']}" class="form-control">
                                </div>

                                <div class="form-group">
                                  <label for="">SEO Word</label>
                                  <select name="seo_word" id="" class="form-control">
                                        {foreach from=$seo_word key=k item=v}
                                          {assign var=pilih value=''}
                                          {if array_key_exists('seo_word',$detail) }
                                            {if $k == $detail['seo_word']}
                                              {$pilih ='selected'}
                                            {/if}
                                          {/if}
                                          <option value="{$k}" {$pilih}>{$v}</option>
                                        {/foreach}
                                  </select>
                                </div>

                                <div class="form-group">
                                  <label for="">URL Frontend</label>
                                  <input type="text" name="url_frontend" id="" class="form-control" value="{$detail['url_frontend']}">
                                </div>

                                <div class="form-group">
                                  <label for="">URL Backend</label>
                                  <input type="text" name="url_backend" id="" class="form-control" value="{$detail['url_backend']}">
                                </div>

                                <div class="form-group">
                                  <label for="">URL Socket</label>
                                  <input type="text" name="url_socket" id="" class="form-control" value="{$detail['url_socket']}">
                                </div>

                            </div><!-- end tab General -->
                            <div role="tabpanel" class="tab-pane" id="email">

                                <div class="form-group">
                                  <label for="">Mail Protocol</label>
                                  <input type="text" name="protocol" id="" value="{$detail['protocol']}" class="form-control">
                                </div>
                                <div class="form-group">
                                  <label for="">Mail Type</label>
                                  <input type="text" name="mailtype" id="" value="{$detail['mailtype']}" class="form-control">
                                </div>
                                <div class="form-group">
                                  <label for="">SMTP Host</label>
                                  <input type="text" name="smtp_host" id="" value="{$detail['smtp_host']}" class="form-control">
                                </div>
                                <div class="form-group">
                                  <label for="">SMTP User</label>
                                  <input type="text" name="smtp_user" id="" value="{$detail['smtp_user']}" class="form-control">
                                </div>
                                <div class="form-group">
                                  <label for="">SMTP Pass</label>
                                  <input type="password" name="smtp_pass" id="" value="{$detail['smtp_pass']}" class="form-control">
                                </div>
                                <div class="form-group">
                                  <label for="">SMTP Port</label>
                                  <input type="text" name="smtp_port" id="" value="{$detail['smtp_port']}" class="form-control">
                                </div>
                                <div class="form-group">
                                  <label for="">Email Sender</label>
                                  <input type="text" name="email_sender" id="" value="{$detail['email_sender']}" class="form-control">
                                </div>
                                <div class="form-group">
                                  <label for="">Nama Sender</label>
                                  <input type="text" name="nama_sender" id="" value="{$detail['nama_sender']}" class="form-control">
                                </div>


                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab">...</div>
                          </div>
                        </div>

                    <!-- end tabs -->
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}
{block name=script_js}
  <script>
  jQuery(document).ready(function($) {
      $('#site_date').datepicker({ format:'yyyy-mm-dd' });
      CKEDITOR.replace('pesan_maintenance');
  });
  </script>
{/block}
