{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">
            <!-- <a href="#" class="btn btn-primary" title="" ><i class="fa fa-refresh"></i></a> -->
  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">

			<div class="box collapsed-box">
              <div class="box-header with-border">
                <h3 class="box-title">{$page_header}</h3>
                {if $this->session->flashdata('pesan') !=""}
                {$this->session->flashdata('pesan')}
              {/if}
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                      {assign var=aturan_form value=['method'=>'get']}
                      {form_open(site_url('komisi/bonus_pasangan/fetch'),$aturan_form)}
                    <div class="form-group">
                        <label for="">Penerima</label>
                        <input type="text" name="penerima" id="penerima" class="form-control">
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label for="">Status Komisi</label>
                        <select name="status_komisi" id="status_komisi" class="form-control">
                          <option value=""></option>
                          <option value="0">Belum Di Approve</option>
                          <option value="1">Sudah Di Approve</option>
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Tanggal Mulai</label>
                        <input type="text" name="tgl_1" id="tgl_1" class="form-control">
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label for="">Tanggal Selesai</label>
                        <input type="text" name="tgl_2" id="tgl_2" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Approve</label>
                        <input type="text" name="tgl_3" id="tgl_3" class="form-control">
                    </div>
                    <!-- /.form-group -->
                  </div>


                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <input type="submit" value="Cari" class="btn btn-success">
              </div>
              </div>
              {form_close()}


              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">

                  <a href="{site_url('komisi/bonus_pasangan/export')}" class="btn btn-primary pull-left">Export</a>
                </div><!-- /.box-header -->
                <!-- form start -->

                  <div class="box-body">

                    <div class="row">
                      <div class="col-md-12">
                        <div class="table-responsive">
                          <table id="" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Penerima</th>
                                <th>ID Penerima</th>
                                <th>Rekening</th>
                                <th>Nama Rekening</th>
                                <th>Jumlah Pasangan</th>
                                <th>Bonus Pasangan</th>
                                 <th>Biaya Admin (10%)</th>
                                <th>status Terima</th>
                                <th>Tgl Pasangan</th>
                                <th>Tgl Approve</th>
                                <th>Aksi</th>
                              </tr>
                            </thead>
                            <tbody>
                              {if array_key_exists('pesan',$list_bonus)}
                                  <tr>
                                    <td colspan="7">Data Bonus Pasangan Tidak Ditemukan</td>
                                  </tr>
                              {/if}
                              {if !array_key_exists('pesan',$list_bonus)}
                                {assign var=no value=$offset}
                                {foreach from=$list_bonus key=k item=v}

                                  {assign var=no value=$no+1}
                                  <tr>
                                    <td>{$no}</td>
                                    <td>{$v['penerima']}</td>
                                    <td>{$v['id_penerima']}</td>
                                    <td>{$v['nama_bank']} - {$v['no_rek']}</td>
                                    <td>{$v['an_bank']}</td>
                                    <td>{$v['jumlah_pasangan']}</td>
                                    <td>{number_format($v['total_komisi'],0,",",".")}</td>
                                    <td>{number_format($v['total_komisi'] -($v['total_komisi']*10)/100,0,",",".")}</td>
                                    <td>{$v['status_terima']}</td>
                                    <td>{$v['tgl']}</td>
                                    <td>{$v['tgl_approve']}</td>
                                    <td>
                                      {if $v['kode_approve'] == 0}
                                          <a href="{site_url('komisi/bonus_pasangan/approve_komisi')}/{$v['id']}" class="btn btn-primary">Approve Komisi</a>
                                      {/if}
                                    </td>
                                  </tr>

                                {/foreach}
                              {/if}
                            </tbody>
                          </table>
                        </div>

                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        {$pagination}
                      </div>
                    </div>
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div>

  	</div>


{/block}

{block name=script_js}
  <script>

    $('#penerima').autocomplete({
      source:"{site_url('komisi/bonus_sponsor/autocomplete_member')}",
      delay: 1000
    });
    $('#produk_id').autocomplete({
      source:"{site_url('cart_catalog/produk_edit/autocomplete_produk_id')}",
      delay: 1000
    });
    $('#judul_produk').autocomplete({
      source:"{site_url('cart_catalog/produk_edit/autocomplete_judul')}",
      delay: 1000
    });
    $("#tgl_1").datepicker({ format:'yyyy-mm-dd' });
    $("#tgl_2").datepicker({ format:'yyyy-mm-dd' });
    $("#tgl_3").datepicker({ format:'yyyy-mm-dd' });

  </script>

{/block}
{block name=sidebar-menu}
   {$side_menu}
{/block}
