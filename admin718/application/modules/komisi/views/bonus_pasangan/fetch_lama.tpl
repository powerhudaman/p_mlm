{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">
            <!-- <a href="#" class="btn btn-primary" title="" ><i class="fa fa-refresh"></i></a> -->
  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">{$page_header}</h3>
                {if $this->session->flashdata('pesan') !=""}
                {$this->session->flashdata('pesan')}
              {/if}
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                      {assign var=aturan_form value=['method'=>'get']}
                      {form_open(site_url('komisi/bonus_pasangan/fetch_lama'),$aturan_form)}
                    <div class="form-group">
                        <label for="">Penerima</label>
                        <input type="text" name="penerima" id="penerima" class="form-control">
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label for="">Status Komisi</label>
                        <select name="status_komisi" id="status_komisi" class="form-control">
                          <option value=""></option>
                          <option value="0">Belum Di Approve</option>
                          <option value="1">Sudah Di Approve</option>
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Tanggal Mulai</label>
                        <input type="text" name="tgl_1" id="tgl_1" class="form-control">
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label for="">Tanggal Selesai</label>
                        <input type="text" name="tgl_2" id="tgl_2" class="form-control">
                    </div>
                    <!-- /.form-group -->
                  </div>


                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <input type="submit" value="Cari" class="btn btn-success">
              </div>
              </div>
              {form_close()}


              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">

                  <a href="{site_url('komisi/bonus_pasangan/export')}" class="btn btn-primary pull-left">Export</a>
                </div><!-- /.box-header -->
                <!-- form start -->

                  <div class="box-body">

                    <div class="row">
                      <div class="col-md-12">
                        <div class="table-responsive">
                          <table id="" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>Kode</th>
                                <th>Nama Penerima</th>
                                <th>Kode Downline 1</th>
                                <th>Nama Downline 1</th>
                                <th>Kode Downline 2</th>
                                <th>Nama Downline 2</th>
                                <th>Nilai Komisi</th>
                                <th>Status terima</th>
                                <th>Tanggal</th>
                                <th>Aksi</th>
                              </tr>
                            </thead>
                            <tbody>
                              {if array_key_exists('pesan',$list_bonus)}
                                  <tr>
                                    <td colspan="8">Data Log Aktivitas Tidak Ditemukan</td>
                                  </tr>
                              {/if}
                              {if !array_key_exists('pesan',$list_bonus)}
                              {foreach from=$list_bonus key=k item=v}
                              <tr>
                                <td>{$v['kode']}</td>
                                <td>{$v['penerima']}</td>
                                <td>{$v['ip1']}</td>
                                <td>{$v['in1']}</td>
                                <td>{$v['ip2']}</td>
                                <td>{$v['in2']}</td>
                                <td>

                                    {if $v['komisi'] == ""}
                                      {number_format($komisi_bonus_pasangan,0,",",".")}
                                    {/if}
                                    {if $v['komisi'] !=""}
                                      {number_format($v['komisi'],0,",",".")}
                                    {/if}
                                </td>
                                <td>{$v['status_terima']}</td>
                                <td>{$v['tgl']}</td>
                                <td>
                                  {if $v['status_approve'] == 0}
                                      <a href="{site_url('komisi/bonus_pasangan/approve_komisi_lama')}/{$v['id']}" class="btn btn-primary">Approve Komisi</a>
                                  {/if}
                                </td>
                              </tr>
                              {/foreach}
                              {/if}
                            </tbody>
                          </table>
                        </div>

                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        {$pagination}
                      </div>
                    </div>
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div>

  	</div>


{/block}

{block name=script_js}
  <script>

    $('#penerima').autocomplete({
      source:"{site_url('komisi/bonus_sponsor/autocomplete_member')}",
      delay: 1000
    });
    $('#produk_id').autocomplete({
      source:"{site_url('cart_catalog/produk_edit/autocomplete_produk_id')}",
      delay: 1000
    });
    $('#judul_produk').autocomplete({
      source:"{site_url('cart_catalog/produk_edit/autocomplete_judul')}",
      delay: 1000
    });
    $("#tgl_1").datepicker({ format:'yyyy-mm-dd' });
    $("#tgl_2").datepicker({ format:'yyyy-mm-dd' });

  </script>

{/block}
{block name=sidebar-menu}
   {$side_menu}
{/block}
