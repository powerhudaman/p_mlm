{extends file='index_admin_.tpl'}
{block name=content}
  	<div class="row">
  		    <div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {form_open_multipart(site_url('komisi/setting_bonus/save'))}
                  <div class="box-body">
                    {if $this->session->flashdata('pesan') !=""}
                      {$this->session->flashdata('pesan')}
                    {/if}
                    <!-- begin tabs -->

                        <div role="tabpanel">
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                              <a href="#general" aria-controls="home" role="tab" data-toggle="tab">Komisi</a>
                            </li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="general">

                                <div class="form-group">
                                  {form_error('bonus_pasangan')}
                                  <label for="">Komisi Bonus Pasangan</label>
                                  <input type="number" name="bonus_pasangan" id="" class="form-control" value="{$detail['bonus_pasangan']}">
                                </div>

                                <div class="form-group">
                                  {form_error('bonus_sponsor')}
                                  <label for="">Komisi Bonus Sponsor</label>
                                  <input type="number" name="bonus_sponsor" id="" class="form-control" value="{$detail['bonus_sponsor']}">
                                </div>

                                <div class="form-group">
                                  {form_error('bonus_cabang')}
                                  <label for="">Komisi Bonus Cabang</label>
                                  <input type="number" name="bonus_cabang" id="" class="form-control" value="{$detail['bonus_cabang']}">
                                </div>

                            </div><!-- end tab General -->
                          </div>
                        </div>

                    <!-- end tabs -->
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}
{block name=script_js}
  <script>
  jQuery(document).ready(function($) {
      $('#site_date').datepicker({ format:'yyyy-mm-dd' });
      CKEDITOR.replace('pesan_maintenance');
  });
  </script>
{/block}
