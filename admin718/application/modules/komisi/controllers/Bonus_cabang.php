<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class bonus_cabang extends MY_Controller {
	protected $approval = array('1'=>'Tidak Approval','2'=>'Approval');
	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');
  protected $component_akses = array();
	protected $bonus_sponsor = 0;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->cek_hak_akses($this->privileges['component']['bonus_cabang'],'1|2');
		$this->bonus_sponsor = $this->crut->setting('komisi_configuration','bonus_cabang','setting')['value'];


	}

	public function fetch($offset = "0"){
    		// $this->cek_hak_akses($this->privileges['log_aktivitas']['log_aktivitas'],'1|2');
        	$a = $this->uri->segment(4);
            $offset = ($a<>'')?$a:0;
    		$filter = "";
    		$status_komisi = $this->input->get('status_komisi',true);
    		$id_penerima = $this->input->get('penerima',true);
    		// $keyword = $this->input->get('keyword',true);
    		// $ip_user = $this->input->get('ip_user',true);
    		// $user_id = $this->input->get('user_id',true);
    		$tgl_1 = $this->input->get('tgl_1',true);
    		$tgl_2 = $this->input->get('tgl_2',true);
				$this->session->set_userdata('status_komisi', $status_komisi);
				$this->session->set_userdata('penerima', $id_penerima );
				$this->session->set_userdata('tgl_1', $tgl_1);
				$this->session->set_userdata('tgl_2', $tgl_2);

    		if($status_komisi !=""){
    			if(empty($filter)){
    				$filter =" where a.approve ='".$status_komisi."'";
    			}else{
    				$filter .=" and a.approve ='".$status_komisi."'";
    			}
    		}

    		if(!empty($id_penerima)){
    			if(empty($filter)){
						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter =" where a.id_member ='".$id_penerima."'";
    			}else{
						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter .=" and a.id_member ='".$id_penerima."'";
    			}
    		}

    		if(!empty($keyword)){
    			if(empty($filter)){
    				$filter =" where a.data like '%".$keyword."%'";
    			}else{
    				$filter .=" and a.data like '%".$keyword."%'";
    			}
    		}

    		if(!empty($ip_user)){
    			if(empty($filter)){
    				$filter =" where a.ip_user ='".$ip_user."'";
    			}else{
    				$filter .=" and a.ip_user ='".$ip_user."'";
    			}
    		}

    		if(!empty($user_id)){
    			if(empty($filter)){
    				$filter =" where CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
    			}else{
    				$filter .=" and CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
    			}
    		}

    		if(!empty($tgl_1) && !empty($tgl_2)){
    			if(empty($filter)){
    				$filter =" where date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    			}else{
    				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    			}
    		}



  		 $q_bonus_sponsor = "SELECT
									a.id AS id,
									b.`nama_lengkap` AS nama_penerima,
								b.`id_member` AS id_penerima,
								b.no_rek as no_rek,
									b.nama_bank as nama_bank,
								b.atas_nama as an_bank,
									c.`id_member` AS id_member,
									c.`nama_lengkap` AS nama_member,
									DATE_FORMAT(a.created_date, '%Y-%M-%d')AS tgl,
									a.`approve` AS status_approve,
									CASE
								WHEN a.approve = '1' THEN
									'Sudah Di Approve'
								WHEN a.approve = '0' THEN
									'Belum Di Approve'
								END AS status_terima,
								a.komisi AS komisi
								FROM
									bonus_cabang AS a
								INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
								INNER JOIN member_master AS c ON a.`id_aktivasi` = c.`id_member` ".$filter." order by a.approve asc , a.created_date DESC";

       $q_bonus_sponsor_count = "SELECT count(a.id) AS total
             FROM bonus_cabang AS a
             INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
             INNER JOIN member_master AS c ON a.`id_aktivasi` = c.`id_member` ".$filter;
   	// 	$d_log = $this->crut->list_data($q_log);


  		// echo $q_bonus_sponsor;
  		// die();

  		$this->load->library('pagination');

  		$config['base_url'] = site_url('komisi/bonus_cabang/fetch');

  		$config['per_page'] = 10;
  		$config['uri_segment'] = 4;
  		$config['num_links'] = 3;
  		$config['full_tag_open'] = '<ul class="pagination">';
  		$config['full_tag_close'] = '</ul>';
  		$config['first_link'] = 'First';
  		$config['first_tag_open'] = '<li>';
  		$config['first_tag_close'] = '</li>';
  		$config['last_link'] = 'Last';
  		$config['last_tag_open'] = '<li>';
  		$config['last_tag_close'] = '</li>';
  		$config['next_link'] = '&gt;';
  		$config['next_tag_open'] = '<li>';
  		$config['next_tag_close'] = '</li>';
  		$config['prev_link'] = '&lt;';
  		$config['prev_tag_open'] = '<li>';
  		$config['prev_tag_close'] = '</li>';
  		$config['cur_tag_open'] = '<li class="active"><a>';
  		$config['cur_tag_close'] = '</a></li>';
  		$config['num_tag_open'] = '<li>';
		  $config['num_tag_close'] = '</li>';
		  $config['reuse_query_string'] = TRUE;

  		$list_bonus = $this->crut->list_data($q_bonus_sponsor,$config['per_page'],$offset);

  		$config['total_rows'] = $this->crut->list_row($q_bonus_sponsor_count)['total'];

  		$this->pagination->initialize($config);
        $data['offset'] =  $offset ;
  		$data['page_header'] ='Data Bonus Cabang';
  		$data['url_admin'] = ADMINS;
  		$data['url_add'] = '';
  		$data['list_bonus'] = $list_bonus;
			$data['komisi_bonus_sponsor'] = $this->bonus_sponsor;

  		$data['pagination'] = $this->pagination->create_links();

  		$data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
  															'plugins/select2/select2.min.css',
  															'plugins/datepicker/datepicker3.css'
  												);
  		$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
  																'plugins/select2/select2.min.js',
  																'plugins/datepicker/bootstrap-datepicker.js'
  												);

  		$this->parser->parse("bonus_cabang/fetch.tpl",$data);
		}

		public function approve_komisi($id){
			$this->cek_hak_akses($this->privileges['component']['bonus_cabang'],'2');
			$q_cek = "select id as id,id_member, id_aktivasi, date_format(created_date,'%Y-%m-%d') as tgl_filter from bonus_cabang where id ='".$id."' and approve ='0'";
			$d_cek = $this->crut->list_row($q_cek);
			if($d_cek !=0){
				$filter = array('id'=>$d_cek['id']);
				$update['approve'] = '1';
				$sukses = "Proses Approve Komisi Dengan Member ".$d_cek['id_member'].' Berhasil';
				$gagal = "Proses Approve Komisi Dengan Member ".$d_cek['id_member'].' Gagal';
				$response = $this->crut->update($filter,'bonus_cabang',$update,'',$sukses,$gagal);
				if($response['status'] && $response['kode'] == "1"){
					/// kirim sms approve bonus cabang
					$cek_s = "select * from member_master where id_member = '".$d_cek['id_member']."';";
					$d_cek_s = $this->crut->list_row($cek_s);
					if($d_cek_s !=0){

						// $pesan = "Yth. Bpk/Ibu Tedi Kurnia (CBM0000004), Komisi pasangan tgl.24/08/2017 Rp. 100,000,-telah kami transfer ke BCA Anda - PT CBM';
						$pesan_sms = 'Yth. Bpk/Ibu '.$d_cek_s['nama_lengkap'].' ('.$d_cek_s['id_member'].'), Komisi Cabang tgl.'.$d_cek['tgl'].' Rp. '.number_format($this->bonus_sponsor,0,",",".").' -telah kami transfer ke '.$d_cek_s['nama_bank'].' Anda - PT CBM';
						$respon_sms = sms_zensiva($pesan_sms,$telepon=$d_cek_s['no_hp']);

						// simpan psean sms ke log
						$input_sms_reg['id_member'] = $d_cek_s['id_member'];
						$input_sms_reg['type_sms'] = 6;
						$input_sms_reg['judul_pesan'] = 'SMS Approval Bonus Cabang '.$d_cek_s['id_member'];
						$input_sms_reg['isi_pesan'] = $pesan_sms;
						$input_sms_reg['no_tujuan'] = $d_cek_s['no_hp'];
						$input_sms_reg['status_terkirim'] = $respon_sms;
						$input_sms_reg['created_date'] = date('Y-m-d H:i:S');
						$this->crut->insert('sms_log',$input_sms_reg,'','','');
						// simpan psean sms ke
					}
					/// kirim sms approve bonus cabang

					// input cashflow
					$keterangan = $d_cek_s['nama_lengkap'].' - '.$d_cek_s['id_member'].',  Terima kasih';
					$input_cashflow['kode_transaksi'] = 'BCB'.genRndString(5,'1234567890');
					$input_cashflow['tp'] = '2';
					$input_cashflow['jenis_transaksi'] = 'BCB';
					$input_cashflow['debet'] = $this->bonus_sponsor;
					$input_cashflow['keterangan'] = $keterangan;
					$input_cashflow['created_date'] = date('Y-m-d H:i:s');
					$input_cashflow['created_by'] = $this->session->userdata('user_id');
					$this->crut->insert('cashflow',$input_cashflow,'','','');
					// input cashflow

					/// simpan notifikasi member
					$input_notifikasi['id_member'] = $d_cek['id_member'];
					$input_notifikasi['pesan_notif'] = "Selamat anda telah mendapatkan bonus cabang dari id member = ".$d_cek['id_aktivasi'];
					$input_notifikasi['type_notif'] = '2';
					$input_notifikasi['status_notif'] = '0';
					$input_notifikasi['url_notif'] = 'cbm/komisi/bonus_cabang/fetch';
					$input_notifikasi['created_date'] = date('Y-m-d H:i:S');
					$this->crut->insert('notifikasi_member',$input_notifikasi,'','','');
					/// simpan notifikasi member

					/// simpan notifikasi admin
					$input_notifikasi_admin['id_member'] = $d_cek['id_member'];
					$input_notifikasi_admin['pesan_notif'] = $d_cek['id_member']." Telah Mendapatkan Bonus Cabang dari aktivasi ".$d_cek['id_aktivasi'];
					$input_notifikasi_admin['type_notif'] = '2';
					$input_notifikasi_admin['status_notif'] = '0';
					$input_notifikasi_admin['url_notif'] = 'komisi/bonus_cabang/fetch?penerima='.$d_cek['id_member'].'&status_komisi=1&tgl_1='.$d_cek['tgl_filter'].'&tgl_2='.$d_cek['tgl_filter'];
					$input_notifikasi_admin['created_date'] = date('Y-m-d H:i:S');
					$this->crut->insert('notifikasi_admin',$input_notifikasi_admin,'','','');
					/// simpan notifikasi admin

				}
				//insert log
				$dt = array('filter'=>$filter,'input'=>$update);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'BonusCabang',
					'jenis_operasi'=>'Update',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log

				//insert log_2
				$insert_log = array(
					'id_admin'=>$this->session->userdata('user_id'),
					'id_member'=>$d_cek['id_member'],
					'halaman'=>'BonusCabang',
					'aksi'=>4,// approve komisi
					'created_date'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas_2',$insert_log,'','','');
				//insert log_2
				$this->session->set_flashdata('pesan',pesan($response));
				redirect(site_url('komisi/bonus_cabang/fetch'));
			}else{
				$response = array('status'=>false,'kode'=>2,'pesan'=>'Komisi Tidak Ditemukan');
				$this->session->set_flashdata('pesan',pesan($response));
				redirect(site_url('komisi/bonus_cabang/fetch'));
			}
		}



		public function autocomplete_member($q =''){
			$q = $this->input->get('term');
			$q_auto = "select id_member,id,nama_lengkap from member_master where concat(id_member,' ',nama_lengkap) like'%".$q."%'";
			$data_option = $this->crut->list_datas($q_auto);
			$json = array();
			foreach($data_option as $k => $v){
				$json[] = array('label'=>$v['id_member'].'-'.$v['nama_lengkap'],'value'=>$v['id_member'].'-'.$v['nama_lengkap']);
			}
			// echo '<pre>';
			// print_r($json);
			$json = json_encode($json);
			echo $json;
		}

		public function export()
		{
			$this->load->library('Excel_generator');
			$filter = "";
    		$status_komisi = $this->session->userdata('status_komisi');
    		$id_penerima = $this->session->userdata('penerima');
    		$tgl_1 = $this->session->userdata('tgl_1');
    		$tgl_2 = $this->session->userdata('tgl_2');

    		if($status_komisi !=""){
    			if(empty($filter)){
    				$filter =" where a.approve ='".$status_komisi."'";
    			}else{
    				$filter .=" and a.approve ='".$status_komisi."'";
    			}
    		}

    		if(!empty($id_penerima)){
    			if(empty($filter)){
						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter =" where a.id_member ='".$id_penerima."'";
    			}else{
						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter .=" and a.id_member ='".$id_penerima."'";
    			}
    		}

    		if(!empty($keyword)){
    			if(empty($filter)){
    				$filter =" where a.data like '%".$keyword."%'";
    			}else{
    				$filter .=" and a.data like '%".$keyword."%'";
    			}
    		}



    		if(!empty($tgl_1) && !empty($tgl_2)){
    			if(empty($filter)){
    				$filter =" where date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    			}else{
    				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    			}
    		}



  		 $query = "SELECT
			a.id AS id,
			b.`nama_lengkap` AS nama_penerima,
		b.`id_member` AS id_penerima,
		b.no_rek as no_rek,
			b.nama_bank as nama_bank,
		b.atas_nama as an_bank,
			c.`id_member` AS id_member,
			c.`nama_lengkap` AS nama_member,
			DATE_FORMAT(a.created_date, '%Y-%M-%d')AS tgl,
			a.`approve` AS status_approve,
			CASE
		WHEN a.approve = '1' THEN
			'Sudah Di Approve'
		WHEN a.approve = '0' THEN
			'Belum Di Approve'
		END AS status_terima,
		a.komisi AS komisi
		FROM
			bonus_cabang AS a
		INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
		INNER JOIN member_master AS c ON a.`id_aktivasi` = c.`id_member` ".$filter ." order by a.approve asc , a.created_date DESC";

			$result = $this->db->query($query);

			$this->excel_generator->set_query($result);
			$this->excel_generator->set_header(array('ID ','PENERIMA','ID PENERIMA','REKENING','BANK','NAMA REKENING','BONUS CABANG','ID MEMBER','NAMA MEMBER','STATUS TERIMA','TANGGAL APPROVE'));
			$this->excel_generator->set_column(array('id','nama_penerima','id_penerima','no_rek','nama_bank','an_bank','komisi','id_member','nama_member','status_terima','tgl'));
			$this->excel_generator->set_width(array( 10,20, 30, 20, 20,20,20,20,20,20,20));
			$this->excel_generator->exportTo2003("Bonus Cabang");


			}


}

/* End of file Customergroup.php */
/* Location: ./application/modules/customergroup/controllers/Customergroup.php */
