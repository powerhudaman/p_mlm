<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Isi_cashflow extends MY_Controller {

	private $list_member_statement_kiri = array();
	private $list_member_statement_kanan = array();
	private $listing_downline_data = array();
	private $no_level_downline = 0;
	private $bonus_sponsor = 0;
	private $bonus_pasangan = 0;
	private $bonus_cabang= 0;

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');

		$this->load->helper('security');
		// $this->cek_hak_akses($this->privileges['component']['member_master'],'1|2');
		$this->load->library('datatables');
		$this->bonus_sponsor = $this->crut->setting('komisi_configuration','bonus_sponsor','setting')['value'];
		$this->bonus_pasangan = $this->crut->setting('komisi_configuration','bonus_pasangan','setting')['value'];
		$this->bonus_cabang = $this->crut->setting('komisi_configuration','bonus_cabang','setting')['value'];
	}
	public function aktivasi_member(){
		$q_member = "select * from member_master where aktivasi='2'";
		$d_member = $this->crut->list_datas($q_member);
		if($d_member !=0){
			foreach ($d_member as $k => $v) {
        echo $v['created_date'].'<br>';
				// input cashflow biaya_admin
				$keterangan = ' Pemasukan Registrasi Member '.$v['id_member'].' nama '.$v['nama_lengkap'];
				$baps_cashflow['kode_transaksi'] = 'RMB'.genRndString(5,'1234567890');
				$baps_cashflow['tp'] = '1';
				$baps_cashflow['jenis_transaksi'] = 'RMB'; // biaya admin pasangan
				$baps_cashflow['kredit'] = '900000';
				$baps_cashflow['keterangan'] = $keterangan;
				$baps_cashflow['created_date'] = $v['created_date'];
				$baps_cashflow['created_by'] = $this->session->userdata('user_id');
				$this->crut->insert('cashflow',$baps_cashflow,'','','');
				// input cashflow biaya_admin
			}

		}
	}
  public function bonus_sponsor(){
    $q_bonus_sponsor = "SELECT    a.id AS id,
                             a.`kode` AS kode,
                             b.`nama_lengkap` AS nama_penerima,
                             b.id_member AS id_penerima,
                             b.no_rek AS no_rek,
                             b.nama_bank AS nama_bank,
                             b.atas_nama AS an_bank,
                             a.komisi AS komisi,
                             c.`id_member` AS kode_downline,
                             c.`nama_lengkap` AS nama_downline,
                             DATE_FORMAT(a.created_date, '%Y-%M-%d')AS tgl,
                             a.`status_approve` AS status_approve,
                             CASE
                               WHEN a.status_approve = '1' THEN
                                 'Sudah Di Approve'
                               WHEN a.status_approve = '0' THEN
                                 'Belum Di Approve'
                               END AS status_terima,
                             a.status_auto_save,
                             a.created_date
                       FROM bonus_sponsor AS a
                 INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
                 INNER JOIN member_master AS c ON a.`id_downline` = c.`id_member` order by a.status_approve ASC;";
    $d_bonus_sponsor = $this->crut->list_datas($q_bonus_sponsor);
    if($d_bonus_sponsor !=0){
      foreach ($d_bonus_sponsor as $k => $v) {
        // input cashflow
        $keterangan = $v['nama_penerima'].' - '.$v['id_penerima'].', telah berhasil mensponsori Sdr/i '.$v['nama_downline'].' -'.$v['kode_downline'].' Terima kasih';
        $input_cashflow['kode_transaksi'] = 'BMS'.genRndString(5,'1234567890');
        $input_cashflow['tp'] = '2';
        $input_cashflow['jenis_transaksi'] = 'BMS';
        $input_cashflow['debet'] = $v['komisi'];
        $input_cashflow['keterangan'] = $keterangan;
        $input_cashflow['created_date'] = $v['created_date'];
        $input_cashflow['created_by'] = $this->session->userdata('user_id');
        $this->crut->insert('cashflow',$input_cashflow,'','','');
        // input cashflow

        if($v['status_approve'] == "1"){
          // input cashflow biaya_admin
          $keterangan = ' Pemasukan Biaya Admin Bonus Sponsor '.$v['id_penerima'];
          $income_cashflow['kode_transaksi'] = 'BAS'.genRndString(5,'1234567890');
          $income_cashflow['tp'] = '1';
          $income_cashflow['jenis_transaksi'] = 'BAS'; // biaya admin sponsor
          $income_cashflow['kredit'] = ((10/100) * $this->bonus_sponsor);
          $income_cashflow['keterangan'] = $keterangan;
          $income_cashflow['created_date'] = $v['created_date'];
          $income_cashflow['created_by'] = $this->session->userdata('user_id');
          $this->crut->insert('cashflow',$income_cashflow,'','','');
          // input cashflow biaya_admin
        }
      }
    }
  }

  public function bonus_pasangan(){
    $q_bonus_pasangan = "SELECT	a.id as id,
                         b.nama_lengkap as penerima,
                         a.id_member as id_penerima,
                         b.no_rek as no_rek,
                         b.nama_bank as nama_bank,
                         b.atas_nama as an_bank,
                         DATE_FORMAT(a.tgl, '%Y-%m-%d')AS tgl,
                         a.t_pasangan AS jumlah_pasangan,
                         a.komisi AS total_komisi,
                         CASE
                           WHEN a.status_approve = '1' THEN
                             'Sudah di Approve'
                           WHEN a.status_approve = '0' THEN
                             'Belum di Approve'
                           END AS status_terima,
                           a.tgl_approve,
                           a.status_approve AS kode_approve,
                           a.updated_date,
                           a.created_date
                   FROM	 bonus_pasangan_2 a
                INNER JOIN  member_master AS b ON a.`id_member` = b.`id_member`
                 WHERE	!isnull(a.id) and a.t_pasangan > 0  order by a.status_approve ASC ";
    $d_bonus_pasangan = $this->crut->list_datas($q_bonus_pasangan);
    if($d_bonus_pasangan !=0){
      foreach ($d_bonus_pasangan as $k => $v) {
          // input cashflow
  				$keterangan = $v['penerima'].' - '.$v['id_penerima'].', telah berhasil mendapatkan bonus pasangan  Terima kasih';
  				$input_cashflow['kode_transaksi'] = $v['id'];
  				$input_cashflow['tp'] = '2';
  				$input_cashflow['jenis_transaksi'] = 'BPS';
  				$input_cashflow['debet'] = $v['total_komisi'];
  				$input_cashflow['keterangan'] = $keterangan;
  				$input_cashflow['created_date'] = $v['created_date'];
  				$input_cashflow['created_by'] = $this->session->userdata('user_id');
  				$this->crut->insert('cashflow',$input_cashflow,'','','');
  				// input cashflow

          if($v['jumlah_pasangan'] > 12 && $v['kode_approve'] == 1){
            // input cashflow
						$keterangan = ' Pemasukan Flashout dari '.$v['id_penerima'];
						$fl_cashflow['kode_transaksi'] = 'FLS'.genRndString(5,'1234567890');
						$fl_cashflow['tp'] = '1';
						$fl_cashflow['jenis_transaksi'] = 'FLS'; // kode flashout
						$fl_cashflow['kredit'] = (($v['jumlah_pasangan'] - 12) * $this->bonus_pasangan);
						$fl_cashflow['keterangan'] = $keterangan;
						$fl_cashflow['created_date'] = $v['created_date'];
						$fl_cashflow['created_by'] = $this->session->userdata('user_id');
						$this->crut->insert('cashflow',$fl_cashflow,'','','');
						// input cashflow
          }
          if($v['kode_approve'] == 1){
            // input cashflow biaya_admin
  					$keterangan = ' Pemasukan Biaya Admin Bonus Pasangan dari '.$v['id_penerima'];
  					$baps_cashflow['kode_transaksi'] = 'BAPS'.genRndString(5,'1234567890');
  					$baps_cashflow['tp'] = '1';
  					$baps_cashflow['jenis_transaksi'] = 'BAPS'; // biaya admin pasangan
  					$baps_cashflow['kredit'] = ((10/100) * $v['total_komisi']);
  					$baps_cashflow['keterangan'] = $keterangan;
  					$baps_cashflow['created_date'] = $v['created_date'];
  					$baps_cashflow['created_by'] = $this->session->userdata('user_id');
  					$this->crut->insert('cashflow',$baps_cashflow,'','','');
  					// input cashflow biaya_admin
          }
      }
    }
  }

  public function bonus_cabang(){
    $q_bonus_cabang = "SELECT
               a.id AS id,
               b.`nama_lengkap` AS nama_penerima,
             b.`id_member` AS id_penerima,
             b.no_rek as no_rek,
               b.nama_bank as nama_bank,
             b.atas_nama as an_bank,
               c.`id_member` AS id_member,
               c.`nama_lengkap` AS nama_member,
               DATE_FORMAT(a.created_date, '%Y-%M-%d')AS tgl,
               a.`approve` AS status_approve,
               CASE
             WHEN a.approve = '1' THEN
               'Sudah Di Approve'
             WHEN a.approve = '0' THEN
               'Belum Di Approve'
             END AS status_terima,
             a.komisi AS komisi,
             a.created_date
             FROM
               bonus_cabang AS a
             INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
             INNER JOIN member_master AS c ON a.`id_aktivasi` = c.`id_member` order by a.approve asc";
    $d_bonus_cabang = $this->crut->list_datas($q_bonus_cabang);
    if($d_bonus_cabang !=0){
      foreach ($d_bonus_cabang as $k => $v) {
        // input cashflow
        $keterangan = $v['nama_penerima'].' - '.$v['id_penerima'].', telah berhasil meregistrasi Sdr/i '.$v['nama_member'].' -'.$v['id_member'].' Terima kasih';
        $input_cashflow['kode_transaksi'] = 'BCB'.genRndString(5,'1234567890');
        $input_cashflow['tp'] = '2';
        $input_cashflow['jenis_transaksi'] = 'BCB';
        $input_cashflow['debet'] = $v['komisi'];
        $input_cashflow['keterangan'] = $keterangan;
        $input_cashflow['created_date'] = $v['created_date'];
        $input_cashflow['created_by'] = $this->session->userdata('user_id');
        $this->crut->insert('cashflow',$input_cashflow,'','','');
        // input cashflow
      }
    }
  }

  public function bonus_reward(){
    $q_bonus_reward = "SELECT a.id AS id, a.`id_member` AS id_member, b.`nama_lengkap` AS nama_penerima, a.`t_level` AS `level`, c.`t_kiri` AS t_kiri,
                       c.`t_kanan` AS t_kanan, a.`t_reward` AS reward,a.komisi as komisi, DATE_FORMAT(a.`created_date`,'%Y-%m-%d') AS tgl,
                       CASE
                         WHEN a.status_approve = '1' THEN
                           'Sudah di Approve'
                         WHEN a.status_approve = '0' THEN
                           'Belum di Approve'
                         END AS status_terima,
                         a.status_approve as status_approve,
                         a.created_date
                        FROM bonus_reward AS a
                       INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
                       INNER JOIN bonus_pasangan_2 AS c ON a.`id_member` = c.`id_member` AND a.`t_pasangan` = c.`t_pasangan` ";

    $d_bonus_reward = $this->crut->list_datas($q_bonus_reward);
    if($d_bonus_reward !=0){
      foreach ($d_bonus_reward as $k => $v) {
        // input cashflow
        $keterangan = $v['nama_penerima'].' - '.$v['id_member'].', telah mendapat bonus reward level '.$v['level'].' Terima kasih';
        $input_cashflow['kode_transaksi'] = 'BBR'.genRndString(5,'1234567890');
        $input_cashflow['tp'] = '2';
        $input_cashflow['jenis_transaksi'] = 'BBR';
        $input_cashflow['debet'] = $v['komisi'];
        $input_cashflow['keterangan'] = $keterangan;
        $input_cashflow['created_date'] = $v['created_date'];
        $input_cashflow['created_by'] = $this->session->userdata('user_id');
        $this->crut->insert('cashflow',$input_cashflow,'','','');
        // input cashflow
      }
    }
  }

}
