<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class bonus_pasangan extends MY_Controller {
	protected $approval = array('1'=>'Tidak Approval','2'=>'Approval');
	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');
  protected $component_akses = array();
	protected $bonus_pasangan = 0;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->cek_hak_akses($this->privileges['component']['bonus_pasangan'],'1|2');
		$this->bonus_pasangan = $this->crut->setting('komisi_configuration','bonus_pasangan','setting')['value'];

	}

	public function fetch($offset = "0"){
    		// $this->cek_hak_akses($this->privileges['log_aktivitas']['log_aktivitas'],'1|2');
    	    $a = $this->uri->segment(4);
            $offset = ($a<>'')?$a:0;
    		$filter = "";
    		$status_komisi = $this->input->get('status_komisi',true);
    		$id_penerima = $this->input->get('penerima',true);
    		// $keyword = $this->input->get('keyword',true);
    		// $ip_user = $this->input->get('ip_user',true);
    		// $user_id = $this->input->get('user_id',true);
    		$tgl_1 = $this->input->get('tgl_1',true);
				$tgl_2 = $this->input->get('tgl_2',true);
    		$tgl_3 = $this->input->get('tgl_3',true);
			$this->session->set_userdata('status_komisi', $status_komisi);
			$this->session->set_userdata('penerima', $id_penerima );
			$this->session->set_userdata('tgl_1', $tgl_1);
			$this->session->set_userdata('tgl_2', $tgl_2);
			$this->session->set_userdata('tgl_3', $tgl_3);
    		if($status_komisi !=""){
    				$filter .=" and a.status_approve ='".$status_komisi."'";
    		}

    		if(!empty($id_penerima)){

						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter .=" and a.id_member ='".$id_penerima."'";

    		}


    		if(!empty($tgl_1) && !empty($tgl_2)){
    				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    		}

				if(!empty($tgl_3)){
    				$filter .=" and date_format(a.tgl_approve,'%Y-%m-%d') = '".$tgl_3."' ";
    		}

  		 $q_bonus_sponsor = "SELECT	a.id as id,
						b.nama_lengkap as penerima,
						a.id_member as id_penerima,
						b.no_rek as no_rek,
						b.nama_bank as nama_bank,
						b.atas_nama as an_bank,
						DATE_FORMAT(a.tgl, '%Y-%m-%d')AS tgl,
						a.t_pasangan AS jumlah_pasangan,
						a.komisi AS total_komisi,
						CASE
							WHEN a.status_approve = '1' THEN
								'Sudah di Approve'
							WHEN a.status_approve = '0' THEN
								'Belum di Approve'
							END AS status_terima,
							a.tgl_approve,
							a.status_approve AS kode_approve
			FROM	 bonus_pasangan_2 a
	INNER JOIN  member_master AS b ON a.`id_member` = b.`id_member`
		WHERE	!isnull(a.id) and a.t_pasangan > 0 ".$filter." order by a.status_approve asc , a.created_date DESC";

       $q_bonus_sponsor_count = "SELECT count(a.id) as total FROM bonus_pasangan_2 a
	   INNER JOIN  member_master AS b ON a.`id_member` = b.`id_member`
		   WHERE	!isnull(a.id) and a.t_pasangan > 0  ".$filter." order by a.status_approve asc , a.created_date DESC";
				   	// 	$d_log = $this->crut->list_data($q_log);


  		// echo $q_bonus_sponsor;
  		// die();

  		$this->load->library('pagination');

  		$config['base_url'] = site_url('komisi/bonus_pasangan/fetch/');
  		$config['total_rows'] = $this->crut->list_row($q_bonus_sponsor_count)['total'];

  		$config['per_page'] = 10;
  		$config['uri_segment'] = 4;
  		$config['num_links'] = 3;
  		$config['full_tag_open'] = '<ul class="pagination">';
  		$config['full_tag_close'] = '</ul>';
  		$config['first_link'] = 'First';
  		$config['first_tag_open'] = '<li>';
  		$config['first_tag_close'] = '</li>';
  		$config['last_link'] = 'Last';
  		$config['last_tag_open'] = '<li>';
  		$config['last_tag_close'] = '</li>';
  		$config['next_link'] = '&gt;';
  		$config['next_tag_open'] = '<li>';
  		$config['next_tag_close'] = '</li>';
  		$config['prev_link'] = '&lt;';
  		$config['prev_tag_open'] = '<li>';
  		$config['prev_tag_close'] = '</li>';
  		$config['cur_tag_open'] = '<li class="active"><a>';
  		$config['cur_tag_close'] = '</a></li>';
  		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['reuse_query_string'] = TRUE;

		  $list_bonus = $this->crut->list_data($q_bonus_sponsor,$config['per_page'],$offset);
		//   echo'<pre>';
		//   print_r($list_bonus);
		//   die();


  		$this->pagination->initialize($config);
        $data['offset'] =  $offset ;
  		$data['page_header'] ='Data Bonus Pasangan';
  		$data['url_admin'] = ADMINS;
  		$data['url_add'] = '';
  		$data['list_bonus'] = $list_bonus;
  		$data['pagination'] = $this->pagination->create_links();
		$data['komisi_bonus_pasangan'] = $this->bonus_pasangan;

  		$data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
  															'plugins/select2/select2.min.css',
  															'plugins/datepicker/datepicker3.css'
  												);
  		$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
  																'plugins/select2/select2.min.js',
  																'plugins/datepicker/bootstrap-datepicker.js'
  												);

  		$this->parser->parse("bonus_pasangan/fetch.tpl",$data);
		}

		public function approve_komisi($id){
			$this->cek_hak_akses($this->privileges['component']['bonus_pasangan'],'2');

			$q_cek = "SELECT id,id_member,DATE_FORMAT(tgl,'%d/%m/%Y') AS tgl,DATE_FORMAT(tgl,'%Y-%m-%d') AS tgl_filter, t_pasangan AS jumlah_pasangan, komisi AS total_komisi,
CASE WHEN status_approve ='1' THEN 'Sudah di Approve' WHEN status_approve ='0' THEN 'Belum di Approve' END AS status_approve
,tgl_approve FROM bonus_pasangan_2 where id ='".$id."' and status_approve ='0' ";
			$d_cek = $this->crut->list_row($q_cek);
			if($d_cek !=0){

				$filter = array('id'=>$d_cek['id']);
				$update['status_approve'] = '1';
				$update['tgl_approve'] = date('Y-m-d');
				$sukses = "Proses Approve Komisi Dengan ID ".$d_cek['id'].' Berhasil';
				$gagal = "Proses Approve Komisi Dengan ID ".$d_cek['id'].' Gagal';
				$response = $this->crut->update($filter,'bonus_pasangan_2',$update,'',$sukses,$gagal);
				if($response['status'] && $response['kode'] == "1"){

					if($d_cek['jumlah_pasangan'] > 12){
						// input cashflow
						$keterangan = ' Pemasukan Flashout';
						$input_cashflow['kode_transaksi'] = microtime();
						$input_cashflow['tp'] = '1';
						$input_cashflow['jenis_transaksi'] = 'FLS'; // kode flashout
						$input_cashflow['kredit'] = (($d_cek['jumlah_pasangan'] - 12) * $this->bonus_pasangan);
						$input_cashflow['keterangan'] = $keterangan;
						$input_cashflow['created_date'] = date('Y-m-d H:i:s');
						$input_cashflow['created_by'] = $this->session->userdata('user_id');
						$this->crut->insert('cashflow',$input_cashflow,'','','');
						// input cashflow
					}
					// input cashflow biaya_admin
					$keterangan = ' Pemasukan Biaya Admin Bonus Pasangan';
					$income_cashflow['kode_transaksi'] = microtime();
					$income_cashflow['tp'] = '1';
					$income_cashflow['jenis_transaksi'] = 'BAPS'; // biaya admin pasangan
					$income_cashflow['kredit'] = ((10/100) * $d_cek['total_komisi']);
					$income_cashflow['keterangan'] = $keterangan;
					$income_cashflow['created_date'] = date('Y-m-d H:i:s');
					$income_cashflow['created_by'] = $this->session->userdata('user_id');
					$this->crut->insert('cashflow',$income_cashflow,'','','');
					// input cashflow biaya_admin

					/// kirim sms approve bonus pasangan
					$cek_s = "select * from member_master where id_member = '".$d_cek['id_member']."';";
					$d_cek_s = $this->crut->list_row($cek_s);
					if($d_cek_s !=0){

						// $pesan = "Yth. Bpk/Ibu Tedi Kurnia (CBM0000004), Komisi pasangan tgl.24/08/2017 Rp. 100,000,-telah kami transfer ke BCA Anda - PT CBM';
						$pesan_sms = 'Yth. Bpk/Ibu '.$d_cek_s['nama_lengkap'].' ('.$d_cek_s['id_member'].'), Komisi pasangan tgl.'.$d_cek['tgl'].' Rp. '.number_format($this->bonus_pasangan,0,",",".").' -telah kami transfer ke '.$d_cek_s['nama_bank'].' Anda - PT CBM';
						$respon_sms = sms_zensiva($pesan_sms,$telepon=$d_cek_s['no_hp']);

							// simpan psean sms ke log
							$input_sms_reg['id_member'] = $d_cek_s['id_member'];
							$input_sms_reg['type_sms'] = 4;
							$input_sms_reg['judul_pesan'] = 'SMS Approval Bonus Pasangan '.$d_cek_s['id_member'];
							$input_sms_reg['isi_pesan'] = $pesan_sms;
							$input_sms_reg['no_tujuan'] = $d_cek_s['no_hp'];
							$input_sms_reg['status_terkirim'] = $respon_sms;
							$input_sms_reg['created_date'] = date('Y-m-d H:i:S');
							$this->crut->insert('sms_log',$input_sms_reg,'','','');
							// simpan psean sms ke log

					}
					/// kirim sms approve bonus pasangan

					// input bonus reward
					$this->input_bonus_reward($d_cek['id_member']);
					// input bonus reward

					/// simpan notifikasi member
					$input_notifikasi['id_member'] = $d_cek['id_member'];
					$input_notifikasi['pesan_notif'] = "Selamat anda telah mendapatkan bonus pasangan";
					$input_notifikasi['type_notif'] = '1';
					$input_notifikasi['status_notif'] = '0';
					$input_notifikasi['url_notif'] = 'cbm/komisi/bonus_pasangan/fetch';
					$input_notifikasi['created_date'] = date('Y-m-d H:i:S');
					$this->crut->insert('notifikasi_member',$input_notifikasi,'','','');
					/// simpan notifikasi member

					/// simpan notifikasi admin
					$input_notifikasi_admin['id_member'] = $d_cek['id_member'];
					$input_notifikasi_admin['pesan_notif'] = $d_cek['id_member']." Telah Mendapatkan Bonus Pasangan dengan total pasangan ".$d_cek['jumlah_pasangan'];
					$input_notifikasi_admin['type_notif'] = '1';
					$input_notifikasi_admin['status_notif'] = '0';
					$input_notifikasi_admin['url_notif'] = 'komisi/bonus_pasangan/fetch?penerima='.$d_cek['id_member'].'&status_komisi=1&tgl_1='.$d_cek['tgl_filter'].'&tgl_2='.$d_cek['tgl_filter'];
					$input_notifikasi_admin['created_date'] = date('Y-m-d H:i:S');
					$this->crut->insert('notifikasi_admin',$input_notifikasi_admin,'','','');
					/// simpan notifikasi admin

					//insert log
								$dt = array('filter'=>$filter,'input'=>$update);
								$insert_log = array(
									'kode_log'=>microtime(),
									'halaman'=>'BonusPasangan',
									'jenis_operasi'=>'Update',
									'data'=>json_encode($dt),
									'ip_user'=>$_SERVER['REMOTE_ADDR'],
									'user_id'=>$this->session->userdata('user_id'),
									'date_created'=>date('Y-m-d H:i:s')
								);
								$this->crut->insert('log_aktivitas',$insert_log,'','','');
								//insert log
								//insert log_2
								$insert_log = array(
									'id_admin'=>$this->session->userdata('user_id'),
									'id_member'=>$d_cek['id_member'],
									'halaman'=>'BonusPasangan',
									'aksi'=>4,// approve komisi
									'created_date'=>date('Y-m-d H:i:s')
								);
								$this->crut->insert('log_aktivitas_2',$insert_log,'','','');
								//insert log_2
								$this->session->set_flashdata('pesan',pesan($response));
								redirect(site_url('komisi/bonus_pasangan/fetch'));

			}else{
				$response = array('status'=>false,'kode'=>2,'pesan'=>'Komisi Tidak Ditemukan');
				$this->session->set_flashdata('pesan',pesan($response));
				redirect(site_url('komisi/bonus_pasangan/fetch'));
			}
		}

	}

	public function input_bonus_reward($id_member,$n_pasangan = ''){
		$level_reward = array(
			'10'=>array('pasangan'=>10,'bonus_fisik'=>'hp android','komisi'=>'1000000','level'=>1),
			'40'=>array('pasangan'=>40,'bonus_fisik'=>'laptop','komisi'=>'3000000','level'=>2),
			'150'=>array('pasangan'=>150,'bonus_fisik'=>'honda beat','komisi'=>'15000000','level'=>3),
			'350'=>array('pasangan'=>350,'bonus_fisik'=>'umroh','komisi'=>'30000000','level'=>4),
			'750'=>array('pasangan'=>750,'bonus_fisik'=>'kawasaki ninja','komisi'=>'60000000','level'=>5),
			'1500'=>array('pasangan'=>1500,'bonus_fisik'=>'honda brio','komisi'=>'120000000','level'=>6),
			'3000'=>array('pasangan'=>3000,'bonus_fisik'=>'honda jazz','komisi'=>'300000000','level'=>7),
			'7500'=>array('pasangan'=>7500,'bonus_fisik'=>'fortuner','komisi'=>'500000000','level'=>8),
			'15000'=>array('pasangan'=>15000,'bonus_fisik'=>'rumah mewah','komisi'=>'1000000000','level'=>9)
		);

		/*
		ALTER TABLE `bonus_reward`
  ADD COLUMN `t_level` TINYINT(2) NULL AFTER `t_pasangan`;
	ALTER TABLE `bonus_reward`
  CHANGE `t_reward` `t_reward` VARCHAR(50) NULL;
		*/

		$q_t_pasangan = "SELECT SUM(t_pasangan) AS total_pasangan FROM bonus_pasangan_2 WHERE id_member ='".$id_member."' AND status_approve ='1'
GROUP BY id_member;";
		$d_t_pasangan = $this->crut->list_row($q_t_pasangan);
		$t_pasangan = 0;
		if($d_t_pasangan !=0){
			$t_pasangan = $d_t_pasangan['total_pasangan'];
			$d_bonus = array(); // data ini nanti akan digunakan untuk menampung data level reward
			if($n_pasangan !=""){
				$t_pasangan = $n_pasangan;
			}
			if($t_pasangan >= 10 && $t_pasangan < 40){ // level 1
				$d_bonus = $level_reward[10];
			}// level 1
			if($t_pasangan >= 40 && $t_pasangan < 150){ // level 2
				$d_bonus = $level_reward[40];
			}// level 2
			if($t_pasangan >= 150 && $t_pasangan < 350){ // level 3
				$d_bonus = $level_reward[150];
			}// level 3
			if($t_pasangan >= 350 && $t_pasangan < 750){ // level 4
				$d_bonus = $level_reward[350];
			}// level 4
			if($t_pasangan >= 750 && $t_pasangan < 1500){ // level 5
				$d_bonus = $level_reward[750];
			}// level 5
			if($t_pasangan >= 1500 && $t_pasangan < 3000){ // level 6
				$d_bonus = $level_reward[1500];
			}// level 6
			if($t_pasangan >= 3000 && $t_pasangan < 7500){ // level 7
				$d_bonus = $level_reward[3000];
			}// level 7
			if($t_pasangan >= 7500 && $t_pasangan < 15000){ // level 8
				$d_bonus = $level_reward[7500];
			}// level 8
			if($t_pasangan >= 15000 ){ // level 9
				$d_bonus = $level_reward[15000];
			}// level 9

			// cek reward apakah sudah pernah masuk atau belum
			if(count($d_bonus) > 0){
				$q_cek_reward = "select * from bonus_reward where id_member ='".$id_member."' and t_level = '".$d_bonus['level']."' ";
				$d_cek_reward = $this->crut->list_row($q_cek_reward);
				if($d_cek_reward == 0){ // bila reward tidak ditemukan maka akan di insert
					$bonus_reward['id_member'] = $id_member;
					$bonus_reward['t_pasangan'] = $t_pasangan;
					$bonus_reward['t_level'] = $d_bonus['level'];
					$bonus_reward['t_reward'] = $d_bonus['bonus_fisik'];
					$bonus_reward['komisi'] = $d_bonus['komisi'];
					$bonus_reward['created_date'] = date('Y-m-d H:i:s');
					$bonus_reward['created_by'] = $this->session->userdata('user_id');

					// echo '<pre>';
					// print_r($bonus_reward);
					// die();

					$response_reward = $this->crut->insert('bonus_reward',$bonus_reward,'','','');
					if($response_reward['kode'] == 1){
						//insert log
						$dt = array('input'=>$bonus_reward);
						$insert_log = array(
							'kode_log'=>microtime(),
							'halaman'=>'BonusReward',
							'jenis_operasi'=>'Insert',
							'data'=>json_encode($dt),
							'ip_user'=>$_SERVER['REMOTE_ADDR'],
							'user_id'=>$this->session->userdata('user_id'),
							'date_created'=>date('Y-m-d H:i:s')
						);
						$this->crut->insert('log_aktivitas',$insert_log,'','','');
						//insert log
					}

				}// bila reward tidak ditemukan maka akan di insert
			}

			// cek reward apakah sudah pernah masuk atau belum

		}


	}
	public function fetch_lama($offset = "0"){
    		// $this->cek_hak_akses($this->privileges['log_aktivitas']['log_aktivitas'],'1|2');
    		// $offset = $this->uri->segment(4);
    		$filter = "";
    		$status_komisi = $this->input->get('status_komisi',true);
    		$id_penerima = $this->input->get('penerima',true);
    		// $keyword = $this->input->get('keyword',true);
    		// $ip_user = $this->input->get('ip_user',true);
    		// $user_id = $this->input->get('user_id',true);
    		$tgl_1 = $this->input->get('tgl_1',true);
    		$tgl_2 = $this->input->get('tgl_2',true);
			$this->session->set_userdata('status_komisi', $status_komisi);
			$this->session->set_userdata('penerima', $id_penerima );
			$this->session->set_userdata('tgl_1', $tgl_1);
			$this->session->set_userdata('tgl_2', $tgl_2);
    		if(!empty($status_komisi)){

    				$filter .=" and a.status_approve ='".$status_komisi."'";

    		}

    		if(!empty($id_penerima)){

						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter .=" and a.id_penerima ='".$id_penerima."'";

    		}


    		if(!empty($tgl_1) && !empty($tgl_2)){
    				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    		}



  		 $q_bonus_sponsor = "SELECT a.`id` as id, a.`kode` AS kode, b.`nama_lengkap` AS penerima, a.`id_pasangan_1` AS ip1, a.`id_pasangan_2` AS ip2,
				p1.`nama_lengkap` AS in1, p2.`nama_lengkap` AS in2,a.status_approve AS status_approve,
				CASE WHEN a.status_approve ='1' THEN 'Sudah di Approve' WHEN a.status_approve ='0' THEN 'Belum di Approve' END status_terima,
				DATE_FORMAT(a.created_date,'%Y-%M-%d') AS tgl, a.komisi as komisi
				FROM bonus_pasangan AS a
				INNER JOIN member_master AS b ON a.`id_penerima` = b.`id_member`
				INNER JOIN member_master AS p1 ON a.`id_pasangan_1` = p1.`id_member`
				INNER JOIN member_master AS p2 ON a.`id_pasangan_2` = p2.`id_member`
				WHERE a.`status_view` = '1' AND !ISNULL(a.`id_pasangan_1`) AND !ISNULL(a.`id_pasangan_2`) ".$filter;

       $q_bonus_sponsor_count = "SELECT count(a.id) as total
				FROM bonus_pasangan AS a
				INNER JOIN member_master AS b ON a.`id_penerima` = b.`id_member`
				INNER JOIN member_master AS p1 ON a.`id_pasangan_1` = p1.`id_member`
				INNER JOIN member_master AS p2 ON a.`id_pasangan_2` = p2.`id_member`
				WHERE a.`status_view` = '1' AND !ISNULL(a.`id_pasangan_1`) AND !ISNULL(a.`id_pasangan_2`) ".$filter;
				   	// 	$d_log = $this->crut->list_data($q_log);


  		// echo $q_bonus_sponsor;
  		// die();

  		$this->load->library('pagination');

  		$config['base_url'] = site_url('komisi/bonus_pasangan/fetch_lama');

  		$config['per_page'] = 50;
  		$config['uri_segment'] = 3;
  		$config['num_links'] = 3;
  		$config['full_tag_open'] = '<ul class="pagination">';
  		$config['full_tag_close'] = '</ul>';
  		$config['first_link'] = 'First';
  		$config['first_tag_open'] = '<li>';
  		$config['first_tag_close'] = '</li>';
  		$config['last_link'] = 'Last';
  		$config['last_tag_open'] = '<li>';
  		$config['last_tag_close'] = '</li>';
  		$config['next_link'] = '&gt;';
  		$config['next_tag_open'] = '<li>';
  		$config['next_tag_close'] = '</li>';
  		$config['prev_link'] = '&lt;';
  		$config['prev_tag_open'] = '<li>';
  		$config['prev_tag_close'] = '</li>';
  		$config['cur_tag_open'] = '<li class="active"><a>';
  		$config['cur_tag_close'] = '</a></li>';
  		$config['num_tag_open'] = '<li>';
  		$config['num_tag_close'] = '</li>';

  		$list_bonus = $this->crut->list_data($q_bonus_sponsor,$config['per_page'],$offset);

  		$config['total_rows'] = $this->crut->list_row($q_bonus_sponsor_count)['total'];

  		$this->pagination->initialize($config);

  		$data['page_header'] ='Data Bonus Pasangan';
  		$data['url_admin'] = ADMINS;
  		$data['url_add'] = '';
  		$data['list_bonus'] = $list_bonus;
  		$data['pagination'] = $this->pagination->create_links();
			$data['komisi_bonus_pasangan'] = $this->bonus_pasangan;

  		$data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
  															'plugins/select2/select2.min.css',
  															'plugins/datepicker/datepicker3.css'
  												);
  		$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
  																'plugins/select2/select2.min.js',
  																'plugins/datepicker/bootstrap-datepicker.js'
  												);

  		$this->parser->parse("bonus_pasangan/fetch_lama.tpl",$data);
		}

		public function approve_komisi_lama($id){
			$this->cek_hak_akses($this->privileges['component']['bonus_pasangan'],'2');

			$q_cek = "select id as id,kode as kode,id_penerima,tgl, id_pasangan_1,id_pasangan_2 from bonus_pasangan where id ='".$id."' and status_approve ='0' and status_view ='1' ";
			$d_cek = $this->crut->list_row($q_cek);
			if($d_cek !=0){
				// echo $d_cek['id_penerima'];
				// die();

				$q_cek_2 = "SELECT id_penerima,COUNT(id) AS total FROM bonus_pasangan WHERE id_penerima='".$d_cek['id_penerima']."' and tgl ='".$d_cek['tgl']."' GROUP BY id_penerima,tgl;";
				$d_cek_2 = $this->crut->list_row($q_cek_2);
				if($d_cek_2['total'] < 13){ // cek apakah datanya kurang dari 13
						$filter = array('id'=>$d_cek['id']);
						$update['status_approve'] = '1';
						$sukses = "Proses Approve Komisi Dengan Nama ".$d_cek['kode'].' Berhasil';
						$gagal = "Proses Approve Komisi Dengan Nama ".$d_cek['kode'].' Gagal';
						$response = $this->crut->update($filter,'bonus_pasangan',$update,'',$sukses,$gagal);
						if($response['status'] && $response['kode'] == "1"){
							/// kirim sms approve bonus pasangan
							$cek_s = "select * from member_master where id_member = '".$d_cek['id_penerima']."';";
							$d_cek_s = $this->crut->list_row($cek_s);
							if($d_cek_s !=0){

								// $pesan = "Yth. Bpk/Ibu Tedi Kurnia (CBM0000004), Komisi pasangan tgl.24/08/2017 Rp. 100,000,-telah kami transfer ke BCA Anda - PT CBM';
								$pesan_sms = 'Yth. Bpk/Ibu '.$d_cek_s['nama_lengkap'].' ('.$d_cek_s['id_member'].'), Komisi pasangan tgl.'.$d_cek['tgl'].' Rp. '.number_format($this->bonus_pasangan,0,",",".").' -telah kami transfer ke '.$d_cek_s['nama_bank'].' Anda - PT CBM';
								sms_zensiva($pesan_sms,$telepon=$d_cek_s['no_hp']);
							}
							/// kirim sms approve bonus pasangan

							/// simpan notifikasi member
							$input_notifikasi['id_member'] = $d_cek['id_penerima'];
							$input_notifikasi['pesan_notif'] = "Selamat anda telah mendapatkan bonus pasangan dari id member kiri = ".$d_cek['id_pasangan_1']." dan id member kanan = ".$d_cek['id_pasangan_2'];
							$input_notifikasi['type_notif'] = '1';
							$input_notifikasi['status_notif'] = '0';
							$input_notifikasi['url_notif'] = 'cbm/komisi/bonus_pasangan/fetch';
							$input_notifikasi['created_date'] = date('Y-m-d H:i:S');
							$this->crut->insert('notifikasi_member',$input_notifikasi,'','','');
							/// simpan notifikasi member

						}
						//insert log
						$dt = array('filter'=>$filter,'input'=>$update);
						$insert_log = array(
							'kode_log'=>microtime(),
							'halaman'=>'BonusPasangan',
							'jenis_operasi'=>'Update',
							'data'=>json_encode($dt),
							'ip_user'=>$_SERVER['REMOTE_ADDR'],
							'user_id'=>$this->session->userdata('user_id'),
							'date_created'=>date('Y-m-d H:i:s')
						);
						$this->crut->insert('log_aktivitas',$insert_log,'','','');
						//insert log
						$this->session->set_flashdata('pesan',pesan($response));
						redirect(site_url('komisi/bonus_pasangan/fetch_lama'));
				}else{
					$response = array('status'=>false,'kode'=>2,'pesan'=>'Untuk Bonus Pasangan Telah melebihi Limit Maka Bonus Pasangan Akan Dipindahkan Ke Pemasukan Perusahaan');
					$this->session->set_flashdata('pesan',pesan($response));
					redirect(site_url('komisi/bonus_pasangan/fetch_lama'));
				}// cek apakah datanya kurang dari 13
			}else{
				$response = array('status'=>false,'kode'=>2,'pesan'=>'Komisi Tidak Ditemukan');
				$this->session->set_flashdata('pesan',pesan($response));
				redirect(site_url('komisi/bonus_pasangan/fetch_lama'));
			}
		}



		public function autocomplete_member($q =''){
			$q = $this->input->get('term');
			$q_auto = "select id_member,id,nama_lengkap from member_master where concat(id_member,' ',nama_lengkap) like'%".$q."%'";
			$data_option = $this->crut->list_datas($q_auto);
			$json = array();
			foreach($data_option as $k => $v){
				$json[] = array('label'=>$v['id_member'].'-'.$v['nama_lengkap'],'value'=>$v['id_member'].'-'.$v['nama_lengkap']);
			}
			// echo '<pre>';
			// print_r($json);
			$json = json_encode($json);
			echo $json;
		}
		public function export()
		{
			$this->load->library('Excel_generator');
			$filter = "";
    		$status_komisi = $this->session->userdata('status_komisi');
    		$id_penerima = $this->session->userdata('id_member');
    		$tgl_1 = $this->session->userdata('tgl_1');
				$tgl_2 = $this->session->userdata('tgl_2');
    		$tgl_3 = $this->session->userdata('tgl_3');

    		if($status_komisi !=""){
    			$filter .=" and a.status_approve ='".$status_komisi."'";
    		}
    		if(!empty($id_penerima)){
				$pecah = explode('-',$id_penerima.'-');
				$id_penerima = $pecah[0];
    			$filter .=" and a.id_member ='".$id_penerima."'";
    		}
    		if(!empty($tgl_1) && !empty($tgl_2)){
    				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    		}
				if(!empty($tgl_3)){
    				$filter .=" and date_format(a.tgl_approve,'%Y-%m-%d') = '".$tgl_3."' ";
    		}
								$query = "SELECT	a.id as id,
																b.nama_lengkap as penerima,
																a.id_member as id_penerima,
																CONCAT('`',b.no_rek) as no_rek,
															    b.nama_bank as nama_bank,
																b.atas_nama as an_bank,
																DATE_FORMAT(a.tgl, '%d/%m/%Y')AS tgl,
																a.t_pasangan AS jumlah_pasangan,
																a.komisi AS total_komisi,
																(a.komisi-(a.komisi/100)*10) AS persen,
																CASE
																	WHEN a.status_approve = '1' THEN
																		'Sudah di Approve'
																	WHEN a.status_approve = '0' THEN
																		'Belum di Approve'
																	END AS status_terima,
																 a.tgl_approve,
																 a.status_approve AS kode_approve
													FROM	 bonus_pasangan_2 a
										  INNER JOIN  member_master AS b ON a.`id_member` = b.`id_member`
												WHERE	! isnull(a.id) and a.komisi > 0 and a.t_pasangan > 0   ".$filter." order by a.status_approve asc , a.created_date DESC";


			$result = $this->db->query($query);

			$this->excel_generator->set_query($result);
			$this->excel_generator->set_header(array('ID ','PENERIMA','ID PENERIMA','REKENING','BANK','NAMA REKENING','JUMLAH PASANGAN','TOTAL KOMISI','BIAYA ADMIN','STATUS TERIMA','TGL PASANGAN','TGL APPROVE'));
			$this->excel_generator->set_column(array('id','penerima','id_penerima','no_rek','nama_bank','an_bank','jumlah_pasangan','total_komisi','persen','status_terima','tgl','tgl_approve'));
			$this->excel_generator->set_width(array( 10, 20,20, 20, 20, 20,20,20,20,20,20,20));
			$this->excel_generator->exportTo2003("Bonus Pasangan");


			}


}

/* End of file Customergroup.php */
/* Location: ./application/modules/customergroup/controllers/Customergroup.php */
