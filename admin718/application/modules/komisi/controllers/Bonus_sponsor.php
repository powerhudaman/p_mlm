<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class bonus_sponsor extends MY_Controller {
	protected $approval = array('1'=>'Tidak Approval','2'=>'Approval');
	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');
  protected $component_akses = array();
	protected $bonus_sponsor = 0;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->cek_hak_akses($this->privileges['component']['bonus_sponsor'],'1|2');
		$this->bonus_sponsor = $this->crut->setting('komisi_configuration','bonus_sponsor','setting')['value'];


	}

	public function fetch($offset = "0"){
    		// $this->cek_hak_akses($this->privileges['log_aktivitas']['log_aktivitas'],'1|2');
    		// $offset = $this->uri->segment(4);
    		$a = $this->uri->segment(4);
            $offset = ($a<>'')?$a:0;
    		$filter = "";
    		$status_komisi = $this->input->get('status_komisi',true);
    		$id_penerima = $this->input->get('penerima',true);
    		// $keyword = $this->input->get('keyword',true);
    		// $ip_user = $this->input->get('ip_user',true);
    		// $user_id = $this->input->get('user_id',true);
    		$tgl_1 = $this->input->get('tgl_1',true);
    		$tgl_2 = $this->input->get('tgl_2',true);
				$status_auto_save = $this->input->get('status_auto_save',true);
			$this->session->set_userdata('status_komisi', $status_komisi);
			$this->session->set_userdata('penerima', $id_penerima );
			$this->session->set_userdata('tgl_1', $tgl_1);
			$this->session->set_userdata('tgl_2', $tgl_2);
			$this->session->set_userdata('status_auto_save', $status_auto_save);

    		if($status_komisi !=""){
    			if(empty($filter)){
    				$filter =" where a.status_approve ='".$status_komisi."'";
    			}else{
    				$filter .=" and a.status_approve ='".$status_komisi."'";
    			}
    		}

    		if(!empty($id_penerima)){
    			if(empty($filter)){
						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter =" where a.id_member ='".$id_penerima."'";
    			}else{
						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter .=" and a.id_member ='".$id_penerima."'";
    			}
    		}

    		if(!empty($keyword)){
    			if(empty($filter)){
    				$filter =" where a.data like '%".$keyword."%'";
    			}else{
    				$filter .=" and a.data like '%".$keyword."%'";
    			}
    		}

    		if(!empty($ip_user)){
    			if(empty($filter)){
    				$filter =" where a.ip_user ='".$ip_user."'";
    			}else{
    				$filter .=" and a.ip_user ='".$ip_user."'";
    			}
    		}

    		if(!empty($user_id)){
    			if(empty($filter)){
    				$filter =" where CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
    			}else{
    				$filter .=" and CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
    			}
    		}

    		if(!empty($tgl_1) && !empty($tgl_2)){
    			if(empty($filter)){
    				$filter =" where date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    			}else{
    				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    			}
    		}


				if(!empty($status_auto_save)){
    			if(empty($filter)){
    				$filter =" where status_auto_save = '".$status_auto_save."' ";
    			}else{
    				$filter .=" and status_auto_save = '".$status_auto_save."' ";
    			}
    		}


  		 $q_bonus_sponsor = "SELECT    a.id AS id,
																a.`kode` AS kode,
																b.`nama_lengkap` AS nama_penerima,
																b.id_member AS id_penerima,
																b.no_rek AS no_rek,
																b.nama_bank AS nama_bank,
																b.atas_nama AS an_bank,
																a.komisi AS komisi,
																c.`id_member` AS kode_downline,
																c.`nama_lengkap` AS nama_downline,
																DATE_FORMAT(a.created_date, '%Y-%M-%d')AS tgl,
																a.`status_approve` AS status_approve,
																CASE
																	WHEN a.status_approve = '1' THEN
																		'Sudah Di Approve'
																	WHEN a.status_approve = '0' THEN
																		'Belum Di Approve'
																	END AS status_terima,
																a.status_auto_save
												  FROM bonus_sponsor AS a
										INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
										INNER JOIN member_master AS c ON a.`id_downline` = c.`id_member` ".$filter." order by a.status_approve asc , a.created_date DESC";

       $q_bonus_sponsor_count = "SELECT count(a.id) AS total
             FROM bonus_sponsor AS a
             INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
             INNER JOIN member_master AS c ON a.`id_downline` = c.`id_member` ".$filter;
   	// 	$d_log = $this->crut->list_data($q_log);


  		// echo $q_bonus_sponsor;
  		// die();

  		$this->load->library('pagination');

  		$config['base_url'] = site_url('komisi/bonus_sponsor/fetch');

  		$config['per_page'] = 10;
  		$config['uri_segment'] = 4;
  		$config['num_links'] = 3;
  		$config['full_tag_open'] = '<ul class="pagination">';
  		$config['full_tag_close'] = '</ul>';
  		$config['first_link'] = 'First';
  		$config['first_tag_open'] = '<li>';
  		$config['first_tag_close'] = '</li>';
  		$config['last_link'] = 'Last';
  		$config['last_tag_open'] = '<li>';
  		$config['last_tag_close'] = '</li>';
  		$config['next_link'] = '&gt;';
  		$config['next_tag_open'] = '<li>';
  		$config['next_tag_close'] = '</li>';
  		$config['prev_link'] = '&lt;';
  		$config['prev_tag_open'] = '<li>';
  		$config['prev_tag_close'] = '</li>';
  		$config['cur_tag_open'] = '<li class="active"><a>';
  		$config['cur_tag_close'] = '</a></li>';
  		$config['num_tag_open'] = '<li>';
		  $config['num_tag_close'] = '</li>';
		  $config['reuse_query_string'] = TRUE;

  		$list_bonus = $this->crut->list_data($q_bonus_sponsor,$config['per_page'],$offset);

  		$config['total_rows'] = $this->crut->list_row($q_bonus_sponsor_count)['total'];

  		$this->pagination->initialize($config);
        $data['offset'] =  $offset ;
  		$data['page_header'] ='Data Bonus Sponsor';
  		$data['url_admin'] = ADMINS;
  		$data['url_add'] = '';
  		$data['list_bonus'] = $list_bonus;
			$data['komisi_bonus_sponsor'] = $this->bonus_sponsor;

  		$data['pagination'] = $this->pagination->create_links();

  		$data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
  															'plugins/select2/select2.min.css',
  															'plugins/datepicker/datepicker3.css'
  												);
  		$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
  																'plugins/select2/select2.min.js',
  																'plugins/datepicker/bootstrap-datepicker.js'
  												);

  		$this->parser->parse("bonus_sponsor/fetch.tpl",$data);
		}

		public function approve_komisi($id){
			$this->cek_hak_akses($this->privileges['component']['bonus_sponsor'],'2');
			$q_cek = "select id as id,kode as kode,id_member as id_member,id_downline as id_downline,date_format(created_date,'%d/%m/%Y') as tgl,date_format(created_date,'%Y-%m-%d') as tgl_filter from bonus_sponsor where id ='".$id."' and status_approve ='0'";
			$d_cek = $this->crut->list_row($q_cek);
			if($d_cek !=0){
				$filter = array('id'=>$d_cek['id']);
				$update['status_approve'] = '1';

				// $q_s = "select count(*) as total from bonus_sponsor where id_member = '".$d_cek['id_member']."' group by id_member ";
				// $d_s = $this->crut->list_row($q_s);

				$q_sc = "SELECT * FROM bonus_sponsor WHERE id_member ='".$d_cek['id_member']."' AND status_auto_save ='1';"; // cek apakah sudah memilik autosave atau belum
				$d_sc = $this->crut->list_row($q_sc);
				if($d_sc == 0){
					$q_sd = "SELECT * FROM bonus_sponsor WHERE id_member ='".$d_cek['id_member']."' ORDER BY created_date ASC LIMIT 1;"; /// cek bonus sponsor pertama kali
					$d_sd = $this->crut->list_row($q_sd);
					if($d_sd !=0){
						$update_auto_save['status_auto_save'] = '1';
						$filter_auto_save = array('id'=>$d_sd['id']);
						$this->crut->update($filter_auto_save,'bonus_sponsor',$update_auto_save,'','','');
					}
				}
				$update['komisi'] = $this->bonus_sponsor;
				$update['updated_date'] = date('Y-m-d H:i:s');

				$sukses = "Proses Approve Komisi Dengan Nama ".$d_cek['kode'].' Berhasil';
				$gagal = "Proses Approve Komisi Dengan Nama ".$d_cek['kode'].' Gagal';
				$response = $this->crut->update($filter,'bonus_sponsor',$update,'',$sukses,$gagal);
				if($response['status'] && $response['kode'] == "1"){

					// input cashflow biaya_admin
					$keterangan = ' Pemasukan Biaya Admin Bonus Sponsor';
					$input_cashflow['kode_transaksi'] = microtime();
					$input_cashflow['tp'] = '1';
					$input_cashflow['jenis_transaksi'] = 'BAS'; // biaya admin sponsor
					$input_cashflow['kredit'] = ((10/100) * $this->bonus_sponsor);
					$input_cashflow['keterangan'] = $keterangan;
					$input_cashflow['created_date'] = date('Y-m-d H:i:s');
					$input_cashflow['created_by'] = $this->session->userdata('user_id');
					$this->crut->insert('cashflow',$input_cashflow,'','','');
					// input cashflow biaya_admin

					$cek_penerima = "select * from member_master where id_member ='".$d_cek['id_member']."'";
					$d_penerima = $this->crut->list_row($cek_penerima);
					if($d_penerima !=0){
						/// kirim pesan ke bonus_sponsor
						$pesan = "Yth. Bpk/Ibu ".$d_penerima['nama_lengkap']." (".$d_penerima['id_member']."), Komisi Sponsor tgl. ".$d_cek['tgl']." Rp. ".number_format($this->bonus_sponsor,0,".",",").",-telah kami transfer ke ".$d_penerima['nama_bank']." Anda - PT CBM";
						$respon_sms = sms_zensiva($pesan,$d_penerima['no_hp']);
							// simpan psean sms ke log
							$input_sms_reg['id_member'] = $d_penerima['id_member'];
							$input_sms_reg['type_sms'] = 3;
							$input_sms_reg['judul_pesan'] = 'SMS Approval Bonus Sponsor '.$d_penerima['id_member'];
							$input_sms_reg['isi_pesan'] = $pesan;
							$input_sms_reg['no_tujuan'] = $d_penerima['no_hp'];
							$input_sms_reg['status_terkirim'] = $respon_sms;
							$input_sms_reg['created_date'] = date('Y-m-d H:i:S');
							$this->crut->insert('sms_log',$input_sms_reg,'','','');
							// simpan psean sms ke log
						/// kirim pesan ke bonus_sponsor

						/// simpan notifikasi member
						$input_notifikasi['id_member'] = $d_cek['id_member'];
						$input_notifikasi['pesan_notif'] = "Selamat anda telah mendapatkan bonus sponsor dari id member = '".$d_cek['id_downline']."'";
						$input_notifikasi['type_notif'] = '0';
						$input_notifikasi['status_notif'] = '0';
						$input_notifikasi['url_notif'] = 'cbm/komisi/bonus_sponsor/fetch';
						$input_notifikasi['created_date'] = date('Y-m-d H:i:S');
						$this->crut->insert('notifikasi_member',$input_notifikasi,'','','');
						/// simpan notifikasi member

						/// simpan notifikasi admin
						$input_notifikasi_admin['id_member'] = $d_cek['id_member'];
						$input_notifikasi_admin['pesan_notif'] = $d_cek['id_member']." Telah Mendapatkan Bonus Sponsor dari ".$d_cek['id_downline'];
						$input_notifikasi_admin['type_notif'] = '0';
						$input_notifikasi_admin['status_notif'] = '0';
						$input_notifikasi_admin['url_notif'] = 'komisi/bonus_sponsor/fetch?penerima='.$d_cek['id_member'].'&status_komisi=1&tgl_1='.$d_cek['tgl_filter'].'&tgl_2='.$d_cek['tgl_filter'];
						$input_notifikasi_admin['created_date'] = date('Y-m-d H:i:S');
						$this->crut->insert('notifikasi_admin',$input_notifikasi_admin,'','','');
						/// simpan notifikasi admin
					}

				}
				//insert log
				$dt = array('filter'=>$filter,'input'=>$update);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'BonusSponsor',
					'jenis_operasi'=>'Update',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log

				//insert log_2
				$insert_log = array(
					'id_admin'=>$this->session->userdata('user_id'),
					'id_member'=>$d_cek['id_member'],
					'halaman'=>'BonusSponsor',
					'aksi'=>4,// approve komisi
					'created_date'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas_2',$insert_log,'','','');
				//insert log_2
				$this->session->set_flashdata('pesan',pesan($response));
				redirect(site_url('komisi/bonus_sponsor/fetch'));
			}else{
				$response = array('status'=>false,'kode'=>2,'pesan'=>'Komisi Tidak Ditemukan');
				$this->session->set_flashdata('pesan',pesan($response));
				redirect(site_url('komisi/bonus_sponsor/fetch'));
			}
		}



		public function autocomplete_member($q =''){
			$q = $this->input->get('term');
			$q_auto = "select id_member,id,nama_lengkap from member_master where concat(id_member,' ',nama_lengkap) like'%".$q."%'";
			$data_option = $this->crut->list_datas($q_auto);
			$json = array();
			foreach($data_option as $k => $v){
				$json[] = array('label'=>$v['id_member'].'-'.$v['nama_lengkap'],'value'=>$v['id_member'].'-'.$v['nama_lengkap']);
			}
			// echo '<pre>';
			// print_r($json);
			$json = json_encode($json);
			echo $json;
		}

		public function export()
		{
			$this->load->library('Excel_generator');
			$filter = "";
    		$status_komisi = $this->session->userdata('status_komisi');
    		$id_penerima = $this->session->userdata('id_member');
    		$tgl_1 = $this->session->userdata('tgl_1');
    		$tgl_2 = $this->session->userdata('tgl_2');
    		$status_auto_save = $this->session->userdata('status_auto_save');

    		if($status_komisi !=""){
    			if(empty($filter)){
    				$filter =" where a.status_approve ='".$status_komisi."'";
    			}else{
    				$filter .=" and a.status_approve ='".$status_komisi."'";
    			}
    		}

    		if(!empty($id_penerima)){
    			if(empty($filter)){
						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter =" where b.id_member ='".$id_penerima."'";
    			}else{
						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter .=" and b.id_member ='".$id_penerima."'";
    			}
    		}

    		if(!empty($keyword)){
    			if(empty($filter)){
    				$filter =" where a.data like '%".$keyword."%'";
    			}else{
    				$filter .=" and a.data like '%".$keyword."%'";
    			}
    		}

    		if(!empty($ip_user)){
    			if(empty($filter)){
    				$filter =" where a.ip_user ='".$ip_user."'";
    			}else{
    				$filter .=" and a.ip_user ='".$ip_user."'";
    			}
    		}

    		if(!empty($user_id)){
    			if(empty($filter)){
    				$filter =" where CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
    			}else{
    				$filter .=" and CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
    			}
    		}

    		if(!empty($tgl_1) && !empty($tgl_2)){
    			if(empty($filter)){
    				$filter =" where date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    			}else{
    				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    			}
    		}


				if(!empty($status_auto_save)){
    			if(empty($filter)){
    				$filter =" where a.status_auto_save = '".$status_auto_save."' ";
    			}else{
    				$filter .=" and a.status_auto_save = '".$status_auto_save."' ";
    			}
    		}



							 $query = "SELECT
                                                a.id AS id,
                                                a.`kode` AS kode,
                                                b.`nama_lengkap` AS nama_penerima,
                                                b.id_member AS id_penerima,
                                                b.no_rek AS no_rek,
                                                b.nama_bank AS nama_bank,
                                                b.atas_nama AS an_bank,
                                                a.komisi AS komisi,
												(a.komisi-(a.komisi/100)*10) AS persen,
                                                c.`id_member` AS kode_downline,
                                                c.`nama_lengkap` AS nama_downline,
                                                DATE_FORMAT(a.created_date, '%Y-%M-%d')AS tgl,
                                                a.`status_approve` AS status_approve,
                                                CASE
                                            WHEN a.status_approve = '1' THEN
                                                'Sudah Di Approve'
                                            WHEN a.status_approve = '0' THEN
                                                'Belum Di Approve'
                                            END AS status_terima,
                                            CONCAT(
                                                b.nama_lengkap,
                                                ' ',
                                                 IF(a.status_auto_save = '1', '(Auto Save)', '')
                                            )AS penerima
                                            FROM
                                                bonus_sponsor AS a
                                            INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
                                            INNER JOIN member_master AS c ON a.`id_downline` = c.`id_member` ".$filter." order by a.status_approve asc , a.created_date DESC";

			$result = $this->db->query($query);

			$this->excel_generator->set_query($result);
			$this->excel_generator->set_header(array('ID ','KODE','PENERMA','ID PENERIMA','REK PENERIMA','BANK','NAMA REKENING','BONUS SPONSOR','BIAYA ADMIN','ID MEMBER','NAMA MEMBER','STATUS TERIMA','TANGGAL'));
			$this->excel_generator->set_column(array('id','kode','penerima','id_penerima','no_rek','nama_bank','an_bank','komisi','persen','kode_downline','nama_downline','status_terima','tgl'));
			$this->excel_generator->set_width(array( 10,20, 20, 20,20, 20,20,20,20,20,20,20,20));
			$this->excel_generator->exportTo2003("Bonus Sponsor");


			}


}

/* End of file Customergroup.php */
/* Location: ./application/modules/customergroup/controllers/Customergroup.php */
