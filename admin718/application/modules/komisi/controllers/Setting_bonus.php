<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_bonus extends MY_Controller {
	protected $approval = array('1'=>'Tidak Approval','2'=>'Approval');
	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');
    protected $code = 'komisi_configuration';
    protected $jenis_komisi = array('bonus_pasangan'=>'Bonus Pasangan','bonus_sponsor'=>'Bonus Sponsor','bonus_cabang'=>'Bonus Cabang');

    protected $component_akses = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->cek_hak_akses($this->privileges['component']['setting_komisi'],'1|2');

	}

    public function edit(){
		$this->cek_hak_akses($this->privileges['component']['setting_komisi'],'2');

		$q_setting = "select * from setting where code='".$this->code."'";

		$data['page_header'] ='Setting Komisi';
        $data['jenis_komisi'] = $this->jenis_komisi;
		$data['css_head'] = array('plugins/datepicker/datepicker3.css');
		$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
									'plugins/datepicker/bootstrap-datepicker.js',
									'plugins/ckeditor/ckeditor.js'
			);
		$data['detail'] = array();
		if($this->crut->list_count($q_setting) > 0){
			$configs = $this->crut->list_datas($q_setting);
			$detail  = array();
			foreach ($configs as $key => $value) {
				$detail[$value['key']] = $value['value'];
			}
			$data['detail'] = $detail;
		}
		$this->parser->parse("setting_bonus/setting_bonus.tpl",$data);
	}

    public function save(){
		$this->cek_hak_akses($this->privileges['component']['setting_komisi'],'2');
        $q_cek = "select * from setting where code='".$this->code."'";
        if($this->crut->list_count($q_cek) > 0){
            $this->crut->delete(array('code'=>$this->code),'setting');
        }
        $input = array();
        $input[] = array(
                'code'=>$this->code,
                'key'=>'bonus_pasangan',
                'value'=> abs($this->input->post('bonus_pasangan'))
            );

        $input[] = array(
                'code'=>$this->code,
                'key'=>'bonus_sponsor',
                'value'=> abs($this->input->post('bonus_sponsor'))
            );

        $input[] = array(
                'code'=>$this->code,
                'key'=>'bonus_cabang',
                'value'=> abs($this->input->post('bonus_cabang'))
            );

        $pesan = $this->crut->insert_batch('setting',$input,'Simpan Setting Global','Setting Komisi Berhasil Di Simpan','Setting Komisi Gagal Di Simpan');

        //insert log
				$dt = array('input'=>$input);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'SettingKomisi',
					'jenis_operasi'=>'Update',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log

        $this->session->set_flashdata('pesan', pesan($pesan));
        redirect(site_url('komisi/setting_bonus/edit'));
	}

}

/* End of file Customergroup.php */
/* Location: ./application/modules/customergroup/controllers/Customergroup.php */
