{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<!--<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>-->
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header} </h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                {form_open_multipart(site_url($url))}
                  <div class="box-body">

                    <!--  <div class="form-group">
                        <label for=""><span class="text-red">*</span> Id Member</label>
                        <input type="text" name="id_member" id="" class="form-control" readonly value="{$id_member}">
                      </div> -->
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>Member Group</label>
                        <select name="id_group" id="id_group" class="form-control">
                            <option value=""></option>
                          {foreach from=$groups key=k item=group}
                            <option value="{$group['id']}">{$group['nama']}</option>
                          {/foreach}
                        </select>
                      </div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>Sponsor</label>
                        <input type="text" name="id_sponsor" id="id_sponsor" class="form-control">
                        <input type="hidden" name="kode_form" id="kode_form" class="form-control" value="{$kode_form}">
                      </div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>Upline</label>
                        <input type="text" name="id_upline" id="id_upline" class="form-control">
                      </div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span> Nama Lengkap</label>
                        <input type="text" name="nama_lengkap" id="" class="form-control" required>
                      </div>


                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>No. Identitas</label>
                        <input type="text" name="no_identitas" id="no_identitas" class="form-control" value="">
                      </div>

                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>Alamat</label>
                        <textarea type="text" name="alamat" id="" class="form-control" ></textarea>
                      </div>
                       <div class="form-group">
                                {assign var=origin_city value=json_decode($detail['kota_asal'],true)}
                                <label for="">Setting Kota Asal</label>
                                <div class="row">
                                  <div class="col-md-4">
                                    <select name="id_prov" id="propinsi_asal_ori" class="form-control">
                                          <option value=""></option>
                                          {foreach from=$l_propinsi key=k item=v}
                                            {assign var=pilih value=''}
                                            {if array_key_exists('kota_asal',$detail) }

                                              {if $v['id'] == $origin_city['propinsi']}
                                                {$pilih ='selected'}
                                              {/if}
                                            {/if}
                                            <option value="{$v['id']}" {$pilih}>{$v['provinsi']}</option>
                                          {/foreach}
                                    </select>
                                  </div>
                                  <div class="col-md-4">
                                    <select name="id_kota" id="kota_asal_ori" class="form-control">
                                      <option value=""></option>
                                        {if count($l_kota) > 0 }
                                         {foreach from=$l_kota key=k item=kota}
                                           {assign var=pilih value=''}
                                             {if $kota['id'] == $origin_city['kota']}
                                               {$pilih ='selected'}
                                             {/if}
                                            <option value="{$kota['city_id']}" {$pilih} >{$kota['type']} {$kota['kota']}</option>
                                          {/foreach}
                                        {/if}

                                    </select>
                                  </div>
                                  <div class="col-md-4">
                                    <select name="id_kec" id="kecamatan_asal_ori" class="form-control">
                                      <option value=""></option>
                                      {if count($l_kecamatan) > 0 }
                                       {foreach from=$l_kecamatan key=k item=kecamatan}
                                         {assign var=pilih value=''}
                                           {if $kecamatan['id'] == $origin_city['kecamatan']}
                                             {$pilih ='selected'}
                                           {/if}
                                          <option value="{$kecamatan['id']}" {$pilih} > {$kecamatan['type']} {$kecamatan['kecamatan']}</option>
                                        {/foreach}
                                      {/if}
                                    </select>
                                  </div>
                                </div>

                              </div>
                       <div class="form-group">
                        <label for="">Foto Profil</label>
                        <input type="file" name="foto" id="">
                      </div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>No HP</label>
                        <input type="text" name="no_hp" id="" class="form-control" value="" required>
                      </div>
                      <div class="form-group">
                        <label for="">Telp</label>
                        <input type="text" name="no_telp"  class="form-control" value="">
                      </div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>Email</label>
                        <input type="email" name="email" id="" class="form-control" value="">
                      </div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>Ahli Waris</label>
                        <input type="text" name="ahli_waris" class="form-control" value="" required>
                      </div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>Hubungan</label>
                        <input type="text" name="hubungan" id="" class="form-control" value="" required>
                      </div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>Nama Bank</label>
                        <input type="text" name="nama_bank" id="nama_bank" class="form-control" value="" required>
                      </div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>NO Rekening</label>
                        <input type="text" name="no_rek" id="no_rek" class="form-control" value="" required>
                      </div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>A.n Bank</label>
                        <input type="text" name="atas_nama" id="atas_nama" class="form-control" value="" required>
                      </div>
                       <div class="form-group">
                         {form_error('pin')}
                        <label for=""><span class="text-red">*</span> PIN</label>
                        <input type="text" name="pin" id="" class="form-control" required>
                      </div>
                        {form_error('pin-konfirm')}
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span> Konfirmasi PIN</label>
                        <input type="password" name="pin-konfirm" id="" class="form-control" required>
                      </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}
{block name=script_js}
  <script>
   jQuery(document).ready(function($) {

   $('#id_sponsor').autocomplete({
          source:"{site_url('member_master/autocomplete_sponsor')}",
          select:function(event,data){
          }
        });
     $('#id_upline').autocomplete({
          source:"{site_url('member_master/autocomplete_upline')}",
          select:function(event,data){
          }
        });

   });

    $("#propinsi_asal_ori").change(function(){

      $("#kecamatan_asal_ori").hide();
      $("#kota_asal_ori").hide();

        var propinsi_origin = $("#propinsi_asal_ori").val();
          var urls = '{site_url("member_master/kota_html")}';
          $.ajax({
            //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
            url:urls+'?propinsi_origin='+propinsi_origin,
            type: 'GET',
            dataType: 'html',
            success:function(data){
                var html = data;
                    $("#kota_asal_ori").html(html);
                    $("#kota_asal_ori").show();
            }
          });

      })

    // kecamatan ori
    $("#kota_asal_ori").change(function(){
      $("#kecamatan_asal_ori").hide();
        var propinsi_origin = $("#propinsi_asal_ori").val();
        var kota_origin = $("#kota_asal_ori").val();
          var urls = '{site_url("member_master/kecamatan_html")}';
          $.ajax({
            //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
            url:urls+'?propinsi_origin='+propinsi_origin+'&kota_origin='+kota_origin,
            type: 'GET',
            dataType: 'html',
            success:function(data){
                var html = data;

                    $("#kecamatan_asal_ori").html(html);
                    $("#kecamatan_asal_ori").show();
            }
          });

      })


      /// setingan untuk rajaongkir

            {if !isset($kota_asal) && !isset($kecamatan_asal) && !isset($kota_tujuan) && !isset($kecamatan_tujuan)}
                // $("#kota_asal_ori").hide();
                // $("#kecamatan_asal_ori").hide();
                $("#kota_asal").hide();
                $("#kecamatan_asal").hide();
                  $("#kota_tujuan").hide();
                $("#kecamatan_tujuan").hide();
            {/if}

  </script>
{/block}
