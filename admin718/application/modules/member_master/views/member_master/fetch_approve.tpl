{extends file='index_admin_.tpl'}

{block name=content}



    <div class="row">

      <div class="col-md-12">

        <div class="box">

          <div class="box-header">



          </div>

          <div class="box-body">

              <div class="btn-group pull-right">

                  <a href="{site_url($url_add)}" class="btn btn-primary" title="Register Member" ><i class="fa fa-plus"></i>Register Member</a>
                  <a href="{site_url('member_master/fetch')}" class="btn btn-primary">Member</a>

              </div>

          </div>

        </div>

      </div>

      <div class="col-md-12 col-sm-12">

              <!-- general form elements -->

              <div class="box box-primary">

                <div class="box-header with-border">

                  <h3 class="box-title">{$page_header}</h3>

                  {if $this->session->flashdata('pesan') !=""}

                    {$this->session->flashdata('pesan')}

                  {/if}

                </div><!-- /.box-header -->

                <!-- form start -->



                  <div class="box-body">

                    <!-- <div class="form-group">
                      <pre>
                        {print_r($menus)}
                      </pre>
                    </div>
                    <div class="form-group">
                      <pre>{print_r($privileges)}</pre>
                    </div> -->

                    <table id="example1" class="table table-bordered table-striped table-hover">

                    <thead>

                      <tr>

                        <th>Id Member</th>

                        <th>Nama</th>

                        <th>Sponsor</th>

                        <th>Upline</th>

                        <th>Kiri</th>

                        <th>Kanan</th>

                        <th>Tgl Registrasi</th>

                        <th>Aksi</th>



                      </tr>

                    </thead>

                    <tbody>

                      <tr>



                        <td>Id Member</td>

                        <td>Nama</td>

                        <td>Sponsor</td>

                        <td>Upline</td>

                        <td>Kiri</td>

                        <td>Kanan</td>

                        <td>Telp</td>

                        <td>

                          <a href="" class="btn btn-warning btn-sm" title="Edit "><i class="fa fa-pencil"></i></a>

                          <a href="" class="btn btn-delete btn-sm" title="Delete "><i class="fa fa-trash"></i></a>

                        </td>

                      </tr>

                    </tbody>

                    <tfoot>

                      <tr>



                        <th>Id Member</th>

                        <th>Nama</th>

                        <th>Sponsor</th>

                        <th>Upline</th>

                        <th>Kiri</th>

                        <th>Kanan</th>

                        <th>Tgl Registrasi</th>

                        <th>Aksi</th>



                      </tr>

                    </tfoot>

                  </table>



                  </div><!-- /.box-body -->



              </div><!-- /.box -->

            </div>



    </div>





{/block}



{block name=script_js}

  {assign var='url' value="member_master/fetch_data_approve"}

    <script>

      $(function () {

         var table = $("#example1").DataTable({

              "processing": true,

              "serverSide": true,

              "ajax": {

                  "url": "{site_url($url)}",

                  "type": "POST"

              },

              columns: [

                        { data:'id_member' },

                        { data:'nama_lengkap' },

                        { data:'id_sponsor' },

                        { data:'id_upline' },

                        { data:'downline_kiri' },

                        { data:'downline_kanan' },

                        { data:'created_date' },

                        { data:'aksi' }

                ],

              "columnDefs":[

                              {

                                 "sTitle":"Id Member",

                                 "aTargets": [ "id_member" ]

                              },

                              {

                                  "aTargets": [ 7 ],

                                  "bSortable": false

                              }

              ]

        });

      });





    </script>



{/block}
