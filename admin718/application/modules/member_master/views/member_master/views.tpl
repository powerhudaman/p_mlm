{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<!--<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>-->
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->


                  <div class="box-body">


                       <div class="row">
            <div class="col-md-6">
              <table class="table table-bordered table-striped">
                <tr>
                  <td>Nama</td>
                  <td>{$get['nama_lengkap']}</td>
                </tr>
                <tr>
                  <td>Tgl Pendaftaran</td>
                  <td>{$get['created_date']}</td>
                </tr>
                <tr>
                  <td>Sponsor</td>
                  <td>{$get['id_sponsor']} | {$sponsor['nama_lengkap']}</td>
                </tr>
                <tr>
                  <td>Upline</td>
                  <td>{$get['id_upline']} | {$upline['nama_lengkap']}</td>
                </tr>
              </table>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
               <table class="table table-bordered table-striped">
                 <tr>
                   <td>Jumlah Jaringan</td>
                   <td>{count($member_kiri)} Kiri / {count($member_kanan)} Kanan</td>
                 </tr>
                 <tr>
                   <td>Level Kedalaman</td>
                   <td>
                     <span id="level"></span> / <span id="level_kanan"></span>
                   </td>
                 </tr>
                 <tr>
                   <td>Sponsoring</td>
                   <td>{$get['id_upline']} | {$sponsor['nama_lengkap']}</td>
                 </tr>
                 <tr>
                   <td>Total Bonus Level</td>
                   <td>{number_format($total_bonus,0,",",".")}</td>
                 </tr>
               </table>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
          </div>



          </div><!-- /.box-body -->


                {form_close()}
                <div class="row">
                  <div class="col-md-6">

                    <div class="box box-info">
                      <div class="box-header with-border">
                        <h3 class="box-title">Kiri</h3>
                      </div>
                      <div class="box-body">
                         <table id="mygrid2" class="table table-bordered table-striped table-hover">
                            <thead>
                              <tr>
                               <td width="15%">No</td>
                                <td width="15%">Level</td>
                                <td width="70%">Nama</td>
                              </tr>
                            </thead>
                            <tbody>
                              {if count($member_kiri) > 0}
                                {assign var='no_kiri' value='0'}
                                {assign var='level' value='0'}
                                {foreach from=$sort_ki key=k item=v}
                                  {assign var='kiri' value=$member_kiri[$k]}
                                  {$no_kiri = $no_kiri + 1}
                                  {if $kiri['level'] > $level}
                                    {$level = $kiri['level']}
                                  {/if}

                                  <tr>
                                    <td width="15%">{$no_kiri}</td>
                                    <td width="15%">{$kiri['level']}</td>
                                    <td width="70%">
                                      <p>
                                        <strong>{$kiri['nama_lengkap']}({$kiri['id_member']})</strong>Tgl.{$kiri['tgl']}  <br>
                                        Sponsor.{$kiri['id_sponsor']} Upline.{$kiri['id_upline']}
                                      </p>
                                    </td>
                                  </tr>
                                {/foreach}
                              {/if}
                            </tbody>

                          </table>
                      </div>

                    </div>

                  </div>

                  <div class="col-md-6">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <h3 class="box-title">Kanan</h3>
                      </div>
                      <div class="box-body">
                         <table id="mygrid" class="table table-bordered table-striped table-hover">
                            <thead>
                              <tr>
                               <td width="15%">No</td>
                                <td width="15%">Level</td>
                                <td width="70%">Nama</td>
                              </tr>
                            </thead>
                            <tbody>
                             {if count($member_kanan) > 0}
                                {assign var='no_kanan' value='0'}
                                {assign var='level_kanan' value='0'}
                                {foreach from=$sort_ka key=k item=v}
                                  {assign var='kanan' value=$member_kanan[$k]}
                                  {$no_kanan = $no_kanan + 1}
                                  {if $kanan['level'] > $level_kanan}
                                    {$level_kanan = $kanan['level']}
                                  {/if}

                                  <tr>
                                    <td width="15%">{$no_kanan}</td>
                                    <td width="15%">{$kanan['level']}</td>
                                    <td width="70%">
                                      <p>
                                        <strong>{$kanan['nama_lengkap']}({$kanan['id_member']})</strong>Tgl.{$kanan['tgl']}  <br>
                                        Sponsor.{$kanan['id_sponsor']} Upline.{$kanan['id_upline']}
                                      </p>
                                    </td>
                                  </tr>
                                {/foreach}
                              {/if}
                            </tbody>

                          </table>
                      </div>
                      <!-- /.box-body -->
                    </div>

                  </div>

                  <!-- /.col (RIGHT) -->
                </div>
              </div><!-- /.box -->
            </div>

  	</div>

{/block}
{block name=script_js}
  {assign var='url' value="Member_master/get_memberkanan/{$get['id_member']}"}
  {assign var='url2' value="Member_master/get_memberkiri/{$get['id_member']}"}
    <script>
      $('#level').html("{$level} Kiri");
      $('#level_kanan').html("{$level_kanan} kanan");
       var id_member ="{$get['id_member']}";
    var t_kiri = "{count($member_kiri)}";
    var t_kanan = "{count($member_kanan)}";

    $.post('update_tk',{ id_member:id_member,t_kiri:t_kiri,t_kanan:t_kanan },function(json){

    });
      /*$(function () {
        $("#mygrid").DataTable({
              "processing": true,
              "serverSide": true,
              "ajax": {
                  "url": "{site_url($url)}",
                  "type": "POST"
              },
              columns: [
                        { data:'id_member' },
                        { data:'nama_lengkap' },
                        { data:'created_date' },
                        { data:'id_sponsor' },
                        { data:'id_upline' }
                ],
              "columnDefs":[
                              {
                                 "sTitle":"Id Member",
                                 "aTargets": [ "id_member" ]
                              },
                              {
                                  "aTargets": [ 9 ],
                                  "bSortable": false
                              }
              ]
        });
         $("#mygrid2").DataTable({
              "processing": true,
              "serverSide": true,
              "ajax": {
                  "url": "{site_url($url2)}",
                  "type": "POST"
              },
              columns: [
                        { data:'id_member' },
                        { data:'nama_lengkap' },
                        { data:'created_date' },
                        { data:'id_sponsor' },
                        { data:'id_upline' }
                ],
              "columnDefs":[
                              {
                                 "sTitle":"Id Member",
                                 "aTargets": [ "id_member" ]
                              },
                              {
                                  "aTargets": [ 9 ],
                                  "bSortable": false
                              }
              ]
        });
      });*/
    </script>

{/block}
