<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_master extends MY_Controller {

	private $list_member_statement_kiri = array();
	private $list_member_statement_kanan = array();
	private $listing_downline_data = array();
	private $no_level_downline = 0;
	private $bonus_sponsor = 0;
	private $bonus_pasangan = 0;
	private $bonus_cabang= 0;

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');

		$this->load->helper('security');
		$this->cek_hak_akses($this->privileges['component']['member_master'],'1|2');
		$this->load->library('datatables');
		$this->bonus_sponsor = $this->crut->setting('komisi_configuration','bonus_sponsor','setting')['value'];
		$this->bonus_pasangan = $this->crut->setting('komisi_configuration','bonus_pasangan','setting')['value'];
		$this->bonus_cabang = $this->crut->setting('komisi_configuration','bonus_cabang','setting')['value'];
	}

	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');

	public function fetch(){
		$this->cek_hak_akses($this->privileges['component']['member_master'],'1|2');

		$data['page_header'] ='Member Master';
		$data['url_admin'] = ADMINS;
		$data['url_add'] = 'member_master/add';

		// $data['contents'] = 'admin_group/add.tpl';

		$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
		$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
									'plugins/datatables/dataTables.bootstrap.min.js');

		$this->parser->parse("member_master/fetch.tpl",$data);
	}

	public function fetch_data(){

		// 			$string .='<a href="'.$v['url'].'/'.$kode.'" class="'.$v['class'].'" title="'.$v['title'].'"><i class="'.$v['simbol'].'"></i></a>';
		$link[] = array('url'=>site_url('member_master/edit'),'class'=>'btn btn-primary','title'=>'Edit Data Member','simbol'=>'fa fa-pencil');
		$link[] = array('url'=>site_url('member_master/views'),'class'=>'btn btn-success','title'=>'Lihat Member Statement','simbol'=>'fa fa-eye');
		$link[] = array('url'=>site_url('member_master/hirarki'),'class'=>'btn btn-success','title'=>'hirarki','simbol'=>'fa fa-group');
		$link[] = array('url'=>site_url('member_master/delete'),'class'=>'btn btn-danger','title'=>'Delete Member','simbol'=>'fa fa-trash');

		$this->datatables->select("id,id_member,nama_lengkap,id_sponsor,id_upline,downline_kiri,downline_kanan,no_hp");
		$this->datatables->from('member_master');
		$this->datatables->where('del',0)
		->unset_column('id')
		// ->unset_column('a.status')
		->add_column('aksi',link_generator_array($link,'$1'),'id')
		// ->add_column('status',ganti_array($this->status,'$2'),'a.status')
		;
		echo $this->datatables->generate('json');
	}

	public function add(){
		$kode_form = md5(uniqid(rand(), true));
		$_SESSION['kode_unik_khusus'] = $kode_form;
		$this->cek_hak_akses($this->privileges['component']['member_master'],'2');

		$q="select * from member_group where status_delete ='0'";

		$data['page_header'] ='Member Master';
		$data['url'] = 'member_master/save';
		$data['status'] = $this->status;
		$data['groups'] = $this->crut->list_datas($q);
		$data['kode_form'] = $kode_form;
	    $data['url_parameter'] = array('propinsi'=>'Propinsi','kota'=>'Kota','kecamatan'=>'Kecamatan');
        $kota_asal = array();
        $kota_asal = $this->crut->setting('kurir_api_bersama','kota_asal','setting');
	    if(!array_key_exists('pesan',$kota_asal)){
	       $kota_asal = $this->crut->setting('kurir_api_bersama','kota_asal','setting')['value'];
	      }
      // daftar propinsi
        $q_propinsi = "select * from master_provinsi";
        $d_propinsi = $this->crut->list_datas($q_propinsi);
        $d_kota = array();
        $d_kecamatan = array();

        if($kota_asal !=""){
          $kota_asal = json_decode($kota_asal,true);


          $q_kota = "select * from master_kota where provinsi_id ='".$kota_asal['propinsi']."'";
          $d_kota = $this->crut->list_datas($q_kota);

          $q_kecamatan = "select * from master_kecamatan where provinsi_id ='".$kota_asal['propinsi']."' and kota_id ='".$kota_asal['kota']."'";
          $d_kecamatan = $this->crut->list_datas($q_kecamatan);
        }

      $data['l_propinsi'] =  $d_propinsi;
      $data['l_kota'] = $d_kota;
      $data['l_kecamatan'] = $d_kecamatan;
      $data['kota_asal'] = $kota_asal;
      //for id member


	  $data['css_head'] = array('plugins/datepicker/datepicker3.css',
								  'plugins/jQueryUI/ui-autocomplete.css'

							);
	  $data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
									'plugins/datepicker/bootstrap-datepicker.js',
									'plugins/ckeditor/ckeditor.js',
									// 'plugins/bootstrap-tag-input/bootstrap-tagsinput.js',
									'plugins/jquery-tags-input/jquery.tagsinput.min.js',
									'plugins/select2/select2.min.js',
									'plugins/lightbox/js/lightbox.min.js'
			);

		$this->parser->parse("member_master/add.tpl",$data);
	}

	public function save(){
		$this->cek_hak_akses($this->privileges['component']['member_master'],'2');

		$this->form_validation->set_rules('pin', 'Pin', 'trim|required|alpha_dash',
			array(
				'required'=>'%s Tidak Boleh Kosong'
				));

		$this->form_validation->set_rules('pin-konfirm', 'PIN Konfirm', 'trim|required|matches[pin]',
			array(
					'required'=>'%s Tidak Boleh Kosong',
					'matches'=>'{field} Harus Sama Dengan Input Pin'
				));
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			$this->add();
		} else {



		$arrData = array(
		      'nama_lengkap', 'no_identitas', 'alamat', 'id_prov', 'id_kota', 'id_kec' ,'no_hp','no_telp','ahli_waris','email','hubungan','nama_bank','nama_bank','atas_nama','no_rek'
		  );

		foreach($arrData as $dt){
		        $input[$dt] =  $this->input->post($dt,true);

		}
		    //Generate ID Member
		    $q = $this->db->query("SELECT max(id_member) as no_akhir FROM member_master ");
		    $row = $q->row();
		    //  $id_kota =  $row->id;
		    if ($q->num_rows() > 0)
		      {
		      $no = $row->no_akhir;
		      $no_akhir = (int)substr($no,3, 7);
		      $no_akhir++;
		      // membuat format kode
		      $pattern = 'CBM'.sprintf("%07s", $no_akhir);
		      $id_member = $pattern;
		      }
		      else
		      { //if empty
		      $pattern = 'CBM0000001';
		      $id_member = $pattern;

		      }
			  $input['id_member'] = $id_member;
		      $input['id_group'] = $this->input->post('id_group',true);

			$input['id_sponsor'] = explode(" | ",$this->input->post('id_sponsor',true))[0];
			$input['id_upline'] = explode(" | ",$this->input->post('id_upline',true))[0];
			$t = date('Y-m-d');
			$t = strtotime($t);
			$d = strtotime("+1 Year",$t);
			$input['expired_date'] = date('Y-m-d',$d);
			$input['created_date'] = date("Y-m-d H:i:s");
			$input['created_by'] = $this->session->userdata('user_id');
			$input['aktivasi'] = '1';

			$input['pin'] = md5($id_member.'-'.$this->input->post('pin'));
			//$input['sort_order'] = $this->input->post('order',true);
			$input['del'] = '0';
			$sukses = "Proses Tambah member master dengan Nama = ".$input['nama_lengkap']." Berhasil";
			$gagal = "Proses Tambah member master dengan Nama = ".$input['nama_lengkap']." Gagal";

			$cek_upline = "select * from member_master where id_member='".$input['id_upline']."'";
			$d_cek_upline = $this->crut->list_row($cek_upline);
			if(!empty($d_cek_upline['downline_kiri']) && !empty($d_cek_upline['downline_kanan'])){
				$pesan = array('status'=>false,'kode'=>2,'pesan'=>'Proses Registrasi Gagal Karena id Upline '.$this->input->post('id_upline',true).' telah melibihi jumlah downlinenya ');
				$this->session->set_flashdata('pesan', pesan($pesan));
				redirect(site_url('member_master/fetch'));
				die();
			}else{
				$pesan = $this->crut->insert('member_master',$input,'Tambah Member',$sukses,$gagal);

				if($pesan['kode'] == 1){

					/// kirim pesan ke member
					// Yth. Bpk/ibu ILAH SUSILAWATI, selamat anda telah terdaftar menjadi Mitra CBM dgn No.ID: CBM100032 Password: -Terima kasih
					$pesan_sms = "Yth. Bpk/Ibu ".$this->input->post('nama_lengkap').", selamat anda telah terdaftar menjadi Mitra CBM dgn No.ID:  ".$id_member.' dengan Password : '.$this->input->post('pin').' Terimakasih';
					$respon_sms = sms_zensiva($pesan_sms,$telepon=$input['no_hp']);

						// simpan psean sms ke log
						$input_sms_reg['id_member'] = $id_member;
						$input_sms_reg['type_sms'] = 1;
						$input_sms_reg['judul_pesan'] = 'Registrasi Member '.$id_member;
						$input_sms_reg['isi_pesan'] = $pesan_sms;
						$input_sms_reg['no_tujuan'] = $input['no_hp'];
						$input_sms_reg['status_terkirim'] = $respon_sms;
						$input_sms_reg['created_date'] = date('Y-m-d H:i:S');
						$this->crut->insert('sms_log',$input_sms_reg,'','','');
						// simpan psean sms ke log

					/// kirim pesan ke member

					/// kirim pesan sponsor
					// Yth. Bpk/ibu TANTO TARDINI - CBM100024, selamat anda telah berhasil mensponsori Sdr/i ILAH SUSILAWATI -CBM100032 Terima kasih
					if($input['id_sponsor'] !=""){
						$cek_s = "select * from member_master where id_member = '".$input['id_sponsor']."';";
						$d_cek_s = $this->crut->list_row($cek_s);
						if($d_cek_s !=0){
							// $pesan = "Yth. Bpk/Ibu ".$this->input->post('nama_lengkap').", selamat anda telah terdaftar menjadi Mitra CBM dgn No.ID:  ".$id_member.' dengan Password : '.$this->input->post('pin').' Terimakasih';
							$pesan_sms = 'Yth. Bpk/ibu '.$d_cek_s['nama_lengkap'].' - '.$input['id_sponsor'].', selamat anda telah berhasil mensponsori Sdr/i '.$this->input->post('nama_lengkap').' -'.$id_member.' Terima kasih';
							$respon_sms = sms_zensiva($pesan_sms,$telepon=$d_cek_s['no_hp']);

							// simpan psean sms ke log
							$input_sms_reg['id_member'] = $input['id_sponsor'];
							$input_sms_reg['type_sms'] = 2;
							$input_sms_reg['judul_pesan'] = 'SMS Informasi Bonus Sponsor '.$input['id_sponsor'];
							$input_sms_reg['isi_pesan'] = $pesan_sms;
							$input_sms_reg['no_tujuan'] = $d_cek_s['no_hp'];
							$input_sms_reg['status_terkirim'] = $respon_sms;
							$input_sms_reg['created_date'] = date('Y-m-d H:i:S');
							$this->crut->insert('sms_log',$input_sms_reg,'','','');
							// simpan psean sms ke log

						}
					}
					/// kirim pesan sponsor
				}
			}






			if(isset($_FILES['foto']) && !empty($_FILES['foto']['name'])){
				$config['upload_path'] = FCPATH.'/assets/img/username/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '100000';
				$config['file_name'] = $id_member;
				$config['overwrite'] = true;

				// $this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ( $this->upload->do_upload('foto')){

					$data = $this->upload->data();

					$update['image'] = $data['file_name'];

					$filter = array('id_member'=>$input['id_member']);
					$this->crut->update($filter,'member_master',$update,'','','');
				}

			}

			//insert tbl admins status = 1 (TIDAK AKTF)


			if($pesan['status'] && $pesan['kode'] == '1' && $input['id_upline'] !=""){


				$q_cek = "select * from member_master where id_member ='".$input['id_upline']."'";
				$d_cek = $this->crut->list_row($q_cek);
				if($d_cek['downline_kiri'] == "" || $d_cek['downline_kanan'] == ""){
					if($d_cek['downline_kiri'] == ""){
					$update_downline['downline_kiri'] = $input['id_member'];
					}
					if($d_cek['downline_kiri'] !="" && $d_cek['downline_kanan'] == ""){
						$update_downline['downline_kanan'] = $input['id_member'];
					}
					$filter=array('id_member'=>$input['id_upline']);
					$this->crut->update($filter,'member_master',$update_downline,'','','');
				}

				// panggil bonus pasangan
				// $this->bonus_pasangan_input(array('id_member'=>$input['id_member']),$input['id_member']);
				if($input['id_upline'] !=""){
					$this->input_bonus_pasangan_2($input['id_upline']);
				}

				// insert bonus_sponsor
				if($input['id_sponsor'] !=""){
					$input_sponsor['kode'] = microtime();
					$input_sponsor['id_member'] = $input['id_sponsor'];
					$input_sponsor['id_downline'] = $id_member;
					$input_sponsor['komisi'] = $this->bonus_sponsor;
					$input_sponsor['created_date'] = date('Y-m-d H:i:s');
					$input_sponsor['created_by'] = $this->session->userdata('user_id');
					$input_sponsor['status_auto_save'] = 2;
					// cek auto_save
					$q_as = "select id from bonus_sponsor where id_member ='".$input['id_sponsor']."' ";
					$d_as = $this->crut->list_datas($q_as);
					if($d_as == 0){
						$input_sponsor['status_auto_save'] = 1;
					}
					// cek auto_save
					$this->crut->insert('bonus_sponsor',$input_sponsor,'','','');

					//insert log_2
					$insert_log = array(
						'id_admin'=>$this->session->userdata('user_id'),
						'id_member'=>$input_bonus_pasangan['id_member'],
						'halaman'=>'BonusSponsor',
						'aksi'=>1,
						'created_date'=>date('Y-m-d H:i:s')
					);
					$this->crut->insert('log_aktivitas_2',$insert_log,'','','');
					//insert log_2

					// input cashflow
					$keterangan = $d_cek_s['nama_lengkap'].' - '.$input['id_sponsor'].', telah berhasil mensponsori Sdr/i '.$this->input->post('nama_lengkap').' -'.$id_member.' Terima kasih';
					$input_cashflow['kode_transaksi'] = $input_sponsor['kode'];
					$input_cashflow['tp'] = '2';
					$input_cashflow['jenis_transaksi'] = 'BMS';
					$input_cashflow['debet'] = $this->bonus_sponsor;
					$input_cashflow['keterangan'] = $keterangan;
					$input_cashflow['created_date'] = date('Y-m-d H:i:s');
					$input_cashflow['created_by'] = $this->session->userdata('user_id');
					$this->crut->insert('cashflow',$input_cashflow,'','','');
					// input cashflow

				}
				// insert bonus_sponsor

				//insert log
				$dt = array('input'=>$input);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'MemberMaster',
					'jenis_operasi'=>'Insert',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log
				//insert log_2
				$insert_log = array(
					'id_admin'=>$this->session->userdata('user_id'),
					'id_member'=>$id_member,
					'halaman'=>'MemberMaster',
					'aksi'=>1,
					'created_date'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas_2',$insert_log,'','','');
				//insert log_2


			}
			$this->session->set_flashdata('pesan', pesan($pesan));
			redirect(site_url('member_master/fetch'));
		}
	}



	protected function input_bonus_pasangan($id_upline,$data_downline = array()){
		$q_cek_upline = "select downline_kiri, downline_kanan,id_upline from member_master where id_member ='".$id_upline."' and !isnull(downline_kiri) and !isnull(downline_kanan)";
		$d_cek_upline = $this->crut->list_row($q_cek_upline);
		if(count($data_downline) == 0){ /// ini digunakan bila yang pertama dapet bonus
			if($d_cek_upline !=0){
				$insert_bonus_pasangan['id_penerima'] = $id_upline;
				$insert_bonus_pasangan['kode'] = genRndString($length = 6, $chars = '1234567890BCDFGHJKLMNPQRSTVWXYZ');
				$insert_bonus_pasangan['id_pasangan_1'] = $d_cek_upline['downline_kiri'];
				$insert_bonus_pasangan['id_pasangan_2'] = $d_cek_upline['downline_kanan'];
				$insert_bonus_pasangan['komisi'] = $this->bonus_pasangan;
				$insert_bonus_pasangan['tgl'] = date('Y-m-d');
				$insert_bonus_pasangan['status_approve'] = 0;
				$insert_bonus_pasangan['status_view'] = 1;
				$insert_bonus_pasangan['created_date'] = date('Y-m-d H:i:s');
				$insert_bonus_pasangan['created_by'] = $this->session->userdata('user_id');
				$response_bonus_pasangan = $this->crut->insert('bonus_pasangan',$insert_bonus_pasangan,'','','');

				//insert log
				$dt = array('input'=>$insert_bonus_pasangan);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'BonusPasangan',
					'jenis_operasi'=>'Insert',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log

				// bila member masih punya upline
				if($response_bonus_pasangan['kode'] == 1 && $d_cek_upline['id_upline'] !=""){
					$this->input_bonus_pasangan($d_cek_upline['id_upline'],$d_cek_upline);
				}
				// bila member masih punya upline
			}
		}/// ini digunakan bila yang pertama dapet bonus
		if(count($data_downline) > 0){
			$q_cek_upline_2 = "select id_upline from member_master where id_member ='".$id_upline."' ";
			$d_cek_upline_2 = $this->crut->list_row($q_cek_upline_2);
			if($d_cek_upline_2 !=0){
				$insert_bonus_pasangan['id_penerima'] = $id_upline;
				$insert_bonus_pasangan['kode'] = genRndString($length = 6, $chars = '1234567890BCDFGHJKLMNPQRSTVWXYZ');
				$insert_bonus_pasangan['id_pasangan_1'] = $data_downline['downline_kiri'];
				$insert_bonus_pasangan['id_pasangan_2'] = $data_downline['downline_kanan'];
				$insert_bonus_pasangan['komisi'] = $this->bonus_pasangan;
				$insert_bonus_pasangan['tgl'] = date('Y-m-d');
				$insert_bonus_pasangan['status_approve'] = 0;
				$insert_bonus_pasangan['status_view'] = 1;
				$insert_bonus_pasangan['created_date'] = date('Y-m-d H:i:s');
				$insert_bonus_pasangan['created_by'] = $this->session->userdata('user_id');
				$response_bonus_pasangan = $this->crut->insert('bonus_pasangan',$insert_bonus_pasangan,'','','');

				//insert log
				$dt = array('input'=>$insert_bonus_pasangan);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'BonusPasangan',
					'jenis_operasi'=>'Insert',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log

				// bila member masih punya upline
				if($response_bonus_pasangan['kode'] == 1 && $d_cek_upline_2['id_upline'] !=""){
					$this->input_bonus_pasangan($d_cek_upline_2['id_upline'],$data_downline);
				}
				// bila member masih punya upline
			}
		}
	}

	protected function input_bonus_pasangan_2($id_member,$t_pasangan = ''){
		$this->list_member_statement_kiri = array();
		$this->list_member_statement_kanan = array();

		$q_member = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan,a.`id_upline`, d.`type`, d.`kota`,

		DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

		c.`nama_lengkap` AS nama_upline FROM member_master AS a

		LEFT JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

		LEFT JOIN member_master AS c ON a.`id_upline` = c.`id_member`

		LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member ='$id_member' ";

		$data['get'] = $this->crut->list_row($q_member);

		// list member kiri
		$this->data_statement_kiri($data['get']['downline_kiri']);
		$this->data_statement_kanan($data['get']['downline_kanan']);


		$total_kiri = count($this->list_member_statement_kiri);

		$total_kanan = count($this->list_member_statement_kanan);

		$input_bonus_pasangan['id_member'] = $id_member;
		$input_bonus_pasangan['t_kiri'] = $total_kiri;
		$input_bonus_pasangan['t_kanan'] = $total_kanan;

		// cek bons di tgl sekarang
		$q_bon = "select * from bonus_pasangan_2 where id_member ='".$id_member."' and tgl='".date('Y-m-d')."' ;";
		$d_bon = $this->crut->list_row($q_bon);
		// cek bons di tgl sekarang

		/// cek nilai bonus pasangan sebelumnya
		$t_pas = 0;
		// $q ="SELECT SUM(t_pasangan) AS total_pasangan FROM bonus_pasangan_2 WHERE id_member ='".$id_member."' AND status_approve ='1' GROUP BY id_member;";
		$q = "SELECT SUM(t_pasangan) AS total_pasangan FROM bonus_pasangan_2 WHERE id_member ='".$id_member."' AND tgl !='".date('Y-m-d')."'
GROUP BY id_member;";
		$d_q = $this->crut->list_row($q); // total pasangan hari sebelumnya

		if($d_q !=0){

			$t_pas = $d_q['total_pasangan'];
		}
		/// cek nilai bonus pasangan sebelumnya
		if($t_pasangan !=""){
			$total_kiri = $t_pasangan;
		}

		if($total_kiri < $total_kanan){
			$input_bonus_pasangan['t_pasangan'] = ($total_kiri - $t_pas);
		}
		if($total_kiri > $total_kanan){
			$input_bonus_pasangan['t_pasangan'] = ($total_kanan - $t_pas);
		}
		if($total_kiri == $total_kanan){
			$input_bonus_pasangan['t_pasangan'] = ($total_kanan - $t_pas);
		}
		if($input_bonus_pasangan['t_pasangan'] < 13){
			$input_bonus_pasangan['komisi'] = $this->bonus_pasangan * $input_bonus_pasangan['t_pasangan'];
		}
		if($input_bonus_pasangan['t_pasangan'] > 12){
			$input_bonus_pasangan['komisi'] = $this->bonus_pasangan * 12;
		}


		$input_bonus_pasangan['tgl'] = date('Y-m-d');

		// echo '<pre>';
		// print_r($input_bonus_pasangan);
		// die();
		if($d_bon == 0){
			$input_bonus_pasangan['status_approve'] = 0;
			$input_bonus_pasangan['created_date'] = date('Y-m-d H:i:S');
			$input_bonus_pasangan['created_by'] = $this->session->userdata('user_id');
			if($input_bonus_pasangan['t_pasangan'] > 0){

				$response = $this->crut->insert('bonus_pasangan_2',$input_bonus_pasangan,'','','');

				//insert log
				$dt = array('input'=>$input_bonus_pasangan);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'BonusPasangan',
					'jenis_operasi'=>'Insert',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log

				//insert log_2
				$insert_log = array(
					'id_admin'=>$this->session->userdata('user_id'),
					'id_member'=>$input_bonus_pasangan['id_member'],
					'halaman'=>'BonusPasangan',
					'aksi'=>1,
					'created_date'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas_2',$insert_log,'','','');
				//insert log_2

				// input cashflow
				$keterangan = $data['get']['nama_lengkap'].' - '.$id_member.', telah berhasil mendapatkan bonus pasangan  Terima kasih';
				$input_cashflow['kode_transaksi'] = $response['ids'];
				$input_cashflow['tp'] = '2';
				$input_cashflow['jenis_transaksi'] = 'BPS';
				$input_cashflow['debet'] = $input_bonus_pasangan['komisi'];
				$input_cashflow['keterangan'] = $keterangan;
				$input_cashflow['created_date'] = date('Y-m-d H:i:s');
				$input_cashflow['created_by'] = $this->session->userdata('user_id');
				$this->crut->insert('cashflow',$input_cashflow,'','','');
				// input cashflow

			}

		}else{
			// $input_bonus_pasangan['status_approve'] = 0;
			$input_bonus_pasangan['updated_date'] = date('Y-m-d H:i:S');
			$input_bonus_pasangan['updated_by'] = $this->session->userdata('user_id');
			$filter = array('id'=>$d_bon['id']);
			if($input_bonus_pasangan['t_pasangan'] > 0){
				$this->crut->update($filter,'bonus_pasangan_2',$input_bonus_pasangan,'','','');

				//insert log
				$dt = array('filter'=>$filter,'input'=>$input_bonus_pasangan);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'BonusPasangan',
					'jenis_operasi'=>'Update',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log
			}
		}

		if($data['get']['id_upline'] !=""){
			$this->input_bonus_pasangan_2($data['get']['id_upline']);
		}



	}

	public function edit($id){

		$this->cek_hak_akses($this->privileges['component']['member_master'],'1|2');
		$q_member = "select * from member_master where id='$id' ";
		$q_kota = 'select * from master_kota';
		$data['status'] = $this->status;
		$data['edit'] = $this->crut->list_row($q_member);

		$sponsor="select * from member_master where id ='".$data['edit']['id_sponsor']."' ";

		$q="select * from member_group where status_delete ='0'";
		$data['groups'] = $this->crut->list_datas($q);

		$data['page_header'] ='Member Master';
		$data['url'] = 'member_master/update';
		$data['status'] = $this->status;

		$data['url_parameter'] = array('propinsi'=>'Propinsi','kota'=>'Kota','kecamatan'=>'Kecamatan');
        $kota_asal = array();
        $kota_asal = $this->crut->setting('kurir_api_bersama','kota_asal','setting');
	    if(!array_key_exists('pesan',$kota_asal)){
	       $kota_asal = $this->crut->setting('kurir_api_bersama','kota_asal','setting')['value'];
	      }
      // daftar propinsi
        $q_propinsi = "select * from master_provinsi";
        $d_propinsi = $this->crut->list_datas($q_propinsi);


        $q_kota = "select * from master_kota where provinsi_id ='".$data['edit']['id_prov']."'";
        $d_kota = $this->crut->list_datas($q_kota);
        $q_kecamatan = "select * from master_kecamatan where provinsi_id ='".$data['edit']['id_prov']."' and kota_id ='".$data['edit']['id_kota']."'";
          $d_kecamatan = $this->crut->list_datas($q_kecamatan);

      $data['l_propinsi'] =  $d_propinsi;
      $data['l_kota'] = $d_kota;
      $data['l_kecamatan'] = $d_kecamatan;

      // echo '<pre>';
      // print_r($data);
      // die();
      // $data['kota_asal'] = $kota_asal;
      //for id member

		$data['sponsor'] = $this->crut->list_datas($sponsor);
		//$data['upline'] = $this->crut->list_datas($upline);

		$this->parser->parse("member_master/edit.tpl",$data);
	}


	public function update(){
		$this->cek_hak_akses($this->privileges['component']['member_master'],'2');


			$arrData = array(
		      'nama_lengkap', 'no_identitas', 'alamat', 'no_hp','no_telp','ahli_waris','email','hubungan','nama_bank','nama_bank','atas_nama','no_rek','aktivasi'
		  );
		//   'id_prov', 'id_kota', 'id_kec' ,
		foreach($arrData as $dt){
		        $input[$dt] =  $this->input->post($dt,true);

		}
			if(isset($_POST['pin']) && isset($_POST['pin-konfirm']) && $_POST['pin'] == $_POST['pin-konfirm'] && $_POST['pin'] !="" && $_POST['pin-konfirm'] !=""){
				$input['pin'] = do_hash($this->input->post('id_member').'-'.$this->input->post('pin'),'md5');

			}
			$input['update_by'] = $this->session->userdata('user_id');
			$sukses = "Proses Update  Member Berhasil";
			$gagal = "Proses Update  Member Gagal";
			 $input['id_group'] = $this->input->post('id_group',true);
			// $response = $this->crut->insert('admins',$input,'Update Group User',$sukses,$gagal);
			$filter = array('id'=>$this->input->post('id',true));

			/// notifikasi sms
			$cek_pin = "SELECT a.*, b.`nama_lengkap` AS nama_member_aktivasi, c.`nama_lengkap` AS nama_cabang, c.`no_hp` AS hp_cabang
			,d.`nama_lengkap` AS nama_sponsor, d.`no_hp` AS no_hp_sponsor, b.id_sponsor as id_member_sponsor FROM loging_pin AS a
			INNER JOIN member_master AS b ON a.`id_member_aktivasi` = b.`id_member`
			INNER JOIN member_master AS c ON a.`id_member` = c.`id_member`
			INNER JOIN member_master AS d ON b.`id_sponsor` = d.`id_member`
						WHERE b.`id` = '".$this->input->post('id',true)."'  ";
			$d_pin = $this->crut->list_row($cek_pin);

			$cek = "select * from member_master where id ='".$this->input->post('id',true)."' and aktivasi ='1' ";
			$d_cek = $this->crut->list_row($cek);
			if($d_pin !=0 && $d_cek !=0 && $this->input->post('aktivasi') == "2"){
				/// kirim pesan ke member
				$pesan = "Yth. Bpk/Ibu ".$d_pin['nama_member_aktivasi'].", selamat anda telah terdaftar menjadi Mitra CBM dgn No.ID:  ".$d_pin['id_member_aktivasi'].' dengan Pin : '.$d_pin['pin'].' Terimakasih';
				// sms_zensiva($pesan,$telepon=$input['no_hp']);
				/// kirim pesan ke member
				/// kirim pesan ke cabang
				$pesan = "Yth. Bpk/Ibu ".$d_pin['nama_sponsor']."-".$d_pin['id_member_sponsor']." selamat anda telah berhasil mensponsori Sdr/i ".$d_pin['nama_member_aktivasi']." -".$d_pin['id_member_aktivasi'].' Terimakasih';
				// sms_zensiva($pesan,$telepon=$d_pin['no_hp_sponsor']);
				/// kirim pesanke cabang
			}
			/// notifikasi sms

			$response = $this->crut->update($filter,'member_master',$input,'Proses Update Member',$sukses,$gagal);

			if($response['kode'] == "1" && $this->input->post('aktivasi') == "2"){





			}

			//insert log
			$dt = array('filter'=>$filter,'input'=>$input);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'MemberMaster',
				'jenis_operasi'=>'Update',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');

			//insert log_2
			$q_member = "select id_member from member_master where id ='".$this->input->post('id',true)."' ";
			$d_member = $this->crut->list_row($q_member);
			$insert_log = array(
				'id_admin'=>$this->session->userdata('user_id'),
				'id_member'=>$input_bonus_pasangan['id_member'],
				'halaman'=>'MemberMaster',
				'aksi'=>2,
				'created_date'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas_2',$insert_log,'','','');
			//insert log_2





			if(isset($_FILES['foto']) && !empty($_FILES['foto']['name'])){
				$config['upload_path'] = FCPATH.'/assets/img/username/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '100000';
				$config['file_name'] = $id_member;
				$config['overwrite'] = true;

				// $this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ( $this->upload->do_upload('foto')){

					$data = $this->upload->data();

					$update['image'] = $data['file_name'];

					$filter = array('id_member'=>$input['id_member']);
					$this->crut->update($filter,'member_master',$update,'','','');
				}

			}
			//insert log


			// redirect(site_url('Admingroup/fetch'));
				$this->session->set_flashdata('pesan', pesan($response));
			redirect(site_url('member_master/fetch'));

	}

	public function delete($id){
		// $this->cek_hak_akses($this->privileges['component']['member_master'],'2');


			$sukses = "Proses Delete Member Berhasil";
			$gagal = "Proses Delete Member Gagal";
			$filter = array('id'=>$id);
			$response = $this->crut->update($filter,'member_master',array('del'=>1),'Update Member Master',$sukses,$gagal);
			//insert log
			$dt = array('filter'=>$filter);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'MemberMaster',
				'jenis_operasi'=>'Delete',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log

			//insert log_2
			$q_member = "select id_member from member_master where id ='".$id."' ";
			$d_member = $this->crut->list_row($q_member);
			$insert_log = array(
				'id_admin'=>$this->session->userdata('user_id'),
				'id_member'=>$d_member['id_member'],
				'halaman'=>'MemberMaster',
				'aksi'=>3,
				'created_date'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas_2',$insert_log,'','','');
			//insert log_2

			$this->session->set_flashdata('pesan', pesan($response));

			redirect(site_url('member_master/fetch'));
	}

	public function autocomplete_sponsor($q =''){
			$q = $this->input->get('term');
			$q_auto = "select id_member,nama_lengkap from member_master where CONCAT(id_member,nama_lengkap) like'%".$q."%' ";
			$data_option = $this->crut->list_datas($q_auto);
			$json = array();
			foreach($data_option as $k => $v){
				$json[] = array('label'=>$v['id_member'].' | '.$v['nama_lengkap'],'value'=>$v['id_member'].' | '.$v['nama_lengkap']);
			}
			// echo '<pre>';
			// print_r($json);
			$json = json_encode($json);
			echo $json;
		}
	public function autocomplete_upline($q =''){
			$q = $this->input->get('term');
			$q_auto = "select id_member,nama_lengkap from member_master where CONCAT(id_member,nama_lengkap) like'%".$q."%' and (downline_kiri is null or downline_kanan is null)";
			$data_option = $this->crut->list_datas($q_auto);
			$json = array();
			foreach($data_option as $k => $v){
				$json[] = array('label'=>$v['id_member'].' | '.$v['nama_lengkap'],'value'=>$v['id_member'].' | '.$v['nama_lengkap']);
			}
			// echo '<pre>';
			// print_r($json);
			$json = json_encode($json);
			echo $json;
		}
	public function kota_html(){
      $prop_payment = $this->input->get('propinsi_origin',true);
      $kota_payment = $this->input->get('kota_origin',true);
      $q_kota_payment = "select * from master_kota where provinsi_id ='".$prop_payment."'";
      $d_kota_payment = $this->crut->list_datas($q_kota_payment);
      if($d_kota_payment == 0){
        echo '<option value="">Kota Tidak Ditemukan</option>';
      }
      if($d_kota_payment != 0){
        foreach ($d_kota_payment as $k => $v) {
          $pilih ='';
          if($v['id'] == $kota_payment){
            $pilih ='selected';
          }
          echo '<option value="'.$v['id'].'" '.$pilih.' >'.$v['type'].' '.$v['kota'].'</option>';
        }
      }
    }

    public function kecamatan_html(){
      $prop_payment = $this->input->get('propinsi_origin',true);
      $kota_payment = $this->input->get('kota_origin',true);
      $kecamatan_payment = $this->input->get('kecamatan_origin',true);
      $q_kecamatan_payment = "select * from master_kecamatan where provinsi_id ='".$prop_payment."' and kota_id ='".$kota_payment."'";
      $d_kecamatan_payment = $this->crut->list_datas($q_kecamatan_payment);
      if($d_kecamatan_payment == 0){
        echo '<option value="">Kecamatan Tidak Ditemukan</option>';
      }
      if($d_kecamatan_payment != 0){
        foreach ($d_kecamatan_payment as $k => $v) {
          $pilih ='';
          if($v['id'] == $kecamatan_payment){
            $pilih ='selected';
          }
          echo '<option value="'.$v['id'].'" '.$pilih.' >'.$v['type'].' '.$v['kecamatan'].'</option>';
        }
      }
    }

    public function views($id){

		$this->cek_hak_akses($this->privileges['component']['member_master'],'1|2');
		$q_member = "select * from member_master where id='$id' ";
		$data['get'] = $this->crut->list_row($q_member);

		$this->data_statement_kiri($data['get']['downline_kiri']);

		$this->data_statement_kanan($data['get']['downline_kanan']);


		$total_kiri = count($this->list_member_statement_kiri);
		$total_kanan = count($this->list_member_statement_kanan);
		// echo $total_kanan;

		$data['jml_jaringan'] = $total_kiri + $total_kanan;


		// list member kiri


		$this->listing_downline($data['get']['id_member']);

		///total bonus
		$data['bonus_sponsor'] = 0;
		$data['bonus_pasangan'] = 0;
		$cek_bonus_sponsor = "select count(*) total_sponsor from bonus_sponsor where id_member ='".$data['get']['id_member']."' and status_approve ='1' group by id_member";
		$d_bonus = $this->crut->list_row($cek_bonus_sponsor);

		$cek_bonus_pasangan = "select sum(komisi) as total_pasangan from bonus_pasangan_2 where id_member ='".$data['get']['id_member']."' and status_approve ='1' group by id_member";
		$d_bonus_2 = $this->crut->list_row($cek_bonus_pasangan);
		// echo $cek_bonus_pasangan;

		$cek_bonus_cabang= "select count(*) total_cabang from bonus_cabang where id_member ='".$data['get']['id_member']."' and approve ='1' group by id_member";
		$d_bonus_3= $this->crut->list_row($cek_bonus_cabang);



		$bonus_pasangan = $d_bonus_2['total_pasangan'];
		$bonus_cabang= $this->bonus_cabang * $d_bonus_3['total_cabang'];
		$bonus_sponsor= $this->bonus_sponsor * $d_bonus['total_sponsor'];
		$data['total_bonus'] = $bonus_pasangan + $bonus_cabang + $bonus_sponsor;

		$list_ki = $this->list_member_statement_kiri;
		$sort_by_level_ki = array();
		foreach ($list_ki as $k => $v) {
			$sort_by_level_ki[$k] = $v['level'];
		}
		array_multisort($sort_by_level_ki,SORT_ASC,$list_ki);

		$list_ka = $this->list_member_statement_kanan;
		$sort_by_level_ka = array();
		foreach ($list_ka as $k => $v) {
			$sort_by_level_ka[$k] = $v['level'];
		}
		array_multisort($sort_by_level_ka,SORT_ASC,$list_ka);

		// echo'<pre>';
		// print_r($sort_by_level_ki);
		// echo'</pre>';
		//
		// echo'<pre>';
		// print_r($list_ki);
		// echo'</pre>';
		//
		// echo'<pre>';
		// print_r($this->list_member_statement_kanan);
		// die();



		//echo'<pre>';
		//print_r($this->listing_downline_data);
		// die();

		$sponsor = $data['get']['id_sponsor'];
		$upline = $data['get']['id_upline'];
		$data['sort_ki'] = $sort_by_level_ki;
		$data['sort_ka'] = $sort_by_level_ka;
		$q_sponsor = "select * from member_master where id_member='$sponsor' ";
		$q_upline = "select * from member_master where id_member='$upline' ";
		$data['member_kiri'] = $this->list_member_statement_kiri;
		$data['member_kanan'] = $this->list_member_statement_kanan;
		$data['sponsor'] = $this->crut->list_row($q_sponsor);
		$data['upline'] = $this->crut->list_row($q_upline);
		//$data['upline'] = $this->crut->list_datas($upline);
		$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
		$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
									'plugins/datatables/dataTables.bootstrap.min.js');
		$this->parser->parse("member_master/views.tpl",$data);
	}


	public function hirarki($id=''){
		$this->cek_hak_akses($this->privileges['component']['member_master'],'1|2');

			if(isset($_POST['member']) && $_POST['member'] !=""){
				$member_post = explode(' | ',$_POST['member'].' | ');
				$id = $member_post[0];
				$q_member = "select * from member_master where id_member='$id' ";
				$data['get'] = $this->crut->list_row($q_member);
			}else{
				$q_member = "select * from member_master where id='$id' ";
				$data['get'] = $this->crut->list_row($q_member);
			}



			$hasil = array();

	    $hasil[0] = $data['get'];

	    $pos_4 = 1;

	    $angka = 0;

	    $verifikasi_downline_level_3 = ''; // variabel ini digunakan untuk menampung data sementara;

	    $this->data_statement_kiri($data['get']['downline_kiri']);

	    $this->data_statement_kanan($data['get']['downline_kanan']);


	    //testing foreach kiri
	    $list_angka_kiri = array(1,3,4,5,6,7,8);
	    $list_kiri = $this->list_member_statement_kiri;
	    foreach ($list_kiri as $k => $v) {
	      // echo $k.' '.$v['level'].'<br>';
	      if($v['level'] == 1){
	        $hasil[1] = $v;
	      }
	      if($v['level'] == 2){
	        // echo $k.' '.$v['level'].' kiri'.$v['id_upline'].'<br>';
	        $dki = $list_kiri[$v['id_upline']]['downline_kiri'];
	        $dka = $list_kiri[$v['id_upline']]['downline_kanan'];
	        // var_dump($dki);
	        if($dki == $k){
	          $hasil[3] = $v;
	        }
	        if($dka == $k){
	          $hasil[4] = $v;
	        }

	      }
	      if($v['level'] == 3){ /// level 3
	        /// level 2 pos kiri

	        $dki_2 = $list_kiri[$v['id_upline']]['downline_kiri'];
	        $dka_2 = $list_kiri[$v['id_upline']]['downline_kanan'];


	        $up_line_l1 = $list_kiri[$v['id_upline']]['id_upline'];


	        $dki_1 = $list_kiri[$up_line_l1]['downline_kiri'];
	        $dka_1 = $list_kiri[$up_line_l1]['downline_kanan'];

	        if($dki_1 == $v['id_upline']){ /// apabila level atas pada posisi kiri

	          if($dki_2 == $k){
	            $hasil[5] = $v;
	          }
	          if($dka_2 == $k){
	            $hasil[6] = $v;
	          }
	        }

	        if($dka_1 == $v['id_upline']){ /// apabila level atas pada posisi kiri

	          if($dki_2 == $k){
	            $hasil[7] = $v;
	          }
	          if($dka_2 == $k){
	            $hasil[8] = $v;
	          }
	        }


	        /// level 2 pos kiri

	      } /// level 3
	    }

	    //testing foreach kanan

	    $list_angka_kanan = array(2,9,10,11,12,12,14);
	    $list_kanan = $this->list_member_statement_kanan;
	    foreach ($list_kanan as $k => $v) {

	      if($v['level'] == 1){
	        $hasil[2] = $v;
	      }
	      if($v['level'] == 2){

	        $dki = $list_kanan[$v['id_upline']]['downline_kiri'];
	        $dka = $list_kanan[$v['id_upline']]['downline_kanan'];

	        if($dki == $k){
	          $hasil[9] = $v;
	        }
	        if($dka == $k){
	          $hasil[10] = $v;
	        }

	      }
	      if($v['level'] == 3){ /// level 3
	        /// level 2 pos kiri

	        $dki_2 = $list_kanan[$v['id_upline']]['downline_kiri'];
	        $dka_2 = $list_kanan[$v['id_upline']]['downline_kanan'];


	        $up_line_l1 = $list_kanan[$v['id_upline']]['id_upline'];


	        $dki_1 = $list_kanan[$up_line_l1]['downline_kiri'];
	        $dka_1 = $list_kanan[$up_line_l1]['downline_kanan'];

	        if($dki_1 == $v['id_upline']){ /// apabila level atas pada posisi kiri

	          if($dki_2 == $k){
	            $hasil[11] = $v;
	          }
	          if($dka_2 == $k){
	            $hasil[12] = $v;
	          }
	        }

	        if($dka_1 == $v['id_upline']){ /// apabila level atas pada posisi kiri

	          if($dki_2 == $k){
	            $hasil[13] = $v;
	          }
	          if($dka_2 == $k){
	            $hasil[14] = $v;
	          }
	        }


	        /// level 2 pos kiri

	      } /// level 3
	    }

	    //testing foreach kanan

			$data['listing_downline_data'] = $this->listing_downline_data;
			$data['hasil_downline'] = json_encode($hasil);
			$data['id_upline'] = 0;

	    if($data['get']['id_upline'] !=""){

	      $q_id_upline = "select id from member_master where id_member='".$data['get']['id_upline']."'";

	      $d_id_upline = $this->crut->list_row($q_id_upline);

	      $data['id_upline'] = $d_id_upline['id'];

		}
			/*

	  $data['css_head'] = array('plugins/datepicker/datepicker3.css',
								  'plugins/jQueryUI/ui-autocomplete.css'

							);
	  $data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
									'plugins/datepicker/bootstrap-datepicker.js',
									'plugins/ckeditor/ckeditor.js',
									// 'plugins/bootstrap-tag-input/bootstrap-tagsinput.js',
									'plugins/jquery-tags-input/jquery.tagsinput.min.js',
									'plugins/select2/select2.min.js',
									'plugins/lightbox/js/lightbox.min.js'
			);
			*/

			$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css',
									  'plugins/jQueryUI/ui-autocomplete.css');
			$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
										'plugins/jQueryUI/jquery-ui.js',
										'plugins/datatables/dataTables.bootstrap.min.js');
			$this->parser->parse("member_master/hirarki.tpl",$data);
	}


	protected function statement_view_kiri($id_member){
		$q_detail = "select id_member,nama_lengkap, id_sponsor,id_upline,downline_kiri,downline_kanan,date_format(created_date,'%d/%m/%Y') as tgl from member_master where id_member = '".$id_member."' ";
		$d_detail = $this->crut->list_row($q_detail);
		if($d_detail !=0){

			$this->list_member_statement_kiri[] = array(
														'id_member'=>$d_detail['id_member'],
														'nama_lengkap'=>$d_detail['nama_lengkap'],
														'sponsor'=>$d_detail['id_sponsor'],
														'upline'=>$d_detail['id_upline'],
														'tgl'=>$d_detail['tgl']
													  );

			if($d_detail['downline_kiri'] !=""){
				$this->statement_view_kiri($d_detail['downline_kiri']);
			}
			if($d_detail['downline_kanan'] !=""){
				$this->statement_view_kiri($d_detail['downline_kanan']);
			}
		}
	}

	protected function statement_view_kanan($id_member){
		$q_detail = "select id_member,nama_lengkap, id_sponsor,id_upline,downline_kanan,downline_kiri,date_format(created_date,'%d/%m/%Y') as tgl from member_master where id_member = '".$id_member."' ";
		$d_detail = $this->crut->list_row($q_detail);
		if($d_detail !=0){

			$this->list_member_statement_kanan[] = array(
														'id_member'=>$d_detail['id_member'],
														'nama_lengkap'=>$d_detail['nama_lengkap'],
														'sponsor'=>$d_detail['id_sponsor'],
														'upline'=>$d_detail['id_upline'],
														'tgl'=>$d_detail['tgl']
													  );

			if($d_detail['downline_kiri'] !=""){
				$this->statement_view_kiri($d_detail['downline_kiri']);
			}
			if($d_detail['downline_kanan'] !=""){
				$this->statement_view_kanan($d_detail['downline_kanan']);
			}

		}
	}

	public function data_statement_kiri($id_member,$level = 1){

		 if($level > 1){ /// proses level 1

				$level = $level + 1;

		 }

		 $d_detail_2 = array();
		 $d_detail_3 = array();

			$q_detail = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

												DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

												c.`nama_lengkap` AS nama_upline FROM member_master AS a

												INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

												INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

												LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$id_member."' ";

			$d_detail = $this->crut->list_row($q_detail);

			if($d_detail != 0){



								$this->list_member_statement_kiri[$id_member] = $d_detail;

									$this->list_member_statement_kiri[$d_detail['id_member']]['level'] = $level;



									if($d_detail['downline_kiri'] !="" || $d_detail['downline_kanan'] !=""){

										$level = $level + 1;

										/// cek downline kiri

										if($d_detail['downline_kiri'] !=""){

											$q_detail_2 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																				DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																				c.`nama_lengkap` AS nama_upline FROM member_master AS a

																				INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																				INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																				LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kiri']."' ";

											$d_detail_2 = $this->crut->list_row($q_detail_2);

											if($d_detail_2 != 0){



												$this->list_member_statement_kiri[$d_detail_2['id_member']] = $d_detail_2;

													$this->list_member_statement_kiri[$d_detail_2['id_member']]['level'] = $level;

											}

										}

										/// cek downline kiri

										/// cek downline kanan

										if($d_detail['downline_kanan'] !=""){

											// echo $d_detail['id_member'].' '.$d_detail['downline_kanan'].'<br>';

											$q_detail_3 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																				DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																				c.`nama_lengkap` AS nama_upline FROM member_master AS a

																				INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																				INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																				LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kanan']."' ";

											$d_detail_3 = $this->crut->list_row($q_detail_3);

											if($d_detail_3 !=0){



												$this->list_member_statement_kiri[$d_detail_3['id_member']] = $d_detail_3;

													$this->list_member_statement_kiri[$d_detail_3['id_member']]['level'] = $level;

											}

										}

										/// cek downline kanan

										/// cek downline level 2

										if($d_detail_2['downline_kiri'] !=""){

											$this->data_statement_kiri($d_detail_2['downline_kiri'],$level);

										}

										if($d_detail_2['downline_kanan'] !=""){

											$this->data_statement_kiri($d_detail_2['downline_kanan'],$level);

										}



										if(array_key_exists('downline_kiri',$d_detail_3) && $d_detail_3['downline_kiri'] !=""){

											$this->data_statement_kiri($d_detail_3['downline_kiri'],$level);

										}

										if(array_key_exists('downline_kanan',$d_detail_3) && $d_detail_3['downline_kanan'] !=""){

											$this->data_statement_kiri($d_detail_3['downline_kanan'],$level);

										}

										/// cek downline level 2

									}

			}





	}



	public function data_statement_kanan($id_member,$level = 1){

		if($level > 1){ /// proses level 1

			 $level = $level + 1;

		}

		$d_detail_2 = array();
		$d_detail_3 = array();

		 $q_detail = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

											 DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

											 c.`nama_lengkap` AS nama_upline FROM member_master AS a

											 INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

											 INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

											 LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$id_member."' ";

		 $d_detail = $this->crut->list_row($q_detail);

		 if($d_detail != 0){



							 $this->list_member_statement_kanan[$id_member] = $d_detail;

								 $this->list_member_statement_kanan[$d_detail['id_member']]['level'] = $level;



								 if($d_detail['downline_kiri'] !="" || $d_detail['downline_kanan'] !=""){

									 $level = $level + 1;

									 /// cek downline kiri

									 if($d_detail['downline_kiri'] !=""){

										 $q_detail_2 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																			 DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																			 c.`nama_lengkap` AS nama_upline FROM member_master AS a

																			 INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																			 INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																			 LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kiri']."' ";

										 $d_detail_2 = $this->crut->list_row($q_detail_2);

										 if($d_detail_2 != 0){



											 $this->list_member_statement_kanan[$d_detail_2['id_member']] = $d_detail_2;

												 $this->list_member_statement_kanan[$d_detail_2['id_member']]['level'] = $level;

										 }

									 }

									 /// cek downline kiri

									 /// cek downline kanan

									 if($d_detail['downline_kanan'] !=""){

										 // echo $d_detail['id_member'].' '.$d_detail['downline_kanan'].'<br>';

										 $q_detail_3 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																			 DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																			 c.`nama_lengkap` AS nama_upline FROM member_master AS a

																			 INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																			 INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																			 LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kanan']."' ";

										 $d_detail_3 = $this->crut->list_row($q_detail_3);

										 if($d_detail_3 !=0){



											 $this->list_member_statement_kanan[$d_detail_3['id_member']] = $d_detail_3;

												 $this->list_member_statement_kanan[$d_detail_3['id_member']]['level'] = $level;

										 }

									 }

									 /// cek downline kanan

									 /// cek downline level 2

									 if($d_detail_2['downline_kiri'] !=""){

										 $this->data_statement_kanan($d_detail_2['downline_kiri'],$level);

									 }

									 if($d_detail_2['downline_kanan'] !=""){

										 $this->data_statement_kanan($d_detail_2['downline_kanan'],$level);

									 }



									 if(array_key_exists('downline_kiri',$d_detail_3) && $d_detail_3['downline_kiri'] !=""){

										 $this->data_statement_kanan($d_detail_3['downline_kiri'],$level);

									 }

									 if(array_key_exists('downline_kiri',$d_detail_3) && $d_detail_3['downline_kanan'] !=""){

										 $this->data_statement_kanan($d_detail_3['downline_kanan'],$level);

									 }

									 /// cek downline level 2

								 }

		 }





 }



	public function total_downline($id){
		$this->list_member_statement_kiri = array();
		$this->list_member_statement_kanan = array();

		$q_member = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

		DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

		c.`nama_lengkap` AS nama_upline FROM member_master AS a

		LEFT JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

		LEFT JOIN member_master AS c ON a.`id_upline` = c.`id_member`

		LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id ='$id' ";

		$data['get'] = $this->crut->list_row($q_member);



		// list member kiri
		$this->data_statement_kiri($data['get']['downline_kiri']);
		$this->data_statement_kanan($data['get']['downline_kanan']);

		// $this->statement_view_kiri($data['get']['downline_kiri']);

		// $this->statement_view_kanan($data['get']['downline_kanan']);

		$total_kiri = count($this->list_member_statement_kiri);

		$total_kanan = count($this->list_member_statement_kanan);



		echo json_encode(array('id'=>$id,'total_kiri'=>$total_kiri,'total_kanan'=>$total_kanan));

	}


	public function listing_downline($id_member,$no_level = 0){
        /// cek downline member master
        $q_cek = "select downline_kiri,downline_kanan from member_master where id_member ='".$id_member."'";
        // echo $q_cek;
        // die();
        $d_cek = $this->crut->list_row($q_cek);
        $d_cek_2_kiri = 0;
        $d_cek_2_kanan = 0;
        if($d_cek!=0){
                // echo $this->no_level_downline.' '.$no_level.'<br>';
            /// cek level begin
                if(array_key_exists($this->no_level_downline, $this->listing_downline_data)){
                    $cek_data = $this->listing_downline_data[$this->no_level_downline];
                    $nilai_cek = $this->no_level_downline + $this->no_level_downline;
                    $hitung_total_data = 0;
                    if(array_key_exists('subs', $cek_data)){
                        foreach ($cek_data as $k => $v) {
                            if(array_key_exists('kiri', $v)){
                                $hitung_total_data = $hitung_total_data + count($v['kiri']);
                            }
                            if(array_key_exists('kanan', $v)){
                                $hitung_total_data = $hitung_total_data + count($v['kanan']);
                            }
                        }

                        if($hitung_total_data == $nilai_cek){
                            $this->no_level_downline = $no_level + 1;
                        }
                    }
                }

            /// cek level
            // $this->no_level_downline = $no_level + 1;

            if($d_cek['downline_kiri'] !=""){
                $q_cek_2_kiri = "select id, id_member,nama_lengkap,image,downline_kiri,downline_kanan,id_upline from member_master where id_member ='".$d_cek['downline_kiri']."'";
                $d_cek_2_kiri = $this->crut->list_row($q_cek_2_kiri);

                $this->listing_downline_data[$this->no_level_downline]['subs'][$d_cek_2_kiri['id_upline']]['kiri'] = array('id_member'=>$d_cek_2_kiri['id_member'],
                      'nama_lengkap'=> $d_cek_2_kiri['nama_lengkap'],
                      'image'=>$d_cek_2_kiri['image'],'id'=>$d_cek_2_kiri['id']);
            }

            if($d_cek['downline_kanan'] !=""){
                $q_cek_2_kanan = "select id, id_member,nama_lengkap,image,downline_kiri,downline_kanan,id_upline from member_master where id_member ='".$d_cek['downline_kanan']."'";
                $d_cek_2_kanan = $this->crut->list_row($q_cek_2_kanan);

                $this->listing_downline_data[$this->no_level_downline]['subs'][$d_cek_2_kanan['id_upline']]['kanan'] = array('id_member'=>$d_cek_2_kanan['id_member'],
                        'nama_lengkap'=> $d_cek_2_kanan['nama_lengkap'],
                         'image'=>$d_cek_2_kanan['image'],'id'=>$d_cek_2_kanan['id']);
            }
            if($d_cek_2_kiri !=0){
                    $this->listing_downline($d_cek_2_kiri['id_member'],$this->no_level_downline);
                }
            if($d_cek_2_kanan !=0){
                    $this->no_level_downline = $this->no_level_downline - 1;
                    $this->listing_downline($d_cek_2_kanan['id_member'],$this->no_level_downline);

                }



        }
		}


		public function fetch_approve(){
			$this->cek_hak_akses($this->privileges['component']['member_master'],'1|2');

			$data['page_header'] ='Member Approve';
			$data['url_admin'] = ADMINS;
			$data['url_add'] = 'member_master/add';

			// $data['contents'] = 'admin_group/add.tpl';

			$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
			$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
										'plugins/datatables/dataTables.bootstrap.min.js');

			$this->parser->parse("member_master/fetch_approve.tpl",$data);
		}

		public function fetch_data_approve(){

			// 			$string .='<a href="'.$v['url'].'/'.$kode.'" class="'.$v['class'].'" title="'.$v['title'].'"><i class="'.$v['simbol'].'"></i></a>';
			$link[] = array('url'=>site_url('member_master/approve_member'),'class'=>'btn btn-primary','title'=>'Approve Member','simbol'=>'fa fa-check-square-o');
			// $link[] = array('url'=>site_url('member_master/views'),'class'=>'btn btn-success','title'=>'Lihat Member Statement','simbol'=>'fa fa-eye');
			// $link[] = array('url'=>site_url('member_master/hirarki'),'class'=>'btn btn-success','title'=>'hirarki','simbol'=>'fa fa-group');
			// $link[] = array('url'=>site_url('member_master/delete'),'class'=>'btn btn-danger','title'=>'Delete Member','simbol'=>'fa fa-trash');

			$filter = array('del'=>0,'aktivasi'=>'1');
			$this->datatables->select("id,id_member,nama_lengkap,id_sponsor,id_upline,downline_kiri,downline_kanan,created_date");
			$this->datatables->from('member_master');
			$this->datatables->where($filter)
			->unset_column('id')
			// ->unset_column('a.status')
			->add_column('aksi',link_generator_array($link,'$1'),'id')
			// ->add_column('status',ganti_array($this->status,'$2'),'a.status')
			;
			echo $this->datatables->generate('json');
		}

		public function approve_member($id){
			$cek = "select * from member_master where id ='".$id."' and aktivasi ='1' ";
			$d_cek = $this->crut->list_row($cek);
			if($d_cek !=0){

				$sukses = "Proses Approve  Member Berhasil";
				$gagal = "Proses Approve  Member Gagal";

				$update['aktivasi'] = '2';
				$update['update_by'] = $this->session->userdata('user_id');
				$filter = array('id'=>$id);
				$response = $this->crut->update($filter,'member_master',$update,'Proses Update Member',$sukses,$gagal);
				// var_dump($response);

				if($response['kode'] == "1"){
					$cek_pin = "SELECT a.*, b.`nama_lengkap` AS nama_member_aktivasi, c.`nama_lengkap` AS nama_cabang,
								c.`no_hp` AS hp_cabang, b.`no_hp` AS no_hp_member, d.`nama_lengkap` AS nama_sponsor, d.`no_hp` AS no_hp_sponsor, b.id_sponsor as id_member_sponsor FROM loging_pin AS a
								INNER JOIN member_master AS b ON a.`id_member_aktivasi` = b.`id_member`
								INNER JOIN member_master AS c ON a.`id_member` = c.`id_member`
								INNER JOIN member_master AS d ON b.`id_sponsor` = d.`id_member`
											WHERE b.`id` = '".$id."'  ";
					$d_pin = $this->crut->list_row($cek_pin);
					if($d_pin !=0){
						/// kirim pesan ke member
						$pesan = "Yth. Bpk/Ibu ".$d_pin['nama_member_aktivasi'].", selamat anda telah terdaftar menjadi Mitra CBM dgn No.ID:  ".$d_pin['id_member_aktivasi'].' dengan Pin : '.$d_pin['pin'].' Terimakasih';
						sms_zensiva($pesan,$telepon=$d_pin['no_hp_member']);
						/// kirim pesan ke member
						/// kirim pesan ke sponsor
						$pesan = "Yth. Bpk/Ibu ".$d_pin['nama_sponsor']."-".$d_pin['id_member_sponsor']." selamat anda telah berhasil mensponsori Sdr/i ".$d_pin['nama_member_aktivasi']." -".$d_pin['id_member_aktivasi'].' Terimakasih';
						sms_zensiva($pesan,$telepon=$d_pin['no_hp_sponsor']);
						/// kirim pesanke sponsor
					}
				}
							// input cashflow biaya_admin
							$keterangan = ' Pemasukan Registrasi Member '.$d_cek['id_member'].' nama '.$d_cek['nama_lengkap'];
							$baps_cashflow['kode_transaksi'] = 'RMB'.genRndString(5,'1234567890');
							$baps_cashflow['tp'] = '1';
							$baps_cashflow['jenis_transaksi'] = 'RMB'; // biaya admin pasangan
							$baps_cashflow['kredit'] = '900000';
							$baps_cashflow['keterangan'] = $keterangan;
							$baps_cashflow['created_date'] = date('Y-m-d H:i:s');
							$baps_cashflow['created_by'] = $this->session->userdata('user_id');
							$this->crut->insert('cashflow',$baps_cashflow,'','','');
							// input cashflow biaya_admin

						//insert log
									$dt = array('filter'=>$filter,'input'=>$update);
									$insert_log = array(
										'kode_log'=>microtime(),
										'halaman'=>'MemberMaster',
										'jenis_operasi'=>'Update',
										'data'=>json_encode($dt),
										'ip_user'=>$_SERVER['REMOTE_ADDR'],
										'user_id'=>$this->session->userdata('user_id'),
										'date_created'=>date('Y-m-d H:i:s')
									);
									$this->crut->insert('log_aktivitas',$insert_log,'','','');

									//insert log_2
									$insert_log = array(
										'id_admin'=>$this->session->userdata('user_id'),
										'id_member'=>$d_cek['id_member'],
										'halaman'=>'MemberMaster',
										'aksi'=>7,// approve member
										'created_date'=>date('Y-m-d H:i:s')
									);
									$this->crut->insert('log_aktivitas_2',$insert_log,'','','');
									//insert log_2

									$this->session->set_flashdata('pesan', pesan($response));
									redirect(site_url('member_master/fetch_approve'));
			}

		}
		public function autocomplete_hirarki($q =''){
				$q = $this->input->get('term');
				$q_auto = "select id,id_member,nama_lengkap from member_master where CONCAT(id_member,nama_lengkap) like'%".$q."%'";
				$data_option = $this->crut->list_datas($q_auto);
				$json = array();
				foreach($data_option as $k => $v){
				  $json[] = array('label'=>$v['id_member'].' | '.$v['nama_lengkap'],'value'=>$v['id_member'].' | '.$v['nama_lengkap']);
				}
				// echo '<pre>';
				// print_r($json);
				$json = json_encode($json);
				echo $json;
		  }
		  public function export()
		{
			$this->load->library('Excel_generator');
			$filter = "";

								$query = "SELECT
												a.*, b.provinsi,
												c.kota,
												d.kecamatan,
												CASE
											WHEN a.aktivasi = '1' THEN
												'Tidak Aktif'
											WHEN a.aktivasi = '2' THEN
												'Aktif'
											END AS status
											FROM
												member_master a
											LEFT JOIN master_provinsi b ON a.id_prov = b.id
											LEFT JOIN master_kota c ON a.id_kota = c.id
											LEFT JOIN master_kecamatan d ON a.id_kec = d.id
											WHERE
												a.del = '0'";


			$result = $this->db->query($query);

			$this->excel_generator->set_query($result);
			$this->excel_generator->set_header(array('ID MEMBER','NAMA','SPONSOR','UPLINE','KIRI','KANAN','TELP','KTP','EMAIL','TGL REGISTER','ALAMAT','PROVINSI','KOTA','KECAMATAN','AHLI WARIS','HUBUNGAN','STATUS'));
			$this->excel_generator->set_column(array('id_member','nama_lengkap','id_sponsor','id_upline','downline_kiri','downline_kanan','no_hp','no_identitas','email','created_date','alamat','provinsi','kota','kecamatan','ahli_waris','hubungan','status'));
			$this->excel_generator->set_width(array( 15, 20,20, 20, 20, 20,20,20,20,20, 20, 20, 20,20,20,20,20));
			$this->excel_generator->exportTo2003("Member Master");


			}

	public function update_tk(){
		if(isset($_POST)){
			$id_member = $this->input->post('id_member',true);
			$t_kiri = $this->input->post('t_kiri',true);
			$t_kanan = $this->input->post('t_kanan',true);

			$filter = array('id_member'=>$id_member);
			$update['t_kiri'] = $t_kiri;
			$update['t_kanan'] = $t_kanan;
			$this->crut->update($filter,'member_master',$update,'','','');
		}
		}

}

/* End of file User.php */
/* Location: ./application/controllers/admin718/User.php */
