<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_edit extends MY_Controller {

	private $list_member_statement_kiri = array();
	private $list_member_statement_kanan = array();
	private $listing_downline_data = array();
	private $no_level_downline = 0;
	private $bonus_sponsor = 0;
	private $bonus_pasangan = 0;
	private $bonus_cabang= 0;

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');

		$this->load->helper('security');
		$this->cek_hak_akses($this->privileges['component']['member_edit'],'1|2');
		$this->load->library('datatables');
		$this->bonus_sponsor = $this->crut->setting('komisi_configuration','bonus_sponsor','setting')['value'];
		$this->bonus_pasangan = $this->crut->setting('komisi_configuration','bonus_pasangan','setting')['value'];
		$this->bonus_cabang = $this->crut->setting('komisi_configuration','bonus_cabang','setting')['value'];
	}

	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');

	public function fetch(){
		$this->cek_hak_akses($this->privileges['component']['member_edit'],'1|2');

		$data['page_header'] ='Member Master';
		$data['url_admin'] = ADMINS;
		$data['url_add'] = 'member_master/add';

		// $data['contents'] = 'admin_group/add.tpl';

		$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
		$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
									'plugins/datatables/dataTables.bootstrap.min.js');

		$this->parser->parse("member_edit/fetch.tpl",$data);
	}

	public function fetch_data(){

		// 			$string .='<a href="'.$v['url'].'/'.$kode.'" class="'.$v['class'].'" title="'.$v['title'].'"><i class="'.$v['simbol'].'"></i></a>';
		$link[] = array('url'=>site_url('member_master/member_edit/edit'),'class'=>'btn btn-primary','title'=>'Edit Data Member','simbol'=>'fa fa-pencil');
		// $link[] = array('url'=>site_url('member_master/views'),'class'=>'btn btn-success','title'=>'Lihat Member Statement','simbol'=>'fa fa-eye');
		// $link[] = array('url'=>site_url('member_master/hirarki'),'class'=>'btn btn-success','title'=>'hirarki','simbol'=>'fa fa-group');
		// $link[] = array('url'=>site_url('member_master/delete'),'class'=>'btn btn-danger','title'=>'Delete Member','simbol'=>'fa fa-trash');
		// $filter = array('downline_kiri'=>null,'downline_kanan'=>null);
		$this->datatables->select("id,id_member,nama_lengkap,id_sponsor,id_upline,downline_kiri,downline_kanan,no_hp");
		$this->datatables->from('member_master')
		->unset_column('id')
		// ->unset_column('a.status')
		->add_column('aksi',link_generator_array($link,'$1'),'id')
		// ->add_column('status',ganti_array($this->status,'$2'),'a.status')
		;
		echo $this->datatables->generate('json');
	}

	protected function input_bonus_pasangan($id_upline,$data_downline = array()){
		$q_cek_upline = "select downline_kiri, downline_kanan,id_upline from member_master where id_member ='".$id_upline."' and !isnull(downline_kiri) and !isnull(downline_kanan)";
		$d_cek_upline = $this->crut->list_row($q_cek_upline);
		if(count($data_downline) == 0){ /// ini digunakan bila yang pertama dapet bonus
			if($d_cek_upline !=0){
				$insert_bonus_pasangan['id_penerima'] = $id_upline;
				$insert_bonus_pasangan['kode'] = genRndString($length = 6, $chars = '1234567890BCDFGHJKLMNPQRSTVWXYZ');
				$insert_bonus_pasangan['id_pasangan_1'] = $d_cek_upline['downline_kiri'];
				$insert_bonus_pasangan['id_pasangan_2'] = $d_cek_upline['downline_kanan'];
				$insert_bonus_pasangan['komisi'] = $this->bonus_pasangan;
				$insert_bonus_pasangan['tgl'] = date('Y-m-d');
				$insert_bonus_pasangan['status_approve'] = 0;
				$insert_bonus_pasangan['status_view'] = 1;
				$insert_bonus_pasangan['created_date'] = date('Y-m-d H:i:s');
				$insert_bonus_pasangan['created_by'] = $this->session->userdata('user_id');
				$response_bonus_pasangan = $this->crut->insert('bonus_pasangan',$insert_bonus_pasangan,'','','');

				//insert log
				$dt = array('input'=>$insert_bonus_pasangan);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'BonusPasangan',
					'jenis_operasi'=>'Insert',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log

				// bila member masih punya upline
				if($response_bonus_pasangan['kode'] == 1 && $d_cek_upline['id_upline'] !=""){
					$this->input_bonus_pasangan($d_cek_upline['id_upline'],$d_cek_upline);
				}
				// bila member masih punya upline
			}
		}/// ini digunakan bila yang pertama dapet bonus
		if(count($data_downline) > 0){
			$q_cek_upline_2 = "select id_upline from member_master where id_member ='".$id_upline."' ";
			$d_cek_upline_2 = $this->crut->list_row($q_cek_upline_2);
			if($d_cek_upline_2 !=0){
				$insert_bonus_pasangan['id_penerima'] = $id_upline;
				$insert_bonus_pasangan['kode'] = genRndString($length = 6, $chars = '1234567890BCDFGHJKLMNPQRSTVWXYZ');
				$insert_bonus_pasangan['id_pasangan_1'] = $data_downline['downline_kiri'];
				$insert_bonus_pasangan['id_pasangan_2'] = $data_downline['downline_kanan'];
				$insert_bonus_pasangan['komisi'] = $this->bonus_pasangan;
				$insert_bonus_pasangan['tgl'] = date('Y-m-d');
				$insert_bonus_pasangan['status_approve'] = 0;
				$insert_bonus_pasangan['status_view'] = 1;
				$insert_bonus_pasangan['created_date'] = date('Y-m-d H:i:s');
				$insert_bonus_pasangan['created_by'] = $this->session->userdata('user_id');
				$response_bonus_pasangan = $this->crut->insert('bonus_pasangan',$insert_bonus_pasangan,'','','');

				//insert log
				$dt = array('input'=>$insert_bonus_pasangan);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'BonusPasangan',
					'jenis_operasi'=>'Insert',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log

				// bila member masih punya upline
				if($response_bonus_pasangan['kode'] == 1 && $d_cek_upline_2['id_upline'] !=""){
					$this->input_bonus_pasangan($d_cek_upline_2['id_upline'],$data_downline);
				}
				// bila member masih punya upline
			}
		}
	}

	protected function input_bonus_pasangan_2($id_member){
		$this->list_member_statement_kiri = array();
		$this->list_member_statement_kanan = array();

		$q_member = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan,a.`id_upline`, d.`type`, d.`kota`,

		DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

		c.`nama_lengkap` AS nama_upline FROM member_master AS a

		LEFT JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

		LEFT JOIN member_master AS c ON a.`id_upline` = c.`id_member`

		LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member ='$id_member' ";

		$data['get'] = $this->crut->list_row($q_member);

		// list member kiri
		$this->data_statement_kiri($data['get']['downline_kiri']);
		$this->data_statement_kanan($data['get']['downline_kanan']);


		$total_kiri = count($this->list_member_statement_kiri);

		$total_kanan = count($this->list_member_statement_kanan);

		$input_bonus_pasangan['id_member'] = $id_member;
		$input_bonus_pasangan['t_kiri'] = $total_kiri;
		$input_bonus_pasangan['t_kanan'] = $total_kanan;

		// cek bons di tgl sekarang
		$q_bon = "select * from bonus_pasangan_2 where id_member ='".$id_member."' and tgl='".date('Y-m-d')."' ;";
		$d_bon = $this->crut->list_row($q_bon);
		// cek bons di tgl sekarang

		/// cek nilai bonus pasangan sebelumnya
		$t_pas = 0;
		$q ="SELECT SUM(t_pasangan) AS total_pasangan FROM bonus_pasangan_2 WHERE id_member ='".$id_member."' GROUP BY id_member;";
		$d_q = $this->crut->list_row($q);
		if($d_q !=0){
			$t_pas = $d_q['total_pasangan'];
		}
		/// cek nilai bonus pasangan sebelumnya


		if($total_kiri < $total_kanan){
			$input_bonus_pasangan['t_pasangan'] = ($total_kiri - $t_pas);
		}
		if($total_kiri > $total_kanan){
			$input_bonus_pasangan['t_pasangan'] = ($total_kanan - $t_pas);
		}
		if($input_bonus_pasangan['t_pasangan'] < 13){
			$input_bonus_pasangan['komisi'] = $this->bonus_pasangan * $input_bonus_pasangan['t_pasangan'];
		}
		if($input_bonus_pasangan['t_pasangan'] > 12){
			$input_bonus_pasangan['komisi'] = $this->bonus_pasangan * 12;
		}


		$input_bonus_pasangan['tgl'] = date('Y-m-d');
		if($d_bon == 0){
			$input_bonus_pasangan['status_approve'] = 0;
			$input_bonus_pasangan['created_date'] = date('Y-m-d H:i:S');
			$input_bonus_pasangan['created_by'] = $this->session->userdata('user_id');
			$this->crut->insert('bonus_pasangan_2',$input_bonus_pasangan,'','','');

			//insert log
			$dt = array('input'=>$input_bonus_pasangan);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'BonusPasangan',
				'jenis_operasi'=>'Insert',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log
		}else{
			// $input_bonus_pasangan['status_approve'] = 0;
			$input_bonus_pasangan['updated_date'] = date('Y-m-d H:i:S');
			$input_bonus_pasangan['updated_by'] = $this->session->userdata('user_id');
			$filter = array('id'=>$d_bon['id']);
			$this->crut->update($filter,'bonus_pasangan_2',$input_bonus_pasangan,'','','');

			//insert log
			$dt = array('filter'=>$filter,'input'=>$input_bonus_pasangan);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'BonusPasangan',
				'jenis_operasi'=>'Update',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log
		}

		if($data['get']['id_upline'] !=""){
			$this->input_bonus_pasangan_2($data['get']['id_upline']);
		}



	}

	public function edit($id){

		$this->cek_hak_akses($this->privileges['component']['member_edit'],'2');
		$q_member = "select * from member_master where id='$id' and isnull(downline_kiri) and isnull(downline_kanan) ;";
		$q_kota = 'select * from master_kota';
		$data['status'] = $this->status;
		$data['edit'] = $this->crut->list_row($q_member);

		$sponsor="select * from member_master where id ='".$data['edit']['id_sponsor']."' ";

		$q="select * from member_group where status_delete ='0'";
		$data['groups'] = $this->crut->list_datas($q);

		$data['page_header'] ='Member Master';
		$data['url'] = 'member_master/member_edit/update';
		$data['status'] = $this->status;

		$data['url_parameter'] = array('propinsi'=>'Propinsi','kota'=>'Kota','kecamatan'=>'Kecamatan');
        $kota_asal = array();
        $kota_asal = $this->crut->setting('kurir_api_bersama','kota_asal','setting');
	    if(!array_key_exists('pesan',$kota_asal)){
	       $kota_asal = $this->crut->setting('kurir_api_bersama','kota_asal','setting')['value'];
	      }
      // daftar propinsi
        $q_propinsi = "select * from master_provinsi";
        $d_propinsi = $this->crut->list_datas($q_propinsi);


        $q_kota = "select * from master_kota where provinsi_id ='".$data['edit']['id_prov']."'";
        $d_kota = $this->crut->list_datas($q_kota);
        $q_kecamatan = "select * from master_kecamatan where provinsi_id ='".$data['edit']['id_prov']."' and kota_id ='".$data['edit']['id_kota']."'";
          $d_kecamatan = $this->crut->list_datas($q_kecamatan);

      $data['l_propinsi'] =  $d_propinsi;
      $data['l_kota'] = $d_kota;
      $data['l_kecamatan'] = $d_kecamatan;

      // echo '<pre>';
      // print_r($data);
      // die();
      // $data['kota_asal'] = $kota_asal;
      //for id member

			$data['css_head'] = array('plugins/datepicker/datepicker3.css',
									 'plugins/jQueryUI/ui-autocomplete.css'

							 );
		 $data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
									 'plugins/datepicker/bootstrap-datepicker.js',
									 'plugins/ckeditor/ckeditor.js',
									 // 'plugins/bootstrap-tag-input/bootstrap-tagsinput.js',
									 'plugins/jquery-tags-input/jquery.tagsinput.min.js',
									 'plugins/select2/select2.min.js',
									 'plugins/lightbox/js/lightbox.min.js'
			 );

		$data['sponsor'] = $this->crut->list_datas($sponsor);
		//$data['upline'] = $this->crut->list_datas($upline);

		$this->parser->parse("member_edit/edit.tpl",$data);
	}


	public function update(){
		$this->cek_hak_akses($this->privileges['component']['member_edit'],'2');
			$arrData = array(
		      'nama_lengkap', 'no_identitas', 'alamat', 'no_hp','no_telp','ahli_waris','email','hubungan','nama_bank','nama_bank','atas_nama','no_rek'
		  );
		//   'id_prov', 'id_kota', 'id_kec' ,
			foreach($arrData as $dt){
			        $input[$dt] =  $this->input->post($dt,true);
			}
			$input['id_sponsor'] = explode(" | ",$this->input->post('id_sponsor',true))[0];
			$input['id_upline'] = explode(" | ",$this->input->post('id_upline',true))[0];
			if(isset($_POST['pin']) && isset($_POST['pin-konfirm']) && $_POST['pin'] == $_POST['pin-konfirm']){
				$input['pin'] = do_hash($this->input->post('id_member').'-'.$this->input->post('pin'),'md5');
			}
			$sukses = "Proses Update  Member Berhasil";
			$gagal = "Proses Update  Member Gagal";
			$input['id_group'] = $this->input->post('id_group',true);
			// $response = $this->crut->insert('admins',$input,'Update Group User',$sukses,$gagal);
			$filter = array('id'=>$this->input->post('id',true));
			// var_dump($input);
			// die();

			$response = $this->crut->update($filter,'member_master',$input,'Proses Update Member',$sukses,$gagal);

			if($response['kode'] == "1" && $this->input->post('aktivasi') == "2"){
				/// data sponsor
				/// cek bonus sponsor sebelumnya
				$del_sponsor = array('id_downline'=>$this->input->post('id_member'));
				$r_del_sponsor = $this->crut->delete($del_sponsor,'bonus_sponsor','','','');
				if($r_del_sponsor['kode'] == "1"){

					$insert_bonus['kode'] = microtime();
					$insert_bonus['id_member'] = $input['id_sponsor'];
					$insert_bonus['id_downline'] = $this->input->post('id_member');
					$insert_bonus['komisi'] = $this->bonus_sponsor;
					$insert_bonus['status_approve'] = 0;
					$insert_bonus['created_date'] = date('Y-m-d H:i:s');

					$s_insert_bonus = $this->crut->insert('bonus_sponsor',$insert_bonus,'','','');
					if($s_insert_bonus['kode'] == "1"){

						/// kirim pesan sponsor
						// Yth. Bpk/ibu TANTO TARDINI - CBM100024, selamat anda telah berhasil mensponsori Sdr/i ILAH SUSILAWATI -CBM100032 Terima kasih
						if($input['id_sponsor'] !=""){
							$cek_s = "select * from member_master where id_member = '".$input['id_sponsor']."';";
							$d_cek_s = $this->crut->list_row($cek_s);
							if($d_cek_s !=0){
								// $pesan = "Yth. Bpk/Ibu ".$this->input->post('nama_lengkap').", selamat anda telah terdaftar menjadi Mitra CBM dgn No.ID:  ".$id_member.' dengan Password : '.$this->input->post('pin').' Terimakasih';
								$pesan_sms = 'Yth. Bpk/ibu '.$d_cek_s['nama_lengkap'].' - '.$input['id_sponsor'].', selamat anda telah berhasil mensponsori Sdr/i '.$this->input->post('nama_lengkap').' -'.$id_member.' Terima kasih';
								sms_zensiva($pesan_sms,$telepon=$d_cek_s['no_hp']);
							}
						}

					}

				}
				/// cek bonus sponsor sebelumnya


				/// data sponsor

				/// data pasangan
				//hapus bonus pasangan
					$q_bp="SELECT * FROM bonus_pasangan WHERE id_pasangan_1 ='".$this->input->post('id_member')."' OR id_pasangan_2 ='".$this->input->post('id_member')."';";
					$d_bp = $this->crut->list_datas($q_bp);
					if($d_bp !=0){
						foreach ($d_bp as $k => $v) {
							$del_pasangan = array('id'=>$v['id']);
							$r_del_pasangan = $this->crut->delete($del_pasangan,'bonus_pasangan','','','');
						}
					}
				//hapus bonus pasangan
				// cek downline
				$q_bp="SELECT * FROM member_master WHERE downline_kiri ='".$this->input->post('id_member')."' OR downline_kanan ='".$this->input->post('id_member')."';";
				$d_bp = $this->crut->list_row($q_bp);
				if($d_bp !=0){
					if($d_bp['downline_kiri'] == $this->input->post('id_member')){
						$up_bp['downline_kiri'] = '';
					}
					if($d_bp['downline_kanan'] == $this->input->post('id_member')){
						$up_bp['downline_kanan'] = '';
					}
					$filter = array('id'=>$d_bp['id']);
					$this->crut->update($filter,'member_master',$up_bp,'','','');
				}
				// cek downline
				$q_cek = "select * from member_master where id_member ='".$input['id_upline']."'";
				$d_cek = $this->crut->list_row($q_cek);
				if($d_cek['downline_kiri'] == "" || $d_cek['downline_kanan'] == ""){
					if($d_cek['downline_kiri'] == ""){
					$update_downline['downline_kiri'] = $input['id_member'];
					}
					if($d_cek['downline_kiri'] !="" && $d_cek['downline_kanan'] == ""){
						$update_downline['downline_kanan'] = $input['id_member'];
					}
					$filter=array('id_member'=>$input['id_upline']);
					$this->crut->update($filter,'member_master',$update_downline,'','','');
				}
				if($input['id_upline'] !=""){
					$this->input_bonus_pasangan($input['id_upline']);
				}
				/// data pasangan


				$cek_pin = "SELECT a.*, b.`nama_lengkap` AS nama_member_aktivasi, c.`nama_lengkap` AS nama_cabang, c.`no_hp` as hp_cabang FROM loging_pin AS a
										INNER JOIN member_master AS b ON a.`id_member_aktivasi` = b.`id_member`
										INNER JOIN member_master AS c ON a.`id_member` = c.`id_member`
										WHERE b.`id` = '".$this->input->post('id',true)."'  ";
				$d_pin = $this->crut->list_row($cek_pin);
				if($d_pin !=0){
					/// kirim pesan ke member
					$pesan = "Yth. Bpk/Ibu ".$d_pin['nama_member_aktivasi'].", selamat anda telah terdaftar menjadi Mitra CBM dgn No.ID:  ".$d_pin['id_member_aktivasi'].' dengan Pin : '.$d_pin['pin'].' Terimakasih';
					sms_zensiva($pesan,$telepon=$input['no_hp']);
					/// kirim pesan ke member
					/// kirim pesan ke cabang
					$pesan = "Yth. Bpk/Ibu ".$d_pin['nama_cabang']."-".$d_pin['id_member']." selamat anda telah berhasil mensponsori Sdr/i ".$d_pin['nama_member_aktivasi']." -".$d_pin['id_member_aktivasi'].' Terimakasih';
					sms_zensiva($pesan,$telepon=$input['no_hp']);
					/// kirim pesanke cabang
				}

			}



			//insert log
			$dt = array('filter'=>$filter,'input'=>$input);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'MemberMaster',
				'jenis_operasi'=>'Update',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');

			if(isset($_FILES['foto']) && !empty($_FILES['foto']['name'])){
				$config['upload_path'] = FCPATH.'/assets/img/username/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '100000';
				$config['file_name'] = $id_member;
				$config['overwrite'] = true;

				// $this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ( $this->upload->do_upload('foto')){

					$data = $this->upload->data();

					$update['image'] = $data['file_name'];

					$filter = array('id_member'=>$input['id_member']);
					$this->crut->update($filter,'member_master',$update,'','','');
				}

			}
			//insert log


			// redirect(site_url('Admingroup/fetch'));
				$this->session->set_flashdata('pesan', pesan($response));
			redirect(site_url('member_master/member_edit/fetch'));

	}

	public function delete($id){
		// $this->cek_hak_akses($this->privileges['component']['member_master'],'2');


			$sukses = "Proses Delete Member Berhasil";
			$gagal = "Proses Delete Member Gagal";
			$filter = array('id'=>$id);
			$response = $this->crut->update($filter,'member_master',array('del'=>1),'Update Member Master',$sukses,$gagal);
			//insert log
			$dt = array('filter'=>$filter);
			$insert_log = array(
				'kode_log'=>microtime(),
				'halaman'=>'MemberMaster',
				'jenis_operasi'=>'Delete',
				'data'=>json_encode($dt),
				'ip_user'=>$_SERVER['REMOTE_ADDR'],
				'user_id'=>$this->session->userdata('user_id'),
				'date_created'=>date('Y-m-d H:i:s')
			);
			$this->crut->insert('log_aktivitas',$insert_log,'','','');
			//insert log
			$this->session->set_flashdata('pesan', pesan($response));

			redirect(site_url('member_master/fetch'));
	}

	public function autocomplete_sponsor($q =''){
			$q = $this->input->get('term');
			$q_auto = "select id_member,nama_lengkap from member_master where CONCAT(id_member,nama_lengkap) like'%".$q."%' ";
			$data_option = $this->crut->list_datas($q_auto);
			$json = array();
			foreach($data_option as $k => $v){
				$json[] = array('label'=>$v['id_member'].' | '.$v['nama_lengkap'],'value'=>$v['id_member'].' | '.$v['nama_lengkap']);
			}
			// echo '<pre>';
			// print_r($json);
			$json = json_encode($json);
			echo $json;
		}
	public function autocomplete_upline($q =''){
			$q = $this->input->get('term');
			$q_auto = "select id_member,nama_lengkap from member_master where CONCAT(id_member,nama_lengkap) like'%".$q."%' and downline_kiri is null or downline_kanan is null";
			$data_option = $this->crut->list_datas($q_auto);
			$json = array();
			foreach($data_option as $k => $v){
				$json[] = array('label'=>$v['id_member'].' | '.$v['nama_lengkap'],'value'=>$v['id_member'].' | '.$v['nama_lengkap']);
			}
			// echo '<pre>';
			// print_r($json);
			$json = json_encode($json);
			echo $json;
		}
	public function kota_html(){
      $prop_payment = $this->input->get('propinsi_origin',true);
      $kota_payment = $this->input->get('kota_origin',true);
      $q_kota_payment = "select * from master_kota where provinsi_id ='".$prop_payment."'";
      $d_kota_payment = $this->crut->list_datas($q_kota_payment);
      if($d_kota_payment == 0){
        echo '<option value="">Kota Tidak Ditemukan</option>';
      }
      if($d_kota_payment != 0){
        foreach ($d_kota_payment as $k => $v) {
          $pilih ='';
          if($v['id'] == $kota_payment){
            $pilih ='selected';
          }
          echo '<option value="'.$v['id'].'" '.$pilih.' >'.$v['type'].' '.$v['kota'].'</option>';
        }
      }
    }

    public function kecamatan_html(){
      $prop_payment = $this->input->get('propinsi_origin',true);
      $kota_payment = $this->input->get('kota_origin',true);
      $kecamatan_payment = $this->input->get('kecamatan_origin',true);
      $q_kecamatan_payment = "select * from master_kecamatan where provinsi_id ='".$prop_payment."' and kota_id ='".$kota_payment."'";
      $d_kecamatan_payment = $this->crut->list_datas($q_kecamatan_payment);
      if($d_kecamatan_payment == 0){
        echo '<option value="">Kecamatan Tidak Ditemukan</option>';
      }
      if($d_kecamatan_payment != 0){
        foreach ($d_kecamatan_payment as $k => $v) {
          $pilih ='';
          if($v['id'] == $kecamatan_payment){
            $pilih ='selected';
          }
          echo '<option value="'.$v['id'].'" '.$pilih.' >'.$v['type'].' '.$v['kecamatan'].'</option>';
        }
      }
    }


		public function data_statement_kiri($id_member,$level = 1){

			 if($level > 1){ /// proses level 1

					$level = $level + 1;

			 }

			 $d_detail_2 = array();
			 $d_detail_3 = array();

				$q_detail = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

													DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

													c.`nama_lengkap` AS nama_upline FROM member_master AS a

													INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

													INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

													LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$id_member."' ";

				$d_detail = $this->crut->list_row($q_detail);

				if($d_detail != 0){



									$this->list_member_statement_kiri[$id_member] = $d_detail;

										$this->list_member_statement_kiri[$d_detail['id_member']]['level'] = $level;



										if($d_detail['downline_kiri'] !="" || $d_detail['downline_kanan'] !=""){

											$level = $level + 1;

											/// cek downline kiri

											if($d_detail['downline_kiri'] !=""){

												$q_detail_2 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																					DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																					c.`nama_lengkap` AS nama_upline FROM member_master AS a

																					INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																					INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																					LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kiri']."' ";

												$d_detail_2 = $this->crut->list_row($q_detail_2);

												if($d_detail_2 != 0){



													$this->list_member_statement_kiri[$d_detail_2['id_member']] = $d_detail_2;

														$this->list_member_statement_kiri[$d_detail_2['id_member']]['level'] = $level;

												}

											}

											/// cek downline kiri

											/// cek downline kanan

											if($d_detail['downline_kanan'] !=""){

												// echo $d_detail['id_member'].' '.$d_detail['downline_kanan'].'<br>';

												$q_detail_3 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																					DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																					c.`nama_lengkap` AS nama_upline FROM member_master AS a

																					INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																					INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																					LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kanan']."' ";

												$d_detail_3 = $this->crut->list_row($q_detail_3);

												if($d_detail_3 !=0){



													$this->list_member_statement_kiri[$d_detail_3['id_member']] = $d_detail_3;

														$this->list_member_statement_kiri[$d_detail_3['id_member']]['level'] = $level;

												}

											}

											/// cek downline kanan

											/// cek downline level 2

											if($d_detail_2['downline_kiri'] !=""){

												$this->data_statement_kiri($d_detail_2['downline_kiri'],$level);

											}

											if($d_detail_2['downline_kanan'] !=""){

												$this->data_statement_kiri($d_detail_2['downline_kanan'],$level);

											}



											if(array_key_exists('downline_kiri',$d_detail_3) && $d_detail_3['downline_kiri'] !=""){

												$this->data_statement_kiri($d_detail_3['downline_kiri'],$level);

											}

											if(array_key_exists('downline_kanan',$d_detail_3) && $d_detail_3['downline_kanan'] !=""){

												$this->data_statement_kiri($d_detail_3['downline_kanan'],$level);

											}

											/// cek downline level 2

										}

				}





		}



		public function data_statement_kanan($id_member,$level = 1){

			if($level > 1){ /// proses level 1

				 $level = $level + 1;

			}

			$d_detail_2 = array();
			$d_detail_3 = array();

			 $q_detail = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

												 DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

												 c.`nama_lengkap` AS nama_upline FROM member_master AS a

												 INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

												 INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

												 LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$id_member."' ";

			 $d_detail = $this->crut->list_row($q_detail);

			 if($d_detail != 0){



								 $this->list_member_statement_kanan[$id_member] = $d_detail;

									 $this->list_member_statement_kanan[$d_detail['id_member']]['level'] = $level;



									 if($d_detail['downline_kiri'] !="" || $d_detail['downline_kanan'] !=""){

										 $level = $level + 1;

										 /// cek downline kiri

										 if($d_detail['downline_kiri'] !=""){

											 $q_detail_2 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																				 DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																				 c.`nama_lengkap` AS nama_upline FROM member_master AS a

																				 INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																				 INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																				 LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kiri']."' ";

											 $d_detail_2 = $this->crut->list_row($q_detail_2);

											 if($d_detail_2 != 0){



												 $this->list_member_statement_kanan[$d_detail_2['id_member']] = $d_detail_2;

													 $this->list_member_statement_kanan[$d_detail_2['id_member']]['level'] = $level;

											 }

										 }

										 /// cek downline kiri

										 /// cek downline kanan

										 if($d_detail['downline_kanan'] !=""){

											 // echo $d_detail['id_member'].' '.$d_detail['downline_kanan'].'<br>';

											 $q_detail_3 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																				 DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																				 c.`nama_lengkap` AS nama_upline FROM member_master AS a

																				 INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																				 INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																				 LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kanan']."' ";

											 $d_detail_3 = $this->crut->list_row($q_detail_3);

											 if($d_detail_3 !=0){



												 $this->list_member_statement_kanan[$d_detail_3['id_member']] = $d_detail_3;

													 $this->list_member_statement_kanan[$d_detail_3['id_member']]['level'] = $level;

											 }

										 }

										 /// cek downline kanan

										 /// cek downline level 2

										 if($d_detail_2['downline_kiri'] !=""){

											 $this->data_statement_kanan($d_detail_2['downline_kiri'],$level);

										 }

										 if($d_detail_2['downline_kanan'] !=""){

											 $this->data_statement_kanan($d_detail_2['downline_kanan'],$level);

										 }



										 if(array_key_exists('downline_kiri',$d_detail_3) && $d_detail_3['downline_kiri'] !=""){

											 $this->data_statement_kanan($d_detail_3['downline_kiri'],$level);

										 }

										 if(array_key_exists('downline_kiri',$d_detail_3) && $d_detail_3['downline_kanan'] !=""){

											 $this->data_statement_kanan($d_detail_3['downline_kanan'],$level);

										 }

										 /// cek downline level 2

									 }

			 }





	 }




}

/* End of file User.php */
/* Location: ./application/controllers/admin718/User.php */
