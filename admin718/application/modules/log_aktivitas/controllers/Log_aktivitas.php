<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_aktivitas extends MY_Controller {

	protected $list_halaman = array(1=>'MemberMaster',2=>'BonusSponsor',3=>'BonusPasangan',4=>'BonusCabang',5=>'BonusReward');
	protected $list_operasi = array();


	public function fetch($offset = 0){
        $this->list_operasi = $this->kode_log_2;        
		$this->cek_hak_akses($this->privileges['log_aktivitas']['log_aktivitas'],'1|2');
		// $offset = $this->uri->segment(4);
		$filter = "";
		$halaman = $this->input->get('halaman',true);
		$jenis_operasi = $this->input->get('jenis_operasi',true);
		$id_member = $this->input->get('id_member',true);
		$pecah = explode(' | ',$id_member.' | ');
		$id_member = $pecah[0];
		$user_id = $this->input->get('user_id',true);
		$tgl_1 = $this->input->get('tgl_1',true);
        $tgl_2 = $this->input->get('tgl_2',true);
        

		if(!empty($halaman)){
			if(empty($filter)){
				$filter =" where a.halaman ='".$halaman."'";
			}else{
				$filter .=" and a.halaman ='".$halaman."'";
			}
		}

		if(!empty($jenis_operasi)){
			if(empty($filter)){
				$filter =" where a.aksi ='".$jenis_operasi."'";
			}else{
				$filter .=" and a.aksi ='".$jenis_operasi."'";
			}
        }
        
        if(!empty($id_member)){
			if(empty($filter)){
				$filter =" where a.id_member ='".$id_member."'";
			}else{
				$filter .=" and a.id_member ='".$id_member."'";
			}
		}


		if(!empty($ip_user)){
			if(empty($filter)){
				$filter =" where a.ip_user ='".$ip_user."'";
			}else{
				$filter .=" and a.ip_user ='".$ip_user."'";
			}
		}

		if(!empty($user_id)){
			if(empty($filter)){
				$filter =" where CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
			}else{
				$filter .=" and CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
			}
		}

		if(!empty($tgl_1) && !empty($tgl_2)){
			if(empty($filter)){
				$filter =" where date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
			}else{
				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
			}
		}



		 $q_log = " select	DATE_FORMAT(a.created_date, '%d-%m-%Y') AS tgl,
                    concat(
                        b.nama_depan,
                        ' ',
                        b.nama_belakang
                    ) AS nama_admin,
                    c.id_member as id_member,
                    c.nama_lengkap as nama_member,
                    a.halaman as halaman,
                    a.aksi as aksi
                FROM
                    log_aktivitas_2 AS a
                INNER JOIN admins AS b ON a.id_admin = b.user_id 
                INNER JOIN member_master as c on a.id_member = c.id_member ".$filter;

                $q_log_count = "select count(*) as total FROM
                    log_aktivitas_2 AS a
                INNER JOIN admins AS b ON a.id_admin = b.user_id 
                INNER JOIN member_master as c on a.id_member = c.id_member ".$filter;
 	        // 	$d_log = $this->crut->list_data($q_log);


		// echo $q_log;
		// die();

		$this->load->library('pagination');

		$config['base_url'] = site_url('log_aktivitas/fetch');

		$config['per_page'] = 50;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$list_logs = $this->crut->list_data($q_log,$config['per_page'],$offset);

		$config['total_rows'] = $this->crut->list_row($q_log_count)['total'];

		$this->pagination->initialize($config);

		$data['page_header'] ='Data Log';
		$data['url_admin'] = '';
		$data['url_add'] = '';
		$data['list_halaman'] = $this->list_halaman;
		$data['list_operasi'] = $this->list_operasi;
		// $data['side_menu'] = $this->side_menu;
		$data['list_logs'] = $list_logs;

		$q_operator = "select a.user_id, concat(a.nama_depan,' ',a.nama_belakang) as nama_admin, b.nama as `group` from admins as a 
		inner JOIN admin_groups as b on b.id = a.user_group_id ";
		$d_operator = $this->crut->list_datas($q_operator);
		$data['list_operator'] = $d_operator;
		$data['pagination'] = $this->pagination->create_links();



			// $data['contents'] = 'admin_group/add.tpl';


			$data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
			'plugins/select2/select2.min.css',
			'plugins/datepicker/datepicker3.css'
				);
				$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
								'plugins/select2/select2.min.js',
								'plugins/datepicker/bootstrap-datepicker.js'
				);

		$this->parser->parse("logs/fetch.tpl",$data);
		}


}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
