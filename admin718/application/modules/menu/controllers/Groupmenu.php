<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groupmenu extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->cek_hak_akses($this->privileges['menu_management']['menu'],'1|2');

	}

	public function index()
	{

	}


	public function add(){
		$this->cek_hak_akses($this->privileges['menu_management']['menu'],'2');

		$data['page_header'] ='Tambah Group Menu';
		$this->parser->parse("/groupadd.tpl",$data);

	}

	public function save(){
		$this->cek_hak_akses($this->privileges['menu_management']['menu'],'2');

		$this->form_validation->set_rules('menu_type', 'Kode Group Menu', 'trim|required|alpha');
		$this->form_validation->set_rules('nama', 'Nama Group Menu', 'trim|required|alpha_numeric_spaces');

		if ($this->form_validation->run() == FALSE) {
			$this->add();
		} else {
			$input['menu_type'] = $this->input->post('menu_type',true);
			$input['nama'] = $this->input->post('nama',true);
			$input['deskripsi'] = $this->input->post('deskripsi',true);
			$sukses = 'Proses Tambah Menu Group Dengan Nama = '.$input['nama'].' Berhasil';
			$gagal = 'Proses Tambah Menu Group Dengan Nama = '.$input['nama'].' Gagal';
			$pesan = $this->crut->insert('menu_type',$input,'Tambah Group Menu',$sukses,$gagal);
			$this->session->set_flashdata('pesan', pesan($pesan));
			redirect(site_url('menu/groupmenu/add'));
		}
	}

	public function fetch(){
		$this->cek_hak_akses($this->privileges['menu_management']['menu'],'1|2');

		$data['page_header'] ='Data Group Menu';
		$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
		$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
									'plugins/datatables/dataTables.bootstrap.min.js');

		$this->parser->parse("groupfetch.tpl",$data);
	}

	public function fetch_data(){
			 $column_order = array('menu_type','nama','deskripsi',null); //set column field database for datatable orderable keterangan
 			 $column_search = array('nama'); //set column field database for datatable searchable just nama
 			 $order = array('id' => 'desc'); // default order

				$q = "select * from menu_type ";
				$i = 0;
				foreach($column_search as $item){

					if($_POST['search']['value']) // if datatable send POST for search
		            {

		                $q.= " where $item like '%".$_POST['search']['value']."%' ";
		            }

		            $i++;
				}

				if(isset($_POST['order'])) // here order processing
		        {
		            $q.= " order by ".$column_order[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir']." ";
		        }
		        else if(isset($order))
		        {
		            $q.= "order by `id` DESC ";
		        }

				$hasil = $this->crut->list_datas($q);
				$total_data = $this->crut->list_count($q);

				$json = array(
						'draw'=>"",
						'recordsTotal' => $total_data,
						'recordsFiltered' => $total_data
					);
				foreach($hasil as $k => $v){
					$json['data'][] = array(
							'menu_type' => $v['menu_type'],
							'nama' =>$v['nama'],
							'keterangan' => $v['deskripsi'],
							'aksi' =>'<a href="'.site_url("menu/groupmenu/edit/".$v['id']).'" class="btn btn-warning btn-sm" title="Edit "><i class="fa fa-pencil"></i></a>
                          <a href="'.site_url("menu/groupmenu/delete/".$v['menu_type']).'" class="btn btn-danger btn-sm" title="Delete "><i class="fa fa-trash"></i></a>
						  <a href="'.site_url("menu/menuitem/fetch/".$v['menu_type']).'" class="btn btn-success btn-sm" title="Edit Menu Item "><i class="fa fa-sitemap"></i></a>
                          '
						);
				}

				$json = json_encode($json);
				echo $json;
	}

	public function edit($id = ''){
		$this->cek_hak_akses($this->privileges['menu_management']['menu'],'2');

		$q_detail = "select * from menu_type where id='".$id."'";
		$data['page_header'] ='Edit Group Menu';
		$data['detail'] = $this->crut->list_row($q_detail);
		$this->parser->parse("groupedit.tpl",$data);
	}

	public function update(){
		$this->cek_hak_akses($this->privileges['menu_management']['menu'],'2');

		$this->form_validation->set_rules('id', 'ID Group Menu', 'trim|required|numeric');
		$this->form_validation->set_rules('menu_type', 'Kode Group Menu', 'trim|required|alpha');
		$this->form_validation->set_rules('nama', 'Nama Group Menu', 'trim|required|alpha_numeric_spaces');

		if ($this->form_validation->run() == FALSE) {
			$this->edit($this->input->post('id'));
		} else {
			$filter = array('id'=>$this->input->post('id',true));
			$input['menu_type'] = $this->input->post('menu_type',true);
			$input['nama'] = $this->input->post('nama',true);
			$input['deskripsi'] = $this->input->post('deskripsi',true);
			$sukses = 'Proses Update Menu Group Dengan Nama = '.$input['nama'].' Berhasil';
			$gagal = 'Proses Update Menu Group Dengan Nama = '.$input['nama'].' Gagal';
			$pesan = $this->crut->update($filter,'menu_type',$input,'Update Group Menu',$sukses,$gagal);
			$this->session->set_flashdata('pesan', pesan($pesan));
			redirect(site_url('menu/groupmenu/fetch'));
		}
	}


	public function delete($menu_type =''){
		$this->cek_hak_akses($this->privileges['menu_management']['menu'],'2');


			$filter_item = array('menu_type'=>$menu_type);
			$filter_master = array('menu_type'=>$menu_type);
			$sukses = 'Proses Delete Menu Group Dengan Nama Group = '.$menu_type.' Berhasil';
			$gagal = 'Proses Delete Menu Group Dengan Nama Group = '.$menu_type.' Gagal';
			$this->crut->delete($filter_item,'menu','Delete Group Menu',$sukses,$gagal);
			$pesan = $this->crut->delete($filter_master,'menu_type','Delete Group Menu',$sukses,$gagal);
			$this->session->set_flashdata('pesan', pesan($pesan));
			redirect(site_url('menu/groupmenu/fetch'));
	}



}

/* End of file Groupmenu.php */
/* Location: ./application/modules/menu/controllers/Groupmenu.php */
