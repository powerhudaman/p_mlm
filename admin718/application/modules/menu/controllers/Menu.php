<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller {

	public function index()
	{
		
	}

	public function parameter(){
		$json = array();
		$q_menu = "select * from menu_type ";
		$menus = $this->crut->list_datas($q_menu);
		foreach ($menus as $k => $v) {
			$json[] = array('code'=>$v['id'],'nama'=>$v['nama']);
		}

		$json = json_encode($json);
		echo $json;
	}

}

/* End of file Menu.php */
/* Location: ./application/modules/menu/controllers/Menu.php */