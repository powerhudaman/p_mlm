{extends file='index_admin_.tpl'}
{block name=content}
  	
  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">
  					
  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                  {if $this->session->flashdata('pesan') !=""}
                    {$this->session->flashdata('pesan')}
                  {/if}
                </div><!-- /.box-header -->
                <!-- form start -->
                {form_open(site_url('menu/groupmenu/save'))}
                  <div class="box-body">
                    <div class="form-group">
                      {form_error('menu_type')}
                      <label for="">Kode Group Menu <span class="text-red">*</span></label>
                      <input type="text" name="menu_type" class="form-control" value="{set_value('menu_type')}">
                    </div>
                    <div class="form-group">
                      {form_error('nama')}
                      <label for="">Nama Group Menu <span class="text-red">*</span></label>
                      <input type="text" name="nama" class="form-control" value="{set_value('menu_type')}">
                    </div>
                    <div class="form-group">
                      {form_error('deskripsi')}
                      <label for="">Deskripsi</label>
                      <textarea name="deskripsi" id="" cols="30" rows="10" class="form-control">{set_value('menu_type')}</textarea>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}