{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">
              <div class="btn-group pull-right">
                  <a href="{site_url('menu/menuitem/fetch')}/{$detail['menu_type']}" class="btn btn-warning" title="Group Menu" ><i class="fa fa-undo"></i></a>
              </div>
  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                  {if $this->session->flashdata('pesan') !=""}
                    {$this->session->flashdata('pesan')}
                  {/if}
                </div><!-- /.box-header -->
                <!-- form start -->
                {form_open(site_url('menu/menuitem/update'))}
                  <div class="box-body">
                      <!-- row menu data begin -->
                      <div class="row">
                        <div class="col-md-9 col-sm-12">
                          <div class="form-group">
                            <label for="">Parrent</label>
                            <select name="level" id="" class="form-control">
                              <option value=""></option>
                              {foreach from=$list_menus key=k item=list_item}
                                {assign var=pilih value=''}
                                {if $detail['parent'] == $list_item['id']}
                                    {$pilih ='selected'}
                                {/if}
                                {assign var=level_tujuan value=$list_item['level']+1}
                                {if $list_item['id'] != $detail['id']}
                                  <option value="{$level_tujuan}:{$list_item['id']}" {$pilih} >{$list_item['nama']}</option>
                                {/if}
                              {/foreach}
                            </select>
                          </div>
                          <div class="form-group">
                            {form_error('publish')}
                            <label for="">Status Publish</label>
                            <select name="publish" id="" class="form-control">
                              <option value=""></option>
                              {foreach from=$status_publish key=k item=status}
                                {assign var=pilih value=''}
                                {if $detail['publish'] == $k}
                                    {$pilih ='selected'}
                                {/if}
                                <option value="{$k}" {$pilih}>{$status}</option>
                              {/foreach}
                            </select>
                          </div>
                          <div class="form-group">
                            {form_error('icons')}
                            <label for="">Icon</label>
                            <select name="icons" id="" class="form-control">
                              <option value=""></option>
                              {assign var=parameter value=json_decode($detail['parameter'],true)}
                              {foreach from=$fa_icons key=k item=v}
                                {assign var=pilih value=''}
                                {if array_key_exists('icons',$parameter) && $parameter['icons'] == $k}
                                  {$pilih='selected'}
                                {/if}
                                <option value="{$k}" {$pilih}>{$k}</option>
                              {/foreach}
                            </select>
                          </div>
                          <div class="form-group">
                            {form_error('status_menu_login')}
                            <label for="">Status Menu Login</label>
                            <select name="status_menu_login" id="" class="form-control">
                              <option value=""></option>
                              {foreach from=$status_menu_login key=k item=status}
                                {assign var=pilih value=''}
                                {if $detail['status_menu_login'] == $k}
                                    {$pilih ='selected'}
                                {/if}
                                <option value="{$k}" {$pilih}>{$status}</option>
                              {/foreach}
                            </select>
                          </div>
                          <div class="form-group">
                            {form_error('nama')}
                            <label for="">Nama Menu Item <span class="text-red">*</span></label>
                            <input type="text" name="nama" class="form-control" value="{$detail['nama']}">
                            <input type="hidden" name="menu_type" value="{$detail['menu_type']}">
                            <input type="hidden" name="id" value="{$detail['id']}">
                          </div>
                          <div class="form-group">
                            {form_error('ordering')}
                            <label for="">No Urut Posisi Menu</label>
                            <input type="text" name="ordering" class="form-control" value="{$detail['ordering']}">
                          </div>
                          <div class="form-group">
                            {form_error('link')}
                            <label for="">Link</label>
                            <textarea name="link" id="link" cols="30" rows="10" class="form-control">{$detail['link']}</textarea>
                          </div>
                        </div> <!-- end col-md-9 -->
                        <div class="col-md-3 col-sm-12" id="setting_link">
                            <div class="form-group">
                              <label for="">Halaman</label>
                              <select name="page" id="page" class="form-control">
                                <option value=""></option>
                                {foreach from=$list_page key=k item=page}
                                <option value="{$page['layout_id']}:{$page['component']}">{$page['nama']}</option>
                                {/foreach}
                              </select>
                              <input type="hidden" name="layout_id" id="layout_id" value="">
                            </div>

                            <div class="form-group" id="page_link_frame">
                              <label for="" id="page_link_label">Halaman</label>
                              <select name="page_link" id="page_link" class="form-control">
                              </select>
                            </div>

                            <div class="form-group" id="page_route_frame">
                              <label for="" id="page_route_label">Route</label>
                              <select name="page_route" id="page_route" class="form-control">
                              </select>
                            </div>
                      </div>
                      <!-- row menu data end -->
                      <!-- row menu permission begin -->
                      <div class="row">
                        <div class="col-md-9 col-sm-12">
                          <table class="table table-bordered table-striped">
                            <tr>
                              <td>Nama Group</td>
                              <td>Tanpa Session</td>
                              <td>Tanpa Akses</td>
                              <td>Bisa Akses</td>
                            </tr>
                            {if $detail['permission'] !=""}
                              {assign var=group_akses value=json_decode($detail['permission'],true)}
                              {foreach from=$list_group key=k item=group}
                                {assign var=cek_0 value=''}
                                {assign var=cek_1 value=''}
                                {assign var=cek_2 value=''}
                                {if $group_akses[$group['customer_group_id']] == 0}
                                    {$cek_0 ='checked'}
                                {/if}
                                {if $group_akses[$group['customer_group_id']] == 1}
                                    {$cek_1 ='checked'}
                                {/if}
                                {if $group_akses[$group['customer_group_id']] == 2}
                                    {$cek_2 ='checked'}
                                {/if}
                                  <tr>
                                    <td>{$group['nama']}</td>
                                    <td><input type="radio" name="hak_akses[{$group['customer_group_id']}]" value="2" id="" {$cek_2}></td>
                                    <td><input type="radio" name="hak_akses[{$group['customer_group_id']}]" value="0" id="" {$cek_0}></td>
                                    <td><input type="radio" name="hak_akses[{$group['customer_group_id']}]" value="1" id="" {$cek_1}></td>
                                  </tr>
                              {/foreach}
                            {/if}
                            {if $detail['permission'] ==""}
                              {foreach from=$list_group key=k item=group}
                                <tr>
                                  <td>{$group['nama']}</td>
                                  <td><input type="radio" name="hak_akses[{$group['customer_group_id']}]" value="2" id=""></td>
                                  <td><input type="radio" name="hak_akses[{$group['customer_group_id']}]" value="0" id=""></td>
                                  <td><input type="radio" name="hak_akses[{$group['customer_group_id']}]" value="1" id=""></td>
                                </tr>
                              {/foreach}
                            {/if}

                          </table>
                        </div>
                      </div>
                      <!-- row menu permission end -->
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}
{block name=script_js}

  <script>
  jQuery(document).ready(function($) {
    $('#page_link_frame').hide();
    $('#page_route_frame').hide();
  });
  $('#page').change(function() {
    var component = $('#page').val();
    var id = component.split(":");
    component = id[1];
    $('#layout_id').val(id[0]);
     var html ='';

    $.ajax({
      url: "{site_url('"+component+"/page_list')}",
      type: 'GET',
      dataType: 'json',
      success:function(data){
            html = '';
            html +='<option value=""></option>';
            $.each(data, function(index, val) {
               /* iterate through array or object */
               html +='<option value="'+val.link+'">'+val.nama+'</option>';
            });

        $('#page_link').html(html);
        $('#page_link_label').html(component);
        $('#page_link_frame').show();
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

    $.ajax({
      url: "{site_url('menu/menuitem/list_route')}/"+$('#layout_id').val(),
      type: 'GET',
      dataType: 'json',
      success:function(data){
        html = '';
        html +='<option value=""></option>';
        $.each(data, function(index, val) {
           /* iterate through array or object */
           html +='<option value="'+val.layout_route_id+'">'+val.route+'</option>';
           $('#page_route').html(html);
           $('#page_route_frame').show();
        });
      }
    });

  });

  $('#page_link').change(function(event) {
    var url_frontend = "{$url_frontend}";
    $('#link').val(url_frontend+$('#page_link').val());
  });
  </script>

{/block}
