<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cashflow extends MY_Controller {

	private $list_member_statement_kiri = array();
	private $list_member_statement_kanan = array();
	private $listing_downline_data = array();
	private $no_level_downline = 0;
	private $bonus_sponsor = 0;
	private $bonus_pasangan = 0;
	private $bonus_cabang= 0;
  /*
    bps	bonus pasangan
    bms	bonus sponsor
    bcb	bonus cabang
    bbr	bonus reward
    BAS	biaya admin sponsor
    BAPS	biaya admin pasangan
    FLS	flashout
  */
  private $jenis_transaksi = array('BPS'=>'Bonus Pasangan','BMS'=>'Bonus Sponsor','BCB'=>'Bonus Cabang','BBR'=>'Bonus Reward','BAS'=>'Biaya Admin Sponsor','BAPS'=>'Biaya Admin Pasangan','FLS'=>'Flashout','CP'=>'Pengeluaran','RMB'=>'Registrasi Member Baru','ATK'=>'ATK','LGST'=>'Logistik','INET'=>'Internet','TLP'=>'Telepon','LTR'=>'Listrik','BBM'=>'BBM','DPS'=>'Deposit Pulsa','KSM'=>'Konsumsi','CP'=>'Biaya Lain-lain');
	private $jenis_pengeluaran = array('ATK'=>'ATK','LGST'=>'Logistik','INET'=>'Internet','TLP'=>'Telepon','LTR'=>'Listrik','BBM'=>'BBM','DPS'=>'Deposit Pulsa','KSM'=>'Konsumsi','CP'=>'Biaya Lain-lain');
	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');

		$this->load->helper('security');
		// $this->cek_hak_akses($this->privileges['component']['member_master'],'1|2');
		$this->load->library('datatables');
		$this->bonus_sponsor = $this->crut->setting('komisi_configuration','bonus_sponsor','setting')['value'];
		$this->bonus_pasangan = $this->crut->setting('komisi_configuration','bonus_pasangan','setting')['value'];
		$this->bonus_cabang = $this->crut->setting('komisi_configuration','bonus_cabang','setting')['value'];
	}

  public function fetch(){
    $filter = "";
    if(isset($_GET['jenis_transaksi']) && $_GET['jenis_transaksi'] !=""){
      $filter = "and a.jenis_transaksi ='".$_GET['jenis_transaksi']."' ";
    }

    if(isset($_GET['tgl_1']) && isset($_GET['tgl_2']) && $_GET['tgl_1'] !="" && $_GET['tgl_2'] !=""){
      $filter .="and DATE_FORMAT(a.created_date,'%Y-%m-%d') between '".$_GET['tgl_1']."' and '".$_GET['tgl_2']."' ";
    }

    $q_cashflow = "SELECT a.`kode_transaksi` AS kode_transaksi,a.`jenis_transaksi` AS jenis_transaksi, a.`keterangan` AS keterangan, a.`debet` AS debet,
 a.`kredit` AS kredit,DATE_FORMAT(a.`created_date`,'%d-%m-%Y') AS tgl,CONCAT(b.`nama_depan`,' ',b.`nama_belakang`) AS nama FROM cashflow AS a
INNER JOIN admins AS b ON a.`created_by` = b.`user_id` where !isnull(b.user_id) ".$filter." ;";

// echo $q_cashflow;

    $d_cashflow = $this->crut->list_datas($q_cashflow);

    $q_kredit_now = "SELECT SUM(a.`kredit`) as total_kredit FROM cashflow AS a INNER JOIN admins AS b ON a.`created_by` = b.`user_id` where !isnull(b.user_id) ".$filter." ;";
    $d_kredit_now = $this->crut->list_row($q_kredit_now);

    $q_debet_now = "SELECT SUM(a.`debet`) as total_debet FROM cashflow AS a INNER JOIN admins AS b ON a.`created_by` = b.`user_id` where !isnull(b.user_id) ".$filter." ;";
    $d_debet_now = $this->crut->list_row($q_debet_now);



    $data['page_header'] ='Data Cashflow';
    $data['url_admin'] = ADMINS;
    $data['url_add'] = '';
    $data['list_cashflow'] = $d_cashflow;
    $data['jenis_transaksi'] = $this->jenis_transaksi;
    $data['t_kredit'] = $d_kredit_now;
    $data['t_debet'] = $d_debet_now;
    $data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
                              'plugins/select2/select2.min.css',
                              'plugins/datepicker/datepicker3.css'
                        );
    $data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
                                'plugins/select2/select2.min.js',
                                'plugins/datepicker/bootstrap-datepicker.js'
                        );
    $this->parser->parse("cashflow/fetch.tpl",$data);
  }

  public function tambah_pengeluaran(){
    $data['page_header'] ='Data Cashflow Tambah Pengeluaran';
    $data['url_admin'] = ADMINS;
		$data['jenis_pengeluaran'] = $this->jenis_pengeluaran;
    $data['url'] = 'cashflow/save_pengeluaran';
    $data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
                              'plugins/select2/select2.min.css',
                              'plugins/datepicker/datepicker3.css'
                        );
    $data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
                                'plugins/select2/select2.min.js',
                                'plugins/datepicker/bootstrap-datepicker.js'
                        );
    $this->parser->parse("cashflow/tambah_pengeluaran.tpl",$data);
  }

  public function save_pengeluaran(){
    // input cashflow
    $keterangan = $this->input->post('keterangan');
    $input_cashflow['kode_transaksi'] = $this->input->post('jenis_pengeluaran').genRndString(5,'1234567890');
    $input_cashflow['tp'] = '2';
    $input_cashflow['jenis_transaksi'] = $this->input->post('jenis_pengeluaran');
    $input_cashflow['debet'] = $this->input->post('nominal');
    $input_cashflow['keterangan'] = $keterangan;
    $input_cashflow['created_date'] = date('Y-m-d H:i:s');
    $input_cashflow['created_by'] = $this->session->userdata('user_id');
    $pesan = $this->crut->insert('cashflow',$input_cashflow,'','Tambah Pengeluaran Sukses','Tambah Pengeluaran Gagal');
    $this->session->set_flashdata('pesan',pesan($pesan));
    redirect(site_url('cashflow/fetch'));
    // input cashflow
  }



}
