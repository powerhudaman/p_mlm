{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">
            <a href="{site_url('cashflow/tambah_pengeluaran')}" class="btn btn-primary pull-right" title="Tambah Pengeluaran" ><i class="fa fa-plus"></i></a>
  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">

			<div class="box collapsed-box">
              <div class="box-header with-border">
                <h3 class="box-title">{$page_header}</h3>
                {if $this->session->flashdata('pesan') !=""}
                {$this->session->flashdata('pesan')}
              {/if}
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                      {assign var=aturan_form value=['method'=>'get']}
                      {form_open(site_url('cashflow/fetch'),$aturan_form)}
                      <!-- /.form-group -->
                      <div class="form-group">
                          <label for="">Jenis Transaksi</label>
                          <select name="jenis_transaksi" id="jenis_transaksi" class="form-control">
                            <option value=""></option>
                            {foreach from=$jenis_transaksi key=k item=i}
                              <option value="{$k}">{$i}</option>
                            {/foreach}
                          </select>
                      </div>
                      <!-- /.form-group -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Tanggal Mulai</label>
                        <input type="text" name="tgl_1" id="tgl_1" class="form-control">
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label for="">Tanggal Selesai</label>
                        <input type="text" name="tgl_2" id="tgl_2" class="form-control">
                    </div>
                    <!-- /.form-group -->
                  </div>


                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <input type="submit" value="Cari" class="btn btn-success">
              </div>
              </div>
              {form_close()}


              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">

                  <a href="{site_url('komisi/bonus_pasangan/export')}" class="btn btn-primary pull-left">Export</a>
                </div><!-- /.box-header -->
                <!-- form start -->

                  <div class="box-body">

                    <div class="row">
                      <div class="col-md-12">
                        <div class="table-responsive">
                          <table id="" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Kode Transaksi</th>
                                <th>Jenis Transaksi</th>
                                <th>Keterangan</th>
                                <th>Debet</th>
                                <th>Kredit</th>
                                <th>Saldo</th>
                                <th>Tanggal</th>
                              </tr>
                            </thead>
                            <tbody>
                              {if $list_cashflow == 0}
                                  <tr>
                                    <td colspan="7">Data Cashflow tidak ditemukan</td>
                                  </tr>
                              {/if}
                              {if $list_cashflow != 0}
                                {assign var=no value=$offset}
                                {assign var=n_saldo value='0'}
                                {foreach from=$list_cashflow key=k item=v}
                                  {if $v['debet'] > 0 && $v['kredit'] == 0 || $v['kredit']==""}
                                    {$n_saldo = $n_saldo - $v['debet']}
                                  {/if}
                                  {if $v['kredit'] > 0 && $v['debet'] == 0 || $v['debet']==""}
                                    {$n_saldo = $n_saldo + $v['kredit']}
                                  {/if}


                                  {assign var=no value=$no+1}
                                  <tr>
                                    <td>{$no}</td>
                                    <td>{$v['kode_transaksi']}</td>
                                    <td>{$jenis_transaksi[$v['jenis_transaksi']]}</td>
                                    <td>{$v['keterangan']}</td>
                                    <td>{number_format($v['debet'],0,",",".")}</td>
                                    <td>{number_format($v['kredit'],0,",",".")}</td>
                                    <th>{number_format($n_saldo,0,",",".")}</th>
                                    <td>{$v['tgl']}</td>
                                  </tr>

                                {/foreach}
                              {/if}
                            </tbody>
                            <tfoot>
                              <!-- <tr>
                                <td colspan="5">Saldo Awal</td>
                                <td></td>
                                <td></td>
                              </tr> -->
                              <tr>
                                <td colspan="5">Total Kredit</td>

                                <td colspan="3">Rp. {number_format($t_kredit['total_kredit'],0,",",".")}</td>
                              </tr>
                              <tr>
                                <td colspan="5">Total Debet</td>

                                <td colspan="3">Rp. {number_format($t_debet['total_debet'],0,",",".")}</td>
                              </tr>
                              <tr>
                                <td colspan="5">Kredit - Debet</td>

                                <td colspan="3">Rp. {number_format(($t_kredit['total_kredit']-$t_debet['total_debet']),0,",",".")}</td>
                              </tr>
                              <!-- <tr>
                                <td colspan="5">Total dengan saldo awal</td>
                                <td></td>
                                <td></td>
                              </tr> -->
                            </tfoot>
                          </table>
                        </div>

                      </div>
                    </div>
                  </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div>

  	</div>


{/block}

{block name=script_js}
  <script>

    $('#penerima').autocomplete({
      source:"{site_url('komisi/bonus_sponsor/autocomplete_member')}",
      delay: 1000
    });
    $('#produk_id').autocomplete({
      source:"{site_url('cart_catalog/produk_edit/autocomplete_produk_id')}",
      delay: 1000
    });
    $('#judul_produk').autocomplete({
      source:"{site_url('cart_catalog/produk_edit/autocomplete_judul')}",
      delay: 1000
    });
    $("#tgl_1").datepicker({ format:'yyyy-mm-dd' });
    $("#tgl_2").datepicker({ format:'yyyy-mm-dd' });

  </script>

{/block}
{block name=sidebar-menu}
   {$side_menu}
{/block}
