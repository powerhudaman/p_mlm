{extends file='index_admin_.tpl'}
{block name=content}

  	<div class="row">
  		<!--<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>-->
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                {form_open_multipart(site_url($url))}
                  <div class="box-body">

                    <!--  <div class="form-group">
                        <label for=""><span class="text-red">*</span> Id Member</label>
                        <input type="text" name="id_member" id="" class="form-control" readonly value="{$id_member}">
                      </div> -->
                      <div class="form-group">
                        <label for="">Kategori Pengeluaran</label>
                        <select name="jenis_pengeluaran" id="jenis_pengeluaran" class="form-control">
                          {foreach from=$jenis_pengeluaran key=k item=i}
                            <option value="{$k}">{$i}</option>
                          {/foreach}
                        </select></div>
                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>Keterangan</label>
                        <textarea type="text" name="keterangan" id="" class="form-control" required></textarea>
                      </div>

                      <div class="form-group">
                        <label for=""><span class="text-red">*</span>Nominal Pengeluaran</label>
                        <input type="number" name="nominal" id="nominal" class="form-control" required>
                      </div>


                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>

                {form_close()}
              </div><!-- /.box -->
            </div>

  	</div>

{/block}
{block name=script_js}
  <script>
   jQuery(document).ready(function($) {

   $('#id_sponsor').autocomplete({
          source:"{site_url('member_master/autocomplete_sponsor')}",
          select:function(event,data){
          }
        });
     $('#id_upline').autocomplete({
          source:"{site_url('member_master/autocomplete_upline')}",
          select:function(event,data){
          }
        });

   });

    $("#propinsi_asal_ori").change(function(){

      $("#kecamatan_asal_ori").hide();
      $("#kota_asal_ori").hide();

        var propinsi_origin = $("#propinsi_asal_ori").val();
          var urls = '{site_url("member_master/kota_html")}';
          $.ajax({
            //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
            url:urls+'?propinsi_origin='+propinsi_origin,
            type: 'GET',
            dataType: 'html',
            success:function(data){
                var html = data;
                    $("#kota_asal_ori").html(html);
                    $("#kota_asal_ori").show();
            }
          });

      })

    // kecamatan ori
    $("#kota_asal_ori").change(function(){
      $("#kecamatan_asal_ori").hide();
        var propinsi_origin = $("#propinsi_asal_ori").val();
        var kota_origin = $("#kota_asal_ori").val();
          var urls = '{site_url("member_master/kecamatan_html")}';
          $.ajax({
            //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
            url:urls+'?propinsi_origin='+propinsi_origin+'&kota_origin='+kota_origin,
            type: 'GET',
            dataType: 'html',
            success:function(data){
                var html = data;

                    $("#kecamatan_asal_ori").html(html);
                    $("#kecamatan_asal_ori").show();
            }
          });

      })


      /// setingan untuk rajaongkir

            {if !isset($kota_asal) && !isset($kecamatan_asal) && !isset($kota_tujuan) && !isset($kecamatan_tujuan)}
                // $("#kota_asal_ori").hide();
                // $("#kecamatan_asal_ori").hide();
                $("#kota_asal").hide();
                $("#kecamatan_asal").hide();
                  $("#kota_tujuan").hide();
                $("#kecamatan_tujuan").hide();
            {/if}

  </script>
{/block}
