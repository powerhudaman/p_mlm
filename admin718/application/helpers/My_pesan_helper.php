<?php

	function pesan($data = array()){
		if($data['status'] && $data['kode'] == 1){
			$pesan = '<div class="alert alert-success alert-dismissable">
	                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                    '.$data['pesan'].'
	                  </div>';
		}
		if(!$data['status'] && $data['kode'] == 2){
			$pesan = '<div class="alert alert-danger alert-dismissable">
	                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                    '.$data['pesan'].'
	                  </div>';
		}


		return $pesan;
	}

	function genRndString($length = 10, $chars = '1234567890BCDFGHJKLMNPQRSTVWXYZ')
		{
		    if($length > 0)
		    {
		        $len_chars = (strlen($chars) - 1);
		        $the_chars = $chars{rand(0, $len_chars)};
		        for ($i = 1; $i < $length; $i = strlen($the_chars))
		        {
		            $r = $chars{rand(0, $len_chars)};
		            if ($r != $the_chars{$i - 1}) $the_chars .=  $r;
		        }

		        return $the_chars;
		    }
	}

	function genRndno($length = 10, $chars = '1234567890')
		{
				if($length > 0)
				{
						$len_chars = (strlen($chars) - 1);
						$the_chars = $chars{rand(0, $len_chars)};
						for ($i = 1; $i < $length; $i = strlen($the_chars))
						{
								$r = $chars{rand(0, $len_chars)};
								if ($r != $the_chars{$i - 1}) $the_chars .=  $r;
						}

						return $the_chars;
				}
		}


	function kategori_tree($data = array()){
		$output = array();
		foreach($data as $k =>$v){
			if($v['p_id'] == ""){
				if(!array_key_exists('url_title',$v)){
					$output[$v['child_id']] = array('id'=>$v['child_id'],'nama'=>$v['nama'],'subs'=>array());
				}else{
					$output[$v['child_id']] = array('id'=>$v['child_id'],'nama'=>$v['nama'],'url_title'=>$v['url_title'],'subs'=>array());
				}
			}
			if($v['p_id'] !="" && array_key_exists($v['p_id'], $output)){
				if(!array_key_exists('url_title',$v)){
					$output[$v['p_id']]['subs'][] = array('id'=>$v['child_id'],'nama'=>'&nbsp;&nbsp;'.$v['nama']);
				}else{
					$output[$v['p_id']]['subs'][] = array('id'=>$v['child_id'],'nama'=>'&nbsp;&nbsp;'.$v['nama'],'url_title'=>$v['url_title']);
				}
			}
		}

		return $output;
	}

	function link_generator($url="",$kode=""){
		$string ='';
		$string ='<a href="'.site_url($url."/edit/".$kode).'" class="btn btn-warning btn-sm" title="Edit "><i class="fa fa-pencil"></i></a>
		          <a href="'.site_url($url."/delete/".$kode).'" class="btn btn-danger btn-sm" title="Delete "><i class="fa fa-trash"></i></a>';
		return $string;
	}

	function link_generator_array($parameter = array(),$kode=""){
		$string ='';
		foreach($parameter as $k => $v){
			$string .='<a href="'.$v['url'].'/'.$kode.'" class="'.$v['class'].'" title="'.$v['title'].'"><i class="'.$v['simbol'].'"></i></a>';
		}

		return $string;
	}

	/**
	 * [sub_generator_kategori digunakan untuk melakukan generate data sub parent kategori produk]
	 * @param  array  $data [description]
	 * @return [type]       [description]
	 */
	function sub_generator_kategori($data = array(),$total_child = 2){
		$output = array();
		$separator = '';
		for($i=0; $i < $total_child; $i++){
			if($i > 0){
				$separator.='--';
			}
			$level = $i + 1;
			foreach ($data as $key => $value) {
				if($value['level'] == $level){
						if($level == 1){
							$output[$value['id']] = array('id'=>$value['id'],'nama'=>$value['nama'],'level'=>$i,'subs'=>array());
						}
						if($level > 1){
							$output[$value['parent_id']]['subs'][] = array('id'=>$value['id'],'nama'=>$separator.' '.$value['nama'],'level'=>$i,'subs'=>array());
						}
				}
			}
		}
		return $output;
	}

	function sms_zensiva($pesan = '',$telepon=''){
		$userkey = "yqos91"; //userkey lihat di zenziva
		$passkey = "cbmjaya"; // set passkey di zenziva
		$message = $pesan;
		$url = "https://alpha.zenziva.net/apps/smsapi.php";
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($message));
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		curl_setopt($curlHandle, CURLOPT_POST, 1);
		$results = curl_exec($curlHandle);
		curl_close($curlHandle);

		$XMLdata = new SimpleXMLElement($results);
		$status = $XMLdata->message[0]->text;
		return $status;

	}
