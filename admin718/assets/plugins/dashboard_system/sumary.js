"use strict";
angular.module('app')
.controller('ctrl_sumary',['$scope','$http',function($scope,$http){
  $scope.urls = app['url_sumary']+'/sumary_sales_day';
  $scope.labels = [];
  $scope.series = ['Brutto', 'Netto','Omset'];
  $scope.colors = ['Blue','Green','Yellow'];
  $scope.data = [
    [],
    [],
    []
  ];
  $scope.tampilkan_sales_day = function(){
    $scope.labels = [];
    $scope.data = [
      [],
      [],
      []
    ];
    $http({
        url: app['url_sumary']+'/sumary_sales_day',
        method: "POST",
        data: { 'tgl_mulai' : $scope.tgl_mulai,'tgl_sampai':$scope.tgl_sampai }
    }).then(function(r_day){
      if(r_day.status == 200){
         angular.forEach(r_day.data, function(value, key) {
           this.push(value.tgl);
         }, $scope.labels);

         angular.forEach(r_day.data, function(value, key) {
           this.push(value.total_brutto);
        }, $scope.data[0]);

        angular.forEach(r_day.data, function(value, key) {
          this.push(value.total_netto);
       }, $scope.data[1]);

       angular.forEach(r_day.data, function(value, key) {
         this.push(value.total_omset);
      }, $scope.data[2]);

        $scope.onClick = function (points, evt) {
          console.log(points, evt);
        };
        $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        $scope.options = {
          scales: {
            yAxes: [
              {
                id: 'y-axis-1',
                type: 'linear',
                display: true,
                position: 'left',
                ticks: {
                    callback: function(label, index, labels) {
                        return label/1000+'k';
                    }
                },
                scaleLabel: {
                    display: true,
                    labelString: '1k = 1000'
                }
              },
              {
                id: 'y-axis-2',
                type: 'linear',
                display: true,
                position: 'right',
                ticks: {
                    callback: function(label, index, labels) {
                        return label/1000+'k';
                    }
                },
                scaleLabel: {
                    display: true,
                    labelString: '1k = 1000'
                }
              }
            ]
          }
        };
      }
    });
  };

  $scope.autoTampil = function(){
    var sekarang = new Date();
    var tahun = sekarang.getFullYear();
    var bulan = sekarang.getMonth()+1;
    var hari = sekarang.getDate();
    var bulans = '0'+bulan;
    if(bulan < 10){
      bulan = bulans;
    }

    $scope.tgl_mulai = tahun+'-'+bulan+'-01';
    $scope.tgl_sampai = tahun+'-'+bulan+'-'+hari;

    $scope.tampilkan_sales_day();

  }

  $scope.autoTampil();

}])
