"use strict";
angular.module('app')
.controller('ctrl_produk',['$scope','$http',function($scope,$http){
  var url = app['url_global'];
  $scope.gridOptions = {
      data: [],
      totalItems:0
  };
  $http({
    method: 'GET',
    url:url+'cart_dashboard/cart_dashboard_api/data_produk'
  }).then(function successCallback(response) {
      $scope.gridOptions.data = response.data;
      if(response.data.length == 'undefined'){
        $scope.gridOptions.totalItems = 0;
      }else{
        $scope.gridOptions.totalItems = response.data.length;
      }
  });

  $scope.edit_produk = function(order_id){
    window.open(url+'/cart_catalog/produk/edit/'+order_id, '_blank');
  }

}])
