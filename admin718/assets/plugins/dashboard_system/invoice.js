"use strict";
angular.module('app')
.controller('ctrl_invoice',['$scope','$http','$uibModal','$window',function($scope,$http,$uibModal,$window){
  var url = app['url_global'];
  $scope.url = url;
  $scope.total_invoice = 0;
  $scope.status_order_aktif;
  $scope.status_orders = []; // data status order
  $scope.orders = []; // data \order atau invoice
  $scope.gridOptions = {
      data: [],
      totalItems:0
  };


        // ambil data status order
        $http({
          method: 'GET',
          url: url+'/cart_dashboard/cart_dashboard_api/status_order'
        }).then(function successCallback(response) {
            $scope.status_orders = response.data;
        });

  // fungsi untuk menampilkan data order sesuai dengan status
  $scope.data_order = function(status_order){
    $scope.status_order_aktif = status_order;
    $scope.gridOptions.data = [];
    $http({
      method: 'GET',
      url:url+'/cart_dashboard/cart_dashboard_api/data_order/'+status_order
    }).then(function successCallback(response) {
        $scope.tutup();

        if(response.data == 0){
          $scope.gridOptions.totalItems = 0;
          $scope.total_invoice = 0;
          $scope.gridOptions.data = [];


        }else{

          $scope.gridOptions.totalItems = response.data.length;
          $scope.total_invoice = response.data.length;
          $scope.gridOptions.data = response.data;

        }

        // console.log($scope.gridOptions.data);
    });


  }
  // fungsi untuk melakukan refresh semua order
  $scope.refresh_order = function(){
        $scope.data_order($scope.status_order_aktif);
  }

  $scope.cetak_order = function(order_id){
    var urls = url+'/cart_order/cart_order/cetak_shipping/'+order_id;
    $window.open(urls,'','width=1366,height=768,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
  }

  $scope.detail_produks =[];
  $scope.order_totals =[];
  $scope.data_pembayarans =[];
  $scope.data_historys =[];
  $scope.data_customer = [];
  $scope.data_penerima = [];
  $scope.data_pengiriman = [];
  $scope.detail_order = function(order_id){
      // open modal
      // $scope.showModal = true;
      //// detail produk order
      $http({
        method: 'GET',
        url:url+'/cart_dashboard/cart_dashboard_api/data_order_produk/'+order_id
      }).then(function successCallback(response) {
          $scope.detail_produks = response.data;
      });

      // data order total
      $http({
        method: 'GET',
        url:url+'/cart_dashboard/cart_dashboard_api/data_order_total/'+order_id
      }).then(function successCallback(response) {
          $scope.order_totals = response.data;
      });
      // data pembayaran
      $http({
        method: 'GET',
        url:url+'/cart_dashboard/cart_dashboard_api/history_pembayaran/'+order_id
      }).then(function successCallback(response) {
          $scope.data_pembayarans = response.data;
      });

      // data log
      $http({
        method: 'GET',
        url:url+'/cart_dashboard/cart_dashboard_api/history_log/'+order_id
      }).then(function successCallback(response) {
          $scope.data_historys = response.data;
      });

      //data_customer
      $http({
        method: 'GET',
        url:url+'/cart_dashboard/cart_dashboard_api/data_customer/'+order_id
      }).then(function successCallback(response) {
          $scope.data_customer = response.data;
      });

      //data penerima
      $http({
        method: 'GET',
        url:url+'/cart_dashboard/cart_dashboard_api/data_penerima/'+order_id
      }).then(function successCallback(response) {
          $scope.data_penerima = response.data;
      });

      //data pengiriman
      $http({
        method: 'GET',
        url:url+'/cart_dashboard/cart_dashboard_api/data_pengiriman/'+order_id
      }).then(function successCallback(response) {
          $scope.data_pengiriman = response.data;
      });
  }

  $scope.tutup = function(){
    $scope.detail_produks =[];
    $scope.order_totals =[];
    $scope.data_pembayarans =[];
    $scope.data_historys =[];
    $scope.data_customer = [];
    $scope.data_penerima = [];
    $scope.data_pengiriman = [];
  }

  $scope.cancel = function() {
    $scope.showModal = false;
    $scope.tutup();
  };

  $scope.edit_order = function(order_id){
    window.open(url+'/cart_order/cart_order/edit_status/'+order_id, '_blank');
  }

}])
