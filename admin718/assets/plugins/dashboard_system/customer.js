"use strict";
angular.module('app')
.controller('ctrl_customers',['$scope','$http',function($scope,$http){
  var url = app['url_global'];
  $scope.gridOptions = {
      data: [],
      totalItems:0
  };
  $http({
    method: 'GET',
    url:url+'cart_dashboard/cart_dashboard_api/data_customers'
  }).then(function successCallback(response) {
      $scope.gridOptions.data = response.data;
      if(response.data.length == 'undefined'){
        $scope.gridOptions.totalItems = 0;
      }else{
        $scope.gridOptions.totalItems = response.data.length;
      }
  });

  $scope.edit_customer = function(customer_id){
    window.open(url+'/customer/customer/edit/'+customer_id, '_blank');
  }

}])
