<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_bersama {

    private $url = 'http://o.tukardata.com';

    public function hitung_biaya($asal,$tujuan,$berat){
        $curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $this->url.'/biaya/'.$asal.'/'.$tujuan.'/'.$berat,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10000,
		  CURLOPT_TIMEOUT => 3000,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		$data ='';
		if ($err) {
		  $data =  "cURL Error #:" . $err;
		} else {
		  $data = $response;
		}
		return $data;
    }

    public function kurirs(){
        $data['kurirs'][] =array('kode'=>'JNE',
                                 'nama_kurir'=>'JNE',
                                 'layanan'=>array(0=>array(
                                                      'kode_layanan'=>'reg',
                                                      'nama_layanan'=>'Reguler'
                                                      ),
                                                  1=>array(
                                                      'kode_layanan'=>'jtr',
                                                      'nama_layanan'=>'Jne Truck'
                                                      )
                                                 )
                                 );
        $data['kurirs'][] =array('kode'=>'POS',
                                 'nama_kurir'=>'POS',
                                 'layanan'=>array(0=>array(
                                                    'kode_layanan'=>'kilat_khusus',
                                                    'nama_layanan'=>'Kilat Khusus'
                                                    )
                                           )
                                 );
        $data['kurirs'][] =array('kode'=>'TIKI',
                                 'nama_kurir'=>'Titipan Kilat',
                                 'layanan'=>array(0=>array(
                                                      'kode_layanan'=>'ss',
                                                      'nama_layanan'=>'SS'
                                                      ),
                                                 1=>array(
                                                      'kode_layanan'=>'hds',
                                                      'nama_layanan'=>'Holiday Services'
                                                      ),
                                                    2=>array(
                                                        'kode_layanan'=>'ons',
                                                        'nama_layanan'=>'One Night Services'
                                                        ),
                                                    3=>array(
                                                      'kode_layanan'=>'reg',
                                                      'nama_layanan'=>'Reguler'
                                                      ),
                                                    4=>array(
                                                        'kode_layanan'=>'eco',
                                                        'nama_layanan'=>'Economic'
                                                        ),

                                           )
                                 );

        return $data['kurirs'];
    }
}