<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_validation extends CI_Form_validation {

	public function myAlpha($string) 
    {
        if ( !preg_match("/^[a-zA-Z .,\-']+$/i",$string) )
        {
            return false;
        }
    }

}

/* End of file My_validation.php */
/* Location: ./application/libraries/My_validation.php */