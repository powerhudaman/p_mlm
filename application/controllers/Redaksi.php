<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Redaksi extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        if($this->site_status !='1'){
            redirect(site_url('maintenance/index'));
        }
        $themes_name = $this->themes_name;
        $this->parser->set_theme($themes_name);

    }

    public function how_to_buy(){
      $themes_name = $this->themes_name;
      $data['themes'] = $themes_name."/index.tpl";

      $view = $themes_name.'/redaksi/how_to_buy.tpl';
      $this->parser->parse($view,$data,false,false,$themes_name);
    }
}