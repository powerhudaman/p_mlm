<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Smartytest extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        if($this->site_status !='1'){
            redirect(site_url('maintenance/index'));
        }
        $themes_name = $this->themes_name;
        $this->parser->set_theme($themes_name);

    }
    public function index(){
        $this->cadangan ='
        <!-- Product Lists
        ================================================== -->
        <div class="container margin-bottom-25">

          <!-- Fancy -->
          <div class="one-third column">

            <!-- Headline -->
            <h3 class="headline">Fancy</h3>
            <span class="line margin-bottom-0"></span>
            <div class="clearfix"></div>


            <ul class="product-list">

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">PAPER BAG <i>2.500</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Bobble Head Ultraman <i>65000</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Bobble Head Mickey Minnie… <i>60000</i></div>
              </a></li>

              <li><div class="clearfix"></div></li>

            </ul>

          </div>
          <!--Fancy end -->

          <!-- Merchandise -->
          <div class="one-third column">

            <!-- Headline -->
            <h3 class="headline">Merchandise</h3>
            <span class="line margin-bottom-0"></span>
            <div class="clearfix"></div>


            <ul class="product-list">

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Kaos Vocaloid Hatsune Miku… <i>125000</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">KAOS IRONMAN COSPLAY <i>102000</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">JAKET NARUTO HINATA SIPUUDEN… <i>175000</i></div>
              </a></li>

              <li><div class="clearfix"></div></li>

            </ul>

          </div>
          <!--Merchandise end -->

          <!-- Stationary -->
          <div class="one-third column">

            <!-- Headline -->
            <h3 class="headline">Stationary</h3>
            <span class="line margin-bottom-0"></span>
            <div class="clearfix"></div>


            <ul class="product-list">

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Maped Gunting 4 <i>9900</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Eversac Backpack Basic Khaki… <i>284050</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Troos Backpack Korean Style… <i>160000</i></div>
              </a></li>

              <li><div class="clearfix"></div></li>

            </ul>

          </div>
          <!--Stationary end -->
        </div>

<div class="container margin-bottom-25">

          <!-- Komputer -->
          <div class="one-third column">

            <!-- Headline -->
            <h3 class="headline">Komputer</h3>
            <span class="line margin-bottom-0"></span>
            <div class="clearfix"></div>


            <ul class="product-list">

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Keyboard Gaming <i>170000</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Mouse Gaming <i>68000</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Microsd <i>10150</i></div>
              </a></li>

              <li><div class="clearfix"></div></li>

            </ul>

          </div>
          <!--Komputer end -->

          <!-- Buku Best Seller -->
          <div class="one-third column">

            <!-- Headline -->
            <h3 class="headline">Buku Best Seller</h3>
            <span class="line margin-bottom-0"></span>
            <div class="clearfix"></div>


            <ul class="product-list">

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Percy Jackson and Olympians…<i>45900</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">THE MAZE RUNNER MOVIE… <i>58650</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Komik KKPK Me vs Robot <i>5000</i></div>
              </a></li>

              <li><div class="clearfix"></div></li>

            </ul>

          </div>
          <!--Buku Best Seller end -->

          <!-- Edu Toys -->
          <div class="one-third column">

            <!-- Headline -->
            <h3 class="headline">Edu Toys</h3>
            <span class="line margin-bottom-0"></span>
            <div class="clearfix"></div>


            <ul class="product-list">

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">Mainan Battleship <i>38250</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">CRANE CONSTRUCTION VEHICLE <i>485100</i></div>
              </a></li>

              <li><a href="#">
                <img src="images/small_product_list_01.jpg" alt="">
                <div class="product-list-desc">LEGO ANGRY BIRDS PIGGY… <i>567000</i></div>
              </a></li>

              <li><div class="clearfix"></div></li>

            </ul>

          </div>
          <!--Edu Toys end -->
        </div>

        ';
        // die();
        $themes_name = $this->themes_name;
        $data['themes'] = $themes_name."/index.tpl";
        $this->parser->parse('coba_menu.tpl',$data);
    }

    /**
     * Showing off Smarty 3 template inheritance features
     *
     */
    public function inheritance()
    {
        // Some example data
        $data['title'] = "The Smarty parser works with template inheritance!";
        $data['body']  = "This is body text to show that Smarty 3 template inheritance works with Smarty Parser.";

        // Load the template from the views directory
        $this->parser->parse("inheritancetest.tpl", $data);

    }

    public function coba(){
        $this->load->controller('mod_menu','mod_menu');
        // var_dump($data);
    }

    public function json(){

//echo'json';

  		$rawdata = file_get_contents("php://input");

  		if(empty($rawdata)) {

  			echo 'data kosong'; /* real-nya redirect saja ke halaman belanja */

  			die();

  		} else {

  			$data = json_decode($rawdata, true);

  			$scode = md5('M4sP1on');



			//echo $scode;

  			if ($scode !== $data['scode']) {

  				echo "ERROR CODE";

  				die();

  			}

  			$fungsi = $data['request'];

  			$this->$fungsi($data);

  		}

  	}



    public function master_produk($p){

      $tampung_data = array(); // variabel untuk menampung data

      $data_sukses = array();// variabel untuk menampung data yang sukses

      $hitung = 0;

      foreach ($p['detail'] as $key => $value) {

        /// vendor harus ambil id dulu

        $q_vendor = "select vendor_id from vendor where kode_supplier ='".$value['supplier']."'";

        $d_vendor = $this->crut->list_row($q_vendor);

        $value['vendor_id'] = $d_vendor['vendor_id'];

        // cek produk sudah pernah di input atau belum

        $q_cek = "select bara from produk where bara = '".$value['kode_barang']."'";

        $d_cek = $this->crut->list_count($q_cek);

        $tampung_data['kode_barang'] = $value['kode_barang'];

        $tampung_data['nama_barang'] = $value['nama_barang'];

        $tampung_data['vendor_id'] = $value['vendor_id'];

        $tampung_data['pengarang'] = $value['pengarang'];

        $tampung_data['harga_beli'] = $value['harga_beli'];

        $tampung_data['harga_jual'] = $value['harga_jual'];

        $tampung_data['tgl_entry'] = $value['tgl_entry'];



        if($d_cek > 0){

          $response = $this->update($tampung_data);

          if($response['status'] && $response['kode'] == 1){

	    	    $data_sukses[] = $tampung_data;

            $hitung++;

          }



        }

        if($d_cek == 0){



	        $response = $this->insert($tampung_data);

          if($response['status'] && $response['kode'] == 1){

	    	    $data_sukses[] = $tampung_data;

            $hitung++;

          }

        }

      }

      if ($hitung == 0) {

  			$res['respone'] = "NO";

  			$res['msg'] = "Data Gagal di Update";

  			$res['detail'] = $data_sukses;

  		} else {

  			$res['respone'] = "OK";

  			$res['msg'] = 'INSERT OK Total Data '.$hitung;

  			$res['detail'] = $data_sukses;

  		}

  		echo json_encode($res);

  		return json_encode($res);

      /// proses looping data







    }



    public function insert($data){

      $input['bara'] = $data['kode_barang'];

  		$input['nama'] = $data['nama_barang'];


  		$input['url_title'] = url_title($input['nama'],'_',true);


  		$input['stock_status_id'] = '1';

  		$input['vendor_id'] = $data['vendor_id'];

  		$input['pengarang_id'] = $data['pengarang'];

  		// $input['shipping'] = $data['shipping'];

      $input['harga_beli'] = $data['harga_beli'];

  		$input['harga'] = $data['harga_jual'];


  		$input['date_show'] = $data['tgl_entry'];

  		$input['date_available'] = $data['tgl_entry'];


  		$input['kurangi_stock'] = '1';

  		$input['minimum'] = '1';

  		$input['jenis_produk'] = 'Reguler';

  		$input['status'] = '1';

  		$input['date_added'] = $data['tgl_entry'];

  		$input['status_delete'] = '0';

  		$input['is_newrelease'] = '0';

  		$input['is_bestseller'] = '0';

  		$input['is_recommended'] = '0';

  		$input['is_bestofthemonth'] = '0';

  		$input['is_complete'] = '0';


  		$response = $this->db->insert('produk',$input,'','','');

  		$pesan['ids'] = $this->db->insert_id();

      	return $response;

    }



    public function update($data){

      // $input['bara'] = $data['kode_barang'];

  		$input['nama'] = $data['nama_barang'];

  		// $input['sub_judul'] = $data['sub_judul'];

  		$input['url_title'] = url_title($input['nama'],'_',true);

  		// $input['keyword'] = $data['keyword'];

  		// $input['keterangan'] = $data['keterangan'];

  		// $input['sku'] = $data['sku'];

  		// $input['upc'] = $data['upc'];

  		// $input['ean'] = $data['ean'];

  		// $input['jan'] = $data['jan'];

  		// $input['isbn'] = $data['isbn'];

  		// $input['mpn'] = $data['mon'];

  		// $input['quantity'] = $data['quantity'];

  		$input['stock_status_id'] = '1';

  		$input['vendor_id'] = $data['vendor_id'];

  		$input['pengarang_id'] = $data['pengarang'];

  		// $input['shipping'] = $data['shipping'];

      $input['harga_beli'] = $data['harga_beli'];

  		$input['harga'] = $data['harga_jual'];

  		// $input['discount'] = $data['discount'];

  		// $input['points'] = $data['point'];

  		$input['date_show'] = $data['tgl_entry'];

  		$input['date_available'] = $data['tgl_entry'];

  		// $input['berat'] = $data['berat'];

  		// $input['panjang'] = $data['panjang'];

  		// $input['lebar'] = $data['lebar'];

  		// $input['tinggi'] = $data['tinggi'];

  		$input['kurangi_stock'] = '1';

  		$input['minimum'] = '1';

  		$input['jenis_produk'] = 'Reguler';

  		$input['status'] = '1';

  		$input['date_added'] = $data['tgl_entry'];



  		$input['status_delete'] = '0';

  		$input['is_newrelease'] = '0';

  		$input['is_bestseller'] = '0';

  		$input['is_recommended'] = '0';

  		$input['is_bestofthemonth'] = '0';

  		$input['is_complete'] = '0';

      $filter = array('bara'=>$data['kode_barang']);

      /*

  		if(isset($_POST['is_newrelease'])){

  			$input['is_newrelease'] = '1';

  		}

  		if(isset($_POST['is_bestseller'])){

  			$input['is_bestseller'] = '1';

  		}

  		if(isset($_POST['is_recommended'])){

  			$input['is_recommended'] = '1';

  		}

  		if(isset($_POST['is_bestofthemonth'])){

  			$input['is_bestofthemonth'] = '1';

  		}

  		if(isset($_POST['is_complete'])){

  			$input['is_complete'] = '1';

  		}

      */

  		$response = $this->crut->update($filter,'produk',$input,'','','');

      return $response;



    }

    /**
     * [update_qty digunakan untuk melakukan update data quantity produk]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function update_quantity($data){
      $data_sukses = array();// variabel untuk menampung data yang sukses

      $hitung = 0;
      foreach ($data['detail'] as $k => $v) {
        $input['quantity'] = $v['quantity'];
        $filter = array('bara'=>$v['kode_barang']);
        $response = $this->crut->update($filter,'produk',$input,'','','');
        if($response['status'] && $response['kode'] == '1'){
          $data_sukses[] = $v;
          $hitung++;
        }
      }

      if ($hitung == 0) {

  			$res['respone'] = "NO";

  			$res['msg'] = "Data Gagal di Update";

  			$res['detail'] = $data_sukses;

  		} else {

  			$res['respone'] = "OK";

  			$res['msg'] = 'INSERT OK Total Data '.$hitung;

  			$res['detail'] = $data_sukses;

  		}

  		echo json_encode($res);

  		return json_encode($res);
    }


}
