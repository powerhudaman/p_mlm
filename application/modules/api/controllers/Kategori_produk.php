<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class kategori_produk extends REST_Controller {
  private $member = array();

  public function __construct()
	{
  		parent::__construct();
      $this->load->model('mdl_crud','crut');
      $this->load->model('api/produk_manajemen/mdl_kategori_produk','kat');
      // $this->load->model('cart_catalog/mdl_produk','d_produk');
      $this->load->library('cart');
  }

  /**
   * [auth digunakan untuk melakukan pengecekan akses]
   * @param  string $root     [nama root kategori]
   * @param  string $komponen [nama fungsi yang akan di akses di api]
   * @param  string $akses    [jenis akses t = tulis b = baca u = update dan h = hapus]
   * @return [type]           [description]
   */
  public function auth($root='',$komponen='',$akses=''){
    $idkey = $this->get('idkey');
    $passkey = $this->get('passkey');
    $q = "select ip_restriction,hak_akses from groupapi where idkey='".$idkey."' and passkey ='".$passkey."' and `status` ='1'";
    $d = $this->crut->list_row($q);
    if($d !=0){
      $permision = json_decode($d['hak_akses'],true);
      $d['hak_akses'] = $permision;
      if(array_key_exists($root,$permision)){
        if(count($permision[$root]['akses']) > 0){
          if(!array_key_exists($akses,$permision[$root]['subs'][$komponen]['akses'])){
            $d = array('pesan'=>'404');
          }
        }
        if(count($permision[$root]['akses']) == 0){
            $d = array('pesan'=>'404');
        }
      }

    }
    if($d == 0){
      $d = array('pesan'=>'Mohon Maaf Anda Tidak Memiliki Akses Ke Api');
    }

    return $d;
  }



  public function akses_get(){
    $idkey = $this->get('idkey');
    $passkey = $this->get('passkey');
    $q = "select ip_restriction,hak_akses from groupapi where idkey='".$idkey."' and passkey ='".$passkey."' and `status` ='1'";
    $d = $this->crut->list_row($q);
    $permision = json_decode($d['hak_akses'],true);
    // $permision = $this->auth('produk_manajemen','kategori_produk','t');

    if(!array_key_exists('u',$permision['produk_manajemen']['subs']['kategori_produk']['akses'])){
      $permision = array('pesan'=>'404');
    }
    // echo '<pre>';
    // print_r($permision);
    // echo '</pre>';
    $this->response($permision);
  }

  public function kategori_produk_get($id_kategori=''){
    $akses = $this->auth('produk_manajemen','kategori_produk','b');
    if(array_key_exists('pesan',$akses)){
      $this->response($akses,404);
    }
    //data ok begin
    if(!array_key_exists('pesan',$akses)){

      $kategori_items = array();
      $q_max = "select max(a.`level`) as total_level from kategori_produk as a LEFT JOIN produk_to_kategori as b on a.kategori_id = b.kategori_id  ORDER BY a.`level` ASC;";
      $total_level = $this->crut->list_row($q_max)['total_level'];

      $temp_total_produk_parent = array(); // varibel ini digunakan untuk menyimpan data sementara untuk menampung array induk;
      for($i = 1; $i <= $total_level; $i++){

        // $q_data = "select a.kategori_id as id, a.nama as nama,a.parent_id as parent_id ,a.`level` as tingkat,count(b.kategori_id) as total_produk from kategori_produk as a LEFT JOIN produk_to_kategori as b on a.kategori_id = b.kategori_id where a.`level` ='".$i."' and a.status ='2'  GROUP BY a.kategori_id ORDER BY a.`level` ASC;";
        $q_data = "select a.kategori_id as id, a.nama as nama,a.parent_id as parent_id ,a.`level` as tingkat,count(b.kategori_id) as total_produk from kategori_produk as a LEFT JOIN produk_to_kategori_filtered as b on a.kategori_id = b.kategori_id where a.`level` ='".$i."' and a.status ='2' GROUP BY a.kategori_id ORDER BY a.sort_order ASC;";
        // echo $q_data;
        $datas = $this->crut->list_datas($q_data);
        if($i == 1){ // bila level masih 1
            foreach ($datas as $k => $v) {
              if(empty($v['url_title'])){
                $kategori_items[$v['id']]= array(
                  'id'=>$v['id'],
                  'nama'=>$v['nama'],
                  'link'=>site_url('cart_catalog/kategoriproduk/index').'/'.$v['id'],
                  'total_produk'=>$v['total_produk'],
                  'subs'=>array(),
                  'total_level'=>1
                );

              }
              if(!empty($v['url_title'])){
                $kategori_items[$v['id']]= array(
                  'id'=>$v['id'],
                  'nama'=>$v['nama'],
                  'link'=>site_url('cart_catalog/kategoriproduk/index').'/'.$v['url_title'],
                  'total_produk'=>$v['total_produk'],
                  'subs'=>array(),
                  'total_level'=>1
                );
              }

            } // end foreach
        } // end if 1
        if($i == 2){
          foreach ($datas as $key => $value) {
            // && array_key_exists($value['parent_id'],$kategori_items)
            if(empty($value['url_title']) ){
                  $kategori_items[$value['parent_id']]['total_level'] = '2';
                $kategori_items[$value['parent_id']]['subs'][$value['id']]=
                array(
                  'id'=>$value['id'],
                  'nama'=>$value['nama'],
                  'link'=>site_url('cart_catalog/kategoriproduk/index').'/'.$value['id'],
                  'total_produk'=>$value['total_produk'],
                  'subs'=>array()
                );
            }
          }
        } // end if i = 2
        if($i == 3){
          foreach ($datas as $key => $value) {
            if($value['total_produk'] > 0){
              // && array_key_exists($value['parent_id'],$kategori_items)
              if(empty($value['url_title']) ){
                  // cari level 1
                  $q_induk = "SELECT b.kategori_id as parent_id, b.`level` as tingkatan, a.kategori_id as child_id, a.`level` as tingkat from kategori_produk as a left JOIN kategori_produk as b on a.parent_id = b.kategori_id where a.kategori_id = '".$value['parent_id']."';";
                  $d_induk = $this->crut->list_row($q_induk);
                  $kategori_items[$d_induk['parent_id']]['total_level'] = '3';
                  $kategori_items[$d_induk['parent_id']]['subs'][$d_induk['child_id']]['subs'][$value['id']]=
                  array(
                    'id'=>$value['id'],
                    'nama'=>$value['nama'],
                    'link'=>site_url('cart_catalog/kategoriproduk/index').'/'.$value['id'],
                    'total_produk'=>$value['total_produk'],
                    'subs'=>array()
                  );
              }
            }

          }
        } // end if i = 3
      } // end for
      // proses hitung total produk parent
      foreach ($kategori_items as $k => $v) {
        $total_produk_parent = 0;
        if(array_key_exists('total_produk',$v)){
          if($v['total_produk'] > 0){
            $total_produk_parent = $total_produk_parent + $v['total_produk'];
          }
          if(count($v['subs']) > 0){
            foreach ($v['subs'] as $k_2 => $v_2) {
              if($v_2['total_produk'] > 0){
                $total_produk_parent = $total_produk_parent + $v_2['total_produk'];
              }
              if(count($v_2['subs']) > 0){
                foreach ($v_2['subs'] as $k_3 => $v_3) {
                  if($v_3['total_produk'] > 0){
                    $total_produk_parent = $total_produk_parent + $v_3['total_produk'];
                  }
                }
              }
            }
          }
          $kategori_items[$v['id']]['total_produk'] = $total_produk_parent;
        }
      }
      $this->response($kategori_items,200);
    }
    //data ok end
  }

  public function insert_post(){
    $akses = $this->auth('produk_manajemen','kategori_produk','t');
    if(array_key_exists('pesan',$akses)){
      $this->response($akses,404);
    }
    if(!array_key_exists('pesan',$akses)){
      $input['parent_id'] = $this->post('parent_id');
      $input['nama'] = $this->post('nama');
      $input['url_title'] = url_title($input['nama'],'-',true);
      $input['sort_order'] = $this->post('sort_order');
      $input['status'] = $this->post('status');
      $input['status_delete'] = '0';
      $input['date_added'] = date('Y-m-d H:i:s');
      $input['level'] = 1;

      $q_cek_level = "select `level` from kategori_produk where kategori_id = '".$input['parent_id']."'";
      $d_cek_level = $this->crut->list_row($q_cek_level);
      if($d_cek_level !=0){
        $input['level'] = $d_cek_level['level'] + 1;
      }

      $sukses = "Proses Tambah Kategori dengan Nama = ".$input['nama']." Berhasil";
      $gagal = "Proses Tambah Kategori dengan Nama = ".$input['nama']." Gagal";
      $pesan = $this->crut->insert('kategori_produk',$input,'Tambah Kategori',$sukses,$gagal);
      $this->response($pesan,200);
    }

  }

  public function update_post($kategori_id){
    $akses = $this->auth('produk_manajemen','kategori_produk','u');
    if(array_key_exists('pesan',$akses)){
      $this->response($akses,404);
    }
    if(!array_key_exists('pesan',$akses)){
      $filter = array('kategori_id'=>$kategori_id);
      $input['parent_id'] = $this->post('parent_id');
      $input['nama'] = $this->post('nama');
      $input['url_title'] = url_title($input['nama'],'-',true);
      $input['sort_order'] = $this->post('sort_order');
      $input['status'] = $this->post('status');

      $input['date_modified'] = date('Y-m-d H:i:s');
      $input['level'] = 1;

      $q_cek_level = "select `level` from kategori_produk where kategori_id = '".$input['parent_id']."'";
      $d_cek_level = $this->crut->list_row($q_cek_level);
      if($d_cek_level !=0){
        $input['level'] = $d_cek_level['level'] + 1;
      }

      $sukses = "Proses Update Kategori Produk dengan Nama = ".$input['nama']." Berhasil";
      $gagal = "Proses Update Kategori Produk dengan Nama = ".$input['nama']." Gagal";
      $pesan = $this->crut->update($filter,'kategori_produk',$input,'Update Kategori Produk',$sukses,$gagal);
      $this->response($pesan,200);
    }

  }

  public function list_produk_get($kategori_id='',$limit='',$offset=''){
    $produks['data'] = $this->kat->produk_tampil_data($kategori_id,$limit,$offset);
    $produks['total'] = $this->kat->produk_tampil_total($kategori_id);

    $this->response($produks,200);

  }


}
