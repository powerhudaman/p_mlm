<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class List_order_soap extends REST_Controller {
  private $member = array();

  public function __construct()
	{
  		parent::__construct();
      $this->load->model('mdl_crud','crut');
      // $this->load->model('cart_catalog/mdl_produk','d_produk');
      $this->load->library('cart');
  }

  /**
   * [auth digunakan untuk melakukan pengecekan akses]
   * @param  string $root     [nama root kategori]
   * @param  string $komponen [nama fungsi yang akan di akses di api]
   * @param  string $akses    [jenis akses t = tulis b = baca u = update dan h = hapus]
   * @return [type]           [description]
   */
  public function auth($root='',$komponen='',$akses=''){
    $idkey = $this->get('idkey');
    $passkey = $this->get('passkey');
    $q = "select ip_restriction,hak_akses from groupapi where idkey='".$idkey."' and passkey ='".$passkey."' and `status` ='1'";
    $d = $this->crut->list_row($q);
    if($d !=0){
      $permision = json_decode($d['hak_akses'],true);
      $d['hak_akses'] = $permision;
      if(array_key_exists($root,$permision)){
        if(count($permision[$root]['akses']) > 0){
          if(!array_key_exists($akses,$permision[$root]['subs'][$komponen]['akses'])){
            $d = array('pesan'=>'404');
          }
        }
        if(count($permision[$root]['akses']) == 0){
            $d = array('pesan'=>'404');
        }
      }

    }
    if($d == 0){
      $d = array('pesan'=>'Mohon Maaf Anda Tidak Memiliki Akses Ke Api');
    }

    return $d;
  }



  public function akses_get(){
    $idkey = $this->get('idkey');
    $passkey = $this->get('passkey');
    $q = "select ip_restriction,hak_akses from groupapi where idkey='".$idkey."' and passkey ='".$passkey."' and `status` ='1'";
    $d = $this->crut->list_row($q);
    $permision = json_decode($d['hak_akses'],true);
    $coba_auth = $this->auth('order_manajemen','list_order_soap','b');
    // $coba_auth = json_decode($coba_auth['hak_akses'],true);
    // echo '<pre>';
    // print_r($coba_auth);
    // die();

    if(!array_key_exists('b',$permision['order_manajemen']['subs']['list_order_soap']['akses'])){
      $permision = array('pesan'=>'404');
    }

    $this->response($coba_auth);
  }

  public function order_list_get(){
    $akses = $this->auth('order_manajemen','list_order_soap','b');
    if(array_key_exists('pesan',$akses)){
      $this->response($akses,404);
    }
    //data ok begin
    if(!array_key_exists('pesan',$akses)){

      $list_order = array();
      $q_order_master = "select a.order_id as kunci, a.invoice_no as nota, a.date_added as tgl, a.date_expired as expired, concat(a.shipping_firstname,' ',a.shipping_lastname) as fullname, a.total_brutto as brutto, a.total_netto as netto, a.shipping_address_1 as alamat,'Millenniastore' as domain, b.id_mis as status_mis, a.shipping_code, a.shipping_method, a.payment_method, a.payment_code,a.kode_keuangan as kode_keuangan  from orders as a INNER JOIN order_status as b on a.order_status_id = b.id where a.soap ='0'";

      $d_order_master = $this->crut->list_datas($q_order_master);
      if($d_order_master == '0'){
        $this->response(array('pesan'=>'Tidak Ada Order Yang Baru','status'=>false,'kode'=>'2'),200);
      }
      if($d_order_master !=0){
        foreach ($d_order_master as $k => $v) {
          $list_order[$v['kunci']]['nota'] = $v['nota'];
          $list_order[$v['kunci']]['tgl'] = $v['tgl'];
          $list_order[$v['kunci']]['expired'] = $v['expired'];
          $list_order[$v['kunci']]['fullname'] = $v['fullname'];
          $list_order[$v['kunci']]['brutto'] = $v['brutto'];
          $list_order[$v['kunci']]['netto'] = $v['netto'];
          $list_order[$v['kunci']]['alamat'] = strip_tags($v['alamat']);
          $list_order[$v['kunci']]['domain'] = $v['domain'];
          $list_order[$v['kunci']]['status_mis'] = $v['status_mis'];
          $list_order[$v['kunci']]['ekspedisi'] = $v['shipping_code'];
          $q_ekspedisi = "select title,value from order_total where order_id='".$v['kunci']."' and code ='Pengiriman'; ";
          $d_ekspedisi = $this->crut->list_row($q_ekspedisi);
          if($d_ekspedisi !=0){
            $list_order[$v['kunci']]['ekspedisi'] = $d_ekspedisi['title'];
          }
          $list_order[$v['kunci']]['pembayaran'] = $v['payment_method'].'-'.$v['payment_code'];
          $setting_pembayaran = $this->crut->setting($v['payment_method'],'id_mis','setting');
          if(!array_key_exists('pesan',$setting_pembayaran)){
            $list_order[$v['kunci']]['pembayaran'] = $setting_pembayaran['value'].'-'.$v['payment_code'];
          }
          $list_order[$v['kunci']]['transfer'] = 0;
          $q_transfer = "select nominal from order_payment_history where order_id='".$v['kunci']."' and `status`='0';";
          $d_transfer = $this->crut->list_row($q_transfer);
          if($d_transfer !=0){
            $list_order[$v['kunci']]['transfer'] = $d_transfer['nominal'];
          }
          $list_order[$v['kunci']]['kode_keuangan'] = $v['kode_keuangan'];
          $list_order[$v['kunci']]['detil_produk'] = '';
            $q_detil_produk = "select a.bara as bara, a.quantity as qty, b.discount as disc, '' as nota, c.kode_supplier as Gol, a.brutto as brutto, a.price as netto, c.kode_supplier as kode, '".$v['status_mis']."' as `status_mis`, 'Millenniastore' as domain from order_product as a
          	INNER JOIN produk as b on a.product_id = b.produk_id
          	LEFT JOIN vendor as c on b.vendor_id = c.vendor_id
          	where a.order_id='".$v['kunci']."' and a.soap='0';";

            $d_detil_produk = $this->crut->list_datas($q_detil_produk);
          if($d_detil_produk !='0'){
            $list_order[$v['kunci']]['detil_produk'] = $d_detil_produk;
          }
        }
        $this->response($list_order,200);
      }

    }
    //data ok end
  }

  public function update_soap_post(){
    $akses = $this->auth('order_manajemen','list_order_soap','u');
    if(array_key_exists('pesan',$akses)){
      $this->response($akses,404);
    }
    //data ok begin
    if(!array_key_exists('pesan',$akses)){

      $soap = $this->post('soap');
      if($soap =='0' || $soap='1'){
        $update_soap['soap'] = $this->post('soap');
        $filter_order = array('order_id'=>$this->post('kunci'));
        $up_status = $this->crut->update($filter_order,'orders',$update_soap,'','Update Soap Berhasil Dengan kode '.$filter_order['order_id'],'Update Soap Gagal Dengan kode '.$filter_order['order_id']);
        if($up_status['status'] && $up_status['kode'] =='1'){
          $update_soap['soap'] = $this->post('soap');
          // $filter_order = array('order_id'=>$this->post('kunci'));
          $up_detil_status = $this->crut->update($filter_order,'order_product',$update_soap,'','','');
        }
        $this->response($up_status,200);

      }else{
        $this->response(array('pesan'=>'Proses Update gagal','status'=>false,'kode'=>'2'),200);

      }

    }

  }

}
