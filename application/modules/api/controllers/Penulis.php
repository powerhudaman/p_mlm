<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Penulis extends REST_Controller {
  private $member = array();

  public function __construct()
	{
  		parent::__construct();
      $this->load->model('mdl_crud','crut');
      $this->load->model('api/produk_manajemen/mdl_penulis','penulis');
      // $this->load->model('cart_catalog/mdl_produk','d_produk');
      $this->load->library('cart');
  }

  /**
   * [auth digunakan untuk melakukan pengecekan akses]
   * @param  string $root     [nama root kategori]
   * @param  string $komponen [nama fungsi yang akan di akses di api]
   * @param  string $akses    [jenis akses t = tulis b = baca u = update dan h = hapus]
   * @return [type]           [description]
   */
  public function auth($root='',$komponen='',$akses=''){
    $idkey = $this->get('idkey');
    $passkey = $this->get('passkey');
    $q = "select ip_restriction,hak_akses from groupapi where idkey='".$idkey."' and passkey ='".$passkey."' and `status` ='1'";
    $d = $this->crut->list_row($q);
    if($d !=0){
      $permision = json_decode($d['hak_akses'],true);
      $d['hak_akses'] = $permision;
      if(array_key_exists($root,$permision)){
        if(count($permision[$root]['akses']) > 0){
          if(!array_key_exists($akses,$permision[$root]['subs'][$komponen]['akses'])){
            $d = array('pesan'=>'404');
          }
        }
        if(count($permision[$root]['akses']) == 0){
            $d = array('pesan'=>'404');
        }
      }

    }
    if($d == 0){
      $d = array('pesan'=>'Mohon Maaf Anda Tidak Memiliki Akses Ke Api');
    }

    return $d;
  }



  public function akses_get(){
    $idkey = $this->get('idkey');
    $passkey = $this->get('passkey');
    $q = "select ip_restriction,hak_akses from groupapi where idkey='".$idkey."' and passkey ='".$passkey."' and `status` ='1'";
    $d = $this->crut->list_row($q);
    $permision = json_decode($d['hak_akses'],true);
    // $permision = $this->auth('produk_manajemen','kategori_produk','t');

    if(!array_key_exists('u',$permision['produk_manajemen']['subs']['penulis']['akses'])){
      $permision = array('pesan'=>'404');
    }
    // echo '<pre>';
    // print_r($permision);
    // echo '</pre>';
    $this->response($permision);
  }


  public function list_produk_get($id_penulis='',$limit='',$offset=''){
    $produks['data'] = $this->penulis->produk_tampil_data($id_penulis,$limit,$offset);
    $produks['total'] = $this->penulis->produk_tampil_total($id_penulis);

    $this->response($produks,200);

  }


}
