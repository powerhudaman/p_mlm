<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Produk extends REST_Controller {
  private $member = array();

  public function __construct()
	{
  		parent::__construct();
      $this->load->model('mdl_crud','crut');
      // $this->load->model('cart_catalog/mdl_produk','d_produk');
      $this->load->library('cart');
  }

  /**
   * [auth digunakan untuk melakukan pengecekan akses]
   * @param  string $root     [nama root kategori]
   * @param  string $komponen [nama fungsi yang akan di akses di api]
   * @param  string $akses    [jenis akses t = tulis b = baca u = update dan h = hapus]
   * @return [type]           [description]
   */
  public function auth($root='',$komponen='',$akses=''){
    $idkey = $this->get('idkey');
    $passkey = $this->get('passkey');
    $q = "select ip_restriction,hak_akses from groupapi where idkey='".$idkey."' and passkey ='".$passkey."' and `status` ='1'";
    $d = $this->crut->list_row($q);
    if($d !=0){
      $permision = json_decode($d['hak_akses'],true);
      $d['hak_akses'] = $permision;
      if(array_key_exists($root,$permision)){
        if(count($permision[$root]['akses']) > 0){
          if(!array_key_exists($akses,$permision[$root]['subs'][$komponen]['akses'])){
            $d = array('pesan'=>'404');
          }
        }
        if(count($permision[$root]['akses']) == 0){
            $d = array('pesan'=>'404');
        }
      }

    }
    if($d == 0){
      $d = array('pesan'=>'Mohon Maaf Anda Tidak Memiliki Akses Ke Api');
    }

    return $d;
  }

  public function insert_post(){
    $akses = $this->auth('produk_manajemen','produk','t');
    if(array_key_exists('pesan',$akses)){
      $this->response($akses,404);
    }
    //data ok begin
    if(!array_key_exists('pesan',$akses)){

      $q_get = "select bara from produk where bara ='".$this->post('bara')."'";
      $d_get = $this->crut->list_row($q_get);
      if($d_get !=0){
        $pesan = array('status'=>false,'pesan'=>'Produk Sudah Pernah Di Input Silahkan Gunakan Metode Update','kode'=>2);
        $this->response($pesan,200);
      }
      if($d_get =="0"){
        $this->db->trans_begin();
        $response = array();
        $input['bara'] = $this->post('bara');
        $input['nama'] = $this->post('nama');
        $input['judul_produk'] = $this->post('judul_produk');
        $input['sub_judul'] = $this->post('sub_judul');
        $input['url_title'] = url_title($input['nama'],'_',true);
        $input['keyword'] = $this->post('keyword');
        $input['keterangan'] = $this->post('keterangan');
        $input['sku'] = $this->post('sku');
        // $input['upc'] = $this->post('upc');
        // $input['ean'] = $this->post('ean');
        // $input['jan'] = $this->post('jan');
        $input['isbn'] = $this->post('isbn');
        // $input['mpn'] = $this->post('mon');
        $input['quantity'] = $this->post('quantity');
        $input['stock_status_id'] = $this->post('stock_status_id');
        $input['vendor_id'] = $this->post('vendor_id');
        $input['pengarang_id'] = $this->post('pengarang_id');
        // $input['shipping'] = $this->post('shipping');
        $input['harga_beli'] = $this->post('harga_beli');
        $input['harga'] = $this->post('harga');
        $input['discount'] = $this->post('discount-master');
        $input['points'] = $this->post('point');
        $input['date_show'] = $this->post('date_show');
        $input['date_available'] = $this->post('date_available');
        $input['berat'] = $this->post('berat');
        $input['panjang'] = $this->post('panjang');
        $input['lebar'] = $this->post('lebar');
        $input['tinggi'] = $this->post('tinggi');
        $input['kurangi_stock'] = '1';
        $input['minimum'] = $this->post('minimum');
        $input['jenis_produk'] = $this->post('jenis_produk');
        $input['status'] = $this->post('status');
        $input['created_by'] = $this->post('created_by');
        $input['date_added'] = date('Y-m-d H:i:s');

        $input['status_delete'] = '0';
        $input['is_newrelease'] = '0';
        $input['is_bestseller'] = '0';
        $input['is_recommended'] = '0';
        $input['is_bestofthemonth'] = '0';
        $input['is_complete'] = '0';

        if(isset($_POST['is_newrelease'])){
          $input['is_newrelease'] = '1';
        }
        if(isset($_POST['is_bestseller'])){
          $input['is_bestseller'] = '1';
        }
        if(isset($_POST['is_recommended'])){
          $input['is_recommended'] = '1';
        }
        if(isset($_POST['is_bestofthemonth'])){
          $input['is_bestofthemonth'] = '1';
        }
        if(isset($_POST['is_complete'])){
          $input['is_complete'] = '1';
        }

        $this->db->insert('produk',$input);
        $pesan['ids'] = $this->db->insert_id();
        if(isset($_POST['stock_new']) && $_POST['stock_new'] == 1){
          $filter_stok_new = array('bara'=>$input['bara']);
          $this->crut->update($filter_stok_new,'stock_new',array('soap'=>'1'),'Update Stok new','','');
        }


        /// proses data kategori produk
        // [kategori] => 1:Buku,5:  buku agama,4:  Mainan,
        if(isset($POST['kategori']) && !empty($POST['kategori'])){
          $filter = array('produk_id'=>$pesan['ids']);
          $this->db->where('produk_id',$filter['produk_id']);
          $this->db->delete('produk_to_kategori');
          $data_kategoris = $POST['kategori'];
          foreach($data_kategoris as $k => $data_kategori){
            if(!empty($data_kategori)){
              $kategori = explode(':',$data_kategori.':');
              $input_produk_kategori['produk_id'] = $pesan['ids'];
              $input_produk_kategori['kategori_id'] = htmlentities(stripslashes(trim($kategori[0])));
              $this->crut->insert('produk_to_kategori',$input_produk_kategori,'Tambah Kategori Produk','','');
            }
          }
        }
        if ($this->db->trans_status() === FALSE)
        {
                $this->db->trans_rollback();
                $response = array('status'=>false,'pesan'=>"Gagal Melakukan Tambah Produk",'kode'=>2);
        }
        else
        {
                $this->db->trans_commit();
                $response = array('status'=>true,'pesan'=>"Sukses Melakukan Tambah Produk",'kode'=>1,'ids'=>$pesan['ids']);
        }

        $this->response($response,200);
      }

    }
  }

  public function update_post($bara){
    $akses = $this->auth('produk_manajemen','produk','u');
    if(array_key_exists('pesan',$akses)){
      $this->response($akses,404);
    }
    //data ok begin
    if(!array_key_exists('pesan',$akses)){

      $this->db->trans_begin();

  		/*$this->db->query('AN SQL QUERY...');
  		$this->db->query('ANOTHER QUERY...');
  		$this->db->query('AND YET ANOTHER QUERY...');*/
  		$filter = array('bara'=>$bara);
  		$input['bara'] = $this->post('bara');
  		$input['nama'] = $this->post('nama');
  		$input['judul_produk'] = $this->post('judul_produk');
  		$input['sub_judul'] = $this->post('sub_judul');
  		$input['url_title'] = url_title($input['nama'],'_',true);
  		$input['keyword'] = $this->post('keyword');
  		$input['keterangan'] = $this->post('keterangan');
  		$input['sku'] = $this->post('sku');
  		// $input['upc'] = $this->post('upc');
  		// $input['ean'] = $this->post('ean');
  		// $input['jan'] = $this->post('jan');
  		$input['isbn'] = $this->post('isbn');
  		// $input['mpn'] = $this->post('mon');
  		$input['quantity'] = $this->post('quantity');
  		$input['stock_status_id'] = $this->post('stock_status_id');
  		$input['vendor_id'] = $this->post('vendor_id');
  		$input['pengarang_id'] = $this->post('pengarang_id');
  		// $input['shipping'] = $this->post('shipping');
  		$input['harga_beli'] = $this->post('harga_beli');
  		$input['harga'] = $this->post('harga');
  		$input['discount'] = $this->post('discount-master');
  		$input['points'] = $this->post('point');
  		$input['date_show'] = $this->post('date_show');
  		$input['date_available'] = $this->post('date_available');
  		$input['berat'] = $this->post('berat');
  		$input['panjang'] = $this->post('panjang');
  		$input['lebar'] = $this->post('lebar');
  		$input['tinggi'] = $this->post('tinggi');
  		$input['kurangi_stock'] = $this->post('kurangi_stock');
  		$input['minimum'] = $this->post('minimum');
  		$input['jenis_produk'] = $this->post('jenis_produk');
  		$input['status'] = $this->post('status');
  		$input['updated_by'] = $this->session->userdata('username');
  		$input['date_modified'] = date('Y-m-d H:i:s');
  		$input['status_delete'] = '0';
  		// $input['qc'] = '2';

  		$input['is_newrelease'] = '0';
  		$input['is_bestseller'] = '0';
  		$input['is_recommended'] = '0';
  		$input['is_bestofthemonth'] = '0';
  		$input['is_complete'] = '0';

  		if(isset($_POST['is_newrelease'])){
  			$input['is_newrelease'] = '1';
  		}
  		if(isset($_POST['is_bestseller'])){
  			$input['is_bestseller'] = '1';
  		}
  		if(isset($_POST['is_recommended'])){
  			$input['is_recommended'] = '1';
  		}
  		if(isset($_POST['is_bestofthemonth'])){
  			$input['is_bestofthemonth'] = '1';
  		}
  		if(isset($_POST['is_complete'])){
  			$input['is_complete'] = '1';
  		}

  		$this->db->where($filter);
  		$this->db->update('produk',$input);
  		$pesan['ids'] = $filter['produk_id'];

  		/// proses data kategori produk
  		// [kategori] => 1:Buku,5:  buku agama,4:  Mainan,
  		if(isset($POST['kategori']) && !empty($POST['kategori'])){
  			$this->db->where('produk_id',$filter['produk_id']);
  			$this->db->delete('produk_to_kategori');
  			$data_kategoris = $POST['kategori'];
  			foreach($data_kategoris as $k => $data_kategori){
  				if(!empty($data_kategori)){
  					$kategori = explode(':',$data_kategori.':');
  					$input_produk_kategori['produk_id'] = $pesan['ids'];
  					$input_produk_kategori['kategori_id'] = htmlentities(stripslashes(trim($kategori[0])));
  					$this->crut->insert('produk_to_kategori',$input_produk_kategori,'Tambah Kategori Produk','','');
  				}
  			}
  		}
      if ($this->db->trans_status() === FALSE)
  		{
  		        $this->db->trans_rollback();
  						$response = array('status'=>false,'pesan'=>"Gagal Melakukan Update Produk",'kode'=>2);
  		}
  		else
  		{
  		        $this->db->trans_commit();
  						$response = array('status'=>true,'pesan'=>"Sukses Melakukan Update Produk",'kode'=>1,'ids'=>$pesan['ids']);
  		}

  		$this->response($response,200);

    }

  }

  public function insert_update_post(){
    $akses = $this->auth('produk_manajemen','produk','t');
    if(array_key_exists('pesan',$akses)){
      $this->response($akses,404);
    }
    //data ok begin
    if(!array_key_exists('pesan',$akses)){

      $q_get = "select produk_id, bara from produk where bara ='".$this->post('bara')."'";
      $d_get = $this->crut->list_row($q_get);
      if($d_get !=0){
        $this->db->trans_begin();

    		/*$this->db->query('AN SQL QUERY...');
    		$this->db->query('ANOTHER QUERY...');
    		$this->db->query('AND YET ANOTHER QUERY...');*/
    		$filter = array('bara'=>$this->post('bara'));
    		$input['bara'] = $this->post('bara');
    		$input['nama'] = $this->post('nama');
    		$input['judul_produk'] = $this->post('judul_produk');
    		$input['sub_judul'] = $this->post('sub_judul');
    		$input['url_title'] = url_title($input['nama'],'_',true);
    		$input['keyword'] = $this->post('keyword');
    		$input['keterangan'] = $this->post('keterangan');
    		$input['sku'] = $this->post('sku');
    		// $input['upc'] = $this->post('upc');
    		// $input['ean'] = $this->post('ean');
    		// $input['jan'] = $this->post('jan');
    		$input['isbn'] = $this->post('isbn');
    		// $input['mpn'] = $this->post('mon');
    		$input['quantity'] = $this->post('quantity');
    		$input['stock_status_id'] = $this->post('stock_status_id');
    		$input['vendor_id'] = $this->post('vendor_id');
    		$input['pengarang_id'] = $this->post('pengarang_id');
    		// $input['shipping'] = $this->post('shipping');
    		$input['harga_beli'] = $this->post('harga_beli');
    		$input['harga'] = $this->post('harga');
    		$input['discount'] = $this->post('discount');
    		$input['points'] = $this->post('point');
    		$input['date_show'] = $this->post('date_show');
    		$input['date_available'] = $this->post('date_available');
    		$input['berat'] = $this->post('berat');
    		$input['panjang'] = $this->post('panjang');
    		$input['lebar'] = $this->post('lebar');
    		$input['tinggi'] = $this->post('tinggi');
    		$input['kurangi_stock'] = $this->post('kurangi_stock');
    		$input['minimum'] = $this->post('minimum');
    		$input['jenis_produk'] = $this->post('jenis_produk');
    		$input['status'] = $this->post('status');
    		$input['updated_by'] = $this->session->userdata('username');
    		$input['date_modified'] = date('Y-m-d H:i:s');
    		$input['status_delete'] = '0';
    		// $input['qc'] = '2';

    		$input['is_newrelease'] = '0';
    		$input['is_bestseller'] = '0';
    		$input['is_recommended'] = '0';
    		$input['is_bestofthemonth'] = '0';
    		$input['is_complete'] = '0';

    		if(isset($_POST['is_newrelease'])){
    			$input['is_newrelease'] = '1';
    		}
    		if(isset($_POST['is_bestseller'])){
    			$input['is_bestseller'] = '1';
    		}
    		if(isset($_POST['is_recommended'])){
    			$input['is_recommended'] = '1';
    		}
    		if(isset($_POST['is_bestofthemonth'])){
    			$input['is_bestofthemonth'] = '1';
    		}
    		if(isset($_POST['is_complete'])){
    			$input['is_complete'] = '1';
    		}

    		$this->db->where($filter);
    		$this->db->update('produk',$input);
    		$pesan['ids'] = $d_get['produk_id'];

    		/// proses data kategori produk
    		// [kategori] => 1:Buku,5:  buku agama,4:  Mainan,
    		if(isset($_POST['kategori']) && !empty($_POST['kategori'])){
    			$this->db->where('produk_id',$d_get['produk_id']);
    			$this->db->delete('produk_to_kategori');
    			$data_kategoris = $_POST['kategori'];
    			foreach($data_kategoris as $k => $data_kategori){
    				if(!empty($data_kategori)){
    					$kategori = explode(':',$data_kategori.':');
    					$input_produk_kategori['produk_id'] = $pesan['ids'];
    					$input_produk_kategori['kategori_id'] = htmlentities(stripslashes(trim($kategori[0])));
    					$this->crut->insert('produk_to_kategori',$input_produk_kategori,'Tambah Kategori Produk','','');
    				}
    			}
    		}
        if ($this->db->trans_status() === FALSE)
    		{
    		        $this->db->trans_rollback();
    						$response = array('status'=>false,'pesan'=>"Gagal Melakukan Update Produk",'kode'=>2);
    		}
    		else
    		{
    		        $this->db->trans_commit();
    						$response = array('status'=>true,'pesan'=>"Sukses Melakukan Update Produk",'kode'=>1);
    		}

    		$this->response($response,200);
      }
      if($d_get =="0"){
        $this->db->trans_begin();
        $response = array();
        $input['bara'] = $this->post('bara');
        $input['nama'] = $this->post('nama');
        $input['judul_produk'] = $this->post('judul_produk');
        $input['sub_judul'] = $this->post('sub_judul');
        $input['url_title'] = url_title($input['nama'],'_',true);
        $input['keyword'] = $this->post('keyword');
        $input['keterangan'] = $this->post('keterangan');
        $input['sku'] = $this->post('sku');
        // $input['upc'] = $this->post('upc');
        // $input['ean'] = $this->post('ean');
        // $input['jan'] = $this->post('jan');
        $input['isbn'] = $this->post('isbn');
        // $input['mpn'] = $this->post('mon');
        $input['quantity'] = $this->post('quantity');
        $input['stock_status_id'] = $this->post('stock_status_id');
        $input['vendor_id'] = $this->post('vendor_id');
        $input['pengarang_id'] = $this->post('pengarang_id');
        // $input['shipping'] = $this->post('shipping');
        $input['harga_beli'] = $this->post('harga_beli');
        $input['harga'] = $this->post('harga');
        $input['discount'] = $this->post('discount');
        $input['points'] = $this->post('point');
        $input['date_show'] = $this->post('date_show');
        $input['date_available'] = $this->post('date_available');
        $input['berat'] = $this->post('berat');
        $input['panjang'] = $this->post('panjang');
        $input['lebar'] = $this->post('lebar');
        $input['tinggi'] = $this->post('tinggi');
        $input['kurangi_stock'] = '1';
        $input['minimum'] = $this->post('minimum');
        $input['jenis_produk'] = $this->post('jenis_produk');
        $input['status'] = $this->post('status');
        $input['created_by'] = $this->post('created_by');
        $input['date_added'] = date('Y-m-d H:i:s');

        $input['status_delete'] = '0';
        $input['is_newrelease'] = '0';
        $input['is_bestseller'] = '0';
        $input['is_recommended'] = '0';
        $input['is_bestofthemonth'] = '0';
        $input['is_complete'] = '0';
        $input['qc'] = '2';

        if(isset($_POST['is_newrelease'])){
          $input['is_newrelease'] = '1';
        }
        if(isset($_POST['is_bestseller'])){
          $input['is_bestseller'] = '1';
        }
        if(isset($_POST['is_recommended'])){
          $input['is_recommended'] = '1';
        }
        if(isset($_POST['is_bestofthemonth'])){
          $input['is_bestofthemonth'] = '1';
        }
        if(isset($_POST['is_complete'])){
          $input['is_complete'] = '1';
        }

        $this->db->insert('produk',$input);
        $pesan['ids'] = $this->db->insert_id();
        if(isset($_POST['stock_new']) && $_POST['stock_new'] == 1){
          $filter_stok_new = array('bara'=>$input['bara']);
          $this->crut->update($filter_stok_new,'stock_new',array('soap'=>'1'),'Update Stok new','','');
        }


        /// proses data kategori produk
        // [kategori] => 1:Buku,5:  buku agama,4:  Mainan,
        if(isset($_POST['kategori']) && !empty($_POST['kategori'])){
          $filter = array('produk_id'=>$pesan['ids']);
          $this->db->where('produk_id',$filter['produk_id']);
          $this->db->delete('produk_to_kategori');
          $data_kategoris = $_POST['kategori'];
          foreach($data_kategoris as $k => $data_kategori){
            if(!empty($data_kategori)){
              $kategori = explode(':',$data_kategori.':');
              $input_produk_kategori['produk_id'] = $pesan['ids'];
              $input_produk_kategori['kategori_id'] = htmlentities(stripslashes(trim($kategori[0])));
              $this->crut->insert('produk_to_kategori',$input_produk_kategori,'Tambah Kategori Produk','','');
            }
          }
        }
        if ($this->db->trans_status() === FALSE)
        {
                $this->db->trans_rollback();
                $response = array('status'=>false,'pesan'=>"Gagal Melakukan Tambah Produk",'kode'=>2);
        }
        else
        {
                $this->db->trans_commit();
                $response = array('status'=>true,'pesan'=>"Sukses Melakukan Tambah Produk",'kode'=>1,'ids'=>$pesan['ids']);
        }

        $this->response($response,200);
      }

    }



  }

  public function detail_produk_get($produk_id =''){
    $akses = $this->auth('produk_manajemen','produk','b');
    if(array_key_exists('pesan',$akses)){
      $this->response($akses,404);
    }
    //data ok begin
    if(!array_key_exists('pesan',$akses)){

      $detail = array();
      $q_detail = "select a.*, b.nama as nama_vendor, c.nama as nama_status from produk as a, vendor as b, produk_status as c where a.vendor_id = b.vendor_id and a.stock_status_id = c.produk_status_id and a.produk_id ='".$produk_id."' and a.status ='1' and a.status_delete ='0'";
      $qty = 0;

      if($this->crut->list_count($q_detail) > 0){
          $produk_detail = $this->crut->list_row($q_detail);
          $date1=date_create($produk_detail['date_show']);
          $date2=date_create(date('Y-m-d'));
          $diff=date_diff($date1,$date2);
          //begin date show
          if($diff->format("%R%a") > -1 || $produk_detail==""){

            $detail['produk_id'] = $produk_id;
            $detail['bara'] = $produk_detail['bara'];
            $detail['nama'] = $produk_detail['nama'];
            $detail['judul_produk'] = $produk_detail['judul_produk'];
            $detail['sub_judul'] = $produk_detail['sub_judul'];
            $detail['gambar_detail'] = $produk_detail['gambar'];
            $detail['gambar_thumb'] = $produk_detail['gambar_thumb'];

            // cek gambar thumb
            //{base_url()}admin718/assets/img/com_cart/produk/{$produk['gambar_thumb']}
            $lokasi_gambar = FCPATH."admin718/assets/img/com_cart/produk/".$produk_detail['gambar_thumb'];
            if(!file_exists($lokasi_gambar) || $produk_detail['gambar_thumb'] == ""){
              $detail['gambar_thumb'] = 'no-image.png';
            }

            $detail['url_title'] = $produk_detail['url_title'];
            $detail['keyword'] = $produk_detail['keyword'];
            $detail['keterangan'] = $produk_detail['keterangan'];
            $detail['sku'] = $produk_detail['sku'];
            $detail['upc'] = $produk_detail['upc'];
            $detail['ean'] = $produk_detail['ean'];
            $detail['jan'] = $produk_detail['jan'];
            $detail['isbn'] = $produk_detail['isbn'];
            $detail['mpn'] = $produk_detail['mpn'];
            $detail['quantity'] = $produk_detail['quantity'];
            $detail['stock_status_id'] = $produk_detail['stock_status_id'];
            $detail['nama_status'] = $produk_detail['nama_status'];
            $detail['vendor_id'] = $produk_detail['vendor_id'];
            $detail['nama_vendor'] = $produk_detail['nama_vendor'];
            $detail['pengarang_id'] = $produk_detail['pengarang_id'];
            $detail['shipping'] = $produk_detail['shipping'];
            $detail['harga_lama'] = '';
            $detail['brutto'] = $produk_detail['harga'];
            $detail['harga'] = $produk_detail['harga'];
            $detail['discount'] = $produk_detail['discount'];
            $detail['discountable'] = $produk_detail['discountable'];
            $detail['point'] = $produk_detail['points'];
            $detail['date_available'] = $produk_detail['date_available'];
            $detail['berat'] = $produk_detail['berat'];
            $detail['panjang'] = $produk_detail['panjang'];
            $detail['lebar'] = $produk_detail['lebar'];
            $detail['tinggi'] = $produk_detail['tinggi'];
            $detail['kurangi_stock'] = $produk_detail['kurangi_stock'];
            $detail['minimum'] = $produk_detail['minimum'];
            $detail['status'] = $produk_detail['status'];
            $detail['viewed'] = $produk_detail['viewed'];

            /// discount dari produk master
            if($produk_detail['discount'] !="" && $produk_detail['discount'] > 0){
              $detail['harga_lama'] = $detail['harga'];
              $detail['harga'] = $detail['harga_lama'] - (($detail['harga_lama'] / 100) * $produk_detail['discount']);
            }

            $detail['gambars'][] = array('gambar'=>$produk_detail['gambar'],'alt'=>$produk_detail['nama'],'title'=>$produk_detail['nama']);

            /// list gambar
            $q_gambar = "select * from produk_image where produk_id='".$produk_id."' order by sort_order ASC";
            $gambars = $this->crut->list_datas($q_gambar);
            if($gambars !=0){
              foreach ($gambars as $k => $v) {
                $detail['gambars'][] = array('gambar'=>$v['image'],'alt'=>$produk_detail['nama'],'title'=>$produk_detail['nama']);
              }
            }


            // list spesifikasi
            $q_attribute = "select b.nama, a.text from produk_attribute as a, attribute as b where a.attribut_id = b.attribute_id and a.produk_id ='".$produk_id."';";
            $attributes = $this->crut->list_datas($q_attribute);
            if($attributes !=0){
              foreach ($attributes as $k => $v) {
                $detail['attributes'][] = array('nama'=>$v['nama'],'nilai'=>$v['text']);
              }
            }


            // list option
            $q_option = "select b.nama, b.type, a.* from produk_option as a, `option` as b where a.option_id = b.option_id and a.produk_id ='".$produk_id."' and a.status_delete ='0';";

            $options = $this->crut->list_datas($q_option);
            if($this->crut->list_count($q_option) > 0){
              foreach ($options as $k => $v) {
                $detail['options'][$k] = array('nama'=>$v['nama'],'type'=>$v['type'],'required'=>$v['required'],'value'=>$v['value'],'option_id'=>$v['option_id'],'produk_option_id'=>$v['produk_option_id']);

                //list option value
                $q_option_value = "select c.nama, b.nama as nama_value, a.* from produk_option_value as a, option_value as b, `option` as c where a.option_value_id = b.option_value_id and a.option_id = c.option_id and a.produk_id ='".$produk_id."' and a.option_id='".$v['option_id']."' and a.status_delete ='0';";
                $hitung_option_values = $this->crut->list_count($q_option_value);
                if($hitung_option_values > 0){
                  $option_values = $this->crut->list_datas($q_option_value);
                  foreach ($option_values as $key => $value) {
                    $detail['options'][$k]['option_values'][] = array('produk_option_value_id'=>$value['produk_option_value_id'],'nama'=>$value['nama_value'],'price_prefix'=>$value['price_prefix'],'price'=>$value['price'],'weight_prefix'=>$value['weight_prefix'],'weight'=>$value['weight'],'points_prefix'=>$value['points_prefix'],'points'=>$value['points'],'subtract'=>$value['subtract']);
                  }
                }

              }
            }


            // proses harga
            $default_group = $this->crut->setting('global_configuration','customer_group','setting')['value'];
            if(isset($_SESSION) && array_key_exists('member_group',$_SESSION)){
              $default_group = $_SESSION['member_group'];
            }

            // cek discount
            $detail['produk_discount'] = array();
            $q_discount = "select * from produk_discount where produk_id ='".$produk_id."' and customer_group_id ='".$default_group."' and ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) and status_delete ='0' and quantity_min <='".$qty."' order by priority,price ASC limit 1;";
            if($this->crut->list_count($q_discount) > 0){
              $discount = $this->crut->list_row($q_discount);
              $qty_max = $discount['quantity_max'];

              if($discount['quantity_max'] == "" || $qty <= $discount['quantity_max'] ){
                $detail['harga_lama'] = $detail['harga'];
                $detail['harga'] = $detail['harga_lama'] - (($detail['harga_lama'] / 100) * $discount['price']);
                $detail['discount'] = $discount['price'];
                $detail['produk_discount'][$produk_id] = array(
                    'product_id'=>$produk_id,
                    'bara'=>$detail['bara'],
                    'quantity'=>$qty,
                    'harga_per_item'=>$detail['harga'],
                    'total_harga'=>$detail['harga'] * $qty
                );
              }
              if($qty > $discount['quantity_max']){
                $detail['harga_lama'] = $detail['harga'];
                $harga_diskon = $detail['harga_lama'] - (($detail['harga_lama'] / 100) * $discount['price']);
                $detail['produk_discount'][$produk_id] = array(
                    'product_id'=>$produk_id,
                    'bara'=>$detail['bara'],
                    'quantity'=>$discount['quantity_max'],
                    'harga_per_item'=>$harga_diskon,
                    'total_harga'=>$harga_diskon * $discount['quantity_max']
                );
              }

            }

            // cek special
            $q_special = "select * from produk_special where produk_id ='".$produk_id."' and customer_group_id ='".$default_group."' and ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) and status_delete ='0' order by priority DESC limit 1;";
            if($this->crut->list_count($q_special) > 0){
              $special = $this->crut->list_row($q_special);
              $detail['harga_lama'] = $detail['harga'];
              $detail['harga'] = $special['price'];
            }

            $filter_order = array('code'=>'order_configuration','key'=>'status_toko');
            $status_toko = $this->crut->detail($filter_order,'setting'); /// aturan apakah toko aktif atau tidak

            if($status_toko['value'] == '1'){

              // limit jenis produk begin
              $q_jenis_produk = "select * from jenis_produk where jenis_produk_id ='".$produk_detail['jenis_produk']."' and status_delete ='0'";
              $d_jenis_produk = $this->crut->list_row($q_jenis_produk);
              if($d_jenis_produk == 0){ /// jika tidak ditemukan maka produk akan di anggap habis

                return $detail;
              }else{
                $qty_jenis_produk = 0;
                if($produk_detail['is_bestseller'] == 1){
                  $qty_jenis_produk = $d_jenis_produk['stok_minimal_bestseller'];
                }
                if($produk_detail['is_bestseller'] == 0){
                  $qty_jenis_produk = $d_jenis_produk['stok_minimal_non_bestseller'];
                }
                if($detail['quantity'] < $qty_jenis_produk){

                  return $detail;
                }
                if($detail['quantity'] > $qty_jenis_produk){ // bila quantity lebih besar syarat jenis produk

                    /// limit produk
                    $q_limit_produk = "select a.produk_id as produk_id, a.`limit` as `limit`, a.status_delete as status_delete from produk_limit as a where a.produk_id ='".$detail['produk_id']."' and a.date_start <= DATE_FORMAT(now(),'%Y-%m-%d') and a.date_end >= DATE_FORMAT(now(),'%Y-%m-%d') and a.status_delete ='0';";

                    if($this->crut->list_count($q_limit_produk) > 0){
                      $limit_produk = $this->crut->list_row($q_limit_produk);
                      /// status jual dari stock status
                      $q_status_jual_dari_stok_status = "select status_jual from produk_status where produk_status_id ='".$detail['stock_status_id']."'";

                      if($this->crut->list_count($q_status_jual_dari_stok_status) > 0){ /// bila stok status ditemukan maka ini akan dijalankan
                          $status_jual = $this->crut->list_row($q_status_jual_dari_stok_status);
                          if($status_jual['status_jual'] == '1'){
                            $detail['tombol'] = site_url('cart_order/cart/add/'.$detail['produk_id']);
                          }
                      }
                      if($this->crut->list_count($q_status_jual_dari_stok_status) == 0){
                        if($detail['status'] == '1'){
                          $detail['tombol'] = site_url('cart_order/cart/add/'.$detail['produk_id']);
                        }
                      }

                    }else{ /// bila tidak ada data limit produk
                      $q_status_jual_dari_stok_status = "select status_jual from produk_status where produk_status_id ='".$detail['stock_status_id']."'";

                      if($this->crut->list_count($q_status_jual_dari_stok_status) > 0){ /// bila stok status ditemukan maka ini akan dijalankan
                          $status_jual = $this->crut->list_row($q_status_jual_dari_stok_status);
                          if($status_jual['status_jual'] == '1'){
                            $detail['tombol'] = site_url('cart_order/cart/add/'.$detail['produk_id']);
                          }
                      }
                      if($this->crut->list_count($q_status_jual_dari_stok_status) == 0){
                        if($detail['status'] == '1'){
                          $detail['tombol'] = site_url('cart_order/cart/add/'.$detail['produk_id']);
                        }
                      }
                    }

                }

              }/// end jenis produk
            }

            $this->response($detail,200);

          } // end if date_show
          if($diff->format("%R%a") < 0 && $produk_detail['date_show'] !=""){
            $detail = array('pesan'=>'Data Produk Tidak Ditemukan');
            $this->response($detail,200);
          }

      }else{
        $detail = array('pesan'=>'Data Produk Tidak Ditemukan');
        $this->response($detail,200);
      }

    }

  }

}
