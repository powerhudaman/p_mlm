<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_penulis extends CI_Model {

  /**
   * [produk_tampil_data digunakan untuk menampilkan data produk]
   * @param  string $kategori_id [description]
   * @param  string $limit       [description]
   * @param  string $offset      [description]
   * @return [type]              [description]
   */
  public function produk_tampil_data($id_penulis='1',$limit='64',$offset='0'){
    $produk_data = array();

      $q_produk="select a.id_1, a.id_2, a.id_3, a.produk_id,a.date_modified,a.qc,a.nama from produk_data_tampil as a inner join produk as b on a.produk_id = b.produk_id and b.pengarang_id = '".$id_penulis."' ";

      $q_produk .=" and a.qc ='3' ";
      $q_produk .=" and b.`status` = '1' group by a.produk_id order by a.date_added DESC limit ".$limit." offset ".$offset." ;";
      // echo $q_produk;
      // die();
      $d_produk = $this->crut->list_datas($q_produk);
      $this->load->model('cart_catalog/mdl_produk','d_produk'); // detail detail produk

      if($d_produk !=0){
        foreach ($d_produk as $k => $v) {
          $q_review = "select count(*) as total_vote from produk_review as a LEFT JOIN customer as b on a.customer_id = b.id where a.produk_id ='".$v['produk_id']."' and a.status_publish ='1'";
          $d_review = $this->crut->list_row($q_review);
          $data_detail_produk = $this->d_produk->detail_thumb($v['produk_id']);
          $produk_data[$k] = $data_detail_produk;
          $produk_data[$k]['link_produk'] = site_url('detail/'.$v['produk_id']);
          if($data_detail_produk['url_title'] !=""){
            $produk_data[$k]['link_produk'] = site_url($data_detail_produk['url_title']);
          }
          $produk_data[$k]['total_vote'] = 0;
          if($d_review !=0){
            $produk_data[$k]['total_vote'] = $d_review['total_vote'];
          }
          $total_vote = $produk_data[$k]['total_vote'];
          $nilai_star = 0;
          $star_1 =0;
          $star_2 =0;
          $star_3 =0;
          $star_4 =0;
          $star_5 =0;
          $q_hitung_star = "select star_produk, count(*) as total_vote from produk_review where status_publish ='1' AND produk_id ='".$v['produk_id']."' GROUP BY star_produk;";
          $d_hitung_star = $this->crut->list_datas($q_hitung_star);
          if($d_hitung_star !=0){
            foreach ($d_hitung_star as $k => $v) {
              $total_vote = $total_vote + $v['total_vote'];
              switch ($v['star_produk']) {
                case '1':
                  $star_1 = $v['total_vote'];
                  break;
                case '2':
                  $star_2 = $v['total_vote'];
                  break;
                case '3':
                  $star_3 = $v['total_vote'];
                  break;
                case '4':
                  $star_4 = $v['total_vote'];
                  break;
                case '5':
                  $star_5 = $v['total_vote'];
                  break;
              }
            }
            if($total_vote > 0){
              $nilai_star = ($star_1 + $star_2 * 2 + $star_3 * 3 + $star_4 * 4 + $star_5 * 5) / $total_vote;
            }
          }
          $produk_data[$k]['nilai_star'] = ceil($nilai_star);
        }
        $respon_data = array('pesan'=>'Produk Ada','status'=>true,'kode'=>1,'produk_data'=>$produk_data);

      }
      if($d_produk == 0){
        $respon_data = array('pesan'=>'Produk Tidak Ditemukan','status'=>false,'kode'=>2);
      }

    return $respon_data;
  }

  /**
   * [produk_tampil_total digunakan untuk menampilkan total data produk]
   * @param  string $kategori_id [description]
   * @return [type]              [description]
   */
  public function produk_tampil_total($id_penulis='1'){
    $produk_data = '';
      $q_produk="select a.id_1, a.id_2, a.id_3, a.produk_id,a.date_modified,a.qc,a.nama from produk_data_tampil as a inner join produk as b on a.produk_id = b.produk_id  and b.pengarang_id ='".$id_penulis."' ";

            $q_produk .=" and a.qc ='3' ";
      $q_produk .=" and b.`status` = '1' group by a.produk_id ;";
      $d_produk = $this->crut->list_count($q_produk);

      if($d_produk !=0){
        $produk_data = $d_produk;
      }
      if($d_produk == 0){
        $produk_data = 0;
      }
    return $produk_data;
  }

}
