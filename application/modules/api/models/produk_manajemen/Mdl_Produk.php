<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_Produk extends CI_Model {

  /**
	 * [insert Digunakan Untuk tambah data produk]
	 * @param  array  $POST [Data yang di proses adalah array post dari controller]
	 * @return [type]       [description]
	 */
	public function insert($POST = array(),$title='',$sukses='',$gagal=''){

		$this->db->trans_begin();

		/*$this->db->query('AN SQL QUERY...');
		$this->db->query('ANOTHER QUERY...');
		$this->db->query('AND YET ANOTHER QUERY...');*/

		$input['bara'] = $this->input->post('bara',true);
		$input['nama'] = $this->input->post('nama',true);
		$input['judul_produk'] = $this->input->post('judul_produk',true);
		$input['sub_judul'] = $this->input->post('sub_judul',true);
		$input['url_title'] = url_title($input['nama'],'_',true);
		$input['keyword'] = $this->input->post('keyword',true);
		$input['keterangan'] = $this->input->post('keterangan',true);
		$input['sku'] = $this->input->post('sku',true);
		$input['upc'] = $this->input->post('upc',true);
		$input['ean'] = $this->input->post('ean',true);
		$input['jan'] = $this->input->post('jan',true);
		$input['isbn'] = $this->input->post('isbn',true);
		$input['mpn'] = $this->input->post('mon',true);
		$input['quantity'] = $this->input->post('quantity',true);
		$input['stock_status_id'] = $this->input->post('stock_status_id',true);
		$input['vendor_id'] = $this->input->post('vendor_id',true);
		$input['pengarang_id'] = $this->input->post('pengarang_id',true);
		// $input['shipping'] = $this->input->post('shipping',true);
		$input['harga_beli'] = $this->input->post('harga_beli',true);
		$input['harga'] = $this->input->post('harga',true);
		$input['discount'] = $this->input->post('discount-master',true);
		$input['points'] = $this->input->post('point',true);
		$input['date_show'] = $this->input->post('date_show',true);
		$input['date_available'] = $this->input->post('date_available',true);
		$input['berat'] = $this->input->post('berat',true);
		$input['panjang'] = $this->input->post('panjang',true);
		$input['lebar'] = $this->input->post('lebar',true);
		$input['tinggi'] = $this->input->post('tinggi',true);
		$input['kurangi_stock'] = $this->input->post('kurangi_stock',true);
		$input['minimum'] = $this->input->post('minimum',true);
		$input['jenis_produk'] = $this->input->post('jenis_produk',true);
		$input['status'] = $this->input->post('status',true);
		$input['created_by'] = $this->session->userdata('username');
		$input['date_added'] = date('Y-m-d H:i:s');

		$input['status_delete'] = '0';
		$input['is_newrelease'] = '0';
		$input['is_bestseller'] = '0';
		$input['is_recommended'] = '0';
		$input['is_bestofthemonth'] = '0';
		$input['is_complete'] = '0';

		if(isset($_POST['is_newrelease'])){
			$input['is_newrelease'] = '1';
		}
		if(isset($_POST['is_bestseller'])){
			$input['is_bestseller'] = '1';
		}
		if(isset($_POST['is_recommended'])){
			$input['is_recommended'] = '1';
		}
		if(isset($_POST['is_bestofthemonth'])){
			$input['is_bestofthemonth'] = '1';
		}
		if(isset($_POST['is_complete'])){
			$input['is_complete'] = '1';
		}

		$this->db->insert('produk',$input);
		$pesan['ids'] = $this->db->insert_id();
		if(isset($_POST['stock_new']) && $_POST['stock_new'] == 1){
			$filter_stok_new = array('bara'=>$input['bara']);
			$this->crut->update($filter_stok_new,'stock_new',array('soap'=>'1'),'Update Stok new','','');
		}


		/// proses data kategori produk
		// [kategori] => 1:Buku,5:  buku agama,4:  Mainan,
		if(isset($POST['kategori']) && !empty($POST['kategori'])){
			$filter = array('produk_id'=>$pesan['ids']);
			$this->db->where('produk_id',$filter['produk_id']);
			$this->db->delete('produk_to_kategori');
			$data_kategoris = $POST['kategori'];
			foreach($data_kategoris as $k => $data_kategori){
				if(!empty($data_kategori)){
					$kategori = explode(':',$data_kategori.':');
					$input_produk_kategori['produk_id'] = $pesan['ids'];
					$input_produk_kategori['kategori_id'] = htmlentities(stripslashes(trim($kategori[0])));
					$this->crut->insert('produk_to_kategori',$input_produk_kategori,'Tambah Kategori Produk','','');
				}
			}
		}

		/// proses data attribute
		if(isset($POST['attribut']) && !empty($POST['attribut'])){
			$attributs = $POST['attribut'];
			foreach($attributs as $k => $attribut){
				$input_produk_attribut['produk_id'] = $pesan['ids'];
				$input_produk_attribut['attribut_id'] = $attribut['id'];
				$input_produk_attribut['text'] = $attribut['value'];
				$this->crut->insert('produk_attribute',$input_produk_attribut,'Tambah Produk Attribut','','');
			}
		}

		// proses data gambar
		if(isset($POST['gambars']) && !empty($POST['gambars'])){
			$gambars = $POST['gambars'];
			foreach($gambars as $k => $gambar){
				$input_produk_image['produk_id'] = $pesan['ids'];
				$input_produk_image['image'] = $gambar['nama_gambar'];
				$response = $this->crut->insert('produk_image',$input_produk_image,'Tambah Produk Image','','');
				if($response['status'] && $response['kode'] == '1'){
					$file_asal = FCPATH.'/assets/img/com_cart/tmp_produk/'.$gambar['nama_gambar'];
					$file_tujuan = FCPATH.'/assets/img/com_cart/produk/'.$gambar['nama_gambar'];
					if(copy($file_asal,$file_tujuan)){
						unlink($file_asal);
					}
					// cek primary
					if(isset($_POST['gambar_primary']) && $_POST['gambar_primary'] == $k){
						$update_gambar['gambar'] = $gambar['nama_gambar'];
						$filter = array('produk_id'=>$pesan['ids']);
						$response = $this->crut->update($filter,'produk',$update_gambar,'','','');
						/*if($response['status'] && $response['kode'] == 1){ // begin resize gambar
								$config_resize['image_library'] = 'gd2';
								$config_resize['source_image'] = $file_tujuan;
								$config_resize['create_thumb'] = TRUE;
								$config_resize['maintain_ratio'] = TRUE;
								$config_resize['width']         = 202;
								$config_resize['height']       = 239;
								$this->image_lib->initialize($config_resize);
								if(!$this->image_lib->resize()){
									// $pesan['pesan'].=' Gambar Berhasil Di Upload Tetapi Gagal Di Resize';
								}else{
									$pecah = explode('.',$gambar['nama_gambar'].'. ');
									$update['gambar_thumb'] = $pecah[0].'_thumb.'.$pecah[1];
									$filter = array('produk_id'=>$pesan['ids']);
									$this->crut->update($filter,'produk',$update,'Update Produk','','');
								}
						}// end resize gambar*/
					}
				}
			}
		}

		// proses data option produk
		 if(isset($POST['produk_option']) && !empty($POST['produk_option'])){
				$produk_option = $POST['produk_option'];
				foreach($produk_option as $k => $option){
					$input_produk_option['produk_id'] = $pesan['ids'];
					$input_produk_option['option_id'] = $option['option_id'];
					if(isset($option['value']) && !empty($option['value'])){
						$input_produk_option['value'] = $option['value'];
					}
					$input_produk_option['required']= $option['required'];

					$pesan_option = $this->crut->insert('produk_option',$input_produk_option,'Tambah Option','','');

					if(isset($option['produk_option_value']) && is_array($option['produk_option_value'])){
						foreach($option['produk_option_value'] as $b => $option_value){

							$input_produk_option_value['produk_option_id'] = $pesan_option['ids'];
							$input_produk_option_value['produk_id'] = $pesan['ids'];
							$input_produk_option_value['option_id'] = $option['option_id'];
							$input_produk_option_value['option_value_id'] = $option_value['option_value_id'];
							$input_produk_option_value['quantity'] = $option_value['quantity'];
							$input_produk_option_value['subtract'] = $option_value['subtract'];
							$input_produk_option_value['price'] = $option_value['price'];
							$input_produk_option_value['price_prefix'] = $option_value['price_prefix'];
							$input_produk_option_value['points'] = $option_value['points'];
							$input_produk_option_value['points_prefix'] = $option_value['points_prefix'];
							$input_produk_option_value['weight'] = $option_value['weight'];
							$input_produk_option_value['weight_prefix'] = $option_value['weight_prefix'];

							$this->crut->insert('produk_option_value',$input_produk_option_value,'Tambah Option Value','','');
						}

					}

				}
			}

		// proses data reward
		if(isset($POST['reward']) && !empty($POST['reward'])){
			$rewards = $POST['reward'];
			foreach($rewards as $k => $reward){
				$input_produk_reward['produk_id'] = $pesan['ids'];
				$input_produk_reward['customer_group_id'] = $reward['customer_group_id'];
				$input_produk_reward['points'] = $reward['reward_point'];
				$this->crut->insert('produk_reward',$input_produk_reward,'Tambah Produk Reward','','');
			}
		}

		//proses discount
		 if(isset($POST['discount']) && !empty($POST['discount'])){
			$discounts = $POST['discount'];
			foreach($discounts as $k => $discount){
				$input_produk_discount['produk_id'] = $pesan['ids'];
				$input_produk_discount['customer_group_id'] = $discount['customer_group_id'];
				$input_produk_discount['quantity_min'] = $discount['qty_min'];
				$input_produk_discount['quantity_max'] = $discount['qty_max'];
				$input_produk_discount['price'] = $discount['harga'];
				$input_produk_discount['date_start'] = $discount['date_start'];
				$input_produk_discount['date_end'] = $discount['date_end'];
				$this->crut->insert('produk_discount',$input_produk_discount,'Tambah Produk Discount','','');
			}
		}

		// proses spesial
		 if(isset($POST['special']) && !empty($POST['special'])){
			$specials = $_POST['special'];
			foreach($specials as $k => $special){
				$input_produk_special['produk_id'] = $pesan['ids'];
				$input_produk_special['customer_group_id'] = $special['customer_group_id'];
				$input_produk_special['price'] = $special['harga'];
				$input_produk_special['date_start'] = $special['date_start'];
				$input_produk_special['date_end'] = $special['date_end'];
				$this->crut->insert('produk_special',$input_produk_special,'Tambah Produk Reward','','');
			}
		}

		// proses limit
		 if(isset($POST['limit']) && !empty($POST['limit'])){
			$limits = $_POST['limit'];
			foreach($limits as $k => $limit){
				$input_produk_limit['produk_id'] = $pesan['ids'];
				$input_produk_limit['limit'] = $limit['limit'];
				$input_produk_limit['date_start'] = $limit['date_start'];
				$input_produk_limit['date_end'] = $limit['date_end'];
				$this->crut->insert('produk_limit',$input_produk_limit,'Tambah Produk limit','','');
			}
		}

		if ($this->db->trans_status() === FALSE)
		{
		        $this->db->trans_rollback();
						$response = array('status'=>false,'pesan'=>$gagal,'kode'=>2);
		}
		else
		{
		        $this->db->trans_commit();
						$response = array('status'=>true,'pesan'=>$sukses,'kode'=>1,'ids'=>$pesan['ids']);
		}

		return $response;

	}

}
