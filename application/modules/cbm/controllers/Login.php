<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
    $this->load->helper('text');
  }

  public function index(){
    $themes_name = $this->themes_name;
    $data['themes'] = $themes_name."/index.tpl";

    $view = $themes_name.'/login.tpl'; // lokasi view di tpl
    $this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya
  }


  public function login_proses(){
      $id_member = $this->input->post('id_member',true);
      $pin = do_hash($id_member.'-'.$this->input->post('pin',true),'md5');
      $q_login = "select id_member, id_group, nama_lengkap,email,kupon,no_hp,id,image from member_master where id_member = ? and pin = ? and aktivasi = ? ;";
      $d_login = $this->crut->list_row_bin($q_login,array($id_member,$pin,'2'));

      if($d_login !=0){
          $id_group = $d_login['id_group'];
          $q_group = "select * from member_group where id = ? and status_delete = ? ;";
          $d_group = $this->crut->list_row_bin($q_group,array($id_group,'0'));
          $grant_akses = array();
          $total_akses = 0;
          if($d_group !=0){
            $tk = json_decode($d_group['hak_akses'],true);
            foreach ($tk as $k => $v) {
              foreach ($v as $k2 => $v2) {
                $grant_akses['component'][$k2] = $v2;
                $total_akses = $total_akses + $v2;
              }
            }
                /// pembuatan session member login
                 $session['login_member'] = array(
                     'id_member'=>$d_login['id_member'],
                     'nama_lengkap'=>$d_login['nama_lengkap'],
                     'email'=>$d_login['email'],
                     'kupon'=>$d_login['kupon'],
                     'member'=>$d_group['nama'],
                     'no_hp'=>$d_login['no_hp'],
                     'id'=>$d_login['id'],
                     'grant_akses'=>$grant_akses,
                     'total_akses'=>$total_akses
                 );
                 $session['image'] = $d_login['image'];

                 $this->session->set_userdata($session);
                // echo '<pre>';
                // print_r($_SESSION);
                // die();
                /// pembuatan session member login
                $pesan = array('status'=>true,'pesan'=>'Anda Berhasil Login','kode'=>1);
                $this->session->set_flashdata('pesan',pesan($pesan));
                redirect(site_url('dashboard'));
          }else{
                $pesan = array('status'=>false,'pesan'=>'Group anda telah expire silahkan menghubungi admin untuk informasi lebih lanjut','kode'=>2);
                $this->session->set_flashdata('pesan',pesan($pesan));
                redirect(site_url('home'));
          }

      }else{
        $pesan = array('status'=>false,'pesan'=>'ID Member Atau Pin Anda Salah','kode'=>2);
        $this->session->set_flashdata('pesan',pesan($pesan));
        redirect(site_url('home'));
      }
  }

  public function logout(){
      if($this->session->has_userdata('login_member')){
        unset($_SESSION['login_member']);
        $pesan = array('status'=>true,'pesan'=>'Anda Berhasil Logout','kode'=>1);
        $this->session->set_flashdata('pesan',pesan($pesan));
        redirect(site_url('home'));
      }else{
        $pesan = array('status'=>false,'pesan'=>'Anda Belum Login Proses Logout Gagal','kode'=>2);
        $this->session->set_flashdata('pesan',pesan($pesan));
        redirect(site_url('home'));
      }

  }

  public function demo(){
        $themes_name = $this->themes_name;
        $data['themes'] = $themes_name."/index.tpl";

        $view = $themes_name.'/coba_menu_2.tpl'; // lokasi view di tpl
        $this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya
  }



}
