<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Member_master extends MY_Controller {





    private $status_member = array('1'=>'Belum Aktivasi','2'=>'Aktif');

    private $list_member_statement_kiri = array();

	private $list_member_statement_kanan = array();

    private $listing_downline_data = array();

    private $listing_downline_id = array();

	private $bonus_sponsor = 0;

	private $bonus_pasangan = 0;

  private $bonus_cabang= 0;

  private $no_level_downline = 0;



  public function __construct()

  {

    parent::__construct();

    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');

    $this->load->helper('text');

    $this->load->helper('security');

    $this->cek_signin();



    $this->id_member = $this->session->userdata('login_member')['id_member'];

    $this->id = $this->session->userdata('login_member')['id'];

    $this->load->library('datatables');

    $this->bonus_sponsor = $this->crut->setting('komisi_configuration','bonus_sponsor','setting')['value'];

    $this->bonus_pasangan = $this->crut->setting('komisi_configuration','bonus_pasangan','setting')['value'];

    $this->bonus_cabang = $this->crut->setting('komisi_configuration','bonus_cabang','setting')['value'];



  }



  public function index(){



    $themes_name = $this->themes_name;

    $data['themes'] = $themes_name."/index.tpl";

    $data['page_title'] = 'Member Master';





    $data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');

	  $data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',

									'plugins/datatables/dataTables.bootstrap.min.js');



    $view = $themes_name.'/member_master/fetch.tpl'; // lokasi view di tpl

    $this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya

  }

  public function fetch_data(){



            // 			$string .='<a href="'.$v['url'].'/'.$kode.'" class="'.$v['class'].'" title="'.$v['title'].'"><i class="'.$v['simbol'].'"></i></a>';

          //  $link[] = array('url'=>site_url('cbm/member_master/edit'),'class'=>'btn btn-primary','title'=>'Edit Data Member','simbol'=>'fa fa-pencil');

            $link[] = array('url'=>site_url('cbm/member_master/views'),'class'=>'btn btn-success','title'=>'Lihat Member Statement','simbol'=>'fa fa-eye');

            $link[] = array('url'=>site_url('cbm/member_master/hirarki'),'class'=>'btn btn-success','title'=>'hirarki','simbol'=>'fa fa-group');

            $link[] = array('url'=>site_url('cbm/member_master/delete'),'class'=>'btn btn-danger','title'=>'Delete Member','simbol'=>'fa fa-trash');



            $this->datatables->select("id,id_member,nama_lengkap,id_sponsor,id_upline,downline_kiri,downline_kanan,no_hp");

            $this->datatables->from('member_master');

            $this->datatables->where('del',0);

            $this->datatables->where('id_sponsor',$this->id_member)

            ->unset_column('id')

            // ->unset_column('a.status')

        //    ->add_column('aksi',link_generator_array($link,'$1'),'id')

           //  ->add_column('status',ganti_array($this->status,'$2'),'a.status')

            ;

            echo $this->datatables->generate('json');

 }

 public function hirarki($id='0'){

      $a = $this->input->post('member');
	  $b =explode("|", $a);
	  $id_member = $b[0];
	  $q_id = "select id from member_master where id_member='".$id_member."'";
	  $data_id = $this->crut->list_row($q_id);

if($data_id['id']<>''){
$id= $data_id['id'];
}else{
  $id = ($id=='0' )? $this->id : $id;
}
   
	

    $themes_name = $this->themes_name;

    $data['themes'] = $themes_name."/index.tpl";

    $data['page_title'] = 'Geneologi Jaringan';

  

    $q_member = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

    DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

    c.`nama_lengkap` AS nama_upline FROM member_master AS a

    LEFT JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

    LEFT JOIN member_master AS c ON a.`id_upline` = c.`id_member`

    LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id ='$id' ";

    $data['get'] = $this->crut->list_row($q_member);

    $data['id_upline'] = 0;

    if($this->id != $id){

      $q_id_upline = "select id from member_master where id_member='".$data['get']['id_upline']."'";

      $d_id_upline = $this->crut->list_row($q_id_upline);

      $data['id_upline'] = $d_id_upline['id'];

    }

  

  

    // list member kiri

    // $this->statement_view_kiri($data['get']['downline_kiri']);

    // $this->statement_view_kanan($data['get']['downline_kanan']);

  

    $this->listing_downline($data['get']['id_member']);

    // $this->listing_downline_baru($data['get']['id_member']);

  

    $coba = $this->listing_downline_data;

    $hasil = array();

    $hasil[0] = $data['get'];

    $pos_4 = 1;

    $angka = 0;  

    $verifikasi_downline_level_3 = ''; // variabel ini digunakan untuk menampung data sementara;

    $this->data_statement_kiri($data['get']['downline_kiri']);
    
    $this->data_statement_kanan($data['get']['downline_kanan']);

    // echo '<pre>';
    
    // print_r($this->list_member_statement_kiri);

    // echo '</pre>';

    // echo '<pre>';
    
    // print_r($this->list_member_statement_kanan);

    // echo '</pre>';

    //testing foreach kiri
    $list_angka_kiri = array(1,3,4,5,6,7,8);
    $list_kiri = $this->list_member_statement_kiri;
    foreach ($list_kiri as $k => $v) {
      // echo $k.' '.$v['level'].'<br>';
      if($v['level'] == 1){
        $hasil[1] = $v;
      }
      if($v['level'] == 2){
        // echo $k.' '.$v['level'].' kiri'.$v['id_upline'].'<br>';
        $dki = $list_kiri[$v['id_upline']]['downline_kiri'];
        $dka = $list_kiri[$v['id_upline']]['downline_kanan'];
        // var_dump($dki);
        if($dki == $k){
          $hasil[3] = $v;
        }
        if($dka == $k){
          $hasil[4] = $v;
        }
        
      }
      if($v['level'] == 3){ /// level 3
        /// level 2 pos kiri
        
        $dki_2 = $list_kiri[$v['id_upline']]['downline_kiri'];
        $dka_2 = $list_kiri[$v['id_upline']]['downline_kanan'];


        $up_line_l1 = $list_kiri[$v['id_upline']]['id_upline'];
        
        
        $dki_1 = $list_kiri[$up_line_l1]['downline_kiri'];
        $dka_1 = $list_kiri[$up_line_l1]['downline_kanan'];
        // echo $k.'='.$v['id_upline'].'-x'.$up_line_l1.'<br>';
        // var_dump($dki_2);
        // var_dump($dki_1);
        

      
        
        if($dki_1 == $v['id_upline']){ /// apabila level atas pada posisi kiri
        //  echo $k.' '.$v['level'].' kiri<br>';
         
          if($dki_2 == $k){
            $hasil[5] = $v;
          }
          if($dka_2 == $k){
            $hasil[6] = $v;
          }
        }

        if($dka_1 == $v['id_upline']){ /// apabila level atas pada posisi kiri
          // echo $k.' '.$v['level'].' kanan<br>';
          if($dki_2 == $k){
            $hasil[7] = $v;
          }
          if($dka_2 == $k){
            $hasil[8] = $v;
          }
        }
        

        /// level 2 pos kiri
        
      } /// level 3
    }

    //testing foreach kanan

    $list_angka_kanan = array(2,9,10,11,12,12,14);
    $list_kanan = $this->list_member_statement_kanan;
    foreach ($list_kanan as $k => $v) {
      // echo $k.' '.$v['level'].'<br>';
      if($v['level'] == 1){
        $hasil[2] = $v;
      }
      if($v['level'] == 2){
        // echo $k.' '.$v['level'].' kiri'.$v['id_upline'].'<br>';
        $dki = $list_kanan[$v['id_upline']]['downline_kiri'];
        $dka = $list_kanan[$v['id_upline']]['downline_kanan'];
        // var_dump($dki);
        if($dki == $k){
          $hasil[9] = $v;
        }
        if($dka == $k){
          $hasil[10] = $v;
        }
        
      }
      if($v['level'] == 3){ /// level 3
        /// level 2 pos kiri
        
        $dki_2 = $list_kanan[$v['id_upline']]['downline_kiri'];
        $dka_2 = $list_kanan[$v['id_upline']]['downline_kanan'];


        $up_line_l1 = $list_kanan[$v['id_upline']]['id_upline'];
        
        
        $dki_1 = $list_kanan[$up_line_l1]['downline_kiri'];
        $dka_1 = $list_kanan[$up_line_l1]['downline_kanan'];
        // echo $k.'='.$v['id_upline'].'-x'.$up_line_l1.'<br>';
        // var_dump($dki_2);
        // var_dump($dki_1);
        

      
        
        if($dki_1 == $v['id_upline']){ /// apabila level atas pada posisi kiri
        //  echo $k.' '.$v['level'].' kiri<br>';
         
          if($dki_2 == $k){
            $hasil[11] = $v;
          }
          if($dka_2 == $k){
            $hasil[12] = $v;
          }
        }

        if($dka_1 == $v['id_upline']){ /// apabila level atas pada posisi kiri
          // echo $k.' '.$v['level'].' kanan<br>';
          if($dki_2 == $k){
            $hasil[13] = $v;
          }
          if($dka_2 == $k){
            $hasil[14] = $v;
          }
        }
        

        /// level 2 pos kiri
        
      } /// level 3
    }

    //testing foreach kanan
    
  

    // echo '<pre>';

    // print_r($this->listing_downline_data);

    // echo '</pre>';



    // //

    // echo '<pre>';

    // print_r($verifikasi_downline_level_3);

    // echo '</pre>';

  

    // echo '<pre>';

    // print_r($hasil);

    // echo '</pre>';

  

    // echo '<pre>';

    // print_r($this->listing_downline_id);

    // echo '</pre>';

    // die();

  

    $data['listing_downline_data'] = $this->listing_downline_data;

    $data['hasil_downline'] = json_encode($hasil);

    //$data['upline'] = $this->crut->list_datas($upline);

    $data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css');
    $data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
                              'plugins/datatables/dataTables.bootstrap.min.js',
                              'plugins/jQueryUI/jquery-ui.min.js'
                        );


    $view = $themes_name.'/member_master/hirarki.tpl'; // lokasi view di tpl

    $this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya

        }

        public function hirarki_asli($id='0'){
          
            
          
              if($id=='0'){
          
                 $id= $this->id;
          
              }else{
          
                $id= $id;
          
              }
          
              $themes_name = $this->themes_name;
          
              $data['themes'] = $themes_name."/index.tpl";
          
              $data['page_title'] = 'Geneologi Jaringan';
          
            
          
              $q_member = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,
          
              DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,
          
              c.`nama_lengkap` AS nama_upline FROM member_master AS a
          
              LEFT JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`
          
              LEFT JOIN member_master AS c ON a.`id_upline` = c.`id_member`
          
              LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id ='$id' ";
          
              $data['get'] = $this->crut->list_row($q_member);
          
              $data['id_upline'] = 0;
          
              if($this->id != $id){
          
                $q_id_upline = "select id from member_master where id_member='".$data['get']['id_upline']."'";
          
                $d_id_upline = $this->crut->list_row($q_id_upline);
          
                $data['id_upline'] = $d_id_upline['id'];
          
              }
          
            
          
            
          
              // list member kiri
          
              // $this->statement_view_kiri($data['get']['downline_kiri']);
          
              // $this->statement_view_kanan($data['get']['downline_kanan']);
          
            
          
              $this->listing_downline($data['get']['id_member']);
          
              // $this->listing_downline_baru($data['get']['id_member']);
          
            
          
              $coba = $this->listing_downline_data;
          
              $hasil = array();
          
              $hasil[0] = $data['get'];
          
              $pos_4 = 1;
          
              $angka = 0;  
          
              $verifikasi_downline_level_3 = ''; // variabel ini digunakan untuk menampung data sementara;
          
              $this->data_statement_kiri($data['get']['downline_kiri']);
              
              $this->data_statement_kanan($data['get']['downline_kanan']);
          
              // echo '<pre>';
              
              // print_r($this->list_member_statement_kiri);
          
              // echo '</pre>';
          
              // echo '<pre>';
              
              // print_r($this->list_member_statement_kanan);
          
              // echo '</pre>';
          
              foreach ($coba as $k => $v) {
          
                foreach ($v['subs'] as $k2 => $v2) {
          
                  foreach ($v2 as $k3 => $v3) {
          
                    $angka++;
          
                    
          
                    if($angka < 15){
          
                      if($angka == 4){
          
                        if($v3['id'] == ""){
          
                          $pos_4 = 0;
          
                        }
          
                        if($v3['id'] !=""){
          
                          $pos_4 = 1;
          
                        }
          
                      }
          
                      if($angka == 6){
          
            
          
                      }
          
                      /// apabila pos 4 = 0 maka pada saat angka di no  itu di tambahkan 2 agar bisa masuk di posisi yang tepat
          
                      if($angka == 6 && $pos_4 == 0){
          
                        $angka = $angka + 2;
          
                      }
          
            
          
                      if($angka == 5 || $angka == 6 || $angka == 7 || $angka == 8 || $angka == 11 || $angka == 12 || $angka == 13 || $angka == 14 ){
          
                        if(array_key_exists('downline_kiri',$v3) && array_key_exists('downline_kanan',$v3)){
          
                          $verifikasi_downline_level_3[$v3['id_member']] = $v3;
          
                          $verifikasi_downline_level_3[$v3['id_member']]['posisi'] = $angka;
          
                      
          
                        }
          
            
          
                      }
          
                      // cek apabila downline level 3 terdeteksi maka $angka akan dikurangi 1
          
                      if(array_key_exists($v3['up_line'],$verifikasi_downline_level_3)){
          
                        $posisi = $verifikasi_downline_level_3[$v3['up_line']]['posisi'];
          
                        // echo $angka.'-1 '.$posisi.' '.$v3['up_line'].'<br>';
          
                        $hasil[$posisi] = $verifikasi_downline_level_3[$v3['up_line']];
          
                        $angka--;
          
                        
          
                      }else{
          
                        $hasil[$angka] = $v3;
          
                        
          
                      }
          
                      // cek apabila downline level 3 terdeteksi maka $angka akan dikurangi 1
          
                      /// apabila pos 4 = 0 maka pada saat angka di no  itu di tambahkan 2 agar bisa masuk di posisi yang tepat
          
                     
          
                        
          
                    
          
                      
          
                    }
          
                  }
          
                }
          
              }
          
              
            
          
              // echo '<pre>';
          
              // print_r($this->listing_downline_data);
          
              // echo '</pre>';
          
          
          
              // //
          
              // echo '<pre>';
          
              // print_r($verifikasi_downline_level_3);
          
              // echo '</pre>';
          
            
          
              // echo '<pre>';
          
              // print_r($hasil);
          
              // echo '</pre>';
          
            
          
              // echo '<pre>';
          
              // print_r($this->listing_downline_id);
          
              // echo '</pre>';
          
              // die();
          
            
          
              $data['listing_downline_data'] = $this->listing_downline_data;
          
              $data['hasil_downline'] = json_encode($hasil);
          
              //$data['upline'] = $this->crut->list_datas($upline);
          
                $data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css');
				$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
                              'plugins/datatables/dataTables.bootstrap.min.js',
                              'plugins/jQueryUI/jquery-ui.min.js'
                        );

          
              $view = $themes_name.'/member_master/hirarki.tpl'; // lokasi view di tpl
          
              $this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya
          
                  }







protected function statement_view_kiri($id_member){

      $q_detail = "select id_member,nama_lengkap, id_sponsor,id_upline,downline_kiri,downline_kanan,date_format(created_date,'%d/%m/%Y') as tgl from member_master where id_member = '".$id_member."' ";

      $d_detail = $this->crut->list_row($q_detail);

      if($d_detail !=0){



        $this->list_member_statement_kiri[] = array(

                              'id_member'=>$d_detail['id_member'],

                              'nama_lengkap'=>$d_detail['nama_lengkap'],

                              'sponsor'=>$d_detail['id_sponsor'],

                              'upline'=>$d_detail['id_upline'],

                              'tgl'=>$d_detail['tgl']

                              );



        if($d_detail['downline_kiri'] !=""){

          $this->statement_view_kiri($d_detail['downline_kiri']);

        }

        if($d_detail['downline_kanan'] !=""){

          $this->statement_view_kiri($d_detail['downline_kanan']);

        }

      }

    }



protected function statement_view_kanan($id_member){

      $q_detail = "select id_member,nama_lengkap, id_sponsor,id_upline,downline_kanan,downline_kiri,date_format(created_date,'%d/%m/%Y') as tgl from member_master where id_member = '".$id_member."' ";

      $d_detail = $this->crut->list_row($q_detail);

      if($d_detail !=0){



        $this->list_member_statement_kanan[] = array(

                              'id_member'=>$d_detail['id_member'],

                              'nama_lengkap'=>$d_detail['nama_lengkap'],

                              'sponsor'=>$d_detail['id_sponsor'],

                              'upline'=>$d_detail['id_upline'],

                              'tgl'=>$d_detail['tgl']

                              );



        if($d_detail['downline_kiri'] !=""){

          $this->statement_view_kanan($d_detail['downline_kiri']);

        }

        if($d_detail['downline_kanan'] !=""){

          $this->statement_view_kanan($d_detail['downline_kanan']);

        }



      }

    }



    public function data_statement_kiri($id_member,$level = 1){

       if($level > 1){ /// proses level 1

          $level = $level + 1;

       }

        $q_detail = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,
        
                          DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,
        
                          c.`nama_lengkap` AS nama_upline FROM member_master AS a
        
                          INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`
        
                          INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`
        
                          LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$id_member."' ";

        $d_detail = $this->crut->list_row($q_detail);

        if($d_detail != 0){

            

                  $this->list_member_statement_kiri[$id_member] = $d_detail;

                    $this->list_member_statement_kiri[$d_detail['id_member']]['level'] = $level;

                    

                    if($d_detail['downline_kiri'] !="" || $d_detail['downline_kanan'] !=""){

                      $level = $level + 1;

                      /// cek downline kiri

                      if($d_detail['downline_kiri'] !=""){

                        $q_detail_2 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,
                        
                                          DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,
                        
                                          c.`nama_lengkap` AS nama_upline FROM member_master AS a
                        
                                          INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`
                        
                                          INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`
                        
                                          LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kiri']."' ";

                        $d_detail_2 = $this->crut->list_row($q_detail_2);  

                        if($d_detail_2 != 0){

                          

                          $this->list_member_statement_kiri[$d_detail_2['id_member']] = $d_detail_2;

                            $this->list_member_statement_kiri[$d_detail_2['id_member']]['level'] = $level;

                        }

                      }

                      /// cek downline kiri

                      /// cek downline kanan

                      if($d_detail['downline_kanan'] !=""){

                        // echo $d_detail['id_member'].' '.$d_detail['downline_kanan'].'<br>';

                        $q_detail_3 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,
                        
                                          DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,
                        
                                          c.`nama_lengkap` AS nama_upline FROM member_master AS a
                        
                                          INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`
                        
                                          INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`
                        
                                          LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kanan']."' ";

                        $d_detail_3 = $this->crut->list_row($q_detail_3);  

                        if($d_detail_3 !=0){

                          

                          $this->list_member_statement_kiri[$d_detail_3['id_member']] = $d_detail_3;

                            $this->list_member_statement_kiri[$d_detail_3['id_member']]['level'] = $level;

                        }

                      }

                      /// cek downline kanan

                      /// cek downline level 2

                      if($d_detail_2['downline_kiri'] !=""){

                        $this->data_statement_kiri($d_detail_2['downline_kiri'],$level);

                      }

                      if($d_detail_2['downline_kanan'] !=""){

                        $this->data_statement_kiri($d_detail_2['downline_kanan'],$level);

                      }



                      if($d_detail_3['downline_kiri'] !=""){

                        $this->data_statement_kiri($d_detail_3['downline_kiri'],$level);

                      }

                      if($d_detail_3['downline_kanan'] !=""){

                        $this->data_statement_kiri($d_detail_3['downline_kanan'],$level);

                      }

                      /// cek downline level 2

                    }

        }

      

      

    }



    public function data_statement_kanan($id_member,$level = 1){

      if($level > 1){ /// proses level 1

         $level = $level + 1;

      }

       $q_detail = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,
       
                         DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,
       
                         c.`nama_lengkap` AS nama_upline FROM member_master AS a
       
                         INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`
       
                         INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`
       
                         LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$id_member."' ";

       $d_detail = $this->crut->list_row($q_detail);

       if($d_detail != 0){

           

                 $this->list_member_statement_kanan[$id_member] = $d_detail;

                   $this->list_member_statement_kanan[$d_detail['id_member']]['level'] = $level;

                   

                   if($d_detail['downline_kiri'] !="" || $d_detail['downline_kanan'] !=""){

                     $level = $level + 1;

                     /// cek downline kiri

                     if($d_detail['downline_kiri'] !=""){

                       $q_detail_2 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,
                       
                                         DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,
                       
                                         c.`nama_lengkap` AS nama_upline FROM member_master AS a
                       
                                         INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`
                       
                                         INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`
                       
                                         LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kiri']."' ";

                       $d_detail_2 = $this->crut->list_row($q_detail_2);  

                       if($d_detail_2 != 0){

                         

                         $this->list_member_statement_kanan[$d_detail_2['id_member']] = $d_detail_2;

                           $this->list_member_statement_kanan[$d_detail_2['id_member']]['level'] = $level;

                       }

                     }

                     /// cek downline kiri

                     /// cek downline kanan

                     if($d_detail['downline_kanan'] !=""){

                       // echo $d_detail['id_member'].' '.$d_detail['downline_kanan'].'<br>';

                       $q_detail_3 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,
                       
                                         DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,
                       
                                         c.`nama_lengkap` AS nama_upline FROM member_master AS a
                       
                                         INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`
                       
                                         INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`
                       
                                         LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kanan']."' ";

                       $d_detail_3 = $this->crut->list_row($q_detail_3);  

                       if($d_detail_3 !=0){

                         

                         $this->list_member_statement_kanan[$d_detail_3['id_member']] = $d_detail_3;

                           $this->list_member_statement_kanan[$d_detail_3['id_member']]['level'] = $level;

                       }

                     }

                     /// cek downline kanan

                     /// cek downline level 2

                     if($d_detail_2['downline_kiri'] !=""){

                       $this->data_statement_kanan($d_detail_2['downline_kiri'],$level);

                     }

                     if($d_detail_2['downline_kanan'] !=""){

                       $this->data_statement_kanan($d_detail_2['downline_kanan'],$level);

                     }



                     if($d_detail_3['downline_kiri'] !=""){

                       $this->data_statement_kanan($d_detail_3['downline_kiri'],$level);

                     }

                     if($d_detail_3['downline_kanan'] !=""){

                       $this->data_statement_kanan($d_detail_3['downline_kanan'],$level);

                     }

                     /// cek downline level 2

                   }

       }

     

     

   }



    public function total_downline($id){

      $q_member = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

      DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

      c.`nama_lengkap` AS nama_upline FROM member_master AS a

      LEFT JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

      LEFT JOIN member_master AS c ON a.`id_upline` = c.`id_member`

      LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id ='$id' ";

      $data['get'] = $this->crut->list_row($q_member);



      // list member kiri

      $this->statement_view_kiri($data['get']['downline_kiri']);

      $this->statement_view_kanan($data['get']['downline_kanan']);

      $total_kiri = count($this->list_member_statement_kiri);

      $total_kanan = count($this->list_member_statement_kanan);



      echo json_encode(array('id'=>$id,'total_kiri'=>$total_kiri,'total_kanan'=>$total_kanan));

    }



private function listing_downline($id_member,$no_level = 0){

          /// cek downline member master

          $q_cek = "select downline_kiri,downline_kanan from member_master where id_member ='".$id_member."'";

          // echo $q_cek;

          // die();

          $d_cek = $this->crut->list_row($q_cek);

          $d_cek_2_kiri = 0;

          $d_cek_2_kanan = 0;

          if($d_cek!=0){

                  // echo $this->no_level_downline.' '.$no_level.'<br>';

              /// cek level begin

                  if(array_key_exists($this->no_level_downline, $this->listing_downline_data)){

                      $cek_data = $this->listing_downline_data[$this->no_level_downline];

                      $nilai_cek = $this->no_level_downline + $this->no_level_downline;

                      $hitung_total_data = 0;

                      if(array_key_exists('subs', $cek_data)){

                          foreach ($cek_data as $k => $v) {

                              if(array_key_exists('kiri', $v)){

                                  $hitung_total_data = $hitung_total_data + count($v['kiri']);

                              }

                              if(array_key_exists('kanan', $v)){

                                  $hitung_total_data = $hitung_total_data + count($v['kanan']);

                              }

                          }



                          if($hitung_total_data == $nilai_cek){

                              $this->no_level_downline = $no_level + 1;

                          }

                      }

                  }



              /// cek level

              // $this->no_level_downline = $no_level + 1;

              $this->listing_downline_data[$this->no_level_downline]['subs'][$id_member]['kiri'] = array(

                    'id_member'=>'',

                    'nama_lengkap'=> '',

                    'image'=>'00x',

                    'id'=>'',

                    'up_line'=>$id_member

                  );

              $this->listing_downline_data[$this->no_level_downline]['subs'][$id_member]['kanan'] = array(

                    'id_member'=>'',

                    'nama_lengkap'=> '',

                    'image'=>'00x',

                    'id'=>'',

                    'up_line'=>$id_member

                  );

              if($d_cek['downline_kiri'] !=""){

                  $q_cek_2_kiri = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

                  DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

                  c.`nama_lengkap` AS nama_upline FROM member_master AS a

                  INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

                  INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

                  LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member ='".$d_cek['downline_kiri']."'";

                  $d_cek_2_kiri = $this->crut->list_row($q_cek_2_kiri);



                  $this->listing_downline_data[$this->no_level_downline]['subs'][$d_cek_2_kiri['id_upline']]['kiri'] = $d_cek_2_kiri;
                  $this->listing_downline_data[$this->no_level_downline]['subs'][$d_cek_2_kiri['id_upline']]['kiri']['up_line'] = $d_cek_2_kiri['id_upline'];

                  // $this->statement_view_kiri($d_cek_2_kiri['downline_kiri']);

                  // $this->statement_view_kanan($d_cek_2_kiri['downline_kanan']);

                  $total_kiri = count($this->list_member_statement_kiri);

                  $total_kanan = count($this->list_member_statement_kanan);

                  $this->listing_downline_data[$this->no_level_downline]['subs'][$d_cek_2_kiri['id_upline']]['kiri']['total_kiri'] = $total_kiri;

                  $this->listing_downline_data[$this->no_level_downline]['subs'][$d_cek_2_kiri['id_upline']]['kiri']['total_kanan'] = $total_kanan;

              }





              if($d_cek['downline_kanan'] !=""){

                  $q_cek_2_kanan = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

                  DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

                  c.`nama_lengkap` AS nama_upline FROM member_master AS a

                  INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

                  INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

                  LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member ='".$d_cek['downline_kanan']."'";

                  $d_cek_2_kanan = $this->crut->list_row($q_cek_2_kanan);



                  $this->listing_downline_data[$this->no_level_downline]['subs'][$d_cek_2_kanan['id_upline']]['kanan'] = $d_cek_2_kanan;
                  $this->listing_downline_data[$this->no_level_downline]['subs'][$d_cek_2_kanan['id_upline']]['kanan']['up_line'] = $d_cek_2_kanan['id_upline'];

                  // $this->statement_view_kiri($d_cek_2_kanan['downline_kiri']);

                  // $this->statement_view_kanan($d_cek_2_kanan['downline_kanan']);

                  $total_kiri = count($this->list_member_statement_kiri);

                  $total_kanan = count($this->list_member_statement_kanan);

                  $this->listing_downline_data[$this->no_level_downline]['subs'][$d_cek_2_kanan['id_upline']]['kanan']['total_kiri'] = $total_kiri;

                  $this->listing_downline_data[$this->no_level_downline]['subs'][$d_cek_2_kanan['id_upline']]['kanan']['total_kanan'] = $total_kanan;

              }

              if($d_cek_2_kiri !=0){

                      $this->listing_downline($d_cek_2_kiri['id_member'],$this->no_level_downline);

                  }

              if($d_cek_2_kanan !=0){

                      $this->no_level_downline = $this->no_level_downline - 1;

                      $this->listing_downline($d_cek_2_kanan['id_member'],$this->no_level_downline);



                  }







          }

      }



      public function listing_downline_baru($id_member, $total_stored_data = 0){

        $q_cek = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan FROM member_master AS a where a.id_member ='".$id_member."'";

        // echo $q_cek;

        // die();

        $d_cek = $this->crut->list_row($q_cek);

        $d_cek_2_kiri = 0;

        $d_cek_2_kanan = 0;

        // $total_stored_data = count($this->listing_downline_id);

        if($total_stored_data > 0){

          $total_stored_data = $total_stored_data + 1;

        }

        // echo $id_member.' nilai total '.$total_stored_data.'<br>';

        if($d_cek!=0){

          if($d_cek['downline_kiri'] !=""){

            $total_stored_data = $total_stored_data + 1;

            // echo $d_cek['downline_kiri'].' nilai total '.$total_stored_data.'<br>';



            $q_cek_2_kiri = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

            DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

            c.`nama_lengkap` AS nama_upline FROM member_master AS a

            INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

            INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

            LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member ='".$d_cek['downline_kiri']."'";

            $d_cek_2_kiri = $this->crut->list_row($q_cek_2_kiri);

            if($d_cek_2_kiri !=0){

              // $this->statement_view_kiri($d_cek_2_kiri['downline_kiri']);

              // $this->statement_view_kanan($d_cek_2_kiri['downline_kanan']);

              $total_kiri = count($this->list_member_statement_kiri);

              $total_kanan = count($this->list_member_statement_kanan);



              $this->listing_downline_id[$total_stored_data] = $d_cek_2_kiri;

              $this->listing_downline_id[$total_stored_data]['total_kiri'] = $total_kiri;

              $this->listing_downline_id[$total_stored_data]['total_kanan'] = $total_kanan;



              /// cek downline kanan

              if($d_cek['downline_kanan'] !=""){

                $total_stored_data = $total_stored_data + 1;

                // echo $d_cek['downline_kanan'].' nilai total '.$total_stored_data.'<br>';

                $q_cek_2_kanan = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

                DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

                c.`nama_lengkap` AS nama_upline FROM member_master AS a

                INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

                INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

                LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member ='".$d_cek['downline_kanan']."'";

                $d_cek_2_kanan = $this->crut->list_row($q_cek_2_kanan);

                if($d_cek_2_kanan !=0){

                  // $this->statement_view_kiri($d_cek_2_kanan['downline_kiri']);

                  // $this->statement_view_kanan($d_cek_2_kanan['downline_kanan']);

                  $total_kiri = count($this->list_member_statement_kiri);

                  $total_kanan = count($this->list_member_statement_kanan);



                  $this->listing_downline_id[$total_stored_data] = $d_cek_2_kanan;

                  $this->listing_downline_id[$total_stored_data]['total_kiri'] = $total_kiri;

                  $this->listing_downline_id[$total_stored_data]['total_kanan'] = $total_kanan;



                  /// lanjutkan fungsi



                  if($d_cek_2_kiri['downline_kiri'] !=""){

                    $total_stored_data = $total_stored_data + 1;

                    $this->listing_downline_baru($d_cek_2_kiri['downline_kiri'],$total_stored_data);

                  }

                  if($d_cek_2_kiri['downline_kanan'] !=""){

                    $total_stored_data = $total_stored_data + 1;



                    $this->listing_downline_baru($d_cek_2_kiri['downline_kanan'],$total_stored_data);

                  }



                  if($d_cek['downline_kanan'] !="" && $d_cek_2_kanan['downline_kiri'] !=""){

                    $total_stored_data = $total_stored_data + 1;



                    $this->listing_downline_baru($d_cek_2_kanan['downline_kiri'],$total_stored_data);

                  }

                  if($d_cek['downline_kanan'] !="" &&  $d_cek_2_kanan['downline_kanan'] !=""){

                    $total_stored_data = $total_stored_data + 1;



                    $this->listing_downline_baru($d_cek_2_kanan['downline_kanan'],$total_stored_data);

                  }

                  /// lanjutkan fungsi





                }

              }

              if($d_cek['downline_kanan'] == ""){

                if($d_cek_2_kiri['downline_kiri'] !=""){

                  $total_stored_data = $total_stored_data + 1;

                  $this->listing_downline_baru($d_cek_2_kiri['downline_kiri'],$total_stored_data);

                }

                if($d_cek_2_kiri['downline_kanan'] !=""){

                  $total_stored_data = $total_stored_data + 1;



                  $this->listing_downline_baru($d_cek_2_kiri['downline_kanan'],$total_stored_data);

                }

              }

              /// cek downline kanan







            }// end d_cek_2_kiri

          }// end downline kiri

        }

      }



      public function views($id='0'){

 

        $themes_name = $this->themes_name;

        $data['themes'] = $themes_name."/index.tpl";

        $data['page_title'] = 'Statement Jaringan';



        if($id=='0'){

            $id= $this->id;

         }else{

           $id= $id;

         }





        $q_member = "select * from member_master where id='$id' ";

        $data['get'] = $this->crut->list_row($q_member);

        $this->data_statement_kiri($data['get']['downline_kiri']);

        $this->data_statement_kanan($data['get']['downline_kanan']);

        

        // $this->statement_view_kiri($data['get']['downline_kiri']);

        // $this->statement_view_kanan($data['get']['downline_kanan']);





        $total_kiri = count($this->list_member_statement_kiri);

        $total_kanan = count($this->list_member_statement_kanan);

        // echo $total_kanan;



        $data['jml_jaringan'] = $total_kiri + $total_kanan;



        // list member kiri





        $this->listing_downline($data['get']['id_member']);



        ///total bonus

    		

			

			$data['bonus_sponsor'] = 0;

			$data['bonus_pasangan'] = 0;

			$data['bonus_cabang'] = 0;

			$cek_bonus_sponsor = "select count(*) total_sponsor from bonus_sponsor where id_member ='".$data['get']['id_member']."' and status_approve ='1' group by id_member";

			$d_bonus = $this->crut->list_row($cek_bonus_sponsor);

			

			$cek_bonus_pasangan = "select sum(komisi) total_pasangan from bonus_pasangan_2 where id_member='".$data['get']['id_member']."' and status_approve ='1' group by id_member";

			$d_bonus_2 = $this->crut->list_row($cek_bonus_pasangan);

			

			$cek_bonus_cabang= "select count(*) total_cabang from bonus_cabang where id_member ='".$data['get']['id_member']."' and approve ='1' group by id_member";

			$d_bonus_3= $this->crut->list_row($cek_bonus_cabang);

			



    			$bonus_pasangan = $d_bonus_2['total_pasangan'];

				$bonus_cabang= $this->bonus_cabang * $d_bonus_3['total_cabang'];

				$bonus_sponsor= $this->bonus_sponsor * $d_bonus['total_sponsor'];

				$data['total_bonus'] = $bonus_pasangan + $bonus_cabang +  $bonus_sponsor;

    		



        // echo'<pre>';

        // print_r($this->list_member_statement_kiri);

        // echo'</pre>';



        // echo'<pre>';

        // print_r($this->list_member_statement_kanan);

        // die();



        //echo'<pre>';

        //print_r($this->listing_downline_data);

        // die();



        $sponsor = $data['get']['id_sponsor'];

        $upline = $data['get']['id_upline'];

        $q_sponsor = "select * from member_master where id_member='$sponsor' ";

        $q_upline = "select * from member_master where id_member='$upline' ";

        $data['member_kiri'] = $this->list_member_statement_kiri;

        $data['member_kanan'] = $this->list_member_statement_kanan;

        $data['sponsor'] = $this->crut->list_row($q_sponsor);

        $data['upline'] = $this->crut->list_row($q_upline);

        //$data['upline'] = $this->crut->list_datas($upline);

        $data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');

        $data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',

                                    'plugins/datatables/dataTables.bootstrap.min.js');



        $view = $themes_name.'/member_master/views.tpl'; // lokasi view di tpl

        $this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk

    }

	public function sponsoring(){



		$themes_name = $this->themes_name;

		$data['themes'] = $themes_name."/index.tpl";

    $data['page_title'] = 'Tabel Jaringan Sponsor';

    

    

		

    $data['sponsoring'] = $this->data_sponsoring($this->id_member);

		$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');

		$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',

									'plugins/datatables/dataTables.bootstrap.min.js');



		$view = $themes_name.'/member_master/sponsor_2.tpl'; // lokasi view di tpl

		$this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya

  }



  private function data_sponsoring($id_member){

    $array = array();

    $q ="SELECT a.id,a.id_member,a.nama_lengkap,a.id_sponsor,a.id_upline,a.downline_kiri,a.downline_kanan,a.no_hp, CONCAT(b.`type`,' ',b.`kota`) AS kota, DATE_FORMAT(a.`created_date`,'%Y-%m-%d') AS tgl FROM member_master AS a

    INNER JOIN master_kecamatan AS b ON a.`id_kec` = b.`id` where a.id_sponsor='".$id_member."';";

    $data_q = $this->crut->list_datas($q);

    if($data_q !=0){

      foreach ($data_q as $k => $v) {

        $array[] = array(

          $v['id_member'],

          $v['nama_lengkap'],

          $v['kota'],

          $v['no_hp'],

          $v['tgl'],

          anchor(site_url('cbm/member_master/hirarki/'.$v['id']),'Lihat',array('class'=>'btn btn-primary'))

        );

      }

    }

    if($data_q == 0){

      $array = array('pesan'=>'tidak ada sponsoring');

    }

    return $array;

  }

  public function get_sponsoring(){



		// 			$string .='<a href="'.$v['url'].'/'.$kode.'" class="'.$v['class'].'" title="'.$v['title'].'"><i class="'.$v['simbol'].'"></i></a>';

		

		$link[] = array('url'=>site_url('member_master/hirarki'),'class'=>'btn btn-success','title'=>'hirarki','simbol'=>'fa fa-group');



		$this->datatables->select("id,id_member,nama_lengkap,id_sponsor,id_upline,downline_kiri,downline_kanan,no_hp");

		$this->datatables->from('member_master');

		$this->datatables->where('del',0);

		$this->datatables->where('id_sponsor',$this->id_member)

		->unset_column('id')

		// ->unset_column('a.status')

		->add_column('aksi',link_generator_array($link,'$1'),'id')

		// ->add_column('status',ganti_array($this->status,'$2'),'a.status')

		;

		echo $this->datatables->generate('json');

	}
	
	public function autocomplete_hirarki($q =''){
		$q = $this->input->get('term');
		$q_auto = "select id,id_member,nama_lengkap from member_master where CONCAT(id_member,nama_lengkap) like'%".$q."%' and id_sponsor='". $this->id_member ."' ";
		$data_option = $this->crut->list_datas($q_auto);
		$json = array();
		foreach($data_option as $k => $v){
		  $json[] = array('label'=>$v['id_member'].' | '.$v['nama_lengkap'],'value'=>$v['id_member'].' | '.$v['nama_lengkap']);
		}
		// echo '<pre>';
		// print_r($json);
		$json = json_encode($json);
		echo $json;
  }





}

