<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bonus_pasangan extends MY_Controller {


    protected $approval = array('1'=>'Tidak Approval','2'=>'Approval');
	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');
    protected $component_akses = array();
	protected $bonus_pasangan = 0;


  public function __construct()
  {
    parent::__construct();
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
    $this->load->helper('text');
    $this->load->library('form_validation');
    $this->cek_signin();

    $this->id_member = $this->session->userdata('login_member')['id_member'];
    $this->bonus_pasangan = $this->crut->setting('komisi_configuration','bonus_pasangan','setting')['value'];
    $this->load->library('datatables');

  }


  public function fetch($offset = "0"){
    // $this->cek_hak_akses($this->privileges['log_aktivitas']['log_aktivitas'],'1|2');
     $a = $this->uri->segment(5);
     $offset = ($a<>'')?$a:0;
    		$filter = "";
    		$status_komisi = $this->input->get('status_komisi',true);
    		$id_penerima = $this->input->get('penerima',true);
    		// $keyword = $this->input->get('keyword',true);
    		// $ip_user = $this->input->get('ip_user',true);
    		// $user_id = $this->input->get('user_id',true);
    		$tgl_1 = $this->input->get('tgl_1',true);
    		$tgl_2 = $this->input->get('tgl_2',true);
			$this->session->set_userdata('status_komisi', $status_komisi);
			$this->session->set_userdata('penerima', $id_penerima );
			$this->session->set_userdata('tgl_1', $tgl_1);
			$this->session->set_userdata('tgl_2', $tgl_2);
    		if($status_komisi !=""){

    				$filter .=" and a.status_approve ='".$status_komisi."'";

    		}

    		if(!empty($id_penerima)){

						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter .=" and a.id_member ='".$id_penerima."'";

    		}


    		if(!empty($tgl_1) && !empty($tgl_2)){
    				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    		}
  		 $q_bonus_sponsor = "SELECT	a.id as id,
																b.nama_lengkap as penerima,
																a.id_member as id_penerima,
																b.no_rek as no_rek,
															    b.nama_bank as nama_bank,
																b.atas_nama as an_bank,
																DATE_FORMAT(a.tgl, '%d/%m/%Y')AS tgl,
																a.t_pasangan AS jumlah_pasangan,
																a.komisi AS total_komisi,
																CASE
																	WHEN a.status_approve = '1' THEN
																		'Sudah di Approve'
																	WHEN a.status_approve = '0' THEN
																		'Belum di Approve'
																	END AS status_terima,
																 a.tgl_approve,
																 a.status_approve AS kode_approve
													FROM	 bonus_pasangan_2 a
										  INNER JOIN  member_master AS b ON a.`id_member` = b.`id_member`
												WHERE b.id_member = '".$this->id_member."' and 	!isnull(a.id) ".$filter;

       $q_bonus_sponsor_count = "SELECT count(id) as total FROM bonus_pasangan_2 a where a.id_member = '".$this->id_member."' and !isnull(a.id) ".$filter;
				   	// 	$d_log = $this->crut->list_data($q_log);


  		// echo $q_bonus_sponsor;
  		// die();

        $this->load->library('pagination');

        $config['base_url'] = site_url('cbm/komisi/bonus_pasangan/fetch');

        $config['per_page'] = 30;
  		$config['uri_segment'] = 5;
  		$config['num_links'] = 3;
  		$config['full_tag_open'] = '<ul class="pagination">';
  		$config['full_tag_close'] = '</ul>';
  		$config['first_link'] = 'First';
  		$config['first_tag_open'] = '<li>';
  		$config['first_tag_close'] = '</li>';
  		$config['last_link'] = 'Last';
  		$config['last_tag_open'] = '<li>';
  		$config['last_tag_close'] = '</li>';
  		$config['next_link'] = '&gt;';
  		$config['next_tag_open'] = '<li>';
  		$config['next_tag_close'] = '</li>';
  		$config['prev_link'] = '&lt;';
  		$config['prev_tag_open'] = '<li>';
  		$config['prev_tag_close'] = '</li>';
  		$config['cur_tag_open'] = '<li class="active"><a>';
  		$config['cur_tag_close'] = '</a></li>';
  		$config['num_tag_open'] = '<li>';
  		$config['num_tag_close'] = '</li>';

        $list_bonus = $this->crut->list_data($q_bonus_sponsor,$config['per_page'],$offset);
        $config['total_rows'] = $this->crut->list_row($q_bonus_sponsor_count)['total'];

        $this->pagination->initialize($config);


        $data['list_bonus'] = $list_bonus;
        $data['offset'] =  $offset ;
        $data['pagination'] = $this->pagination->create_links();
            $data['komisi_bonus_pasangan'] = $this->bonus_pasangan;

        $data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
                                                            'plugins/select2/select2.min.css',
                                                            'plugins/datepicker/datepicker3.css'
                                                );
        $data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
                                                                'plugins/select2/select2.min.js',
                                                                'plugins/datepicker/bootstrap-datepicker.js'
                                                );
        $themes_name = $this->themes_name;
        $data['themes'] = $themes_name."/index.tpl";
        $data['page_title'] = 'Bonus Pasangan';
        $view = $themes_name.'/bonus_pasangan/fetch.tpl'; // lokasi view di tpl
        $this->parser->parse($view,$data,false,false,$themes_name);
    }


 public function autocomplete_member($q =''){
    $q = $this->input->get('term');
    $q_auto = "select id_member,id,nama_lengkap from member_master where id_sponsor='".$this->id_member."' AND concat(id_member,' ',nama_lengkap) like'%".$q."%'";
    $data_option = $this->crut->list_datas($q_auto);
    $json = array();
    foreach($data_option as $k => $v){
        $json[] = array('label'=>$v['id_member'].'-'.$v['nama_lengkap'],'value'=>$v['id_member'].'-'.$v['nama_lengkap']);
    }
    // echo '<pre>';
    // print_r($json);
    $json = json_encode($json);
    echo $json;
}
public function export()
{

    $this->load->library('Excel_generator');
    $filter = "";
    $status_komisi = $this->session->userdata('status_komisi');
    $id_penerima = $this->session->userdata('login_member')['id_member'];
    $tgl_1 = $this->session->userdata('tgl_1');
    $tgl_2 = $this->session->userdata('tgl_2');

    if(!empty($status_komisi)){
        $filter .=" and a.status_approve ='".$status_komisi."'";
    }
    if(!empty($id_penerima)){
        $pecah = explode('-',$id_penerima.'-');
        $id_penerima = $pecah[0];
        $filter .=" and a.id_member ='".$id_penerima."'";
    }
    if(!empty($tgl_1) && !empty($tgl_2)){
            $filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    }
                        $query = "SELECT	a.id as id,
                                                        b.nama_lengkap as penerima,
                                                        a.id_member as id_penerima,
                                                        CONCAT('`',b.no_rek) as no_rek,
                                                        b.nama_bank as nama_bank,
                                                        b.atas_nama as an_bank,
                                                        DATE_FORMAT(a.tgl, '%d/%m/%Y')AS tgl,
                                                        a.t_pasangan AS jumlah_pasangan,
                                                        a.komisi AS total_komisi,
                                                        CASE
                                                            WHEN a.status_approve = '1' THEN
                                                                'Sudah di Approve'
                                                            WHEN a.status_approve = '0' THEN
                                                                'Belum di Approve'
                                                            END AS status_terima,
                                                         a.tgl_approve,
                                                         a.status_approve AS kode_approve
                                            FROM	 bonus_pasangan_2 a
                                  INNER JOIN  member_master AS b ON a.`id_member` = b.`id_member`
                                        WHERE	! isnull(a.id) and a.komisi > 0 and a.t_pasangan > 0   ".$filter;


    $result = $this->db->query($query);

    $this->excel_generator->set_query($result);
    $this->excel_generator->set_header(array('ID ','PENERIMA','ID PENERIMA','REKENING','BANK','NAMA REKENING','JUMLAH PASANGAN','TOTAL KOMISI','STATUS TERIMA','TANGGAL APPROVE'));
    $this->excel_generator->set_column(array('id','penerima','id_penerima','no_rek','nama_bank','an_bank','jumlah_pasangan','total_komisi','status_terima','tgl_approve'));
    $this->excel_generator->set_width(array( 10, 20,20, 20, 20, 20,20,20,20,20));
    $this->excel_generator->exportTo2003("Bonus Pasangan");  

}




}
