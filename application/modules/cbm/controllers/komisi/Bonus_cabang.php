<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Bonus_cabang extends MY_Controller {





    protected $approval = array('1'=>'Tidak Approval','2'=>'Approval');

	protected $status = array('1'=>'Tidak Aktif','2'=>'Aktif');

    protected $component_akses = array();

	protected $bonus_cabang = 0;





  public function __construct()

  {

    parent::__construct();

    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');

    $this->load->helper('text');

    $this->load->library('form_validation');

	$this->cek_signin();
	$this->cek_hak_akses($this->member_group_akses['member'],'1|2');
	



    $this->id_member = $this->session->userdata('login_member')['id_member'];

    $this->bonus_cabang= $this->crut->setting('komisi_configuration','bonus_cabang','setting')['value'];

    $this->load->library('datatables');



  }





  public function fetch($offset = "0"){

    // $this->cek_hak_akses($this->privileges['log_aktivitas']['log_aktivitas'],'1|2');

     $a = $this->uri->segment(5);
     $offset = ($a<>'')?$a:0;
   
			// $this->cek_hak_akses($this->privileges['log_aktivitas']['log_aktivitas'],'1|2');
    		// $offset = $this->uri->segment(4);
    		$filter = "";
    		$status_komisi = $this->input->get('status_komisi',true);
    		$id_penerima = $this->input->get('penerima',true);
    		// $keyword = $this->input->get('keyword',true);
    		// $ip_user = $this->input->get('ip_user',true);
    		// $user_id = $this->input->get('user_id',true);
    		$tgl_1 = $this->input->get('tgl_1',true);
    		$tgl_2 = $this->input->get('tgl_2',true);
				$this->session->set_userdata('status_komisi', $status_komisi);
				$this->session->set_userdata('penerima', $id_penerima );
				$this->session->set_userdata('tgl_1', $tgl_1);
				$this->session->set_userdata('tgl_2', $tgl_2);

    		if(!empty($status_komisi)){
    			if(empty($filter)){
    				$filter =" where a.approve ='".$status_komisi."'";
    			}else{
    				$filter .=" and a.approve ='".$status_komisi."'";
    			}
    		}

    		if(!empty($id_penerima)){
    			if(empty($filter)){
						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter =" where a.id_member ='".$id_penerima."'";
    			}else{
						$pecah = explode('-',$id_penerima.'-');
						$id_penerima = $pecah[0];
    				$filter .=" and a.id_member ='".$id_penerima."'";
    			}
    		}

    		if(!empty($keyword)){
    			if(empty($filter)){
    				$filter =" where a.data like '%".$keyword."%'";
    			}else{
    				$filter .=" and a.data like '%".$keyword."%'";
    			}
    		}

    		if(!empty($ip_user)){
    			if(empty($filter)){
    				$filter =" where a.ip_user ='".$ip_user."'";
    			}else{
    				$filter .=" and a.ip_user ='".$ip_user."'";
    			}
    		}

    		if(!empty($user_id)){
    			if(empty($filter)){
    				$filter =" where CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
    			}else{
    				$filter .=" and CONCAT(b.`nama_depan`,' ',b.`nama_belakang`,' ( ',b.`username`,' )') like '%".$user_id."%'";
    			}
    		}

    		if(!empty($tgl_1) && !empty($tgl_2)){
    			if(empty($filter)){
    				$filter =" where date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    			}else{
    				$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
    			}
    		}



  		 $q_bonus_sponsor = "SELECT
									a.id AS id,
									b.`nama_lengkap` AS nama_penerima,
								b.`id_member` AS id_penerima,
								b.no_rek as no_rek,
									b.nama_bank as nama_bank,
								b.atas_nama as an_bank,
									c.`id_member` AS id_member,
									c.`nama_lengkap` AS nama_member,
									DATE_FORMAT(a.created_date, '%Y-%M-%d')AS tgl,
									a.`approve` AS status_approve,
									CASE
								WHEN a.approve = '1' THEN
									'Sudah Di Approve'
								WHEN a.approve = '0' THEN
									'Belum Di Approve'
								END AS status_terima,
								a.komisi AS komisi
								FROM
									bonus_cabang AS a
								INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
								INNER JOIN member_master AS c ON a.`id_aktivasi` = c.`id_member` WHERE a.id_member = '".$this->id_member."'".$filter;

       $q_bonus_sponsor_count = "SELECT count(a.id) AS total
             FROM bonus_cabang AS a
             INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
             INNER JOIN member_master AS c ON a.`id_aktivasi` = c.`id_member` WHERE a.id_member = '".$this->id_member."'".$filter;
   	// 	$d_log = $this->crut->list_data($q_log);


  		// echo $q_bonus_sponsor;
  		// die();
  		$this->load->library('pagination');

  		$config['base_url'] = site_url('cbm/komisi/bonus_cabang/fetch');

  		$config['per_page'] = 30;
  		$config['uri_segment'] = 5;
  		$config['num_links'] = 3;
  		$config['full_tag_open'] = '<ul class="pagination">';
  		$config['full_tag_close'] = '</ul>';
  		$config['first_link'] = 'First';
  		$config['first_tag_open'] = '<li>';
  		$config['first_tag_close'] = '</li>';
  		$config['last_link'] = 'Last';
  		$config['last_tag_open'] = '<li>';
  		$config['last_tag_close'] = '</li>';
  		$config['next_link'] = '&gt;';
  		$config['next_tag_open'] = '<li>';
  		$config['next_tag_close'] = '</li>';
  		$config['prev_link'] = '&lt;';
  		$config['prev_tag_open'] = '<li>';
  		$config['prev_tag_close'] = '</li>';
  		$config['cur_tag_open'] = '<li class="active"><a>';
  		$config['cur_tag_close'] = '</a></li>';
  		$config['num_tag_open'] = '<li>';
  		$config['num_tag_close'] = '</li>';

  		$list_bonus = $this->crut->list_data($q_bonus_sponsor,$config['per_page'],$offset);

  		$config['total_rows'] = $this->crut->list_row($q_bonus_sponsor_count)['total'];

  		$this->pagination->initialize($config);

  		$data['page_header'] ='Data Bonus Cabang';
  		$data['url_admin'] = ADMINS;
  		$data['url_add'] = '';
  		$data['offset'] =  $offset ;
  		$data['list_bonus'] = $list_bonus;
			$data['komisi_bonus_cabang'] = $this->bonus_cabang;

  		$data['pagination'] = $this->pagination->create_links();

  		$data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css',
  															'plugins/select2/select2.min.css',
  															'plugins/datepicker/datepicker3.css'
  												);
  		$data['js_footer'] = array('plugins/jQueryUI/jquery-ui.js',
  																'plugins/select2/select2.min.js',
  																'plugins/datepicker/bootstrap-datepicker.js'
  												);

        $themes_name = $this->themes_name;

        $data['themes'] = $themes_name."/index.tpl";

        $data['page_title'] = 'Bonus Cabang';

        $view = $themes_name.'/bonus_cabang/fetch.tpl'; // lokasi view di tpl

        $this->parser->parse($view,$data,false,false,$themes_name);

    }





 public function autocomplete_member($q =''){

    $q = $this->input->get('term');

    $q_auto = "select id_member,id,nama_lengkap from member_master where id_sponsor='".$this->id_member."' AND concat(id_member,' ',nama_lengkap) like'%".$q."%'";

    $data_option = $this->crut->list_datas($q_auto);

    $json = array();

    foreach($data_option as $k => $v){

        $json[] = array('label'=>$v['id_member'].'-'.$v['nama_lengkap'],'value'=>$v['id_member'].'-'.$v['nama_lengkap']);

    }

    // echo '<pre>';

    // print_r($json);

    $json = json_encode($json);

    echo $json;

}

public function export()

{
	$this->load->library('Excel_generator');
	$filter = "";
	$status_komisi = $this->session->userdata('status_komisi');
	$id_penerima = $this->session->userdata('login_member')['id_member'];
	$tgl_1 = $this->session->userdata('tgl_1');
	$tgl_2 = $this->session->userdata('tgl_2');

	if(!empty($status_komisi)){
		if(empty($filter)){
			$filter =" where a.status_approve ='".$status_komisi."'";
		}else{
			$filter .=" and a.status_approve ='".$status_komisi."'";
		}
	}

	if(!empty($id_penerima)){
		if(empty($filter)){
				$pecah = explode('-',$id_penerima.'-');
				$id_penerima = $pecah[0];
			$filter =" where a.id_member ='".$id_penerima."'";
		}else{
				$pecah = explode('-',$id_penerima.'-');
				$id_penerima = $pecah[0];
			$filter .=" and a.id_member ='".$id_penerima."'";
		}
	}

	if(!empty($keyword)){
		if(empty($filter)){
			$filter =" where a.data like '%".$keyword."%'";
		}else{
			$filter .=" and a.data like '%".$keyword."%'";
		}
	}



	if(!empty($tgl_1) && !empty($tgl_2)){
		if(empty($filter)){
			$filter =" where date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
		}else{
			$filter .=" and date_format(a.created_date,'%Y-%m-%d') between '".$tgl_1."' and '".$tgl_2."' ";
		}
	}



   $query = "SELECT
	a.id AS id,
	b.`nama_lengkap` AS nama_penerima,
b.`id_member` AS id_penerima,
b.no_rek as no_rek,
	b.nama_bank as nama_bank,
b.atas_nama as an_bank,
	c.`id_member` AS id_member,
	c.`nama_lengkap` AS nama_member,
	DATE_FORMAT(a.created_date, '%Y-%M-%d')AS tgl,
	a.`approve` AS status_approve,
	CASE
WHEN a.approve = '1' THEN
	'Sudah Di Approve'
WHEN a.approve = '0' THEN
	'Belum Di Approve'
END AS status_terima,
a.komisi AS komisi
FROM
	bonus_cabang AS a
INNER JOIN member_master AS b ON a.`id_member` = b.`id_member`
INNER JOIN member_master AS c ON a.`id_aktivasi` = c.`id_member` ".$filter;

	$result = $this->db->query($query);

	$this->excel_generator->set_query($result);
	$this->excel_generator->set_header(array('ID ','PENERIMA','ID PENERIMA','REKENING','BANK','NAMA REKENING','BONUS CABANG','ID MEMBER','NAMA MEMBER','STATUS TERIMA','TANGGAL APPROVE'));
	$this->excel_generator->set_column(array('id','nama_penerima','id_penerima','no_rek','nama_bank','an_bank','komisi','id_member','nama_member','status_terima','tgl'));
	$this->excel_generator->set_width(array( 10,20, 30, 20, 20,20,20,20,20,20,20));
	$this->excel_generator->exportTo2003("Bonus Cabang");


	}









}
