<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifikasi extends MY_Controller{
	private $type_notif = array('0'=>'Bonus Sponsor','1'=>'Bonus Pasangan','2'=>'Bonus Cabang','3'=>'Bonus Reward');
	public function __construct()
  {
    parent::__construct();
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
    $this->load->helper('text');
    $this->load->helper('security');
    $this->cek_signin();
    // $this->cek_hak_akses($this->member_group_akses['member'],'1|2');
    $this->id_member = $this->session->userdata('login_member')['id_member'];
    $this->id = $this->session->userdata('login_member')['id'];
    $this->load->library('datatables');

  }

  public function link($no){
    $id_member = $this->session->userdata('login_member')['id_member'];
    $q_notif = "select * from notifikasi_member where id='".$no."' and id_member ='".$id_member."' order by created_date DESC";
    // echo $q_notif;
    $d_notif = $this->crut->list_row($q_notif);
    if($d_notif !=0){
      $update_notif['status_notif'] = '1';
      $update_notif['updated_date'] = date('Y-m-d H:i:s');
      $res = $this->crut->update(array('id'=>$no),'notifikasi_member',$update_notif,'','','');
      if($res['kode'] == "1"){
        redirect(site_url($d_notif['url_notif']));
      }
    }
  }

	public function lihat_semua_notifikasi(){
		$themes_name = $this->themes_name;
		$data['themes'] = $themes_name."/index.tpl";
		$data['page_title'] = 'Semua Notifikasi';
		$data['type_notif'] = $this->type_notif;

		$data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
															'plugins/datatables/dataTables.bootstrap.min.js'
												);

		$view = $themes_name.'/notifikasi/fetch.tpl'; // lokasi view di tpl
		$this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya
	}

}
