<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends MY_Controller{

	public function __construct()
  {
    parent::__construct();
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
    $this->load->helper('text');
    $this->load->helper('security');
    $this->cek_signin();
    // $this->cek_hak_akses($this->member_group_akses['member'],'1|2');
    $this->id_member = $this->session->userdata('login_member')['id_member'];
    $this->id = $this->session->userdata('login_member')['id'];
    $this->load->library('datatables');

  }

	public function index()
	{
		$this->biodata();

	}

	public function biodata(){
		$themes_name = $this->themes_name;
    $data['themes'] = $themes_name."/index.tpl";
    $data['page_title'] = 'Biodata';
		$id = $this->id;
		$q_member = "select * from member_master where id='$id' ";
    $data['get'] = $this->crut->list_row($q_member);
		$data['sponsor'] = 0;
		$data['upline'] = 0;
		if($data['get']['id_sponsor'] !=""){
			$q_sponsor = "select * from member_master where id_member ='".$data['get']['id_sponsor']."'";
			$d_sponsor = $this->crut->list_row($q_sponsor);
			$data['sponsor'] = $d_sponsor;
		}
		if($data['get']['id_upline'] !=""){
			$q_upline = "select * from member_master where id_member ='".$data['get']['id_upline']."'";
			$d_upline = $this->crut->list_row($q_upline);
			$data['upline'] = $d_upline;
		}


    $data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
	  $data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
									'plugins/datatables/dataTables.bootstrap.min.js');

    $view = $themes_name.'/data_pribadi/biodata.tpl'; // lokasi view di tpl
    $this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya
	}

	public function ganti_pin(){
		$themes_name = $this->themes_name;
    $data['themes'] = $themes_name."/index.tpl";
    $data['page_title'] = 'Ganti Pin';
  	$data['css_head'] = array('plugins/datatables/dataTables.bootstrap.css');
	  $data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
									'plugins/datatables/dataTables.bootstrap.min.js');

    $view = $themes_name.'/data_pribadi/pin.tpl'; // lokasi view di tpl
    $this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya
	}

	public function proses_ganti_pin(){
		$this->form_validation->set_rules('pin_lama', 'Pin Lama', 'trim|required|alpha_dash',
			array(
				'required'=>'%s Tidak Boleh Kosong'
				));
		$this->form_validation->set_rules('pin', 'Pin', 'trim|required|alpha_dash',
			array(
				'required'=>'%s Tidak Boleh Kosong'
				));

		$this->form_validation->set_rules('pin-konfirm', 'PIN Konfirm', 'trim|required|matches[pin]',
			array(
					'required'=>'%s Tidak Boleh Kosong',
					'matches'=>'{field} Harus Sama Dengan Input Pin'
				));
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
		if ($this->form_validation->run() == true) {
			$this->ganti_pin();
		} else {
			$id = $this->id;
			$q_member = "select * from member_master where id='$id' ";
			$data = $this->crut->list_row($q_member);

			$cek_pas = "select * from member_master where id_member ='".$data['id_member']."' and pin ='".do_hash($data['id_member'].'-'.$this->input->post('pin_lama',true),'md5')."'";
			$d_pas = $this->crut->list_row($cek_pas);
			if($d_pas !=0){
				$update['pin'] = do_hash($data['id_member'].'-'.$this->input->post('pin',true),'md5');
				$filter = array('id'=>$this->id);
				$sukses = "Proses Ganti Pin Berhasil";
				$gagal = "Proses Ganti Pin Gagal";
				$response = $this->crut->update($filter,'member_master',$update,'',$sukses,$gagal);

				$this->session->set_flashdata('pesan', pesan($response));
				redirect(site_url('cbm/detail/ganti_pin'));
			}
			if($d_pas == 0){
				$response = array('status'=>false,'kode'=>2,'pesan'=>'Password lama yang anda input salah');
				$this->session->set_flashdata('pesan', pesan($response));
				redirect(site_url('cbm/detail/ganti_pin'));
			}
		}

	}


	public function upload_foto(){
		$this->load->library('upload');
		if(isset($_FILES['foto']) && !empty($_FILES['foto']['name'])){
			$config['upload_path'] = FCPATH.'admin718/assets/img/username/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '10000000';
			$config['file_name'] = $this->session->userdata['login_member']['id_member'];
			$config['overwrite'] = true;

			// $this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ( $this->upload->do_upload('foto')){

				$data = $this->upload->data();

				$update['image'] = $data['file_name'];

				$filter = array('id'=>$this->id);
				$sukses = "Proses Ganti Foto Profil Berhasil";
				$gagal = "Proses Ganti Foto Profil Gagal";
				$response = $this->crut->update($filter,'member_master',$update,'',$sukses,$gagal);
			}

		}else{
			$response = array('status'=>false,'kode'=>2,'pesan'=>'Anda Belum memasukan Gambar');

		}

		$this->session->set_flashdata('pesan', pesan($response));
		redirect(site_url('cbm/detail/biodata'));
	}

}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */
