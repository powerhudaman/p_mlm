<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_manajemen extends MY_Controller {

  private $data_downline_non_aktif = array();
  private $data_downline_non_simpan = array();
  private $status_member = array('1'=>'Belum Aktivasi','2'=>'Aktif');
  private $kupon = 0;
  private $bonus_cabang= 0;
  private $bonus_pasangan = 0;


  public function __construct()
  {
    parent::__construct();
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
    $this->load->helper('text');
    // $this->listing_downline_non_aktif($this->session->userdata('login_member')['id_member']);
    $this->data_member_aktivasi($this->session->userdata('login_member')['id_member']);
    $this->cek_signin();
    $this->kupon = $this->session->userdata('login_member')['kupon'];
    $this->cek_hak_akses($this->member_group_akses['member'],'1|2');
    $this->bonus_cabang = $this->crut->setting('komisi_configuration','bonus_cabang','setting')['value'];
    $this->bonus_pasangan = $this->crut->setting('komisi_configuration','bonus_pasangan','setting')['value'];

    $this->load->library('upload');

  }

  public function index(){

    $themes_name = $this->themes_name;
    $data['themes'] = $themes_name."/index.tpl";
    $data['page_title'] = 'Aktivasi PIN';
    $data['member_belum_aktif'] = $this->data_downline_non_aktif;
    $data['kupon'] = $this->kupon;

    $data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
                              'plugins/datatables/dataTables.bootstrap.min.js'
                        );

    $view = $themes_name.'/member_manajemen/fetch.tpl'; // lokasi view di tpl
    $this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya
  }



  public function aktivasi_pin($id){
    // echo'<pre>';
    // print_r($this->data_downline_non_simpan);
    // die();
    if(array_key_exists($id,$this->data_downline_non_simpan)){
      $data = $this->data_downline_non_simpan[$id];
      $update['pin'] =do_hash($data['id_member'].'-'.$data['pin'],'md5');
      // $update['aktivasi'] = 2;
      $update['update_date'] = date('Y-m-d H:i:s');
      $filter = array('id'=>$id);
      $response = $this->crut->update($filter,'member_master',$update,'','Proses Aktivasi Pin Dengan Member '.$data['id_member'].'-'.$data['nama_lengkap'].' Sukses','Proses Aktivasi Pin Dengan Member '.$data['id_member'].'-'.$data['nama_lengkap'].' Gagal');
      if($response['status'] && $response['kode'] == 1){
        /// insert pin log
        $loging_pin['id_member'] = 'CBM00001';
        $loging_pin['id_member_aktivasi'] = $data['id_member'];
        $loging_pin['pin'] = $data['pin'];
        $loging_pin['pin_enkripsi'] = $update['pin'];
        $loging_pin['created_date'] = date('Y-m-d H:i:s');
        $response_loggin_pin = $this->crut->insert('loging_pin',$loging_pin,'','','');
        $sms_message = $response['pesan'].' Dengan Pin '.$data['pin'];
        // $sms = sms_zensiva($sms_message,$data['no_hp']);

        // echo $sms;
        /// insert pin log
        /// insert bonus cabang
          if($response_loggin_pin['status'] && $response_loggin_pin['kode'] == 1){
            $bonus_cabang['id_member'] =  'CBM00001';
            $bonus_cabang['id_aktivasi'] = $data['id_member'];
            $bonus_cabang['created_date'] = date('Y-m-d H:i:s');
            $response_bonus_cabang = $this->crut->insert('bonus_cabang',$bonus_cabang,'','','');
          }
        /// insert bonus cabang
      }
    }
  }

  private function data_member_aktivasi($id_member){
    $q_cek = "select a.*,b.pin as pin from member_master as a inner join loging_pin as b on b.id_member_aktivasi = a.id_member where b.id_member='".$this->session->userdata('login_member')['id_member']."' ";
    $d_cek = $this->crut->list_datas($q_cek);
    if($d_cek !=0){
      $no = 0;
      foreach ($d_cek as $k => $v) {
        $no++;
        $this->data_downline_non_aktif[] = array(
          $no,
          $v['id_member'],
          $v['nama_lengkap'],
          $v['pin'],
          $this->status_member[$v['aktivasi']],
          $v['created_date']
        );
      }
    }
  }
  private function listing_downline_non_aktif($id_member){
    $q_cek = "select downline_kiri, downline_kanan from member_master where id_member ='".$id_member."'";
    $d_cek = $this->crut->list_row($q_cek);
    if($d_cek !=0){

        if($d_cek['downline_kiri'] !=""){ /// cek downline kiri
          $pin = genRndString('4');
          $cek_downline_kiri = "select * from member_master where id_member ='".$d_cek['downline_kiri']."' and aktivasi ='1' ";
          $d_downline_kiri = $this->crut->list_row($cek_downline_kiri);
          if($d_downline_kiri !=0){
            $this->data_downline_non_aktif[] = array(
              $d_downline_kiri['id_member'],
              $d_downline_kiri['nama_lengkap'],
              $pin,
              $this->status_member[$d_downline_kiri['aktivasi']],
              '<a href="'.site_url('cbm/member_manajemen/aktivasi_pin/').'/'.$d_downline_kiri['id'].'" class="btn btn-primary">Aktivasi</a>'
            );

            $this->data_downline_non_simpan[$d_downline_kiri['id']] = array(
              'id_member'=>$d_downline_kiri['id_member'],
              'nama_lengkap'=>$d_downline_kiri['nama_lengkap'],
              'pin'=>$pin,
              'status'=>$this->status_member[$d_downline_kiri['aktivasi']],
              'aktivasi'=>'<a href="'.site_url('cbm/member_manajemen/aktivasi_pin/').'/'.$d_downline_kiri['id'].'" class="btn btn-primary">Aktivasi</a>',
              'no_hp'=>$d_downline_kiri['no_hp']
            );

            $this->listing_downline_non_aktif($d_downline_kiri['id_member']);
          }
        }/// cek downline kiri
        if($d_cek['downline_kanan'] !=""){ /// cek downline kanan
          $pin = genRndString('4');
          $cek_downline_kanan = "select * from member_master where id_member ='".$d_cek['downline_kanan']."' and aktivasi ='1' ";
          $d_downline_kanan = $this->crut->list_row($cek_downline_kanan);
          if($d_downline_kanan !=0){
            $this->data_downline_non_aktif[] = array(
              $d_downline_kanan['id_member'],
              $d_downline_kanan['nama_lengkap'],
              $pin,
              $this->status_member[$d_downline_kanan['aktivasi']],
              '<a href="'.site_url('cbm/member_manajemen/aktivasi_pin/').'/'.$d_downline_kanan['id'].'" class="btn btn-primary">Aktivasi</a>'
            );

            $this->data_downline_non_simpan[$d_downline_kanan['id']] = array(
              'id_member'=>$d_downline_kanan['id_member'],
              'nama_lengkap'=>$d_downline_kanan['nama_lengkap'],
              'pin'=>$pin,
              'status'=>$this->status_member[$d_downline_kanan['aktivasi']],
              'aktivasi'=>'<a href="'.site_url('cbm/member_manajemen/aktivasi_pin/').'/'.$d_downline_kanan['id'].'" class="btn btn-primary">Aktivasi</a>',
              'no_hp'=>$d_downline_kanan['no_hp']
            );
            $this->listing_downline_non_aktif($d_downline_kanan['id_member']);

          }
        }/// cek downline kanan

    }
  }

  public function registrasi_member(){
    $this->cek_hak_akses($this->member_group_akses['member'],'2');

    if($this->kupon < 1 || $this->kupon == ""){
      $pesan = array('status'=>false,'pesan'=>'Anda Sudah Tidak Memiliki Kupon Untuk Melakukan Registrasi Member Silahkan hubungi Admin untuk melakukan request Kupon','kode'=>2);
      $this->session->set_flashdata('pesan',pesan($pesan));
      redirect(site_url('cbm/member_manajemen/index'));
      die();
    }

    $themes_name = $this->themes_name;
    $data['themes'] = $themes_name."/index.tpl";
    $data['page_title'] = 'Registrasi';
    $data['kupon'] = $this->kupon;
    $data['url'] = 'cbm/member_manajemen/save';

    $data['url_parameter'] = array('propinsi'=>'Propinsi','kota'=>'Kota','kecamatan'=>'Kecamatan');
      $kota_asal = array();
      $kota_asal = $this->crut->setting('kurir_api_bersama','kota_asal','setting');
    if(!array_key_exists('pesan',$kota_asal)){
       $kota_asal = $this->crut->setting('kurir_api_bersama','kota_asal','setting')['value'];
      }
    // daftar propinsi
      $q_propinsi = "select * from master_provinsi";
      $d_propinsi = $this->crut->list_datas($q_propinsi);
      $d_kota = array();
      $d_kecamatan = array();

      if($kota_asal !=""){
        $kota_asal = json_decode($kota_asal,true);


        $q_kota = "select * from master_kota where provinsi_id ='".$kota_asal['propinsi']."'";
        $d_kota = $this->crut->list_datas($q_kota);

        $q_kecamatan = "select * from master_kecamatan where provinsi_id ='".$kota_asal['propinsi']."' and kota_id ='".$kota_asal['kota']."'";
        $d_kecamatan = $this->crut->list_datas($q_kecamatan);
      }

    $data['l_propinsi'] =  $d_propinsi;
    $data['l_kota'] = $d_kota;
    $data['l_kecamatan'] = $d_kecamatan;
    $data['kota_asal'] = $kota_asal;
    //for id member

    $data['css_head'] = array('plugins/jQueryUI/ui-autocomplete.css');
    $data['js_footer'] = array('plugins/datatables/jquery.dataTables.min.js',
                              'plugins/datatables/dataTables.bootstrap.min.js',
                              'plugins/jQueryUI/jquery-ui.min.js'
                        );

    $view = $themes_name.'/member_manajemen/registrasi.tpl'; // lokasi view di tpl
    $this->parser->parse($view,$data,false,false,$themes_name); // cara parsing nya. ini yang digunakan untuk menampilkan data halamannya
  }

  public function save(){
    $this->cek_hak_akses($this->member_group_akses['member'],'2');
    if($this->kupon < 1 || $this->kupon == ""){
      $pesan = array('status'=>false,'pesan'=>'Anda Sudah Tidak Memiliki Kupon Untuk Melakukan Registrasi Member Silahkan hubungi Admin untuk melakukan request Kupon','kode'=>2);
      $this->session->set_flashdata('pesan',pesan($pesan));
      redirect(site_url('cbm/member_manajemen/index'));
      die();
    }

		$this->form_validation->set_rules('pin', 'Pin', 'trim|required',
			array(
				'required'=>'%s Tidak Boleh Kosong'
				));

		$this->form_validation->set_rules('pin-konfirm', 'PIN Konfirm', 'trim|required|matches[pin]',
			array(
					'required'=>'%s Tidak Boleh Kosong',
					'matches'=>'{field} Harus Sama Dengan Input Pin'
				));
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			$this->registrasi_member();
		} else {



		$arrData = array(
		      'nama_lengkap', 'no_identitas', 'alamat', 'id_prov', 'id_kota', 'id_kec' ,'no_hp','no_telp','ahli_waris','email','hubungan','nama_bank','nama_bank','atas_nama','no_rek'
		  );

		foreach($arrData as $dt){
		        $input[$dt] =  $this->input->post($dt,true);

		}
		    //Generate ID Member
		    $q = $this->db->query("SELECT max(id_member) as no_akhir FROM member_master ");
		    $row = $q->row();
		    //  $id_kota =  $row->id;
		    if ($q->num_rows() > 0)
		      {
		      $no = $row->no_akhir;
		      $no_akhir = (int)substr($no,3, 7);
		      $no_akhir++;
		      // membuat format kode
		      $pattern = 'CBM'.sprintf("%07s", $no_akhir);
		      $id_member = $pattern;
		      }
		      else
		      { //if empty
		      $pattern = 'CBM0000001';
		      $id_member = $pattern;

		      }
			  $input['id_member'] = $id_member;
		      $input['id_group'] = $input['id_group'] = $this->crut->setting('global_configuration','member_group','setting')['value'];

			$input['id_sponsor'] = explode(" | ",$this->input->post('id_sponsor',true))[0];
			$input['id_upline'] = explode(" | ",$this->input->post('id_upline',true))[0];
			$t = date('Y-m-d');
			$t = strtotime($t);
			$d = strtotime("+1 Year",$t);
			$input['expired_date'] = date('Y-m-d',$d);
			$input['created_date'] = date("Y-m-d H:i:S");
      $input['aktivasi'] = '1';
			// $input['created_by'] = $this->session->userdata('user_id');

			$input['pin'] = do_hash($id_member.'-'.$this->input->post('pin'),'md5');
			//$input['sort_order'] = $this->input->post('order',true);
			$input['del'] = '0';
			$sukses = "Proses Registrasi Member dengan Nama = ".$input['nama_lengkap']." Berhasil";
			$gagal = "Proses Registrasi Member dengan Nama = ".$input['nama_lengkap']." Gagal";

			$cek_upline = "select * from member_master where id_member='".$input['id_upline']."'";
			$d_cek_upline = $this->crut->list_row($cek_upline);
			if(!empty($d_cek_upline['downline_kiri']) && !empty($d_cek_upline['downline_kanan'])){
				$pesan = array('status'=>false,'kode'=>2,'pesan'=>'Proses Registrasi Gagal Karena id Upline '.$this->input->post('id_upline',true).' telah melibihi jumlah downlinenya ');
				$this->session->set_flashdata('pesan', pesan($pesan));
				redirect(site_url('cbm/member_manajemen/index'));
				die();
			}else{
				$pesan = $this->crut->insert('member_master',$input,'Tambah Member',$sukses,$gagal);
			}
      /// insert pin log
      $loging_pin['id_member'] = $this->session->userdata('login_member')['id_member'];
      $loging_pin['id_member_aktivasi'] = $input['id_member'];
      $loging_pin['pin'] = $this->input->post('pin',true);
      $loging_pin['pin_enkripsi'] = $input['pin'];
      $loging_pin['created_date'] = date('Y-m-d H:i:s');
      $response_loggin_pin = $this->crut->insert('loging_pin',$loging_pin,'','','');
      $sms_message = $response['pesan'].' Dengan Pin '.$this->input->post('pin',true);
      // $sms = sms_zensiva($sms_message,$data['no_hp']);

      // echo $sms;
      /// insert pin log
      /// insert bonus cabang
        if($response_loggin_pin['status'] && $response_loggin_pin['kode'] == 1){
          $bonus_cabang['id_member'] =  $this->session->userdata('login_member')['id_member'];
          $bonus_cabang['komisi'] =  $this->bonus_cabang;
          $bonus_cabang['id_aktivasi'] = $input['id_member'];
          $bonus_cabang['created_date'] = date('Y-m-d H:i:s');
          $response_bonus_cabang = $this->crut->insert('bonus_cabang',$bonus_cabang,'','','');
          if($response_bonus_cabang['status'] && $response_bonus_cabang['kode'] == 1){
            /// update total kupon
            $update_kupon['kupon'] = $this->kupon - 1;
            $filter_kupon = array('id_member'=>$this->session->userdata('login_member')['id_member']);
            $response_kupon = $this->crut->update($filter_kupon,'member_master',$update_kupon,'','','');
            if($response_kupon['status'] && $response_kupon['kode'] == 1){
              $_SESSION['login_member']['kupon'] = $update_kupon['kupon'];
            }
            /// update total kupon
          }

        }
      /// insert bonus cabang

      if($pesan['status'] && $pesan['kode'] == '1' && $input['id_upline'] !=""){
				$q_cek = "select * from member_master where id_member ='".$input['id_upline']."'";
				$d_cek = $this->crut->list_row($q_cek);
				if($d_cek['downline_kiri'] == "" || $d_cek['downline_kanan'] == ""){
					if($d_cek['downline_kiri'] == ""){
					$update_downline['downline_kiri'] = $input['id_member'];
					}
					if($d_cek['downline_kiri'] !="" && $d_cek['downline_kanan'] == ""){
						$update_downline['downline_kanan'] = $input['id_member'];
					}
					$filter=array('id_member'=>$input['id_upline']);
					$this->crut->update($filter,'member_master',$update_downline,'','','');
				}

				// panggil bonus pasangan
        if($input['id_upline'] !=""){
          $this->input_bonus_pasangan_2($input['id_upline']);
				}
				
				// insert bonus_sponsor
				if($input['id_sponsor'] !=""){
					$input_sponsor['kode'] = microtime();
					$input_sponsor['id_member'] = $input['id_sponsor'];
					$input_sponsor['id_downline'] = $id_member;
					$input_sponsor['komisi'] = $this->bonus_sponsor;
					$input_sponsor['created_date'] = date('Y-m-d H:i:s');
					$input_sponsor['created_by'] = $this->session->userdata('user_id');
					$input_sponsor['status_auto_save'] = 2;
					// cek auto_save
					$q_as = "select id from bonus_sponsor where id_member ='".$input['id_sponsor']."' ";
					$d_as = $this->crut->list_datas($q_as);
					if($d_as == 0){
						$input_sponsor['status_auto_save'] = 1;
					}
					// cek auto_save
					$this->crut->insert('bonus_sponsor',$input_sponsor,'','','');

				}
				// insert bonus_sponsor

			if(isset($_FILES['foto']) && !empty($_FILES['foto']['name'])){
				$config['upload_path'] = FCPATH.'admin718/assets/img/username/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '100000';
				$config['file_name'] = $id_member;
				$config['overwrite'] = true;

				// $this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ( $this->upload->do_upload('foto')){

					$data = $this->upload->data();

					$update['image'] = $data['file_name'];

					$filter = array('id_member'=>$input['id_member']);
					$this->crut->update($filter,'member_master',$update,'','','');
				}

			}

			//insert tbl admins status = 1 (TIDAK AKTF)




				//insert log
				// $dt = array('input'=>$input);
				// $insert_log = array(
				// 	'kode_log'=>microtime(),
				// 	'halaman'=>'MemberMaster',
				// 	'jenis_operasi'=>'Insert',
				// 	'data'=>json_encode($dt),
				// 	'ip_user'=>$_SERVER['REMOTE_ADDR'],
				// 	'user_id'=>$this->session->userdata('user_id'),
				// 	'date_created'=>date('Y-m-d H:i:s')
				// );
				// $this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log


			}
			$this->session->set_flashdata('pesan', pesan($pesan));
			redirect(site_url('cbm/member_manajemen/index'));
		}
	}


  protected function input_bonus_pasangan($id_upline,$data_downline = array()){
    $q_cek_upline = "select downline_kiri, downline_kanan,id_upline from member_master where id_member ='".$id_upline."' and !isnull(downline_kiri) and !isnull(downline_kanan)";
    $d_cek_upline = $this->crut->list_row($q_cek_upline);
    if(count($data_downline) == 0){ /// ini digunakan bila yang pertama dapet bonus
    if($d_cek_upline !=0){
      $insert_bonus_pasangan['id_penerima'] = $id_upline;
      $insert_bonus_pasangan['kode'] = genRndString($length = 6, $chars = '1234567890BCDFGHJKLMNPQRSTVWXYZ');
      $insert_bonus_pasangan['id_pasangan_1'] = $d_cek_upline['downline_kiri'];
      $insert_bonus_pasangan['id_pasangan_2'] = $d_cek_upline['downline_kanan'];
      $insert_bonus_pasangan['komisi'] = $this->bonus_pasangan;
      $insert_bonus_pasangan['tgl'] = date('Y-m-d');
      $insert_bonus_pasangan['status_approve'] = 0;
      $insert_bonus_pasangan['status_view'] = 1;
      $insert_bonus_pasangan['created_date'] = date('Y-m-d H:i:s');
      $insert_bonus_pasangan['created_by'] = $this->session->userdata('user_id');
      $response_bonus_pasangan = $this->crut->insert('bonus_pasangan',$insert_bonus_pasangan,'','','');

      //insert log
      $dt = array('input'=>$insert_bonus_pasangan);
      $insert_log = array(
        'kode_log'=>microtime(),
        'halaman'=>'BonusPasangan',
        'jenis_operasi'=>'Insert',
        'data'=>json_encode($dt),
        'ip_user'=>$_SERVER['REMOTE_ADDR'],
        'user_id'=>$this->session->userdata('user_id'),
        'date_created'=>date('Y-m-d H:i:s')
      );
      $this->crut->insert('log_aktivitas',$insert_log,'','','');
      //insert log

      // bila member masih punya upline
      if($response_bonus_pasangan['kode'] == 1 && $d_cek_upline['id_upline'] !=""){
        $this->input_bonus_pasangan($d_cek_upline['id_upline'],$d_cek_upline);
      }
      // bila member masih punya upline
    }
    }/// ini digunakan bila yang pertama dapet bonus
    if(count($data_downline) > 0){
    $q_cek_upline_2 = "select id_upline from member_master where id_member ='".$id_upline."' ";
    $d_cek_upline_2 = $this->crut->list_row($q_cek_upline_2);
    if($d_cek_upline_2 !=0){
      $insert_bonus_pasangan['id_penerima'] = $id_upline;
      $insert_bonus_pasangan['kode'] = genRndString($length = 6, $chars = '1234567890BCDFGHJKLMNPQRSTVWXYZ');
      $insert_bonus_pasangan['id_pasangan_1'] = $data_downline['downline_kiri'];
      $insert_bonus_pasangan['id_pasangan_2'] = $data_downline['downline_kanan'];
      $insert_bonus_pasangan['komisi'] = $this->bonus_pasangan;
      $insert_bonus_pasangan['tgl'] = date('Y-m-d');
      $insert_bonus_pasangan['status_approve'] = 0;
      $insert_bonus_pasangan['status_view'] = 1;
      $insert_bonus_pasangan['created_date'] = date('Y-m-d H:i:s');
      $insert_bonus_pasangan['created_by'] = $this->session->userdata('user_id');
      $response_bonus_pasangan = $this->crut->insert('bonus_pasangan',$insert_bonus_pasangan,'','','');

      //insert log
      $dt = array('input'=>$insert_bonus_pasangan);
      $insert_log = array(
        'kode_log'=>microtime(),
        'halaman'=>'BonusPasangan',
        'jenis_operasi'=>'Insert',
        'data'=>json_encode($dt),
        'ip_user'=>$_SERVER['REMOTE_ADDR'],
        'user_id'=>$this->session->userdata('user_id'),
        'date_created'=>date('Y-m-d H:i:s')
      );
      $this->crut->insert('log_aktivitas',$insert_log,'','','');
      //insert log

      // bila member masih punya upline
      if($response_bonus_pasangan['kode'] == 1 && $d_cek_upline_2['id_upline'] !=""){
        $this->input_bonus_pasangan($d_cek_upline_2['id_upline'],$data_downline);
      }
      // bila member masih punya upline
    }
    }
  }

  public function input_bonus_pasangan_2($id_member,$t_pasangan = ''){
		$this->list_member_statement_kiri = array();
		$this->list_member_statement_kanan = array();

		$q_member = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan,a.`id_upline`, d.`type`, d.`kota`,

		DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

		c.`nama_lengkap` AS nama_upline FROM member_master AS a

		LEFT JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

		LEFT JOIN member_master AS c ON a.`id_upline` = c.`id_member`

		LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member ='$id_member' ";

		$data['get'] = $this->crut->list_row($q_member);

		// list member kiri
		$this->data_statement_kiri($data['get']['downline_kiri']);
		$this->data_statement_kanan($data['get']['downline_kanan']);


		$total_kiri = count($this->list_member_statement_kiri);

		$total_kanan = count($this->list_member_statement_kanan);

		$input_bonus_pasangan['id_member'] = $id_member;
		$input_bonus_pasangan['t_kiri'] = $total_kiri;
		$input_bonus_pasangan['t_kanan'] = $total_kanan;

		// cek bons di tgl sekarang
		$q_bon = "select * from bonus_pasangan_2 where id_member ='".$id_member."' and tgl='".date('Y-m-d')."' ;";
		$d_bon = $this->crut->list_row($q_bon);
		// cek bons di tgl sekarang

		/// cek nilai bonus pasangan sebelumnya
		$t_pas = 0;
		// $q ="SELECT SUM(t_pasangan) AS total_pasangan FROM bonus_pasangan_2 WHERE id_member ='".$id_member."' AND status_approve ='1' GROUP BY id_member;";
		$q = "SELECT SUM(t_pasangan) AS total_pasangan FROM bonus_pasangan_2 WHERE id_member ='".$id_member."' AND tgl !='".date('Y-m-d')."'
GROUP BY id_member;";
		$d_q = $this->crut->list_row($q); // total pasangan hari sebelumnya

		if($d_q !=0){

			$t_pas = $d_q['total_pasangan'];
		}
		/// cek nilai bonus pasangan sebelumnya
		if($t_pasangan !=""){
			$total_kiri = $t_pasangan;
		}

		if($total_kiri < $total_kanan){
			$input_bonus_pasangan['t_pasangan'] = ($total_kiri - $t_pas);
		}
		if($total_kiri > $total_kanan){
			$input_bonus_pasangan['t_pasangan'] = ($total_kanan - $t_pas);
		}
		if($total_kiri == $total_kanan){
			$input_bonus_pasangan['t_pasangan'] = ($total_kanan - $t_pas);
		}
		if($input_bonus_pasangan['t_pasangan'] < 13){
			$input_bonus_pasangan['komisi'] = $this->bonus_pasangan * $input_bonus_pasangan['t_pasangan'];
		}
		if($input_bonus_pasangan['t_pasangan'] > 12){
			$input_bonus_pasangan['komisi'] = $this->bonus_pasangan * 12;
		}


		$input_bonus_pasangan['tgl'] = date('Y-m-d');

		// echo '<pre>';
		// print_r($input_bonus_pasangan);
		// die();
		if($d_bon == 0){
			$input_bonus_pasangan['status_approve'] = 0;
			$input_bonus_pasangan['created_date'] = date('Y-m-d H:i:S');
			$input_bonus_pasangan['created_by'] = $this->session->userdata('user_id');
			if($input_bonus_pasangan['t_pasangan'] > 0){

				$this->crut->insert('bonus_pasangan_2',$input_bonus_pasangan,'','','');

				//insert log
				$dt = array('input'=>$input_bonus_pasangan);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'BonusPasangan',
					'jenis_operasi'=>'Insert',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log

			}

		}else{
			// $input_bonus_pasangan['status_approve'] = 0;
			$input_bonus_pasangan['updated_date'] = date('Y-m-d H:i:S');
			$input_bonus_pasangan['updated_by'] = $this->session->userdata('user_id');
			$filter = array('id'=>$d_bon['id']);
			if($input_bonus_pasangan['t_pasangan'] > 0){
				$this->crut->update($filter,'bonus_pasangan_2',$input_bonus_pasangan,'','','');

				//insert log
				$dt = array('filter'=>$filter,'input'=>$input_bonus_pasangan);
				$insert_log = array(
					'kode_log'=>microtime(),
					'halaman'=>'BonusPasangan',
					'jenis_operasi'=>'Update',
					'data'=>json_encode($dt),
					'ip_user'=>$_SERVER['REMOTE_ADDR'],
					'user_id'=>$this->session->userdata('user_id'),
					'date_created'=>date('Y-m-d H:i:s')
				);
				$this->crut->insert('log_aktivitas',$insert_log,'','','');
				//insert log
			}
		}

		if($data['get']['id_upline'] !=""){
			$this->input_bonus_pasangan_2($data['get']['id_upline']);
		}



	}

public function data_statement_kiri($id_member,$level = 1){

		 if($level > 1){ /// proses level 1

				$level = $level + 1;

		 }

		 $d_detail_2 = array();
		 $d_detail_3 = array();

			$q_detail = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

												DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

												c.`nama_lengkap` AS nama_upline FROM member_master AS a

												INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

												INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

												LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$id_member."' ";

			$d_detail = $this->crut->list_row($q_detail);

			if($d_detail != 0){



								$this->list_member_statement_kiri[$id_member] = $d_detail;

									$this->list_member_statement_kiri[$d_detail['id_member']]['level'] = $level;



									if($d_detail['downline_kiri'] !="" || $d_detail['downline_kanan'] !=""){

										$level = $level + 1;

										/// cek downline kiri

										if($d_detail['downline_kiri'] !=""){

											$q_detail_2 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																				DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																				c.`nama_lengkap` AS nama_upline FROM member_master AS a

																				INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																				INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																				LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kiri']."' ";

											$d_detail_2 = $this->crut->list_row($q_detail_2);

											if($d_detail_2 != 0){



												$this->list_member_statement_kiri[$d_detail_2['id_member']] = $d_detail_2;

													$this->list_member_statement_kiri[$d_detail_2['id_member']]['level'] = $level;

											}

										}

										/// cek downline kiri

										/// cek downline kanan

										if($d_detail['downline_kanan'] !=""){

											// echo $d_detail['id_member'].' '.$d_detail['downline_kanan'].'<br>';

											$q_detail_3 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																				DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																				c.`nama_lengkap` AS nama_upline FROM member_master AS a

																				INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																				INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																				LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kanan']."' ";

											$d_detail_3 = $this->crut->list_row($q_detail_3);

											if($d_detail_3 !=0){



												$this->list_member_statement_kiri[$d_detail_3['id_member']] = $d_detail_3;

													$this->list_member_statement_kiri[$d_detail_3['id_member']]['level'] = $level;

											}

										}

										/// cek downline kanan

										/// cek downline level 2

										if($d_detail_2['downline_kiri'] !=""){

											$this->data_statement_kiri($d_detail_2['downline_kiri'],$level);

										}

										if($d_detail_2['downline_kanan'] !=""){

											$this->data_statement_kiri($d_detail_2['downline_kanan'],$level);

										}



										if(array_key_exists('downline_kiri',$d_detail_3) && $d_detail_3['downline_kiri'] !=""){

											$this->data_statement_kiri($d_detail_3['downline_kiri'],$level);

										}

										if(array_key_exists('downline_kanan',$d_detail_3) && $d_detail_3['downline_kanan'] !=""){

											$this->data_statement_kiri($d_detail_3['downline_kanan'],$level);

										}

										/// cek downline level 2

									}

			}





	}



	public function data_statement_kanan($id_member,$level = 1){

		if($level > 1){ /// proses level 1

			 $level = $level + 1;

		}

		$d_detail_2 = array();
		$d_detail_3 = array();

		 $q_detail = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

											 DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

											 c.`nama_lengkap` AS nama_upline FROM member_master AS a

											 INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

											 INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

											 LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$id_member."' ";

		 $d_detail = $this->crut->list_row($q_detail);

		 if($d_detail != 0){



							 $this->list_member_statement_kanan[$id_member] = $d_detail;

								 $this->list_member_statement_kanan[$d_detail['id_member']]['level'] = $level;



								 if($d_detail['downline_kiri'] !="" || $d_detail['downline_kanan'] !=""){

									 $level = $level + 1;

									 /// cek downline kiri

									 if($d_detail['downline_kiri'] !=""){

										 $q_detail_2 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																			 DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																			 c.`nama_lengkap` AS nama_upline FROM member_master AS a

																			 INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																			 INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																			 LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kiri']."' ";

										 $d_detail_2 = $this->crut->list_row($q_detail_2);

										 if($d_detail_2 != 0){



											 $this->list_member_statement_kanan[$d_detail_2['id_member']] = $d_detail_2;

												 $this->list_member_statement_kanan[$d_detail_2['id_member']]['level'] = $level;

										 }

									 }

									 /// cek downline kiri

									 /// cek downline kanan

									 if($d_detail['downline_kanan'] !=""){

										 // echo $d_detail['id_member'].' '.$d_detail['downline_kanan'].'<br>';

										 $q_detail_3 = "SELECT a.id, a.id_member,a.nama_lengkap,a.image,a.downline_kiri,a.downline_kanan, d.`type`, d.`kota`,

																			 DATE_FORMAT(a.`created_date`,'%d/%m/%Y') AS tgl, a.`id_sponsor`, b.`nama_lengkap` AS nama_sponsor, a.`id_upline`,

																			 c.`nama_lengkap` AS nama_upline FROM member_master AS a

																			 INNER JOIN member_master AS b ON a.`id_sponsor` = b.`id_member`

																			 INNER JOIN member_master AS c ON a.`id_upline` = c.`id_member`

																			 LEFT JOIN master_kecamatan AS d ON a.`id_kec` = d.`id` where a.id_member = '".$d_detail['downline_kanan']."' ";

										 $d_detail_3 = $this->crut->list_row($q_detail_3);

										 if($d_detail_3 !=0){



											 $this->list_member_statement_kanan[$d_detail_3['id_member']] = $d_detail_3;

												 $this->list_member_statement_kanan[$d_detail_3['id_member']]['level'] = $level;

										 }

									 }

									 /// cek downline kanan

									 /// cek downline level 2

									 if($d_detail_2['downline_kiri'] !=""){

										 $this->data_statement_kanan($d_detail_2['downline_kiri'],$level);

									 }

									 if($d_detail_2['downline_kanan'] !=""){

										 $this->data_statement_kanan($d_detail_2['downline_kanan'],$level);

									 }



									 if(array_key_exists('downline_kiri',$d_detail_3) && $d_detail_3['downline_kiri'] !=""){

										 $this->data_statement_kanan($d_detail_3['downline_kiri'],$level);

									 }

									 if(array_key_exists('downline_kiri',$d_detail_3) && $d_detail_3['downline_kanan'] !=""){

										 $this->data_statement_kanan($d_detail_3['downline_kanan'],$level);

									 }

									 /// cek downline level 2

								 }

		 }





 }

  public function autocomplete_sponsor($q =''){
			$q = $this->input->get('term');
			$q_auto = "select id_member,nama_lengkap from member_master where CONCAT(id_member,nama_lengkap) like'%".$q."%' ";
			$data_option = $this->crut->list_datas($q_auto);
			$json = array();
			foreach($data_option as $k => $v){
				$json[] = array('label'=>$v['id_member'].' | '.$v['nama_lengkap'],'value'=>$v['id_member'].' | '.$v['nama_lengkap']);
			}
			// echo '<pre>';
			// print_r($json);
			$json = json_encode($json);
			echo $json;
		}
	public function autocomplete_upline($q =''){
			$q = $this->input->get('term');
			$q_auto = "select id_member,nama_lengkap from member_master where CONCAT(id_member,nama_lengkap) like'%".$q."%' and downline_kiri is null or downline_kanan is null";
			$data_option = $this->crut->list_datas($q_auto);
			$json = array();
			foreach($data_option as $k => $v){
				$json[] = array('label'=>$v['id_member'].' | '.$v['nama_lengkap'],'value'=>$v['id_member'].' | '.$v['nama_lengkap']);
			}
			// echo '<pre>';
			// print_r($json);
			$json = json_encode($json);
			echo $json;
		}
	public function kota_html(){
      $prop_payment = $this->input->get('propinsi_origin',true);
      $kota_payment = $this->input->get('kota_origin',true);
      $q_kota_payment = "select * from master_kota where provinsi_id ='".$prop_payment."'";
      $d_kota_payment = $this->crut->list_datas($q_kota_payment);
      if($d_kota_payment == 0){
        echo '<option value="">Kota Tidak Ditemukan</option>';
      }
      if($d_kota_payment != 0){
        foreach ($d_kota_payment as $k => $v) {
          $pilih ='';
          if($v['id'] == $kota_payment){
            $pilih ='selected';
          }
          echo '<option value="'.$v['id'].'" '.$pilih.' >'.$v['type'].' '.$v['kota'].'</option>';
        }
      }
    }

    public function kecamatan_html(){
      $prop_payment = $this->input->get('propinsi_origin',true);
      $kota_payment = $this->input->get('kota_origin',true);
      $kecamatan_payment = $this->input->get('kecamatan_origin',true);
      $q_kecamatan_payment = "select * from master_kecamatan where provinsi_id ='".$prop_payment."' and kota_id ='".$kota_payment."'";
      $d_kecamatan_payment = $this->crut->list_datas($q_kecamatan_payment);
      if($d_kecamatan_payment == 0){
        echo '<option value="">Kecamatan Tidak Ditemukan</option>';
      }
      if($d_kecamatan_payment != 0){
        foreach ($d_kecamatan_payment as $k => $v) {
          $pilih ='';
          if($v['id'] == $kecamatan_payment){
            $pilih ='selected';
          }
          echo '<option value="'.$v['id'].'" '.$pilih.' >'.$v['type'].' '.$v['kecamatan'].'</option>';
        }
      }
    }

}
