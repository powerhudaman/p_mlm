<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Menu {

	public function Menu($parameter ='3',$themes_name ='eshopper',$position='')
	{
        $CA =get_instance();
        $CA->load->model('Mdl_crud','crut');
				$theme_terpilih = $CA->crut->setting('global_configuration','themes','setting')['value'];
				if($themes_name == $theme_terpilih){
					$menu_items = array();

	        $q_menu= "select * from menu_type where id ='".$parameter."'";
	        $menu_type = $CA->crut->list_row($q_menu);
	        $menu_type = $menu_type['menu_type'];

	        // $q_menu_item = "select * from menu where menu_type ='".$menu_type."' and level ='1' and publish='1' ";
					$q_menu_item ="SELECT a.*,b.route FROM menu AS a LEFT JOIN layout_route AS b ON a.id = b.menu_id WHERE a.menu_type = '".$menu_type."' AND a.publish = '1' and a.level ='1'  group by a.id order by a.ordering ASC";
					// die();
	        $menu_level_1 = $CA->crut->list_datas($q_menu_item);
					$group = $CA->crut->setting('global_configuration','customer_group','setting')['value'];

					if($CA->session->has_userdata('member_group')){
						$group = $CA->session->userdata('member_group'); // digunakan untuk mengecek group permission
					}

					$login = 0;
					if($CA->session->has_userdata('member_id')){
						$login = $CA->session->userdata('member_id');
					}

	        $json_permission = array();

	        // var_dump($menu_level_1);
	        if($menu_level_1 > 0){
	                foreach($menu_level_1 as $k =>$v){
	                    // cek menu punya permission atau tidak
	                  if($login == 0){
													$permission = json_decode($v['permission'],true);

	                        // bila permission kosong maka fungsi ini akan di jalankan
	                        // cek apakah punya submenu atau tidak
	                        $q_cek_sub = "select a.*,b.route from menu as a LEFT JOIN layout_route AS b ON a.id = b.menu_id where a.parent='".$v['id']."' and a.level ='2' and a.publish='1' group by a.id";
													// echo $q_cek_sub;
													// die();

													if($permission[$group] == 2){
														// if($v['route'] !=""){
														// 	$v['link'] = $v['route'];
														// }
														if($v['parameter'] !="" && $v['parameter'] !='null'){
															$icon = json_decode($v['parameter'],true);
			                        $menu_items[$k] = array('nama'=>$v['nama'],'alias'=>$v['alias'],'link'=>$v['link']);
															if(array_key_exists('icons',$icon)){
																$menu_items[$k]['icon'] = $icon['icons'];
															}
														}

													}
	                        if($CA->crut->list_count($q_cek_sub) > 0){
	                            $menu_level_2 = $CA->crut->list_datas($q_cek_sub);
	                            foreach($menu_level_2 as $sub => $c){
	                                // pengecekan permission pada sub menu

	                                /*if(empty($c['permission'])){
	                                    // bila permission tidak ada maka fungsi ini akan dilakukan
																			if($c['route'] !=""){
																				$c['link'] = $c['route'];
																			}
	                                    $menu_items[$k]['subs'][] = array('nama'=>$c['nama'],'alias'=>$c['alias'],'link'=>$c['link']);

	                                }*/
	                                if(!empty($c['permission'])){
	                                    //variabel digunakan untuk pengekcekan hak akses halaman
	                                    $json_permission[$menu_type][] = array($c['link']=>$c['permission']);

	                                    // bila ada permission maka ini akan di lakukan
	                                    $permission = json_decode($c['permission'],true);
	                                    if(array_key_exists($group, $permission)){
	                                        // jika group mempunyai akses maka ini akan dijalankan
	                                        // cek apakah punya submenu atau tidak
	                                        if($permission[$group] == 2){
																							if($c['route'] !=""){
																								$c['link'] = $c['route'];
																							}
	                                            $menu_items[$k]['subs'][] = array('nama'=>$c['nama'],'alias'=>$c['alias'],'link'=>$c['link']);

	                                        }
	                                    }
	                                }
	                            }
	                        }
	                    }

	                    if($login > 0){
	                        //variabel digunakan untuk pengekcekan hak akses halaman
	                        $json_permission[$menu_type][] = array($v['link']=>$v['permission']);

	                        //bila permission tidak kosong maka akan di lakukan pengecekan permission disini
	                        $permission = json_decode($v['permission'],true);

	                        if(array_key_exists($group, $permission)){
	                            if($permission[$group] !=0 && $v['status_menu_login'] == 1){
	                                // jika group mempunyai akses maka ini akan dijalankan
	                                // cek apakah punya submenu atau tidak
	                                // if($login == 0 ||)
	                                $q_cek_sub = "select a.*,b.route from menu as a LEFT JOIN layout_route AS b ON a.id = b.menu_id where a.parent='".$v['id']."' and a.level ='2' and a.publish='1' group by a.id";
	                                $menu_items[$k] = array('nama'=>$v['nama'],'alias'=>$v['alias'],'link'=>$v['link']);
	                                if($CA->crut->list_count($q_cek_sub) > 0){
	                                    $menu_level_2 = $CA->crut->list_datas($q_cek_sub);
	                                    foreach($menu_level_2 as $sub => $c){
	                                        // pengecekan permission pada sub menu
	                                        /*if(empty($c['permission'])){
	                                            // bila permission tidak ada maka fungsi ini akan dilakukan
																							if($c['route'] !=""){
																								$c['link'] = $c['route'];
																							}
	                                            $menu_items[$k]['subs'][] = array('nama'=>$c['nama'],'alias'=>$c['alias'],'link'=>$c['link']);

	                                        }*/
	                                        if($c['status_menu_login'] == 1){
	                                            //variabel digunakan untuk pengekcekan hak akses halaman
	                                            $json_permission[$menu_type][] = array($c['link']=>$c['permission']);

	                                            // bila ada permission maka ini akan di lakukan
	                                            $permission = json_decode($c['permission'],true);
	                                            if(array_key_exists($group, $permission)){
	                                                // jika group mempunyai akses maka ini akan dijalankan
	                                                // cek apakah punya submenu atau tidak
	                                                if($permission[$group] !=0){
																											if($c['route'] !=""){
																												$c['link'] = $c['route'];
																											}
	                                                    $menu_items[$k]['subs'][] = array('nama'=>$c['nama'],'alias'=>$c['alias'],'link'=>$c['link']);

	                                                }
	                                            }
	                                        }
	                                    }
	                                }
	                            }
	                        }

	                    }


	                }
	        }


	        $themes_name = $themes_name;
	        $data['menu'] = $CA->crut->list_row($q_menu);
	        $data['menu_items'] = $menu_items;
	        $data['posisi'] = $position;
	        $alamat_view = $themes_name.'/mod_menu/mod_menu.tpl';
	        $view = $CA->parser->parse($alamat_view,$data,true,$themes_name);

	        // echo '<pre>';
	        // print_r($menu_items);
	        // echo'</pre><br>';
					// die();
	        // echo $view;
	        return $view;
				}

	}

}

/* End of file Mod_menu.php */
/* Location: ./application/modules/mod_menu/controllers/Mod_menu.php */
