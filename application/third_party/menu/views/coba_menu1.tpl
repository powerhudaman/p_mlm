

<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Game Gear
										</a>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">PS3 </a></li>
											<li><a href="#">PS4 </a></li>
											<li><a href="#">Xbox 360 </a></li>
											<li><a href="#">Xbox one</a></li>
											<li><a href="#">PC </a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#mens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Games
										</a>
									</h4>
								</div>
								<div id="mens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">Adventure</a></li>
											<li><a href="#">Action</a></li>
											<li><a href="#">Car</a></li>
											<li><a href="#">Racing</a></li>
											<li><a href="#">FPS</a></li>
											<li><a href="#">Shooter</a></li>
											<li><a href="#">Simulation</a></li>
											<li><a href="#">Card Game</a></li>
											<li><a href="#">Vr</a></li>
	
										</ul>
									</div>
								</div>
							</div>
						</div><!--/category-products-->