<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-05-18 16:46:53
         compiled from "C:\xampp\htdocs\p_mlm\application\views\cbm\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:78105ec29fdd739375-56776258%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6df1b5e4ea89f9dbcf7a0a3db0877506c5bce955' => 
    array (
      0 => 'C:\\xampp\\htdocs\\p_mlm\\application\\views\\cbm\\login.tpl',
      1 => 1589527838,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '78105ec29fdd739375-56776258',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'this' => 0,
    'cfg_login' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ec29fdd7ac7c4_24829959',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ec29fdd7ac7c4_24829959')) {function content_5ec29fdd7ac7c4_24829959($_smarty_tpl) {?><!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
  <meta name="author" content="Coderthemes">

  <link rel="shortcut icon" href="assets/images/favicon_1.ico">

  <title><?php echo $_smarty_tpl->tpl_vars['this']->value->site_name;?>
</title>

  <link href="<?php echo theme_url('assets/css/bootstrap.min.css');?>
" rel="stylesheet" type="text/css" />
  <link href="<?php echo theme_url('assets/css/core.css');?>
" rel="stylesheet" type="text/css" />
  <link href="<?php echo theme_url('assets/css/components.css');?>
" rel="stylesheet" type="text/css" />
  <link href="<?php echo theme_url('assets/css/icons.css');?>
" rel="stylesheet" type="text/css" />
  <link href="<?php echo theme_url('assets/css/pages.css');?>
" rel="stylesheet" type="text/css" />
  <link href="<?php echo theme_url('assets/css/responsive.css');?>
" rel="stylesheet" type="text/css" />

  <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"><?php echo '</script'; ?>
>
        <![endif]-->

  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/modernizr.min.js');?>
"><?php echo '</script'; ?>
>

</head>

<body>

  <div class="account-pages"></div>
  <div class="clearfix"></div>
  <div class="wrapper-page">
    <div class=" card-box">
      <div class="panel-heading">
        <h3 class="text-center"><strong class="text-custom"><?php echo $_smarty_tpl->tpl_vars['this']->value->site_name;?>
</strong> </h3>
      </div>


      <div class="panel-body">
          <?php $_smarty_tpl->tpl_vars['cfg_login'] = new Smarty_variable(array('id'=>'form_login','class'=>'form-horizontal m-t-20'), null, 0);?>
          <?php echo form_open(site_url('cbm/login/login_proses'),$_smarty_tpl->tpl_vars['cfg_login']->value);?>

          <div class="form-group">
            <?php if ($_smarty_tpl->tpl_vars['this']->value->session->flashdata('pesan')!='') {?>
              <?php echo $_smarty_tpl->tpl_vars['this']->value->session->flashdata('pesan');?>

            <?php }?>
          </div>
          <div class="form-group ">
            <div class="col-xs-12">
              <input class="form-control" type="text" name="id_member" required placeholder="ID Member">
            </div>
          </div>

          <div class="form-group">
            <div class="col-xs-12">
              <input class="form-control" type="password" name="pin" required placeholder="Pin">
            </div>
          </div>

          <div class="form-group text-center m-t-40">
            <div class="col-xs-12">
              <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">Login</button>
            </div>
          </div>

        <?php echo form_close();?>


      </div>
    </div>

  </div>




  <?php echo '<script'; ?>
>
    var resizefunc = [];
  <?php echo '</script'; ?>
>

  <!-- jQuery  -->
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/jquery.min.js');?>
"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/bootstrap.min.js');?>
"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/detect.js');?>
"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/fastclick.js');?>
"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/jquery.slimscroll.js');?>
"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/jquery.blockUI.js');?>
"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/waves.js');?>
"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/wow.min.js');?>
"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/jquery.nicescroll.js');?>
"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/jquery.scrollTo.min.js');?>
"><?php echo '</script'; ?>
>


  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/jquery.core.js');?>
"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo theme_url('assets/js/jquery.app.js');?>
"><?php echo '</script'; ?>
>

</body>

</html>
<?php }} ?>
