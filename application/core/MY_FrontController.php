<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_FrontController extends CI_Controller {

	protected $Routes;
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->Routes = $this->uri->segment(1).'/'.$this->uri->segment(2);
		echo $this->Routes;
		die();
	}

}

/* End of file MY_Controll.php */
/* Location: ./application/core/MY_Controll.php */