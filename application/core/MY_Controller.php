<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	// inisialisasi
	public $pos_1;
    public $pos_2;
    public $pos_3;
    public $pos_4;
    public $pos_5;
    public $pos_6;
    public $pos_7;
    public $pos_8;
    public $pos_9;
    public $pos_10;
    public $pos_11;
		public $pos_12;
		public $pos_13;
		public $pos_14;
		public $pos_15;
    public $pos_16;
		public $cadangan;

		public $site_name;
    public $site_url;
    public $themes_name;
    public $site_phone;


    public $site_status;
    public $pesan_maintenance;
    public $site_date;

		public $total_akses;
		public $member_group;
		public $member_group_akses;

		public $component_akses_member; // variabel ini digunakan untuk melakukan listing daftar component yang bisadi akses oleh member
	// protected $themes_name ='eshopper';

	public $notif_member = array();
	public $t_new_notif = 0;


	public function __construct()
	{
		parent::__construct();
				$this->load->helper('file_manipulation');
				delete_all_files(APPPATH.'/cache/smarty/compiled/*');
        // template
        $themes_name = $this->crut->setting('global_configuration','themes','setting');
        $this->themes_name = $themes_name['value'];

        // site name
        $this->site_name = $this->crut->setting('global_configuration','site_name','setting')['value'];
				$this->site_phone = $this->crut->setting('global_configuration','site_phone','setting')['value'];
        $this->site_url = $this->crut->setting('global_configuration','url_frontend','setting')['value'];

        // site_maintenance
        $this->site_status = $this->crut->setting('global_configuration','site_status','setting')['value'];
        $this->pesan_maintenance = $this->crut->setting('global_configuration','pesan_maintenance','setting')['value'];
        $this->site_date = $this->crut->setting('global_configuration','site_date','setting')['value'];

				$this->total_akses = 0;
				$this->member_group = '';
				$this->member_group_akses = array();
				$this->component_akses_member = array();
				if($this->session->has_userdata('login_member')){
					$this->total_akses = $this->session->userdata('login_member')['total_akses'];
					$this->member_group = $this->session->userdata('login_member')['member'];
					$this->member_group_akses = $this->session->userdata('login_member')['grant_akses']['component'];

					$q_component = "select * from component_akses where front_status = '1' and status_delete ='0'";
					$d_component = $this->crut->list_datas($q_component);
					$this->component_akses_member = $d_component;
					$this->notifikasi_member();
					if(count($this->notif_member) > 0){
						$no = 0;
						foreach ($this->notif_member as $k => $v) {
							if($v['status_notif'] == 0){
								$no++;
							}
						}
						$this->t_new_notif = $no;
					}

				}

				// deteksi route
				$route = $this->uri->segment(1).'/'.$this->uri->segment(2);
				$segs = $this->uri->segment_array();
				if(count($segs) == 0){
					$q_route = "select * from layout_route where route like '%".$route."%';";
					$layout = $this->crut->list_row($q_route);


				}
				if(count($segs) > 0){
						$hit_route = 0;
						$layout = 0;
						foreach ($segs as $key => $value) {
							# code...
							$hit_route++;
							if($hit_route == 1){
								$route = $value;
							}
							if($hit_route > 1){
								$route .='/'.$value;
							}
							$q_route = "select * from layout_route where route='".$route."'";
							$layout = $this->crut->list_row($q_route);
							if($layout !="0" && is_array($layout)){
								// echo 'selesai '.$q_route;
								break;
							}
							if($layout == 0){
								$q_route = "select a.* from layout_route as a
		 						inner join layout as b on a.layout_id = b.layout_id where b.`primary` ='1';";
		 						$layout = $this->crut->list_row($q_route);
							}

							// echo $hit_route.' '.$route.' </br>';
						}
				}

				 // layout
        $layout_id = $layout['layout_id'];
				// echo $themes_name['value'].'<br>';
        // list modul
        $q_modul_code = "select code,sort_order from layout_module where layout_id='".$layout_id."' group by code order by sort_order ASC";
				// echo $q_modul_code;

        $list_modul_code = $this->crut->list_datas($q_modul_code);
        if($this->crut->list_count($q_modul_code) > 0){

        	foreach($list_modul_code as $k =>$v){
	            $this->load->controller($v['code'],$v['code']);
	        }

	        $q_moduls = "select * from layout_module where layout_id ='".$layout_id."' order by sort_order ASC";
					// echo $q_moduls.'<br>';
					// die();
	        $list_moduls = $this->crut->list_datas($q_moduls);
	        foreach($list_moduls as $k => $modul){
							$mod = '';
							// echo $modul['code'].'-'.$modul['parameter'].'<br>';

	            // $this->load->controller($modul['code'],$modul['code']);
	            $mod = $this->$modul['code']->$modul['code']($modul['parameter'],$this->themes_name,$modul['position'],$modul['size_block']);
	            switch ($modul['position']) {
	                case 'pos-1':
	                    $this->pos_1.=$mod;
	                break;
	                case 'pos-2':
	                    $this->pos_2.=$mod;
	                break;
	                case 'pos-3':
	                    $this->pos_3.=$mod;
	                break;
	                case 'pos-4':
	                    $this->pos_4.=$mod;
	                break;
	                case 'pos-5':
	                    $this->pos_5.=$mod;
	                break;
	                case 'pos-6':
	                    $this->pos_6.=$mod;
	                break;
	                case 'pos-7':
	                    $this->pos_7.=$mod;
	                break;
	                case 'pos-8':
	                    $this->pos_8.=$mod;
	                break;
	                case 'pos-9':
	                    $this->pos_9.=$mod;
	                break;
	                case 'pos-10':
	                    $this->pos_10.=$mod;
	                break;
	                case 'pos-11':
	                    $this->pos_11.=$mod;
						break;
	                case 'pos-12':
	                    $this->pos_12.=$mod;
											break;

									case 'pos-13':
	                    $this->pos_13.=$mod;
											break;

									case 'pos-14':
	                    $this->pos_14.=$mod;
											break;

									case 'pos-15':
	                    $this->pos_15.=$mod;
											break;

									case 'pos-16':
	                    $this->pos_16.=$mod;
	                break;

	            }
	        }
					// var_dump($this->pos_1);
					/*var_dump($this->pos_2);
					var_dump($this->pos_3);
					var_dump($this->pos_4);
					var_dump($this->pos_5);
					var_dump($this->pos_6);
					var_dump($this->pos_7);
					var_dump($this->pos_8);
					var_dump($this->pos_9);
					var_dump($this->pos_10);
					var_dump($this->pos_11);
					var_dump($this->pos_12);*/



        } // end if layout module > 0
        $this->cek_cookie();
				// die();

	}


	public function maintenance(){
        $this->parser->parse(FCPATH.'assets/maintenance/maintenance.tpl');
        die();
    }


    public function cek_cookie(){
    	$this->load->helper('cookie');
    	$cookie_status = $this->input->cookie('remember_me');
    	$token = $this->input->cookie('remember_me_token');
    	if($cookie_status){
    		$q = "select id, customer_group_id,email,nama_depan,nama_belakang from customer where salt ='".$token."' and status ='2'";
    		$hitung = $this->crut->list_count($q);
    		if($hitung > 0){
    			$response = $this->crut->list_row($q);
    			 $sessi = array(
    			 		'member_id' => $response['id'],
			    		'member_fullname'=>$response['nama_depan'].' '.$response['nama_belakang'],
			    		'member_email' => $response['email'],
			    		'member_group' => $response['customer_group_id'],
			    		'member_logged' => true
			    	);

			    $this->session->set_userdata($sessi);
    		}
    	}
    }

    public function cek_login(){
    	$kode = $this->session->userdata('member_id');
    	$status = $this->session->userdata('member_logged');
    	$email = $this->session->userdata('member_email');

    	if(!$status && empty($email)){
    		redirect(site_url('smartytest/index'));
    	}

    }

		protected function notifikasi_member(){
			$id_member = $this->session->userdata('login_member')['id_member'];
			$q_notif = "select * from notifikasi_member where id_member ='".$id_member."' order by created_date DESC";
			// echo $q_notif;
			$d_notif = $this->crut->list_datas($q_notif);
			if($d_notif !=0){
				$this->notif_member = $d_notif;
			}
		}

		public function cek_signin(){
			if(!$this->session->has_userdata('login_member')){
				$pesan = array('status'=>false,'pesan'=>'Anda Belum Login Silahkan Login Terlebih Dahulu','kode'=>2);
        $this->session->set_flashdata('pesan',pesan($pesan));
				redirect(site_url('home'));

			}
		}

    public function cek_akses_halaman(){
    	$adress = $this->uri->segment(1).'/'.$this->uri->segment('2');

    	$q_adress = "select * from menu where link ='".$adress."'";
    	$data = $this->crut->list_datas($q_adress);
    	$hitung_akses = 0;
    	$group = $this->session->userdata('member_group'); // digunakan untuk mengecek group permission
        if($data > 0){
            foreach ($data as $key => $value) {
                if($value['permission'] == ""){
                    $hitung_akses++;
                }
                if($value['permission'] !=""){
                    $permision = json_decode($value['permission'],true);
                    if(array_key_exists($group, $permision)){
                        if($permision[$group] == 1){
                            $hitung_akses++;
                        }
                    }
                }

            }
        }
        if($data == 0){
             $hitung_akses++;
        }

    	if($hitung_akses == 0){
    		redirect(site_url('smartytest/index'));
    	}
    }

		/**
		 * [cek_hak_akses fungsi ini digunakan untuk malakukan pengecekan terhadap hak akses apabila rolenya tidak sesuai maka akan di redirect ke dashboard]
		 * @param  [type] $param [kunci akses]
		 * @param  string $role  [aturan akses]
		 * @return [type]        [description]
		 */
		public function cek_hak_akses($param,$role = ''){
			$hitung = $param;
			if($hitung == 0){
				$pesan = array('status'=>false,'pesan'=>'Anda Tidak Memilik Hak akses ','kode'=>2);
				$this->session->set_flashdata('pesan', 'value');
				redirect(site_url('dashboard'));
			}
			if(!empty($role)){
				$pecah_roler = explode('|', $role.'|');
				foreach($pecah_roler as $k => $v){
					if(!empty($v)){
						if($hitung >= $v){
							break;
						}else{
							$pesan = array('status'=>false,'pesan'=>'Anda Tidak Memilik Hak akses ',kode=>2);
							$this->session->set_flashdata('pesan', 'value');
							redirect(site_url('dashboard'));
						}
					}
				}
			}
		}

}

/* End of file MY_Controll.php */
/* Location: ./application/core/MY_Controll.php */
