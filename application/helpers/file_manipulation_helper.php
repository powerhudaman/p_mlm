<?php

	/**
	 * [copy_folder digunakan untuk melakukan copy folder]
	 * @param  string $src [sumber folder]
	 * @param  string $dst [tujuan folder]
	 * @return [type]      [description]
	 */
	function copy_folder($src='',$dst='') {
		$dir = opendir($src);
		@mkdir($dst);
		while(false !== ( $file = readdir($dir)) ) {
			if (( $file != '.' ) && ( $file != '..' )) {
				if ( is_dir($src . '/' . $file) ) {
				recurse_copy($src . '/' . $file,$dst . '/' . $file);
				}
				else {
				copy($src . '/' . $file,$dst . '/' . $file);
				}
			}
		}
		closedir($dir);
	}

	/**
	 * [delete_folder Fungsi untuk delete file]
	 * @param  [type] $path [description]
	 * @return [type]       [description]
	 */
	function delete_folder($path)
	{
	    if (is_dir($path) === true)
	    {
	        $files = array_diff(scandir($path), array('.', '..'));

	        foreach ($files as $file)
	        {
	            delete_folder(realpath($path) . '/' . $file);
	        }

	        return rmdir($path);
	    }

	    else if (is_file($path) === true)
	    {
	        return unlink($path);
	    }

	    return false;
	}

	function delete_all_files($path){
		$files = glob($path, GLOB_BRACE); // get all file names
		foreach($files as $file){ // iterate files
		  if(is_file($file))
		    unlink($file); // delete file
		}
	}

?>
