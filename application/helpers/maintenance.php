<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->site_status =='1'){
            redirect(site_url('smartytest/index'));
        }
	}

	public function index()
	{
		$this->parser->parse(FCPATH.'assets/maintenance/maintenance.tpl');
	}

	public function registemail(){
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[notifikasi_maintenance.email]');
		if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {
			$input['email'] = $this->input->post('email',true);
			$sukses = 'Email Anda Sudah Kami Registrasikan Bila Ada Pemberitahuan Lebih Lanjut Akan Kami Informasikan ';
			$sukses = 'Email Anda Gagal Kami Registrasikan Silahkan Coba Daftar Lagi ';
			$pesan = $this->crut->insert('notifikasi_maintenance',$input,'Tambah Email Notifikasi',$sukses,$gagal);
			$this->session->set_flashdata('pesan', pesan($pesan));
			redirect(site_url('maintenance/index'));
		}
	}

}

/* End of file Maintenance.php */
/* Location: ./application/controllers/Maintenance.php */