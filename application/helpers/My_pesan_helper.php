<?php

	function pesan($data = array()){
		if($data['status'] && $data['kode'] == 1){
			$pesan = '<div class="alert alert-success alert-dismissable">
	                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                    '.$data['pesan'].'
	                  </div>';
		}
		if(!$data['status'] && $data['kode'] == 2){
			$pesan = '<div class="alert alert-danger alert-dismissable">
	                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                    '.$data['pesan'].'
	                  </div>';
		}


		return $pesan;
	}

	function genRndString($length = 10, $chars = '1234567890BCDFGHJKLMNPQRSTVWXYZ')
		{
		    if($length > 0)
		    {
		        $len_chars = (strlen($chars) - 1);
		        $the_chars = $chars{rand(0, $len_chars)};
		        for ($i = 1; $i < $length; $i = strlen($the_chars))
		        {
		            $r = $chars{rand(0, $len_chars)};
		            if ($r != $the_chars{$i - 1}) $the_chars .=  $r;
		        }

		        return $the_chars;
		    }
		}

		function genRndno($length = 10, $chars = '1234567890')
			{
			    if($length > 0)
			    {
			        $len_chars = (strlen($chars) - 1);
			        $the_chars = $chars{rand(0, $len_chars)};
			        for ($i = 1; $i < $length; $i = strlen($the_chars))
			        {
			            $r = $chars{rand(0, $len_chars)};
			            if ($r != $the_chars{$i - 1}) $the_chars .=  $r;
			        }

			        return $the_chars;
			    }
			}

			function sub_generator_kategori($data = array(),$total_child = 2){
				$output = array();
				$separator = '';
				for($i=0; $i < $total_child; $i++){
					if($i > 0){
						$separator.='-';
					}
					$level = $i + 1;
					foreach ($data as $key => $value) {
						if($value['level'] == $level){
								if($level == 1){
									$output[$value['id']] = array('id'=>$value['id'],'nama'=>$value['nama'],'level'=>$i,'subs'=>array());
								}
								if($level > 1){
									$output[$value['parent_id']]['subs'][] = array('id'=>$value['id'],'nama'=>$separator.' '.$value['nama'],'level'=>$i,'subs'=>array());
								}
						}
					}
				}
				return $output;
			}

			/**
			 * [generate_numbers fungsi untuk membuat no urut]
			 * @param  [type] $start  [description]
			 * @param  [type] $count  [description]
			 * @param  [type] $digits [description]
			 * @return [type]         [description]
			 */
			function generate_numbers($start, $count, $digits) {
			   $result = array();
			   for ($n = $start; $n < $start + $count; $n++) {

			      $result = str_pad($n, $digits, "0", STR_PAD_LEFT);

			   }
			   return $result;
			}

			function sms_zensiva($pesan = '',$telepon=''){
				$userkey = "nm3eok"; //userkey lihat di zenziva
				$passkey = "cbmmaju2017"; // set passkey di zenziva
				$message = $pesan;
				$url = "https://reguler.zenziva.net/apps/smsapi.php";
				$curlHandle = curl_init();
				curl_setopt($curlHandle, CURLOPT_URL, $url);
				curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($message));
				curl_setopt($curlHandle, CURLOPT_HEADER, 0);
				curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
				curl_setopt($curlHandle, CURLOPT_POST, 1);
				$results = curl_exec($curlHandle);
				curl_close($curlHandle);

				$XMLdata = new SimpleXMLElement($results);
				$status = $XMLdata->message[0]->text;
				return $status;
			}
