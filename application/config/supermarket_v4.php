<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['full_tag_open'] = '<div class="page-nav">';
$config['full_tag_close'] = '</div>';
$config['first_link'] = '';
$config['first_tag_open'] = '';
$config['first_tag_close'] = '';
$config['last_link'] = '';
$config['last_tag_open'] = '';
$config['last_tag_close'] = '';
$config['next_link'] = '&gt;';
$config['next_tag_open'] = '<div class="page-nav-item">';
$config['next_tag_close'] = '</div>';
$config['prev_link'] = '&lt;';
$config['prev_tag_open'] = '<div class="page-nav-item">';
$config['prev_tag_close'] = '</div>';
$config['cur_tag_open'] = '<div class="page-nav-item active"><a>';
$config['cur_tag_close'] = '</a></div>';
$config['num_tag_open'] = '<div class="page-nav-item">';
$config['num_tag_close'] = '</div>';
$config['reuse_query_string'] = true;
