<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_crud extends CI_Model {
	/**
	 * [insert description]
	 * @param  string $table  [Nama Tabel]
	 * @param  array  $data   [Data Input]
	 * @param  string $title  [Judul Modul]
	 * @param  string $sukses [pesan sukses]
	 * @param  string $gagal  [Pesan Berhasil]
	 * @return [type]         [array respon]
	 */
	public function insert($table='',$data=array(),$title='',$sukses='',$gagal=''){
		if($this->db->insert($table,$data)){
			$ids = $this->db->insert_id();
			$response = array('status'=>true,'pesan'=>$sukses,'kode'=>1,'ids'=>$ids);
		}else{
			$response = array('status'=>flse,'pesan'=>$gagal,'kode'=>2);
		}

		return $response;
	}

	/**
	 * [insert_batch description]
	 * @param  [type] $table  [Nama Tabel]
	 * @param  array  $input  [Data Yang akan di input]
	 * @param  [type] $title  [Judul]
	 * @param  string $sukses [Sukses]
	 * @param  string $gagal  [Gagal]
	 * @return [type]         [description]
	 */
	public function insert_batch($table,$input = array(),$title,$sukses='',$gagal=''){
		if($this->db->insert_batch($table,$input)){
			$respon = array("status"=>true,"pesan"=>$sukses,"kode"=>1,'ids'=> $this->db->insert_id());
		}else{
			$respon = array("status"=>false,"pesan"=>$gagal,"kode"=>2);

		}

		return $respon;
	}

	/**
	 * [update description]
	 * @param  array  $filter [filter data]
	 * @param  string $table  [tabel]
	 * @param  array  $data   [data yang akan di update]
	 * @param  string $title  [judul]
	 * @param  string $sukses [pesan sukses]
	 * @param  string $gagal  [pesan gagal]
	 * @return [type]         [return]
	 */
	public function update($filter=array(),$table='',$data=array(),$title='',$sukses='',$gagal=''){
		$this->db->where($filter);
		if($this->db->update($table,$data)){
			$response = array('status'=>true,'pesan'=>$sukses,'kode'=>1);
		}else{
			$response = array('status'=>flse,'pesan'=>$gagal,'kode'=>2);
		}

		return $response;
	}

	/**
	 * [list_count description]
	 * @param  [type] $q [query]
	 * @return [type]    [total data]
	 */
	public function list_count($q){
		$data = $this->db->query($q);
		if($data->num_rows() > 0){
			$data = $data->num_rows();
		}else{
			$data = 0;
		}
		return $data;
	}

	/**
	 * [list_count description]
	 * @param  [type] $q      [query]
	 * @param  string $limit  [limit]
	 * @param  string $offset [offset]
	 * @return [type]         [description]
	 */
	public function list_data($q,$limit='20',$offset='0'){
		$q.=' limit '.$limit.' offset '.$offset;
		$data = $this->db->query($q);
		if($data->num_rows() > 0){
			$data = $data->result_array();
		}else{
			$data = array('pesan'=>'Data Tidak Ditemukan');
		}
		return $data;
	}

	public function list_datas($q){
		$data = $this->db->query($q);
		if($data->num_rows() > 0){
			$data = $data->result_array();
		}else{
			$data = 0;
		}
		return $data;
	}

	public function list_row($q){
		$data = $this->db->query($q);
		if($data->num_rows() > 0){
			$data = $data->row_array();
		}else{
			$data = 0;
		}
		return $data;
	}


	/**
	 * [list_count_binquery binding digunakan untuk meningkatakan keamana terhadap sql injection]
	 * @param  [type] $q         [description]
	 * @param  array  $parameter [description]
	 * @return [type]            [parameter untuk query binding]
	 */
	public function list_count_bin($q,$parameter = array()){
		$data = $this->db->query($q,$parameter);
		if($data->num_rows() > 0){
			$data = $data->num_rows();
		}else{
			$data = 0;
		}
		return $data;
	}

	/**
	 * [list_data_bin query binding untuk meningkatakan keamanan sql injection]
	 * @param  [type] $q         [description]
	 * @param  string $limit     [description]
	 * @param  string $offset    [description]
	 * @param  array  $parameter [description]
	 * @return [type]            [description]
	 */
	public function list_data_bin($q,$limit='20',$offset='0',$parameter = array()){
		$q.=' limit '.$limit.' offset '.$offset;
		$data = $this->db->query($q,$parameter);
		if($data->num_rows() > 0){
			$data = $data->result_array();
		}else{
			$data = array('pesan'=>'Data Tidak Ditemukan');
		}
		return $data;
	}

	/**
	 * [list_datas_bin query binding digunakan untuk meningkatkan kemanaman terhadap sql injection]
	 * @param  [type] $q         [description]
	 * @param  array  $parameter [description]
	 * @return [type]            [description]
	 */
	public function list_datas_bin($q,$parameter = array()){
		$data = $this->db->query($q,$parameter);
		if($data->num_rows() > 0){
			$data = $data->result_array();
		}else{
			$data = 0;
		}
		return $data;
	}

	/**
	 * [list_row_bin query binding digunakan untuk meningkatkan keamanan]
	 * @param  [type] $q         [description]
	 * @param  array  $parameter [description]
	 * @return [type]            [description]
	 */
	public function list_row_bin($q,$parameter = array()){
		$data = $this->db->query($q,$parameter);
		if($data->num_rows() > 0){
			$data = $data->row_array();
		}else{
			$data = 0;
		}
		return $data;
	}

	/**
	 * [detail description]
	 * @param  [type] $filter [filter data]
	 * @param  [type] $table  [tabel]
	 * @return [type]         [description]
	 */
	public function detail($filter,$table){
		$this->db->where($filter);
		$data = $this->db->get($table);
		if($data->num_rows() > 0){
			$data = $data->row_array();
		}else{
			$data = 0;
		}
		return $data;
	}

	/**
	 * [delete description]
	 * @param  [type] $filter [filter data]
	 * @param  [type] $table  [tabel]
	 * @param  [type] $title  [judul]
	 * @param  [type] $sukses [pesan sukses]
	 * @param  [type] $gagal  [pesan gagal]
	 * @return [type]         [description]
	 */
	public function delete($filter,$table,$title='',$sukses='',$gagal=''){
		$this->db->where($filter);
		if($this->db->delete($table)){
			$response = array('status'=>true,'pesan'=>$sukses,'kode'=>1);
		}else{
			$response = array('status'=>flse,'pesan'=>$gagal,'kode'=>2);
		}

		return $response;
	}

	public function get_products($q){
		$sess = $this->session->userdata('id');
		$filter = array('id_member'=>$sess,'status'=>1);
	    $this->db->where($filter);
	    $this->db->like('nama', $q);
	    $query = $this->db->get('produks');
	    if($query->num_rows() > 0){
	      foreach ($query->result_array() as $row){
	        $new_row['ids']=htmlentities(stripslashes($row['id']));
	        $new_row['value']=htmlentities(stripslashes($row['nama']));
	        $new_row['label']=htmlentities(stripslashes($row['nama']));
	        $new_row['harga_beli']=htmlentities(stripslashes($row['harga_beli']));
	        $new_row['harga']=htmlentities(stripslashes($row['harga']));
	        $new_row['berat']=htmlentities(stripslashes($row['berat']));
	        $row_set[] = $new_row; //build an array
	      }
	      echo json_encode($row_set); //format the array into json data
	    }
	}

	public function get_customers($q){
		$sess = $this->session->userdata('id');
		$filter = array('id_member'=>$sess,'status_delete'=>0);
	    $this->db->where($filter);
	    $this->db->like('no_hp', $q);
	    $this->db->or_like('nama', $q);
	    $query = $this->db->get('customers');
	    if($query->num_rows() > 0){
	      foreach ($query->result_array() as $row){
	        $new_row['ids']=htmlentities(stripslashes($row['id']));
	        $new_row['value']=htmlentities(stripslashes($row['nama']));
	        $new_row['label']=htmlentities(stripslashes($row['no_hp'].'|'.$row['nama']));
	        $new_row['no_hp']=htmlentities(stripslashes($row['no_hp']));
	        $new_row['alamat']=htmlentities(stripslashes($row['alamat']));
	        $new_row['email']=htmlentities(stripslashes($row['email']));
	        $new_row['pinbb']=htmlentities(stripslashes($row['pinbb']));
	        $row_set[] = $new_row; //build an array
	      }
	      echo json_encode($row_set); //format the array into json data
	    }
	}

	/**
	 * [ext description]
	 * @param  string $nama [description]
	 * @return [type]       [description]
	 */
	public function ext($nama = ""){
		$filter = array('nama'=>$nama);
		$this->db->where($filter);
		$data = $this->db->get("extensions");
		if($data->num_rows() > 0){
			$data = $data->row_array();
		}else{
			$data = 0;
		}
		return $data;
	}

	/**
	 * [setting digunakan untuk mendapatkan data setting]
	 * @param  [type] $code [description]
	 * @return [type]       [description]
	 */
	public function setting($code,$key,$table){
		$filter = array('code'=>$code,'key'=>$key);
		$this->db->where($filter);
		$data = $this->db->get($table);
		if($data->num_rows() > 0){
			$data = $data->row_array();
		}else{
			$data = array('pesan'=>'setting tidak ditemukan');
		}

		return $data;
	}

	public function detail_pembayaran($metode_pembayaran,$kode_metode_pembayaran){
		$q = "select `value` from setting where `code` = ?  and `key` = ? ";
		$p_q = array($metode_pembayaran,'rekening');
		$d_q = $this->db->query($q,$p_q);
		$json = array();
		if($d_q->num_rows() > 0){
			$d_q = $d_q->row_array();
			$dson = json_decode($d_q['value'],true);
			foreach ($dson as $key => $value) {
				if($value['kode_metode_pembayaran'] == $kode_metode_pembayaran){
					$json = $value;
				}
			}
		}

		return $json;
	}

	public function replace_data($table='',$data=array(),$title='',$sukses='',$gagal=''){
		if($this->db->replace($table,$data)){
			$ids = $this->db->insert_id();
			$response = array('status'=>true,'pesan'=>$sukses,'kode'=>1,'ids'=>$ids);
		}else{
			$response = array('status'=>flse,'pesan'=>$gagal,'kode'=>2);
		}

		return $response;
	}

}

/* End of file mdl_crud.php */
/* Location: ./application/models/mdl_crud.php */
