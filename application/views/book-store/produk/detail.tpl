{extends file=$themes}
{block name="main-content"}
    {if $detail['qc'] == '3'}
      <!-- Shop Detail -->
      <div class="product-grid-holder">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <br>
              <ul class="tc-breadcrumb">
                <li><a href="{$this->site_url}" class="text-primary">Home</a></li>
                <li><a href="{site_url('kategoriproduk/index/')}/{$pos_kategori['id_1']}" class="text-primary">{$pos_kategori['nama_1']}</a></li>
                <li><a href="{site_url('kategoriproduk/index/')}/{$pos_kategori['id_2']}" class="text-primary">{$pos_kategori['nama_2']}</a></li>
                {if $pos_kategori['id_3'] !=""}
                <li><a href="{site_url('kategoriproduk/index/')}/{$pos_kategori['id_3']}" class="text-primary">{$pos_kategori['nama_3']}</a></li>
                {/if}
                <li class="active">{$pos_kategori['judul_produk']}</li>
              </ul>
              <br>
              <br>
              <br>

            </div>
          </div>
          <!-- Alert -->

            {if $this->session->flashdata('pesan') !=""}
              <div class="add-cart-alert">
                {$this->session->flashdata('pesan')}
              </div>
            {/if}

          <!-- Alert -->

          {if array_key_exists('tombol',$detail)}
            {assign var=cfg value=['id'=>'form_add_cart']}
            {form_open($detail['tombol'],$cfg)}
          {/if}

          <!-- Single Product Detail -->
          <div class="single-product-detail">

            <div class="row">

              <!-- Product Thumnbnail -->
              <div class="col-lg-4 col-md-5">
                <div class="product-thumnbnail">
                  <ul class="product-slider">
                      {assign var=aktif value=''}
                      {assign var=total_gambar value='0'}
                      {foreach from=$detail['gambars'] key=k item=gambar}
                        {$total_gambar = $total_gambar + 1}
                        {if $total_gambar < 4}
                        <li>
                          <img src="{base_url()}admin718/assets/img/com_cart/produk/{$detail['gambar_detail']}" width="173" height="259" alt="">
                        </li>
                        {/if}
                      {/foreach}
                  </ul>
                  <div id="product-thumbs">
                      {$total_gambar=0}
                      {foreach from=$detail['gambars'] key=k item=gambar}
                        {if $total_gambar < 2}
                        <a data-slide-index="{$total_gambar}" href=""><img src="{base_url()}admin718/assets/img/com_cart/produk/{$gambar['gambar']}" width="55" height="81" alt="" /></a>
                        {/if}
                        {$total_gambar = $total_gambar + 1}
                      {/foreach}
                  </div>
                </div>
              </div>
              <!-- Product Thumnbnail -->

              <!-- Product Detail -->
              <div class="col-lg-8 col-md-7">
                <div class="single-product-detail">
                    <span class="availability">Ketersediaan :<strong>{$detail['nama_status']}</strong></span>
                    {if $detail['judul_produk'] !=''}
                      <h3>{$detail['judul_produk']}</h3>
                    {/if}
                    {if $detail['judul_produk'] ==''}
                      <h3>{$detail['nama']}</h3>
                    {/if}

                    {if $nilai_star > 0}
                      <ul class="rating-stars">
                        {for $star=1 to $nilai_star}
                            <i class="fa fa-star" style="color:#F0BF2D;"></i>
                        {/for}
                        <li style="margin-right:5px;"> <a href="#tab-1" onclick="sembunyikan('#tab-1')">{$total_vote} customer review</a></li>
                      </ul>
                    {/if}

                    <h4>Overview</h4>
                    <p>{word_limiter(strip_tags($detail['keterangan']),40)} <a href="#desk" style="margin-left:1%;color:blue;">Read More</a></p>
                    {if array_key_exists('options',$detail)}
                        {foreach from=$detail['options'] key=k item=op}
                          <div class="quantity-box {if $op['required'] > 0}Required{/if}">
                            <span>{$op['nama']}</span>
                            {if $op['type'] == 'text'}
                              <input type="text" name="option[{$k}][{$op['produk_option_id']}]" value="{$op['value']}" placeholder="{$op['name']}" id="option{$op['produk_option_id']}" class="form-control" length="300" {if $op['required'] > 0}required{/if} />
                            {/if}
                            {if $op['type'] == 'textarea'}
                              <textarea name="option[{$k}][{$op['produk_option_id']}]" id="option{$op['produk_option_id']}" cols="30" rows="10" class="form-control">{$op['value']}</textarea>
                            {/if}
                            {if $op['type'] == 'select'}
                              <select name="option[{$k}][{$op['produk_option_id']}]" id="option{$op['produk_option_id']}" class="form-control">
                                  <option value=""></option>
                                {if array_key_exists('option_values',$op)}
                                  {foreach from=$op['option_values'] key=ko item=opv}
                                    <option value="{$opv['produk_option_value_id']}">{$opv['nama']} (Harga {$opv['price_prefix']} {$opv['price']})</option>
                                  {/foreach}
                                {/if}
                              </select>
                            {/if}
                            {if $op['type'] == 'radio'}
                                <span>
                                  <input type="radio" name="option[{$k}][{$op['produk_option_id']}]" value="{$opv['produk_option_value_id']}" /> {$opv['nama']} (Harga {$opv['price_prefix']} {$opv['price']})
                                </span>
                            {/if}
                            {if $op['type'] == 'radio'}
                                <span>
                                  <input type="checkbox" name="option[{$k}][{$op['produk_option_id']}][]" value="{$opv['produk_option_value_id']}" /> {$opv['nama']} (Harga {$opv['price_prefix']} {$opv['price']})
                                </span>
                            {/if}
                            </div>
                          {/foreach}
                    {/if}
                    <div class="quantity-box">
                      <label>Qty :</label>
                      <div class="sp-quantity">
                          <div class="sp-minus fff"><a class="ddd" data-multi="-1">-</a></div>
                          <div class="sp-input">
                              <input type="text" name="qty" class="quntity-input" title="Qty" value="{$detail['minimum']}" />
                          </div>
                          <div class="sp-plus fff"><a class="ddd" data-multi="1">+</a></div>
                      </div>
                    </div>
                    <ul class="btn-list">
                      {if array_key_exists('tombol',$detail)}
                        <li><a class="btn-1 sm shadow-0" onclick="add_cart()" href="#">add to cart</a></li>
                      {/if}
                      {if !array_key_exists('tombol',$detail)}
                        <li><a class="btn-1 sm shadow-0"  href="#">produk tidak tersedia</a></li>
                      {/if}
                      <li><a class="btn-1 sm shadow-0 blank" href="{site_url('cart_order/cart/add_wishlist')}/{$detail['produk_id']}"><i class="fa fa-heart"></i></a></li>
                      <li><a class="btn-1 sm shadow-0 blank" href="#"><i class="fa fa-repeat"></i></a></li>
                      <li><a class="btn-1 sm shadow-0 blank" href="#"><i class="fa fa-share-alt"></i></a></li>
                    </ul>
                </div>
              </div>
              <!-- Product Detail -->

            </div>
          </div>
          <!-- Single Product Detail -->

          {if array_key_exists('tombol',$detail)}
              {form_close()}
          {/if}

          <!-- Disc Nd Reviews -->
          <div class="disc-nd-reviews tc-padding-bottom">
            <div class="row">
              <div id="disc-reviews-tabs" class="disc-reviews-tabs">

                <!-- Tabs Nav -->
                <div class="col-lg-3 col-xs-12">
                  <div class="tabs-nav">
                    <ul>
                      <li><a href="#tab-2" onclick="sembunyikan('#tab-2')">Description</a></li>
                      <li><a href="#tab-1" onclick="sembunyikan('#tab-1')">Reviews</a></li>
                    </ul>
                  </div>
                </div>
                <!-- Tabs Nav -->

                <!-- Tabs Content -->
                <div class="col-lg-9 col-xs-12">
                  <div class="tabs-content">
                    <!-- Description -->
                    <div id="tab-2">
                      <div class="description" id="desk">

                        {assign var=keterangan value=strip_tags($detail['keterangan'],"<table><tr><tr><p><br><ul><li><a><img><span>")}
                        {$keterangan}
                      </div>
                      {if !array_key_exists('pesan',$pk_related)}
                      <!-- Related Products -->
                      <div class="related-products">
                        <h5>Mungkin Anda <span>akan suka ini</span></h5>
                        <div class="related-produst-slider">
                          {assign var=no_r value=0}
                          {foreach from=$pk_related key=k item=produk}
                            {$no_r = $no_r+1}
                            {if $no_r < 4}
                              <div class="item">
                                <div class="product-box">
                                    <div class="product-img">
                                      <img src="{base_url()}admin718/assets/img/com_cart/produk/{$produk['gambar_thumb']}" alt="{$produk['nama']}" widht="132" height="197">
                                      <ul class="product-cart-option position-center-x">
                                        <li><a href="#"><i class="fa fa-eye"></i></a></li>
                                        <li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
                                        <li><a href="#"><i class="fa fa-share-alt"></i></a></li>
                                      </ul>
                                    </div>
                                    <div class="product-detail">
                                      <!-- <span>Novel</span> -->
                                      {if $produk['judul_produk'] !=''}
                                        <h5>{$produk['judul_produk']}</h5>
                                      {/if}
                                      {if $produk['judul_produk'] ==''}
                                        <h5>{$produk['nama']}</h5>
                                      {/if}
                                      <p>How to Write a Book Review...</p>
                                      <div class="rating-nd-price">
                                        <strong>Rp. {number_format($produk['harga'],0,",",".")}</strong>
                                        <ul class="rating-stars">
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star-half-o"></i></li>
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                            {/if}
                          {/foreach}
                        </div>
                      </div>
                      <!-- Related Products -->
                      {/if}

                    </div>
                    <!-- Description -->


                    <!-- Reviews -->
                    <div id="tab-1">
                      <div class="reviews">
                        <!-- tab review -->
                        <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#home">User Review</a></li>
                          <li><a data-toggle="tab" href="#menu1">Editor Review</a></li>
                        </ul>

                        <div class="tab-content">
                          <!-- user review -->
                          <div id="home" class="tab-pane fade in active">
                              <!-- Reviews List -->
                              <div class="reviews-list" style="margin-top:2%;">
                                {if $list_review !=0}
                                    <ul>
                                      {foreach from=$list_review key=k item=review}
                                        <li>
                                          <img src="{theme_url('images/reviews/img-01.jpg')}" alt="">
                                          <div class="comment">
                                            <div class="reviews-detail">
                                              <h6>{$review['customer']} <span>{$review['date_added']}</span></h6>
                                              <ul class="rating-stars">
                                                  {for $start=1 to $review['star_produk']}
                                                    <li><i class="fa fa-star"></i></li>
                                                  {/for}
                                                </ul>
                                              </div>
                                            <p>{$review['review']}</p>
                                          </div>
                                        </li>
                                      {/foreach}
                                    </ul>
                                {/if}
                                {if $list_review ==0}
                                  <ul>
                                    <li> <strong>Belum Ada Review</strong> </li>
                                  </ul>
                                {/if}
                              </div>
                              <!-- Reviews List -->

                              <!-- Form -->
                              <div class="form-holder add-review">
                                    <h5>Add a Review</h5>
                                    <h6>Your Rating</h6>

                                    {assign var=cfg_review value=['id'=>'form_add_review']}
                                    {if $status_review['status_login'] == 1}

                                          {form_open(site_url('cart_catalog/produk/simpan_review'),$cfg_review)}
                                            <input type="hidden" name="star_produk" id="rate" value="0">
                                            <input type="hidden" name="produk_id" value="{$detail['produk_id']}">

                                            <div class="row">
                                              <div class="col-sm-12">
                                                <div class="form-group">
                                                  <div id="rateYo"></div>
                                                </div>
                                                <div class="form-group">
                                                  <textarea class="form-control" name="review" required="required" rows="3" placeholder="Text here..."></textarea>
                                                </div>
                                              </div>
                                              <div class="col-sm-12">
                                                <a class="btn-1 shadow-0 sm" href="#" onclick="add_review()">Submit</a>
                                              </div>
                                            </div>
                                          {form_close()}

                                    {/if}
                                    {if $status_review['status_login'] == 2}
                                      {if $this->session->has_userdata(member_id)}
                                        {if $status_review['status_beli'] == 1}

                                            {form_open(site_url('cart_catalog/produk/simpan_review'),$cfg_review)}
                                              <input type="hidden" name="star_produk" id="rate" value="0">
                                              <input type="hidden" name="produk_id" value="{$detail['produk_id']}">

                                              <div class="row">
                                                <div class="col-sm-12">
                                                  <div class="form-group">
                                                    <div id="rateYo"></div>
                                                  </div>
                                                  <div class="form-group">
                                                    <textarea class="form-control" name="review" required="required" rows="3" placeholder="Text here..."></textarea>
                                                  </div>
                                                </div>
                                                <div class="col-sm-12">
                                                  <a class="btn-1 shadow-0 sm" href="#" onclick="add_review()">Submit</a>
                                                </div>
                                              </div>
                                            {form_close()}

                                        {/if}
                                        {if $status_review['status_beli'] == 2}
                                          {if $user_review == 1}

                                              {form_open(site_url('cart_catalog/produk/simpan_review'),$cfg_review)}
                                                <input type="hidden" name="star_produk" id="rate" value="0">
                                                <input type="hidden" name="produk_id" value="{$detail['produk_id']}">

                                                <div class="row">
                                                  <div class="col-sm-12">
                                                    <div class="form-group">
                                                      <div id="rateYo"></div>
                                                    </div>
                                                    <div class="form-group">
                                                      <textarea class="form-control" name="review" required="required" rows="3" placeholder="Text here..."></textarea>
                                                    </div>
                                                  </div>
                                                  <div class="col-sm-12">
                                                    <a class="btn-1 shadow-0 sm" href="#" onclick="add_review()">Submit</a>
                                                  </div>
                                                </div>
                                              {form_close()}

                                          {/if}
                                        {/if}
                                      {/if}
                                    {/if}

                              </div>
                              <!-- Form -->
                          </div>
                          <!-- user review -->
                          <!-- editor review -->
                          <div id="menu1" class="tab-pane fade">
                            <!--Editor Reviews List -->


                              {if $list_review_editor !=0}
                                <div class="reset-this">
                                  {foreach from=$list_review_editor key=k item=review}
                                        {$review['review']}
                                  {/foreach}
                                </div>
                              {/if}
                              {if $list_review_editor ==0}
                                <ul>
                                  <li> <strong>Belum Ada Review</strong> </li>
                                </ul>
                              {/if}

                            <!--Editor Reviews List -->
                          </div>
                          <!-- editor review -->
                        </div>
                        <!-- tab review -->

                      </div>
                    </div>
                    <!-- Reviews -->



                  </div>
                </div>
                <!-- Tabs Content -->

              </div>
            </div>
          </div>
          <!-- Disc Nd Reviews -->

        </div>
      </div>
      <!-- Shop Detail -->

      <!-- Produk by penulis -->
      <!-- Best Seller Products -->
      <section class="best-seller tc-padding" style="border:0;" id="p_penulis">
        <div class="container">

          <!-- Best sellers Tabs -->
          <div id="best-sellers-tabs-p" class="best-sellers-tabs" style="border:0;">

            <!-- Nav tabs -->
            <div class="tabs-nav-holder">
              <div><a href="#tab-P-1" class="btn btn-warning" style="color:#ffffff;  background-color:#ff851d;">DARI PENULIS YANG SAMA</a></div>
            </div>
            <!-- Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content">

              <!-- Best Seller Slider -->
              <div id="tab-p-1" style="border:0;">
                <div class="best-seller-slider" id="item_penulis">

                  <!-- Product Box -->
                  <div class="item">
                    <div class="product-box">
                      <div class="product-img">
                        <img src="{theme_url('images/best-seller/img-01.jpg')}" width="132" height="197" alt="">
                        <ul class="product-cart-option position-center-x">
                          <li><a href="#"><i class="fa fa-eye"></i></a></li>
                          <li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
                          <li><a href="#"><i class="fa fa-share-alt"></i></a></li>
                        </ul>
                        <span class="sale-bacth">sale</span>
                      </div>
                      <div class="product-detail">
                        <span>Novel</span>
                        <h5><a href="book-detail.html">Dilan</a></h5>
                        <p>How to Write a Book Review...</p>
                        <div class="rating-nd-price">
                          <strong>$280.99</strong>
                          <ul class="rating-stars">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                          </ul>
                        </div>
                        <div class="aurthor-detail">
                          <span><img src="images/book-aurthor/img-01.jpg" alt="">Pawel Kadysz</span>
                          <a class="add-wish" href="#"><i class="fa fa-heart"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Product Box -->

                </div>
              </div>
              <!-- Best Seller Slider -->
            </div>
            <!-- Tab panes -->

          </div>
          <!-- Best sellers Tabs -->

        </div>
      </section>
      <!-- Best Seller Products -->
      <!-- Produk by penulis -->

      <!-- Produk by kategori -->
      <!-- Best Seller Products -->
      <section class="best-seller tc-padding" style="border:0;" id="p_kategori">
        <div class="container">

          <!-- Best sellers Tabs -->
          <div id="best-sellers-tabs" class="best-sellers-tabs" style="border:0;">

            <!-- Nav tabs -->
            <div class="tabs-nav-holder">
              <div><a href="#tab-s-1" class="btn btn-warning" style="color:#ffffff; background-color:#ff851d;">DARI KATEGORI YANG SAMA</a></div>
            </div>
            <!-- Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content">

              <!-- Best Seller Slider -->
              <div id="tab-s-1" style="border:0;">
                <div class="best-seller-slider" id="item_pbk">

                  <!-- Product Box -->
                  <div class="item">
                    <div class="product-box">
                      <div class="product-img">
                        <img src="{theme_url('images/best-seller/img-01.jpg')}" width="132" height="197" alt="">
                        <ul class="product-cart-option position-center-x">
                          <li><a href="#"><i class="fa fa-eye"></i></a></li>
                          <li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
                          <li><a href="#"><i class="fa fa-share-alt"></i></a></li>
                        </ul>
                        <span class="sale-bacth">sale</span>
                      </div>
                      <div class="product-detail">
                        <span>Novel</span>
                        <h5><a href="book-detail.html">Dilan</a></h5>
                        <p>How to Write a Book Review...</p>
                        <div class="rating-nd-price">
                          <strong>$280.99</strong>
                          <ul class="rating-stars">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                          </ul>
                        </div>
                        <div class="aurthor-detail">
                          <span><img src="images/book-aurthor/img-01.jpg" alt="">Pawel Kadysz</span>
                          <a class="add-wish" href="#"><i class="fa fa-heart"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Product Box -->

                </div>
              </div>
              <!-- Best Seller Slider -->
            </div>
            <!-- Tab panes -->

          </div>
          <!-- Best sellers Tabs -->

        </div>
      </section>
      <!-- Best Seller Products -->
      <!-- Produk by kategori -->



    {/if}
    {if $detail['qc'] != '3'}
      <div class="product-grid-holder">
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group text-center">
                  <h5>Produk Tidak DItemukan</h5>
                </div>
              </div>
            </div>
          </div>
      </div>
    {/if}
{/block}
{block name=script_js}
  <script type="text/javascript">
        $(function(){

            $("#rateYo").rateYo({
              rating    : 0,
              precision: 0,
              spacing   : "5px",
              maxValue: 5,
              multiColor: {
                "startColor": "#FF0000", //RED
                "endColor"  : "#FF8401"  //GREEN
              },
              onSet: function (rating, rateYoInstance) {
               $('#rate').val(rating);
                // alert("Rating is set to: " + rating);
              }
            });
        });
        var ganti = function(url){
          $('#main_image').attr('src',url);
        }
        var add_cart = function(){
          $('#form_add_cart')[0].submit(function(event) {
            /* Act on the event */
          });
        }
        var add_review = function(){
          $('#form_add_review')[0].submit(function(event) {
            /* Act on the event */
          });
        }
        //
        // var add_to_cart_related_{$id_tag} = function(id_produk){
        //   var url = "{site_url('cart_order/cart_api/add_test')}/"+id_produk;
        //   $.post(url, { qty: '1' }, function(data, textStatus, xhr) {
        //       $('#pesan_notif').html(data.pesan);
        //       // if(data.status && data.kode == '1'){
        //         // window.location.href = "{site_url('cart_order/cart/show')}";
        //       // }
        //   });
        // }
        //
        // function activaTab(tabID){
        //     $('.nav-pills a[href="#' + tabID + '"]').tab('show');
        // };
        var sembunyikan = function(tab){
          $('#tab-1').hide();
          $('#tab-2').hide();
          $(tab).show();
        }
        sembunyikan('#tab-2');
        var buku_by_kategori = function(){
          // http://modular.develop.com/api/kategori_produk/list_produk/269/10/0?idkey=S9RP3YD7JL&passkey=XJX3LFSG3CF4Q91WB
          var kategori = "{$pos_kategori['id_2']}";
          var url = "{site_url('api/kategori_produk/list_produk')}/"+kategori+"/10/0?idkey=S9RP3YD7JL&passkey=XJX3LFSG3CF4Q91WB";
          var gambar = "{theme_url('images/best-seller/img-01.jpg')}";
          var html = '';
          $.getJSON(url, { }, function(json, textStatus) {

              // console.log(json);
              if(json.data.kode == 1){
                  $.each(json.data.produk_data,function(index, el) {
                      html +='<div class="item">';
                        html +='<div class="product-box">';
                          html +='<div class="product-img">';
                            html +='<img src="{base_url()}admin718/assets/img/com_cart/produk/'+el.gambar_thumb+'" width="132" height="197" alt="">';
                            html +='<ul class="product-cart-option position-center-x">';
                              html +='<li><a href="'+el.link_produk+'"><i class="fa fa-eye"></i></a></li>';
                              html +='<li><a href="#item_pbk" onclick="modal_detail_buku('+el.produk_id+')"><i class="fa fa-cart-arrow-down"></i></a></li>';
                              html +='<li><a href="#"><i class="fa fa-share-alt"></i></a></li>';
                            html +='</ul>';
                            html +='<span class="sale-bacth">'+el.discount+'%</span>';
                          html +='</div>';
                          html +='<div class="product-detail">';
                            html +="<span>{$pos_kategori['nama_2']}</span>";
                            html +='<h5><a href="'+el.link_produk+'">'+el.judul_produk+'</a></h5>';
                            if(el.total_vote == 0){
                              html +='<p>Belum Ada Komentar...</p>';
                            }
                            if(el.total_vote > 0 ){
                              html +='<p>'+el.total_vote+' Komentar...</p>';
                            }
                            html +='<div class="rating-nd-price">';

                                    if(el.harga_lama == 0){
                                      html +='<strong>Rp. '+el.harga+'</strong>';
                                    }
                                    if(el.harga_lama > 0){
                                      html +='<strong class="harga-discount">Rp. '+el.harga_lama+'</strong>';
                                      html +='<strong>Rp. '+el.harga+'</strong>';
                                    }

                              html +='<ul class="rating-stars">';
                                    if(el.total_vote > 0 ){
                                      for(count = 1; count <= el.nilai_star; count++){
                                          html +='<li><i class="fa fa-star"></i></li>';
                                       }
                                    }
                              html +='</ul>';
                            html +='</div>';

                          html +='</div>';
                        html +='</div>';
                      html +='</div>';
                  });
                  $('#item_pbk').html(html);
                  // Best Seller Slider
                  if (jQuery("#item_pbk").length != '') {
                      jQuery('#item_pbk').owlCarousel({
                        loop:true,
                        margin:30,
                        nav:true,
                        dots: false,
                        smartSpeed:600,
                        responsiveClass:true,
                        autoplay:true,
                          responsive:{
                              0:{ items:1},
                              320:{ items:1},
                              480:{ items:2},
                              640:{ items:2},
                              768:{ items:2},
                              800:{ items:2},
                              960:{ items:3},
                              991:{ items:2},
                              1024:{ items:3},
                              1199:{ items:3},
                              1200:{ items:4}
                          }
                      })
                  }
              }
          });
        }

        var buku_by_penulis = function(){
          // http://modular.develop.com/api/kategori_produk/list_produk/269/10/0?idkey=S9RP3YD7JL&passkey=XJX3LFSG3CF4Q91WB
          var kategori = "{$detail['pengarang_id']}";
          var url = "{site_url('api/penulis/list_produk')}/"+kategori+"/10/0?idkey=S9RP3YD7JL&passkey=XJX3LFSG3CF4Q91WB";
          var gambar = "{theme_url('images/best-seller/img-01.jpg')}";
          var html = '';
          $.getJSON(url, { }, function(json, textStatus) {

              // console.log(json);
              if(json.data.kode == 1){
                  $.each(json.data.produk_data,function(index, el) {
                      html +='<div class="item">';
                        html +='<div class="product-box">';
                          html +='<div class="product-img">';
                            html +='<img src="{base_url()}admin718/assets/img/com_cart/produk/'+el.gambar_thumb+'" width="132" height="197" alt="">';
                            html +='<ul class="product-cart-option position-center-x">';
                              html +='<li><a href="'+el.link_produk+'"><i class="fa fa-eye"></i></a></li>';
                              html +='<li><a href="#item_penulis" onclick="modal_detail_buku('+el.produk_id+')"><i class="fa fa-cart-arrow-down"></i></a></li>';
                              html +='<li><a href="#"><i class="fa fa-share-alt"></i></a></li>';
                            html +='</ul>';
                            html +='<span class="sale-bacth">'+el.discount+'%</span>';
                          html +='</div>';
                          html +='<div class="product-detail">';
                            html +="<span>{$pos_kategori['nama_2']}</span>";
                            html +='<h5><a href="'+el.link_produk+'">'+el.judul_produk+'</a></h5>';
                            if(el.total_vote == 0){
                              html +='<p>Belum Ada Komentar...</p>';
                            }
                            if(el.total_vote > 0 ){
                              html +='<p>'+el.total_vote+' Komentar...</p>';
                            }
                            html +='<div class="rating-nd-price">';

                                    if(el.harga_lama == 0){
                                      html +='<strong>Rp. '+el.harga+'</strong>';
                                    }
                                    if(el.harga_lama > 0){
                                      html +='<strong class="harga-discount">Rp. '+el.harga_lama+'</strong>';
                                      html +='<strong>Rp. '+el.harga+'</strong>';
                                    }

                              html +='<ul class="rating-stars">';
                                    if(el.total_vote > 0 ){
                                      for(count = 1; count <= el.nilai_star; count++){
                                          html +='<li><i class="fa fa-star"></i></li>';
                                       }
                                    }
                              html +='</ul>';
                            html +='</div>';

                          html +='</div>';
                        html +='</div>';
                      html +='</div>';
                  });
                  $('#item_penulis').html(html);
                  // Best Seller Slider
                  if (jQuery("#item_penulis").length != '') {
                      jQuery('#item_penulis').owlCarousel({
                        loop:true,
                        margin:30,
                        nav:true,
                        dots: false,
                        smartSpeed:600,
                        responsiveClass:true,
                        autoplay:true,
                          responsive:{
                              0:{ items:1},
                              320:{ items:1},
                              480:{ items:2},
                              640:{ items:2},
                              768:{ items:2},
                              800:{ items:2},
                              960:{ items:3},
                              991:{ items:2},
                              1024:{ items:3},
                              1199:{ items:3},
                              1200:{ items:4}
                          }
                      })
                  }
              }
          });
        }

        buku_by_kategori();
        buku_by_penulis();



    </script>
{/block}
