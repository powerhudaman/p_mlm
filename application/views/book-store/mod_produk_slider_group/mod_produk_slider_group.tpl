<!-- Best Seller Products produk group slider -->
  <section class="best-seller tc-padding">
    <div class="container ">

      <!-- Main Heading -->
      <div class="main-heading-holder">
        <div class="main-heading style-1">
          <h2>
            {$title_produk_slider}
          </h2>
        </div>
      </div>
      <!-- Main Heading -->

      <!-- Best sellers Tabs -->
      <div id="best-sellers-tabs_{$group_slider['id']}" class="best-sellers-tabs">

        <!-- Nav tabs -->
        <div class="tabs-nav-holder">
          <ul class="tabs-nav" >
              {assign var=no value=0}
              {foreach from=$produk_sliders key=produk_slider item=ps}
                {$no = $no+1}
                {if $no == '1'}
                  <li><a href="#{$group_slider['id']}_{$ps['id']}">{$ps['nama']}</a></li>
                {/if}
                {if $no > 1}
                  <li><a href="#{$group_slider['id']}_{$ps['id']}">{$ps['nama']}</a></li>
                {/if}
              {/foreach}

          </ul>
        </div>
        <!-- Nav tabs -->

        <!-- Tab panes -->
        <div class="tab-content">
          {$no=0}
          {foreach from=$produk_sliders key=produk_slider item=ps}
            {$no = $no+1}

              <!-- Best Seller Slider -->
              <div id="{$group_slider['id']}_{$ps['id']}">

                <div class="best-seller-slider">
                  {foreach from=$produks[$ps['id']] key=k_p item=produk}
                    <!-- Product Box -->
                    <div class="item">
                      <div class="product-box">
                        <div class="product-img">
                          <img src="{base_url()}admin718/assets/img/com_cart/produk/{$produk['gambar']}" alt="{$produk['nama']}" width="132px" height="197px">
                          <ul class="product-cart-option position-center-x">
                            <li><a href="#"><i class="fa fa-eye"></i></a></li>
                            <li><a href="#{$ps['id']}" onclick="modal_detail_buku({$produk['produk_id']})" ><i class="fa fa-cart-arrow-down"></i></a></li>
                            <li><a href="#"><i class="fa fa-share-alt"></i></a></li>
                          </ul>
                          <span class="sale-bacth">{$produk['discount']}%</span>
                        </div>
                        <div class="product-detail">
                          <span>{$ps['nama']}</span>
                          <h5 class="box-h5">
                            {if $seo =='0'}

                              {if $produk['judul_produk'] !=''}
                                <a href="{site_url('detail')}/{$produk['produk_id']}">{$produk['judul_produk']}</a>
                              {/if}
                              {if $produk['judul_produk'] ==''}
                                <a href="{site_url('detail')}/{$produk['produk_id']}">{$produk['nama']}</a>
                              {/if}

                            {/if}
                            {if $seo =='1'}
                              {if $produk['url_title'] !=''}
                                  {if $produk['judul_produk'] !=''}
                                    <a href="{site_url($produk['url_title'])}">{$produk['judul_produk']}</a>
                                  {/if}
                                  {if $produk['judul_produk'] ==''}
                                    <a href="{site_url($produk['url_title'])}">{$produk['nama']}</a>
                                  {/if}
                              {/if}
                              {if $produk['url_title'] ==''}

                                {if $produk['judul_produk'] !=''}
                                  <a href="{site_url('detail')}/{$produk['produk_id']}">{$produk['judul_produk']}</a>
                                {/if}
                                {if $produk['judul_produk'] ==''}
                                  <a href="{site_url('detail')}/{$produk['produk_id']}">{$produk['nama']}</a>
                                {/if}

                              {/if}
                            {/if}


                          </h5>
                          <p>
                            {if $produk['total_vote'] == 0}
                              Belum Ada Review
                            {/if}
                            {if $produk['total_vote'] > 0}
                              <ul class="rating-stars">
                                {for $f='1' to $produk['nilai_star']}
                                  <li><i class="fa fa-star"></i></li>
                                {/for}
                                <li style="margin-left:10px;"><a href="{site_url('detail')}/{$produk['produk_id']}#tab-1">{$produk['total_vote']} reviews</a></li>
                              </ul>
                            {/if}
                          </p>
                          <div class="rating-nd-price">
                            {if $produk['discount'] > 0 && $produk['harga_lama'] > 0}
                              <strong class="harga-discount">Rp. {number_format($produk['harga_lama'],0,",",".")}</strong>
                            {/if}
                            <strong>Rp. {number_format($produk['harga'],0,",",".")}</strong>
                            <ul class="rating-stars">
                              {if $nilai_star > 0}
                                {for $star=1 to $produk['nilai_star']}
                                  <li><i class="fa fa-star"></i></li>
                                {/for}
                              {/if}
                            </ul>
                          </div>
                          <!-- <div class="aurthor-detail">
                            <span><img src="{theme_url('images/book-aurthor/img-01.jp')}g" alt="">Pawel Kadysz</span>
                            <a class="add-wish" href="#"><i class="fa fa-heart"></i></a>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <!-- Product Box -->
                  {/foreach}
                </div>
                {if $group_slider['nama_link'] !="" && $group_slider['tujuan_link'] !=""}

                <a href="{$group_slider['tujuan_link']}" class="pull-right text-primary" style="margin:20px 10px 10px 0px; font-size:22px; border:solid 1px #e0e0e0;">{$group_slider['nama_link']}</a>

                {/if}
              </div>
              <!-- Best Seller Slider -->
          {/foreach}


        </div>
        <!-- Tab panes -->

      </div>
      <!-- Best sellers Tabs -->

    </div>
  </section>

  <script>

    if (jQuery(".best-seller-slider").length != '') {
        jQuery('.best-seller-slider').owlCarousel({
            loop:true,
            margin:30,
            nav:true,
            dots: false,
            smartSpeed:600,
            responsiveClass:true,
            autoplay:true,
            responsive:{
                0:{ items:1},
                320:{ items:1},
                480:{ items:2},
                640:{ items:2},
                768:{ items:2},
                800:{ items:2},
                960:{ items:3},
                991:{ items:2},
                1024:{ items:3},
                1199:{ items:3},
                1200:{ items:4}
            }
        })
    }
  // Best Seller Tabs
    if (jQuery("#best-sellers-tabs_{$group_slider['id']}").length != '') {
        jQuery( "#best-sellers-tabs_{$group_slider['id']}" ).tabs();
    }
  </script>
<!-- Best Seller Products produk group slider -->
