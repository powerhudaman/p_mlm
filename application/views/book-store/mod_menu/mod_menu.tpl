{if $posisi=="pos-5"}    <div class="sidebar">
      <h4>{$menu['nama']}</h4>
      <ul>
        {foreach from=$menu_items key=k item=item}
             {assign var=aktif value=''}
             {if !array_key_exists('subs',$item)}
                 {if $item['link'] == site_url($this->uri->uri_string())}
                   {$aktif='ui-state-active'}
                 {/if}

                 <li class="{$aktif}"><a href="{$item['link']}">{$item['nama']}</a></li>
             {/if}
        {/foreach}
      </ul>
    </div>

{/if}{if $posisi=="pos-2"}    <!-- navigasi bookstore begin -->
    <div class="navigation">
      <ul>
        {foreach from=$menu_items key=k item=item}
          {assign var=icon value=''}

          {if array_key_exists('icon',$item)}
            {$icon = $item['icon']}
          {/if}

           {if !array_key_exists('subs',$item)}
             <li><a href="{$item['link']}"><i class="fa {$icon}"></i>{$item['nama']}</a></li>
           {/if}

           {if array_key_exists('subs',$item)}
             <li class="active dropdown-icon">
               <a href="#"><i class="fa {$icon}"></i>{$item['nama']}</a>
               <ul>
                 {foreach from=$item['subs'] key=sub item=v}
                   <li><a href="{$v['link']}">{$v['nama']}</a></li>
                 {/foreach}
               </ul>
             </li>

           {/if}

       {/foreach}
      </ul>
    </div>
    <!-- navigasi bookstore end -->
{/if}
{if $posisi=="pos-8"}

    <div class="brands_products"><!--brands_products-->
        <h2>{$menu['nama']}</h2>
        <div class="brands-name">
            <ul class="nav nav-pills nav-stacked">
                {foreach from=$menu_items key=k item=item}
                    <li><a href="{$item['link']}">{$item['nama']}</a></li>
                {/foreach}
            </ul>
        </div>
    </div><!--/brands_products-->
{/if}
{if $posisi == 'pos-12'}  <!-- Footer Column -->  <div class="col-lg-2 col-sm-6">    <div class="footer-column footer-links">      <h4>{$menu['nama']}</h4>      <ul>        {foreach from=$menu_items key=k item=item}          <li><a href="{$item['link']}">{$item['nama']}</a></li>        {/foreach}      </ul>    </div>  </div>  <!-- Footer Column -->
{/if}