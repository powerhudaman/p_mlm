<!-- Book Collections -->
<section class="book-collection">
	<div class="container">
		<div class="row">

			<!-- Book Collections Tabs -->
			<div id="book-collections-tabs-gg">

				<!-- collection Name -->
				<div class="col-lg-3 col-sm-12">
					<div class="sidebar">
						<h4>Kategori Produk</h4>
						<ul>
							<!-- <li class="box-link"><a href="#book-collections-tabs-gg">Coba</a></li> -->

							{foreach from=$menu_items key=k item=i}
							<li id="heshin-{$i['id']}" class="heshin"><a href="#book-collections-tabs-gg" onclick="data_buku_perkategori({$i['id']})">{ucfirst($i['nama'])}</a></li>
							{if count($i['subs']) > 0} {foreach from=$i['subs'] key=kk item=ii}
								{if $ii['total_produk'] > 0}
									<li id="heshin-{$ii['id']}" class="heshin"><a href="#book-collections-tabs-gg" onclick="data_buku_perkategori({$ii['id']})">{character_limiter(ucfirst($ii['nama']),25)}</a></li>
								{/if}
							{/foreach} {/if} {/foreach}
						</ul>
					</div>
				</div>
				<!-- collection Name -->

				<!-- Collection Content -->
				<div class="col-lg-9 col-sm-12">
					<div class="collection">

						<!-- Secondary heading -->
						<div class="sec-heading">
							<h3>Shop <span class="theme-color">Books</span> Collection</h3>
							<a class="view-all" href="#" id="shopping-content-view-all">View All<i class="fa fa-angle-double-right"></i></a>
						</div>
						<!-- Secondary heading -->

						<!-- Collection Content -->
						<div class="collection-content" id="shop-collection-content">


							<ul>
								<li>
									<div class="s-product">
										<div class="s-product-img">
											<img src="http://modular.develop.com//admin718/assets/img/com_cart/produk/covUB-141_thumb.jpg" alt="" width="151px" height="218px" >
											<div class="s-product-hover">
												<div class="position-center-x">
													<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
															<!-- <a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a> -->
												</div>
											</div>
										</div>
										<h6><a href="book-detail.html">Ramadan Kareem</a></h6>
										<div class="rating-nd-price">
                                                          <strong class="harga-discount">Rp. 60.000</strong>
                                                        <strong>Rp. 45.000</strong>


                          </div>
										<div class="row">
											<div class="col-md-12">
												<a class="" href="http://dev.mizanstore.com/detail/53109#tab-1">1 reviews</a>
											</div>
										</div>

										<span>Richard Matherson</span>
									</div>
								</li>
							</ul>
						</div>
						<!-- Collection Content -->
					</div>
				</div>
				<!-- Collection Content -->

			</div>
			<!-- Book Collections Tabs -->

		</div>
	</div>
</section>
<!-- Book Collections -->

<script>
	var id_key = "{$akses_api['id_key']}";
	var pass_key = "{$akses_api['pass_key']}";
	var url_produk = "{site_url('api/kategori_produk/list_produk/1/10/0?idkey=S9RP3YD7JL&passkey=XJX3LFSG3CF4Q91WB')}";
	var url_gambar = "{base_url()}/admin718/assets/img/com_cart/produk";
	var data_buku_default = function() {

		$.ajax({
			url: url_produk,
			type: 'GET',
			dataType: 'json',
			beforeSend:function(){
				tidak_ditemukan('Loading Data');
			},
			success:function(data){
				if(!data.data.status){
					tidak_ditemukan(data.data.pesan);
				}
				if(data.data.status){
					kelas_aktif(1);
					url_kategori_produk = "{site_url('cart_catalog/kategoriproduk/index')}/1";
					$('#shopping-content-view-all').attr('href',url_kategori_produk);
					var html = '';
					html += '<ul>';
					$.each(data.data.produk_data,function(index, el) {
						var url_buku = el.link_produk;
						html += '<li>';
							html += '<div class="s-product">';
								html += '<div class="s-product-img">';
									html += '<img src="'+url_gambar+'/'+el.gambar_thumb+'" alt="" width="151px" height="218px">';
									html += '<div class="s-product-hover">';
										html += '<div class="position-center-x">';
											html += '<a href="#book-collections-tabs-gg" onclick="modal_detail_buku('+el.produk_id+')" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>';
											// html += '<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="'+url_buku+'">Quick view</a>';
										html += '</div>';
									html += '</div>';
							html += '</div>';
							if(el.judul_produk == ""){
									html += '<h6><a href="'+url_buku+'">'+el.nama+'</a></h6>';
							}
							if(el.judul_produk != ""){
									html += '<h6><a href="'+url_buku+'">'+el.judul_produk+'</a></h6>';
							}

								html +='<div class="rating-nd-price">';
									if(el.harga_lama > 0){
											html +='<strong class="harga-discount" style="font-size:03em;">Rp.'+el.harga_lama+'</strong>';
									}
									html +='<strong style="font-size:1em;" >Rp.'+el.harga+'</strong>';
								html +='</div>';
								html +='<div class="row">';
									html +='<div class="col-md-12">';
										html +='<a class="" href="'+url_buku+'">'+el.total_vote+' reviews</a>';
									html +='</div>';
								html +='</div>';
							html += '<span>'+el.pengarang_id+'</span>';
							html += '</div>';
						html += '</li>';
					});
					html += '</ul>';
					$('#shop-collection-content').html(html);
				}
			}
		});
	}

	var data_buku_perkategori = function(id){
		url_produk = "{site_url('api/kategori_produk/list_produk/"+id+"/10/0?idkey=S9RP3YD7JL&passkey=XJX3LFSG3CF4Q91WB')}";
		url_kategori_produk = "{site_url('cart_catalog/kategoriproduk/index')}/"+id;
		kelas_aktif(id);
		$('#shopping-content-view-all').attr('href',url_kategori_produk);
		$.ajax({
			url: url_produk,
			type: 'GET',
			dataType: 'json',
			beforeSend:function(){
				tidak_ditemukan('Loading Data');
			},
			success:function(data){

				if(!data.data.status){
					tidak_ditemukan(data.data.pesan);
				}
				if(data.data.status){
					var html = '';
					html += '<ul>';
					$.each(data.data.produk_data,function(index, el) {
						var url_buku = el.link_produk;
						html += '<li>';
							html += '<div class="s-product">';
								html += '<div class="s-product-img">';
									html += '<img src="'+url_gambar+'/'+el.gambar_thumb+'" alt="" width="151px" height="218px">';
									html += '<div class="s-product-hover">';
										html += '<div class="position-center-x">';
											html += '<a href="#book-collections-tabs-gg" onclick="modal_detail_buku('+el.produk_id+')" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>';
											// html += '<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="'+url_buku+'">Quick view</a>';
										html += '</div>';
									html += '</div>';
							html += '</div>';
							if(el.judul_produk == ""){
									html += '<h6><a href="'+url_buku+'">'+el.nama+'</a></h6>';
							}
							if(el.judul_produk != ""){
									html += '<h6><a href="'+url_buku+'">'+el.judul_produk+'</a></h6>';
							}
							html +='<div class="rating-nd-price">';
								if(el.harga_lama > 0){
										html +='<strong class="harga-discount" style="font-size:0.5em;">Rp.'+el.harga_lama+'</strong>';
								}
								html +='<strong style="font-size:1em;">Rp.'+el.harga+'</strong>';
							html +='</div>';
							html +='<div class="row">';
								html +='<div class="col-md-12">';
									html +='<a class="" href="'+url_buku+'">'+el.total_vote+' reviews</a>';
								html +='</div>';
							html +='</div>';
							html += '<span>'+el.pengarang_id+'</span>';
							html += '</div>';
						html += '</li>';
					});
					html += '</ul>';
					$('#shop-collection-content').html(html);
				}
			}
		});

	}

	// data_buku_perkategori(1);

	var tidak_ditemukan = function(pesan){
		var html ='';
				html +='<div class="col-md-3"></div>';
				html +='<div class="col-md-6 text-center">';
				html +='<br><br><br><br><br><br>';
				html +='<h2>'+pesan+'</h2>';
				html +='</div>';
				html +='<div class="col-md-3"></div>';
				$('#shop-collection-content').html(html);

	}

	var kelas_aktif = function(id){
		$('.heshin').removeClass('box-link');
		$('#heshin-'+id).addClass('box-link');
	}

	data_buku_default();

	if (jQuery("#book-collections-tabs").length != '') {
		jQuery("#book-collections-tabs").tabs();
	}
</script>
