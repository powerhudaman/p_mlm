{extends file=$themes}
{block name="main-content"}
  <style media="screen">
    .shop-cart{
      margin: 0px 0px 20px;
      display: block;
    }
  </style>
  <!-- Keranjang begin -->
    <div class="shop-cart">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <br>
            <br>
            <br>
          </div>
        </div>
        <div class="row">
          <!-- sukses page begin -->
            <!-- title begin -->
            <div class="col-md-12" style="border-bottom:solid 1.5px #e8e5e5; margin-bottom:15px;">
                <h4 class="title-page">Hai {$pembeli['nama_depan']} {$pembeli['nama_belakang']}, Terimakasih telah berbelanja</h4>
                <p>Pesanan anda dengan No Invoice <span class="text-success" style="font-size:20px;">{$invoice_no}</span> Telah Kami Terima. </p>
            </div>
            <div class="col-md-12">
                <div class="col-md-8">
                    <h5>Jumlah Bayar</h5><br>
                    <h2 class="text-primary"> Rp. {number_format($total_tagihan_inv,0,",",".")} </h2><br><br>
                </div>
            </div>
            <!-- title end -->
            {if $tombol_konfirmasi !="" && $tombol_konfirmasi == "1"}
            <!-- metode pembayaran begin -->
            <div class="col-md-12" style="margin:10px 0px 20px 0px; border-bottom:solid 1.5px #e8e5e5;">
                <div class="">
                    <h3>Lakukan transfer bayar ke rekening berikut</h3>
                    <table width="100%">
                        <tbody>
                            <tr>
                                <th>Nama Bank</th>
                                <td>
                                    BCA
                                </td>
                            </tr>
                            <tr>
                                <th>No Rekening</th>
                                <td>{$pembayaran['no_rekening']}</td>
                            </tr>
                            <tr>
                                <th>Atas Nama </th>
                                <td>{$pembayaran['atas_nama']}</td>
                            </tr>
                            <tr>
                                <th>Untuk Melakukan Konfirmasi Pembayaran klik Link Berikut : </th>
                                <td>
                                    <a href="{site_url('cart_payment/Konfirmasi/index')}?invoice_no={$invoice_no}" class="btn btn-warning">Konfirmasi Pembayaran</a>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="text-center"><strong>Lakukan pembayaran sebelum tanggal : {$order_expired}/</strong></th>
                            </tr>
                        </tbody>
                    </table>
                    <br><br>
                </div>
            </div>
            <!-- metode pembayaran end -->
            {/if}
            <!-- langkah pembayaran begin -->
            {if $payment_guide !=""}
              <div class="col-md-12" style="margin:10px 0px 20px 0px; border-bottom:solid 1.5px #e8e5e5;">
                  <div class="">
                    <h3>Langkah Pembayaran</h3>
                      {$payment_guide}
                      <br><br>
                  </div>
              </div>
            {/if}
            <!-- langkah pembayaran end -->
            <div class="col-md-12">
                <div class="">
                    <h3>Ringkasan Order</h3><br>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="product-name">Nama Produk</th>
                                <th class="product-price">Harga</th>
                                <th class="product-quantity">Qty</th>
                                <th class="product-subtotal">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$carts key=k item=cart}
                                <tr class="cart_item">
                                    <td class="product-name" data-title="Product Name">
                                        <a href="{site_url('detail')}/{$cart['id']}">{$cart['name']}</a>
                                        <p>SKU: {$cart['bara']}
                                        </p>
                                        <ul>
                                        {if count($cart['options']) > 0}
                                            {foreach from=$cart['options'] key=a item=option}

                                              {if count($option['opt_value']) > 0}
                                                <li>{$option['nama']} :
                                                  {foreach from=$option['opt_value']  key=b item=opt_value }
                                                    {$opt_value['nama']}
                                                  {/foreach}
                                                </li>
                                              {/if}

                                            {/foreach}
                                        {/if}
                                        </ul>
                                        <p></p>
                                    </td>
                                    <td class="product-price" data-title="Unit Price">
                                        <span class="amount">Rp. {number_format($cart['price'],0,",",".")}</span>
                                    </td>
                                    <td class="product-quantity" data-title="Qty">
                                        <div class="quantity buttons_added">
                                            {$cart['qty']}
                                        </div>
                                    </td>
                                    <td class="product-subtotal" data-title="Subtotal">
                                        <span class="amount">Rp. {number_format($cart['subtotal'],0,",",".")}</span>
                                    </td>
                                </tr>
                            {/foreach}

                            {assign var=total_bayar value=0}
                              <tr class="cart-subtotal">
                                  <th colspan="3">Subtotal:</th>
                                  <td><span class="amount">Rp. {number_format($this->cart->total(),0,",",".")}</span></td>
                                  {$total_bayar = $total_bayar + $this->cart->total()}
                              </tr>
                            {if $total_potongan_reseller > 0}
                              <tr class="cart-subtotal">
                                  <th colspan="3">Total Diskon Reseller:</th>
                                  <td><span class="amount">Rp. {number_format($total_potongan_reseller,0,",",".")}</span></td>
                              </tr>
                            {/if}
                            {if isset($biaya_ongkir['kurir'])}
                              <tr class="cart-subtotal">
                                  <th colspan="3">Biaya Pengiriman : {$biaya_ongkir['kurir']} - {$biaya_ongkir['layanan']} Tujuan {if !$this->session->has_userdata('customer_shipping')}
                                    ({$penerima['kota']})
                                  {/if}
                                  {if $this->session->has_userdata('customer_shipping')}
                                   ({$penerima['shipping_city']})
                                 {/if}</th>
                                  <td><span class="amount">Rp. {number_format($biaya_ongkir['nominal'],0,",",".")} {$total_bayar = $total_bayar + $biaya_ongkir['nominal']}</span></td>
                              </tr>
                            {/if}
                            {if $kode_unik !=''}
                              <tr class="cart-subtotal">
                                  <th colspan="3">Kode Unik:</th>
                                  <td><span class="amount">Rp. {number_format($kode_unik,0,",",".")} {$total_bayar = $total_bayar + $kode_unik}</span></td>
                              </tr>
                            {/if}
                            {if count($promo) > 0}
                              {foreach from=$promo key=prom item=v_promo}
                                <tr class="cart-subtotal">
                                    <th colspan="3">{$v_promo['jenis_promo']} ({$v_promo['kode']}) :</th>
                                    <td><span class="amount">Rp. {$v_promo['nilai_potongan']} {$total_bayar = $total_bayar - $v_promo['nilai_potongan']}</span></td>
                                </tr>
                              {/foreach}
                            {/if}
                            <tr class="cart-subtotal">
                                <th colspan="3">Grandtotal:</th>
                                <td><span class="amount">Rp. {number_format($total_bayar,0,",",".")}</span></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-12" style="margin:20px 0px 20px 0px;">
                    <div class="col-md-12">
                      <div class="col-md-12">
                        <h3>Data Pembeli</h3>
                      </div>
                      <div class="col-md-11">
                        <div class="col-md-12">
                          <div class="col-md-2">
                            <strong>Nama Lengkap</strong>
                          </div>
                          <div class="col-md-10">: {$pembeli['nama_depan']} {$pembeli['nama_belakang']}</div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-2">
                            <strong>Kota</strong>
                          </div>
                          <div class="col-md-10">: {$pembeli['kota']}</div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-2">
                            <strong>Alamat</strong>
                          </div>
                          <div class="col-md-10">: {$pembeli['alamat_1']}</div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-2">
                            <strong>No Telepon</strong>
                          </div>
                          <div class="col-md-10">: {$pembeli['telephone']}</div>
                        </div>
                      </div>
                      <div class="col-md-1"></div>
                    </div>
                    <br>
            </div>

            <div class="col-md-12" style="margin:10px 0px 20px 0px;">

                    <div class="col-md-12">
                      <div class="col-md-12">
                        <h3>Data Penerima (<strong>Tujuan Pengiriman</strong>)</h3>
                      </div>
                      {if $this->session->has_userdata('customer_shipping')}
                        <div class="col-md-11">
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Nama Lengkap</strong>
                            </div>
                            <div class="col-md-10">: {$penerima['shipping_firstname']} {$penerima['shipping_lastname']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Kota</strong>
                            </div>
                            <div class="col-md-10">: {$penerima['shipping_city']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Alamat</strong>
                            </div>
                            <div class="col-md-10">: {$penerima['shipping_address_1']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>No Telepon</strong>
                            </div>
                            <div class="col-md-10">: {$penerima['shipping_telephone']}</div>
                          </div>
                        </div>
                        <div class="col-md-1"></div>
                      {/if}
                      {if !$this->session->has_userdata('customer_shipping')}
                        <div class="col-md-11">
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Nama Lengkap</strong>
                            </div>
                            <div class="col-md-10">: {$pembeli['nama_depan']} {$pembeli['nama_belakang']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Kota</strong>
                            </div>
                            <div class="col-md-10">: {$pembeli['kota']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Alamat</strong>
                            </div>
                            <div class="col-md-10">: {$pembeli['alamat_1']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>No Telepon</strong>
                            </div>
                            <div class="col-md-10">: {$pembeli['telephone']}</div>
                          </div>
                        </div>
                        <div class="col-md-1"></div>
                      {/if}
                    </div>
                    <br>
            </div>


          <!-- sukses page end -->
        </div>
        <div class="row">
          <div class="col-md-12">
            <br>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
  <!-- Keranjang end -->
{/block}
{block name="script_js"}
  {if !empty($url_socket)}
    <script type='text/javascript' src='{$url_socket}/socket.io/socket.io.js'></script>
    <script>
      // socket
      var socket = io.connect('{$url_socket}');
      // var form_1 = $('#form_1');
      var inv_no = "{$invoice_no}";
      var pesan = "{$pembeli['nama_depan']} {$pembeli['nama_belakang']} Berhasil Melakukan Order Baru";
      var user ="customer Melakukan Order";
      var order_id = "";
        // e.preventDefault();
        socket.emit('invoice',{ invoice:inv_no,pesan:pesan,user:user,order_id:order_id,status_invoice:'Baru' });
    </script>
  {/if}
{/block}
