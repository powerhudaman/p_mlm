{extends file=$themes}

{block name="main-content"}
<style media="screen">
  .shop-cart {
    margin: 0px 0px 20px;
    display: block;
  }
</style>
<!-- Keranjang begin -->
<div class="shop-cart">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <br>
        <br>
        <br>
        <h4>Checkout</h4>
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4"></div>
    </div>
    <div class="row">
      <div class="col-md-7" style=" min-height:150px;">
        <style media="screen">
          .panel-accordion {
            border: 1px solid #d3cfcf;
          }

          .panel-accordion-title {
            background-color: #827c7c;
            color: #fcfcfc;
          }
        </style>
        <!-- checkout begin -->
        {assign var=aturan_form value=['id'=>'form-customer','method'=>'post','data-toggle'=>'validator']}
        {form_open(site_url('cart_order/checkout/konfirmasi_order'),$aturan_form)}
          <div id="accor-page">
            <div class="panel-group" id="accordion">
              <!-- accordion 1 begin -->
              <div class="panel panel-accordion">

                <div class="panel-heading panel-accordion-title">
                  <!-- panel-heading -->
                  <h4 class="panel-title"> <!-- title 1 -->
                  <a data-toggle="collapse" data-parent="#accordion" href="#accordionOne">
                    1. Metode
                  </a>
                 </h4>
                </div>
                <!-- panel body -->
                <div id="accordionOne" class="panel-collapse collapse in">
                  <div class="panel-body">
                    {if !$this->session->has_userdata('member_id')}
                      <!-- metode login begin -->
                        <div class="col-md-12">
                        <div class="col-md-6">
                          <div class="radio">
                            <input type="radio" name="r_guest" id="r_guest_1" value="tamu"><span style="margin-left:10px;margin-top:10px;">Belanja Sebagai Tamu </span>
                          </div>
                          <div class="radio">
                            <input type="radio" name="r_guest" id="r_guest_2" value="daftar"><span style="margin-left:10px;margin-top:10px;">Registrasi Member</span>
                          </div>
                          <span class="pull-left" style="margin-top:20px;">
                          <a class="btn btn-warning" href="#accordiondua" onclick="check_toggle('accordiondua','accordionOne')">Lanjut</a>
                        </span>
                        </div>
                        <div class="col-md-6">
                            <h4>Login</h4><br>
                            <div class="form-group">
                              <label>Email Address</label>
                              <input type="email" name="email_login" id="email_login" placeholder="">
                              <input type="hidden" name="url_callback" id="url_callback" value="{site_url('cart_order/checkout/index')}">
                            </div>
                            <div class="form-group">
                              <label>Password</label>
                              <input type="password" name="password_login" id="password_login" placeholder="">
                            </div>
                            <span class="pull-left" style="margin-top:20px;">
                                <a class="btn btn-primary" href="#" onclick="frm_login()">Login</a>
                            </span>
                        </div>
                      </div>
                      <!-- metode login end -->
                    {/if}
                    {if $this->session->has_userdata('member_id')}
                      <!-- bila sudah login -->
                      <div class="col-md-12">
                          <div class="method-content-left">
                              <div class="color-333">Selamat Datang <strong>{$this->session->userdata('member_fullname')}</strong></div>
                              <a class="btn btn-warning pull-right" data-toggle="collapse" data-parent="#accordion" href="#accordiondua">Continue</a>
                          </div>
                      </div>
                      <!-- bila sudah login -->
                    {/if}

                  </div>
                </div>
              </div>
              <!-- accordion 1 end -->
              <!-- data pembeli begin -->
              <div class="panel panel-accordion">

                <div class="panel-heading panel-accordion-title">
                  <!-- panel-heading -->
                  <h4 class="panel-title"> <!-- title 1 -->
                      <a data-toggle="collapse" data-parent="#accordion" href="#accordiondua">
                        2. Informasi Data Pembeli
                      </a>
                     </h4>
                </div>
                <!-- panel body -->
                <div id="accordiondua" class="panel-collapse collapse">
                  <div class="panel-body">
                    <!-- form data pembeli begin -->
                    <!-- data pembeli begin -->
                    {if is_array($alamat) && count($alamat) > 0}
                      <div class="form-group">
                          <label for="">Pilih Alamat</label>
                          <select name="pilih_alamat_pembeli" id="pilih_alamat_pembeli" class="form-control">
                            <option value=""></option>
                            {foreach from=$alamat key=k item=v}
                              <option value="{$v['id']}">{$v['nama_depan']} - {word_limiter($v['alamat_1'],2)}</option>
                            {/foreach}
                          </select>
                      </div>
                    {/if}
                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" name="email" id="email" class="form-control" placeholder="Masukan Email Anda" value="{$pembeli['email']}" data-error="Masukan Alamat Email Dengan Benar">
                    </div>
                    <div class="form-group">
                      <label>Nama Depan <span class="red">*</span></label>
                      <input type="text" name="nama_depan" id="nama_depan" data-error="Nama Depan Tidak Boleh Kosong" class="form-control" placeholder="Masukan Nama Depan Anda" value="{$pembeli['nama_depan']}" required>
                    </div>
                    <div class="form-group">
                      <label>Nama Belakang</label>
                      <input type="text" name="nama_belakang" id="nama_belakang" class="form-control" placeholder="Masukan Nama Belakang Anda" value="{$pembeli['nama_belakang']}">
                    </div>
                    <div class="form-group">
                      <label>Alamat 1</label>
                      <textarea name="payment_address_1" id="payment_address_1" cols="30" rows="5" class="form-control">{strip_tags(htmlentities($pembeli['alamat_1']))}</textarea>
                    </div>
                    <div class="form-group" id="p_provinsi">
                      <label>Propinsi</label>
                      {if $prop_payment == 0}
                        <select name="propinsi" id="propinsi" class="form-control">
                              <option value=""></option>
                              {foreach from=$propinsi key=k item=v}

                                <option value="{$v['id']}" {$pilih}>{$v['provinsi']}</option>
                              {/foreach}
                        </select>
                      {/if}
                      {if $prop_payment > 0}
                        <select name="propinsi" id="propinsi" class="form-control">
                              {foreach from=$propinsi key=k item=v}
                                {assign var=pilih value=''}
                                {if $v['id'] == $prop_payment}
                                  {$pilih='selected'}
                                {/if}
                                <option value="{$v['id']}" {$pilih}>{$v['provinsi']}</option>
                              {/foreach}
                        </select>
                      {/if}
                    </div>
                    <div class="form-group" id="p_kota">
                      <label for="">Kota</label>
                      {if count($kota_payment) == 0}
                        <select name="kota" id="kota" class="form-control">
                              <option value=""></option>
                        </select>
                      {/if}
                      {if count($kota_payment) > 0}
                        <select name="kota" id="kota" class="form-control">
                              {foreach from=$kota_payment key=k item =kota}
                                {assign var=pilih value=''}
                                {if $kota['id'] == $kot_payment}
                                  {$pilih='selected'}
                                {/if}
                                <option value="{$kota['id']}" {$pilih}>{$kota['kota']}</option>
                              {/foreach}
                        </select>
                      {/if}
                    </div>
                    <div class="form-group" id="p_kecamatan">
                      <label for="">Kecamatan</label>
                      {if count($kecamatan_payment) == 0}
                        <select name="kecamatan" id="kecamatan" class="form-control">
                              <option value=""></option>
                        </select>
                      {/if}
                      {if count($kecamatan_payment) > 0}
                        <select name="kecamatan" id="kecamatan" class="form-control">
                          {foreach from=$kecamatan_payment key=k item =kecamatan}
                            {assign var=pilih value=''}
                            {if $kecamatan['id'] == $kec_payment}
                              {$pilih='selected'}
                            {/if}
                            <option value="{$kecamatan['id']}" {$pilih}>{$kecamatan['kecamatan']}</option>
                          {/foreach}
                        </select>
                      {/if}
                      <input type="hidden" name="payment_city" id="payment_city" value="{$nama_kota_payment}">

                    </div>
                    <div class="form-group">
                      <label for="">Kodepos</label>
                      <input type="text" name="payment_postcode" id="payment_postcode"  class="form-control" placeholder="Masukan Kode Pos Kota Anda" value="{$pembeli['kodepos']}">
                    </div>
                    <div class="form-group">
                      <label for="">No Telp</label>
                      <input type="text" name="payment_telephone" id="payment_telephone"  class="form-control" placeholder="Masukan No Telp Anda" value="{$pembeli['telephone']}">
                    </div>
                    <div class="form-group">
                      {if $this->session->has_userdata('member_id')}
                      <label for=""><input type="checkbox" name="simpan_alamat_pembeli" id="simpan_alamat_pembeli" class="" value="1" {if $this->session->has_userdata('simpan_alamat_pembeli')} checked {/if}> Simpan alamat</label>
                      {/if}
                    </div>
                    <div class="form-group">
                      <a href="#check-2" class="btn btn-primary pull-left" id="ubah_data_payment" onclick="activate_data_customer_sementara_payment()">Ubah Data Pembeli</a>
                      <a href="#check-2" class="btn btn-success pull-left" id="save_data_payment" onclick="data_customer_sementara_payment()">Simpan Data Pembeli</a>
                      <a data-toggle="collapse" data-parent="#accordion" href="#accordionTiga" onclick="continue_alamat('accordionTiga','accordiondua')" class="btn btn-warning pull-right" id="langkah-2">Langkah Selanjutnya</a>
                    </div>
                    <!-- data pembeli end -->
                    <!-- form data pembeli end -->
                  </div>
                </div>
              </div>
              <!-- data pembeli end -->
              <!-- data penerima begin -->
              <div class="panel panel-accordion">

                <div class="panel-heading panel-accordion-title">
                  <!-- panel-heading -->
                  <h4 class="panel-title"> <!-- title 3 -->
                    <a data-toggle="collapse" data-parent="#accordion" href="#accordionTiga">
                      3. Informasi Data Pengiriman
                    </a>
                  </h4>
                </div>
                <!-- panel body -->
                <div id="accordionTiga" class="panel-collapse collapse">
                  <div class="panel-body">
                    <!-- form data pembeli begin -->
                    <!-- data penerima begin -->
                    <div class="form-group">
                      <input type="checkbox" name="pengiriman_beda" id="pengiriman_beda" class="" {if $this->session->has_userdata('customer_shipping')} checked {/if}> Alamat pengiriman berbeda Dengan pembeli
                    </div>
                    {if $this->session->has_userdata('reseller') && $dropship_status == 1}
                    <div class="form-group">

                      <label for=""><input type="checkbox" name="dropship" id="dropship" class="" value="1" {if $this->session->has_userdata('dropship')} checked {/if}> Pengiriman Dropship</label>

                    </div>
                    {/if}
                    <div id="data-penerima">
                      {if is_array($alamat) && count($alamat) > 0}
                        <div class="form-group">
                            <label for="">Pilih Alamat</label>
                            <select name="pilih_alamat_shipping" id="pilih_alamat_shipping" class="form-control">
                              <option value=""></option>
                              {foreach from=$alamat key=k item=v}
                                <option value="{$v['id']}">{$v['nama_depan']} - {word_limiter($v['alamat_1'],2)}</option>
                              {/foreach}
                            </select>
                        </div>
                      {/if}
                      <div class="form-group">
                        <label>Nama Depan <span class="red">*</span></label>
                        <input type="text" name="shipping_firstname" id="shipping_firstname" class="form-control" placeholder="Masukan Nama Depan Penerima" value="{$penerima['shipping_firstname']}">
                      </div>
                      <div class="form-group">
                        <label>Nama Belakang</label>
                        <input type="text" name="shipping_lastname" id="shipping_lastname" class="form-control" placeholder="Masukan Nama Belakang Penerima" value="{$penerima['shipping_lastname']}">
                      </div>
                      <div class="form-group">
                        <label>Alamat 1</label>
                        <textarea name="shipping_address_1" id="shipping_address_1" cols="30" rows="5" class="form-control">{strip_tags(htmlentities($penerima['shipping_address_1']))}</textarea>
                      </div>
                      <div class="form-group" id="s_provinsi">
                        <label>Propinsi</label>
                        {if $prop_shipping == 0}
                          <select name="propinsi_shipping" id="propinsi_shipping" class="form-control">
                                <option value=""></option>
                                {foreach from=$propinsi key=k item=v}
                                  <option value="{$v['id']}" {$pilih}>{$v['provinsi']}</option>
                                {/foreach}
                          </select>
                        {/if}
                        {if $prop_shipping > 0}
                          <select name="propinsi_shipping" id="propinsi_shipping" class="form-control">
                            {foreach from=$propinsi key=k item=v}
                              {assign var=pilih value=''}
                              {if $v['id'] == $prop_shipping}
                                {$pilih='selected'}
                              {/if}
                              <option value="{$v['id']}" {$pilih}>{$v['provinsi']}</option>
                            {/foreach}
                          </select>
                        {/if}
                      </div>
                      <div class="form-group" id="s_kota">
                        <label for="">Kota</label>
                        {if count($kota_shipping) == 0}
                          <select name="kota_shipping" id="kota_shipping" class="form-control">
                                <option value=""></option>
                          </select>
                        {/if}
                        {if count($kota_shipping) > 0}
                          <select name="kota_shipping" id="kota_shipping" class="form-control">
                            {foreach from=$kota_shipping key=k item =kota}
                              {assign var=pilih value=''}
                              {if $kota['id'] == $kot_shipping}
                                {$pilih='selected'}
                              {/if}
                              <option value="{$kota['id']}" {$pilih}>{$kota['kota']}</option>
                            {/foreach}
                          </select>
                        {/if}
                      </div>
                      <div class="form-group" id="s_kecamatan">
                        <label for="">Kecamatan</label>
                        {if count($kecamatan_shipping) == 0}
                        <select name="kecamatan_shipping" id="kecamatan_shipping" class="form-control">
                              <option value=""></option>
                        </select>
                        {/if}
                        {if count($kecamatan_shipping) > 0}
                          <select name="kecamatan_shipping" id="kecamatan_shipping" class="form-control">
                            {foreach from=$kecamatan_shipping key=k item =kecamatan}
                              {assign var=pilih value=''}
                              {if $kecamatan['id'] == $kec_shipping}
                                {$pilih='selected'}
                              {/if}
                              <option value="{$kecamatan['id']}" {$pilih}>{$kecamatan['kecamatan']}</option>
                            {/foreach}
                          </select>
                        {/if}
                        <input type="hidden" name="shipping_city" id="shipping_city" value="{$nama_kota_shipping}">

                      </div>
                      <div class="form-group">
                        <label for="">Kodepos</label>
                        <input type="text" name="shipping_postcode" id="shipping_postcode"  class="form-control" placeholder="Masukan Kode Pos Kota Penerima" value="{$penerima['shipping_postcode']}">
                      </div>
                      <div class="form-group">
                        <label for="">No Telp</label>
                        <input type="text" name="shipping_telephone" id="shipping_telephone"  class="form-control" placeholder="Masukan No Telp Penerima" value="{$penerima['shipping_telephone']}">
                      </div>
                    </div>
                    <div class="form-group">
                      {if $this->session->has_userdata('member_id')}
                      <label for=""><input type="checkbox" name="simpan_alamat" id="simpan_alamat" class="" value="1" {if $this->session->has_userdata('simpan_alamat_pembeli')} checked {/if}> Simpan alamat pengiriman</label>
                      {/if}
                    </div>

                    <div class="form-group">
                      <a href="#accordionTiga" class="btn btn-primary pull-left" id="ubah_data_shipping" onclick="activate_data_customer_sementara_shipping()">Ubah Data Penerima</a>
                      <a href="#accordionTiga" class="btn btn-success pull-left" id="save_data_shipping" onclick="data_customer_sementara_shipping()">Simpan Data Penerima</a>
                      <a data-toggle="collapse" data-parent="#accordion" href="#accordionEmpat" onclick="continue_alamat('accordionEmpat','accordionTiga')" class="btn btn-warning pull-right" id="langkah-3">Langkah Selanjutnya</a>
                    </div>
                    <!-- data penerima end -->
                    <!-- form data pembeli end -->
                  </div>
                </div>
              </div>
              <!-- data penerima end -->
              <!-- metode pengiriman begin -->
              <div class="panel panel-accordion">

                <div class="panel-heading panel-accordion-title">
                  <!-- panel-heading -->
                  <h4 class="panel-title"> <!-- title 1 -->
                    <a data-toggle="collapse" data-parent="#accordion" href="#accordionEmpat">
                      4. Metode Pengiriman
                    </a>
                   </h4>
                </div>
                <!-- panel body -->
                <div id="accordionEmpat" class="panel-collapse collapse">
                  <div class="panel-body">
                    <!-- metode Pengiriman begin -->
                    <div class="method-content-left-bottom">

                      <ul class="nav nav-tabs">
                        {assign var=no value='0'}
                        {foreach from=$list_kurir key=k item=menu}
                          {$no = $no + 1}
                          <li class="{if $no == '1'} active {/if}"> <a data-toggle="tab" href="#{$menu['nama']}{$k}">{$menu['nama_pengiriman']}</a></li>
                        {/foreach}
                      </ul>

                      <div class="tab-content">
                        {foreach from=$list_kurir key=k item=htm}
                          <div id="{$htm['nama']}{$k}" class="tab-pane fade in">
                            <br>
                            <!-- <h3>{$htm['nama_pengiriman']}</h3> -->
                            <br>
                            {$htm['html']}
                          </div>
                        {/foreach}
                      </div>

                      <div class="form-group">
                        {if $skip_langkah_pembayaran == '0'}
                          <!-- <a class="btn btn-warning pull-right" data-toggle="collapse" data-parent="#accordion" href="#accordionLima" onclick="check_toggle('accordionLima','accordionEmpat')" id="btn-check-4">Langkah Selanjutnya</a> -->
                          <a class="btn btn-warning pull-right"  href="{site_url('checkout/order_preview')}"  id="btn-check-4">Langkah Selanjutnya</a>
                        {/if}
                        {if $skip_langkah_pembayaran == '1'}
                          <!-- <a href="#" class="btn btn-warning pull-right" onclick="verifikasi_alamat()">Konfirmasi Order</a> -->
                          <a href="{site_url('checkout/order_preview')}" class="btn btn-warning pull-right">Konfirmasi Order</a>
                        {/if}
                      </div>
                    </div>
                    <!-- metode Pengiriman end -->
                  </div>
                </div>
              </div>
              <!-- metode pengiriman end -->
              {if $skip_langkah_pembayaran =='0'}
              <!-- metode pembayaran begin
              <div class="panel panel-accordion">

                <div class="panel-heading panel-accordion-title">

                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#accordionLima">
                      5. Metode Pembayaran
                    </a>
                   </h4>
                </div>

                <div id="accordionLima" class="panel-collapse collapse">
                  <div class="panel-body">


                      <ul class="nav nav-tabs">
                        {assign var=no value='0'}
                        {foreach from=$list_pembayaran key=k item=menu}
                          {$no = $no + 1}
                          <li class="{if $no == '1'} active {/if}" ><a data-toggle="tab" href="#{$menu['nama']}{$k}"  id="link_tab_payment{$no}" data-tabe="{$menu['nama']}{$k}">{$menu['nama_pembayaran']}</a></li>
                        {/foreach}
                      </ul>

                      <div class="tab-content">
                        {$no =0}
                        {foreach from=$list_pembayaran key=k item=htm}
                          {$no = $no + 1}
                          <div id="{$htm['nama']}{$k}" class="tab-pane fade in {if $no == '1'}active{/if}">
                            <br>

                            <br>
                            {$htm['html']}
                          </div>
                        {/foreach}
                      </div>

                      <div class="form-group">
                        <!-- <a href="#" class="btn btn-warning pull-right" onclick="verifikasi_alamat()">Konfirmasi Order</a>
                        <a href="{site_url('checkout/order_preview')}" class="btn btn-warning pull-right">Konfirmasi Order</a>
                      </div>


                  </div>
                </div>
              </div>
               metode pembayaran end -->
              {/if}
            </div>
          </div>
        {form_close()}
        <!-- checkout end -->
      </div>
      <div class="col-md-5" style=" min-height:150px;">
        <table id="rincian_pesanan">

        </table>
      </div>
    </div>
  </div>
</div>
<!-- Keranjang end -->

<!-- Modal Konfirmasi Order -->
<div id="modal-konfirmasi-order" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Verifikasi Alamat Tujuan Pengiriman</h4>
      </div>
      <div class="modal-body" style="min-height:350px">
      	<br>
        <div id="mko-body">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" onclick="konfirmasi_order()">Konfirmasi Order</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal Konfirmasi Order -->

{/block}

{block name=script_js}
  <script>
  var frm_login = function(){
    // $('#frm_login')[0].submit(function(event) {
    //   /* Act on the event */
    // });
    var url ="{site_url('customer/customer/login_proses_js')}";
    var url_callback = "{site_url('cart_order/checkout/index')}";
    var email_login = $('#email_login').val();
    var password_login = $('#password_login').val();
    $.post(url, { email:email_login,password:password_login,url_callback:url_callback }, function(data, textStatus, xhr) {
      /*optional stuff to do after success */
        window.location.reload();
    });
  }
  // konfirmasi order
  var konfirmasi_order = function(){
    $('#form-customer')[0].submit(function() {
      /* Act on the event */
    });
  }
  //check out shipping
  var check_toggle = function(id,id_lama=''){
    if($("#r_guest_2").is(":checked")){
      // alert('di pilih redgister');
      window.location.replace("{site_url('customer/login')}");
    }
    // alert('togel jalan');
    var buka = $('#'+id);
    if(id_lama !=""){
      var tutup = $('#'+id_lama);
      tutup.collapse('hide');

    }
    buka.collapse('show');
    // coba.find('.checkout-toggle').fadeToggle( '3000', 'linear', '10000');
    // coba.collapse();
  }

  // fungsi ini digunakan untuk melakukan continue tab dan mengirimkan data alamat member
  var continue_alamat = function(id,id_lama){
    data_customer_sementara();

    //
    if(id =="accordionTiga"){
      data_customer_sementara_payment();
    }
    if(id =="accordionEmpat"){
      data_customer_sementara_shipping();
    }
    check_toggle(id,id_lama);
  }

    jQuery(document).ready(function($) {
        $('#data-penerima').hide();
        if($('#pengiriman_beda').prop('checked') == true){
          $('#data-penerima').show();
        }
        $("#form-password").hide();



        $("#form-password-konfirm").hide();

        $('#ubah_data_payment').hide();
        $('#save_data_payment').hide();
        $('#ubah_data_shipping').hide();
        $('#save_data_shipping').hide();


        {if $this->session->has_userdata('customer_payment')}
          disable_data_customer_sementara_payment();
        {/if}

        {if $this->session->has_userdata('customer_shipping')}
          disable_data_customer_sementara_shipping();
        {/if}

        // id_propinsi,id_kota,id_kecamatan
        {if $pembeli['id_propinsi'] !="" && $pembeli['id_kota'] !="" && $pembeli['id_kecamatan'] !=""}
          eksekusi_ongkir();
        {/if}

        ringkasan_order();

        setTimeout(function() {
          // $("#link_tab_payment").click();
          // if($('#link_tab_payment2').attr('data-tabe') !== undefined ){
            var data_tab = $('#link_tab_payment2').attr('data-tabe');
            if(data_tab !== undefined || null){
              $('.nav-tabs a[href="#'+data_tab+'"]').tab('show');
            }
            //  $("#link_tab_payment").trigger('click');
          // }

       },10);

    });
      // $('#link_tab_payment').trigger('click');
    $('#link_tab_payment').on("click",".title",function(){
      alert('sdfdsf');
    });
    $('#pengiriman_beda').click(function(event) {

      if($('#pengiriman_beda').prop( "checked" )){
          activate_data_customer_sementara_shipping();
          $('#data-penerima').show();
      }else{
        $('#data-penerima').hide();
        $.ajax({
          url: "{site_url('cart_order/checkout/hapus_customer_shipping')}",
          type: 'GET',
          dataType: 'html',
          success:function(data){
            $("#shipping_firstname").val('');
            $("#shipping_lastname").val('');
            $("#shipping_company").val('');
            $("#shipping_address_1").val('');
            $("#shipping_address_2").val('');
            $("#shipping_city").val('');
            $("#shipping_postcode").val('');
            $("#shipping_telephone").val('');

            //generate tujuan pengiriman
            $("#propinsi_shipping").val('');
            $("#kota_shipping").html('');
            $("#kecamatan_shipping").html('');
            $('#ubah_data_shipping').hide();
          }
        });

      }
    });

    $('#daftar_member').click(function(event) {

      if($('#daftar_member').prop( "checked" )){
        $("#form-password").show();
        $("#form-password-konfirm").show();
      }else{
        $("#form-password").hide();
        $("#form-password-konfirm").hide();
      }
    });

    var alamat_member = {json_encode($alamat)};
    // pilih alamat pembeli
    $('#pilih_alamat_pembeli').change(function(event) {
      /* Act on the event */
      var pilih_id = $('#pilih_alamat_pembeli').val();
      var data_alamat = findId(alamat_member,pilih_id);

      /*
      id="propinsi_shipping"
      id="kota_shipping"
      id="kecamatan_shipping"
       */

      /// input alamat shipping
      $("#nama_depan").val(data_alamat.nama_depan);
      $("#nama_belakang").val(data_alamat.nama_belakang);
      // $("#shipping_company").val(data_alamat.company);
      $("#payment_address_1").val(data_alamat.alamat_1);
      // $("#shipping_address_2").val(data_alamat.alamat_2);
      $("#payment_city").val(data_alamat.kota);
      $("#payment_postcode").val(data_alamat.kodepos);
      $("#payment_telephone").val(data_alamat.telephone);

      //generate tujuan pengiriman
      $("#propinsi").val(data_alamat.id_propinsi);
      $("#kota").html('');
      $("#kecamatan").html('');

      // get kota
      $.ajax({
        url: "{site_url('cart_order/checkout/kota_html')}",
        type: 'POST',
        dataType: 'html',
        data: { propinsi_shipping: data_alamat.id_propinsi, kota_shipping: data_alamat.id_kota  },
        success:function(response){
          $("#kota").html(response);

          $.ajax({
            url: "{site_url('cart_order/checkout/kecamatan_html')}",
            type: 'POST',
            dataType: 'html',
            data: { propinsi_shipping: data_alamat.id_propinsi, kota_shipping: data_alamat.id_kota, kecamatan_shipping: data_alamat.id_kecamatan  },
            success:function(response_kecamatan){
              $("#kecamatan").html(response_kecamatan);
              // untuk menghitung biaya ongkir otomatis
              var kota = data_alamat.id_kota;
              var kecamatan = data_alamat.id_kecamatan;
              {foreach from=$list_kurir key=k item=i}
                    {$i['biaya_ongkir']};
              {/foreach}

            }
          });

        }
      });

    });
    // pilih alamat pembeli
    // pilih alamat pengiriman
    $('#pilih_alamat_shipping').change(function(event) {
      /* Act on the event */
      var pilih_id = $('#pilih_alamat_shipping').val();
      var data_alamat = findId(alamat_member,pilih_id);

      /*
      id="propinsi_shipping"
      id="kota_shipping"
      id="kecamatan_shipping"
       */

      /// input alamat shipping
      $("#shipping_firstname").val(data_alamat.nama_depan);
      $("#shipping_lastname").val(data_alamat.nama_belakang);
      $("#shipping_company").val(data_alamat.company);
      $("#shipping_address_1").val(data_alamat.alamat_1);
      $("#shipping_address_2").val(data_alamat.alamat_2);
      $("#shipping_city").val(data_alamat.kota);
      $("#shipping_postcode").val(data_alamat.kodepos);
      $("#shipping_telephone").val(data_alamat.telephone);

      //generate tujuan pengiriman
      $("#propinsi_shipping").val(data_alamat.id_propinsi);
      $("#kota_shipping").html('');
      $("#kecamatan_shipping").html('');

      // get kota
      $.ajax({
        url: "{site_url('cart_order/checkout/kota_html')}",
        type: 'POST',
        dataType: 'html',
        data: { propinsi_shipping: data_alamat.id_propinsi, kota_shipping: data_alamat.id_kota  },
        success:function(response){
          $("#kota_shipping").html(response);

          $.ajax({
            url: "{site_url('cart_order/checkout/kecamatan_html')}",
            type: 'POST',
            dataType: 'html',
            data: { propinsi_shipping: data_alamat.id_propinsi, kota_shipping: data_alamat.id_kota, kecamatan_shipping: data_alamat.id_kecamatan  },
            success:function(response_kecamatan){
              $("#kecamatan_shipping").html(response_kecamatan);
              // untuk menghitung biaya ongkir otomatis
              var kota = data_alamat.id_kota;
              var kecamatan = data_alamat.id_kecamatan;
              {foreach from=$list_kurir key=k item=i}
                    {$i['biaya_ongkir']};
              {/foreach}

            }
          });

        }
      });

    });
    //pilih alamat pengiriman

    $('#simpan_alamat').on('click', function(event) {
      var nilai = 0;
      if($('#simpan_alamat').prop('checked') == false){
        nilai = 0;
      }
      if($('#simpan_alamat').prop('checked') == true){
        nilai = 1;
      }
      $.post("{site_url('cart_order/checkout/sess_simpan_alamat_pengirim')}",{ nilai_pengirim: nilai }, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
      });
    });

    $('#simpan_alamat_pembeli').on('click', function(event) {
      var nilai_pembeli = 0;

        if($('#simpan_alamat_pembeli').prop('checked') == false){
          nilai_pembeli = 0;
        }
        if($('#simpan_alamat_pembeli').prop('checked') == true){
          nilai_pembeli = 1;
        }


      $.post("{site_url('cart_order/checkout/sess_simpan_alamat_pembeli')}",{ nilai_pembeli: nilai_pembeli }, function(data, textStatus, xhr) {

      });
    });

    $('#dropship').on('click', function(event) {
      var nilai_pembeli = 0;

        if($('#dropship').prop('checked') == false){
          nilai_pembeli = 0;
        }
        if($('#dropship').prop('checked') == true){
          nilai_pembeli = 1;
        }


      $.post("{site_url('cart_order/checkout/sess_dropship')}",{ dropship: nilai_pembeli }, function(data, textStatus, xhr) {

      });
    });

    function findId(data, idToLookFor) {
        var categoryArray = data;
        for (var i = 0; i < categoryArray.length; i++) {
            if (categoryArray[i].id == idToLookFor) {
                return(categoryArray[i]);
            }
        }
    }

    /**
     * [eksekusi_ongkir fungsi ini digunakan untuk mengekeskui menghitung ongkir secara otomatis ini akan digunakan bila data member sudah ada alamat default]
     * @return {[type]} [description]
     */
    var eksekusi_ongkir = function(){

      var propinsi = "{$pembeli['id_propinsi']}";
      var kota = "{$pembeli['id_kota']}";
      var kecamatan = "{$pembeli['id_kecamatan']}";
      {foreach from=$list_kurir key=k item=i}
            {$i['biaya_ongkir']};
      {/foreach}
    }

    var data_customer_sementara = function(){
      var email=$("#email").val();
      var nama_depan=$("#nama_depan").val();
      var nama_belakang=$("#nama_belakang").val();
      var payment_company=$("#payment_company").val();
      var payment_address_1=$("#payment_address_1").val();
      var payment_address_2=$("#payment_address_2").val();
      var payment_city=$("#payment_city").val();
      var payment_postcode=$("#payment_postcode").val();
      var payment_telephone=$("#payment_telephone").val();

      var propinsi=$("#propinsi").val();
      var kota=$("#kota").val();
      var kecamatan=$("#kecamatan").val();


      // var =$("#pilih_alamat_shipping").val();
      var shipping_firstname=$("#shipping_firstname").val();
      var shipping_lastname=$("#shipping_lastname").val();
      var shipping_company=$("#shipping_company").val();
      var shipping_address_1=$("#shipping_address_1").val();
      var shipping_address_2=$("#shipping_address_2").val();
      var shipping_city=$("#shipping_city").val();
      var shipping_postcode=$("#shipping_postcode").val();
      var shipping_telephone=$("#shipping_telephone").val();

      var propinsi_shipping=$("#propinsi_shipping").val();
      var kota_shipping=$("#kota_shipping").val();
      var kecamatan_shipping=$("#kecamatan_shipping").val();

      var post_data = {
        'email':email,
        'nama_depan':nama_depan,
        'nama_belakang':nama_belakang,
        'payment_company':payment_company,
        'payment_address_1':payment_address_1,
        'payment_address_2':payment_address_2,
        'payment_city':payment_city,
        'payment_postcode':payment_postcode,
        'payment_telephone':payment_telephone,
        'propinsi':propinsi,
        'kota':kota,
        'kecamatan':kecamatan,
        'shipping_firstname':shipping_firstname,
        'shipping_lastname':shipping_lastname,
        'shipping_company':shipping_company,
        'shipping_address_1':shipping_address_1,
        'shipping_address_2':shipping_address_2,
        'shipping_city':shipping_city,
        'shipping_postcode':shipping_postcode,
        'shipping_telephone':shipping_telephone,
        'propinsi_shipping':propinsi_shipping,
        'kota_shipping':kota_shipping,
        'kecamatan_shipping':kecamatan_shipping
      };

      $.post('{site_url("cart_order/checkout/data_customer_sementara")}', { POST: post_data }, function(data, textStatus, xhr) {});

    }

    // proses modifikasi tujuan pengiriman
    var data_customer_sementara_payment = function(){
        var email=$("#email").val();
        var nama_depan=$("#nama_depan").val();
        var nama_belakang=$("#nama_belakang").val();
        var payment_company=$("#payment_company").val();
        var payment_address_1=$("#payment_address_1").val();
        var payment_address_2=$("#payment_address_2").val();
        var payment_city=$("#payment_city").val();
        var payment_postcode=$("#payment_postcode").val();
        var payment_telephone=$("#payment_telephone").val();

        var propinsi=$("#propinsi").val();
        var kota=$("#kota").val();
        var kecamatan=$("#kecamatan").val();

        var post_data = {
          'email':email,
          'nama_depan':nama_depan,
          'nama_belakang':nama_belakang,
          'payment_company':payment_company,
          'payment_address_1':payment_address_1,
          'payment_address_2':payment_address_2,
          'payment_city':payment_city,
          'payment_postcode':payment_postcode,
          'payment_telephone':payment_telephone,
          'propinsi':propinsi,
          'kota':kota,
          'kecamatan':kecamatan
        };

      $.post('{site_url("cart_order/checkout/data_customer_sementara_payment")}', { POST: post_data }, function(data, textStatus, xhr) {
        disable_data_customer_sementara_payment();
        $('#save_data_payment').hide();
        $('#langkah-2').show();
      });
    }

    var disable_data_customer_sementara_payment = function(){
        // data-toggle="tooltip" data-placement="top" title="Hooray!"
        $("#payment_address_1").attr('data-toggle','tooltip');
        $("#payment_address_1").attr('data-placement','top');
        $("#payment_address_1").attr('title','Bilah Ingin Melakukan Perubahan alamat silahkan klik tombol ubah');

        $("#email").prop('readonly',true);
        $("#nama_depan").prop('readonly',true);
        $("#nama_belakang").prop('readonly',true);
        $("#payment_company").prop('readonly',true);
        $("#payment_address_1").prop('readonly',true);
        $("#payment_address_2").prop('readonly',true);
        $("#payment_city").prop('readonly',true);
        $("#payment_postcode").prop('readonly',true);
        $("#payment_telephone").prop('readonly',true);
        $("#propinsi").prop('readonly',true);
        $("#kota").prop('readonly',true);
        $("#kecamatan").prop('readonly',true);

        $('#p_provinsi').hide();
        $('#p_kota').hide();
        $('#p_kecamatan').hide();

        $('#ubah_data_payment').show();

    }

    var activate_data_customer_sementara_payment = function(){
        $("#email").prop('readonly',false);
        $("#nama_depan").prop('readonly',false);
        $("#nama_belakang").prop('readonly',false);
        $("#payment_company").prop('readonly',false);
        $("#payment_address_1").prop('readonly',false);
        $("#payment_address_2").prop('readonly',false);
        $("#payment_city").prop('readonly',false);
        $("#payment_postcode").prop('readonly',false);
        $("#payment_telephone").prop('readonly',false);

        $('#p_provinsi').show();
        $('#p_kota').show();
        $('#p_kecamatan').show();

        $('#ubah_data_payment').hide();
        $('#save_data_payment').show();
        $('#langkah-2').hide();

    }

    var data_customer_sementara_shipping = function(){

      // var =$("#pilih_alamat_shipping").val();
      var shipping_firstname=$("#shipping_firstname").val();
      var shipping_lastname=$("#shipping_lastname").val();
      var shipping_company=$("#shipping_company").val();
      var shipping_address_1=$("#shipping_address_1").val();
      var shipping_address_2=$("#shipping_address_2").val();
      var shipping_city=$("#shipping_city").val();
      var shipping_postcode=$("#shipping_postcode").val();
      var shipping_telephone=$("#shipping_telephone").val();

      var propinsi_shipping=$("#propinsi_shipping").val();
      var kota_shipping=$("#kota_shipping").val();
      var kecamatan_shipping=$("#kecamatan_shipping").val();

      var post_data = {
        'shipping_firstname':shipping_firstname,
        'shipping_lastname':shipping_lastname,
        'shipping_company':shipping_company,
        'shipping_address_1':shipping_address_1,
        'shipping_address_2':shipping_address_2,
        'shipping_city':shipping_city,
        'shipping_postcode':shipping_postcode,
        'shipping_telephone':shipping_telephone,
        'propinsi_shipping':propinsi_shipping,
        'kota_shipping':kota_shipping,
        'kecamatan_shipping':kecamatan_shipping
      };
      if(shipping_firstname !="" && kecamatan_shipping !=""){
        $.post('{site_url("cart_order/checkout/data_customer_sementara_shipping")}', { POST: post_data }, function(data, textStatus, xhr) {
          disable_data_customer_sementara_shipping();
          $('#save_data_shipping').hide();
          $('#langkah-3').show();
      });
      }

    }

    var disable_data_customer_sementara_shipping = function(){
        $("#shipping_address_1").attr('data-toggle','tooltip');
        $("#shipping_address_1").attr('data-placement','top');
        $("#shipping_address_1").attr('title','Bilah Ingin Melakukan Perubahan alamat silahkan klik tombol ubah');

        $("#shipping_firstname").prop('readonly',true);
        $("#shipping_lastname").prop('readonly',true);
        $("#shipping_company").prop('readonly',true);
        $("#shipping_address_1").prop('readonly',true);
        $("#shipping_address_2").prop('readonly',true);
        $("#shipping_city").prop('readonly',true);
        $("#shipping_postcode").prop('readonly',true);
        $("#shipping_telephone").prop('readonly',true);

        $('#s_provinsi').hide();
        $('#s_kota').hide();
        $('#s_kecamatan').hide();

        $('#ubah_data_shipping').show();

    }

    var activate_data_customer_sementara_shipping = function(){
       $("#shipping_firstname").prop('readonly',false);
        $("#shipping_lastname").prop('readonly',false);
        $("#shipping_company").prop('readonly',false);
        $("#shipping_address_1").prop('readonly',false);
        $("#shipping_address_2").prop('readonly',false);
        $("#shipping_city").prop('readonly',false);
        $("#shipping_postcode").prop('readonly',false);
        $("#shipping_telephone").prop('readonly',false);

        $('#s_provinsi').show();
        $('#s_kota').show();
        $('#s_kecamatan').show();

        $('#ubah_data_shipping').hide();
        $('#save_data_shipping').show();
        $('#langkah-2').hide();

    }
    // proses modifikasi tujuan pengiriman

    // pemilihan kota
    $("#propinsi").change(function() {
        // $("#kecamatan_tujuan").hide();
        $("#kota").hide();
        $("#kecamatan").hide();

        var propinsi_origin = $("#propinsi").val();
        var urls = '{site_url("cart_order/checkout/kota")}';
        $.ajax({
                //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
                url: urls + '?propinsi=' + propinsi_origin,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    var html = '<option value=""</option>';
                    $.each(data, function(k, v) {

                        html += '<option value="' + v.id + '">' + v.nama_kota + '</option>';
                    });

                    $("#kota").html(html);
                    $("#kota").show();
                    $('#btn-check-4').hide();
                }
            });
    })

    // kecamatan tujuan
    $("#kota").change(function() {
        $("#kecamatan").hide();
        {if $this->session->has_userdata('kurir') == true}
          // hapus_kurir();
        {/if}
        var kota_origin = $("#kota").val();
        var urls = '{site_url("cart_order/checkout/kecamatan")}';
        $.ajax({
                //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
                url: urls + '?kota=' + kota_origin,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    var html = '<option value=""</option>';
                    $.each(data, function(k, v) {
                        html += '<option value="' + v.id + '">' + v.nama_kecamatan + '</option>';
                    });

                    $("#kecamatan").html(html);
                    $("#kecamatan").show();
                    $('#btn-check-4').hide();
                }
            });
    });

    // pilih tujuan
    $("#kecamatan").change(function(){

        var propinsi_origin = $("#propinsi").val();
        var kota_origin = $("#kota").val();
        var kecamatan = $('#kecamatan').val();
        var urls = '{site_url("cart_order/checkout/kecamatan_detail_indonesia")}';

        $.post(urls, { kecamatan: kecamatan }, function(data, textStatus, xhr) {
          /*optional stuff to do after success */
          data = jQuery.parseJSON(data);
          $('#payment_city').val(data.nama_kecamatan);
          $('#payment_postcode').val(data.kode_pos);

          // cek apkah memiliki tujuan pengiriman yang berbeda atau tidak bila berbeda maka ongkir akan dihitung berdasarkan tujuan pengiriman berbeda.
          $.getJSON('{site_url("cart_order/checkout/cek_customer_shipping")}', { }, function(json, textStatus) {
              if(json.kode == "1"){
                kecamatan = json.data.id_kecamatan;
              }
              {foreach from=$list_kurir key=k item=i}
                    {$i['biaya_ongkir']};
              {/foreach}
          });
        });

    });

    // skrip ini untuk shipping //
    // pemilihan kota
    $("#propinsi_shipping").change(function() {
        // $("#kecamatan_tujuan").hide();
        $("#kota_shipping").hide();
        $("#kecamatan_shipping").hide();

        var propinsi_origin = $("#propinsi_shipping").val();
        var urls = '{site_url("cart_order/checkout/kota")}';
        $.ajax({
                //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
                url: urls + '?propinsi=' + propinsi_origin,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    var html = '<option value=""</option>';
                    $.each(data, function(k, v) {

                        html += '<option value="' + v.id + '">' + v.nama_kota + '</option>';
                    });

                    $("#kota_shipping").html(html);
                    $("#kota_shipping").show();
                    $('#btn-check-4').hide();
                }
            });
    })

    // kecamatan tujuan
    $("#kota_shipping").change(function() {
        $("#kecamatan_shipping").hide();
        {if $this->session->has_userdata('kurir') == true}
          // hapus_kurir();
        {/if}
        var kota_origin = $("#kota_shipping").val();
        var urls = '{site_url("cart_order/checkout/kecamatan")}';
        $.ajax({
                //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
                url: urls + '?kota=' + kota_origin,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    var html = '<option value=""</option>';
                    $.each(data, function(k, v) {
                        html += '<option value="' + v.id + '">' + v.nama_kecamatan + '</option>';
                    });

                    $("#kecamatan_shipping").html(html);
                    $("#kecamatan_shipping").show();
                    $('#btn-check-4').hide();
                }
            });
    });

    // pilih tujuan
    $("#kecamatan_shipping").change(function(){
        var propinsi_origin = $("#propinsi_shipping").val();
        var kota_origin = $("#kota_shipping").val();
        var kecamatan = $('#kecamatan_shipping').val();
        var urls = '{site_url("cart_order/checkout/kecamatan_detail_indonesia")}';

        $.post(urls, { kecamatan: kecamatan }, function(data, textStatus, xhr) {
          /*optional stuff to do after success */
          data = jQuery.parseJSON(data);
          $('#shipping_city').val(data.nama_kecamatan);
          $('#shipping_postcode').val(data.kode_pos);
        });
        {foreach from=$list_kurir key=k item=i}
              {$i['biaya_ongkir']};
        {/foreach}
    });

    var total_bayar = function(){
      $.getJSON('{site_url("cart_order/cart_api/data_total_pembayaran")}', '', function(json, textStatus) {
          $('#total_dibayar').html(json.total_bayar);
      });
    }

    var hapus_kurir = function(){
      var urls = '{site_url("cart_order/checkout/hapus_kurir")}';
      $.ajax({
              //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
              url: urls,
              type: 'GET',
              dataType: 'html',
              success: function(data) {
                location.reload();
              }
          });
    }

    var ringkasan_order = function(){
      var url="{site_url('cart_order/cart_api/ringkasan')}";
      var html = '';
      html+='<tr>';
      html+='<th colspan="2">Rincian Pesanan</th>';
          html+='</tr>';
      $.getJSON(url, { }, function(json, textStatus) {
          /*optional stuff to do after success */
          $.each(json,function(index, el) {
            html +='<tr>';
                 html+='<td width="70%">'+el.nama+'</td>';
                 html+='<td>Rp. '+el.value+'</td>';
                 html+='</tr>';
          });

          $('#rincian_pesanan').html(html);
      });
    }

    // modal verifikasi alamat
    var verifikasi_alamat = function(){
      var url ="{site_url('cart_order/checkout/verifikasi_alamat')}";
      $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html',
        success:function(data){
          $('#mko-body').html(data);
          $('#modal-konfirmasi-order').modal('show');

        }
      });

    }

    // modal verifikasi alamat


  </script>

{/block}
