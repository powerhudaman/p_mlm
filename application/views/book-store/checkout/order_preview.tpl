{extends file=$themes}
{block name="main-content"}
  <style media="screen">
    .shop-cart{
      margin: 0px 0px 20px;
      display: block;
    }
  </style>
  <!-- Keranjang begin -->
    <div class="shop-cart">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <br>
            <br>
            <br>
          </div>
        </div>
        <div class="row">
          <!-- sukses page begin -->
            <div class="col-md-12">
                <div class="">
                    <h3>Order Preview</h3><br>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="product-name">Nama Produk</th>
                                <th class="product-price">Harga</th>
                                <th class="product-quantity">Qty</th>
                                <th class="product-subtotal">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$carts key=k item=cart}
                                <tr class="cart_item">
                                    <td class="product-name" data-title="Product Name">
                                        <a href="{site_url('detail')}/{$cart['id']}">{$cart['name']}</a>
                                        <p>SKU: {$cart['bara']}
                                        </p>
                                        <ul>
                                        {if count($cart['options']) > 0}
                                            {foreach from=$cart['options'] key=a item=option}

                                              {if count($option['opt_value']) > 0}
                                                <li>{$option['nama']} :
                                                  {foreach from=$option['opt_value']  key=b item=opt_value }
                                                    {$opt_value['nama']}
                                                  {/foreach}
                                                </li>
                                              {/if}

                                            {/foreach}
                                        {/if}
                                        </ul>
                                        <p></p>
                                    </td>
                                    <td class="product-price" data-title="Unit Price">
                                        <span class="amount">Rp. {number_format($cart['price'],0,",",".")}</span>
                                    </td>
                                    <td class="product-quantity" data-title="Qty">
                                        <div class="quantity buttons_added">
                                            {$cart['qty']}
                                        </div>
                                    </td>
                                    <td class="product-subtotal" data-title="Subtotal">
                                        <span class="amount">Rp. {number_format($cart['subtotal'],0,",",".")}</span>
                                    </td>
                                </tr>
                            {/foreach}

                            {assign var=total_bayar value=0}
                              <tr class="cart-subtotal">
                                  <th colspan="3">Subtotal:</th>
                                  <td><span class="amount">Rp. {number_format($this->cart->total(),0,",",".")}</span></td>
                                  {$total_bayar = $total_bayar + $this->cart->total()}
                              </tr>
                            {if $total_potongan_reseller > 0}
                              <tr class="cart-subtotal">
                                  <th colspan="3">Total Diskon Reseller:</th>
                                  <td><span class="amount">Rp. {number_format($total_potongan_reseller,0,",",".")}</span></td>
                              </tr>
                            {/if}
                            {if isset($biaya_ongkir['kurir'])}
                              <tr class="cart-subtotal">
                                  <th colspan="3">Biaya Pengiriman : {$biaya_ongkir['kurir']} - {$biaya_ongkir['layanan']} Tujuan {if !$this->session->has_userdata('customer_shipping')}
                                    ({$penerima['kota']})
                                  {/if}
                                  {if $this->session->has_userdata('customer_shipping')}
                                   ({$penerima['shipping_city']})
                                 {/if}</th>
                                  <td><span class="amount">Rp. {number_format($biaya_ongkir['nominal'],0,",",".")} {$total_bayar = $total_bayar + $biaya_ongkir['nominal']}</span></td>
                              </tr>
                            {/if}
                            {if $kode_unik !=''}
                              <tr class="cart-subtotal">
                                  <th colspan="3">Kode Unik:</th>
                                  <td><span class="amount">Rp. {number_format($kode_unik,0,",",".")} {$total_bayar = $total_bayar + $kode_unik}</span></td>
                              </tr>
                            {/if}
                            {if count($promo) > 0}
                              {foreach from=$promo key=prom item=v_promo}
                                <tr class="cart-subtotal">
                                    <th colspan="3">{$v_promo['jenis_promo']} ({$v_promo['kode']}) :</th>
                                    <td><span class="amount">Rp. {$v_promo['nilai_potongan']} {$total_bayar = $total_bayar - $v_promo['nilai_potongan']}</span></td>
                                </tr>
                              {/foreach}
                            {/if}
                            <tr class="cart-subtotal">
                                <th colspan="3">Grandtotal:</th>
                                <td><span class="amount">Rp. {number_format($total_bayar,0,",",".")}</span></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-12" style="margin:20px 0px 20px 0px;">
                    <div class="col-md-12">
                      <div class="col-md-12">
                        <h3>Data Pembeli</h3>
                      </div>
                      <div class="col-md-11">
                        <div class="col-md-12">
                          <div class="col-md-2">
                            <strong>Nama Lengkap</strong>
                          </div>
                          <div class="col-md-10">: {$pembeli['nama_depan']} {$pembeli['nama_belakang']}</div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-2">
                            <strong>Kota</strong>
                          </div>
                          <div class="col-md-10">: {$pembeli['kota']}</div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-2">
                            <strong>Alamat</strong>
                          </div>
                          <div class="col-md-10">: {$pembeli['alamat_1']}</div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-2">
                            <strong>No Telepon</strong>
                          </div>
                          <div class="col-md-10">: {$pembeli['telephone']}</div>
                        </div>
                      </div>
                      <div class="col-md-1"></div>
                    </div>
                    <br>
            </div>

            <div class="col-md-12" style="margin:10px 0px 20px 0px;">

                    <div class="col-md-12">
                      <div class="col-md-12">
                        <h3>Data Penerima (<strong>Tujuan Pengiriman</strong>)</h3>
                      </div>
                      {if $this->session->has_userdata('customer_shipping')}
                        <div class="col-md-11">
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Nama Lengkap</strong>
                            </div>
                            <div class="col-md-10">: {$penerima['shipping_firstname']} {$penerima['shipping_lastname']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Kota</strong>
                            </div>
                            <div class="col-md-10">: {$penerima['shipping_city']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Alamat</strong>
                            </div>
                            <div class="col-md-10">: {$penerima['shipping_address_1']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>No Telepon</strong>
                            </div>
                            <div class="col-md-10">: {$penerima['shipping_telephone']}</div>
                          </div>
                        </div>
                        <div class="col-md-1"></div>
                      {/if}
                      {if !$this->session->has_userdata('customer_shipping')}
                        <div class="col-md-11">
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Nama Lengkap</strong>
                            </div>
                            <div class="col-md-10">: {$pembeli['nama_depan']} {$pembeli['nama_belakang']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Kota</strong>
                            </div>
                            <div class="col-md-10">: {$pembeli['kota']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>Alamat</strong>
                            </div>
                            <div class="col-md-10">: {$pembeli['alamat_1']}</div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-2">
                              <strong>No Telepon</strong>
                            </div>
                            <div class="col-md-10">: {$pembeli['telephone']}</div>
                          </div>
                        </div>
                        <div class="col-md-1"></div>
                      {/if}
                    </div>
                    <br>
            </div>


          <!-- sukses page end -->
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4">
              <a href="{site_url('checkout/index')}" class="btn btn-warning pull-left">Ubah Alamat</a>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <a href="{site_url('checkout/pilih_pembayaran')}" class="btn btn-success pull-right">Metode Pembayaran</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!-- Keranjang end -->
{/block}
{block name="script_js"}

{/block}
