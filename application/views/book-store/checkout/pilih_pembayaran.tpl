{extends file=$themes}
{block name="main-content"}
  <style media="screen">
    .shop-cart{
      margin: 0px 0px 20px;
      display: block;
    }
  </style>
  <!-- Keranjang begin -->
    <div class="shop-cart">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <br>
            <br>
            <h4>Metode Pembayaran</h4>

          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <!-- metode pembayaran begin -->
              <ul class="nav nav-tabs">
                {assign var=no value='0'}
                {foreach from=$list_pembayaran key=k item=menu}
                  {$no = $no + 1}
                  <li class="{if $no == '1'} active {/if}" ><a data-toggle="tab" href="#{$menu['nama']}{$k}"  id="link_tab_payment{$no}" data-tabe="{$menu['nama']}{$k}">{$menu['nama_pembayaran']}</a></li>
                {/foreach}
              </ul>

              <div class="tab-content">
                {$no =0}
                {foreach from=$list_pembayaran key=k item=htm}
                  {$no = $no + 1}
                  <div id="{$htm['nama']}{$k}" class="tab-pane fade in {if $no == '1'}active{/if}">
                    <br>
                    <!-- <h3>{$htm['nama_pembayaran']}</h3> -->
                    <br>
                    {$htm['html']}
                  </div>
                {/foreach}
              </div>
            <!-- metode pembayaran end -->
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4">
              <a href="{site_url('checkout/index')}" class="btn btn-warning pull-left">Ubah Alamat</a>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <a href="{site_url('cart_order/checkout/simpan_order')}" class="btn btn-success pull-right">Selesaikan Order</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!-- Keranjang end -->
{/block}
{block name="script_js"}
  <script>
    jQuery(document).ready(function($) {
      setTimeout(function() {
        // $("#link_tab_payment").click();
        // if($('#link_tab_payment2').attr('data-tabe') !== undefined ){
          var data_tab = $('#link_tab_payment2').attr('data-tabe');
          if(data_tab !== undefined || null){
            $('.nav-tabs a[href="#'+data_tab+'"]').tab('show');
          }
          //  $("#link_tab_payment").trigger('click');
        // }

     },10);
    });
  </script>
{/block}
