<!DOCTYPE html>
<!-- saved from url=(0053)http://modular.develop.com/cart_order/checkout/sukses -->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" style="font-family: sans-serif;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;overflow-x: hidden;">


<body class="shoping-cart" cz-shortcut-listen="true" style="margin: 0;font-family: &quot;Lato&quot;, sans-serif;font-size: 14px;line-height: 25px;color: #666666;width: 90%;">
    <!-- sukses page begin -->
    <br><br>
    <div class="main-content container" style="padding: 0 10px;">
        <div class="shop-table-content container" style="padding: 0;">
            <div class="row base-selector cart-box" style="background-color: #fff;border: 1px solid #ddd;margin-top: 30px;padding: 20px;display: inline-block;width: 100%;">
                <!-- title begin -->
                <div class="col-md-12" style="border-bottom:solid 1.5px #e8e5e5; margin-bottom:15px;">
                    <h4 class="title-page" style="font-size: 16px;color: #333;margin-bottom: 0;margin-top: 0;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;margin: 35px 0 20px;">Millenniastore.id</h4>
                    
                </div>
                <div class="col-md-12" style="border-bottom:solid 1.5px #e8e5e5; margin-bottom:15px;">
                    <h4 class="title-page" style="font-size: 16px;color: #333;margin-bottom: 0;margin-top: 0;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;margin: 35px 0 20px;text-transform: uppercase;">Hai {$pembeli['nama_depan']} {$pembeli['nama_belakang']}, Terimakasih telah berbelanja</h4>
                    <p style="margin-bottom: 20px;">Pesanan anda dengan No Invoice <span class="text-success" style="font-size:20px;">{$invoice_no}</span> Telah Kami Terima. </p>
                </div>
                <div class="col-md-12">
                    <div class="col-md-8">
                        <h5 style="font-size: 14px;color: #333;margin-bottom: 0;margin-top: 0;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;">Jumlah Bayar</h5><br>
                        <h2 class="text-primary" style="font-size: 24px;color: #333;margin-bottom: 0;margin-top: 0;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;"> Rp. {number_format($total_tagihan_inv,0,",",".")} </h2><br><br>
                    </div>
                </div>
                <!-- title end -->
                {if $tombol_konfirmasi !="" && $tombol_konfirmasi == "1"}
                <!-- metode pembayaran begin -->
                <div class="col-md-12" style="margin:10px 0px 20px 0px; border-bottom:solid 1.5px #e8e5e5;">
                    <div class="">
                        <h3 style="font-size: 18px;color: #333;margin-bottom: 0;margin-top: 0;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;">Lakukan transfer bayar ke rekening berikut</h3>
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <th style="text-align:left;">Nama Bank</th>
                                    <td style="text-align:left;">
                                        <!-- <img src="{$url_backend}/assets/img/com_cart/payment_logo/{$detail_metode_pembayaran['logo_text']}" class="img" alt="" height="100px" width="150px"> -->
                                        <strong>{$detail_metode_pembayaran['nama_bank']}</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align:left;">No Rekening</th>
                                    <td style="text-align:left;">{$pembayaran['no_rekening']}</td>
                                </tr>
                                <tr>
                                    <th style="text-align:left;">Atas Nama </th>
                                    <td style="text-align:left;">{$pembayaran['atas_nama']}</td>
                                </tr>
                                <tr>
                                    <th style="text-align:left;">Untuk Melakukan Konfirmasi Pembayaran klik Link Berikut : </th>
                                    <td style="text-align:left;">
                                        <a href="{site_url('cart_payment/Konfirmasi/index')}?invoice_no={$invoice_no}" class="button orange" style="background-color: #ff8400;color: #fff;-webkit-transition: all 0.3s ease-in-out 0s;-moz-transition: all 0.3s ease-in-out 0s;-ms-transition: all 0.3s ease-in-out 0s;-o-transition: all 0.3s ease-in-out 0s;transition: all 0.3s ease-in-out 0s;text-decoration: none;border-radius: 3px;display: inline-block;font-size: 14px;font-weight: 700;line-height: 33px;padding: 0 20px;text-transform: uppercase;">Konfirmasi Pembayaran</a>
                                    </td>
                                </tr>
                                <tr>
                                  <th colspan="2"><strong>Lakukan pembayaran sebelum tanggal : {$order_expired}</strong></th>
                                </tr>
                            </tbody>
                        </table>
                        <br><br>
                    </div>
                </div>
                <!-- metode pembayaran end -->
                {/if}
                <!-- langkah pembayaran begin -->
                {if $payment_guide !=""}
                <div class="col-md-12" style="margin:10px 0px 20px 0px; border-bottom:solid 1.5px #e8e5e5;">
                    <div class="">
                        <h3 style="font-size: 18px;color: #333;margin-bottom: 0;margin-top: 0;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;">Langkah Pembayaran</h3>
                        {$payment_guide}
                        <br><br>
                    </div>
                </div>
                {/if}
                <!-- langkah pembayaran end -->
                <div class="col-md-12">
                    <div class="">
                        <h3 style="font-size: 18px;color: #333;margin-bottom: 0;margin-top: 0;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;">Ringkasan Order</h3><br>
                        <table style="border-spacing: 0;border-collapse: collapse;width: 100%;border: 1px solid #ececec;">
                            <thead>
                                <tr>
                                    <th class="product-name" style="padding: 10px;border: 1px solid #ececec; text-align:left;">Nama Produk</th>
                                    <th class="product-price" style="padding: 10px;border: 1px solid #ececec; text-align:left;">Harga</th>
                                    <th class="product-quantity" style="padding: 10px;border: 1px solid #ececec; text-align:left;">Qty</th>
                                    <th class="product-subtotal" style="padding: 10px;border: 1px solid #ececec; text-align:left;">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach from=$carts key=k item=cart}
                                  <tr class="cart_item">
                                      <td class="product-name" data-title="Product Name" style="padding: 10px;border: 1px solid #ececec; text-align:left;">
                                          <a href="{site_url('detail')}/{$cart['id']}" style="background-color: transparent;color: #333;-webkit-transition: all 0.3s ease-in-out 0s;-moz-transition: all 0.3s ease-in-out 0s;-ms-transition: all 0.3s ease-in-out 0s;-o-transition: all 0.3s ease-in-out 0s;transition: all 0.3s ease-in-out 0s;text-decoration: none;">{$cart['name']}</a>
                                          <p style="margin-bottom: 20px;">SKU: {$cart['bara']}
                                          </p>
                                          {if count($cart['options']) > 0}
                                            <ul style="margin: 0;padding: 0;list-style: none;">
                                              {foreach from=$cart['options'] key=a item=option}

                                                {if count($option['opt_value']) > 0}
                                                  <li>{$option['nama']} :
                                                    {foreach from=$option['opt_value']  key=b item=opt_value }
                                                      {$opt_value['nama']}
                                                    {/foreach}
                                                  </li>
                                                {/if}

                                              {/foreach}
                                            </ul>
                                          {/if}
                                          <p style="margin-bottom: 20px;"></p>
                                      </td>
                                      <td class="product-price" data-title="Unit Price" style="padding: 10px;border: 1px solid #ececec; text-align:left;">
                                          <span class="amount">Rp. {number_format($cart['price'],0,",",".")}</span>
                                      </td>
                                      <td class="product-quantity" data-title="Qty" style="padding: 10px;border: 1px solid #ececec; text-align:left;">
                                          <div class="quantity buttons_added">
                                              {$cart['qty']}
                                          </div>
                                      </td>
                                      <td class="product-subtotal" data-title="Subtotal" style="padding: 10px;border: 1px solid #ececec; text-align:left;">
                                          <span class="amount">Rp. {number_format($cart['subtotal'],0,",",".")}</span>
                                      </td>
                                  </tr>
                                {/foreach}
                                {assign var=total_bayar value=0}

                                <tr class="cart-subtotal">
                                    <th colspan="3" style="padding: 10px;border: 1px solid #ececec; text-align:left;">Subtotal:</th>
                                    <td style="padding: 10px;border: 1px solid #ececec; text-align:left;"><span class="amount">Rp. {number_format($this->cart->total(),0,",",".")}</span></td>
                                    {$total_bayar = $total_bayar + $this->cart->total()}
                                </tr>
                                {if isset($biaya_ongkir['kurir'])}
                                  <tr class="cart-subtotal">
                                      <th colspan="3" style="padding: 10px;border: 1px solid #ececec; text-align:left;">
                                          Biaya Pengiriman : {$biaya_ongkir['kurir']} - {$biaya_ongkir['layanan']} Tujuan {if !$this->session->has_userdata('customer_shipping')}
                                            ({$penerima['kota']})
                                          {/if}
                                          {if $this->session->has_userdata('customer_shipping')}
                                           ({$penerima['shipping_city']})
                                         {/if}
                                      </th>
                                      <td style="padding: 10px;border: 1px solid #ececec; text-align:left;"><span class="amount">Rp. {number_format($biaya_ongkir['nominal'],0,",",".")} {$total_bayar = $total_bayar + $biaya_ongkir['nominal']}</span></td>
                                  </tr>
                                {/if}

                                {if $kode_unik !=''}
                                    <tr class="cart-subtotal">
                                      <th colspan="3" style="padding: 10px;border: 1px solid #ececec; text-align:left;">Kode Unik:</th>
                                      <td style="padding: 10px;border: 1px solid #ececec; text-align:left;"><span class="amount">Rp. {number_format($kode_unik,0,",",".")}</span></td>
                                      {$total_bayar = $total_bayar + $kode_unik}
                                    </tr>
                                {/if}

                                {if count($promo) > 0}
                                  {foreach from=$promo key=prom item=v_promo}
                                    <tr class="cart-subtotal">
                                      <th colspan="3" style="padding: 10px;border: 1px solid #ececec; text-align:left;">{$v_promo['jenis_promo']} ({$v_promo['kode']}) :</th>
                                      <td style="padding: 10px;border: 1px solid #ececec; text-align:left;"><span class="amount">{$v_promo['nilai_potongan']} {$total_bayar = $total_bayar - $v_promo['nilai_potongan']}</span></td>
                                    </tr>
                                  {/foreach}
                                {/if}

                                <tr class="grand">
                                    <th colspan="3" style="padding: 10px;border: 1px solid #ececec; text-align:left;">Grand Total:</th>
                                    <td style="padding: 10px;border: 1px solid #ececec; text-align:left;"><span class="amount">Rp. {number_format($total_bayar,0,",",".")}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-12" style="margin:20px 0px 20px 0px; border-bottom:solid 1.5px #e8e5e5;">
                    <div class="">
                        <h3 style="font-size: 18px;color: #333;margin-bottom: 0;margin-top: 0;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;">Data Pembeli</h3>
                        <br>
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <th style="text-align:left;">Nama Lengkap</th>
                                    <td style="text-align:left;">: {$pembeli['nama_depan']} {$pembeli['nama_belakang']}</td>
                                </tr>
                                <tr>
                                    <th style="text-align:left;">Kota</th>
                                    <td style="text-align:left;">: {$pembeli['kota']}</td>
                                </tr>
                                <tr>
                                    <th style="text-align:left;">Alamat</th>
                                    <td style="text-align:left;">: {$pembeli['alamat_1']}</td>
                                </tr>
                                <tr>
                                    <th style="text-align:left;">No Telepon </th>
                                    <td style="text-align:left;">: {$pembeli['telephone']}</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
                <div class="col-md-12" style="margin:10px 0px 20px 0px; border-bottom:solid 1.5px #e8e5e5;">
                    <div class="">
                        <h3 style="font-size: 18px;color: #333;margin-bottom: 0;margin-top: 0;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;">Data Penerima (<strong style="font-weight: bold;">Tujuan Pengiriman</strong>)</h3>
                        <br>
                        {if $this->session->has_userdata('customer_shipping')}
                          <table style="width: 100%;">
                              <tbody>
                                  <tr>
                                      <th style="text-align:left;">Nama Lengkap</th>
                                      <td style="text-align:left;">: {$penerima['shipping_firstname']} {$penerima['shipping_lastname']}</td>
                                  </tr>
                                  <tr>
                                      <th style="text-align:left;">Kota</th>
                                      <td style="text-align:left;">: {$penerima['shipping_city']}</td>
                                  </tr>
                                  <tr>
                                      <th style="text-align:left;">Alamat</th>
                                      <td style="text-align:left;">: {$penerima['shipping_address_1']}</td>
                                  </tr>
                                  <tr>
                                      <th style="text-align:left;">No Telepon </th>
                                      <td style="text-align:left;">: {$penerima['shipping_telephone']}</td>
                                  </tr>
                              </tbody>
                          </table>
                        {/if}
                        {if !$this->session->has_userdata('customer_shipping')}
                          <table style="width: 100%;">
                              <tbody>
                                  <tr>
                                      <th style="text-align:left;">Nama Lengkap</th>
                                      <td style="text-align:left;">: {$penerima['nama_depan']} {$penerima['nama_belakang']}</td>
                                  </tr>
                                  <tr>
                                      <th style="text-align:left;">Kota</th>
                                      <td style="text-align:left;">: {$penerima['kota']}</td>
                                  </tr>
                                  <tr>
                                      <th style="text-align:left;">Alamat</th>
                                      <td style="text-align:left;">: {$penerima['alamat_1']}</td>
                                  </tr>
                                  <tr>
                                      <th style="text-align:left;">No Telepon </th>
                                      <td style="text-align:left;">: {$penerima['telephone']}</td>
                                  </tr>
                              </tbody>
                          </table>
                        {/if}

                        <br>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <br>
        <br>
        <br>
    </div>
    <!-- sukses page end -->
  </body>

</html>
