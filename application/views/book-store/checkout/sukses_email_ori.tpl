<!DOCTYPE html>
<html style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-family: sans-serif;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 10px;-webkit-tap-highlight-color: rgba(0,0,0,0);overflow-x: hidden;">
<head style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
  <title style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">Selamat Transaksi Anda Sukses Dengan No Invoice {$invoice_no}</title>
</head>
<body class="shoping-cart" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin: 0;font-family: &quot;Lato&quot;, sans-serif;font-size: 14px;line-height: 25px;color: #666666;background-color: #f5f5f5;width: 100%;overflow-x: hidden;">
  <!-- sukses page begin -->
  <div class="main-content container" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;padding: 0 10px;">
      <h4 class="title-page" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;line-height: 1.1;color: #333;margin-top: 0;margin-bottom: 0;font-size: 16px;margin: 35px 0 20px;text-transform: uppercase;">Sukses Page Dengan No Order {$invoice_no}</h4>
      <div class="shop-table-content container" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;padding: 0;">
          <div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
              <div class="col-md-12" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 100%;">
                  <div class="base-selector cart-box" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: #fff;border: 1px solid #ddd;margin-top: 30px;padding: 20px;display: inline-block;width: 100%;">
                    <h3 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;page-break-after: avoid;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;line-height: 1.1;color: #333;margin-top: 0;margin-bottom: 0;font-size: 18px;">Ringkasan Order</h3>
                    <table style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border-spacing: 0;border-collapse: collapse;background-color: transparent;width: 100%;border: 1px solid #ececec;">
                      <thead style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;display: table-header-group;">
                          <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th class="product-name" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Nama Produk</th>
                              <th class="product-price" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Harga</th>
                              <th class="product-quantity" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Qty</th>
                              <th class="product-subtotal" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Subtotal</th>
                          </tr>
                      </thead>
                        <tbody style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
                          {foreach from=$carts key=k item=cart}
                            <tr class="cart_item" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                                <td class="product-name" data-title="Product Name" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">
                                    <a href="{site_url('detail')}/{$cart['id']}" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: transparent;color: #333;text-decoration: underline;-webkit-transition: all 0.3s ease-in-out 0s;-moz-transition: all 0.3s ease-in-out 0s;-ms-transition: all 0.3s ease-in-out 0s;-o-transition: all 0.3s ease-in-out 0s;transition: all 0.3s ease-in-out 0s;">{$cart['name']}</a>
                                    <p style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;margin-bottom: 20px;">Bara: {$cart['bara']}
                                        <ul style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-top: 0;margin-bottom: 10px;margin: 0;padding: 0;list-style: none;">
                                        {if count($cart['options']) > 0}
                                            {foreach from=$cart['options'] key=a item=option}
                                              <li style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;list-style: none;">{$option['nama']} :
                                              {if count($option['opt_value']) > 0}
                                                  {foreach from=$option['opt_value']  key=b item=opt_value }
                                                    {$opt_value['nama']}
                                                  {/foreach}
                                              {/if}
                                              </li>
                                            {/foreach}
                                        {/if}
                                        </ul>
                                    </p>
                                </td>
                                <td class="product-price" data-title="Unit Price" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">
                                    <span class="amount" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">Rp. {number_format($cart['price'],0,",",".")}</span>
                                </td>
                                <td class="product-quantity" data-title="Qty" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">
                                    <div class="quantity buttons_added" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
                                        {$cart['qty']}
                                    </div>
                                </td>
                                <td class="product-subtotal" data-title="Subtotal" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">
                                    <span class="amount" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">Rp. {number_format($cart['subtotal'],0,",",".")}</span>
                                </td>
                            </tr>
                          {/foreach}
                          {assign var=total_bayar value=0}
                            <tr class="cart-subtotal" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th colspan="3" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Subtotal:</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;"><span class="amount" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">Rp. {number_format($this->cart->total(),0,",",".")}</span></td>
                              {$total_bayar = $total_bayar + $this->cart->total()}
                            </tr>
                          {if isset($biaya_ongkir['kurir'])}
                            <tr class="cart-subtotal" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th colspan="3" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Biaya Pengiriman : {$biaya_ongkir['kurir']} - {$biaya_ongkir['layanan']} Tujuan ({$biaya_ongkir['kota']})</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;"><span class="amount" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
                                Rp. {number_format($biaya_ongkir['nominal'],0,",",".")} {$total_bayar = $total_bayar + $biaya_ongkir['nominal']}
                              </span></td>
                            </tr>
                          {/if}
                          {if count($promo) > 0}
                            {foreach from=$promo key=prom item=v_promo}
                              <tr class="cart-subtotal" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                                <th colspan="3" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">{$v_promo['jenis_promo']} ({$v_promo['kode']}) :</th>
                                <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;"><span class="amount" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">Rp. {$v_promo['nilai_potongan']} {$total_bayar = $total_bayar - $v_promo['nilai_potongan']}</span></td>
                              </tr>
                            {/foreach}
                          {/if}
                          <tr class="grand" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                            <th colspan="3" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Grand Total:</th>
                            <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;"><span class="amount" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">Rp. {number_format($total_bayar,0,",",".")}</span></td>
                          </tr>
                        </tbody>
                    </table>
                  </div>
              </div>
          </div>
          <div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
                  <div class="col-md-6" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 100%;">
                    <div class="base-selector cart-box" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: #fff;border: 1px solid #ddd;margin-top: 30px;padding: 20px;display: inline-block;width: 100%;">
                      <h3 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;page-break-after: avoid;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;line-height: 1.1;color: #333;margin-top: 0;margin-bottom: 0;font-size: 18px;">Data Pembeli</h3>
                      <table style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border-spacing: 0;border-collapse: collapse;background-color: transparent;width: 100%;border: 1px solid #ececec;">
                          <tbody style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Nama Lengkap</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$pembeli['nama_depan']} {$pembeli['nama_belakang']}</td>
                            </tr>
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Kota</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$pembeli['kota']}</td>
                            </tr>
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Alamat</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$pembeli['alamat_1']}</td>
                            </tr>
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">No Telepon </th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$pembeli['telephone']}</td>
                            </tr>
                          </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col-md-6" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 100%;">
                    <div class="base-selector cart-box" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: #fff;border: 1px solid #ddd;margin-top: 30px;padding: 20px;display: inline-block;width: 100%;">
                      <h3 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;page-break-after: avoid;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;line-height: 1.1;color: #333;margin-top: 0;margin-bottom: 0;font-size: 18px;">Data Penerima (<strong style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-weight: 700;">Tujuan Pengiriman</strong>)</h3>
                      {if $this->session->has_userdata('customer_shipping')}
                      <table style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border-spacing: 0;border-collapse: collapse;background-color: transparent;width: 100%;border: 1px solid #ececec;">
                          <tbody style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Nama Lengkap</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$penerima['shipping_firstname']} {$penerima['shipping_lastname']}</td>
                            </tr>
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Kota</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$penerima['shipping_city']}</td>
                            </tr>
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Alamat</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$penerima['shipping_address_1']}</td>
                            </tr>
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">No Telepon </th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$penerima['shipping_telephone']}</td>
                            </tr>
                          </tbody>
                      </table>
                      {/if}
                      {if !$this->session->has_userdata('customer_shipping')}
                      <table style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border-spacing: 0;border-collapse: collapse;background-color: transparent;width: 100%;border: 1px solid #ececec;">
                          <tbody style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Nama Lengkap</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$penerima['nama_depan']} {$penerima['nama_belakang']}</td>
                            </tr>
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Kota</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$penerima['kota']}</td>
                            </tr>
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Alamat</th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$penerima['alamat_1']}</td>
                            </tr>
                            <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                              <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">No Telepon </th>
                              <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$penerima['telephone']}</td>
                            </tr>
                          </tbody>
                      </table>
                      {/if}
                    </div>
                  </div>

          </div>
          <div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
              <div class="col-md-12" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 100%;">
                  <div class="base-selector cart-box" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: #fff;border: 1px solid #ddd;margin-top: 30px;padding: 20px;display: inline-block;width: 100%;">
                    <h3 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;page-break-after: avoid;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;line-height: 1.1;color: #333;margin-top: 0;margin-bottom: 0;font-size: 18px;">Metode Pembayaran</h3>
                    <table style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border-spacing: 0;border-collapse: collapse;background-color: transparent;width: 100%;border: 1px solid #ececec;">
                        <tbody style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
                          <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                            <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Nama Bank</th>
                            <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$pembayaran['nama_bank']}</td>
                          </tr>
                          <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                            <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">No Rekening</th>
                            <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$pembayaran['no_rekening']}</td>
                          </tr>
                          <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                            <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Atas Nama </th>
                            <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">{$pembayaran['atas_nama']}</td>
                          </tr>
                          <tr style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;page-break-inside: avoid;">
                            <th style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;text-align: left;border: 1px solid #ececec;">Untuk Melakukan Konfirmasi Pembayaran klik Link Berikut : </th>
                            <td style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 10px;border: 1px solid #ececec;">
                              <a href="{site_url('cart_payment/Konfirmasi/index')}?invoice_no={$invoice_no}" class="button orange" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: #ff8400;color: #fff;text-decoration: underline;-webkit-transition: all 0.3s ease-in-out 0s;-moz-transition: all 0.3s ease-in-out 0s;-ms-transition: all 0.3s ease-in-out 0s;-o-transition: all 0.3s ease-in-out 0s;transition: all 0.3s ease-in-out 0s;border-radius: 3px;display: inline-block;font-size: 14px;font-weight: 700;line-height: 33px;padding: 0 20px;text-transform: uppercase;">Konfirmasi Pembayaran</a>
                            </td>
                          </tr>
                        </tbody>
                    </table>
                  </div>
              </div>
          </div>
          {if $payment_guide !=""}
          <div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
              <div class="col-md-12" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 100%;">
                  <div class="base-selector cart-box" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: #fff;border: 1px solid #ddd;margin-top: 30px;padding: 20px;display: inline-block;width: 100%;">
                    <h3 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;page-break-after: avoid;font-family: &quot;Lato&quot;, sans-serif;font-weight: 600;line-height: 1.1;color: #333;margin-top: 0;margin-bottom: 0;font-size: 18px;">Langkah Pembayaran</h3>
                      {$payment_guide}
                  </div>
              </div>
          </div>
          {/if}
      </div>
  </div>

  <br>
  <br>
  <br>
  <p class="text-center">Untuk informasi lebih lanjut mengenai pembelian anda, silakan hubungi:</p>
  <p class="text-center">email: cs@millenniastore.id | phone: 021-29047525</p>
  <!-- sukses page end -->
</body>
</html>
