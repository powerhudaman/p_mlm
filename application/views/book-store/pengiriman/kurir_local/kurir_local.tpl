<style media="screen">
  .box-pengiriman{
    display: block;
    margin: 0px;
  }
  .item-pilihan{
    display: inline-block;
    width: 100%;
  }
  .item-pilihan .radio{
    display: inline-block;
    width: 15%;
  }
  .item-pilihan .label{
    display: inline-block;
    width: 70%;
    margin-left: 5px;
  }
</style>
<div class="form-group" id="kurir_local_services"></div>
<div class="col-md-12" id="pengiriman_kurir_local">




</div>
  <script>
  /*

  <div class="col-md-4 col-sm-6 col-xs-12 outer-box-pembayaran">
    <img src="./kurir_local_files/jne.jpg" alt="" class="img" height="100px" width="150px"><br>
    <ul>
      <li class="item-pilihan">
       <input type="radio" name="kurir_local" id="" >
       <span > <strong>Reguler</strong> Rp. 25000 </span>
      </li>
      <li class="item-pilihan">
       <input type="radio" name="kurir_local" id="" >
       <span > <strong>Oke</strong> Rp. 22500 </span>
      </li>
    </ul>
  </div>
   */

  var kurir_local = function(kecam_id =''){
    var url_local = "{site_url('cart_kurir/kurir_local/biaya_ongkir')}?kecamatan_tujuan="+kecam_id;
    var html ='';
    $.getJSON(url_local, { param1: 'value1' }, function(json, textStatus) {
        /*optional stuff to do after success */
        $.each(json,function(index, el1) {

          html +='<div class="col-md-4 col-sm-6 col-xs-12 box-pengiriman">';
            html +='<img src="{$url_backend}/assets/img/com_cart/kurir_logo/'+el1.logo+'" alt="" class="img" height="100px" width="150px"><br>';
            html +='<ul>';

              $.each(el1.jenis_pengiriman,function(index2, el2) {
                html +='<li class="item-pilihan">';
                 html +='<input type="radio" name="kurir_local" id="kurir_local_biaya_' + el2.nos + '" onclick="set_kurir_kirim_local(\'' + el2.nos + '\')" value="'+el2.nos+'" >';
                 html +='<span > <strong>'+el2.nama_layanan+'</strong> Rp. '+el2.biaya+' </span>';
                html +='</li>';
              });

            html +='</ul>';
          html +='</div>';
        });
        $('#pengiriman_kurir_local').html(html);
    });

  }

  var set_kurir_kirim_local = function(nos){
    // alert(nos);

    $.post('{site_url("cart_kurir/kurir_local/pilih_kurir")}', { biaya: nos }, function(data) {
      var parse = jQuery.parseJSON(data);
        if(parse.kode == '1'){
          // alert(parse.pesan);
          $("#kurir_local_services").html(parse.pesan);
          $('#pengiriman_kurir_local').html('');
          // $('#td_kurir').html(kurir+' - Layanan '+layanan);
          // $('#td_biaya').html(parse.nominal);
          // total_bayar();
          {if $skip_langkah_pembayaran == '1'}
           var kode_metode_pembayaran = "{$default_pembayaran[1]}";
           var kode_key = "{$default_pembayaran[1]}";
           var url_pembayaran = "{site_url('cart_payment')}/{$default_pembayaran[0]}/pilih_pembayaran";
           $.post(url_pembayaran,{ kode_metode_pembayaran:kode_metode_pembayaran, kode_key:kode_key },function(respons){

           });
          {/if}
          $('#btn-check-4').show();
          ringkasan_order(); /// ini fungsi dari halaman checkout
        }else{
          // alert(parse.pesan);
          // $('#td_kurir').html('');
          // $('#td_biaya').html('0');
          kurir_local('');
        }

    });

  }

  </script>
