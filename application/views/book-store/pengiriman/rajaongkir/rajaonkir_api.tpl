<div class="col-md-12">
    <div class="form-group">
        <label for="">Propinsi Tujuan</label>
        <select name="propinsi_tujuan" id="propinsi_tujuan" class="form-control">
              <option value=""></option>
              {foreach from=$propinsi['rajaongkir']['results'] key=k item=v}
                {assign var=pilih value=''}
                  {if $v['province_id'] == $_SESSION['kurir']['rajaongkir']['propinsi_tujuan']}
                    {$pilih ='selected'}
                  {/if}
                <option value="{$v['province_id']}" {$pilih}>{$v['province']}</option>
              {/foreach}
        </select>
    </div>

    <div class="form-group">
        <label for="">Kota Tujuan</label>
        <select name="kota_tujuan" id="kota_tujuan" class="form-control">
          <option value=""></option>
            {if $kotas !="tak_ado" }
             {foreach from=$kotas['rajaongkir']['results'] key=k item=kota}
               {assign var=pilih value=''}
                 {if $kota['city_id'] == $_SESSION['kurir']['rajaongkir']['kota_tujuan']}
                   {$pilih ='selected'}
                 {/if}
                <option value="{$kota['city_id']}" {$pilih} >{$kota['city_name']}</option>
              {/foreach}
            {/if}
        </select>
    </div>

    <div class="form-group">
        <label for="">Kecamatan Tujuan</label>
        <select name="kecamatan_tujuan" id="kecamatan_tujuan" class="form-control">
        <option value=""></option>
        {if $kecamatans !="tak_ado" }
         {foreach from=$kecamatans['rajaongkir']['results'] key=k item=kecamatan}
           {assign var=pilih value=''}
             {if $kecamatan['subdistrict_id'] == $_SESSION['kurir']['rajaongkir']['kecamatan_tujuan']}
               {$pilih ='selected'}
             {/if}
            <option value="{$kecamatan['subdistrict_id']}" {$pilih} >{$kecamatan['subdistrict_name']}</option>
          {/foreach}
        {/if}
      </select>
    </div>
    <div class="form-group" id="kurir_services"></div>
</div>
<script>
    // kota tujuan
    $("#propinsi_tujuan").change(function() {
        $("#kecamatan_tujuan").hide();
        $("#kota_tujuan").hide();

        var propinsi_origin = $("#propinsi_tujuan").val();
        var urls = '{site_url("cart_kurir/rajaongkir/kota")}';
        $.ajax({
                //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
                url: urls + '?propinsi=' + propinsi_origin,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    var html = '<option value=""</option>';
                    $.each(data, function(k, v) {

                        html += '<option value="' + v.city_id + '">' + v.city_name + ' (' + v.type + ')</option>';
                    });


                    $("#kota_tujuan").html(html);
                    $("#kota_tujuan").show();
                }
            })
            .done(function() {
                console.log("success");
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

    })

    // kecamatan tujuan
    $("#kota_tujuan").change(function() {
        $("#kecamatan_tujuan").hide();

        var kota_origin = $("#kota_tujuan").val();
        var urls = '{site_url("cart_kurir/rajaongkir/kecamatan")}';
        $.ajax({
                //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
                url: urls + '?kota=' + kota_origin,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    var html = '<option value=""</option>';
                    $.each(data, function(k, v) {

                        html += '<option value="' + v.subdistrict_id + '">' + v.subdistrict_name + ' (' + v.type + ')</option>';
                    });


                    $("#kecamatan_tujuan").html(html);
                    $("#kecamatan_tujuan").show();
                }
            })
            .done(function() {
                console.log("success");
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    });

    var biaya_ongkir = function(kecam_id) {
        $("#kurir_services").html('Tunggu Sebentar Biaya Pengiriman Sedang Dihitung');
        var url = '{site_url("cart_kurir/rajaongkir/biaya_ongkir_api?kecamatan_tujuan=")}' + kecam_id;
        $.ajax({
                url: '{site_url("cart_kurir/rajaongkir/biaya_ongkir_api?kecamatan_tujuan=")}' + kecam_id,
                type: 'GET',
                dataType: 'json',
                success: function(data) {

                    var html = '<ul>';
                    var nos = 0;
                    $.each(data, function(k, v) {

                        html += '<li><strong>' + k + '</strong>';

                        $.each(v, function(a, b) {
                            nos++;

                            html += '<ul>';
                            html += '<li>';
                            html += '<input type="radio" name="biaya" id="biaya_' + nos + '" onclick="set_kurir_kirim(\'' + nos + '\')" value="' + nos + '">';
                            html += '<input type="hidden" name="kurir" id="kurir_' + nos + '" value="' + k + '">';
                            html += '<input type="hidden" name="service" id="service_' + nos + '" value="' + b.service + '">';
                            html += '<input type="hidden" name="id_kota" id="kota_id_' + nos + '" value="">';
                            html += b.service + ' Biaya = ' + b.biaya + ' </li>';
                            html += '</ul>';

                        })


                        html += '</li>';

                    })

                    html += '</ul>';

                    $("#kurir_services").html(html);

                }
            })
            .done(function() {
                console.log("success");
            })
            .fail(function() {
                biaya_ongkir(kecam_id);
            })
            .always(function() {
                console.log("complete");
            });

    }

    $("#kecamatan_tujuan").change(function(event) {
        var kecamatan = $("#kecamatan_tujuan").val();
        // set_tujuan();
        biaya_ongkir(kecamatan);

    });

    $("#cek_ongkir").click(function() {
        var kecamatan = $("#kecamatan_tujuan").val();
        biaya_ongkir(kecamatan);
    });

    var set_kurir_kirim = function(ids) {
        // cekDataPengirim_raja();
        var propinsi_tujuan = $("#propinsi_tujuan").val();
        var kota_tujuan = $("#kota_tujuan").val();
        var kecamatan_tujuan = $("#kecamatan_tujuan").val();
        var biaya = $("#biaya_" + ids).val();
        var kurir = $("#kurir_" + ids).val();
        var layanan = $("#service_" + ids).val();

        // var ongkir_detail = ongkir + ' (' + kurir + ' ' + kurir_type + ' Tujuan: ' + $("#kota").val() + ')';

        $.post('{site_url("cart_kurir/rajaongkir/pilih_kurir_api")}', {
            propinsi_tujuan: propinsi_tujuan,
            kota_tujuan: kota_tujuan,
            kecamatan_tujuan: kecamatan_tujuan,
            biaya: biaya,
            kurir: kurir,
            layanan: layanan
        }, function(data) {
          var parse = jQuery.parseJSON(data);
            if(parse.kode == '1'){
              alert(parse.pesan);
              $("#kurir_services").html(parse.pesan);
              $('#td_kurir').html(kurir+' - Layanan '+layanan);
              $('#td_biaya').html(parse.nominal);
              // console.log(parse.ses_kurir);
              pilih_kurir(parse.ses_kurir);
            }else{
              alert(parse.pesan);
              $('#td_kurir').html('');
              $('#td_biaya').html('0');
              biaya_ongkir(kecamatan_tujuan);
            }

        });
    }
</script>
