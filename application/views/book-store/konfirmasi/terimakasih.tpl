{extends file=$themes}
{block name="main-content"}
    <br><br>
    <style media="screen">
      .shop-cart {
        margin: 0px 0px 20px;
        display: block;
      }
    </style>
    <!-- Keranjang begin -->
    <div class="shop-cart">
      <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="base-selector cart-box">
                      <h3 class="text-center">Terima kasih telah melakukan konfirmasi pembayaran, informasi lain telah kami kirimkan ke email Anda</h3><br>
                      <p class="text-center">
                        Saat ini kami akan segera lakukan verifikasi pembayaran paling lambat 2 x 24 jam (hari kerja) dan akan kami informasikan kembali jika pesanan memasuki tahap selanjutnya.
                      </p>
                      <p class="text-center">
                        Terimakasih telah berbelanja di <strong>{$this->site_name}</strong>
                      </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
{/block}
