{extends file=$themes}

{block name="main-content"}

    <style media="screen">
      .shop-cart {
        margin: 0px 0px 20px;
        display: block;
      }
    </style>
    <!-- Keranjang begin -->
    <div class="shop-cart">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <br>
            <br>
            <br>
            <h4>Konfirmasi Pembayan</h4>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-4"></div>
        </div>
        <div class="row">
          <div class="col-md-12">
            {assign var=cfg_konfirmasi value=['id'=>'frm_konfirmasi']}
            {form_open_multipart(site_url('cart_payment/konfirmasi/proses'),$cfg_konfirmasi)}

              <div class="form-group">

                <label for="">No Invoice <span class="text-red">*</span> </label>
                <input type="text" name="invoice_no" id="invoice_no" class="form-control" value="{$this->input->get('invoice_no')}">

                <span id="loading" style="display:none;"><img src="http://millenniastore.id//assets/plugins/loader/spin.gif" alt=""></span>

              </div>
              <div class="form-group">
                {form_error('metode_pembayaran')}
                <label for="">Pilih Metode Pembayaran</label>
                <select name="metode_pembayaran" id="metode_pembayaran" class="form-control">
                  <option value="">Pilih Metode Pembayaran</option>
                  {foreach from=$list_payment key=k item=v}
                    <option value="{$v['value']}">{$v['label']}</option>
                  {/foreach}
                </select>
              </div>

              <div class="form-group">
                {form_error('pay_from')}
                <label for="">Bank Asal</label>
                <input type="text" name="pay_from" id="pay_from" class="form-control" value="{set_value('bayar_dari')}">
              </div>
              <div class="form-group">
                {form_error('nama_pemilik')}
                <label for="">Nama Pemilik Rekening</label>
                <input type="text" name="nama_pemilik" id="nama_pemilik" class="form-control" value="{set_value('nama_pemilik')}">
              </div>

              <div class="form-group">
                {form_error('tgl_bayar')}
                <label for="">Tanggal Pembayaran <span class="text-red">*</span></label>
                <input type="text" name="tgl_bayar" id="tgl_bayar" class="form-control" value="{set_value('tgl_bayar')}">
              </div>
              <div class="form-group">
                {form_error('nominal')}
                <label for="">Jumlah yang sudah ditransferkan </label>
                {if $detail_order == 0}
                  <input type="text" name="nominal" id="nominal" class="form-control" value="{set_value('nominal')}">
                {/if}
                {if $detail_order !=0}
                  <input type="text" name="nominal" id="nominal" class="form-control" value="{$detail_order['total']}">
                {/if}
                  <p>Inputlah sesuai dengan jumlah uang yang anda transfer. Masukan jumlah pembayaran tanpa tanda titik</p>
              </div>
              <div class="form-group">
                <label for="">Bukti Pembayaran (<strong>Max 600kb</strong>)</label>
                <input type="file" name="bukti_pembayaran" id="bukti_pembayaran" class="form-control" >
              </div>

              <div class="form-group">
                <label for="">Keterangan (Opsional)</label>
                <textarea name="keterangan" id="keterangan" cols="30" rows="10" class="form-control"></textarea>
              </div>

              <a class="btn btn-warning" href="#" onclick="frm_konfirmasi()">Konfirmasi Pembayaran</a>
            {form_close()}
          </div>
        </div>
      </div>
    </div>
    <!-- Keranjang end -->
    <br>
    <br>
    <br>
{/block}

{block name=script_js}
  <script>
    $('#tgl_bayar').datepicker({
      dateFormat:"yy-mm-dd"
    });
    var frm_konfirmasi = function(){
      $('#frm_konfirmasi')[0].submit(function(event) {
        /* Act on the event */
      });
    }

    $('#invoice_no').change(function(event) {
      /* Act on the event */
      $('#loading').show();
      var inv = $('#invoice_no').val();
      var url_invoice = "{site_url('cart_payment/konfirmasi/nilai_order')}?invoice_no="+inv;
      setTimeout(function(){

        $.getJSON(url_invoice, { }, function(json, textStatus) {
            /*optional stuff to do after success */
            if(json.status && json.kode == '1'){
              $('#invoice_no').val(json.data.invoice_no);
              $('#nominal').val(json.data.nilai_order);
              var url_metode_pembayaran = "{site_url('cart_payment/konfirmasi/metode_pembayaran')}?metode_pembayaran="+json.data.metode_pembayaran;
              $.getJSON(url_metode_pembayaran, { }, function(mp, textStatus) {
                  /*optional stuff to do after success */
                  if(mp.status && mp.kode == '1'){
                    $('#metode_pembayaran').html('');
                    var html = '<option value="">Pilih Metode Pembayaran</option>';
                    $.each(mp.data,function(index, el) {
                        html += '<option value="'+el.value+'">'+el.label+'</option>';
                    });
                    $('#metode_pembayaran').html(html);
                    $('#loading').hide();
                  }
              });
            }
            if(!json.status && json.kode == '2'){
              $('#invoice_no').val('');
              $('#nominal').val('');
              alert(json.pesan);
              $('#loading').hide();
            }
        });
      },500);

    });
  </script>
{/block}
