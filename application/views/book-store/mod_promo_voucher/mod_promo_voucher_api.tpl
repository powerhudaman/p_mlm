<div class="{$size_block}">
      {if $status == '1'}
          {if $voucher !=""}
              <div class="row">
                  <div class="col-md-12">
                    <table class="table table-bordered">
                      <tr>
                        <td>Kode Voucher</td>
                        <td>{$voucher['kode_voucher']}</td>
                      </tr>
                      <tr>
                        <td>Nama Event</td>
                        <td>{$voucher['nama_voucher']}</td>
                      </tr>
                      <tr>
                        <td>Jenis Voucher</td>
                        <td>{$jenis_voucher[$voucher['jenis']]}</td>
                      </tr>
                      <tr>
                        <td>Nilai Voucher</td>
                        <td>{$voucher['nilai_voucher']}</td>
                      </tr>
                      <tr>
                        <td>Nilai Potongan</td>
                        <td>{$voucher['nilai_potongan']}</td>
                      </tr>
                      <tr>
                        <td colspan="2"><a href="#" onclick="hapus_voucher()" class="btn btn-danger btn-sm">Lepaskan Voucher</a></td>
                      </tr>
                    </table>
                  </div>
                  
              </div>
          {/if}
          {if empty($voucher)}

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="">Masukan Kode Voucher </label>
                      <input type="text" name="kode_voucher" id="kode_voucher" class="form-control">
                  </div>
                  <div class="form-group">
                  </div>
                  <div class="form-group">
                    <a href="#" onclick="use_voucher()" class="btn btn-primary btn-sm">Gunakan Voucher</a>
                  </div>
                </div>
              </div>

          {/if}
      {/if}
</div>

<script>
var use_voucher = function(){
  var kode_voucher = $('#kode_voucher').val();
  var url = "{$url}/cart_promo/voucher/use_voucher_api";
  $.ajax({
    url: url+'/'+kode_voucher,
    type: 'GET',
    dataType: 'json',
    success:function(response){
      alert(response.pesan);
      keranjang();
    }
  });
}

var hapus_voucher = function(){
  var url = "{$url}/cart_promo/voucher/hapus_voucher";
  $.ajax({
    url: url,
    type: 'GET',
    dataType: 'json',
    success:function(response){
      alert(response.pesan);
      keranjang();
    }
  });

}


</script>
