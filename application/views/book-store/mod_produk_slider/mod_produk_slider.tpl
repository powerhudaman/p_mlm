{if $posisi == 'pos-3'}
  <!-- Upcoming Release -->
  <section class="upcoming-release">

    <!-- Heading -->
    <div class="container-fluid p-0">
        <div class="release-heading pull-right h-white">
          <h5>{$title_produk_slider}</h5>
        </div>
    </div>
    <!-- Heading -->

    <!-- Upcoming Release Slider -->
    <div class="upcoming-slider">
        <div class="container">
          <!-- Release Book Detail -->
          <div class="release-book-detail h-white p-white">
            <div class="release-book-slider">
              {foreach from=$produks key=k item=produk}
                <div class="item">
                  <div class="detail">
                    <h5><a href="{site_url('detail')}/{$produk['produk_id']}">
                            {if $produk['judul_produk'] !=''}
                              {$produk['judul_produk']}
                            {/if}
                            {if $produk['judul_produk'] ==''}
                              {$produk['nama']}
                            {/if}
                        </a>
                    </h5>
                    <p>{word_limiter(strip_tags($produk['keterangan']),10)}</p>
                    {if $produk['harga_lama'] !='' && $produk['harga_lama'] > 0}
                      <span style="display: block;width: 50%;font-size: 18px;text-decoration: line-through double;color: red;">Rp. {number_format($produk['harga_lama'],0,",",".")}</span>
                      <span style="display: block;width: 50%;font-size: 22px;">Rp. {number_format($produk['harga'],0,",",".")}</span>
                    {/if}
                    {if $produk['harga_lama'] ==''}
                      <span>Rp. {number_format($produk['harga'],0,",",".")}</span>
                    {/if}

                    <i class="fa fa-angle-double-right"></i>
                  </div>
                  <div class="detail-img">
                    <img src="{base_url()}admin718/assets/img/com_cart/produk/{$produk['gambar_hd']}" alt="{$produk['nama']}" width="112px" height="156px">
                  </div>
                </div>
              {/foreach}

            </div>
          </div>
          <!-- Release Book Detail -->
          <!-- Thumbs -->
          <div class="release-thumb-holder">
            <ul id="release-thumb" class="release-thumb">
              {assign var=angka value=0}
              {foreach from=$produks key=k item=produk}
                <li>
                  <a data-slide-index="{$angka}" href="">
                    <span>
                      {if $produk['judul_produk'] !=''}
                        {ellipsize($produk['judul_produk'],10,1)}
                      {/if}
                      {if $produk['judul_produk'] ==''}
                        {ellipsize($produk['nama'],1,1)}
                      {/if}
                    </span>
                    <img src="{base_url()}admin718/assets/img/com_cart/produk/{$produk['gambar']}" alt="{$produk['nama']}" width="92px" height="122px">
                    <img class="b-shadow" src="{base_url()}themes/book-store/images/upcoming-release/b-shadow.png" alt="">
                    <span onclick="modal_detail_buku({$produk['produk_id']})" class="plus-icon">+</span>
                  </a>
                </li>
                {$angka=$angka+1}
              {/foreach}

            </ul>
          </div>
          <!-- Thumbs -->
        </div>
      </div>
    <!-- Upcoming Release Slider -->

  </section>
  <!-- Upcoming Release -->
{/if}
{if $posisi == 'pos-11'}
  <!-- Recomend products produk slider -->
  <div class="recomended-products tc-padding">
    <div class="container">

      <!-- Main Heading -->
      <div class="main-heading-holder">
        <div class="main-heading">
          <h2>{$title_produk_slider}</h2>
        </div>
      </div>
      <!-- Main Heading -->

      <!-- Recomend products Slider -->
      <div class="recomend-slider">

        {assign var=angka value=0}
        {foreach from=$produks key=k item=produk}
          <!-- Item -->
          <div class="item">
            <a href="{$produk['link']}"><img src="{base_url()}admin718/assets/img/com_cart/produk/{$produk['gambar']}" alt="{$produk['nama']}" width="109px" height="150px"></a>
          </div>
          <!-- Item -->
          {$angka=$angka+1}
        {/foreach}


      </div>
      <!-- Recomend products Slider -->

    </div>
  </div>
  <!-- Recomend products produk slider -->
{/if}
{if $posisi != 'pos-3' && $posisi !='pos-11'}
    <br>
      <div class="detail-slide relates">
    <br>
          <h3 class="title-detail-slide">{$title_produk_slider}</h3>
          <div class="supermartket-owl-carousel" data-number="5" data-margin="30" data-navcontrol="yes">
            {foreach from=$produks key=k item=produk}
                <div class="product-box">
                    <div class="product-box-content">
                        <figure class="img-product">
                            <img src="{base_url()}admin718/assets/img/com_cart/produk/{$produk['gambar_hd']}" alt="{$produk['nama']}" height="207" width="175">
                            <a href="{site_url('detail')}/{$produk['produk_id']}" class="flaticon-search"></a>
                        </figure>
                        <div class="product-box-text">
                          {if $produk['judul_produk'] !=''}
                            <a href="{site_url('detail')}/{$produk['produk_id']}" class="product-name">{character_limiter($produk['judul_produk'],14)}</a>
                          {/if}
                          {if $produk['judul_produk'] ==''}
                            <a href="{site_url('detail')}/{$produk['produk_id']}" class="product-name">{character_limiter($produk['nama'],14)}</a>
                          {/if}

                          {if $produk['harga_lama'] !='' && $produk['harga_lama'] > 0}
                            <p class="product-cost"> <span class="product-cost-discount">Rp. {number_format($produk['harga_lama'],0,",",".")}</span> <span style="margin-left:0.5em;">Rp. {number_format($produk['harga'],0,",",".")}</span></p>

                          {/if}
                          {if $produk['harga_lama'] ==''}
                            <p class="product-cost">{number_format($produk['harga'],0,",",".")}</p>
                          {/if}
                            <div class="product-box-bottom">
                                <a href="#" onclick="add_to_cart_related_{$id_tag}({$produk['produk_id']})" class="add-to-cart"><i class="flaticon-commerce"></i>Add To Cart</a>
                                <!--<a href="#" class="wishlist"><i class="flaticon-like"></i></a>
                                <a href="#" class="refresh-product"><i class="flaticon-arrows"></i></a>-->
                            </div>
                        </div>
                    </div>
                </div>
            {/foreach}
          </div>
      </div>

      <script type="text/javascript">
        var add_to_cart_related_{$id_tag} = function(id_produk){
          var url = "{site_url('cart_order/cart_api/add_test')}/"+id_produk;
          $.post(url, { qty: '1' }, function(data, textStatus, xhr) {
              $('#pesan_notif').html(data.pesan);
              // if(data.status && data.kode == '1'){
                // window.location.href = "{site_url('cart_order/cart/show')}";
              // }
          });
        }
      </script>
{/if}
