	<!-- banner slider begin -->
	<div id="main-slider" class="main-slider">
		{assign var=no value='-1'}

		{foreach from=$banner_items key=k item=v}

			{$no = $no+1}

			{assign var=aktif value=''}

			{if $no==0}

				{$aktif='active'}

			{/if}
				<!-- Item -->
				<div class="item">
					<img src="{base_url()}admin718/assets/img/com_banner/{$v['gambar']}" alt="">
					<div class="banner-overlay">
						<div class="container position-center-center">

							<!-- caption -->
							<div class="caption style-2">
								<h1>{$v['nama']}</h1>
								<b>{$v['sub_judul']}</b>
								{if $v['link']!=''}
									<a href="{site_url({$v['link']})}" class="btn-1">{$v['nama_link']}<i class="fa fa-arrow-circle-right"></i></a>
								{/if}
							</div>
							<!-- caption -->

						</div>
					</div>
				</div>
				<!-- Item -->
		{/foreach}
	</div>
	<!-- banner slider end -->