{extends file=$themes}

{block name="main-content"}
  <!-- content list produk -->
  <!-- Main Content -->
	<main class="main-content">

		<!-- Book List -->
		<div class="tc-padding">
			<div class="container">
				<div class="row">

					<!-- Content -->
					<div class="col-lg-9 col-md-8 col-xs-12 pull-right pull-none">

						<!-- Book List Header -->
						<div class="book-list-header">
							<style media="screen">
								.box-pengarang{
									margin-bottom: 20px;
									border-bottom:solid 1px #c6c6c6;
								}
							</style>
							<div class="col-md-12 box-pengarang">
								<h4>Buku Karya {$pengarang['nama']}</h4>
							</div>
							<!-- Heading -->
							<h4>Sortir Berdasarkan : </h4>
							{assign var=sortir value=['newitem'=>'Produk Terbaru','name'=>'Nama Produk (A-Z)','lowprice'=>'Harga Terendah','highprice'=>'Harga Tertinggi']}

							<select name="sortir" id="sortir">
								{foreach from=$sortir key=k_s item=v_s}
									{assign var=pilih value=''}
									{if $this->input->get('sortir') == $k_s}
											{$pilih='selected'}
									{/if}
									<option value="{$k_s}" {$pilih}>{$v_s}</option>
								{/foreach}
							</select>
							<!-- Heading -->

							<!-- Filter Nav -->
							<div class="filter-tags-holder">
								<!-- <ul id="filterbale-nav" class="option-set">
									<li><a class="selected" data-filter="*" href="#">All General</a></li>
									{foreach from=$list_kategori_aktif key=k item=v}
									<li><a data-filter=".{$k}" href="#">{ellipsize($v,10)}</a></li>
									{/foreach}
								</ul> -->
							</div>
							<!-- Filter Nav -->

						</div>
						<!-- Book List Header -->
						<!-- Book List Widgets -->
						<div id="filter-masonry" class="gallery-masonry">
              {if !array_key_exists('pesan',$list_produk)}
									<div class="row">
										<div class="col-md-12">
										</div>
									</div>

                {foreach from=$list_produk key=k item=produk}
                    <!-- book begin -->
                      <div class="book-list-widget masonry-grid {$produk['kategori_produk']}" id="p_{$produk['produk_id']}">
												{if $produk['discount'] > 0 && $produk['harga_lama'] > 0}
                    			<span class="triangle-topright pull-right"><span class="diskon-triangle">{$produk['discount']}%</span></span>
												{/if}
                      <div class="book-list-detail">
                        <img src="{base_url()}admin718/assets/img/com_cart/produk/{$produk['gambar_detail']}" alt="product" width="109px" height="164px">
                        <div class="detail">
                          <span>by {$pengarang['nama']}</span>
                          <div class="book-name">
                              {if $setting_seo == 1}
                                {if !empty($produk['url_title'])}
                                  {if $produk['judul_produk'] !=""}
                                    <h5><a href="{site_url($produk['url_title'])}" class="product-name" title="{$produk['judul_produk']}">{ucfirst($produk['judul_produk'])}</a></h5>
                                  {/if}
                                  {if $produk['judul_produk'] ==""}
                                    <h5><a href="{site_url($produk['url_title'])}" class="product-name" title="{$produk['nama']}">{ucfirst($produk['nama'])}</a></h5>
                                  {/if}

                                {/if}
                                {if empty($produk['url_title'])}
                                  {if $produk['judul_produk'] !=""}
                                    <h5><a href="{site_url('detail')}/{$produk['produk_id']}" class="product-name" title="{$produk['judul_produk']}">{ucfirst($produk['judul_produk'])}</a></h5>
                                  {/if}
                                  {if $produk['judul_produk'] ==""}
                                    <h5><a href="{site_url('detail')}/{$produk['produk_id']}" class="product-name" title="{$produk['nama']}">{ucfirst($produk['nama'])}</a></h5>
                                  {/if}
                                {/if}
                              {/if}
                              {if $setting_seo == 0}
                                {if $produk['judul_produk'] !=""}
                                  <h5><a href="{site_url('detail')}/{$produk['produk_id']}" class="product-name" title="{$produk['judul_produk']}">{ucfirst($produk['judul_produk'])}</a></h5>
                                {/if}
                                {if $produk['judul_produk'] ==""}
                                  <h5><a href="{site_url('detail')}/{$produk['produk_id']}" class="product-name" title="{$produk['nama']}">{ucfirst($produk['nama'])}</a></h5>
                                {/if}
                              {/if}
                            {if $produk['harga_lama'] > 0} <strong class="harga-discount" style="margin-left:2%">Rp.{number_format($produk['harga_lama'],0,",",".")}</strong> {/if}<strong style="margin-left:1%">	Rp.{number_format($produk['harga'],0,",",".")}</strong>
                          </div>
													{if array_key_exists('total_vote',$produk) && array_key_exists('nilai_star',$produk)}
													<ul class="rating-stars">
														{for $star=1 to $produk['nilai_star']}
                            	<li><i class="fa fa-star"></i></li>
														{/for}
                            <li>{$produk['total_vote']} reviews</li>
                          </ul>
													{/if}
                          <p>{word_limiter(strip_tags($produk['keterangan']),30)} <a href="{site_url('detail')}/{$produk['produk_id']}" style="margin-left:1%;color:gray;">Read More</a></p>
                          </div>
                      </div>
                      <div class="book-list-btm">
                        <div class="user-likes">
													{if !array_key_exists('reviewer',$produk)}
															<span>Belum Ada Review</span>
													{/if}
													{if array_key_exists('reviewer',$produk)}
                          <ul>
														{assign var=hitung_review value=0}
														{foreach from=$produk['reviewer'] key=rv item=r}
															{$hitung_review = $hitung_review + 1}
                            	<li><img src="{theme_url('images/user-likes/img-01.jpg')}" alt=""></li>
														{/foreach}
														{if $produk['reviewer'][0]['customer'] ==""}
															{$produk['reviewer'][0]['customer'] = 'Anonymous'}
														{/if}
														{if $hitung_review == 1}
															<li>{$produk['reviewer'][0]['customer']} menyukai buku ini</li>
														{/if}
														{if $hitung_review > 1}
															{$hitung_review = $hitung_review - 1}
															<li class="li-terakhir">{$produk['reviewer'][0]['customer']} dan {$hitung_review} teman lainya menyukai buku ini</li>
														{/if}
                          </ul>
													{/if}
                        </div>
                        <div class="like-nd-share">
                          <ul>
                            <li>
                              {if $setting_seo == 1}
                                {if !empty($produk['url_title'])}
                                  <a href="{site_url('detail')}/{$produk['url_title']}"><i class="fa fa-eye"></i>View</a>
                                {/if}
                                {if empty($produk['url_title'])}
                                  <a href="{site_url('detail')}/{$produk['produk_id']}"><i class="fa fa-eye"></i>View</a>
                                {/if}
                              {/if}
                              {if $setting_seo == 0}
                                <a href="{site_url('detail')}/{$produk['produk_id']}"><i class="fa fa-eye"></i>View</a>
                              {/if}
                            </li>
                            <!-- <li><a href="#"><i class="fa fa-share-alt"></i>Share</a></li> -->
														<li><a href="#p_{$produk['produk_id']}" onclick="modal_detail_buku({$produk['produk_id']})"><i class="fa fa-tags"></i> <span style="color:black">Beli</span></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- book end -->
                {/foreach}
              {/if}

              {if array_key_exists('pesan',$list_produk)}
                  <h4>Tidak Ada Produk Dalam Kategori Ini</h4>
              {/if}

						</div>
						<!-- Book List Widgets -->
            {if !array_key_exists('pesan',$list_produk)}
              <!-- Pagination -->
								{$halaman}
              <!-- Pagination -->
            {/if}


					</div>
					<!-- Content -->

					<!-- Aside -->
					<aside class="col-lg-3 col-md-4 col-xs-12 pull-left pull-none">
						<!-- aside detail pengarang  -->
						<div class="aside-widget">
							<div class="row">
								<div class="col-md-12">
									<h4>{$pengarang['nama']}</h4>
									{if $pengarang['foto'] == ""}
										<img src="https://images-na.ssl-images-amazon.com/images/I/81sKnbxNXZL._UX250_.jpg" alt="" class="img">
									{/if}
									{if $pengarang['foto'] !=""}
										<img src="{base_url()}admin718/assets/img/com_cart/pengarang/{$pengarang['foto']}" alt="" class="img">
									{/if}
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 text-justify">
									{$pengarang['tentang_penulis']}
								</div>
							</div>
						</div>
						<!-- aside detail pengarang  -->
						<!-- Aside Widget author -->
						{if $author_of_the_weeks !=0}
						<div class="aside-widget">
							<h6>Author of the Week</h6>
							<ul class="s-arthor-list">
								{foreach from=$author_of_the_weeks key=ka item=va}
								<li>
									<div class="s-arthor-wighet">
										<div class="s-arthor-img">
											{if $va['foto'] ==""}
												<img src="{theme_url('images/s-arthor-list/img-01.jpg')}" alt="">
											{/if}
											{if $va['foto'] !=""}
												<img src="{base_url()}admin718/assets/img/com_cart/pengarang/{$va['foto']}" width="45px" height="45px" alt="">
											{/if}
											<div class="overlay">
												<a class="position-center-center" href="#">+</a>
											</div>
										</div>
										<div class="s-arthor-detail">
											<h6>{$va['nama']} <a href="#">@jdoe</a></h6>
											<!-- <a href="#">+ Follow on</a> -->
											<a href="{site_url('penulis/tentang_penulis/')}/{$va['kode_pengarang']}">Tentang Penulis</a>
										</div>
									</div>
								</li>
								{/foreach}
							</ul>
						</div>
						{/if}
						<!-- Aside Widget author -->


						<!-- Aside Widget -->
						<div class="aside-widget">
							<h6>Books of the Year</h6>
							<ul class="books-year-list">
								<li>
									<div class="books-post-widget">
										<img src="{theme_url('images/books-year-list/img-01.jpg')}" alt="">
										<h6><a href="#">My Brilliant Friend The Neapolitan Novels, Book One</a></h6>
										<span>By Elena Ferrante</span>
									</div>
								</li>
								<li>
									<div class="books-post-widget">
										<img src="{theme_url('images/books-year-list/img-02.jpg')}" alt="">
										<h6><a href="#">As night fell, something stirred the darkness.</a></h6>
										<span>By Meg Caddy</span>
									</div>
								</li>
								<li>
									<div class="books-post-widget">
										<img src="{theme_url('images/books-year-list/img-03.jpg')}" alt="">
										<h6><a href="#">The Rosie Project: Don Tillman 1</a></h6>
										<span>By Graeme Simsion</span>
									</div>
								</li>
								<li>
									<div class="books-post-widget">
										<img src="{theme_url('images/books-year-list/img-04.jpg')}" alt="">
										<h6><a href="#">Heartbreaking, joyous, traumatic, intimate and</a></h6>
										<span>By Magda Szubanski</span>
									</div>
								</li>
							</ul>
						</div>
						<!-- Aside Widget -->

					</aside>
					<!-- Aside -->

				</div>
			</div>
		</div>
		<!-- Blog All Views -->

	</main>
	<!-- Main Content -->

  <!-- content list produk -->

</div>
{/block}
{block name=script_js}
  <script>
    var add_to_cart = function(id_produk){
      var url = "{site_url('cart_order/cart_api/add_test')}/"+id_produk;
      $.post(url, { qty: '1' }, function(data, textStatus, xhr) {
          $('#pesan_notif').html(data.pesan);
          $("#pesan_notif").load(location.href + " #pesan_notif");
          if(data.status && data.kode == '1'){
            window.location.href = "{site_url('cart_order/cart/show')}";
          }
      });
    }
    /* -- Price Filter */
    // $('.ui-slider-range').each(function(e) {
        var nilai_a = 2500;
        var nilai_b = 150000;
        {if $this->input->get('range_a')!='' }
        nilai_a = "{$this->input->get('range_a')}";
        nilai_b = "{$this->input->get('range_b')}";
        $('#amount').html(nilai_a)
        $('#amount2').html(nilai_b);
        $('#amounta').html( nilai_a)
        $('#amount2a').html( nilai_b);
        {/if}
        $('#ui-slider-range').slider({
            range: true,
            min: 2500,
            max: 150000,
            values: [nilai_a, nilai_b],
            slide: function (event, ui) {
                $('#amount').html( ui.values[0])
                $('#amount2').html( ui.values[1]);
                $('#amounta').html( ui.values[0])
                $('#amount2a').html( ui.values[1]);
                $('#range_a').val(ui.values[0]);
                $('#range_b').val(ui.values[1]);
            }
        });
    // });
    /* -- Price Filter */
    /*$('.slider-range').each(function(e) {
        $(this).slider({
            range: true,
            min: 2500,
            max: 1500000,
            values: [2500, 1000000],
            slide: function (event, ui) {
                $(this).find('.amount').html( ui.values[0])
                $(this).find('.amount2').html( ui.values[1]);
                $('#range_a').val(ui.values[0]);
                $('#range_b').val(ui.values[1]);
            }
        });
        $(this).find('.amount').html($(this).slider('values', 0));
        $(this).find('.amount2').html($(this).slider('values', 1));
    });*/

    var cari_range = function(){
      $('#frm_range_price')[0].submit(function(event) {
        /* Act on the event */
      });
    }

    $('#sortir').change(function() {
      /* Act on the event */
      window.location = "{current_url()}?sortir="+$('#sortir').val();
    });

  </script>
{/block}
