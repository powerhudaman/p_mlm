<!doctype html>
<html class="no-js" lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content=""/>
<!-- Document Title -->
<title>{$this->site_name}</title>

<!-- StyleSheets -->
<link rel="stylesheet" href="{theme_url('css/bootstrap/bootstrap.min.css')}">
<link rel="stylesheet" href="{theme_url('css/font-awesome.min.css')}">
<link rel="stylesheet" href="{theme_url('css/animate.css')}">
<link rel="stylesheet" href="{theme_url('css/icomoon.css')}">
<link rel="stylesheet" href="{theme_url('css/main.css')}">
<link rel="stylesheet" href="{theme_url('css/color-1.css')}">
<link rel="stylesheet" href="{theme_url('style.css')}">
<link rel="stylesheet" href="{theme_url('css/responsive.css')}">
<link rel="stylesheet" href="{theme_url('css/transition.css')}">



<!-- Switcher CSS -->
<link href="{theme_url('switcher/switcher.css')}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" id="skins" href="{theme_url('css/default.css')}" type="text/css" media="all">

<!-- JavaScripts -->
<script src="{theme_url('js/vendor/modernizr.js')}"></script>
<!-- <script src="{theme_url('js/vendor/jquery.js')}"></script> -->
<script src="{theme_url('js/vendor/jquery.min.js')}"></script>
<script src="{theme_url('js/vendor/bootstrap.min.js')}"></script>

<!-- Java Script -->
<script src="{theme_url('js/datepicker.js')}"></script>
<script src="{theme_url('js/contact-form.js')}"></script>
<script src="{theme_url('js/bigslide.js')}"></script>
<script src="{theme_url('js/3d-book-showcase.js')}"></script>
<script src="{theme_url('js/turn.js')}"></script>
<script src="{theme_url('js/jquery-ui.js')}"></script>
<script src="{theme_url('js/mcustom-scrollbar.js')}"></script>
<script src="{theme_url('js/timeliner.js')}"></script>
<script src="{theme_url('js/parallax.js')}"></script>
<script src="{theme_url('js/countdown.js')}"></script>
<script src="{theme_url('js/countTo.js')}"></script>
<script src="{theme_url('js/owl-carousel.js')}"></script>
<script src="{theme_url('js/bxslider.js')}"></script>
<script src="{theme_url('js/appear.js')}"></script>
<script src="{theme_url('js/sticky.js')}"></script>
<script src="{theme_url('js/prettyPhoto.js')}"></script>
<script src="{theme_url('js/isotope.pkgd.js')}"></script>
<script src="{theme_url('js/wow-min.js')}"></script>
<script src="{theme_url('js/classie.js')}"></script>

	{if isset($css_head)}

				{foreach from=$css_head item=css }
						<link rel="stylesheet" href="{base_url('assets')}/{$css}">
				{/foreach}
	{/if}

	{if isset($js_head)}

				{foreach from=$js_head item=js }
						<script src="{base_url('assets')}/{$js}"></script>
				{/foreach}

	{/if}


	{block name="script_css"}

	{/block}

</head>
<body class="white-bg">

<!-- Preloader -->
<!-- <div id="status">
	<div id="preloader">
		<div class="preloader position-center-center">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
</div> -->
<!-- Preloader -->

<!-- Wrapper -->
<div class="wrapper push-wrapper">

	<!-- Header -->
	<header id="header">

		<!-- Top Bar -->
		<div class="topbar">
			<div class="container">

				<!-- Online Option -->
				<div class="online-option">
					<ul>
						<li><a href="#">how to buy</a></li>
						<li><a href="{site_url('cart_payment/konfirmasi/index')}">konfirmasi pembayaran</a></li>
					</ul>
				</div>
				<!-- Online Option -->

				<!-- Cart Option -->
				<div class="cart-option">
					<ul>
						<li class="add-cart"><a href="{site_url('keranjang')}"><i class="fa fa-shopping-bag"></i><span id="total_items_cart">{$this->cart->total_items()}</span></a></li>
						{*<li><a href="#"><i class="fa fa-heart-o"></i>wish List 0 items</a></li>*}
						{if !$this->session->has_userdata('member_logged')}
							<li><a href="#" data-toggle="modal" data-target="#login-modal"><i class="fa fa-user"></i>Login / Register</a></li>
						{/if}
						{if $this->session->has_userdata('member_logged')}
							<li><a href="{site_url('customer/logout')}"><i class="fa fa-user"></i>Logout</a></li>
						{/if}
					</ul>
				</div>
				<!-- Cart Option -->

			</div>
		</div>
		<!-- Top Bar -->

		<!-- Nav -->
		<nav class="nav-holder style-1">
			<div class="container">
				<div class="mega-dropdown-wrapper">

					<!-- Logo -->
					<div class="logo">
						<a href="{$this->site_url}"><img src="{base_url()}/admin718/assets/img/logo/logo.png" alt="" width="185px" height="64px"></a>
					</div>
					<!-- Logo -->

					<!-- Search bar -->
					<div class="search-bar">
						<a href="#" onclick="pencarian_modal()"><i class="fa fa-search"></i></a>
					</div>
					<!-- Search bar -->

					<!-- Responsive Button -->
					<div class="responsive-btn">
						<a href="#menu" class="menu-link circle-btn"><i class="fa fa-bars"></i></a>
					</div>
					<!-- Responsive Button -->

					<!-- Navigation -->
					<!-- Pos 2 Begin  -->
					{block name="pos-2"}
	          	{$this->pos_2}
	        {/block}
					<!-- Pos 2 End  -->
					<!-- Navigation -->

				</div>
			</div>
		</nav>
		<!-- Nav -->

		<!--BANNER-->
		<!-- pos4 begin -->
		{block name="pos-4"}
			{if $this->pos_4 !=""}
				{$this->pos_4}
			{/if}

		{/block}
		<!-- pos4 end -->
		<!--BANNER-->

	</header>
	<!-- Header -->

	<!-- Main Content -->
	{block name="main-content"}
	<main class="main-content">

			<!-- pos3 begin -->
			{block name="pos-3"}

					{$this->pos_3}

			{/block}
			<!-- pos3 end -->


		<!-- pos6 begin -->
		{block name="pos-6"}
			{if $this->pos_6 !=""}
				{$this->pos_6}
			{/if}



		{/block}

		<!-- pos6 end -->

		<!-- pos9 begin -->
		{block name="pos-9"}
			{if $this->pos_9 !=""}
				{$this->pos_9}
			{/if}

			<!-- Add Banners single banner -->
			<section class="add-banners-holder tc-padding-bottom">
				<div class="container">
					<div class="row">

						<!-- Banner -->
						<div class="col-lg-6 col-sm-12">
							<div class="add-banner add-banner-1">
								<div class="z-inedex-2 p-relative">
									<h3>Celebrate the Book Authors</h3>
									<p>How to Write a Book Review Request to Bloggers, a guide for authors</p>
									<hr>
									<strong class="font-merriweather">Buy Now 280.99 <sup>$</sup></strong>
								</div>
								<img class="adds-book" src="{theme_url('images/add-banners/add-books/img-01.png')}" alt="">
							</div>
						</div>
						<!-- Banner -->

						<!-- Banner -->
						<div class="col-lg-6 col-sm-12">
							<div class="add-banner add-banner-2">
								<div class="z-inedex-2 p-relative">
									<strong>Look Books 2016</strong>
									<h3>Up to 20% off</h3>
									<hr>
									<p>of advance enternce exam Books</p>
								</div>
								<img class="adds-book" src="{theme_url('images/add-banners/add-books/img-02.png')}" alt="">
							</div>
						</div>
						<!-- Banner -->

					</div>
				</div>
			</section>
			<!-- Add Banners single banner -->
		{/block}
		<!-- pos9 end -->

		<!-- pos11 begin -->
		{block name="pos-11"}
			{if $this->pos_11 !=""}
				{$this->pos_11}
			{/if}

			{*<!-- Book Collections -->
			<section class="book-collection">
				<div class="container">
					<div class="row">

						<!-- Book Collections Tabs -->
						<div id="book-collections-tabs-gg">

							<!-- collection Name -->
							<div class="col-lg-3 col-sm-12">
								<div class="sidebar">
									<h4>Top Books Catagories</h4>
									<ul>
										<li><a href="#">Science fiction</a></li>
										<li><a href="#">Satire</a></li>
										<li><a href="#">Drama</a></li>
										<li><a href="#">Action and Adventure</a></li>
										<li><a href="#">Self help</a></li>
										<li><a href="#">Health</a></li>
										<li><a href="#">Guide</a></li>
										<li><a href="#">Religion, Spirituality &amp; New Age</a></li>
										<li><a href="#">Encyclopedias</a></li>
										<li><a href="#">Dictionaries</a></li>
										<li><a href="#">Cookbooks</a></li>
										<li><a href="#">Autobiographies</a></li>
										<li><a href="#">Biographies</a></li>
										<li><a href="#">Fantasy</a></li>
										<li><a href="#">Horror</a></li>
										<li><a href="#">Mystery</a></li>
										<li><a href="#">Solipsist</a></li>
										<li><a href="#">The Zombie Survival</a></li>
										<li><a href="#">I Am Legend</a></li>
										<li><a href="#">Everything i Never Told</a></li>
										<li><a href="#">Big Little Lies</a></li>
										<li><a href="#">The Bone Clocks</a></li>
										<li><a href="#">Revival</a></li>
										<li><a href="#">Station Eleven</a></li>
										<li><a href="#">The American Lady</a></li>
									</ul>
								</div>
							</div>
							<!-- collection Name -->

							<!-- Collection Content -->
							<div class="col-lg-9 col-sm-12">
								<div class="collection">

									<!-- Secondary heading -->
									<div class="sec-heading">
										<h3>Shop <span class="theme-color">Books</span> Collection</h3>
										<a class="view-all" href="#">View All<i class="fa fa-angle-double-right"></i></a>
									</div>
									<!-- Secondary heading -->

									<!-- Collection Content -->
									<div class="collection-content">
										<ul>
											<li>
												<div class="s-product">
													<div class="s-product-img">
														<img src="{theme_url('images/products-collection/img-01.jpg')}" alt="">
														<div class="s-product-hover">
															<div class="position-center-x">
																<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
																<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a>
															</div>
														</div>
													</div>
													<h6><a href="book-detail.html">Ramadan Kareem</a></h6>
													<span>Richard Matherson</span>
												</div>
											</li>
											<li>
												<div class="s-product">
													<div class="s-product-img">
														<img src="{theme_url('images/products-collection/img-02.jpg')}" alt="">
														<div class="s-product-hover">
															<div class="position-center-x">
																<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
																<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a>
															</div>
														</div>
													</div>
													<h6><a href="book-detail.html">Mars Club</a></h6>
													<span>Eden Lepucki</span>
												</div>
											</li>
											<li>
												<div class="s-product">
													<div class="s-product-img">
														<img src="{theme_url('images/products-collection/img-03.jpg')}" alt="">
														<div class="s-product-hover">
															<div class="position-center-x">
																<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
																<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a>
															</div>
														</div>
													</div>
													<h6><a href="book-detail.html">Festa Junnai</a></h6>
													<span>George R.R. Martin</span>
												</div>
											</li>
											<li>
												<div class="s-product">
													<div class="s-product-img">
														<img src="{theme_url('images/products-collection/img-04.jpg')}" alt="">
														<div class="s-product-hover">
															<div class="position-center-x">
																<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
																<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a>
															</div>
														</div>
													</div>
													<h6><a href="book-detail.html">Beer Fsstivak</a></h6>
													<span>Micheal Circhton</span>
												</div>
											</li>
											<li>
												<div class="s-product">
													<div class="s-product-img">
														<img src="{theme_url('images/products-collection/img-05.jpg')}" alt="">
														<div class="s-product-hover">
															<div class="position-center-x">
																<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
																<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a>
															</div>
														</div>
													</div>
													<h6><a href="book-detail.html">Rock Festival</a></h6>
													<span>Richard Matherson</span>
												</div>
											</li>
											<li>
												<div class="s-product">
													<div class="s-product-img">
														<img src="{theme_url('images/products-collection/img-06.jpg')}" alt="">
														<div class="s-product-hover">
															<div class="position-center-x">
																<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
																<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a>
															</div>
														</div>
													</div>
													<h6><a href="book-detail.html">Summer Festival</a></h6>
													<span>Edgar Rice Burroghs</span>
												</div>
											</li>
											<li>
												<div class="s-product">
													<div class="s-product-img">
														<img src="{theme_url('images/products-collection/img-07.jpg')}" alt="">
														<div class="s-product-hover">
															<div class="position-center-x">
																<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
																<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a>
															</div>
														</div>
													</div>
													<h6><a href="book-detail.html">Festa JUnnai</a></h6>
													<span>Max Brooks</span>
												</div>
											</li>
											<li>
												<div class="s-product">
													<div class="s-product-img">
														<img src="{theme_url('images/products-collection/img-08.jpg')}" alt="">
														<div class="s-product-hover">
															<div class="position-center-x">
																<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
																<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a>
															</div>
														</div>
													</div>
													<h6><a href="book-detail.html">Summer Festival</a></h6>
													<span>J.R.R. Tolkien</span>
												</div>
											</li>
											<li>
												<div class="s-product">
													<div class="s-product-img">
														<img src="{theme_url('images/products-collection/img-09.jpg')}" alt="">
														<div class="s-product-hover">
															<div class="position-center-x">
																<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
																<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a>
															</div>
														</div>
													</div>
													<h6><a href="book-detail.html">New Year Collection</a></h6>
													<span>Henry Rollins</span>
												</div>
											</li>
											<li>
												<div class="s-product">
													<div class="s-product-img">
														<img src="{theme_url('images/products-collection/img-10.jpg')}" alt="">
														<div class="s-product-hover">
															<div class="position-center-x">
																<a href="#" class="plus-icon"><i class="fa fa-shopping-cart"></i></span>
																<a class="btn-1 sm shadow-0" data-toggle="modal" data-target="#quick-view" href="#">Quick view</a>
															</div>
														</div>
													</div>
													<h6><a href="book-detail.html">Happy Halloween</a></h6>
													<span>Lily King</span>
												</div>
											</li>
										</ul>
									</div>
									<!-- Collection Content -->

								</div>
							</div>
							<!-- Collection Content -->

						</div>
						<!-- Book Collections Tabs -->

					</div>
				</div>
			</section>
			<!-- Book Collections --> *}

			<!-- Related Products -->
			<section class="related-product tc-padding-bottom hori-scroll">
			  <div class="container">

			      <!-- Main Heading -->
					<div class="main-heading-holder">
						<div class="main-heading">
							<h2>Bookshop <span class="theme-color">Related</span> Products</h2>
							<p>Have a reading loft in my house I will make this happen with</p>
						</div>
					</div>
					<!-- Main Heading -->

					<!-- Content -->
			      <div class="content scroll-x">
			        <ul>
			        	<li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-01.png')}" alt="">
			              	<h5><a href="shop-detail.html">Business card</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-02.png')}" alt="">
			              	<h5><a href="shop-detail.html">Basket Books</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-03.png')}" alt="">
			              	<h5><a href="shop-detail.html">Cd Cover</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-04.png')}" alt="">
			              	<h5><a href="shop-detail.html">File Folder</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-05.png')}" alt="">
			              	<h5><a href="shop-detail.html">Books Rack</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-01.png')}" alt="">
			              	<h5><a href="shop-detail.html">Stationary</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-01.png')}" alt="">
			              	<h5><a href="shop-detail.html">Business card</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-02.png')}" alt="">
			              	<h5><a href="shop-detail.html">Basket Books</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-03.png')}" alt="">
			              	<h5><a href="shop-detail.html">Cd Cover</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-04.png')}" alt="">
			              	<h5><a href="shop-detail.html">File Folder</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-05.png')}" alt="">
			              	<h5><a href="shop-detail.html">Books Rack</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-01.png')}" alt="">
			              	<h5><a href="shop-detail.html">Stationary</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-01.png')}" alt="">
			              	<h5><a href="shop-detail.html">Business card</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-02.png')}" alt="">
			              	<h5><a href="shop-detail.html">Basket Books</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-03.png')}" alt="">
			              	<h5><a href="shop-detail.html">Cd Cover</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-04.png')}" alt="">
			              	<h5><a href="shop-detail.html">File Folder</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-05.png')}" alt="">
			              	<h5><a href="shop-detail.html">Books Rack</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-01.png')}" alt="">
			              	<h5><a href="shop-detail.html">Stationary</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-01.png')}" alt="">
			              	<h5><a href="shop-detail.html">Business card</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-02.png')}" alt="">
			              	<h5><a href="shop-detail.html">Basket Books</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-03.png')}" alt="">
			              	<h5><a href="shop-detail.html">Cd Cover</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-04.png')}" alt="">
			              	<h5><a href="shop-detail.html">File Folder</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-05.png')}" alt="">
			              	<h5><a href="shop-detail.html">Books Rack</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			            <li>
			            		<span class="price"><sup>$</sup>12.00</span>
			              	<img src="{theme_url('images/related-products/img-01.png')}" alt="">
			              	<h5><a href="shop-detail.html">Stationary</a></h5>
			              	<ul class="product-cart-option position-center-x">
									<li><a href="#"><i class="fa fa-eye"></i></a></li>
									<li><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
									<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
								</ul>
			            </li>
			        </ul>
			      </div>
			      <!-- Content -->

			  </div>
			</section>
			<!-- Related Products -->



		{/block}
		<!-- pos11 end -->



	</main>
	{/block}
	<!-- Main Content -->

	<!-- Footer -->

		<footer id="footer">

		    <!-- Footer columns -->
		    <div class="footer-columns">
		    	<div class="container">

		    		<!-- Columns Row -->
		    		<div class="row">

			    		<!-- Footer Column -->
			    		<div class="col-lg-4 col-sm-6">
			    			<div class="footer-column logo-column">
			    				<a href="{$this->site_url}"><img src="{base_url()}/admin718/assets/img/logo/logo.png" alt="" width="185px" height="62px"></a>

			    				<ul class="address-list">
			    					<li class="text-justify"><i class="fa fa-home"></i>Pertokoan Plaza Cirendeu <br> <span style="margin-left:11%;" >Jl. Cirendeu Raya No.20</span> <br> <span style="margin-left:11%;" >Tangerang Selatan 15419</span></li>
			    					<li><i class="fa fa-phone"></i>021 22741405</li>
			    					<li><i class="fa fa-envelope"></i>cs@mizan.com</li>
			    				</ul>
			    			</div>
			    		</div>
			    		<!-- Footer Column -->
							<!-- pos12 begin -->
							{block name="pos-12"}
								{if $this->pos_12 !=""}
									{$this->pos_12}
								{/if}

							{/block}
							<!-- pos12 end -->


		    		</div>
		    		<!-- Columns Row -->

		    	</div>
		    </div>
		    <!-- Footer columns -->

		    <!-- Sub Footer -->
		   	<div class="sub-foorer">
		   		<div class="container">
		   			<div class="row">
			   			<div class="col-sm-6">
			   				<p>Copyright <i class="fa fa-copyright"></i> 2017 <span class="theme-color">{$this->site_name}</span> All Rights Reserved.</p>
			   			</div>
			   			<div class="col-sm-6">
			   				<a class="back-top" href="#">Back to Top<i class="fa fa-caret-up"></i></a>
			   				<ul class="cards-list">
			   					<li><img src="{theme_url('images/cards/img-01.jp')}g" alt=""></li>
			   					<li><img src="{theme_url('images/cards/img-02.jp')}g" alt=""></li>
			   					<li><img src="{theme_url('images/cards/img-03.jp')}g" alt=""></li>
			   					<li><img src="{theme_url('images/cards/img-04.jp')}g" alt=""></li>
			   				</ul>
			   			</div>
		   			</div>
		   		</div>
		   	</div>
		    <!-- Sub Footer -->

		</footer>

	<!-- Footer -->

</div>
<!-- Wrapper -->


<!-- Slide Menu -->
<nav id="menu" class="responive-nav">
	<a class="r-nav-logo" href="index.html"><img src="{theme_url('images/logo-1.png')}" alt=""></a>
	<ul class="respoinve-nav-list">
		<li>
			<a class="triple-eff" data-toggle="collapse" href="#list-1"><i class="pull-right fa fa-angle-down"></i>Home</a>
			<ul class="collapse" id="list-1">
				<li><a href="index.html">home 1</a></li>
				<li><a href="index-2.html">home 2</a></li>
			</ul>
		</li>
		<li>
			<a class="triple-eff" data-toggle="collapse" href="#list-2"><i class="pull-right fa fa-angle-down"></i>Shop</a>
			<ul class="collapse" id="list-2">
				<li><a href="shop.html">shop</a></li>
				<li><a href="shop-detail.html">shop Detail</a></li>
			</ul>
		</li>
		<li>
			<a class="triple-eff" data-toggle="collapse" href="#list-3"><i class="pull-right fa fa-angle-down"></i>Blog</a>
			<ul class="collapse" id="list-3">
				<li><a href="blog-all-views.html">blog all views</a></li>
				<li><a href="blog-larg.html">blog Larg</a></li>
				<li><a href="blog-list.html">blog List</a></li>
				<li><a href="blog-grid.html">blog Grid</a></li>
				<li><a href="blog-detail.html">blog detail</a></li>
			</ul>
		</li>
		<li>
			<a class="triple-eff" data-toggle="collapse" href="#list-4"><i class="pull-right fa fa-angle-down"></i>Pages</a>
			<ul class="collapse" id="list-4">
				<li><a href="about.html">about</a></li>
				<li><a href="gallery.html">gallery</a></li>
				<li><a href="event-list.html">event list</a></li>
				<li><a href="event-detail.html">event detail</a></li>
				<li><a href="book-list.html">blog list</a></li>
				<li><a href="book-detail.html">book detail</a></li>
				<li><a href="404.html">404</a></li>
			</ul>
		</li>
		<li>
			<a class="triple-eff" data-toggle="collapse" href="#list-5"><i class="pull-right fa fa-angle-down"></i>author</a>
			<ul class="collapse" id="list-5">
				<li><a href="author.html">author</a></li>
				<li><a href="author-detail.html">author detail</a></li>
			</ul>
		</li>
		<li><a href="contact.html">Contact</a></li>
	</ul>
</nav>
<!-- Slide Menu -->

<!-- View Pages -->
<div class="modal fade open-book-view" id="open-book-view" role="dialog" tabindex="-1">
  	<div class="position-center-center" role="document">
 	   	<div class="modal-content">
 	   		<button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	    	<div id="magazine">
				<div style="background-image:url(images/pages/01.jpg);"></div>
				<div style="background-image:url(images/pages/02.jpg);"></div>
				<div style="background-image:url(images/pages/03.jpg);"></div>
				<div style="background-image:url(images/pages/04.jpg);"></div>
				<div style="background-image:url(images/pages/04.jpg);"></div>
				<div style="background-image:url(images/pages/05.jpg);"></div>
				<div style="background-image:url(images/pages/05.jpg);"></div>
				<div style="background-image:url(images/pages/06.jpg);"></div>
			</div>
    	</div>
  	</div>
</div>
<!-- View Pages -->

<!-- Pencarian Modal -->
<div class="modal fade login-modal" id="pencarian-modal" role="dialog" tabindex="-1">
	<div class="position-center-center" role="document">
		<div class="modal-content">
			<strong>Pencarian</strong>
			<button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			{assign var=aturan_form value=['method'=>'get']}
			{assign var=url_pencarian value=site_url('pencarian/hasil_pencarian')}
      {form_open($url_pencarian,$aturan_form)}
				<div class="form-group">
					<input class="form-control" required="required" placeholder="Pencarian" name="kata_pencarian" id="kata_pencarian">
				</div>
				<input type="submit" class="btn-1 shadow-0 full-width" value="Cari">
			{form_close()}
		</div>
	</div>
</div>
<!-- Pencarian Modal -->

<!-- Login Modal -->
<div class="modal fade login-modal" id="login-modal" role="dialog" tabindex="-1">
	<div class="position-center-center" role="document">
		<div class="modal-content">
			<strong>Login</strong>
			<button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			{assign var=cfg_login value=['id'=>'form_login']}
			{form_open(site_url('customer/login_proses'),$cfg_login)}
				<div class="form-group">
					<input class="form-control" required="required" placeholder="Email Address" name="email">
				</div>
				<div class="form-group">
					<input class="form-control" type="password" required="required" name="password" placeholder="Password">
				</div>
				<input type="submit" class="btn-1 shadow-0 full-width" value="Login">
			{form_close()}
					<a href="{site_url('customer/login')}" class="btn btn-primary text-center" style="width:100%;">Register</a>
		</div>
	</div>
</div>
<!-- Login Modal -->

<!-- Quick View -->
<div class="modal fade quick-view" id="quick-view" role="dialog" tabindex="-1">
	<div class="position-center-center" role="document">
		<div class="modal-content">
			<button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="single-product-detail">
				<div class="row">

					<!-- Product Thumnbnail -->
					<div class="col-sm-5">
						<div class="product-thumnbnail">
							<img id="detail_img" src="{theme_url('images/qiuck-view/img-01.jp')}g" alt="">
						</div>
					</div>
					<!-- Product Thumnbnail -->

					<!-- Product Detail -->
					<div class="col-sm-7">
						{assign var=cfg_var value=['id'=>'detail_cart']}
						{form_open('',$cfg_var)}
						<div class="single-product-detail">
							<span class="availability">Availability :<strong id="detail_status_produk">Stock<i class="fa fa-check-circle"></i></strong></span>
							<h3 id="detail_judul_produk">Land the Earth Beach</h3>
							<ul class="rating-stars" id="detail_bintang">
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star-half-o"></i></li>
								<li>1 customer review</li>
							</ul>
							<div class="prics"><del class="was" id="detail_diskon_produk">$32.00</del><span class="now" id="detail_harga_produk">$30.99</span></div>
							<h4>Overview</h4>
							<p id="detail_keterangan_produk">With this highly anticipated new novel, the author of the bestselling Life of Pi returns to the storytelling power and luminous wisdom of his master novel. The High Mountains of Portugal.</p>
						<input type="hidden" name="detail_cart_id" id="detail_cart_id" readonly>
							<div class="quantity-box">
								<label>Qty :</label>
								<div class="sp-quantity">
									<div class="sp-minus fff"><a class="ddd" data-multi="-1">-</a></div>
									<div class="sp-input">
									  <input type="text" name="detail_cart_qty" id="detail_cart_qty" class="quntity-input" value="1" min="1" />
									</div>
									<div class="sp-plus fff"><a class="ddd" data-multi="1">+</a></div>
								</div>
							</div>
							<ul class="btn-list">
								<li><button class="btn-1 sm shadow-0 ">add to cart</button></li>
								<li><a class="btn-1 sm shadow-0 blank" href="#"><i class="fa fa-heart"></i></a></li>
								<li><a class="btn-1 sm shadow-0 blank" href="#"><i class="fa fa-repeat"></i></a></li>
								<li><a class="btn-1 sm shadow-0 blank" href="#"><i class="fa fa-share-alt"></i></a></li>
							</ul>
						</div>
						{form_close()}
					</div>
					<!-- Product Detail -->

				</div>
			</div>
			<!-- Single Product Detail -->

		</div>
	</div>
</div>
<!-- Quick View -->

<!-- script js -->


<script type="text/javascript">
  var pencarian_modal = function(){
		$('#pencarian-modal').modal('show');
	}
	var modal_detail_buku = function(produk_id){
		var url="{site_url('cart_catalog/produk/detail_json')}";
		// alert(url);



		$.ajax({
			url: url+'/'+produk_id,
			type: 'GET',
			dataType: 'json',
			success: function(json){
				if(json.status == true){
					console.log(json);
					var url_gambar = "{base_url()}admin718/assets/img/com_cart/produk/"+json.detail.gambar_thumb;
					$('#detail_img').attr('src', url_gambar);
					$('#detail_status_produk').html(json.detail.nama_status);
					var html ="";
					for(i=0; i < json.nilai_star; i++){
						html +='<li><i class="fa fa-star"></i></li>';
					}
						html +='<li>'+json.total_vote+' customer review</li>';
						$('#detail_bintang').html(html);
						html ="";
						html +=json.detail.keterangan;
						html +=' <a href="'+json.detail.link_produk+'">Read More</a>'
					$('#detail_judul_produk').html(json.detail.nama);
					$('#detail_diskon_produk').html(json.detail.harga_lama);
					$('#detail_harga_produk').html(json.detail.harga);
					$("#detail_keterangan_produk").html(html);
					$('#detail_cart_id').val(produk_id);
					$('#detail_cart_qty').val(1);
					$('#quick-view').modal('show');
					/*
					 */
				}
			}
		});
	}

	$('#detail_cart').submit(function(){
		event.preventDefault();
		var detail_cart = $('#detail_cart').serializeArray();
		// console.log(detail_cart[0].value);
		var url = "{site_url('cart_order/cart_api/add_test')}/"+detail_cart[0].value;
		if(detail_cart[1].value  < 1){
			detail_cart[1].value = 1;
		}
		$.post(url, { qty: detail_cart[1].value }, function(data, textStatus, xhr) {
				// $('#pesan_notif').html(data.pesan);
				alert(data.pesan);

					var url="{site_url('cart_order/cart_api/ringkasan')}";

						$.getJSON(url, { }, function(json, textStatus) {
							/*optional stuff to do after success */
							$.each(json,function(index, el) {
								if(el.nama == "Total Item"){
									$('#total_items_cart').html(el.value)
								}
							});

						});

				$('#quick-view').modal('hide');
				//if(data.status && data.kode == '1'){
					// window.location.href = "{site_url('cart_order/cart/show')}";

				// }
		});
	})

</script>

<script src="{theme_url('js/main.js')}"></script>

{if isset($js_footer)}
			{foreach from=$js_footer item=v }
					<script src="{base_url('assets')}/{$v}"></script>
			{/foreach}
{/if}

{block name="script_js"}

{/block}

<!-- script js -->




</body>
</html>
