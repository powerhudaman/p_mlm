{extends file=$themes}
{block name="main-content"}
  <style media="screen">
    .shop-cart{
      margin: 0px 0px 20px;
      display: block;
    }
  </style>
  <!-- Keranjang begin -->
  <div class="shop-cart">
    <div class="container">
      <div class="row">
        <div class="col-md-12" id="lokasi-alert" style="margin-top:20px;"></div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <br>
          <br>
          <br>
          <h4>Keranjang Belanja</h4>
        </div>
        <div class="col-md-4">

        </div>
        <div class="col-md-4">
          <br>
          <br>
          <!-- <a href="{site_url('checkout')}" class="btn btn-primary pull-left">Checkout</a> -->
        </div>
      </div>
      {if count($carts) == 0}
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-8">
              <h4>Keranjang Belanja Anda Masih Kosong Silahkan Klik <a href="{$this->site_url}">Disini</a> </h4>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      {/if}
      {if count($carts) > 0}
        {assign var=cfg value=['class'=>'shoping-form','id'=>'frm_show']}
        {form_open(site_url('cart_order/cart/update'),$cfg)}
          <div class="row">
            <div class="col-md-8" style=" min-height:150px;">
              <table class="table">
                <thead>
                  <tr>
                    <th colspan="2">Produk</th>
                    <th>Harga Produk</th>
                    <th>Quantity</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {foreach from=$carts key=k item=cart}
                    <tr>
                      <td width="15%">
                        <img src="{base_url()}admin718/assets/img/com_cart/produk/{$cart['gambar']}" alt="" class="img img-thumbnail" width="107" height="154">
                      </td>
                      <td style="wordwrap:break-word;" width="30%">
                        <p class="text-justify">
                          {$cart['name']}
                        </p>
                        <ul>
                          <li>Sku : SKU: {$cart['bara']}</li>
                          {if count($cart['options']) > 0}
                              {foreach from=$cart['options'] key=a item=option}
                                {if count($option['opt_value']) > 0}
                                  <li>{$option['nama']} :
                                    {foreach from=$option['opt_value']  key=b item=opt_value }
                                      {$opt_value['nama']}
                                    {/foreach}
                                  </li>
                                {/if}
                              {/foreach}
                          {/if}
                        </ul>
                      </td>
                      <td width="15%">
                        <strong>Rp. {number_format($cart['price'],0,",",".")}</strong>
                      </td>
                      <td width="25%">
                        <div class="quantity-box">

                          <div class="sp-quantity">
                            <div class="sp-minus fff"><a class="ddd" data-multi="-1" onclick="update_qty('{$k}','{$cart['id']}','minus')">-</a></div>
                            <div class="sp-input">
                              <input type="text" class="quntity-input" value="{$cart['qty']}" name="cart[{$k}][quantity]" id="qty_{$k}" />
                            </div>
                            <div class="sp-plus fff"><a class="ddd" data-multi="1" onclick="update_qty('{$k}','{$cart['id']}','plus')">+</a></div>
                          </div>
                        </div>
                      </td>
                      <td width="5%" style="padding-left:5px;">
                        <a href="{site_url('cart_order/cart/hapus_cart')}/{$k}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  {/foreach}

                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2">

                        <a href="{site_url('cart_order/cart/kosongkan')}" class="btn btn-danger pull-left">Kosongkan</a>
                        <a href="{$this->site_url}" class="btn btn-warning pull-left" style="margin-left:10px;">Lanjutkan Belanja</a>
                        <a href="{site_url('checkout')}" class="btn btn-primary pull-left" style="margin-left:10px;">Checkout</a>

                    </td>
                    <td></td>
                    <td></td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div class="col-md-4" style=" min-height:150px;">
              <table>
                <thead>
                  <tr>
                    <th colspan="2">Rincian Pesanan</th>
                    {assign var=total_bayar value=0}
                  </tr>
                </thead>
                <tbody id="data-ringkasan">
                  <tr>
                    <td width="70%">Subtotal</td>
                    <td id="sub_total">Rp. {number_format($this->cart->total(),0,",",".")}</td>
                    {$total_bayar = $total_bayar + $this->cart->total()}
                  </tr>
                  <tr>
                    <td width="70%">Biaya Pengiriman : Rp. {$biaya_ongkir['kurir']} / {$biaya_ongkir['layanan']} </td>
                    <td>Rp. {number_format($biaya_ongkir['nominal'],0,",",".")} {$total_bayar = $total_bayar + $biaya_ongkir['nominal']}</td>
                  </tr>
                  {if count($promo) > 0}
                    {foreach from=$promo key=prom item=v_promo}
                      <tr>
                        <td width="70%">{$v_promo['jenis_promo']} ({$v_promo['kode']})</td>
                        <td>- Rp. {$v_promo['nilai_potongan']} {$total_bayar = $total_bayar - $v_promo['nilai_potongan']}</td>
                      </tr>
                    {/foreach}
                  {/if}
                  <tr>
                    <td width="70%">Grandtotal</td>
                    <td><strong id="grandtotal">Rp. {number_format($total_bayar,0,",",".")}</strong></td>
                  </tr>
                </tbody>

              </table>
            </div>
          </div>
        {form_close()}
      {/if}


    </div>
  </div>
  <!-- Keranjang end -->

  <!--- posisi --->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-8">
            <!-- Pos 6 Begin  -->
  					{block name="pos-6"}
  	          	{$this->pos_6}
  	        {/block}
  					<!-- Pos 6 End  -->
          </div>
          <div class="col-md-4">
            <!-- Pos 7 Begin  -->
            {block name="pos-7"}
                {$this->pos_7}
            {/block}
            <!-- Pos 7 End  -->
          </div>
        </div>
      </div>
    </div>
  <!--- posisi --->

  <!-- posisi 10 -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          {block name="pos-10"}
              {$this->pos_10}
          {/block}
        </div>
      </div>
    </div>
  <!-- posisi 10 -->

  <!-- posisi 11 -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          {block name="pos-11"}
              {$this->pos_11}
          {/block}
        </div>
      </div>
    </div>
  <!-- posisi 11 -->

{/block}
{block name=script_js}
  <script type="text/javascript">

        var ganti = function(url){
          $('#main_image').attr('src',url);
        }
        var add_cart = function(){
          $('#form_add_cart')[0].submit(function(event) {
            /* Act on the event */
          });
        }
        var add_review = function(){
          $('#form_add_review')[0].submit(function(event) {
            /* Act on the event */
          });
        }
        var update_cart = function(){
          $('#frm_show')[0].submit(function(event) {
            /* Act on the event */
          });
        }

        var update_qty = function(rowid,produk_id,operan){
            var qty = parseInt($('#qty_'+rowid).val());
            if(operan == "plus"){
              qty = qty + 1;
            }
            if(operan == "minus"){
              qty = qty - 1;
            }
            if(qty < 1){
              qty = 1;
            }
            var url = "{site_url('cart_order/cart_api/update_cart')}";
            $.post(url, { rowid:rowid, qty:qty, id :produk_id }, function(data, textStatus, xhr) {
              /*optional stuff to do after success */
              // window.location.href = "{site_url('cart_order/cart/show')}";
              // alert(qty);

              $.getJSON("{site_url('cart_order/cart_api/ringkasan')}", { }, function(json, textStatus) {
                var html ='';

                $.each(json,function(k, v) {
                  html +='<tr>';
                    html +='<td width="70%">'+v.nama+'</td>';
                    html +='<td>Rp.'+v.value+' </td>';
                  html +='</tr>';
                  if(v.nama == "Total Item"){
                    $('#total_items_cart').html(v.value);
                  }
                });
                $('#data-ringkasan').html(html);
              });

              if(data.pesan.kode == 1){
                var html ='';
                html +='<div class="alert alert-success alert-dismissable">';
                  html +='<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                  html +=data.pesan.pesan;
                html +='</div>';
                $('#qty_'+rowid).val(qty);
              }
              if(data.pesan.kode == 2){
                var html ='';
                html +='<div class="alert alert-danger alert-dismissable">';
                  html +='<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                  html +=data.pesan.pesan;
                html +='</div>';
                var qty_asal = $('#qty_'+rowid).val();
                $('#qty_'+rowid).val(qty_asal);
              }
              $('#lokasi-alert').html(html);
            });
        }
    </script>
{/block}
