{extends file=$themes}
{block name="main-content"}
  <br><br>
  
    <!-- redaksi mulai -->
    <div class="container">
        <p><strong><em>How To Buy</em></strong><strong> (Cara Berbelanja Melalui Millenniastore.id)</strong></p>
        <ol>
        <li><strong style="font-size:1.5em;">Mulai dengan memilih kategori</strong></li>
        </ol>
        <img src="{base_url('assets/img/redaksi_how_to_buy/1.1.png')}" alt=""><br>
        <p>Pencarian barang dapat dilakukan dengan memasukkan nama barang yang ingin dicari di kolom pencarian di atas.</p>
        <img src="{base_url('assets/img/redaksi_how_to_buy/1.2.png')}" alt=""><br>
        <p>Pilihan lainnya, kamu dapat melakukan pencarian barang yang kamu inginkan melalui salah satu dari kolom kategori yang tersedia: <strong><em>DEAL</em>, <em>MERCHANDISE</em>, <em>COMPUTER ACCESSORIES</em>, <em>STATIONERY</em>, <em>EDU TOYS</em>, <em>FANCY</em>, <em>BOOKS</em>, <em>OTHER</em>,</strong> dan <strong><em>HOT ITEM</em></strong>.</p>
        <ol start="2">
        <li><strong style="font-size:1.5em;">Klik dan buka barang yang kamu inginkan untuk melihat lebih detail barang</strong></li>
        </ol>
        <img src="{base_url('assets/img/redaksi_how_to_buy/2.1.png')}" alt=""><br>
        <p>Perhatikan detail ketersediaan barang pada bagian pojok kanan atas. Jika pada kolom <strong><em>availability</em></strong> tertulis <strong>tersedia</strong>, barang tersebut dapat langsung kamu beli. Masukkan jumlah barang yang kamu inginkan dengan menambahkannya di kolom <strong>Qty</strong> (Kuantitas) dan jika sudah selesai silakan klik tombol <strong><em>ADD TO CART</em></strong>.</p>
        <p>(Catatan: untuk barang yang sementara ini <strong>tidak tersedia</strong>, kamu bisa mendapat notifikasi jika di masa mendatang barang tersebut sudah tersedia kembali dengan klik <strong>tombol bertanda hati</strong> (<em>wishlist</em>) di samping tombol <strong><em>ADD TO CART</em></strong>)</p>
        <p>Ketika membuka detail barang yang akan kamu beli, terdapat <strong>kolom rekomendasi</strong> berisi barang lain yang mungkin kamu inginkan untuk ditambahkan ke dalam keranjang belanjaanmu.</p>
        <img src="{base_url('assets/img/redaksi_how_to_buy/2.2.png')}" alt=""><br>
        <ol start="3">
        <li><strong style="font-size:1.5em;">Masuk ke keranjang belanja</strong></li>
        </ol>
        <img src="{base_url('assets/img/redaksi_how_to_buy/3.1.png')}" alt=""><br><br>

        <p>Lakukan pengecekan terhadap barang belanjaanmu dengan klik <strong>keranjang</strong> yang berada di pojok kanan atas halaman website <strong>{$this->site_name}</strong>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        <img src="{base_url('assets/img/redaksi_how_to_buy/3.2.png')}" alt=""><br>
        <p>Perhatikan kolom <strong>produk</strong>, <strong>harga</strong>, dan <strong>kuantitas</strong> (qty) barang yang kamu beli. Perbaharui jumlah barang dengan klik <strong>UPDATE KERANJANG</strong>. Jika barang yang kamu beli tidak sesuai dengan yang kamu inginkan, silakan klik <strong>KOSONGKAN KERANJANG</strong> dan mulailah dengan berbelanja lagi.</p>
        <p>&nbsp;Jika semua sudah tepat seperti yang kamu inginkan, ada dua pilihan yang dapat kamu lakukan. Pertama, melanjutkan belanja barang lain dengan klik <strong>LANJUT BELANJA</strong>. Kedua, menyelesaikan proses berbelanja dengan klik <strong>CHECKOUT</strong>.</p>
        <ol start="4">
        <li><strong style="font-size:1.5em;">Selesai Berbelanja</strong></li>
        </ol>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.1.png')}" alt=""><br>
        <p>Jika kamu sudah selesai berbelanja dan menekan tombol <strong>CHECKOUT</strong> berikut adalah tampilan laman yang akan muncul dalam layar komputermu. Ada langkah-langkah yang harus dilakukan setelah <strong>CHECKOUT</strong>, yakni</p>
        <ul>
        <li><strong style="font-size:1.5em;">a. Mengisi METODE</strong></li>
        </ul>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.a.1.png')}" alt=""><br>
        <p>Jika kamu sudah mendaftarkan dirimu sebagai <em>member</em>, silakan <strong>LOGIN</strong> dengan memasukkan alamat surel (<em>e-mail</em>) dan kata sandi (<em>password</em>) ke dalam kolom yang berada di samping kanan.</p>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.a.2.png')}" alt=""><br>
        <p>Jika tampilan layar desktop telah memperlihatkan tulisan &ldquo;Selamat datang&rdquo; maka langkah selanjutnya akan menjadi lebih mudah.</p>
        <ul>
        <li><strong style="font-size:1.5em;">b. Mengisi INFORMASI DATA PEMBELI</strong></li>
        </ul>
        <p>Jika kamu sudah menjadi <em>member</em> <strong>{$this->site_name}</strong>, kolom <strong>INFORMASI DATA PEMBELI</strong> akan otomatis terisi sesuai dengan data yang telah kamu daftarkan. Jika belum menjadi <em>member</em>, silakan isi seluruh data <strong>INFORMASI DATA PEMBELI</strong> secara lengkap untuk memudahkan pendataan dalam sistem.</p>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.b.1.png')}" alt=""><br>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.b.2.png')}" alt=""><br>
        <p>Klik tombol <strong>LANGKAH SELANJUTNYA</strong> apabila telah mengisi semua kolom yang diperlukan seperti gambar di atas.</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <ul>
        <li><strong style="font-size:1.5em;">c. Mengisi INFORMASI DATA PENGIRIMAN</strong></li>
        </ul>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.c.1.png')}" alt=""><br>
        <p><strong>INFORMASI DATA PENGIRIMAN</strong> sedikit berbeda dengan <strong>INFORMASI DATA PEMBELI</strong>. Kolom <strong>INFORMASI DATA PENGIRIMAN</strong> dapat diisi persis sama dengan <strong>INFORMASI DATA PEMBELI</strong> apabila kamu ingin mengirimkan barang ke alamat yang sama, sesuai dengan data dalam <strong>INFORMASI DATA PEMBELI</strong>.</p>
        <p>Apabila data <strong>INFORMASI DATA PENGIRIMAN</strong> berbeda dengan <strong>INFORMASI DATA PEMBELI</strong> (contoh: kamu membeli untuk dikirim sebagai hadiah kepada teman/saudara/kerabat/keluarga), maka silakan cetang pernyataan <strong>&ldquo;alamat penerima berbeda dengan pembeli&rdquo;</strong> dan lakukan pengisian sesuai dengan data calon penerima barang secara lengkap.</p>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.c.2.png')}" alt=""><br>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.c.3.png')}" alt=""><br>
        <p>Pastikan seluruh data diisi dengan lengkap agar barang dapat terkirim sesuai dengan alamat yang ditujukan. Jika sudah yakin, silakan klik tombol <strong>LANGKAH SELANJUTNYA</strong>.</p>
        <ul>
        <li><strong style="font-size:1.5em;">d. Mengisi METODE PENGIRIMAN</strong></li>
        </ul>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.d.1.png')}" alt=""><br>
        <p>Langkah terakhir yang harus dilakukan sebelum menyelesaikan proses <strong>CHECKOUT</strong> adalah memilih <strong>METODE PENGIRIMAN</strong>. Terdapat tiga agen ekspedisi yang dapat dipilih, yakni <strong>JNE</strong>, <strong>TIKI</strong>, dan <strong>POS INDONESIA</strong>. Silakan pilih salah satu metode agar penghitungan total biaya yang harus dibayarkan dapat diselesaikan.</p>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.d.2.png')}" alt=""><br>
        <p>Jika sudah selesai, silakan klik tombol <strong>KONFIRMASI ORDER</strong>.</p>
        <p>&nbsp;</p>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.d.3.png')}" alt=""><br>
        <p>Halaman ini menandakan bahwa seluruh rangkaian proses belanja melalui <strong>{$this->site_name}</strong> telah selesai dilakukan. Catat alamat rekening untuk melakukan pembayaran atas barang yang telah dipesan. Jangan lupa untuk mem<strong>perhatikan tanggal kadaluarsa invoice</strong>. Lakukan pembayaran sebelum jatuh tempo agar <em>invoice</em> tidak kadaluwarsa.</p>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.d.4.png')}" alt=""><br>
        <img src="{base_url('assets/img/redaksi_how_to_buy/4.2.d.5.png')}" alt=""><br>
        <p>Periksa kembali data pesanan pada kolom <strong>RINGKASAN ORDER</strong>.&nbsp; Seluruh rangkaian pembelian barang di <strong>{$this->site_name}</strong> telah selesai. Jika belum sempat mencatat nomor rekening pembayaran dan masih ingin memeriksa <strong>RINGKASAN ORDER</strong>, silakan cek surel (<em>e-mail</em>) karena <em>invoice</em> sudah dikirimkan ke alamat tersebut.</p>
        <p>&nbsp;</p>
        <p>Terima kasih sudah berbelanja melalui <strong>{$this->site_name}</strong>!</p>
    </div>
    
    <!-- redaksi mulai -->

{/block}

