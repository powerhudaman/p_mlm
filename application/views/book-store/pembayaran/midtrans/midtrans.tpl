<div class="col-md-12">

    <div class="col-md-4"></div>
    <div class="col-md-4">
        <a href="#" class="btn btn-primary" style="width:100%;" id="pay-button">Pilih Pembayaran</a>
    </div>
    <div class="col-md-4"></div>
    <script type="text/javascript"
            src="https://app.sandbox.midtrans.com/snap/snap.js"
            data-client-key="{$client_key}"></script>
<script>
    // kirim data ke halaman finish
                 var email=$("#email").val();
                var nama_depan=$("#nama_depan").val();
                var nama_belakang=$("#nama_belakang").val();
                var payment_company=$("#payment_company").val();
                var payment_address_1=$("#payment_address_1").val();
                var payment_address_2=$("#payment_address_2").val();
                var payment_city=$("#payment_city").val();
                var payment_postcode=$("#payment_postcode").val();
                var payment_telephone=$("#payment_telephone").val();

                var propinsi=$("#propinsi").val();
                var kota=$("#kota").val();
                var kecamatan=$("#kecamatan").val();


                // var =$("#pilih_alamat_shipping").val();
                var shipping_firstname=$("#shipping_firstname").val();
                var shipping_lastname=$("#shipping_lastname").val();
                var shipping_company=$("#shipping_company").val();
                var shipping_address_1=$("#shipping_address_1").val();
                var shipping_address_2=$("#shipping_address_2").val();
                var shipping_city=$("#shipping_city").val();
                var shipping_postcode=$("#shipping_postcode").val();
                var shipping_telephone=$("#shipping_telephone").val();

                var propinsi_shipping=$("#propinsi_shipping").val();
                var kota_shipping=$("#kota_shipping").val();
                var kecamatan_shipping=$("#kecamatan_shipping").val();


    $('#pay-button').click(function (event) {
        event.preventDefault();
        $(this).attr("disabled", "disabled");

        $.ajax({
        url: '{site_url()}/cart_payment/midtrans_url/token',
        cache: false,

        success: function(data) {
            //location = data;

            console.log('token = '+data);

            var resultType = document.getElementById('result-type');
            var resultData = document.getElementById('result-data');

            function changeResult(type,data){
                $("#result-type").val(type);
                $("#result-data").val(JSON.stringify(data));
                //resultType.innerHTML = type;
                //resultData.innerHTML = JSON.stringify(data);
            }


            snap.pay(data, {

            onSuccess: function(result){
                changeResult('success', result);
                console.log(result.status_message);
                console.log(result);


                var post_data = {
                    'email':email,
                    'nama_depan':nama_depan,
                    'nama_belakang':nama_belakang,
                    'payment_company':payment_company,
                    'payment_address_1':payment_address_1,
                    'payment_address_2':payment_address_2,
                    'payment_city':payment_city,
                    'payment_postcode':payment_postcode,
                    'payment_telephone':payment_telephone,
                    'propinsi':propinsi,
                    'kota':kota,
                    'kecamatan':kecamatan,
                    'shipping_firstname':shipping_firstname,
                    'shipping_lastname':shipping_lastname,
                    'shipping_company':shipping_company,
                    'shipping_address_1':shipping_address_1,
                    'shipping_address_2':shipping_address_2,
                    'shipping_city':shipping_city,
                    'shipping_postcode':shipping_postcode,
                    'shipping_telephone':shipping_telephone,
                    'propinsi_shipping':propinsi_shipping,
                    'kota_shipping':kota_shipping,
                    'kecamatan_shipping':kecamatan_shipping
                };

                console.log(post_data);

                // kirim data ke halaman finish

                //$("#form-customer").submit();
            },
            onPending: function(result){
                changeResult('pending', result);
                console.log(result.status_message);


                var post_data = {
                    'email':email,
                    'nama_depan':nama_depan,
                    'nama_belakang':nama_belakang,
                    'payment_company':payment_company,
                    'payment_address_1':payment_address_1,
                    'payment_address_2':payment_address_2,
                    'payment_city':payment_city,
                    'payment_postcode':payment_postcode,
                    'payment_telephone':payment_telephone,
                    'propinsi':propinsi,
                    'kota':kota,
                    'kecamatan':kecamatan,
                    'shipping_firstname':shipping_firstname,
                    'shipping_lastname':shipping_lastname,
                    'shipping_company':shipping_company,
                    'shipping_address_1':shipping_address_1,
                    'shipping_address_2':shipping_address_2,
                    'shipping_city':shipping_city,
                    'shipping_postcode':shipping_postcode,
                    'shipping_telephone':shipping_telephone,
                    'propinsi_shipping':propinsi_shipping,
                    'kota_shipping':kota_shipping,
                    'kecamatan_shipping':kecamatan_shipping
                };

                console.log(post_data);
                //$("#payment-form").submit();
                $("#form-customer").submit();
            },
            onError: function(result){
                changeResult('error', 'Pastikan Anda Sudah Melengkapi Data diri Dan Kurir yang digunakan');
                {*console.log(result.status_message);*}


            }
            });
        }
        });
    });



</script>
</div>
