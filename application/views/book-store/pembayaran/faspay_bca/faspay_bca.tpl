<div class="col-md-12">
    <style media="screen">
    input[type="radio"],input[type="checkbox"]{
      height: 18px;
      width: 15%;
    }
    </style>
    <table class="table table-striped table-bordered">
        {foreach from=$pay_channel key=k item=rek}
          <tr>
            {if $rek['status'] == 1}
              <td>
                {if $total_order >= $rek['minimal_order']}
                  <input type="radio" name="kode_metode_pembayaran" id="metode_pembayaran_faspay_bca{$k}" onclick="set_pembayaran_faspay_bca({$k})" value="{$rek['kode_metode_pembayaran']}" data-key="{$k+rand(0,100)}">
                {/if}
                <img src="{$url_backend}/assets/img/com_cart/payment_logo/{$rek['logo_text']}" alt="" height="75px" width="200px">
              </td>
              <td>
                {if $total_order >= $rek['minimal_order']}
                  (<strong>{$rek['nama_bank']}</strong>)
                {/if}
                {if $total_order < $rek['minimal_order']}
                 Minimal Belanja Anda Harus {$rek['minimal_order']} Untuk Bisa Menggunakan Pembayaran Ini
                {/if}
              </td>
            {/if}
          </tr>
        {/foreach}
    </table>
</div>
<script>

var set_pembayaran_faspay_bca = function(ids) {
    // cekDataPengirim_raja();
    var kode_metode_pembayaran = $("#metode_pembayaran_faspay_bca"+ids).val();
    var kode_key = ids;
    // alert(kode_metode_pembayaran);

    // var ongkir_detail = ongkir + ' (' + kurir + ' ' + kurir_type + ' Tujuan: ' + $("#kota").val() + ')';

    $.post('{site_url("cart_payment/faspay_bca/pilih_pembayaran")}', {
        kode_metode_pembayaran: kode_metode_pembayaran,
        kode_key: kode_key
    }, function(data) {
      var parse = jQuery.parseJSON(data);
      // alert(parse.pesan);
        /*if(parse.kode == '1'){
          alert(parse.pesan);
          $("#kurir_services").html(parse.pesan);
        }else{
          alert(parse.pesan);
          biaya_ongkir(kecamatan_tujuan);
        }*/

    });
}

</script>
