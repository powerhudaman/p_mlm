{extends file=$themes}
{block name="main-content"}<br><br>
{form_open(site_url('customer/save_adress'))}
  <div class="container">
    <div class="row">
        <div class="col-md-3">
            <!-- pos5 begin -->
              {block name="pos-5"}
                {$this->pos_5}
              {/block}
            <!-- pos5 end -->
        </div>
        <div class="col-md-9">
          {form_open(site_url('customer/save_adress'))}

          <div class="form-group">

          	{form_error('nama_depan')}

            <label for="">Nama Depan <span class="text-red">*</span> </label><input type="text" name="nama_depan" id="" class="form-control" value="{set_value('nama_depan')}">

          </div>

          <div class="form-group">

          	{form_error('nama_belakang')}

            <label for="">Nama Belakang </label><input type="text" name="nama_belakang" id=""  value="{set_value('nama_belakang')}" class="form-control">

          </div>

          <div class="form-group">

          	{form_error('company')}

            <label for="">Perusahaan</label>

            <input type="text" name="company" id="" value="{set_value('company')}" class="form-control">

          </div>

          <div class="form-group">

          	{form_error('alamat_1')}

            <label for="">Alamat 1</label>

            <textarea name="alamat_1" id="" cols="30" rows="10" class="form-control">{set_value('alamat_1')}</textarea>

          </div>

          <div class="form-group">
            <label for="">Input Propinsi</label>
            <select name="id_propinsi" id="propinsi" class="form-control">
                  <option value=""></option>
                  {foreach from=$propinsi key=k item=v}
                    {assign var=pilih value=''}
                     {if $v['id'] == $prop_regis}
                       {$pilih='selected'}
                     {/if}
                    <option value="{$v['id']}" {$pilih}>{$v['provinsi']}</option>
                  {/foreach}
            </select>
          </div>

          <div class="form-group">
            <label for="">Input Kota</label>
            <select name="id_kota" id="kota" class="form-control">
                  <option value=""></option>
                  {if count($l_kota) > 0}
                  {foreach from=$l_kota key=k item =kota}
                    {assign var=pilih value=''}
                    {if $kota['id'] == $kot_regis}
                      {$pilih='selected'}
                    {/if}
                    <option value="{$kota['id']}" {$pilih}>{$kota['kota']}</option>
                  {/foreach}
                  {/if}
            </select>
          </div>

          <div class="form-group">
            <label for="">Input Kecamatan</label>
            <select name="id_kecamatan" id="kecamatan" class="form-control">
                    <option value=""></option>
                    {if count($l_kecamatan) > 0}
                      {foreach from=$l_kecamatan key=k item =kecamatan}
                        {assign var=pilih value=''}
                        {if $kecamatan['id'] == $kec_regis}
                          {$pilih='selected'}
                        {/if}
                        <option value="{$kecamatan['id']}" {$pilih}>{$kecamatan['kecamatan']}</option>
                      {/foreach}
                    {/if}
              </select>
          </div>

          <div class="form-group">

            {form_error('telephone')}

            <label for="">Telephone / Handphone</label>

            <input type="number" name="telephone" id="" value="{set_value('telephone')}" class="form-control">

          </div>

          <div class="form-group">

          	{form_error('kodepos')}

            <label for="">Kodepos</label>

            <input type="text" name="kodepos" id="" value="{set_value('kodepos')}" class="form-control">
            <input type="hidden" name="kota" id="alamat_kota" value="{set_value('kota')}" class="form-control" placeholder="Silahkan Input Kota Anda">
          </div>

          <div class="form-group">

            <input type="submit" value="Simpan" class="btn btn-primary">

          </div>



          {form_close()}
        </div>
    </div>
</div>

{/block}{block name=script_js}	<script>	// pemilihan kota	// pemilihan kota	$("#propinsi").change(function() {			// $("#kecamatan_tujuan").hide();			$("#kota").hide();			$("#kecamatan").hide();			var propinsi_origin = $("#propinsi").val();			var urls = '{site_url("cart_order/checkout/kota")}';			$.ajax({							//url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,							url: urls + '?propinsi=' + propinsi_origin,							type: 'GET',							dataType: 'json',							success: function(data) {									var html = '<option value=""</option>';									$.each(data, function(k, v) {											html += '<option value="' + v.id + '">' + v.nama_kota + '</option>';									});									$("#kota").html(html);									$("#kota").show();									$('#btn-check-4').hide();							}					});	})	// kecamatan tujuan	$("#kota").change(function() {			$("#kecamatan").hide();			var kota_origin = $("#kota").val();			var urls = '{site_url("cart_order/checkout/kecamatan")}';			$.ajax({							//url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,							url: urls + '?kota=' + kota_origin,							type: 'GET',							dataType: 'json',							success: function(data) {									var html = '<option value=""</option>';									$.each(data, function(k, v) {											html += '<option value="' + v.id + '">' + v.nama_kecamatan + '</option>';									});									$("#kecamatan").html(html);									$("#kecamatan").show();									$('#btn-check-4').hide();							}					});	});	// pilih tujuan	$("#kecamatan").change(function(){			var propinsi_origin = $("#propinsi").val();			var kota_origin = $("#kota").val();			var kecamatan = $('#kecamatan').val();			var urls = '{site_url("cart_order/checkout/kecamatan_detail_indonesia")}';			$.post(urls, { kecamatan: kecamatan }, function(data, textStatus, xhr) {				/*optional stuff to do after success */				data = jQuery.parseJSON(data);				$('#alamat_kota').val(data.nama_kecamatan);			});	});	</script>{/block}