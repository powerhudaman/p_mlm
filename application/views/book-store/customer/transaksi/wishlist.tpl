{extends file=$themes}
{block name="tag_body"}
  <body>
{/block}
{block name="main-content"}
      <br>
          <div class="container">
              <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                  {if $this->session->flashdata('pesan') !=""}
                    {$this->session->flashdata('pesan')}
                  {/if}
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                    <!-- pos5 begin -->
                      {block name="pos-5"}
                        {$this->pos_5}
                      {/block}
                    <!-- pos5 end -->
                </div>
                  <div class="col-md-9">
                      <p class="title-left">Wishlist Saya</p>

                      <div class="contact-form shop-sidebar-content">
                        <div class="table-responsive">
                          <table class="table">
                            {if $wishlists!=0}
                            <thead>
                              <td>No</td>
                              <td>SKU Produk</td>
                              <td>Nama Produk</td>
                              <td>Status Produk</td>
                              <td>Tanggal Wishlist</td>
                              <td></td>
                            </thead>
                            <tbody>
                              {assign var=no value=0}
                              {foreach from=$wishlists key=k item=wishlist}
                              {$no = $no+1}
                              <tr class="cart_item">
                                <td>{$no}</td>
                                <td class="text-left">{$wishlist['sku']}</td>
                                <td class="text-left">
                                  {if $wishlist['judul_produk'] ==""}
                                      {$wishlist['nama']}
                                  {/if}
                                  {if $wishlist['judul_produk'] !=""}
                                      {$wishlist['judul_produk']}
                                  {/if}
                                </td>
                                <td>{$wishlist['status_produk']}</td>
                                <td>{$wishlist['tgl_wishlist']}</td>
                                <td>
                                  {if $wishlist['status_buy'] == '0'}
                                  <a href="#" class="btn btn-sm btn-success" title="Add To Cart" alt="Log Aktivitas Invoice" onclick="add_to_cart({$wishlist['produk_id']})"><i class="fa fa-cart-plus"></i></a>
                                  {/if}
                                  <a href="{site_url('customer/transaksi/hapus_wishlist')}/{$wishlist['produk_id']}" class="btn btn-sm btn-danger" title="Hapus Wishlist" alt="Log Pembayaran Invoice"><i class="fa fa-trash"></i></a>
                                </td>
                              </tr>
                              {/foreach}
                            </tbody>
                            {/if}
                            {if $wishlists == 0}
                              <tr>
                                <td>
                                    <h4>Anda Belum Pernah Menambahkan Produk Ke Dalam Wishlist</h4>
                                </td>
                              </tr>
                            {/if}
                          </table>
                        </div>
                      </div>
                  </div>
              </div>

          </div>
        <br>
        <br>
        <br>
        <br>
        <br>
{/block}

{block name=script_js}
	<script>
  var add_to_cart = function(id_produk){
    var url = "{site_url('cart_order/cart_api/add_test')}/"+id_produk;
    $.post(url, { qty: '1' }, function(data, textStatus, xhr) {
        $('#pesan_notif').html(data.pesan);
        $("#pesan_notif").load(location.href + " #pesan_notif");
        if(data.status && data.kode == '1'){
          window.location.href = "{site_url('customer/transaksi/wishlist')}";
        }
    });
  }
	</script>
{/block}
