{extends file=$themes}
{block name="pos-6" append}


			<div class="row">
				<div class="col-sm-12 col-md-12 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Masukan ALamat Email Anda Untuk Melakukan Reset Password</h2>
						{form_open(site_url('customer/send_token_reset'))}

							{if $this->session->flashdata('pesan') !=""}
			                    {$this->session->flashdata('pesan')}
			                  {/if}

							{form_error('email')}
							<input type="email" name="email" placeholder="Email Address" />
							<div class="col-sm-12 col-md-12">
								<div class="col-sm-6 col-md-6">
									<button type="submit" class="btn btn-default">Reset</button>
								</div>
							</div>
						{form_close()}
					</div><!--/login form-->
				</div>
			</div>


{/block}

{block name=script_js}

{/block}
