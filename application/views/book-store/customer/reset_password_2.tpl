{extends file=$themes}
{block name="pos-6" append}


			<div class="row">
				<div class="col-sm-12 col-md-12 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Silahkan Masukan Password Yang Baru</h2>
						{form_open(site_url('customer/do_reset_password'))}

							{if $this->session->flashdata('pesan') !=""}
			                    {$this->session->flashdata('pesan')}
			                  {/if}

							{form_error('password')}
							<label for="">Password Baru</label>
							<input type="password" name="password" placeholder="Password" />
							{form_error('password_konfirmasi')}
							<label for="">Password Konfirmasi</label>
							<input type="password" name="password_konfirmasi" placeholder="Password Konfirmasi" />
							<div class="col-sm-12 col-md-12">
								<div class="col-sm-6 col-md-6">
									<button type="submit" class="btn btn-default">Simpan</button>
								</div>
							</div>
						{form_close()}
					</div><!--/login form-->
				</div>
			</div>


{/block}

{block name=script_js}

{/block}
