{extends file=$themes}

{block name="main-content"}
  <!-- begin resgitrasi login -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <br>
          <br>
          {if $this->session->flashdata('pesan') !=""}
            {$this->session->flashdata('pesan')}
          {/if}
          <br>

        </div>
      </div>
      <div class="row">
        <div class="col-md-5">
          {assign var=cfg_login value=['id'=>'form_login']}
          {form_open(site_url('customer/login_proses'),$cfg_login)}


          <div class="form-group">
            <h4>Login Customer</h4>
          </div>
          <div class="form-group">
            <label for="">{form_error('email')}</label>
            <input type="text" placeholder="Your email" name="email">
          </div>
          <div class="form-group">
            <label for="">{form_error('password')}</label>
            <input type="password" name="password" placeholder="Your Password">
          </div>
          <div class="form-group">
            <a class="btn btn-warning" href="#" onclick="login()">Login</a>
          </div>
          {form_close()}
        </div>
        <div class="col-md-7">
          {assign var=cfg_register value=['id'=>'form_register']}
          {form_open(site_url('customer/register_proses'),$cfg_register)}
          <div class="form-group">
            <h4>Registrasi Customer</h4>
          </div>
          <div class="form-group">

            <label for="">{form_error('email')}</label>
            <input type="email" name="email" id="" class="form-control" value="{set_value('email')}" placeholder="Silahkan Input Email Anda">
          </div>
          <div class="form-group">

            <label for="">{form_error('password')}</label>
            <input type="password" name="password" id="" class="form-control" value="" placeholder="Silahkan Input Password Anda">
          </div>
          <div class="form-group">

            <label for="">{form_error('password_konfirm')}</label>
            <input type="password" name="password_konfirm" id="" class="form-control" value="" placeholder="Silahkan Input Password Konfirmasi Anda">
          </div>
          <div class="form-group">

            <label for="">{form_error('nama_depan')}</label>
            <input type="text" name="nama_depan" id="" class="form-control" value="{set_value('nama_depan')}" placeholder="SIlahkan Input Nama Depan Anda">
          </div>
          <div class="form-group">

            <label for="">{form_error('nama_belakang')}</label>
            <input type="text" name="nama_belakang" id="" class="form-control" value="{set_value('nama_belakang')}" placeholder="Silahkan Input Nama Belakang Anda">
          </div>
          <div class="form-group">

            <label for="">{form_error('tgl_lahir')}</label>
            <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control" value="{set_value('tgl_lahir')}" placeholder="Silahkan Input Tanggal Lahir Anda">
          </div>
          <div class="form-group">

            <label for="">{form_error('jenis_kelamin')}</label>
            <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
              <option value="L" {if $this->input->post('jenis_kelamin') == 'L'} selected {/if}>Laki-laki</option>
              <option value="P" {if $this->input->post('jenis_kelamin') == 'P'} selected {/if}>Perempuan</option>
            </select>
          </div>
          <div class="form-group">

            <label for="">{form_error('telephone')}</label>
            <input type="text" name="telephone" class="form-control" value="{set_value('telephone')}" placeholder="Silahkan Input No Telpon Anda">
          </div>
          <div class="form-group">

            <label for="">{form_error('newsletter')}</label>
            <input type="checkbox" name="newsletter" checked="checked" id="" value="1">
          </div>
          <div class="form-group">

            <label for="">{form_error('company')}</label>
            <input type="text" name="company" id="" value="{set_value('company')}" class="form-control" placeholder="Silahkan Input Tempat Anda Bekerja Atau Sekolah">
          </div>
          <div class="form-group">
            <label for="">Input Propinsi</label>
            <select name="id_propinsi" id="propinsi" class="form-control">
                  <option value=""></option>
                  {foreach from=$propinsi key=k item=v}
                    {assign var=pilih value=''}
                     {if $v['id'] == $prop_regis}
                       {$pilih='selected'}
                     {/if}
                    <option value="{$v['id']}" {$pilih}>{$v['provinsi']}</option>
                  {/foreach}
            </select>
          </div>
          <div class="form-group">
            <label for="">Input Kota</label>
            <select name="id_kota" id="kota" class="form-control">
                  <option value=""></option>
                  {if count($l_kota) > 0}
                  {foreach from=$l_kota key=k item =kota}
                    {assign var=pilih value=''}
                    {if $kota['id'] == $kot_regis}
                      {$pilih='selected'}
                    {/if}
                    <option value="{$kota['id']}" {$pilih}>{$kota['kota']}</option>
                  {/foreach}
                  {/if}
            </select>
          </div>
          <div class="form-group">
            <label for="">Input Kecamatan</label>
            <select name="id_kecamatan" id="kecamatan" class="form-control">
                    <option value=""></option>
                    {if count($l_kecamatan) > 0}
                      {foreach from=$l_kecamatan key=k item =kecamatan}
                        {assign var=pilih value=''}
                        {if $kecamatan['id'] == $kec_regis}
                          {$pilih='selected'}
                        {/if}
                        <option value="{$kecamatan['id']}" {$pilih}>{$kecamatan['kecamatan']}</option>
                      {/foreach}
                    {/if}
              </select>
          </div>
          <div class="form-group">

            <label for="">{form_error('alamat_1')}</label>
            <textarea name="alamat_1" id="" cols="30" rows="5" class="form-control" placeholder="Silahkan Input Alamat Anda">{set_value('alamat_1')}</textarea>
          </div>
          <div class="form-group">

            <label for="">{form_error('kodepos')}</label>
            <input type="hidden" name="kota" id="alamat_kota" value="{set_value('kota')}" class="form-control" placeholder="Silahkan Input Kota Anda">
            <input type="number" name="kodepos" id="" value="{set_value('kodepos')}" class="form-control" placeholder="Silahkan Input Kodepos Anda">
          </div>
          <div class="form-group">
            <a class="btn btn-success" onclick="register()" href="#">Register</a>
          </div>
          {form_close()}
          <br>
          <br>
          <br>
        </div>
      </div>
    </div>
  <!-- end resgitrasi login -->
{/block}

{block name=script_js}
	<script>
	$('#tgl_lahir').datepicker({
		dateFormat:'yy-mm-dd',
		changeYear:true,
		changeMonth:true
	});
	var login = function(){
		$('#form_login')[0].submit(function(event) {
			/* Act on the event */
		});
	}

	var register = function(){
		$('#form_register')[0].submit(function(event) {
			/* Act on the event */
		});
	}

	// pemilihan kota
	// pemilihan kota
	$("#propinsi").change(function() {
			// $("#kecamatan_tujuan").hide();
			$("#kota").hide();
			$("#kecamatan").hide();
			var propinsi_origin = $("#propinsi").val();
			var urls = '{site_url("cart_order/checkout/kota")}';
			$.ajax({
							//url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
							url: urls + '?propinsi=' + propinsi_origin,
							type: 'GET',
							dataType: 'json',
							success: function(data) {
									var html = '<option value=""</option>';
									$.each(data, function(k, v) {

											html += '<option value="' + v.id + '">' + v.nama_kota + '</option>';
									});

									$("#kota").html(html);
									$("#kota").show();
									$('#btn-check-4').hide();
							}
					});
	})

	// kecamatan tujuan
	$("#kota").change(function() {
			$("#kecamatan").hide();
			var kota_origin = $("#kota").val();
			var urls = '{site_url("cart_order/checkout/kecamatan")}';
			$.ajax({
							//url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
							url: urls + '?kota=' + kota_origin,
							type: 'GET',
							dataType: 'json',
							success: function(data) {
									var html = '<option value=""</option>';
									$.each(data, function(k, v) {
											html += '<option value="' + v.id + '">' + v.nama_kecamatan + '</option>';
									});

									$("#kecamatan").html(html);
									$("#kecamatan").show();
									$('#btn-check-4').hide();
							}
					});
	});

	// pilih tujuan
	$("#kecamatan").change(function(){

			var propinsi_origin = $("#propinsi").val();
			var kota_origin = $("#kota").val();
			var kecamatan = $('#kecamatan').val();
			var urls = '{site_url("cart_order/checkout/kecamatan_detail_indonesia")}';

			$.post(urls, { kecamatan: kecamatan }, function(data, textStatus, xhr) {
				/*optional stuff to do after success */
				data = jQuery.parseJSON(data);
				$('#alamat_kota').val(data.nama_kecamatan);
			});
	});

	</script>
{/block}
