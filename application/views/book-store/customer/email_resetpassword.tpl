<div style="font-family:\"verdana, helvetica\"; font-size: 1.2em; color:#302E2E;">
  <p>&nbsp;</p>
  <p>
    Salam :)<br /><br />
    Kami menerima permintaan reset password untuk akun Anda di {$site_name} atasnama email ini.<br />
    Jika bukan Anda, mohon abaikan email ini. Tetapi jika iya, untuk lanjut memproses reset password, silahkan klik link / tautan dibawah ini:<br /><br />

    <a style="background: #34d939;
    background-image: -webkit-linear-gradient(top, #34d939, #32b82b);
    background-image: -moz-linear-gradient(top, #34d939, #32b82b);
    background-image: -ms-linear-gradient(top, #34d939, #32b82b);
    background-image: -o-linear-gradient(top, #34d939, #32b82b);
    background-image: linear-gradient(to bottom, #34d939, #32b82b);
    -webkit-border-radius: 0.5em;
    -moz-border-radius: 0.5em;
    border-radius: 0.5em;
    font-family: Arial;
    color: #ffffff;
    font-size: 1em;
    padding: 0.5em 1em 0.5em 1em;
    text-decoration: none;" href="{site_url('customer/form_reset_password/')}/{$code}">Reset Password</a><br /><br><br>
    Link tersebut berlaku 1 jam dari waktu email ini terkirim.<br />
    Terima kasih.<br />
    --<br />
  <strong>{$site_name}</strong></p>
</div>
