{extends file=$themes}
{block name="pos-6" append}

			<div class="row">
				<div class="col-sm-12 col-md-12 col-sm-offset-1">
					<div class=""><!--login form-->
						<h2>Registrasi</h2>
						{form_open(site_url('customer/register_proses'))}

							{if $this->session->flashdata('pesan') !=""}
			                    {$this->session->flashdata('pesan')}
			                {/if}

			                <div class="form-group">
								{form_error('email')}
								<label for="">Email <span class="text-red">*</span></label>
								<input type="email" name="email" id="" class="form-control" value="{set_value('email')}">
							</div>
							<div class="form-group">
								{form_error('password')}
								<label for="">Password <span class="text-red">*</span></label>
								<input type="password" name="password" id="" class="form-control" value="">
							</div>
							<div class="form-group">
								{form_error('password_konfirm')}
								<label for="">Konfirmasi Password <span class="text-red">*</span></label>
								<input type="password" name="password_konfirm" id="" class="form-control" value="">
							</div>

							<div class="form-group">
								{form_error('nama_depan')}
								<label for="">Nama Depan <span class="text-red">*</span></label>
								<input type="text" name="nama_depan" id="" class="form-control" value="{set_value('nama_depan')}">
							</div>
							<div class="form-group">
								{form_error('nama_belakang')}
								<label for="">Nama Belakang</label>
								<input type="text" name="nama_belakang" id="" class="form-control" value="{set_value('nama_belakang')}">
							</div>

							<div class="form-group">
								{form_error('tgl_lahir')}
								<label for="">Tanggal Lahir</label>
								<input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control" value="{set_value('tgl_lahir')}">
							</div>

							<div class="form-group">
								{form_error('jenis_kelamin')}
								<label for="">Jenis Kelamin</label> <br>
								<input type="radio" name="jenis_kelamin" id="" value="L"> Laki - Laki <br>
								<input type="radio" name="jenis_kelamin" id="" value="P"> Perempuan
							</div>

							<div class="form-group">
								{form_error('telephone')}
								<label for="">No Telephone / Handphone</label>
								<input type="text" name="telephone" id="" class="form-control" value="{set_value('telephone')}">
							</div>
							<div class="form-group">
								{form_error('newsletter')}
								<input type="checkbox" name="newsletter" checked="checked" id="" value="1">
								<label for="">Newsletter</label>
							</div>

							<div class="form-group">
							  	{form_error('company')}
							    <label for="">Perusahaan</label>
							    <input type="text" name="company" id="" value="{set_value('company')}" class="form-control">
							</div>
							<div class="form-group">
							  	{form_error('alamat_1')}
							    <label for="">Alamat 1</label>
							    <textarea name="alamat_1" id="" cols="30" rows="10" class="form-control">{set_value('alamat_1')}</textarea>
							</div>
							<div class="form-group">
							  	{form_error('alamat_2')}
							    <label for="">Alamat 2</label>
							    <textarea name="alamat_2" id="" cols="30" rows="10" class="form-control">{set_value('alamat_2')}</textarea>
							</div>
							<div class="form-group">
							  	{form_error('kota')}
							    <label for="">Kota</label>
							    <input type="text" name="kota" id="" value="{set_value('kota')}" class="form-control">
							</div>
							<div class="form-group">
							  	{form_error('kodepos')}
							    <label for="">Kodepos</label>
							    <input type="text" name="kodepos" id="" value="{set_value('kodepos')}" class="form-control">
							</div>


							<div class="form-group"><input type="submit" value="Simpan" class="btn btn-primary"></div>


						{form_close()}
					</div><!--/login form-->
				</div>
			</div>

{/block}

{block name=script_js}

	<script>
	$('#tgl_lahir').datepicker({
		dateFormat:'yy-mm-dd',
		changeYear:true,
		changeMonth:true
	});
	</script>

{/block}
