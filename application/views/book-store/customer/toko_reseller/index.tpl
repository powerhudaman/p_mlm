{extends file=$themes}
{block name="tag_body"}
  <body>
{/block}
{block name="main-content"}
  <br><br>
  <div class="container">

      <div class="row">
          <div class="col-md-3">

                            <!-- pos5 begin -->
                            {block name="pos-5"}
                              {$this->pos_5}
                            {/block}
                            <!-- pos5 end -->
          </div>
          <div class="col-md-9">
              {$this->session->flashdata('pesan')}
              <h4 class="title-left">Data Toko</h4>

              <div class="contact-form shop-sidebar-content">
                {form_open_multipart(site_url('customer/toko_reseller/save_toko'))}
                  <div class="form-group">
                    <label for="">Nama Toko</label>
                    <input type="text" name="nama_toko" id="nama_toko" class="form-control" value="{$toko['nama_toko']}">
                  </div>
                  <div class="form-group">
                    <label for="">Tagline</label>
                    <input type="text" name="tagline" id="tagline" class="form-control" value="{$toko['tagline']}">
                  </div>
                  <div class="form-group">
                    <label for="">Url</label>
                    <input type="text" name="url" id="url" class="form-control" value="{$toko['url']}">
                  </div>
                  <div class="form-group">
                    <label for="">Logo Toko</label>
                    <input type="file" name="gambar" id="gambar" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Alamat Toko</label>
                    <textarea name="alamat" id="" cols="30" rows="10" class="form-control">{$toko['alamat']}</textarea>
                  </div>
                  <div class="form-group">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                  </div>
                {form_close()}
              </div>
          </div>
      </div>

  </div>


{/block}

{block name=script_js}
	
{/block}
