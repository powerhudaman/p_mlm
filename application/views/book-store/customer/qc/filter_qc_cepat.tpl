{extends file=$themes}
{block name="tag_body"}
  <body>
{/block}
{block name="main-content"}

  <div class="main-content">
      <br>
      <div class="contact-top">
          <div class="container">

              <div class="row">
                  <div class="col-md-3">
                      <div class="sidebar-categories">
                          <div class="shop-sidebar">
                              <div class="sidebar-title">Account</div>
                              <div class="shop-sidebar-content">
                                  <div class="categories-sidebar">
                                    <!-- pos5 begin -->
                                    {block name="pos-5"}
                                      {$this->pos_5}
                                    {/block}
                                    <!-- pos5 end -->
                                  </div>

                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-9">
                      <div class="row">
                        <div class="col-md-12">
                          {assign var=aturan_form value=['method'=>'get']}
                          {form_open(site_url('customer/qc/tampil_cepat'),$aturan_form)}
                          <div class="form-group">
                            <label for="">Filter Range Waktu Update</label><br>
                            <div class="col-md-6">
                              <input type="text" name="range_a" id="range_a" class="form-control" style="position: relative; z-index: 100000;">
                            </div>
                            <div class="col-md-6">
                              <input type="text" name="range_b" id="range_b" class="form-control" style="position: relative; z-index: 100000;">
                            </div>
                          </div>
                          <div class="form-group">
                              <br><br>
                              <input type="submit" value="Cari" class="btn btn-success">
                          </div>
                          {form_close()}
                        </div>
                      </div>
                  </div>
              </div>

          </div>
      </div>
  </div>
{/block}

{block name=script_js}
	<script>
    $('#range_a').datepicker({
  		dateFormat:'yy-mm-dd',
  		changeYear:true,
  		changeMonth:true
  	});

    $('#range_b').datepicker({
  		dateFormat:'yy-mm-dd',
  		changeYear:true,
  		changeMonth:true
  	});

  // var total_order = function()


	</script>
{/block}
