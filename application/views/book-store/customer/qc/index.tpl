{extends file=$themes}
{block name="tag_body"}
  <body>
{/block}
{block name="main-content"}

  <div class="main-content">
      <br>
      <div class="contact-top">
          <div class="container">

              <div class="row">
                  <div class="col-md-3">
                      <div class="sidebar-categories">
                          <div class="shop-sidebar">
                              <div class="sidebar-title">Account</div>
                              <div class="shop-sidebar-content">
                                  <div class="categories-sidebar">
                                    <!-- pos5 begin -->
                                    {block name="pos-5"}
                                      {$this->pos_5}
                                    {/block}
                                    <!-- pos5 end -->
                                  </div>

                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-9">
                      <div class="row">
                        <div class="col-md-12">
                          <a href="{site_url('customer/qc/filter_qc_cepat')}" class="btn btn-warning pull-right">Qc Cepat</a>
                        </div>
                        <div class="col-md-12">
                          {assign var=aturan_form value=['method'=>'get']}
                          {form_open(site_url('customer/qc/tampil'),$aturan_form)}
                          <h4>Filter Produk Yang Akan Di QC</h4>
                          <div class="form-group">
                            {form_error('parent_id')}
                            <label for="">Kategori Produk (Wajib Di isi)</label>
                            <select name="ids_kategori" id="ids_kategori" class="form-control" required>
                              <option value=""></option>
                                {foreach from=$kategoris key=k item=kategori}
                                  <option value="{$kategori['id']}">{$kategori['nama']}</option>
                                  {if count($kategori['subs']) > 0}
                                    {foreach from=$kategori['subs'] key=a item=sub}
                                      <option value="{$sub['id']}">{$sub['nama']}</option>
                                    {/foreach}
                                  {/if}
                                {/foreach}
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="">Filter Range Waktu Update</label><br>
                            <div class="col-md-6">
                              <input type="text" name="range_a" id="range_a" class="form-control" style="position: relative; z-index: 100000;">
                            </div>
                            <div class="col-md-6">
                              <input type="text" name="range_b" id="range_b" class="form-control" style="position: relative; z-index: 100000;">
                            </div>
                          </div>
                          <div class="form-group">
                              <label for="">Status QC</label>
                              <select name="qc" id="status_qc" class="form-control">
                                <option value=""></option>
                                <option value="0">Review Ulang</option>
                                <option value="1">Daftar Perbaikan</option>
                                <option value="2">Belum Review</option>
                                <option value="3">Ok</option>
                              </select>
                          </div>
                          <div class="form-group">
                              <br><br>
                              <input type="submit" value="Cari" class="btn btn-success">
                          </div>
                          {form_close()}
                        </div>
                      </div>
                  </div>
              </div>

          </div>
      </div>
  </div>

  <div class="modal fade" id="log-history" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="z-index:100000;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Histori Status Invoice</h4>
        </div>
        <div class="modal-body" id="body-history">
          <!--<table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No Invoice </th>
                        <th>Deskripsi</th>
                        <th>Status Order</th>
                        <th>Tanggal</th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="data_history in data_historys" class="ng-scope">
                      <td class="ng-binding">inv-985/2017-02/9163787328282</td>
                      <td class="ng-binding">Nurhuda Melakukan Konfirmasi Pembayaran untuk Invoice inv-985/2017-02/9163787328282</td>
                      <td class="ng-binding">Konfirmasi</td>
                      <td class="ng-binding">2017-02-13 07:49:54</td>
                    </tr>
                  </tbody>
          </table>-->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="log-pembayaran" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Histori Pembayaran Invoice</h4>
        </div>
        <div class="modal-body" id="body-pembayaran">
          <table class="table table-bordered table-striped">
                    <thead>
                      <tr>

                      </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="data_pembayaran in data_pembayarans" class="ng-scope">
                        <td class="ng-binding">inv-985/2017-02/9163787328282</td>
                        <td class="ng-binding">2017-02-13</td>
                        <td class="ng-binding">BCA</td>
                        <td class="ng-binding text-right">530.000</td>
                        <td class="ng-binding text-right">530.000</td>
                        <td></td>
                    </tr>
                  </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="detail-order" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Detail Order</h4>
        </div>
        <div class="modal-body" id="body-detail">
          <table class="table table-bordered table-striped">
                    <tbody><tr>
                      <th>Id Produk</th>
                      <th>Bara</th>
                      <th>Nama Produk</th>
                      <th>Harga</th>
                      <th>Quantity</th>
                      <th>Subtotal</th>
                    </tr>
                    <!-- ngRepeat: data_produk in detail_produks --><tr ng-repeat="data_produk in detail_produks" class="ng-scope">
                      <td class="ng-binding">3</td>
                      <td class="ng-binding">4437</td>
                      <td class="ng-binding">CAL CITIZEN CT-500 cba</td>
                      <td class="ng-binding text-right">39.000</td>
                      <td class="ng-binding text-right">10</td>
                      <td class="ng-binding text-right">39.0000</td>
                    </tr><!-- end ngRepeat: data_produk in detail_produks --><tr ng-repeat="data_produk in detail_produks" class="ng-scope">
                      <td class="ng-binding">2</td>
                      <td class="ng-binding">2127</td>
                      <td class="ng-binding">SULING TREND</td>
                      <td class="ng-binding text-right">7.000</td>
                      <td class="ng-binding text-right">15</td>
                      <td class="ng-binding text-right">105.000</td>
                    </tr><!-- end ngRepeat: data_produk in detail_produks -->
                    <tr>
                      <td colspan="6"></td>
                    </tr>
                    <!-- ngRepeat: order_total in order_totals --><tr ng-repeat="order_total in order_totals" class="ng-scope">
                      <td colspan="5"><strong class="ng-binding">Sub Total</strong></td>
                      <td class="ng-binding text-right">495.000</td>
                    </tr><!-- end ngRepeat: order_total in order_totals --><tr ng-repeat="order_total in order_totals" class="ng-scope">
                      <td colspan="5"><strong class="ng-binding">Pengiriman (Jne - CTC 1 Kg)</strong></td>
                      <td class="ng-binding text-right">35.000</td>
                    </tr><!-- end ngRepeat: order_total in order_totals --><tr ng-repeat="order_total in order_totals" class="ng-scope">
                      <td colspan="5"><strong class="ng-binding">Total Tagihan</strong></td>
                      <td class="ng-binding text-right">530.000</td>
                    </tr><!-- end ngRepeat: order_total in order_totals -->
                  </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

{/block}

{block name=script_js}
	<script>
    $('#range_a').datepicker({
  		dateFormat:'yy-mm-dd',
  		changeYear:true,
  		changeMonth:true
  	});

    $('#range_b').datepicker({
  		dateFormat:'yy-mm-dd',
  		changeYear:true,
  		changeMonth:true
  	});

  // var total_order = function()


	</script>
{/block}
