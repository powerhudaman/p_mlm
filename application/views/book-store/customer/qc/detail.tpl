{extends file=$themes} {block name="main-content"}
<br><br>
<div class="categories main-content">
    <div class="container">
        <div class="breadcrumb-sidebar">
            <div class="breadcrumb-wrap">
                <nav class="breadcrumb-trail breadcrumbs">
                    <ul class="trail-items">
                        <li class="trail-item trail-begin">
                            <a href="{$this->site_url}"><span>Home</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="product-box product-box-primary">
            <div class="product-box-content">
                <div class="row">
                    <div class="col-md-5">
                        <figure class="img-product">
                            <img id="main_image" class="img-product-size" src="{base_url()}admin718/assets/img/com_cart/produk/{$detail['gambar_detail']}" alt="{$detail['nama']}" height="536" width="467">
                            <div class="img-room"><img src="{theme_url('assets/images/room.jpg')}" alt="room" height="32" width="32"></div>
                        </figure>
                        <div class="featue-slide supermartket-owl-carousel" data-number="3" data-margin="15" data-navcontrol="yes">
                            {assign var=aktif value=''} {assign var=total_gambar value='0'} {foreach from=$detail['gambars'] key=k item=gambar} {$total_gambar = $total_gambar + 1} {if total_gambar == 1} {$aktif ='active'} {/if}
                            <div class="feature-slide-item">
                                <figure style="max-width:126px; min-height:145px;">
                                    <a href="#main_image" onclick="ganti('{base_url()}admin718/assets/img/com_cart/produk/{$gambar['gambar']}')">
                                        <img src="{base_url()}admin718/assets/img/com_cart/produk/{$gambar['gambar']}" alt="" width="126" height="143">
                                    </a>
                                </figure>
                            </div>
                            {/foreach}
                        </div>
                    </div>
                    {if array_key_exists('tombol',$detail)} {assign var=cfg value=['id'=>'form_add_cart']} {form_open($detail['tombol'],$cfg)} {/if}
                    <div class="col-md-7">
                        <div class="single-product-content">
                            {if $this->session->flashdata('pesan') !=""} {$this->session->flashdata('pesan')} {/if} {if $nilai_star > 0}
                            <p class="rating-star">
                                <span style="color:#757070; margin-right:5px;">Customer Reviews : </span> {for $star=1 to $nilai_star}
                                <i class="fa fa-star"></i> {/for}
                                <span style="color:#757070; margin-left:5px;">({$total_vote} Review)</span>
                            </p>
                            {/if} {if $detail['judul_produk'] !=''}
                            <h3 class="product-title">{$detail['judul_produk']}</h3> {/if} {if $detail['judul_produk'] ==''}
                            <h3 class="product-title">{$detail['nama']}</h3> {/if}
                            <a href="#" class="write">Write Your Review</a> {if $detail['harga_lama'] !='' && $detail['harga_lama'] > 0}
                            <p class="product-cost"> <span class="product-cost-discount">Rp. {number_format($detail['harga_lama'],0,",",".")}</span></p>
                            <p class="product-cost">Rp. {number_format($detail['harga'],0,",",".")}</p>

                            {/if} {if $detail['harga_lama'] ==''}
                            <p class="product-cost">{number_format($detail['harga'],0,",",".")}</p>
                            {/if}

                            <p class="stock">Availability:<span><i class="fa fa-check-square-o" aria-hidden="true"></i>{$detail['nama_status']}</span></p>
                            <div class="desc-product-title">Quick OverView</div>
                            <div class="desc-product">{word_limiter(stripslashes($detail['keterangan']),30)}
                                <div class="social">
                                    <div class="facebook-social">
                                        <a href="#"><img src="{theme_url('assets/images/detail1.jpg')}" alt="detail1" height="20" width="53"></a>
                                        <span class="amount-folow">5</span>
                                    </div>
                                    <div class="tweet-social">
                                        <a href="#"><img src="{theme_url('assets/images/detail2.jpg')}" alt="detail1" height="20" width="60"></a>
                                    </div>
                                    <div class="pin-it-social">
                                        <a href="#"><img src="{theme_url('assets/images/detail3.jpg')}" alt="detail1" height="20" width="38"></a>
                                    </div>
                                    <div class="share-social">
                                        <a href="#"><img src="{theme_url('assets/images/detail4.jpg')}" alt="detail1" height="20" width="50"></a>
                                        <span class="amount-folow">6</span>
                                    </div>
                                </div>
                                {if array_key_exists('options',$detail)} {foreach from=$detail['options'] key=k item=op}
                                <div class="select-detail {if $op['required'] > 0}Required{/if}">
                                    <span>{$op['nama']}</span> {if $op['type'] == 'text'}
                                    <input type="text" name="option[{$k}][{$op['produk_option_id']}]" value="{$op['value']}" placeholder="{$op['name']}" id="option{$op['produk_option_id']}" class="form-control" length="300" {if $op[ 'required']> 0}required{/if}
                                    /> {/if} {if $op['type'] == 'textarea'}
                                    <textarea name="option[{$k}][{$op['produk_option_id']}]" id="option{$op['produk_option_id']}" cols="30" rows="10" class="form-control">{$op['value']}</textarea> {/if} {if $op['type'] == 'select'}
                                    <select name="option[{$k}][{$op['produk_option_id']}]" id="option{$op['produk_option_id']}" class="selectdetail1">
                                              <option value=""></option>
                                            {if array_key_exists('option_values',$op)}
                                              {foreach from=$op['option_values'] key=ko item=opv}
                                                <option value="{$opv['produk_option_value_id']}">{$opv['nama']} (Harga {$opv['price_prefix']} {$opv['price']})</option>
                                              {/foreach}
                                            {/if}
                                          </select> {/if} {if $op['type'] == 'radio'}
                                    <span>
                                              <input type="radio" name="option[{$k}][{$op['produk_option_id']}]" value="{$opv['produk_option_value_id']}" /> {$opv['nama']} (Harga {$opv['price_prefix']} {$opv['price']})
                                            </span> {/if} {if $op['type'] == 'radio'}
                                    <span>
                                              <input type="checkbox" name="option[{$k}][{$op['produk_option_id']}][]" value="{$opv['produk_option_value_id']}" /> {$opv['nama']} (Harga {$opv['price_prefix']} {$opv['price']})
                                            </span> {/if}
                                </div>
                                {/foreach} {/if}
                                <div class="product-box-bottom">
                                    <div class="quanlity-product">
                                        <span>Qty:</span>
                                        <div class="quantity buttons_added">
                                            <a class="sign minus" href="#"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                            <input type="text" size="3" class="input-text qty text" name='qty' title="Qty" value="{$detail['minimum']}">
                                            <a class="sign plus" href="#"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <div class="button-detail">
                                        <a href="#" class="refresh-product" onclick="komentars({$detail['produk_id']})" title="Beri Komentar" alt="Beri Komentar"><i class="fa fa-pencil"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end div 7 -->
                        {if array_key_exists('tombol',$detail)} {form_close()} {/if}
                    </div>
                </div>
            </div>
        </div>
        <div class="detail-products product-tabs" id="tabs-content">
            <ul  class="nav nav-pills">
                {if $total_review > 0}
                  <li class="active">
                      <a href="#3a" data-toggle="tab">Reviews ({$total_review})</a>
                  </li>
                  <li>
                      <a  href="#1a" data-toggle="tab">Description</a>
                  </li>
                  <li>
                      <a href="#2a" data-toggle="tab">Spesifikasi</a>
                  </li>
                {/if}
                {if $total_review == 0}
                  <li class="active">
                      <a  href="#1a" data-toggle="tab">Description</a>
                  </li>
                  <li>
                      <a href="#2a" data-toggle="tab">Spesifikasi</a>
                  </li>
                  <li>
                      <a href="#3a" data-toggle="tab">Reviews ({$total_review})</a>
                  </li>
                {/if}

            </ul>
            <div class="product-tabs-content tab-content clearfix">
              {assign var=aktif_0 value=''}
              {assign var=aktif_1 value=''}
              {if $total_review > 0}
                {$aktif_1='active'}
              {/if}
              {if $total_review == 0}
                {$aktif_0='active'}
              {/if}
                <div class="tab-pane {$aktif_0}" id="1a">
                  {$detail['keterangan']}
                </div>
                <div class="tab-pane" id="2a">
                  <table class="table border-0">
                    <tr>
                      <td>SKU</td>
                      <td>{$detail['sku']}</td>
                    </tr>
                    <tr>
                      <td>ISBN</td>
                      <td>{$detail['isbn']}</td>
                    </tr>
                    <tr>
                      <td>Berat</td>
                      <td>{$detail['berat']} Gram</td>
                    </tr>
                    <tr>
                      <td>Dimensi (P/L/T)</td>
                      <td>{$detail['panjang']} Cm / {$detail['lebar']} Cm/ {$detail['tinggi']} Cm</td>
                    </tr>
                    {foreach from=$detail['attributes'] key=k item=attribute}
                      <tr>
                        <td>{$attribute['nama']}</td>
                        <td>{$attribute['nilai']}</td>
                      </tr>
                    {/foreach}
                  </table>
                </div>
                <div class="tab-pane {$aktif_1}" id="3a">
                    <div id="reviews">
                        <div id="comments">

                              {if $list_review !=0}
                                  <h2 class="review-title">{$total_review} review untuk {$detail['nama']}</h2>
                                  <br>
                                  <ol class="commentlist">
                                  {foreach from=$list_review key=k item=review}
                                    <!--<div class="row">
                                      <div class="col-md-12">
                                        <ul>
                                          <li><a href="#"><i class="fa fa-user"></i></a></li>
                                          <li><a href="#"><i class="fa fa-calendar-o"></i></a></li>
                                        </ul>
                                        <p></p><br>
                                        <h4>Skor {$review['star_produk']}</h4>
                                      </div>
                                    </div>-->
                                    <li class="comment">
                                        <div class="comment_container">
                                            <img alt="rev" src="{theme_url('assets/images/p1.jpg')}" width="100" height="100" class="avatar">
                                            <div class="comment-text">
                                                <div class="meta">
                                                    <strong>{$review['customer']}</strong>-
                                                    <time  datetime="2013-06-07T13:01:25+00:00">{$review['date_added']}</time>
                                                    <div class="rating" title="Rated 5 out of 5">
                                                        {for $start=1 to $review['star_produk']}
                                                          <i class="fa fa-star"></i>
                                                        {/for}
                                                    </div>
                                                </div>
                                                <div class="description">
                                                    <p>{$review['review']}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                  {/foreach}
                                  </ol>
                              {/if}
                              {if $list_review == 0}
                                <h2 class="review-title">Belum Ada Review</h2><br><br>
                              {/if}
                        </div>
                        <div id="review_form">
                          {assign var=cfg_review value=['id'=>'form_add_review']}
                          {if $status_review['status_login'] == 1}
                              <div class="comment-respond">
                                  <h3 class="comment-reply-title">Add a review</h3>
                                {form_open(site_url('cart_catalog/produk/simpan_review'),$cfg_review)}
                                  <input type="hidden" name="star_produk" id="rate" value="0">
                                  <input type="hidden" name="produk_id" value="{$detail['produk_id']}">
                                  <p>
                                      <label>Comment</label>
                                      <textarea name="review" rows="3"></textarea>
                                  </p>
                                  <p>
                                      <a class="button orange" href="#" onclick="add_review()">Submit</a>
                                  </p>
                                  <div id="rateYo"></div>
                                {form_close()}
                              </div>
                          {/if}
                          {if $status_review['status_login'] == 2}
                            {if $this->session->has_userdata(member_id)}
                              {if $status_review['status_beli'] == 1}
                                <div class="comment-respond">
                                    <h3 class="comment-reply-title">Add a review</h3>
                                  {form_open(site_url('cart_catalog/produk/simpan_review'),$cfg_review)}
                                    <input type="hidden" name="star_produk" id="rate" value="0">
                                    <input type="hidden" name="produk_id" value="{$detail['produk_id']}">


                                    <p>
                                        <label>Comment</label>
                                        <textarea name="review" rows="3"></textarea>
                                    </p>
                                    <p>
                                        <a class="button orange" href="#" onclick="add_review()">Submit</a>
                                    </p>
                                    <div id="rateYo"></div>
                                  {form_close()}
                                </div>
                              {/if}
                              {if $status_review['status_beli'] == 2}
                                {if $user_review == 1}
                                  <div class="comment-respond">
                                      <h3 class="comment-reply-title">Add a review</h3>
                                    {form_open(site_url('cart_catalog/produk/simpan_review'),$cfg_review)}
                                      <input type="hidden" name="star_produk" id="rate" value="0">
                                      <input type="hidden" name="produk_id" value="{$detail['produk_id']}">

                                      <p>
                                          <label>Comment</label>
                                          <textarea name="review" rows="3"></textarea>
                                      </p>
                                      <p>
                                          <a class="button orange" href="#" onclick="add_review()">Submit</a>
                                      </p>
                                      <div id="rateYo"></div>
                                    {form_close()}
                                  </div>
                                {/if}
                              {/if}
                            {/if}
                          {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-comentar" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="z-index:100000;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Review Produk</h4>
            </div>
            <div class="modal-body" id="body-comentar">
                <div class="form-group">
                    <label for="">Status QC</label>
                    <select name="status_qc_detail" id="status_qc" class="form-control">
            <option value="0">Perbaiki</option>
            <option value="2">Ok</option>

          </select>
                    <input type="hidden" name="id_komentar" id="ids_komentar">
                    <input type="hidden" name="id_produk" id="ids_produk">
                </div>
                <div class="form-group">
                    <label for="">Komentar QC</label>
                    <textarea name="" id="komentar" cols="30" rows="10" class="form-control" style="display:none;"></textarea>
                    <textarea name="" id="komentar_detail" cols="30" rows="10" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="b_simpan" onclick="">Simpan</button>
            </div>
        </div>
    </div>
</div>
{/block} {block name=script_js}
<script>
    var komentars = function(id_produk) {
        $('#modal-comentar').modal('show');
        $('#ids_produk').val(id_produk);

        var url = "{site_url('customer/qc/get_comment')}/" + id_produk;
        $.getJSON(url, {}, function(json, textStatus) {
            /*optional stuff to do after success */
            if (json == 0) {
                $('#b_simpan').attr('onclick', 'simpan()');
            }
            if (json != 0) {
                $('#ids_komentar').val(json.id);
                $('#komentar').val(json.keterangan);
                $('#komentar_detail').val(json.keterangan_detail);
                $('#b_simpan').attr('onclick', 'update()');

            }
        });

    }

    var simpan = function() {
        var url = "{site_url('customer/qc/save_comentar_detail')}";
        var produk_id = $('#ids_produk').val();
        var komentar = $('#komentar').val();
        var komentar_detail = $('#komentar_detail').val();
        var status_qc = $('#status_qc').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            processData: true,
            data: {
                produk_id: produk_id,
                keterangan: komentar,
                keterangan_detail: komentar_detail, status_qc: status_qc
            },
            success: function(data) {
                $('#komentar').val('');
                $('#komentar_detail').val('');

                $('#modal-comentar').modal('hide');
                alert(data.pesan);
                location.reload();

            }
        });
    }

    var update = function() {
            var url = "{site_url('customer/qc/update_comentar_detail')}";
            var id_komentar = $('#ids_komentar').val();
            var produk_id = $('#ids_produk').val();
            var komentar = $('#komentar').val();
            var komentar_detail = $('#komentar_detail').val();
            var status_qc = $('#status_qc').val();
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                processData: true,
                data: {
                    produk_id: produk_id,
                    keterangan: komentar,
                    keterangan_detail: komentar_detail,
                    status_qc: status_qc,
                    id_commentar: id_komentar
                },
                success: function(data) {
                    $('#komentar').val('');
                    $('#komentar_detail').val('');
                    $('#modal-comentar').modal('hide');
                    alert(data.pesan);
                    location.reload();

                }
            });
        }
        /*$(function() {

            $("#rateYo").rateYo({
                rating: 0,
                precision: 0,
                spacing: "5px",
                maxValue: 5,
                multiColor: {
                    "startColor": "#FF0000", //RED
                    "endColor": "#FF8401" //GREEN
                },
                onSet: function(rating, rateYoInstance) {
                    $('#rate').val(rating);
                    // alert("Rating is set to: " + rating);
                }
            });
        });*/
    var ganti = function(url) {
        $('#main_image').attr('src', url);
    }
    var add_cart = function() {
        $('#form_add_cart')[0].submit(function(event) {
            /* Act on the event */
        });
    }
    var add_review = function() {
        $('#form_add_review')[0].submit(function(event) {
            /* Act on the event */
        });
    }

</script>
{/block}
