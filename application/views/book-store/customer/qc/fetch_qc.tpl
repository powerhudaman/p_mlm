{extends file=$themes}
{block name="tag_body"}
  <body>
{/block}
{block name="main-content"}
  <br>
  <div class="main-content categories categories-grid1" style="background-color: #F5F5F5" data-background-color="#F5F5F5">
      <div class="container">
        <div class="categories-page" style="margin-left:10px; margin-right:10px;">
            <div class="row">
                <div class="content col-md-9 has-sidebar-left">
                  <!-- pos4 begin -->
                  {block name="pos-4"}
                    {$this->pos_4}
                  {/block}
                  <!-- pos5 end -->
                    <div class="categories-list">
                        {assign var=cfg_form value=['method'=>'post','id'=>'frm_checkbox']}
                        {form_open(site_url('customer/qc/simpan_produk_oke'),$cfg_form)}
                        <div class="panel-categories">
                            <div class="row">
                              <div class="col-md-12" id="pesan_notif">

                              </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="view-categories">
                                        <!--<div class="click-grid color-active">
                                            <i class="flaticon-four-grid-layout-design-interface-symbol"></i>
                                        </div>-->
                                        <div class="click-list">
                                            <i class="flaticon-squares-gallery-grid-layout-interface-symbol"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="select-categories">

                                        <div class="select-categories-content">
                                            <span>Total Produk Yang Ditemukan {$total_produk} </span>
                                            <br>
                                            <!-- <input type="submit" value="Simpan Produk Oke" class="btn btn-primary"> -->
                                            <input type="checkbox" name="" id="pilih_semua"> Pilih Semua Produk <br>
                                            <a href="#" class="btn btn-primary" onclick="update_banyak()">Simpan</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    {$halaman}
                                </div>
                            </div>
                        </div>
                        <div class="products-categories categories-list">
                          {if !array_key_exists('pesan',$list_produk)}
                            {foreach from=$list_produk key=k item=produk}
                                <div class="product-box" id="produk_{$produk['produk_id']}">
                                    <div class="product-box-content">
                                        <figure class="img-product">
                                            <img src="{base_url()}admin718/assets/img/com_cart/produk/{$produk['gambar_thumb']}" alt="product" width="175" height="207">
                                            {if $setting_seo == 1}
                                              {if !empty($produk['url_title'])}
                                                <a href="{site_url('customer/qc/detail')}/{$produk['url_title']}" class="flaticon-search"></a>
                                              {/if}
                                              {if empty($produk['url_title'])}
                                                <a href="{site_url('customer/qc/detail')}/{$produk['produk_id']}" class="flaticon-search"></a>
                                              {/if}
                                            {/if}
                                            {if $setting_seo == 0}
                                              <a href="{site_url('customer/qc/detail')}/{$produk['produk_id']}" class="flaticon-search"></a>
                                            {/if}
                                        </figure>
                                        <div class="product-box-text" >
                                            {if $setting_seo == 1}
                                              {if !empty($produk['url_title'])}
                                                {if $produk['judul_produk'] !=""}
                                                  <a href="{site_url('customer/qc/detail')}/{$produk['url_title']}" class="product-name">{$produk['judul_produk']}</a>
                                                {/if}
                                                {if $produk['judul_produk'] ==""}
                                                  <a href="{site_url('customer/qc/detail')}/{$produk['url_title']}" class="product-name">{$produk['nama']}</a>
                                                {/if}

                                              {/if}
                                              {if empty($produk['url_title'])}
                                                {if $produk['judul_produk'] !=""}
                                                  <a href="{site_url('customer/qc/detail')}/{$produk['produk_id']}" class="product-name">{$produk['judul_produk']}</a>
                                                {/if}
                                                {if $produk['judul_produk'] ==""}
                                                  <a href="{site_url('customer/qc/detail')}/{$produk['produk_id']}" class="product-name">{$produk['nama']}</a>
                                                {/if}
                                              {/if}
                                            {/if}
                                            {if $setting_seo == 0}
                                              {if $produk['judul_produk'] !=""}
                                                <a href="{site_url('customer/qc/detail')}/{$produk['produk_id']}" class="product-name">{$produk['judul_produk']}</a>
                                              {/if}
                                              {if $produk['judul_produk'] ==""}
                                                <a href="{site_url('customer/qc/detail')}/{$produk['produk_id']}" class="product-name">{$produk['nama']}</a>
                                              {/if}
                                            {/if}
                                            <span style="margin-left:10px;">
                                              <input type="checkbox" name="list[{$produk['produk_id']}]" id="list{$produk['produk_id']}" value="{$produk['produk_id']}">
                                            </span>
                                              <input type="hidden" name="" id="sq{$produk['produk_id']}" value="{$produk['status_qc']}"><br>
                                            <div class="product-box-bottom">
                                                <a href="#produk_{$produk['produk_id']}" class="add-to-cart" onclick="comentar({$produk['produk_id']})"><i class="fa fa-pencil"></i></a>
                                                {if $produk['qc_review']!=''}
                                                  <a href="#" class="wishlist warning"><i class="fa fa-check-square-o"></i></a>
                                                {/if}
                                                {if $produk['status_qc']=='0'}
                                                  <a href="#" class="wishlist warning"><i class="fa fa-history"></i></a>
                                                {/if}
                                                {if $produk['status_qc'] =='3'}
                                                  <a href="#" class="wishlist warning" title="Produk OK"><i class="fa fa-check"></i></a>
                                                {/if}
                                            </div>
                                            <a href="#" class="write">Write Your Review</a>
                                            {if $produk['harga_lama'] !='' && $produk['harga_lama'] > 0}
                                              <p class="product-cost"> <span class="product-cost-discount">Rp. {number_format($produk['harga_lama'],0,",",".")}</span> <span style="margin-left:0.4em;">Rp. {number_format($produk['harga'],0,",",".")}</span></p>

                                            {/if}
                                            {if $produk['harga_lama'] ==''}
                                              <p class="product-cost">{number_format($produk['harga'],0,",",".")}</p>
                                            {/if}
                                            <p class="desc-product">
                                              {assign var=keterangan value=strip_tags($produk['keterangan'],'<table><tr><tr><p><br><ul><li><a><img>')}
                                              
                                              {$keterangan}<br>
                                              <ul>
                                                <li>Berat = {$produk['berat']}</li>
                                                <li>SKU = {$produk['sku']}</li>
                                                <li>ISBN = {$produk['isbn']}</li>
                                                <li>Dimensi P/L/T = {$produk['panjang']} Cm/{$produk['lebar']} Cm/{$produk['tinggi']} Cm </li>
                                              </ul>
                                            </p>

                                        </div>

                                    </div>
                                </div>
                            {/foreach}
                          {/if}

                          {if array_key_exists('pesan',$list_produk)}
                              <h4>Tidak Ada Produk Dalam Kategori Ini</h4>
                          {/if}
                        </div>
                        <div class="panel-categories">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="view-categories">
                                        <!--<div class="click-grid color-active">
                                            <i class="flaticon-four-grid-layout-design-interface-symbol"></i>
                                        </div>-->
                                        <div class="click-list">
                                            <i class="flaticon-squares-gallery-grid-layout-interface-symbol"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="select-categories">

                                        <div class="select-categories-content" id="lokasi_hiden">
                                          <!--
                                            ids_kategori=549&range_a=&range_b=&qc=#
                                          -->
                                          <input type="hidden" name="ids_kategori" value="{$this->input->get('ids_kategori')}">
                                          <input type="hidden" name="range_a" value="{$this->input->get('range_a')}">
                                          <input type="hidden" name="range_b" value="{$this->input->get('range_b')}">
                                          <input type="hidden" name="qc" value="{$this->input->get('qc')}">
                                          <input type="hidden" name="halaman" value="{$this->uri->segment(4)}">
                                          <input type="hidden" name="status_qc_banyak" id="status_qc_banyak" value="">
                                          <input type="hidden" name="komentar_qc_banyak" id="komentar_qc_banyak" value="">

                                          <span>Total Produk Yang Ditemukan {$total_produk}</span><br>
                                          <!-- <input type="submit" value="Simpan Produk Oke" class="btn btn-primary"> -->
                                          <input type="checkbox" name="" id="pilih_semua_2"> Pilih Semua Produk <br>
                                          <a href="#" class="btn btn-primary" onclick="update_banyak()">Simpan</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    {$halaman}
                                </div>
                            </div>
                        </div>
                        {form_close()}
                    </div>
                </div>
                <div class="sidebar col-md-3">

                    <div class="sidebar-categories">
                        <div class="shop-sidebar">
                            <div class="sidebar-title">Shop By</div>
                            <div class="shop-sidebar-content">
                                <div class="categories-sidebar">
                                    <!-- pos5 begin -->
                                    {block name="pos-5"}
                                      {$this->pos_5}
                                    {/block}
                                    <!-- pos5 end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-icon-box">
                <div class="row">
                  {block name="pos-11"}
                    {$this->pos_11}
                  {/block}
                </div>
            </div>
        </div>
      </div>

</div>

<div class="modal fade" id="modal-comentar" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="z-index:100000;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Review Produk</h4>
      </div>
      <div class="modal-body" id="body-comentar">
        <div class="form-group">
          <label for="">Status QC</label>
          <select name="status_qc" id="status_qc" class="form-control">
            <option value="1">Perbaiki</option>
            <option value="3" id="opt-2">Ok</option>

          </select>
          <input type="hidden" name="id_komentar" id="ids_komentar">
          <input type="hidden" name="id_produk" id="ids_produk">
        </div>
        <div class="form-group">
          <label for="">Keterangan : </label> <br>
          <p id="komentar"></p>
        </div>
        <div class="form-group">
          <div class="col-md-2">List To Do</div>
          <div class="col-md-8"><input type="text" name="nama_todo" id="nama_todo" class="form-control"></div>
          <div class="col-md-2"><a href="#" class="btn btn-primary" id="bt_todo" data-no="0" onclick="tambah_todo()"><i class="fa fa-plus"></i></a></div>
        </div>
        <div class="row">
          <br><br>
          <div class="col-md-12">
              <div class="col-md-4">
                <ul class="list-group" id="list_todo_data">
                  <!--<li></li>
                  <input type="hidden" name="list_todo[][nama_todo]" id="" value="">
                  <input type="hidden" name="list_todo[][checked]" id="" value="">-->
                </ul>
              </div>
              <div class="col-md-8"></div>
          </div>

        </div>
        <div class="row"></div>
        <div class="row"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" data-dismiss="modal" id="b_simpan" onclick="">Simpan</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-comentar-banyak" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="z-index:100000;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Simpan Komentar Produk Massal</h4>
      </div>
      <div class="modal-body" id="">
        <div class="form-group">
          <label for="">Status QC</label>
          <select name="status_banyak" id="status_banyak" class="form-control">
            <option value=""></option>
            <option value="1">Perbaiki</option>
            <option value="3">Ok</option>
          </select>
        </div>
        <!-- <div class="form-group">
          <label for="">Komentar QC</label>
          <textarea name="" id="komentar_banyak" cols="30" rows="10" class="form-control"></textarea>
        </div> -->
        <div class="form-group">
          <div class="col-md-2">List To Do</div>
          <div class="col-md-8"><input type="text" name="nama_todo" id="nama_todo_banyak" class="form-control"></div>
          <div class="col-md-2"><a href="#" class="btn btn-primary" id="bt_todo_banyak" data-no="0" onclick="tambah_todo_banyak()"><i class="fa fa-plus"></i></a></div>
        </div>
        <div class="row">
          <br><br>
          <div class="col-md-12">
              <div class="col-md-4">
                <ul class="list-group" id="list_todo_data_banyak">
                  <!--<li></li>
                  <input type="hidden" name="list_todo[][nama_todo]" id="" value="">
                  <input type="hidden" name="list_todo[][checked]" id="" value="">-->
                </ul>
              </div>
              <div class="col-md-8"></div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" data-dismiss="modal" id="b_simpan_banyak" onclick="simpan_form()">Simpan</button>
      </div>
    </div>
  </div>
</div>

{/block}
{block name=script_js}
  <script>
    $('#pilih_semua').click(function () {
        $('input:checkbox').prop('checked', this.checked);
    });
    $('#pilih_semua_2').click(function () {
        $('input:checkbox').prop('checked', this.checked);
    });
    var comentar = function(id_produk){
      $('#opt-2').show();
      $('#ids_produk').val('');
      $('#komentar').html('');
      $('#komentar_detail').val('');
      $('#status_qc').val('');
      $('#bt_todo').attr('href','#produk_'+id_produk);
      $('#bt_todo').attr('data-no','0');
      $('#list_todo_data').html('');

      if($('#sq'+id_produk).val() == '3'){
        $('#opt-2').hide();
        $('#b_simpan').attr('onclick','simpan()');
        $('#modal-comentar').modal('show');
        $('#ids_produk').val(id_produk);
      }
      if($('#sq'+id_produk).val() != '3'){
        var url = "{site_url('customer/qc/get_comment')}/"+id_produk;
        $.getJSON(url, { }, function(json, textStatus) {
            /*optional stuff to do after success */
            if(json == 0){
              $('#b_simpan').attr('onclick','simpan()');
            }
            if(json !=0){
              $('#ids_komentar').val(json.id);
              if($('#sq'+id_produk).val() != '3'){
                $('#status_qc').val(json.status_qc);
                if(json.list_todo!=""){
                  var no = parseInt($('#bt_todo').attr('data-no'));
                  var html ='';
                  $.each(json.list_todo,function(index, el) {
                    no = no + 1;

                            html +='<li class="list-group-item" id="todo_item_'+no+'">';

                            html +=' '+no+'. '+el.nama_todo;
                            html +='<input type="hidden" class="ltd" name="list_todo['+no+'][nama_todo]" id="" value="'+el.nama_todo+'">';
                            html +='<input type="hidden" class="ltd" name="list_todo['+no+'][checked]" id="" value="'+el.checked+'">';
                            if(el.checked == '1'){
                              html +='<span class="pull-right"><a href=#produk_"'+id_produk+'" class="btn btn-success"><i class="fa fa-check"></i></a></span> ';
                            }
                            if(el.checked == '0'){
                              html +='<span class="pull-right"><a href=#produk_"'+id_produk+'" onclick="hapus_todo('+no+')" class="btn btn-danger"><i class="fa fa-trash"></i></a></span>';
                            }
                            html +='</li>';

                  });
                  $('#list_todo_data').append(html);
                  $('#bt_todo').attr('data-no',no);
                }

              }
              $('#komentar').html(json.keterangan);
              $('#komentar_detail').val(json.keterangan_detail);
              $('#b_simpan').attr('onclick','update()');

            }
            $('#modal-comentar').modal('show');
            $('#ids_produk').val(id_produk);
        });
      }


    }

    var simpan = function(){
     var url = "{site_url('customer/qc/save_comentar')}";
     var produk_id = $('#ids_produk').val();
     var komentar = $('#komentar').val();
     var komentar_detail = $('#komentar_detail').val();
     var status_qc = $('#status_qc').val();
     var list_todo = $('input[class="ltd"]').serialize();
    //  console.log(list_todo);
     if(status_qc == ""){
       alert('status Qc Wajib DI isi');
     }
     if(status_qc !=''){
       $.ajax({
         url: url,
         type: 'POST',
         dataType: 'json',
         processData: true,
         data: { produk_id:produk_id,keterangan:komentar,keterangan_detail:komentar_detail,status_qc:status_qc,list_todo:list_todo },
         success:function(data){
           if(status_qc != '3'){
             $('#list'+produk_id).attr('checked',false);
             $('#sq'+produk_id).val(status_qc)
           }
           $('#komentar').val('');
           $('#komentar_detail').val('');
           $('#modal-comentar').modal('hide');
           alert(data.pesan);
           {if $this->uri->segment('3') =='review_ulang'}
             location.reload();
           {/if}
         }
       });
     }

   }

   var update = function(){
     var url = "{site_url('customer/qc/update_comentar')}";
     var id_komentar = $('#ids_komentar').val();
     var produk_id = $('#ids_produk').val();
     var komentar = $('#komentar').val();
     var komentar_detail = $('#komentar_detail').val();
     var status_qc = $('#status_qc').val();
     var list_todo = $('input[class="ltd"]').serialize();
     $.ajax({
       url: url,
       type: 'POST',
       dataType: 'json',
       processData: true,
       data: { produk_id:produk_id,keterangan:komentar,keterangan_detail:komentar_detail,status_qc:status_qc,id_commentar:id_komentar,list_todo:list_todo },
       success:function(data){
         if(status_qc != '3'){
           $('#list'+produk_id).attr('checked',false);
         }
         $('#komentar').val('');
         $('#komentar_detail').val('');
         $('#modal-comentar').modal('hide');
         alert(data.pesan);
         {if $this->uri->segment('3') =='review_ulang'}
           location.reload();
         {/if}
       }
     });
   }

   var tambah_todo = function(){
     var no = parseInt($('#bt_todo').attr('data-no'));
     no = no + 1;
     var id_target = $('#bt_todo').attr('href');

     var nama_todo = $('#nama_todo').val();

     var html ='';
             html +='<li class="list-group-item" id="todo_item_'+no+'">'+no+'. '+nama_todo;
             html +='<input type="hidden" class="ltd" name="list_todo['+no+'][nama_todo]" id="" value="'+nama_todo+'">';
             html +='<input type="hidden" class="ltd" name="list_todo['+no+'][checked]" id="" value="0">';
             html +='<span class="pull-right"><a href="'+id_target+'" onclick="hapus_todo('+no+')" class="btn btn-danger"><i class="fa fa-trash"></i></a></span>';
             html +='</li>';
        $('#list_todo_data').append(html);
        $('#bt_todo').attr('data-no',no);
        $('#nama_todo').val('');

   }

   var hapus_todo = function(no){
     $('#todo_item_'+no).remove();
   }

   var tambah_todo_banyak = function(){
     var no = parseInt($('#bt_todo_banyak').attr('data-no'));
     no = no + 1;
     var id_target = $('#bt_todo_banyak').attr('href');

     var nama_todo = $('#nama_todo_banyak').val();
     var html_2 ='';
     var html ='';
             html +='<li class="list-group-item" id="todo_item__banyak'+no+'">'+no+'. '+nama_todo;
             html +='<input type="hidden" class="ltd" name="list_todo['+no+'][nama_todo]" id="" value="'+nama_todo+'">';
             html +='<input type="hidden" class="ltd" name="list_todo['+no+'][checked]" id="" value="0">';
             html +='<span class="pull-right"><a href="'+id_target+'" onclick="hapus_todo_banyak('+no+')" class="btn btn-danger"><i class="fa fa-trash"></i></a></span>';
             html +='</li>';
             html_2 +='<input type="hidden" class="ltd" name="list_todo['+no+'][nama_todo]" id="" value="'+nama_todo+'">';
             html_2 +='<input type="hidden" class="ltd" name="list_todo['+no+'][checked]" id="" value="0">';

        $('#list_todo_data_banyak').append(html);
        $('#lokasi_hiden').append(html_2)
        $('#bt_todo_banyak').attr('data-no',no);
        $('#nama_todo_banyak').val('');

   }

   var hapus_todo_banyak = function(no){
     $('#todo_item__banyak'+no).remove();
   }

   var update_banyak = function(){
     $('#modal-comentar-banyak').modal('show');
   }

   $('#status_banyak').change(function(){
     $('#status_qc_banyak').val($('#status_banyak').val());
   });

   $('#komentar_banyak').change(function(){
     $('#komentar_qc_banyak').val($('#komentar_banyak').val());
   });

   var simpan_form = function(){
     $('#frm_checkbox')[0].submit(function(event) {
       /* Act on the event */
     });
   }

  </script>
{/block}
