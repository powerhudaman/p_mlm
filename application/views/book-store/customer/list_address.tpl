{extends file=$themes}
{block name="main-content"}<!-- begin resgitrasi login -->	<div class="container">		<div class="row">			<div class="col-md-12">				<br>				<br>				{if $this->session->flashdata('pesan') !=""}					{$this->session->flashdata('pesan')}				{/if}				<br>			</div>		</div>		<div class="row">			<div class="col-md-3">				<!-- pos5 begin -->					{block name="pos-5"}						{$this->pos_5}					{/block}				<!-- pos5 end -->			</div>			<div class="col-md-9">				<table class="table">					<tr>						<td>No</td>						<td>Nama Lengkap</td>						<td>Alamat Lengkap</td>						<td>Aksi</td>					</tr>					{assign var='no' value='0'}					{if $alamats != 0}						{foreach from=$alamats key=k item=alamat}							{$no = $no + 1}							<tr>								<td>{$no}</td>								<td>{$alamat['nama_depan']} {$alamat['nama_belakang']}</td>								<td>{$alamat['alamat_1']} <br> {$alamat['alamat_2']}</td>								<td>									<a href="{site_url('customer/edit_adress')}/{$alamat['id']}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>|{if $alamat['default'] =='0'}<a href="#" onclick="delete_id({$alamat['id']})" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>{/if}								</td>							</tr>						{/foreach}					{/if}					{if $alamats == 0}						<tr>							<td colspan="4">Data Tidak Ditemukan</td>						</tr>					{/if}				</table>				<br>				<br>				<br>			</div>		</div>	</div><!-- end resgitrasi login -->{/block}

{block name=script_js}	<script>
	function delete_id(no){
		if(confirm("Apakah Anda Yakin Akan Menghapus Alamat ini ?.")){
			window.location.href="{site_url('customer/delete_adress')}/"+no;
		}
	}
	</script>{/block}