{extends file=$themes}
{block name="tag_body"}
  <body>
{/block}
{block name="main-content"}
  <br><br>
  <div class="container">

      <div class="row">
          <div class="col-md-3">

                            <!-- pos5 begin -->
                            {block name="pos-5"}
                              {$this->pos_5}
                            {/block}
                            <!-- pos5 end -->
          </div>
          <div class="col-md-9">
              <h4 class="title-left">Data Transaksi</h4>

              <div class="contact-form shop-sidebar-content">
                <div class="table-responsive">
                  <table class="table">
                    {if $orders!=0}
                    <thead>
                      <td>No</td>
                      <td>Inv No</td>
                      <td>Tagihan</td>
                      <td>Tanggal Order</td>
                      <td>Status</td>
                      <td></td>
                    </thead>
                    <tbody>
                      {assign var=no value=0}
                      {foreach from=$orders key=k item=order}
                      {$no = $no+1}
                      <tr class="cart_item" id="dewa_{$order['order_id']}">
                        <td>{$no}</td>
                        <td>{$order['invoice_no']}</td>
                        <td class="text-right">{number_format($order['tagihan'],0,",",".")}</td>
                        <td>{$order['tgl']}</td>
                        <td>{$order['status']}</td>
                        <td>
                          <a href="#dewa_{$order['order_id']}" class="btn btn-sm btn-warning" title="Detail Invoice" alt="Detail Invoice" onclick="detail_invoice({$order['order_id']})"><i class="fa fa-eye"></i></a>
                          <a href="#dewa_{$order['order_id']}" class="btn btn-sm btn-primary" title="Tujuan Pengiriman" alt="Detail Invoice" onclick="tujuan_pengiriman({$order['order_id']})"><i class="fa fa-truck"></i></a>
                          <a href="#dewa_{$order['order_id']}" class="btn btn-sm btn-success" title="Log Aktivitas Invoice" alt="Log Aktivitas Invoice" onclick="history_invoice({$order['order_id']})"><i class="fa fa-book"></i></a>
                          <a href="#dewa_{$order['order_id']}" class="btn btn-sm btn-primary" title="Log Pembayaran Invoice" alt="Log Pembayaran Invoice" onclick="history_pembayaran({$order['order_id']})"><i class="fa fa-money"></i></a>
                        </td>
                      </tr>
                      {/foreach}
                    </tbody>
                    {/if}
                    {if $orders == 0}
                      <tr>
                        <td>
                            <h4>Anda Belum Pernah Melakukan Order</h4>
                        </td>
                      </tr>
                    {/if}
                  </table>
                </div>
              </div>
          </div>
      </div>

  </div>

  <div class="modal fade" id="log-history" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="z-index:100000;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Histori Status Invoice</h4>
        </div>
        <div class="modal-body" id="body-history">
          <!--<table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No Invoice </th>
                        <th>Deskripsi</th>
                        <th>Status Order</th>
                        <th>Tanggal</th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="data_history in data_historys" class="ng-scope">
                      <td class="ng-binding">inv-985/2017-02/9163787328282</td>
                      <td class="ng-binding">Nurhuda Melakukan Konfirmasi Pembayaran untuk Invoice inv-985/2017-02/9163787328282</td>
                      <td class="ng-binding">Konfirmasi</td>
                      <td class="ng-binding">2017-02-13 07:49:54</td>
                    </tr>
                  </tbody>
          </table>-->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="tujuan_pengiriman" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="z-index:100000;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Data Tujuan Pengiriman</h4>
        </div>
        <div class="modal-body" id="body-pengiriman">
          <!--<table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No Invoice </th>
                        <th>Deskripsi</th>
                        <th>Status Order</th>
                        <th>Tanggal</th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="data_history in data_historys" class="ng-scope">
                      <td class="ng-binding">inv-985/2017-02/9163787328282</td>
                      <td class="ng-binding">Nurhuda Melakukan Konfirmasi Pembayaran untuk Invoice inv-985/2017-02/9163787328282</td>
                      <td class="ng-binding">Konfirmasi</td>
                      <td class="ng-binding">2017-02-13 07:49:54</td>
                    </tr>
                  </tbody>
          </table>-->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="log-pembayaran" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Histori Pembayaran Invoice</h4>
        </div>
        <div class="modal-body" id="body-pembayaran">
          <table class="table table-bordered table-striped">
                    <thead>
                      <tr>

                      </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="data_pembayaran in data_pembayarans" class="ng-scope">
                        <td class="ng-binding">inv-985/2017-02/9163787328282</td>
                        <td class="ng-binding">2017-02-13</td>
                        <td class="ng-binding">BCA</td>
                        <td class="ng-binding text-right">530.000</td>
                        <td class="ng-binding text-right">530.000</td>
                        <td></td>
                    </tr>
                  </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="detail-order" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Detail Order</h4>
        </div>
        <div class="modal-body" id="body-detail">
          <table class="table table-bordered table-striped">
                    <tbody><tr>
                      <th>Id Produk</th>
                      <th>Bara</th>
                      <th>Nama Produk</th>
                      <th>Harga</th>
                      <th>Quantity</th>
                      <th>Subtotal</th>
                    </tr>
                    <!-- ngRepeat: data_produk in detail_produks --><tr ng-repeat="data_produk in detail_produks" class="ng-scope">
                      <td class="ng-binding">3</td>
                      <td class="ng-binding">4437</td>
                      <td class="ng-binding">CAL CITIZEN CT-500 cba</td>
                      <td class="ng-binding text-right">39.000</td>
                      <td class="ng-binding text-right">10</td>
                      <td class="ng-binding text-right">39.0000</td>
                    </tr><!-- end ngRepeat: data_produk in detail_produks --><tr ng-repeat="data_produk in detail_produks" class="ng-scope">
                      <td class="ng-binding">2</td>
                      <td class="ng-binding">2127</td>
                      <td class="ng-binding">SULING TREND</td>
                      <td class="ng-binding text-right">7.000</td>
                      <td class="ng-binding text-right">15</td>
                      <td class="ng-binding text-right">105.000</td>
                    </tr><!-- end ngRepeat: data_produk in detail_produks -->
                    <tr>
                      <td colspan="6"></td>
                    </tr>
                    <!-- ngRepeat: order_total in order_totals --><tr ng-repeat="order_total in order_totals" class="ng-scope">
                      <td colspan="5"><strong class="ng-binding">Sub Total</strong></td>
                      <td class="ng-binding text-right">495.000</td>
                    </tr><!-- end ngRepeat: order_total in order_totals --><tr ng-repeat="order_total in order_totals" class="ng-scope">
                      <td colspan="5"><strong class="ng-binding">Pengiriman (Jne - CTC 1 Kg)</strong></td>
                      <td class="ng-binding text-right">35.000</td>
                    </tr><!-- end ngRepeat: order_total in order_totals --><tr ng-repeat="order_total in order_totals" class="ng-scope">
                      <td colspan="5"><strong class="ng-binding">Total Tagihan</strong></td>
                      <td class="ng-binding text-right">530.000</td>
                    </tr><!-- end ngRepeat: order_total in order_totals -->


                  </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

{/block}

{block name=script_js}
	<script>
  var history_invoice = function(order_id){
      var html ='';
    $("#log-history").modal('show');
    var url = "{site_url('customer/Dashboard_api/history_log')}/"+order_id;
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      beforeSend:function(){
        html ='<h4>Loading</h4>';
        $('#body-history').html(html);
      },
      success:function(data){
        html = '';

        html +='<table class="table table-bordered table-striped">';
                  html +='<thead>';
                    html +='<tr>';
                      html +='<th>No Invoice </th>';
                      html +='<th>Deskripsi</th>';
                      html +='<th>Status Order</th>';
                      html +='<th>Tanggal</th>';
                    html +='</tr>';
                  html +='</thead>';
                  html +='<tbody>';
                  $.each(data,function(index, el) {
                    html +='<tr>';
                      html +='<td>'+el.invoice_no+'</td>';
                      html +='<td>'+el.comment+'</td>';
                      html +='<td>'+el.status_order+'</td>';
                      html +='<td>'+el.date_added+'</td>';
                    html +='</tr>';
                  });

                html +='</tbody>';
        html +='</table>';
        $('#body-history').html(html);
        // alert(html);

      }

    });

  }
  var history_pembayaran = function(order_id){
      var html ='';
    $("#log-pembayaran").modal('show');
    var url = "{site_url('customer/Dashboard_api/history_pembayaran')}/"+order_id;
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      beforeSend:function(){
        html ='<h4>Loading</h4>';
        $('#body-pembayaran').html(html);
      },
      success:function(data){
        html = '';
        if(data !=0){
          html +='<table class="table table-bordered table-striped">';
                    html +='<thead>';
                      html +='<tr>';
                        html +='<th>No Invoice </th>';
                        html +='<th>Tanggal Bayar</th>';
                        html +='<th>Bayar Dari</th>';
                        html +='<th>Tagihan</th>';
                        html +='<th>Nilai Transfer</th>';
                        html +='<th>Bukti Pembayaran</th>';
                      html +='</tr>';
                    html +='</thead>';
                    html +='<tbody>';
                    $.each(data,function(index, el) {
                      html +='<tr>';
                        html +='<td>'+el.invoice_no+'</td>';
                        html +='<td>'+el.tgl_bayar+'</td>';
                        html +='<td>'+el.pay_from+'</td>';
                        html +='<td>'+el.tagihan+'</td>';
                        html +='<td>'+el.nominal+'</td>';
                        html +='<td><img src="{base_url()}/assets/img/com_payment/konfirmasi/'+el.bukti_pembayaran+'" alt="" width="100px" height="100px" class="img"></td>';
                      html +='</tr>';
                    });

                  html +='</tbody>';
          html +='</table>';
        }
        if(data == 0){
          html ='<h4>Belum Ada Data Konfirmasi Pembayaran</h4>';
        }

        $('#body-pembayaran').html(html);
      }

    });

  }

  var tujuan_pengiriman = function(order_id){
      var html ='';
    $("#tujuan_pengiriman").modal('show');
    var url = "{site_url('customer/Dashboard_api/data_penerima')}/"+order_id;
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      beforeSend:function(){
        html ='<h4>Loading</h4>';
        $('#body-pengiriman').html(html);
      },
      success:function(data){
        html = '';
        if(data !=0){


                      $.getJSON("{site_url('customer/Dashboard_api/data_pengiriman')}/"+order_id, { }, function(json, textStatus) {

                        html +='<table class="table table-bordered table-striped">';

                                  html +='<tbody>';
                                    html +='<tr>';
                                      html +='<td colspan="2">Tujuan Pengiriman</td>';
                                    html +='</tr>';

                                    html +='<tr>';
                                      html +='<td>Nama Depan</td>';
                                      html +='<td>'+data.shipping_firstname+'</td>';
                                    html +='</tr>';
                                    html +='<tr>';
                                      html +='<td>Nama Belakang</td>';
                                      html +='<td>'+data.shipping_lastname+'</td>';
                                    html +='</tr>';
                                    html +='<tr>';
                                      html +='<td>Alamat Pengiriman</td>';
                                      html +='<td>'+data.shipping_address_1+' '+data.shipping_city+'</td>';
                                    html +='</tr>';
                                    html +='<tr>';
                                      html +='<td>Telepon</td>';
                                      html +='<td>'+data.shipping_telephone+'</td>';
                                    html +='</tr>';
                                    html +='<tr>';
                                      html +='<td>Kode Pos</td>';
                                      html +='<td>'+data.shipping_postcode+'</td>';
                                    html +='</tr>';

                                    console.log(json);
                                    html +='<tr>';
                                      html +='<td>Kurir Dan Layanan</td>';
                                      html +='<td>'+json.kurir+' '+json.layanan+'</td>';
                                    html +='</tr>';
                                    html +='<tr>';
                                      html +='<td>Biaya</td>';
                                      html +='<td>'+json.nominal+'</td>';
                                    html +='</tr>';

                                    html +='</tbody>';
                            html +='</table>';

                            $('#body-pengiriman').html(html);

                      });



        }
        if(data == 0){
          html ='<h4></h4>';
          $('#body-pengiriman').html(html);
        }

      }

    });

  }

  var detail_invoice = function(order_id){
      var html ='';
      var total_html ='';
    $("#detail-order").modal('show');
    var url = "{site_url('customer/Dashboard_api/data_order_produk')}/"+order_id;
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      beforeSend:function(){
        html ='<h4>Loading</h4>';
        $('#body-detail').html(html);
      },
      success:function(data){
        var url_option = "{site_url('customer/Dashboard_api/data_order_produk_option')}";
        var url_total = "{site_url('customer/Dashboard_api/data_order_total')}/"+order_id;
        var json_html ='';
        var no_item = 0;
        html = '';
        html +='<table class="table table-bordered table-striped">';
                  html +='<thead>';
                    html +='<tr>';
                      html +='<th>Id Produk</th>';
                      // html +='<th>Bara</th>';
                      html +='<th>Nama Produk</th>';
                      html +='<th>Harga</th>';
                      html +='<th>Qty</th>';
                      html +='<th>Subtotal</th>';
                    html +='</tr>';
                  html +='</thead>';
                  html +='<tbody id="item-detail-order">';
                  $.each(data,function(index, el) {

                    html +='<tr>';
                      html +='<td>'+el.product_id+'</td>';
                      // html +='<td>'+el.bara+'</td>';
                      html +='<td id="produk_'+el.product_id+'_'+no_item+'">';
                        html +=el.nama;
                        if(el.option !=0){
                          html +='<br>';
                          html +='<ul>';
                              $.each(el.option,function(k, v) {
                                  html +='<li>'+v.name+' '+v.value+'</li>';
                              });
                          html +='</ul>';
                        }
                      html +='</td>';
                      html +='<td>'+el.price+'</td>';
                      html +='<td>'+el.quantity+'</td>';
                      html +='<td>'+el.total+'</td>';
                    html +='</tr>';

                  });
                  html +='<tr>';
                    html +='<td colspan="5"></td>';
                  html +='</tr>';

                html +='</tbody>';
        html +='</table>';
        $.getJSON(url_total, { }, function(json_total, textStatus) {
          total_html = '';
            $.each(json_total,function(inp,va) {
              total_html +='<tr>';
                total_html +='<td colspan="4"><strong>'+va.title+'</strong></td>';
                total_html +='<td>'+va.value+'</td>';
              total_html +='</tr>';
            });
            $('#item-detail-order').append(total_html);
        });


      }

    }).done(function() {
      $('#body-detail').html(html);

    });

  }

  // var total_order = function()


	</script>
{/block}
