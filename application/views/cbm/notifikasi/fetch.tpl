{extends file=$themes} {block name="main-content"}

<div class="panel panel-border panel-custom" style="margin-top:2%;">
  <div class="panel-heading">
  </div>
  <div class="panel-body">
    <div class="col-sm-12">
      <div class="form-group">
        {if $this->session->flashdata('pesan') !=""}
          {$this->session->flashdata('pesan')}
        {/if}
      </div>
      <div class="card-box table-responsive">




        <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
          <div class="row">
            <div class="col-sm-12">
              <div class="card-box">
									<div class="row">
										<div class="col-lg-12">
											<h4 class="m-t-0 header-title"><b>Semua Notifikasi</b></h4>
											<div class="p-20">
												<table class="table table table-hover m-0">
													<thead>
														<tr>
															<th>#</th>
															<th>Judul Notifikasi</th>
															<th>Pesan Notifikasi</th>
                              <th>Link Notifikasi</th>
                              <th>Tgl Notifikasi</th>
															<th>Status Notifikasi</th>
														</tr>
													</thead>
													<tbody>
                            {assign var='angka' value='0'}
                            {foreach from=$this->notif_member key=k item=i}
                              {$angka = $angka + 1}
                              {if $angka < 51}
                                <tr>
                                  <th scope="row">{$angka}</th>
                                  <td>{$type_notif[$i['type_notif']]}</td>
                                  <td>{$i['pesan_notif']}</td>
                                  <td><a href="{site_url('cbm/notifikasi/link')}/{$i['id']}" class="btn btn-primary">Lihat</a></td>
                                  <td>{$i['created_date']}</td>
                                  <td>{if $i['status_notif'] == '1'}Sudah Dilihat{/if}{if $i['status_notif'] == '0'}Belum Dilihat{/if}</td>
                                </tr>
                              {/if}
                            {/foreach}

													</tbody>
												</table>
											</div>

										</div>
									</div>
								</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



{/block}
{block name=script_js}

  <script>
  </script>

{/block}
