{extends file=$themes} {block name="main-content"}

<div class="panel panel-border panel-custom" style="margin-top:2%;">
  <div class="panel-heading">

  </div>
  <div class="panel-body">
    <div class="col-sm-12">
      <div class="form-group">
        {if $this->session->flashdata('pesan') !=""}
          {$this->session->flashdata('pesan')}
        {/if}
      </div>
      <div class="card-box table-responsive">
        <table id="mygrid"class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
        width="100%">
                    <thead>
                      <tr>
                        <th>Id Member</th>
                        <th>Nama</th>
                        <th>Sponsor</th>
                        <th>Upline</th>
                        <th>Kiri</th>
                        <th>Kanan</th>
                        <th>Telp</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Id Member</td>
                        <td>Nama</td>
                        <td>Sponsor</td>
                        <td>Upline</td>
                        <td>Kiri</td>
                        <td>Kanan</td>
                        <td>Telp</td>
                      </tr>
                    </tbody>
                  </table>
      </div>
    </div>
  </div>
</div>
{/block}
{block name=script_js}
{assign var='url' value="cbm/member_master/fetch_data"}
  <script>
    $(function () {
       var table = $("#mygrid").DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{site_url($url)}",
                "type": "POST"
            },
            columns: [
                      { data:'id_member' },
                      { data:'nama_lengkap' },
                      { data:'id_sponsor' },
                      { data:'id_upline' },
                      { data:'downline_kiri' },
                      { data:'downline_kanan' },
                      { data:'no_hp' }
              ],
            "columnDefs":[
                            {
                               "sTitle":"Id Member",
                               "aTargets": [ "id_member" ]
                            }
            ]
      });
    });
  </script>
{/block}
