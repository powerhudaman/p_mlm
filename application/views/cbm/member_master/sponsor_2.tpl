{extends file=$themes} {block name="main-content"}



<div class="panel panel-border panel-custom" style="margin-top:2%;">

  <div class="panel-body">

    <div class="col-sm-12">

      <div class="form-group">

        {if $this->session->flashdata('pesan') !=""}

          {$this->session->flashdata('pesan')}

        {/if}

      </div>

      <div class="card-box table-responsive">

        

    

          

        

        <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

          <div class="row">

            <div class="col-sm-12">

              {if !array_key_exists('pesan',$sponsoring)}	

              	<table id="data_member_belum_aktif" class="display table table-bordered table-striped" width="100%"></table>

              {/if}

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>







{/block}

{block name=script_js}



  <script>

  	{if !array_key_exists('pesan',$sponsoring)}	

    $(document).ready(function() {

      var data_set = {json_encode($sponsoring)}



      // dataTables_paginate paging_simple_numbers pull-right

      var extensions = {

         "sFilter": "dataTables_filter pull-right",

         "sPaging":"dataTables_paginate paging_simple_numbers pull-right "

     }

     // Used when bJQueryUI is false

     $.extend($.fn.dataTableExt.oStdClasses, extensions);

     // Used when bJQueryUI is true

     $.extend($.fn.dataTableExt.oJUIClasses, extensions);



        $('#data_member_belum_aktif').DataTable( {

            data: data_set,

            columns: [

                { title: "ID Member" },

                { title: "Nama Lengkap" },

                { title: "Kota" },

                { title: "No Hp" },

                { title: "Tanggal Gabung" },

                { title: "Lihat Jaringan"}

            ]

        } );

    } );

    {/if}

  </script>



{/block}

