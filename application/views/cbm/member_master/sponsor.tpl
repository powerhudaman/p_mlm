{extends file=$themes} {block name="main-content"}

<div class="panel panel-border panel-custom" style="margin-top:2%;">
  <div class="panel-heading">
    <h4 class="m-t-0 header-title"></h4>
    
  </div>
  <div class="panel-body">
    <div class="col-sm-12">
      <div class="form-group">
        {if $this->session->flashdata('pesan') !=""}
          {$this->session->flashdata('pesan')}
        {/if}
      </div>
      <div class="card-box table-responsive">
        
    
          
        
        <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
          <div class="row">
            <div class="col-sm-12">
              <table id="mygrid" class="display table table-bordered table-striped" width="100%">
			  <thead>
                      <tr>
                       
                        <th>Id Member</th>
                        <th>Nama</th>
                        <th>Kota</th>
                        <th>HP</th>
                        <th>Tgl Gabung</th>
                        <th>Lihat Geneologi</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    </tbody>
                  </table>
			  
			  </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



{/block}
{block name=script_js}
{assign var='url' value="cbm/member_master/get_sponsoring"}
  <script>
  
    $(document).ready(function() {
       var table = $("#mygrid").DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{site_url($url)}",
                "type": "POST"
            },
            columns: [
                      { data:'id_member' },
                      { data:'nama_lengkap' },
                      { data:'id_kota' },
                      { data:'no_hp' },
                      { data:'date_created' },
                      { data:'aksi'}
              ],
            "columnDefs":[
                            {
                               "sTitle":"Id Member",
                               "aTargets": [ "id_member" ]
                            }
            ]
      });
    });
  </script>

{/block}
