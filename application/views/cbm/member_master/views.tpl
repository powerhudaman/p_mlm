{extends file=$themes} {block name="main-content"}

<div class="panel panel-border panel-custom" style="margin-top:2%;">
<div class="panel-heading">

</div>
<div class="panel-body">
  <div class="col-sm-12">
    <div class="form-group">
      {if $this->session->flashdata('pesan') !=""}
        {$this->session->flashdata('pesan')}
      {/if}
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="card-box">
          <div class="row">
            <div class="col-md-6">
              <table class="table table-bordered table-striped">
                <tr>
                  <td>Nama</td>
                  <td>{$get['nama_lengkap']}</td>
                </tr>
                <tr>
                  <td>Tgl Pendaftaran</td> 
                  <td>{$get['created_date']}</td>
                </tr>
                <tr>
                  <td>Sponsor</td>
                  <td>{$sponsor['nama_lengkap']} - {$get['id_sponsor']} </td>
                </tr>
                <tr>
                  <td>Upline</td>
                  <td>{$upline['nama_lengkap']} - {$get['id_upline']} </td>
                </tr>
              </table>
            </div>

            <div class="col-md-6">
              <table class="table table-bordered table-striped">
                <tr>
                  <td>Jumlah Jaringan</td>
                  <td>{count($member_kiri)} Kiri / {count($member_kanan)} Kanan</td>
                </tr>
                <tr>
                  <td>Level Kedalaman</td>
                  <td>
                    <span id="level"></span> / <span id="level_kanan"></span>
                  </td>
                </tr>
                <tr>
                  <td>Sponsoring</td>
                  <td>{$sponsor['nama_lengkap']} - {$get['id_upline']} </td>
                </tr>
                <tr>
                  <td>Total Bonus Level</td>
                  <td>{number_format($total_bonus,0,",",".")}</td>
                </tr>
              </table>
            </div>

            <div class="col-md-6">
              <div class="panel panel-border panel-custom">
                <div class="panel-heading">
                  <h3 class="panel-title">Member Kiri</h3>
                </div>
                <div class="panel-body">
                  <div class="card-box table-responsive">

                    <table id="mygrid"
                    class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                    width="100%">
                      <thead>
                        <tr>
                         <td width="15%">No</td>
                          <td width="15%">Level</td>
                          <td width="70%">Nama</td>
                        </tr>
                      </thead>
                      <tbody>
                        {if count($member_kiri) > 0}
                          {assign var='no_kiri' value='0'}
                          {assign var='level' value='0'}
                          {foreach from=$member_kiri key=k item=kiri}
                            {$no_kiri = $no_kiri + 1}
                            {if $kiri['level'] > $level}
                              {$level = $kiri['level']}
                            {/if}

                            <tr>
                              <td width="15%">{$no_kiri}</td>
                              <td width="15%">{$kiri['level']}</td>
                              <td width="70%">
                                <p>
                                  <strong>{$kiri['nama_lengkap']}({$kiri['id_member']})</strong>Tgl.{$kiri['tgl']}  <br>
                                  Sponsor.{$kiri['id_sponsor']} Upline.{$kiri['id_upline']}
                                </p>
                              </td>
                            </tr>
                          {/foreach}
                        {/if}
                      </tbody>

                    </table>

                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="panel panel-border panel-custom">
                <div class="panel-heading">
                  <h3 class="panel-title">Member Kanan</h3>
                </div>
                <div class="panel-body">

                  <div class="card-box table-responsive">


                    <table id="mygrid"
                    class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                    width="100%">

                      <thead>
                        <tr>
                         <td width="15%">No</td>
                          <td width="15%">Level</td>
                          <td width="70%">Nama</td>
                        </tr>
                      </thead>
                      <tbody>
                       {if count($member_kanan) > 0}
                          {assign var='no_kanan' value='0'}
                          {assign var='level_kanan' value='0'}
                          {foreach from=$member_kanan key=k item=kanan}
                            {$no_kanan = $no_kanan + 1}
                            {if $kanan['level'] > $level}
                              {$level_kanan = $kanan['level']}
                            {/if}
                            <tr>
                              <td width="15%">{$no_kanan}</td>
                              <td width="15%">{$kanan['level']}</td>
                              <td width="70%">
                                <p>
                                  <strong>{$kanan['nama_lengkap']}({$kanan['id_member']})</strong>Tgl.{$kanan['tgl']}  <br>
                                  Sponsor.{$kanan['id_sponsor']} Upline.{$kanan['id_upline']}
                                </p>
                              </td>
                            </tr>
                          {/foreach}
                        {/if}
                      </tbody>

                    </table>

                  </div>


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>










{/block}
{block name=script_js}
  {assign var='url' value="Member_master/get_memberkanan/{$get['id_member']}"}
  {assign var='url2' value="Member_master/get_memberkiri/{$get['id_member']}"}
    <script>
    $('#level').html("{$level} Kiri");
    $('#level_kanan').html("{$level_kanan} kanan");
      /*$(function () {
        $("#mygrid").DataTable({
              "processing": true,
              "serverSide": true,
              "ajax": {
                  "url": "{site_url($url)}",
                  "type": "POST"
              },
              columns: [
                        { data:'id_member' },
                        { data:'nama_lengkap' },
                        { data:'created_date' },
                        { data:'id_sponsor' },
                        { data:'id_upline' }
                ],
              "columnDefs":[
                              {
                                 "sTitle":"Id Member",
                                 "aTargets": [ "id_member" ]
                              },
                              {
                                  "aTargets": [ 9 ],
                                  "bSortable": false
                              }
              ]
        });
         $("#mygrid2").DataTable({
              "processing": true,
              "serverSide": true,
              "ajax": {
                  "url": "{site_url($url2)}",
                  "type": "POST"
              },
              columns: [
                        { data:'id_member' },
                        { data:'nama_lengkap' },
                        { data:'created_date' },
                        { data:'id_sponsor' },
                        { data:'id_upline' }
                ],
              "columnDefs":[
                              {
                                 "sTitle":"Id Member",
                                 "aTargets": [ "id_member" ]
                              },
                              {
                                  "aTargets": [ 9 ],
                                  "bSortable": false
                              }
              ]
        });
      });*/
    </script>

{/block}
