{extends file=$themes} {block name="main-content"}
<div class="panel panel-border panel-custom" style="margin-top:2%;">
    <div class="panel-heading">

    </div>
    <div class="panel-body">
        
        <div class="row">
          <div class="col-md-4"></div>
          <form method="post" action="{site_url('cbm/Member_master/hirarki/')}">
            <div class="input-group col-md-3">
                <input type="text" name="member" class="form-control" placeholder="Member" id="member">
                <span class="input-group-btn">
                <button type="submit" name="cari" class="btn waves-effect waves-light btn-primary">Cari</button>
                </span>
            </div>
          </form>
          <div class="tree-bar-x" style="overflow-x">
            {if $id_upline > 0}
              <a href="{site_url('cbm/Member_master/hirarki/')}/{$id_upline}" class="btn btn-primary" style="margin-left:46%;"><i class="ion-arrow-up-a"></i></a>
            {/if}
          	<ul>
          		<li id="induk">
          			<a href="#" id="pos-0">
                  <span>Induk</span>
                  <br>
                  <span>nama Induk</span>
                  <br>
                  <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                  <div class="blok-total">
                    <span class="span-inline">10</span><span class="span-inline">12</span>
                  </div>
          			</a>
                <!-- child -->
                  <ul>
                    <li>
                      <a href="#" id="pos-1">
                        <br>
                        <br>
                        <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                        <div class="blok-total">
                          <span class="span-inline">0</span><span class="span-inline">0</span>
                        </div>
                      </a>
                        <!-- child -->
                          <ul>
                            <li>
                              <a href="#" id="pos-3">
                                <br>
                                <br>
                                <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                <div class="blok-total">
                                  <span class="span-inline">0</span><span class="span-inline">0</span>
                                </div>
                              </a>
                                <!-- child -->
                                  <ul>
                                    <li >
                                      <a href="#" id="pos-5">
                                        <br>
                                        <br>
                                        <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                        <div class="blok-total">
                                          <span class="span-inline">0</span><span class="span-inline">0</span>
                                        </div>
                                      </a>
                                    </li>
                                    <li>
                                      <a href="#" id="pos-6">
                                        <br>
                                        <br>
                                        <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                        <div class="blok-total">
                                          <span class="span-inline">0</span><span class="span-inline">0</span>
                                        </div>
                                      </a>
                                    </li>
                                  </ul>
                                <!-- child -->
                            </li>
                            <li >
                              <a href="#" id="pos-4">
                                <br>
                                <br>
                                <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                <div class="blok-total">
                                  <span class="span-inline">0</span><span class="span-inline">0</span>
                                </div>
                              </a>
                              <!-- child -->
                                <ul>
                                  <li >
                                    <a href="#" id="pos-7">
                                      <br>
                                      <br>
                                      <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                      <div class="blok-total">
                                        <span class="span-inline">0</span><span class="span-inline">0</span>
                                      </div>
                                    </a>
                                  </li>
                                  <li >
                                    <a href="#" id="pos-8">
                                      <br>
                                      <br>
                                      <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                      <div class="blok-total">
                                        <span class="span-inline">0</span><span class="span-inline">0</span>
                                      </div>
                                    </a>
                                  </li>
                                </ul>
                              <!-- child -->
                            </li>
                          </ul>
                        <!-- child -->
                    </li>
                    <li >
                      <a href="#" id="pos-2">
                        <br>
                        <br>
                        <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                        <div class="blok-total">
                          <span class="span-inline">0</span><span class="span-inline">0</span>
                        </div>
                      </a>
                        <!-- child -->
                          <ul>
                            <li >
                              <a href="#" id="pos-9">
                                <br>
                                <br>
                                <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                <div class="blok-total">
                                  <span class="span-inline">0</span><span class="span-inline">0</span>
                                </div>
                              </a>
                                <!-- child -->
                                  <ul>
                                    <li >
                                      <a href="#" id="pos-11">
                                        <br>
                                        <br>
                                        <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                        <div class="blok-total">
                                          <span class="span-inline">0</span><span class="span-inline">0</span>
                                        </div>
                                      </a>
                                    </li>
                                    <li >
                                      <a href="#" id="pos-12">
                                        <br>
                                        <br>
                                        <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                        <div class="blok-total">
                                          <span class="span-inline">0</span><span class="span-inline">0</span>
                                        </div>
                                      </a>
                                    </li>
                                  </ul>
                                <!-- child -->
                            </li>
                            <li >
                              <a href="#" id="pos-10">
                                <br>
                                <br>
                                <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                <div class="blok-total">
                                  <span class="span-inline">0</span><span class="span-inline">0</span>
                                </div>
                              </a>
                              <!-- child -->
                                <ul>
                                  <li >
                                    <a href="#" id="pos-13">
                                      <br>
                                      <br>
                                      <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                      <div class="blok-total">
                                        <span class="span-inline">0</span><span class="span-inline">0</span>
                                      </div>
                                    </a>
                                  </li>
                                  <li >
                                    <a href="#" id="pos-14">
                                      <br>
                                      <br>
                                      <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="90px" height="90px">
                                      <div class="blok-total">
                                        <span class="span-inline">0</span><span class="span-inline">0</span>
                                      </div>
                                    </a>
                                  </li>
                                </ul>
                              <!-- child -->
                            </li>
                          </ul>
                        <!-- child -->
                    </li>
                  </ul>
                <!-- child -->
          		</li>
          	</ul>
          </div>
        </div>
        <div class="row">
          {*<div class="tree-bar-x">
              <ul>
                <li id="{$get['id_member']}">
                  <a href="#">
                    <span>
                      {$get['id_member']}
                    </span>
                    <br>
                    <span>
                      {$get['nama_lengkap']}
                    </span>
                    <br>
                    {if $get['image'] == ""}
                      <img src="{base_url()}admin718/assets/img/gambar_dummy/orang.jpg" width="100px" height="100px">
                    {/if}
                    {if $get['image'] !=""}
                      <img src="{base_url()}admin718/assets/img/username/{$get['image']}" width="100px" height="100px">
                    {/if}
                  </a>
                  <!--<ul>
                    <li>
                      <a href="#">Child</a>
                      <ul>
                        <li>
                          <a href="#">Grand Child</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="#">Child</a>
                      <ul>
                        <li><a href="#">Grand Child</a></li>
                        <li>
                          <a href="#">Grand Child</a>
                          <ul>
                            <li>
                              <a href="#">Great Grand Child</a>
                            </li>
                            <li>
                              <a href="#">Great Grand Child</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                  </ul>-->
                </li>
              </ul>
            </div>*}

        </div>

    </div>
</div>


<div class="row">
  <!--<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">

  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>-->
  <div class="col-md-12 col-sm-12">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">{$page_header}</h3>
      </div>

    </div>
    <!-- /.box -->
  </div>

  <div id="content-pop-over" style="display:none;">

  </div>

</div>
{/block}
{block name=script_js}
  {assign var='url' value="Member_master/get_memberkanan/{$get['id_member']}"}
  {assign var='url2' value="Member_master/get_memberkiri/{$get['id_member']}"}
  <script>
   
     jQuery(document).ready(function($) {
    $('#member').autocomplete({
      source: "{site_url('cbm/member_master/autocomplete_hirarki')}",
      select: function(event, data) {
        $("#member").focus();
       
      }
    });


				$("#target").attr("href","{site_url('cbm/member_master/hirarki/')}" + $("#member").val());
				return false; 
			});
    
 
    /*
    <ul>
      <li>
        <a href="#">Child</a>
        <ul>
          <li>
            <a href="#">Grand Child</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="#">Child</a>
        <ul>
          <li><a href="#">Grand Child</a></li>
          <li>
            <a href="#">Grand Child</a>
            <ul>
              <li>
                <a href="#">Great Grand Child</a>
              </li>
              <li>
                <a href="#">Great Grand Child</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
    */
    var data_downline = {json_encode($listing_downline_data)};
    // console.log(data_downline);

    var gambar_downline = function(data_downline){

      $.each(data_downline,function(index,el){ /// each level 1 yang tampil adalah subs
        // console.log(index);
        $.each(el.subs,function(index_2, el_2) { // each level 2 ynag tampil adalah kanan dan kiri. untuk index_2 sudah tampil id upline
          // console.log(index_2);
          // console.log(el_2);
          var html = '<ul>';

          $.each(el_2,function(index_3, el_3) { // each level 3
            // console.log(index_3);
            // console.log(el_3);
                // html +='<li id="'+el_3.id_member+'">';
                var link = "{site_url('cbm/member_master/hirarki')}/"+el_3.id;
                var lokasi_gambar = "{base_url()}admin718/assets/img/gambar_dummy/orang.jpg";
                var lokasi_gambar_asli = "{base_url()}admin718/assets/img/username/"+el_3.image;
                  // html +='<a href="'+link+'">';
                  // html +='<span>('+index_3+')</span><br>';
                    html +='<span>'+el_3.id_member+'</span><br>';
                    html +='<span>'+el_3.nama_lengkap+'</span><br>';
                    if(el_3.image == null || el_3.image =="" || el_3.image =="00x"){
                      html +='<img src="'+lokasi_gambar+'" width="100px" height="100px">';
                    }else{
                      html +='<img src="'+lokasi_gambar_asli+'" width="100px" height="100px">';
                    }
                  // html +='</a>';
                  // html +='<ul>';
                  //   html +='<li>';
                  //     html +='<a href="#">Grand Child</a>';
                  //   html +='</li>';
                  // html +='</ul>';
                // html +='</li>'
          });

          html +='</ul>';
          $('#'+index_2).append(html);

        });// each level 2 ynag tampil adalah kanan dan kiri. untuk index_2 sudah tampil id upline
      });/// each level 1 yang tampil adalah subs
    }
    // gambar_downline(data_downline);
    var data_downline_2 = {$hasil_downline};
    var gambar_downline_2 = function(data_downline){

      console.log(data_downline);

      $.each(data_downline,function(index, el) {
        if(el.id !=""){
          var html = '';
          var link = "{site_url('cbm/member_master/hirarki')}/"+el.id;
          var lokasi_gambar = "{base_url()}admin718/assets/img/gambar_dummy/orang.jpg";
          var lokasi_gambar_asli = "{base_url()}admin718/assets/img/username/"+el.image;
            html +='<a href="'+link+'" onmouseover="pop_tampil('+index+')">';
            // html +='<span>('+index+')</span><br>';
              html +='<span>'+el.id_member+'</span><br>';
              html +='<span>'+el.nama_lengkap+'</span><br>';
              if(el.image == null || el.image =="" || el.image =="00x"){
                html +='<img src="'+lokasi_gambar+'" width="90px" height="90px">';
              }else{
                html +='<img src="'+lokasi_gambar_asli+'" width="90px" height="90px">';
              }



            var link_total = "{site_url('cbm/member_master/total_downline')}/"+el.id;
            $.getJSON(link_total, { }, function(json, textStatus) {
                /*optional stuff to do after success */
                $('#pos-'+index+'-total_kiri').html(json.total_kiri);
                $('#pos-'+index+'-total_kanan').html(json.total_kanan);

                html +='<div class="blok-total">';
                  html +='<span class="span-inline" id="pos-'+index+'-total_kiri">'+json.total_kiri+'</span><span class="span-inline" id="pos-'+index+'-total_kanan">'+json.total_kanan+'</span>';
                html +='</div>';
              html +='</a>';

              $('#pos-'+index).html(html);


            });

        }

      });
    }

    gambar_downline_2(data_downline_2);
    var pop_tampil = function(id){
      var data_json = data_downline_2[id];
      // console.log(data_json.id_member);
      var html = '';
      html +='<table class="table table-striped">';
        html +='<tr>';
          html +='<td>Tgl Gabung</td>';
          html +='<td>'+data_json.tgl+'</td>';
        html +='</tr>';
        html +='<tr>';
          html +='<td>Username</td>';
          html +='<td>'+data_json.id_member+'</td>';
        html +='</tr>';
        html +='<tr>';
          html +='<td>Nama</td>';
          html +='<td>'+data_json.nama_lengkap+'</td>';
        html +='</tr>';
        html +='<tr>';
          html +='<td>Sponsor</td>';
          html +='<td>'+data_json.id_sponsor+'</td>';
        html +='</tr>'
        html +='<tr>';
          html +='<td>Nama Sponsor</td>';
          if(data_json.nama_sponsor == null){
            html +='<td></td>';
          }else{
            html +='<td>'+data_json.nama_sponsor+'</td>';
          }

        html +='</tr>';
        html +='<tr>';
          html +='<td>Upline</td>';
          html +='<td>'+data_json.id_upline+'</td>';
        html +='</tr>';
        html +='<tr>';
          html +='<td>Nama Upline</td>';
          if(data_json.nama_upline == null){
            html +='<td></td>';
          }else{
            html +='<td>'+data_json.nama_upline+'</td>';
          }
        html +='</tr>';
      html +='</table>';

      $('#content-pop-over').html(html);
      if(id == 5 || id == 6 || id == 7 || id == 8 || id == 3 || id == 4){
        $('#pos-'+id).popover({ trigger: "hover",content: $('#content-pop-over').html(),
        html: true,placement:'right' });
      }
      if(id == 11 || id == 12 || id == 13 || id == 14 || id == 9 || id == 10){
        $('#pos-'+id).popover({ trigger: "hover",content: $('#content-pop-over').html(),
        html: true,placement:'left' });
      }else{
        $('#pos-'+id).popover({ trigger: "hover",content: $('#content-pop-over').html(),
        html: true,placement:'bottom' });
      }

    }
    pop_tampil(1);
    $('#i_pop').popover({ trigger: "hover",content: $('#content-pop-over').html(),
    html: true,placement:'bottom' });


  </script>

{/block}
{block name=script_css}
  <style media="screen">
    /*Now the CSS*/

    * {
      margin: 0;
      padding: 0;
    }
    .blok-total{
      width: 100%;
      display: block;
    }
    .span-inline {
      background-color:#c8e4f8;
      text-align: center;
      width: 50%;
      display: inline-block;
    }
    .tree-bar-x ul {

      padding-top: 20px;
      position: relative;


      transition: all 0.5s;
      -webkit-transition: all 0.5s;
      -moz-transition: all 0.5s;
    }

    .tree-bar-x li {
      float: left;
      text-align: center;
      list-style-type: none;
      position: relative;
      padding: 20px 5px 0 5px;

      transition: all 0.5s;
      -webkit-transition: all 0.5s;
      -moz-transition: all 0.5s;
    }
    /*We will use ::before and ::after to draw the connectors*/

    .tree-bar-x li::before,
    .tree-bar-x li::after {
      content: '';
      position: absolute;
      top: 0;
      right: 50%;
      border-top: 1px solid #ccc;
      width: 50%;
      height: 20px;
    }

    .tree-bar-x li::after {
      right: auto;
      left: 50%;
      border-left: 1px solid #ccc;
    }
    /*We need to remove left-right connectors from elements without
      any siblings*/

    .tree-bar-x li:only-child::after,
    .tree-bar-x li:only-child::before {
      display: none;
    }
    /*Remove space from the top of single children*/

    .tree-bar-x li:only-child {
      padding-top: 0;
    }
    /*Remove left connector from first child and
      right connector from last child*/

    .tree-bar-x li:first-child::before,
    .tree-bar-x li:last-child::after {
      border: 0 none;
    }
    /*Adding back the vertical connector to the last nodes*/

    .tree-bar-x li:last-child::before {
      border-right: 1px solid #ccc;
      border-radius: 0 5px 0 0;
      -webkit-border-radius: 0 5px 0 0;
      -moz-border-radius: 0 5px 0 0;
    }

    .tree-bar-x li:first-child::after {
      border-radius: 5px 0 0 0;
      -webkit-border-radius: 5px 0 0 0;
      -moz-border-radius: 5px 0 0 0;
    }
    /*Time to add downward connectors from parents*/

    .tree-bar-x ul ul::before {
      content: '';
      position: absolute;
      top: 0;
      left: 50%;
      border-left: 1px solid #ccc;
      width: 0;
      height: 20px;
    }

    .tree-bar-x li a {
      border: 1px solid #ccc;

      text-decoration: none;
      color: #666;
      font-family: arial, verdana, tahoma;
      font-size: 11px;
      display: inline-block;

      border-radius: 5px;
      -webkit-border-radius: 5px;
      -moz-border-radius: 5px;

      transition: all 0.5s;
      -webkit-transition: all 0.5s;
      -moz-transition: all 0.5s;
    }
    /*Time for some hover effects*/
    /*We will apply the hover effect the the lineage of the element also*/

    .tree-bar-x li a:hover,
    .tree-bar-x li a:hover+ul li a {
      background: #c8e4f8;
      color: #000;
      border: 1px solid #94a0b4;
    }
    /*Connector styles on hover*/

    .tree-bar-x li a:hover+ul li::after,
    .tree-bar-x li a:hover+ul li::before,
    .tree-bar-x li a:hover+ul::before,
    .tree-bar-x li a:hover+ul ul::before {
      border-color: #94a0b4;
    }
    /*Thats all. I hope you enjoyed it.
      Thanks :)*/

      @media (min-width) {
        overflow-y: scroll;
      }
  </style>
{/block}
