{extends file=$themes} {block name="main-content"}

<div class="panel panel-border panel-custom" style="margin-top:2%;">
  <div class="panel-heading">

  </div>
  <div class="panel-body">
    <div class="col-sm-12">
      <div class="form-group">
        {if $this->session->flashdata('pesan') !=""}
          {$this->session->flashdata('pesan')}
        {/if}
      </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">{$page_header}</h3><br>
                          {if $this->session->flashdata('pesan') !=""}
                            {$this->session->flashdata('pesan')}
                          {/if}
                        </div><!-- /.box-header -->
                        <!-- form start -->

                          <div class="box-body">
                            <div class="row">

                              <div class="col-md-12" id="filter">
                                  <div class="portlet ">
                                      <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default">
                                       <div class="portlet-heading bg-custom">

                                           <h3 class="portlet-title">
                                                Pencarian Data
                                           </h3>

                                           <div class="clearfix"></div>
                                       </div>
                                     </a>
                                       <div id="bg-default" class="panel-collapse collapse out">
                                           <div class="portlet-body">

                                {assign var=aturan_form  value=['method'=>'get','class'=>'form-horizontal']}
                                {form_open(site_url('cbm/komisi/bonus_pasangan/fetch'),$aturan_form)}
                                <!--
                                $halaman = $this->input->get('halaman',true);
                                $jenis_operasi = $this->input->get('jenis_operasi',true);
                                $keyword = $this->input->get('keyword',true);
                                $ip_user = $this->input->get('ip_user',true);
                                $user_id = $this->input->get('user_id',true);
                                $tgl_1 = $this->input->get('tgl_1',true);
                                $tgl_2 = $this->input->get('tgl_2',true);
                               -->
                              <div class="form-group">
                                  <label class="col-md-2 control-label" for="example-email">Status Komisi</label>
                                  <div class="col-md-8">
                                      <select name="status_komisi" id="status_komisi" class="form-control">
                                          <option value=""></option>
                                          <option value="0">Belum Di Approve</option>
                                          <option value="1">Sudah Di Approve</option>
                                        </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Tanggal Mulai</label>
                                  <div class="col-md-8">
                                      <input type="text"  name="tgl_1" id="tgl_1" class="form-control" >
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Tanggal Selasai</label>
                                  <div class="col-md-8">
                                      <input type="text"  name="tgl_2" id="tgl_2" class="form-control" >
                                  </div>
                              </div>


                                <div class="form-group m-b-0">
                                    <div class="col-sm-offset-2 col-sm-8">

                                      <button type="submit" class="btn btn-info waves-effect waves-light">
                                          <span class="btn-label"><i class="fa  md-search"></i>
                                          </span>Cari</button>
                                    </div>
                                </div>
                                {form_close()}
                              </div>

                                           </div>
                                       </div>

                              <div class="form-group m-b-0">
                                      <a href="{site_url('cbm/komisi/bonus_pasangan/export')}" class="btn btn-default btn-rounded waves-effect waves-light pull-left">Export</a>

                              </div>
                            </div>
                            <div class="row" style="margin-top:10px;">
                              <div class="col-md-12">
                                <div class="table-responsive">
                                  <table id="" class="table table-bordered table-striped">
                                    <thead>
                                      <tr>
                                        <th>No</th>
                                        <th>Penerima</th>
                                        <th>ID Penerima</th>
                                        <th>Rekening</th>
                                        <th>Nama Rekening</th>
                                        <th>Jumlah Pasangan</th>
                                        <th>Bonus Pasangan</th>
                                        <th>status Terima</th>
                                        <th>Tgl Approve</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {if array_key_exists('pesan',$list_bonus)}
                                      <tr>
                                        <td colspan="7">Data Bonus Pasangan Tidak Ditemukan</td>
                                      </tr>
                                  {/if}
                                  {if !array_key_exists('pesan',$list_bonus)}
                                  {assign var=no value=$offset}
                                  {foreach from=$list_bonus key=k item=v}
                                  {if $v['total_komisi'] > 0 && $v['jumlah_pasangan'] > 0}
                                   {assign var=no value=$no+1}
                                    <tr>
                                      <td>{$no}</td>
                                      <td>{$v['penerima']}</td>
                                      <td>{$v['id_penerima']}</td>
                                      <td>{$v['nama_bank']} - {$v['no_rek']}</td>
                                      <td>{$v['an_bank']}</td>
                                      <td>{$v['jumlah_pasangan']}</td>
                                      <td>{number_format($v['total_komisi'],0,",",".")}</td>
                                      <td>{$v['status_terima']}</td>
                                      <td>{$v['tgl_approve']}</td>

                                      </tr>
                                      {/if}
                                      {/foreach}
                                      {/if}
                                    </tbody>
                                  </table>
                                </div>

                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                {$pagination}
                              </div>
                            </div>
                          </div><!-- /.box-body -->

                      </div><!-- /.box -->
</div>
</div>






{/block}
{block name=script_js}
<script>

  $('#penerima').autocomplete({
    source:"{site_url('cbm/komisi/bonus_pasangan/autocomplete_member')}",
    delay: 1000
  });

  $("#tgl_1").datepicker({ format:'yyyy-mm-dd' });
  $("#tgl_2").datepicker({ format:'yyyy-mm-dd' });

</script>

{/block}
