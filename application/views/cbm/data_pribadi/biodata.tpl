{extends file=$themes} {block name="main-content"}
<!-- biodata  -->
<div class="row">
  <div class="col-sm-12">
    <div class="card-box">

      <h4 class="m-t-0 header-title"><b>Data Member</b></h4>
      <div class="row">
        <div class="col-md-8">
          <div class="form-group">
            {if $this->session->flashdata('pesan') !=""}
              {$this->session->flashdata('pesan')}
            {/if}
          </div>
            <table class="table table-striped">
              <tr>
                <td width="30%">ID Member</td>
                <td>:</td>
                <td>{$get['id_member']}</td>
              </tr>
              <tr>
                <td width="30%">Username</td>
                <td>:</td>
                <td>{$get['id_member']}</td>
              </tr>
              <tr>
                <td width="30%">Peringkat</td>
                <td>:</td>
                <td>-</td>
              </tr>

            </table>
        </div>

        <div class="col-md-4">
        </div>
      </div>

      <h4 class="m-t-0 header-title"><b>Data Jaringan</b></h4>
      <div class="row">
        <div class="col-md-8">
            <table class="table table-striped">
              <tr>
                <td width="30%">Sponsor</td>
                <td>:</td>
                <td>{$sponsor['id_member']}</td>
              </tr>
              <tr>
                <td width="30%">Nama Sponsor</td>
                <td>:</td>
                <td>{$sponsor['nama_lengkap']}</td>
              </tr>
              <tr>
                <td width="30%">Upline</td>
                <td>:</td>
                <td>{$upline['id_member']}</td>
              </tr>
              <tr>
                <td width="30%">Nama Upline</td>
                <td>:</td>
                <td>{$upline['nama_lengkap']}</td>
              </tr>

            </table>
        </div>

        <div class="col-md-4">
        </div>
      </div>

      <h4 class="m-t-0 header-title"><b>Biodata</b></h4>

      <div class="row">
        <div class="col-md-8">
          {form_open_multipart(site_url('cbm/detail/upload_foto'))}
            <table class="table table-striped">
              <tr>
                <td width="30%">Nama Lengkap</td>
                <td>:</td>
                <td>{$get['nama_lengkap']}</td>
              </tr>
              <tr>
                <td width="30%">No. Identitas</td>
                <td>:</td>
                <td>{$get['no_identitas']}</td>
              </tr>
              <tr>
                <td width="30%">Alamat</td>
                <td>:</td>
                <td>{$get['alamat']}</td>
              </tr>
              <tr>
                <td width="30%">No HP</td>
                <td>:</td>
                <td>{$get['no_hp']}</td>
              </tr>
              <tr>
                <td width="30%">Telp</td>
                <td>:</td>
                <td>{$get['no_telp']}</td>
              </tr>
              <tr>
                <td width="30%">Ahli Waris</td>
                <td>:</td>
                <td>{$get['ahli_waris']}</td>
              </tr>
              <tr>
                <td width="30%">Hubungan</td>
                <td>:</td>
                <td>{$get['hubungan']}</td>
              </tr>
              <tr>
                <td width="30%">Nama Bank</td>
                <td>:</td>
                <td>{$get['nama_bank']}</td>
              </tr>
              <tr>
                <td width="30%">NO Rekening</td>
                <td>:</td>
                <td>{$get['no_rek']}</td>
              </tr>
              <tr>
                <td width="30%">A.n Bank</td>
                <td>:</td>
                <td>{$get['atas_nama']}</td>
              </tr>
              <tr>
                <td width="30%">Foto Profile</td>
                <td>:</td>
                <td>
                  <input type="file" name="foto" id="">
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <input type="submit" value="Ganti Foto Profil" class="btn btn-primary">
                </td>
              </tr>
            </table>
          {form_close()}
        </div>

        <div class="col-md-4">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- biodata  -->

{/block} {block name=script_js}
<script>
  jQuery(document).ready(function($) {

    $('#id_sponsor').autocomplete({
      source: "{site_url('cbm/member_manajemen/autocomplete_sponsor')}",
      select: function(event, data) {}
    });
    $('#id_upline').autocomplete({
      source: "{site_url('cbm/member_manajemen/autocomplete_upline')}",
      select: function(event, data) {}
    });

  });

  $("#propinsi_asal_ori").change(function() {

    $("#kecamatan_asal_ori").hide();
    $("#kota_asal_ori").hide();

    var propinsi_origin = $("#propinsi_asal_ori").val();
    var urls = '{site_url("cbm/member_manajemen/kota_html")}';
    $.ajax({
      //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
      url: urls + '?propinsi_origin=' + propinsi_origin,
      type: 'GET',
      dataType: 'html',
      success: function(data) {
        var html = data;
        $("#kota_asal_ori").html(html);
        $("#kota_asal_ori").show();
      }
    });

  })

  // kecamatan ori
  $("#kota_asal_ori").change(function() {
    $("#kecamatan_asal_ori").hide();
    var propinsi_origin = $("#propinsi_asal_ori").val();
    var kota_origin = $("#kota_asal_ori").val();
    var urls = '{site_url("cbm/member_manajemen/kecamatan_html")}';
    $.ajax({
      //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
      url: urls + '?propinsi_origin=' + propinsi_origin + '&kota_origin=' + kota_origin,
      type: 'GET',
      dataType: 'html',
      success: function(data) {
        var html = data;

        $("#kecamatan_asal_ori").html(html);
        $("#kecamatan_asal_ori").show();
      }
    });

  });


  /// setingan untuk rajaongkir

  {
    if !isset($kota_asal) && !isset($kecamatan_asal) && !isset($kota_tujuan) && !isset($kecamatan_tujuan)
  }
  // $("#kota_asal_ori").hide();
  // $("#kecamatan_asal_ori").hide();
  $("#kota_asal").hide();
  $("#kecamatan_asal").hide();
  $("#kota_tujuan").hide();
  $("#kecamatan_tujuan").hide(); {
    /if}
</script>
{/block}
