{extends file=$themes} {block name="main-content"}

<!-- form registrasi  -->
<div class="row">
  <div class="col-sm-12">
    <div class="card-box">
      <div class="row">
        <div class="col-md-12">

          {form_open_multipart(site_url('cbm/detail/proses_ganti_pin'))}
          <div class="form-group">
            {if $this->session->flashdata('pesan') !=""}
              {$this->session->flashdata('pesan')}
            {/if}
          </div>
          <div class="form-group">
            {form_error('pin_lama')}
            <label for=""><span class="text-red">*</span> PIN Lama</label>
            <input type="text" name="pin_lama" id="" class="form-control" required>
          </div>
            <div class="form-group">
              {form_error('pin')}
              <label for=""><span class="text-red">*</span> PIN</label>
              <input type="text" name="pin" id="" class="form-control" required>
            </div>
            {form_error('pin-konfirm')}
            <div class="form-group">
              <label for=""><span class="text-red">*</span> Konfirmasi PIN</label>
              <input type="password" name="pin-konfirm" id="" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-purple waves-effect waves-light">Ganti Pin</button>
          {form_close()}
        </div>

      </div>
    </div>
  </div>
</div>
<!-- form registrasi  -->

{/block} {block name=script_js}
<script>
  jQuery(document).ready(function($) {

    $('#id_sponsor').autocomplete({
      source: "{site_url('cbm/member_manajemen/autocomplete_sponsor')}",
      select: function(event, data) {}
    });
    $('#id_upline').autocomplete({
      source: "{site_url('cbm/member_manajemen/autocomplete_upline')}",
      select: function(event, data) {}
    });

  });

  $("#propinsi_asal_ori").change(function() {

    $("#kecamatan_asal_ori").hide();
    $("#kota_asal_ori").hide();

    var propinsi_origin = $("#propinsi_asal_ori").val();
    var urls = '{site_url("cbm/member_manajemen/kota_html")}';
    $.ajax({
      //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
      url: urls + '?propinsi_origin=' + propinsi_origin,
      type: 'GET',
      dataType: 'html',
      success: function(data) {
        var html = data;
        $("#kota_asal_ori").html(html);
        $("#kota_asal_ori").show();
      }
    });

  })

  // kecamatan ori
  $("#kota_asal_ori").change(function() {
    $("#kecamatan_asal_ori").hide();
    var propinsi_origin = $("#propinsi_asal_ori").val();
    var kota_origin = $("#kota_asal_ori").val();
    var urls = '{site_url("cbm/member_manajemen/kecamatan_html")}';
    $.ajax({
      //url: 'index.php?route=shipping/huda_kurir/listdestination&propinsi='+propinsi_origin+'&token='+token,
      url: urls + '?propinsi_origin=' + propinsi_origin + '&kota_origin=' + kota_origin,
      type: 'GET',
      dataType: 'html',
      success: function(data) {
        var html = data;

        $("#kecamatan_asal_ori").html(html);
        $("#kecamatan_asal_ori").show();
      }
    });

  });


  /// setingan untuk rajaongkir

  {if !isset($kota_asal) && !isset($kecamatan_asal) && !isset($kota_tujuan) && !isset($kecamatan_tujuan)}
  // $("#kota_asal_ori").hide();
  // $("#kecamatan_asal_ori").hide();
  $("#kota_asal").hide();
  $("#kecamatan_asal").hide();
  $("#kota_tujuan").hide();
  $("#kecamatan_tujuan").hide();
  {/if}
</script>
{/block}
