<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon_1.ico">

        <title>{$this->site_name}</title>

        <!--Morris Chart CSS -->
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">

        <link href="{theme_url('assets/css/bootstrap.min.css')}" rel="stylesheet" type="text/css" />
        <link href="{theme_url('assets/css/core.css')}" rel="stylesheet" type="text/css" />
        <link href="{theme_url('assets/css/components.css')}" rel="stylesheet" type="text/css" />
        <link href="{theme_url('assets/css/icons.css')}" rel="stylesheet" type="text/css" />
        <link href="{theme_url('assets/css/pages.css')}" rel="stylesheet" type="text/css" />
        <link href="{theme_url('assets/css/responsive.css')}" rel="stylesheet" type="text/css" />
        <link href="{theme_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}" rel="stylesheet">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{theme_url('assets/js/modernizr.min.js')}"></script>

        {if isset($css_head)}

      				{foreach from=$css_head item=css }
      						<link rel="stylesheet" href="{base_url('assets')}/{$css}">
      				{/foreach}
      	{/if}

      	{if isset($js_head)}

      				{foreach from=$js_head item=js }
      						<script src="{base_url('assets')}/{$js}"></script>
      				{/foreach}

      	{/if}


      	{block name="script_css"}

      	{/block}


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                      <!--   <a href="{$this->site_url}" class="logo">CBM<span>{$this->site_name}</span></a> -->
                        <a href="{$this->site_url}" class="logo">CBM<span> INDONESIA</span></a>
                        <!-- Image Logo here -->
                        <!--<a href="index.html" class="logo">-->
                            <!--<i class="icon-c-logo"> <img src="{theme_url('assets/images/logo_sm.png" height="42')}"/> </i>-->
                            <!--<span><img src="{theme_url('assets/images/logo_light.png" height="20')}"/></span>-->
                        <!--</a>-->
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect waves-light">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="dropdown top-menu-item-xs">
                                    <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                        <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">{$this->t_new_notif}</span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-lg">
                                        <li class="notifi-title"><span class="label label-default pull-right">Baru {$this->t_new_notif}</span>Notifikasi</li>
                                        <li class="list-group slimscroll-noti notification-list">
                                          {foreach from=$this->notif_member key=k item=i}
                                           <!-- list item-->
                                           {if $i['status_notif'] == '0'}
                                           <a href="{site_url('cbm/notifikasi/link')}/{$i['id']}" class="list-group-item" title="{$i['pesan_notif']}">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <img src="{$i['gambar_notif']}" alt="">
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">{$i['title_notif']}</h5>
                                                    <p class="m-0">
                                                        <small>{word_limiter($i['pesan_notif'],10)}</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>
                                           {/if}
                                           {/foreach}
                                        </li>
                                        <li>
                                            <a href="{site_url('cbm/notifikasi/lihat_semua_notifikasi')}" class="list-group-item text-right">
                                                <small class="font-600">Lihat semua notifikasi</small>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="hidden-xs">
                                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                </li>
                                <li class="dropdown top-menu-item-xs">
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                      {if $this->session->userdata('image') !=""}
                                        <img src="{base_url()}admin718/assets/img/username/{$this->session->userdata('image')}" alt="user-img" class="img-circle" width="35px" height="36px">
                                      {/if}
                                      {if $this->session->userdata('image') ==""}
                                        <img src="{theme_url('assets/images/users/avatar-1.jpg')}" alt="user-img" class="img-circle">
                                      {/if}
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                                        <li><a href="{site_url('logout')}"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                        	<!-- <li class="text-muted menu-title">Navigation</li> -->

                            <!-- <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="index.html">Dashboard 1</a></li>
                                    <li><a href="dashboard_2.html">Dashboard 2</a></li>
                                    <li><a href="dashboard_3.html">Dashboard 3</a></li>
                                    <li><a href="dashboard_4.html">Dashboard 4</a></li>
                                </ul>
                            </li> -->
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Profile </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="{site_url('cbm/detail/biodata')}">Biodata</a></li>
                                    <li><a href="{site_url('cbm/detail/ganti_pin')}">Ganti Pin</a></li>
                                </ul>
                            </li>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i> <span> Jaringan </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                  <li><a href="{site_url('cbm/member_master/views')}">Statement Member</a></li>
                                  <li><a href="{site_url('cbm/member_master/hirarki')}">Geneologi Jaringan</a></li>
								    <li><a href="{site_url('cbm/member_master/sponsoring')}">Tabel Sponsoring</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class=" md-wallet-giftcard"></i> <span> Komisi </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="{site_url('cbm')}/komisi/bonus_pasangan/fetch">Bonus Pasangan</a></li>
                                    <li><a href="{site_url('cbm')}/komisi/bonus_sponsor/fetch">Bonus Sponsor</a></li>
                                    {if $this->member_group_akses['member'] > 0}
									   <li><a href="{site_url('cbm')}/komisi/bonus_cabang/fetch">Bonus Cabang</a></li>
                                    {/if}
                                </ul>
                            </li>
                            {if $this->total_akses > 0}

                                    {if $this->component_akses_member !=0}
                                      {foreach from=$this->component_akses_member key=k item=v}
                                        {if array_key_exists($v['code_component'],$this->member_group_akses)}
                                          {if $this->member_group_akses[$v['code_component']] > 0}
                                            <li><a href="{site_url('cbm')}/{$v['url_front']}"><i class="ti-save"></i> <span>{$v['nama_component']}</span></a></li>
                                          {/if}
                                        {/if}
                                      {/foreach}
                                    {/if}
                            {/if}

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">{$page_title}</h4>
                            </div>
                        </div>

                        <!-- Main content  -->
                        	{block name="main-content"}
                            <div class="row">
                                <div class="col-md-6 col-lg-3">
                                    <div class="widget-bg-color-icon card-box fadeInDown animated">
                                        <div class="bg-icon bg-icon-info pull-left">
                                            <i class="md md-attach-money text-info"></i>
                                        </div>
                                        <div class="text-right">
                                            <h3 class="text-dark"><b class="counter">31,570</b></h3>
                                            <p class="text-muted">Total Revenue</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-3">
                                    <div class="widget-bg-color-icon card-box">
                                        <div class="bg-icon bg-icon-pink pull-left">
                                            <i class="md md-add-shopping-cart text-pink"></i>
                                        </div>
                                        <div class="text-right">
                                            <h3 class="text-dark"><b class="counter">280</b></h3>
                                            <p class="text-muted">Today's Sales</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-3">
                                    <div class="widget-bg-color-icon card-box">
                                        <div class="bg-icon bg-icon-purple pull-left">
                                            <i class="md md-equalizer text-purple"></i>
                                        </div>
                                        <div class="text-right">
                                            <h3 class="text-dark"><b class="counter">0.16</b>%</h3>
                                            <p class="text-muted">Conversion</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-3">
                                    <div class="widget-bg-color-icon card-box">
                                        <div class="bg-icon bg-icon-success pull-left">
                                            <i class="md md-remove-red-eye text-success"></i>
                                        </div>
                                        <div class="text-right">
                                            <h3 class="text-dark"><b class="counter">64,570</b></h3>
                                            <p class="text-muted">Today's Visits</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->
                          {/block}
                        <!-- Main content  -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    © 2017. All rights reserved.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar nicescroll">
                <h4 class="text-center">Chat</h4>
                <div class="contact-list nicescroll">
                    <ul class="list-group contacts-list">
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{theme_url('assets/images/users/avatar-1.jpg')}" alt="">
                                </div>
                                <span class="name">Chadengle</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{theme_url('assets/images/users/avatar-2.jpg')}" alt="">
                                </div>
                                <span class="name">Tomaslau</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{theme_url('assets/images/users/avatar-3.jpg')}" alt="">
                                </div>
                                <span class="name">Stillnotdavid</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{theme_url('assets/images/users/avatar-4.jpg')}" alt="">
                                </div>
                                <span class="name">Kurafire</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{theme_url('assets/images/users/avatar-5.jpg')}" alt="">
                                </div>
                                <span class="name">Shahedk</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{theme_url('assets/images/users/avatar-6.jpg')}" alt="">
                                </div>
                                <span class="name">Adhamdannaway</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{theme_url('assets/images/users/avatar-7.jpg')}" alt="">
                                </div>
                                <span class="name">Ok</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{theme_url('assets/images/users/avatar-8.jpg')}" alt="">
                                </div>
                                <span class="name">Arashasghari</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{theme_url('assets/images/users/avatar-9.jpg')}" alt="">
                                </div>
                                <span class="name">Joshaustin</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{theme_url('assets/images/users/avatar-10.jpg')}" alt="">
                                </div>
                                <span class="name">Sortino</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{theme_url('assets/js/jquery.min.js')}"></script>
        <script src="{theme_url('assets/js/bootstrap.min.js')}"></script>
        <script src="{theme_url('assets/js/detect.js')}"></script>
        <script src="{theme_url('assets/js/fastclick.js')}"></script>

        <script src="{theme_url('assets/js/jquery.slimscroll.js')}"></script>
        <script src="{theme_url('assets/js/jquery.blockUI.js')}"></script>
        <script src="{theme_url('assets/js/waves.js')}"></script>
        <script src="{theme_url('assets/js/wow.min.js')}"></script>
        <script src="{theme_url('assets/js/jquery.nicescroll.js')}"></script>
        <script src="{theme_url('assets/js/jquery.scrollTo.min.js')}"></script>

        <script src="{theme_url('assets/plugins/peity/jquery.peity.min.js')}"></script>

        <!-- jQuery  -->
        <script src="{theme_url('assets/plugins/waypoints/lib/jquery.waypoints.js')}"></script>
        <script src="{theme_url('assets/plugins/counterup/jquery.counterup.min.js')}"></script>



        <script src="{theme_url('assets/plugins/morris/morris.min.js')}"></script>
        <script src="{theme_url('assets/plugins/raphael/raphael-min.js')}"></script>

        <script src="{theme_url('assets/plugins/jquery-knob/jquery.knob.js')}"></script>

        <script src="{theme_url('assets/pages/jquery.dashboard.js')}"></script>

        <script src="{theme_url('assets/js/jquery.core.js')}"></script>
        <script src="{theme_url('assets/js/jquery.app.js')}"></script>
        <script src="{theme_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });

                $(".knob").knob();

            });
        </script>

        {if isset($js_footer)}
        			{foreach from=$js_footer item=v }
        					<script src="{base_url('assets')}/{$v}"></script>
        			{/foreach}
        {/if}

        {block name="script_js"}

        {/block}


    </body>
</html>
