<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Progress - Free Responsive Website Template by Download Website Templates</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{base_url()}assets/maintenance/images/ico/apple-touch-icon-144.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{base_url()}assets/maintenance/images/ico/apple-touch-icon-114.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{base_url()}assets/maintenance/images/ico/apple-touch-icon-72.png">
<link rel="apple-touch-icon-precomposed" href="{base_url()}assets/maintenance/images/ico/apple-touch-icon-57.png">
<link rel="shortcut icon" href="{base_url()}assets/maintenance/images/ico/favicon.png">
<!--[if IE]><![endif]-->
<link rel="stylesheet" href="{base_url()}assets/maintenance/css/style.css">
<script src="{base_url()}assets/maintenance/js/jquery.js"></script>
<script src="{base_url()}assets/maintenance/js/countdown.js"></script>
<script src="{base_url()}assets/maintenance/js/owlcarousel.js"></script>
<script src="{base_url()}assets/maintenance/js/uikit.scrollspy.js"></script>


</head>
<body id="backtotop">

<div class="fullwidth clearfix">
	<div id="topcontainer" class="bodycontainer clearfix" data-uk-scrollspy="{ cls:'uk-animation-fade', delay: 300, repeat: true }">
		{if $this->session->flashdata('pesan') !=""}
	        {$this->session->flashdata('pesan')}
	    {/if}
	    <br>
		<p><span class="fa fa-signal"></span></p>
		<h2>{$this->site_name}</h2>
		<h1><span>Progress</span><br />is coming soon</h1>
		<p>It's almost ready ... honest</p>
		
	</div>
</div>

<div class="arrow-separator arrow-white"></div>

<div class="fullwidth colour1 clearfix">
	<div id="countdown" class="bodycontainer clearfix" data-uk-scrollspy="{ cls:'uk-animation-fade', delay: 300, repeat: true }">

		<div id="countdowncont" class="clearfix">
			<ul id="countscript">
				<li>
					<span class="days">00</span>
					<p>Hari</p>
				</li>
				<li>
					<span class="hours">00</span>
					<p>Jam</p>
				</li>
				<li class="clearbox">
					<span class="minutes">00</span>
					<p>Menit</p>
				</li>
				<li>
					<span class="seconds">00</span>
					<p>Detik</p>
				</li>
			</ul>
		</div>
	
	</div>
</div>

<div class="arrow-separator arrow-theme"></div>

<div class="fullwidth colour2 clearfix">
	<div id="maincont" class="bodycontainer clearfix" data-uk-scrollspy="{ cls:'uk-animation-fade', delay: 300, repeat: true }">

		{$this->pesan_maintenance} <br>
		<h4>{form_error('email')}</h4>
		<div id="signupform" class="sb-search clearfix">
			{form_open(site_url('maintenance/registemail'))}

				<input class="sb-search-input" type="email" name="email" placeholder="Enter email ..." type="text" value="">
				<input class="sb-search-submit" value="" type="submit">
				<button class="formbutton" type="submit"><span class="fa fa-search"></span></button>
			{form_close()}
		</div>
	
	</div>
</div>


<div class="arrow-separator arrow-grey"></div>

<div class="fullwidth clearfix">
	<div id="footercont" class="bodycontainer clearfix" data-uk-scrollspy="{ cls:'uk-animation-fade', delay: 300, repeat: true }">

		<p class="backtotop"><a title="" href="#backtotop"><span class="fa fa-angle-double-up"></span></a></p>
		<div id="socialmedia" class="clearfix">
			<ul>
				<li><a title="" href="#" rel="external"><span class="fa fa-facebook"></span></a></li>
				<li><a title="" href="#" rel="external"><span class="fa fa-twitter"></span></a></li>
				<li><a title="" href="#" rel="external"><span class="fa fa-google-plus"></span></a></li>
				<li><a title="" href="#" rel="external"><span class="fa fa-linkedin"></span></a></li>
				<li><a title="" href="#" rel="external"><span class="fa fa-pinterest"></span></a></li>
			</ul>
		</div>
	</div>
</div>

	<script>

	$(document).ready(function() {
		"use strict";
		$('.backtotop a').bind('click',function(event){
			var $anchor = $(this);
			$('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top }, 500,'easeInOutExpo');
			event.preventDefault();
		});
	});

	/** Countdown Timer **/

	$(document).ready(function() {
		"use strict";
		$("#countdown").countdown({
			date: "{$this->site_date|date_format:'%d %b %Y'} 10:00:00", /** Enter new date here **/
			format: "on"
		},
		function() {
			// callback function
		});
	});

	/** Owl Carousel **/

	$(window).load(function() {
		"use strict";
		$("#commentslider").owlCarousel({
			autoPlay: false, /** for autoplay - change false to 4000 to auto change every 4 seconds (for example) **/
			autoHeight: true,
			stopOnHover: true,
			navigation: false,
			navigationText: ["prev","next"],
			pagination: true,
			paginationNumbers: false,
			paginationSpeed: 500,
			goToFirstSpeed: 500,
			slideSpeed: 500,
			rewindSpeed: 500,
			singleItem: true,
			lazyLoad: false,
			transitionStyle: "backSlide" /** fade, backSlide, goDown, fadeUp - Remove whole line for slide transition **/
		});
	});

	</script>
   
</body>
</html>