{extends file=$smarty.const.ADMIN_PAGE}
{block name=content}
  	
  	<div class="row">
  		<div class="col-md-12">
  			<div class="box">
  				<div class="box-header">
  					
  				</div>
  				<div class="box-body">

  				</div>
  			</div>
  		</div>
  		<div class="col-md-12 col-sm-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">{$page_header}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                
                  <div class="box-body">
                    <table class="table table-bordered table-striped">
                    	<tr>
                    		<th>Nama Group</th>
                    		<th>Total User</th>
                    	</tr>
                    	{foreach from=$admin_groups key=k item=v}
                    	<tr>
                    		<td>{$v['nama_group']}</td>
                    		<td>{$v['total_user']}</td>
                    	</tr>
                    	{/foreach}
                    </table>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    
                  </div>

              </div><!-- /.box -->
            </div>

  	</div>

{/block}