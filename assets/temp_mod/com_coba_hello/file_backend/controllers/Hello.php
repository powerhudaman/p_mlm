<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hello extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
       
		
	}

	public function index()
	{
		$q = "select a.nama as nama_group, count(b.username) as total_user from admin_groups as a, admins as b where a.id = b.user_group_id group by a.id";
		$data['page_header']='hello';
		$data['admin_groups'] = $this->crut->list_datas($q);
        $this->parser->parse("hello.tpl", $data);
		
	}

}

/* End of file Hello.php */
/* Location: ./application/modules/hello/controllers/Hello.php */